//
//  PRCardsTableCell.m
//  pr
//
//  Created by Alexander Pimenov on 24.06.13.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PRCardsTableCell.h"
#import "UIColor+HexColorCreation.h"


@interface PRCardsTableCell ()

@property (weak, nonatomic) IBOutlet UILabel *cardNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *expirationDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *verifiedLabel;

@end

@implementation PRCardsTableCell

- (void)setVerified:(BOOL)verified
{
    _verified = verified;
    [[self verifiedLabel] setText: verified ? NSLocalizedString(@"YES", nil) : NSLocalizedString(@"NO", nil)];
}

- (void)setCardNumber:(NSString *)cardNumber
{
    _cardNumber = cardNumber;
    [[self cardNumberLabel] setText:cardNumber];
}

- (void)setExpirationDate:(NSString *)expirationDate
{
    _expirationDate = expirationDate;
    [[self expirationDateLabel] setText:expirationDate];
}

- (void)setActive:(BOOL)active
{
    _active = active;

    if (active)
    {
        [[self statusLabel] setText:@"AKTYWNA"];
        [[self statusLabel] setTextColor:[UIColor colorWithHexString:@"0DB63A"]];
    }
    else
    {
        [[self statusLabel] setText:@"ZABLOKOWANA"];
        [[self statusLabel] setTextColor:[UIColor colorWithHexString:@"AD2333"]];
    }
}

@end
