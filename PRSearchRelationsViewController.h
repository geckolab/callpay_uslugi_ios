//
//  PRSearchRelationsViewController.h
//  pr
//
//  Created by Marcin Szulc on 16.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PRDatePickerViewController.h"
#import "AppDelegate.h"

@class PRDatePickerViewController;
@class PRStandardControl;
@class PRStandardButton;

@interface PRSearchRelationsViewController : ProjectViewController <PRDatePickerProtocol, PRAppDelegateProtocol>

@property (strong, nonatomic) IBOutlet UILabel *labelStationFrom;
@property (strong, nonatomic) IBOutlet UILabel *labelStationTo;

@property (strong, nonatomic) IBOutlet UILabel *labelDate;

@property (strong, nonatomic) PRDatePickerViewController *datePickerViewController;
@property (strong, nonatomic) NSDate *date;

@property (strong, nonatomic) IBOutlet PRStandardButton *buttonSearch;

@property (strong, nonatomic) IBOutlet PRStandardControl *controlStationTo;

- (IBAction)controlDateOnClick:(id)sender;
- (IBAction)buttonSearchOnClick:(id)sender;
- (IBAction)controlStationFromOnClick:(id)sender;
- (IBAction)controlStationToOnClick:(id)sender;

- (void) resetOrderObject;

@end
