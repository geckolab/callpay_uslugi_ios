//
//  PRVerifyViewController.m
//  pr
//
//  Created by Marcin Szulc on 17.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PRVerifyViewController.h"
#import "PRUtils.h"
#import "PRAsyncConnection.h"
#import "XLog.h"
#import "SettingsEntity+Additions.h"
//#import "PRAppDelegate.h"
#import "Config.h"

#import "PRStandardButton.h"

#import "PRVerifySmsViewController.h"

#import "PRDBManager.h"

static NSString * const LogTag = @"VerifyViewController";

@interface PRVerifyViewController ()
{
    CGFloat kbHeight;
}

@property (strong, nonatomic) NSString *cert_token;

@end

@implementation PRVerifyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Register for keyboard notifications.
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillShowNotification:)
     name:UIKeyboardWillShowNotification object:nil];
    
//    PRAppDelegate *appDelegate = (PRAppDelegate*)[UIApplication sharedApplication].delegate;
    SettingsEntity *settings = [SettingsEntity fetchSettingsObjectWithManagedObjectContext:[[PRDBManager object]managedObjectContext]];
    
    _textName.text = settings.userName;
    _textSurname.text = settings.userSurname;
    _textPhoneNo.text = settings.userPhoneNo;
    
    
    [self enableOrDisableVerifyButton];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    // Deregister for keyboard notifications
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:UIKeyboardWillShowNotification object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"anuluj", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"gotowe", nil) style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    [numberToolbar sizeToFit];
    _textPhoneNo.inputAccessoryView = numberToolbar;
    
    UIBarButtonItem * mcmLogo = [[UIBarButtonItem alloc] initWithCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mcm_icon"]]];
    self.navigationItem.rightBarButtonItem = mcmLogo;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)cancelNumberPad{
    _textPhoneNo.text = @"";
    [self backgroundTouched:nil];
}

-(void)doneWithNumberPad{
    [self backgroundTouched:nil];
}

- (void) keyboardWillShowNotification:(NSNotification *)aNotification
{
    const BOOL isIpad = [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad;

    if(isIpad == YES)
    {
        return;
    }
    
    NSDictionary *info = [aNotification userInfo];
    kbHeight = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    
    [UIView animateWithDuration:0.2 animations:^{
        CGRect frame = self.view.frame;
        frame.origin.y =  MIN(0, -kbHeight + frame.size.height - _textPhoneNo.frame.origin.y - _textPhoneNo.frame.size.height);
        self.view.frame = frame;
    }];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"verifySms"])
    {
        PRVerifySmsViewController *dest = segue.destinationViewController;
        dest.phoneNo = _textPhoneNo.text;
        dest.cert_token = _cert_token;
    }
}

- (void) enableOrDisableVerifyButton
{
    
    if(_textName.text.length > 0 && _textPhoneNo.text.length > 0 && _textSurname.text.length>0 && _regulationsSwitch.isOn)
    {
        _buttonVerify.enabled = YES;
    }
    else
    {
        _buttonVerify.enabled = NO;
    }

}

- (void) saveNameSurnamePhone
{
//    PRAppDelegate *appDelegate = (PRAppDelegate*)[UIApplication sharedApplication].delegate;
    SettingsEntity *settings = [SettingsEntity fetchSettingsObjectWithManagedObjectContext:[[PRDBManager object]managedObjectContext]];
    
    settings.userName = _textName.text;
    settings.userSurname = _textSurname.text;
    settings.userPhoneNo = _textPhoneNo.text;
    
    NSError *error;
    
    if([[[PRDBManager object] managedObjectContext] save:&error] == NO)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        return;
    }
}

- (IBAction)textDidEndOnExit:(id)sender {
    [self saveNameSurnamePhone];
    
    if([sender isEqual:_textName])
    {
        [_textSurname becomeFirstResponder];
    }
    else if([sender isEqual:_textSurname])
    {
        [_textPhoneNo becomeFirstResponder];
    }
    else {
        [self backgroundTouched:sender];
    }
}

- (IBAction)buttonVerifyOnClick:(id)sender {
//    PRAppDelegate *appDelegate = (PRAppDelegate*)[UIApplication sharedApplication].delegate;
    SettingsEntity *settings = [SettingsEntity fetchSettingsObjectWithManagedObjectContext:[[PRDBManager object]managedObjectContext]];
    
    settings.userName = _textName.text;
    settings.userSurname = _textSurname.text;
    settings.userPhoneNo = _textPhoneNo.text;
    settings.userPassword = [PRUtils generateRandomString:CONFIG_PASSWORD_LENGTH];
    
    NSError *error;
    
    if([[[PRDBManager object]managedObjectContext] save:&error] == NO)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        return;
    }
    
    PRAsyncConnection *connection = [[PRAsyncConnection alloc] initWithParent:self successSelector:@selector(connectionSucces:) didFailSelector:@selector(connectionFailed:)];
    [connection sendRegisterRequestWithPhoneNo:_textPhoneNo.text andPassword:settings.userPassword andName:settings.userName andSurname:settings.userSurname];
    
    [_buttonVerify setLockedForResponse:YES];
}

- (IBAction)backgroundTouched:(id)sender {
    [self saveNameSurnamePhone];
    
    [_textName resignFirstResponder];
    [_textPhoneNo resignFirstResponder];
    [_textSurname resignFirstResponder];

    [UIView animateWithDuration:0.2 animations:^{
            CGRect frame = self.view.frame;
            frame.origin.y = 64;
            self.view.frame = frame;
        }];
    
    
    [self enableOrDisableVerifyButton];
}

- (IBAction)regulationSwitchValueChanged:(id)sender {
    [self enableOrDisableVerifyButton];
}

#pragma mark - connection
- (void) connectionFailed:(PRAsyncConnection*) connection
{
    [_buttonVerify setLockedForResponse:NO];
    [PRUtils displayStandardAlertViewWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertConnectionFailed", nil) andConfirmButtonTitle:NSLocalizedString(@"alertOk", nil)];
}

- (void) connectionSucces:(PRAsyncConnection*) connection
{
    [_buttonVerify setLockedForResponse:NO];
    
    XLogTag(LogTag, @"%@", [[NSString alloc] initWithData:connection.responseData encoding:NSUTF8StringEncoding]);
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:connection.responseData                          
                          options:kNilOptions
                          error:&error];
    
    if(error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        return;
    }
    
    NSDictionary *register_response = [json objectForKey:@"register_response"];
    
    NSNumber *status = [register_response objectForKey:@"status"];
    
    if(status == nil)
    {
        [PRUtils displayStatusErrorDialog];
        return;
    }
    
    _cert_token = nil;
    
    switch (status.intValue) {
        case STATUS_OK:
        {
            _cert_token = [register_response objectForKey:@"cert_token"];
        }
            break;
        case STATUS_INVALID_PASSWORD:
        {
            //should not happen since password is generated randomly
        }
            break;
        case STATUS_USER_BLOCKED:
        {
            [PRUtils displayStandardAlertViewWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertUserBlocked", nil) andConfirmButtonTitle:NSLocalizedString(@"alertOk", nil)];
        }
            break;
        case STATUS_WRON_PHONE_NO:
        {
            [PRUtils displayStandardAlertViewWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertWrongPhoneNo", nil) andConfirmButtonTitle:NSLocalizedString(@"alertOk", nil)];
        }
            break;
    }
    
    if(_cert_token!=nil)
    {
        [self performSegueWithIdentifier:@"verifySms" sender:nil];
    }
    
}

- (void)viewDidUnload {
    [self setTextSurname:nil];
    [self setRegulationsSwitch:nil];
    [super viewDidUnload];
}
@end
