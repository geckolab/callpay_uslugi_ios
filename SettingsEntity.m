//
//  SettingsEntity.m
//  CallPay
//
//  Created by Jacek on 15.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "SettingsEntity.h"


@implementation SettingsEntity

@dynamic lastCheckDate;
@dynamic lastUpdateDate;
@dynamic tableDiscountsVersion;
@dynamic tableIdTypesVersion;
@dynamic tableStationsVersion;
@dynamic tableTrainAttributesVersion;
@dynamic userAcceptedCardsWarning;
@dynamic userAcceptedIdWarning;
@dynamic userAcceptedTermsOfVerification;
@dynamic userCertFileName;
@dynamic userHasRegioCard;
@dynamic userIdNo;
@dynamic userIdType;
@dynamic userName;
@dynamic userPassword;
@dynamic userPhoneNo;
@dynamic userSurname;
@dynamic userVerified;

@end
