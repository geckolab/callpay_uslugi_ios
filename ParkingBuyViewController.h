//
//  ParkingBuyViewController.h
//  CallPay
//
//  Created by Jacek on 10.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "ProjectViewController.h"
#import "Ticket+Extensions.h"
#import "ParkingCatalogsManager.h"

@interface ParkingBuyViewController : ProjectViewController
{
    BOOL isFiltered;
    UISearchBar *searchBar;
    UITableView *tableView;
}

@property (nonatomic, retain) NSDictionary *dataToProcess;
@property (nonatomic, retain) NSArray *allDataKeys;
@property (nonatomic, retain) NSMutableArray *filteredDataKeys;
@property (nonatomic, retain) Ticket *ticket;
@property (nonatomic, strong) ParkingCatalogsManager *catalogsManager;

- (NSArray *) dataKeysToShow;

@end
