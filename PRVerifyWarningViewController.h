//
//  PRVerifyWarningViewController.h
//  pr
//
//  Created by Marcin Szulc on 04.10.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol PRVerifyWarningDelegate <NSObject>

- (void) buttonOkClickedInViewController:(UIViewController*) controller;

@end

@interface PRVerifyWarningViewController : UIViewController

@property (atomic, assign) int amount;
@property (weak, nonatomic) id <PRVerifyWarningDelegate> delegate;

@end
