//
//  LocalizationView.h
//  CallPay
//
//  Created by Jacek on 26.03.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomSwitch.h"

@interface LocalizationView : UIView
{
    UILabel *titleLabel;
    UILabel *descriptionLabel;
}

@property(nonatomic, strong) CustomSwitch *btnSwith;

-(void)setup;

@end
