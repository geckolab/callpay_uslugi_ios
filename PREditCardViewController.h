//
//  PREditCardViewController.h
//  pr
//
//  Created by Alexander Pimenov on 24.06.13.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PRCardObject;
@class PREditCardViewController;


@protocol PREditCardViewControllerDelegate <NSObject>

- (void)editCardViewControllerDeletedCard:(PREditCardViewController *)editCardViewController;

@end


@interface PREditCardViewController : UIViewController

@property (weak, nonatomic) id <PREditCardViewControllerDelegate> delegate;

@property (nonatomic) PRCardObject *card;

@end
