//
//  ParkingBuyViewController.m
//  CallPay
//
//  Created by Jacek on 10.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "ParkingBuyViewController.h"

#import "BuyTicketCell.h"

#import "ParkingBuyTicketViewController.h"

#import "ParkingTicket.h"

#import "ProjectLocations.h"
#import "CarsManager.h"

@interface ParkingBuyViewController ()

@end

@implementation ParkingBuyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.catalogsManager = [[ParkingCatalogsManager alloc] initWithDelegate:self];
//    self.ticket = newTicket;
    self.dataToProcess = [self.catalogsManager parseJsonForCurrentCity];NSLog(@"dataToProcess %@",self.dataToProcess);
    NSArray *a1 = [[NSArray alloc] initWithArray:[[self.dataToProcess allKeys] sortedArrayUsingSelector:@selector(localizedStandardCompare:)]];
    NSDictionary *d1 = [self.dataToProcess objectForKey: [[self.dataToProcess allKeys] lastObject]];
//    NSLog(@"dataToProcess 2 %@",[self.dataToProcess objectForKey: [[self.dataToProcess allKeys] lastObject]]);
    NSLog(@"dataToProcess 3 %@", [d1 objectForKey: [[d1 allKeys] lastObject]]);
    NSDictionary *d2 = [d1 objectForKey: [[d1 allKeys] lastObject]];
    NSArray *a2 = [[NSArray alloc] initWithArray:[[d2 allKeys] sortedArrayUsingSelector:@selector(localizedStandardCompare:)]];
    self.dataToProcess = d2;
    self.allDataKeys = [[NSArray alloc] initWithArray:[[d2 allKeys] sortedArrayUsingSelector:@selector(localizedStandardCompare:)]];
    
    NSLog(@"a2 %@",a2);
//    self.dataToProcess = [self.dataToProcess objectForKey: [[self.dataToProcess allKeys] lastObject]];
//    self.allDataKeys = [[self.dataToProcess allKeys] sortedArrayUsingSelector:@selector(localizedStandardCompare:)];
//    self.allDataKeys = [[[self.dataToProcess allKeys] allKeys] sortedArrayUsingSelector:@selector(localizedStandardCompare:)];
    self.filteredDataKeys = [[NSMutableArray alloc] init];
    isFiltered = NO;
    
//    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 55, [[UIScreen mainScreen] applicationFrame].size.width, 40)];
//    [self.searchBar setBackgroundImage:[UIImage new]];
//    [self.searchBar setTranslucent:YES];
//    [self.searchBar setDelegate:self];
//    [self.view addSubview:self.searchBar];
    float endSearchBarPosY = 20;//self.searchBar.frame.origin.y + self.searchBar.frame.size.height;
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(16, endSearchBarPosY, self.view.bounds.size.width - 32, [self getWindowContentHeight] - endSearchBarPosY - 20)];
    [self.view addSubview:self.tableView];
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"BuyTicketCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TicketCell"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger) getRowsCount {
    return [self.allDataKeys count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self getRowsCount];
}


- (UITableViewCell *)tableView:(UITableView *)table cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"TicketCell";
    //    NSString *cellIdentifier = [NSString stringWithFormat:@"TicketCell", indexPath.row];
    BuyTicketCell *cell = [table dequeueReusableCellWithIdentifier:cellIdentifier];
    
    //    if (cell == nil || IS_IOS7_OR_LATER)
    if (cell == nil)
    {
        cell = [[BuyTicketCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    //    else {
    //        if ([cell subviews]){
    //            for (UIView *subview in [cell subviews]) {
    //                [subview removeFromSuperview];
    //            }
    //        }
    //    }
    
    //  static NSString *CellIdentifier = @"Cell";
    //
    //  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //
    //  if (cell == nil) {
    //    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    //  }
    
    cell.selectedBackgroundView.backgroundColor=[UIColor clearColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    //    cell.textLabel.text = [[self dataKeysToShow] objectAtIndex:indexPath.row];
    NSDictionary *dict = (NSDictionary*)[self.dataToProcess valueForKey:[[self dataKeysToShow] objectAtIndex:indexPath.row]];
    //    cell.detailTextLabel.text = [dict valueForKey:@"price"];
    //        cell.textLabel.text = [cell.textLabel.text stringByAppendingString: [NSString stringWithFormat:@" %@",[dict valueForKey:@"price"]]];
    cell.nameLabel.text = [[self dataKeysToShow] objectAtIndex:indexPath.row];
    //    [cell.nameLabel setTextColor:[UIColor redColor]];
    //    cell.priceLabel.text = [dict valueForKey:@"price"];
    [cell setPrice:[dict valueForKey:@"price"]];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tabView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 78;
}
- (CGFloat)tableView:(UITableView *)tabView heightForHeaderInSection:(NSInteger)section
{
    return 2;
}
- (UIView *)tableView:(UITableView *)tabView viewForHeaderInSection:(NSInteger)section
{
    UIView *hv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tabView.frame.size.width, 2)];
    [hv setBackgroundColor:[UIColor clearColor]];
    return hv;
}
- (NSArray *) dataKeysToShow {
    return isFiltered ? self.filteredDataKeys : self.allDataKeys;
}
- (NSIndexPath *)tableView:(UITableView *)tabView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = (NSDictionary*)[self.dataToProcess valueForKey:[[self dataKeysToShow] objectAtIndex:indexPath.row]];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
//    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ParkingBuyTicketVC"];
    ParkingBuyTicketViewController *vc = (ParkingBuyTicketViewController*)[storyboard instantiateViewControllerWithIdentifier:@"ParkingBuyTicketVC"];
    
    ParkingTicket *newTicket = (ParkingTicket *) [NSEntityDescription insertNewObjectForEntityForName: @"ParkingTicket" inManagedObjectContext:[[CarsManager object] managedObjectContext]];
    [newTicket setStatus: @(0)];
    [newTicket setDuration:[NSNumber numberWithInt:[[dict valueForKey:@"duration"] intValue]]];
    [newTicket setCityIndex: [ProjectLocations object].currentParkingCity];
    [newTicket setCar:[[CarsManager object] findWithPlateNumber:[[CarsManager object] getDefault]]];
    [newTicket setProductCode:[dict valueForKey:@"product_code"]];//[NSString stringWithFormat:@"%@%@",[dict valueForKey:@"product_code"],newTicket.car.plateNumber]];
    [newTicket setPrice:[NSNumber numberWithInt:[[dict valueForKey:@"price"] intValue]]];
    [newTicket setName:[[self dataKeysToShow] objectAtIndex:indexPath.row]];
    
    [vc setupWithTicket:newTicket];
    
    [self.navigationController pushViewController:vc animated:YES];
    
    return indexPath;
}

@end
