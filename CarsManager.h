//
//  CarsManager.h
//  CallPay
//
//  Created by Jacek on 14.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Car.h"

@interface CarsManager : NSObject
{
    id carListVCDelegate;
}
#define CURRENT_CAR @"CURRENT_CAR"

@property (nonatomic, strong) NSString *currentCar;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContextPrivateQueue;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic, strong) NSMutableArray *carsList;

+ (CarsManager *) object;

-(void)setCarListVCDelegate:(id)delegate;

-(void)addCar:(NSString*)plateNumber;

-(Car *)findWithPlateNumber:(NSString*)plateNumer;

-(void)removeCarWithEntity:(Car*)entity;

-(void)reloadData;

- (void)saveContext;

-(int)countAll;

-(NSString*)getDefault;

@end
