//
//  PROrderObject.m
//  pr
//
//  Created by Marcin Szulc on 21.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PROrderObject.h"
#import "PRRelationObject.h"

@implementation PROrderObject

- (id) init
{
    self = [super init];
    
    if(self)
    {}
    
    return self;
}

- (NSNumber*) vehicleClass
{
    if(_relationFrom.relationSelectedClass.intValue == 1 || _relationBack.relationSelectedClass.intValue == 1)
    {
        return [NSNumber numberWithInt:1];
    }
    
    return [NSNumber numberWithInt:2];
}

@end
