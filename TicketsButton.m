//
//  TicketsButton.m
//  CallPay
//
//  Created by Jacek on 25.03.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "TicketsButton.h"

@implementation TicketsButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setup];
    }
    return self;
}
- (id)init
{
    self = [super init];
    if (self)
    {
        [self setFrame:CGRectMake(0, 0, 60, 60)];
        [self setup];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self setup];
    }
    return self;
}
-(void)setup
{
    [self setBackgroundImage:[UIImage new] forState:UIControlStateNormal];
    
//    CALayer *bgLayer = self.layer;
//    bgLayer.borderWidth = 2.0;
//    bgLayer.borderColor = [UIColor whiteColor].CGColor;
//    bgLayer.backgroundColor = [UIColor blackColor].CGColor;
//    bgLayer.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.48].CGColor;
//    bgLayer.opacity = 0.48;
//    [self.layer setDelegate:self];
//    CALayer *topStripe = [CALayer layer];
//    topStripe.frame = CGRectMake(10, 10, bgLayer.frame.size.width, 2);
//    topStripe.backgroundColor = [UIColor whiteColor].CGColor;
//    [bgLayer addSublayer:topStripe];
//    [self.layer addSublayer:topStripe];
//    [self.layer setNeedsDisplay];
    [[self titleLabel] setFont:[UIFont fontWithName:@"Swis721LtEU" size:self.titleLabel.font.pointSize]];
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    //    return CGRectMake((self.frame.size.width - 96) / 2, 91, 96, 50);
    return CGRectMake(85, 0, self.frame.size.width - 85, self.frame.size.height);
}

- (CGRect)imageRectForContentRect:(CGRect)contentRect
{//85
    return CGRectMake((85 - self.currentImage.size.width) / 2 , (self.frame.size.height - self.currentImage.size.height) / 2, self.currentImage.size.width, self.currentImage.size.height);
}
- (void) drawLayer:(CALayer*) layer inContext:(CGContextRef) ctx
{
    NSLog(@"draw layer");
        if ([self isHighlighted] || [self isSelected])
        {
            [layer setBackgroundColor:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.48].CGColor];
        }
        else
        {
            [layer setBackgroundColor:[UIColor clearColor].CGColor];
        }
    
    CGContextSetStrokeColorWithColor(ctx, [UIColor whiteColor].CGColor);
    CGContextSetLineWidth(ctx, 2);
    
    CGContextMoveToPoint(ctx, 0, 0);
    CGContextAddLineToPoint(ctx, layer.frame.size.width, 0);
    
    CGContextStrokePath(ctx);
    
    CGContextSetLineWidth(ctx, 2);
    
    CGContextMoveToPoint(ctx, 0, layer.frame.size.height);
    CGContextAddLineToPoint(ctx, layer.frame.size.width, layer.frame.size.height);
    
    CGContextStrokePath(ctx);
    

}
-(void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
//    [self.layer setNeedsDisplay];
//    if(highlighted)
//    {
//        [self.layer setBackgroundColor:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.48].CGColor];
//    }
//    else
//    {
//        [self.layer setBackgroundColor:[UIColor clearColor].CGColor];
//    }
    [self.layer setNeedsDisplay];
}
-(void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
//    [self.layer setNeedsDisplay];
//    if(selected)
//    {
//        [self.layer setBackgroundColor:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.48].CGColor];
//    }
//    else
//    {
//        [self.layer setBackgroundColor:[UIColor clearColor].CGColor];
//    }
    [self.layer setNeedsDisplay];
}
-(void)setNeedsDisplay
{
    [super setNeedsDisplay];
    [self.layer setNeedsDisplay];
}
@end
