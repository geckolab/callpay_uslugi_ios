//
//  PRUpdateViewController.h
//  pr
//
//  Created by Marcin Szulc on 18.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PRUpdateViewController;


@protocol PRUpdateViewControllerDelegate <NSObject>

- (void)updateViewControllerShouldBeDismissed:(PRUpdateViewController *)updateViewController;

@end


@interface PRUpdateViewController : ProjectViewController

@property (weak, nonatomic) id <PRUpdateViewControllerDelegate> delegate;

@end
