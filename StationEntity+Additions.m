//
//  StationEntity+Additions.m
//  pr
//
//  Created by Marcin Szulc on 18.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "StationEntity+Additions.h"

@implementation StationEntity (Additions)

+ (StationEntity*) insertStationWithId:(NSNumber*) index andName:(NSString*) name withManagedObjectContext:(NSManagedObjectContext*) managedObjectContext
{
    StationEntity *station = [NSEntityDescription insertNewObjectForEntityForName:@"StationEntity" inManagedObjectContext:managedObjectContext];
    
    station.stationId = index;
    station.stationName = name;
    
    return station;
}

+ (void) clearAllRecordsWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext
{
    NSArray *stations = [self fetchAllStationsWithManagedObjectContext:managedObjectContext];
    
    for(StationEntity *station in stations)
    {
        [managedObjectContext deleteObject:station];
    }
    
}

+ (NSArray*) fetchAllStationsWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"StationEntity"];
    [request setSortDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"stationName" ascending:YES]]];
    return [managedObjectContext executeFetchRequest:request error:nil];
}

+ (NSArray*) fetchAllStationsWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext whereStationIdWitihinIndexes:(NSMutableArray*) indexes
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"StationEntity"];
    [request setPredicate:[NSPredicate predicateWithFormat:@"stationId in %@", indexes]];
    [request setSortDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"stationName" ascending:YES]]];
    return [managedObjectContext executeFetchRequest:request error:nil];
}

+ (NSArray*) fetchAllStationsWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext whereNameBeginsWithString:(NSString*) name
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"StationEntity"];
    [request setPredicate:[NSPredicate predicateWithFormat:@"stationName beginswith[cd] %@", name]];
    [request setSortDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"stationName" ascending:YES]]];
    return [managedObjectContext executeFetchRequest:request error:nil];
}

+ (NSArray*) fetchAllStationsWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext whereNameBeginsWithString:(NSString*) name andStationIdWitihinIndexes:(NSMutableArray*) indexes
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"StationEntity"];
    [request setSortDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"stationName" ascending:YES]]];
    [request setPredicate:[NSPredicate predicateWithFormat:@"stationName beginswith[cd] %@ AND stationId in %@", name, indexes]];
    return [managedObjectContext executeFetchRequest:request error:nil];
}

+ (StationEntity*) fetchStationAtIndex:(int) index withManagedObjectContext:(NSManagedObjectContext*) managedObjectContext
{
    return [[self fetchAllStationsWithManagedObjectContext:managedObjectContext] objectAtIndex:index];
}

@end
