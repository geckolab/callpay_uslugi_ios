//
//  CPMPMTextField.m
//  CallPay
//
//  Created by Jacek on 09.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "CPMPMTextField.h"

@interface CPMPMTextField ()
@property (nonatomic) NSString *hintMessage;
@property (nonatomic) UIButton *hintButton;
@property (nonatomic) id hintTarget;
@property (nonatomic) SEL hintSelector;
@end

@implementation CPMPMTextField

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {

//        self.backgroundColor = [UpaidMPM paymentInstance].uiConfig.textFieldBackgroundColor;
        self.background = [[UIImage imageNamed:@"new_input_field"] resizableImageWithCapInsets:UIEdgeInsetsMake(8, 8, 8, 8)];
//        self.borderStyle = UITextBorderStyleNone;
//        self.edgeInsets = [UpaidMPM paymentInstance].uiConfig.textFieldEdgeInsets;
        self.textColor = [UIColor whiteColor];
        self.edgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    }
    
    return self;
}

- (void) setupHintButton {
    self.hintButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [self.hintButton setImage:[UIImage imageNamed:@"new_mpm_hint"] forState:UIControlStateNormal];
//    [self.hintButton setImage:[UIImage imageNamed:@"mpm_hint_pressed"] forState:UIControlStateHighlighted];
    [self.hintButton addTarget:self.hintTarget action:self.hintSelector forControlEvents:UIControlEventTouchUpInside];
    UIView *container = [[UIView alloc] initWithFrame: CGRectMake(0, 0, self.hintButton.frame.size.width + self.edgeInsets.right, self.hintButton.frame.size.height)];
    [container addSubview: self.hintButton];
    self.rightView = container;
    self.rightViewMode = UITextFieldViewModeAlways;
}
- (void) setHintMessage: (NSString *) message {
    _hintMessage = message;
    
    [self setupHintButton];
}

- (void) setHintTarget: (id) target andSelector: (SEL) selector {
    self.hintTarget = target;
    self.hintSelector = selector;
    
    [self setupHintButton];
}

@end
//new_input_field