//
//  PRPassengerDetailsViewController.h
//  pr
//
//  Created by Marcin Szulc on 23.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PRStandardButton;
@class PROrderObject;
@class PRCardObject;

@interface PRPassengerDetailsViewController : ProjectViewController <UIActionSheetDelegate>

@property (strong, nonatomic) PROrderObject *orderObject;
@property (strong, nonatomic) PRCardObject *defaultCard;

@property (strong, nonatomic) IBOutlet UITextField *textName;
@property (strong, nonatomic) IBOutlet UITextField *textSurname;
@property (strong, nonatomic) IBOutlet UISwitch *switchRegioCard;



@property (strong, nonatomic) IBOutlet PRStandardButton *buttonIdType;
@property (strong, nonatomic) IBOutlet UITextField *textIdNo;
@property (strong, nonatomic) IBOutlet UILabel *labelDiscount;
@property (strong, nonatomic) IBOutlet UILabel *labelPrice;
@property (strong, nonatomic) IBOutlet PRStandardButton *buttonBuyTicket;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicatorPrice;

- (IBAction)buttonIdTypeOnClick:(id)sender;
- (IBAction)controlDiscountOnClick:(id)sender;
- (IBAction)buttonBuyTicketOnClick:(id)sender;

- (IBAction)backgroundOnClick:(id)sender;

- (IBAction)switchRegioCardValueChanged:(id)sender;

@end
