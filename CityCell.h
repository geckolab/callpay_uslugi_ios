//
//  CityCell.h
//  CallPay
//
//  Created by Jacek on 02.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RectangleWhiteBarsView.h"
#import "CityActiveStatusView.h"
#import "CellConfirmButton.h"

@interface CityCell : UITableViewCell
{
    //RectangleWhiteBarsView *barsView;
}

@property (nonatomic, weak) IBOutlet UILabel *cityNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *activeLabel;
@property (nonatomic, strong) IBOutlet CityActiveStatusView *cityStatus;
@property (nonatomic, weak) IBOutlet CellConfirmButton *downloadButton;

@property (nonatomic, strong) RectangleWhiteBarsView *barsView;

@end
