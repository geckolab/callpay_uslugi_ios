//
//  HitoryTicketCell.h
//  CallPay
//
//  Created by Jacek on 02.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CellDeleteButton.h"

#import "RectangleWhiteBarsView.h"

@interface HitoryTicketCell : UITableViewCell
{
    RectangleWhiteBarsView *barsView;
}

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *buyDateLabel;
@property (nonatomic, weak) IBOutlet CellDeleteButton *deleteButton;

@end
