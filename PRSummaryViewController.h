//
//  PRSummaryViewController.h
//  pr
//
//  Created by Marcin Szulc on 24.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRSummaryViewController : UIViewController

- (IBAction)buttonSearchAgainOnClick:(id)sender;
- (IBAction)buttonYourTicketOnClick:(id)sender;

@end
