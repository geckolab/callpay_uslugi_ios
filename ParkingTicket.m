//
//  ParkingTicket.m
//  CallPay
//
//  Created by Jacek on 10.06.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "ParkingTicket.h"
#import "Car.h"


@implementation ParkingTicket

@dynamic cityIndex;
@dynamic name;
@dynamic price;
@dynamic productCode;
@dynamic productName;
@dynamic purchaseDate;
@dynamic securityCodeType;
@dynamic securityCodeValueLength;
@dynamic securityCodeValueType;
@dynamic seller;
@dynamic sessionToken;
@dynamic status;
@dynamic transactionId;
@dynamic verificationDate;
@dynamic verificationPhone;
@dynamic verificationTime;
@dynamic duration;
@dynamic car;

@end
