//
//  ParkingCatalogsManager.h
//  CallPay
//
//  Created by Jacek on 13.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ASIHTTPRequest.h"
#import "CatalogsManagerDelegate.h"

@interface ParkingCatalogsManager : NSObject
{
    NSString *_cityIndex, *_currentCity;
    id <CatalogsManagerDelegate> _delegate;
}

@property (nonatomic, retain) NSMutableArray *catalogsToUpdate;

- (id) initWithDelegate: (id) delegate;
- (void) startDownload: (NSString *) cityIndex;
- (NSDictionary *) dictionaryForCityIndex: (NSString *) cityIndex;
- (void) deleteCatalogForCity: (NSString *) cityIndex;
- (BOOL) checkCatalogsCache;
- (NSString *) sizeToUpdate;
- (NSString *) nameForCurrentCity;
- (void) deleteCatalogsCache;
- (void) updateCatalogsCache;
- (void) updateCurrentCityCache;

- (NSDictionary *) parseJsonForCurrentCity ;

@end
