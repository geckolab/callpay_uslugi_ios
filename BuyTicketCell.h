//
//  BuyTicketCell.h
//  CallPay
//
//  Created by Jacek on 30.03.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RectangleWhiteBarsView.h"

@interface BuyTicketCell : UITableViewCell
{
    RectangleWhiteBarsView *barsView;
}

@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet UILabel *priceLabel;

-(void)setPrice:(NSString*)price;

@end
