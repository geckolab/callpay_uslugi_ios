//
//  PRStationsViewController.m
//  pr
//
//  Created by Marcin Szulc on 16.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PRStationsViewController.h"
//#import "PRAppDelegate.h"
#import "StationEntity+Additions.h"
#import "PRStationCell.h"
#import "PROrderObject.h"
#import "PRAsyncConnection.h"
#import "XLog.h"
#import "PRAlertView.h"
#import "PRUtils.h"

#import "PRDBManager.h"

static NSString * const LogTag = @"StationsViewController";

@interface PRStationsViewController ()

@property (strong, nonatomic) NSArray *cells;
@property (strong, nonatomic) PRAlertView *alert;

@property (strong, nonatomic) NSMutableArray *stationsLoadedIndexes;

@end

@implementation PRStationsViewController

@synthesize titleLabel = _titleLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIBarButtonItem * mcmLogo = [[UIBarButtonItem alloc] initWithCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mcm_icon"]]];
    self.navigationItem.rightBarButtonItem = mcmLogo;
    _stationsLoadedIndexes = [[NSMutableArray alloc] init];
    
    
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    [self hideKeyboard];
}

- (void) hideKeyboard
{
    [_textStation resignFirstResponder];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self hideKeyboard];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(_orderObject.stationFromToFill == NO)
    {
        _textStation.enabled = NO;
        _progressIndicator.hidden = NO;
        
        PRAsyncConnection *connection = [[PRAsyncConnection alloc] initWithParent:self successSelector:@selector(connectionSuccess:) didFailSelector:@selector(connectionFailed:)];
        [connection sendGetStationsToRequestWithDate:[PRUtils getFormattedDateFromDateForConnection:_orderObject.orderDateFrom] andStationFromIndex:_orderObject.stationFrom.stationId.intValue];
        
    }
    else
    {
        _progressIndicator.hidden = YES;
        [self fillTable];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
- (void) fillTable
{
    _textStation.enabled = YES;
    _progressIndicator.hidden = YES;
    
    NSString *searchedName = _textStation.text;
    
//    PRAppDelegate *appDelegate = (PRAppDelegate*)[UIApplication sharedApplication].delegate;
    
    if(_orderObject.stationFromToFill == YES)
    {
        if(searchedName.length == 0)
        {
            _cells = [StationEntity fetchAllStationsWithManagedObjectContext:[[PRDBManager object]managedObjectContext]];
        }
        else
        {
            _cells = [StationEntity fetchAllStationsWithManagedObjectContext:[[PRDBManager object]managedObjectContext] whereNameBeginsWithString:searchedName];
        }
    }
    else
    {
        if(searchedName.length == 0)
        {
            _cells = [StationEntity fetchAllStationsWithManagedObjectContext:[[PRDBManager object]managedObjectContext] whereStationIdWitihinIndexes:_stationsLoadedIndexes];
        }
        else
        {
            _cells = [StationEntity fetchAllStationsWithManagedObjectContext:[[PRDBManager object]managedObjectContext] whereNameBeginsWithString:searchedName andStationIdWitihinIndexes:_stationsLoadedIndexes];
        }
    }
    
    [_stationsTable reloadData];
}

#pragma mark - table delegate and datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _cells.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *cellIdentifier = [NSString stringWithFormat:@"station%d", indexPath.row];
    
    PRStationCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PRStationCell"
                                                     owner:self options:nil];
        for (id oneObject in nib)
        {
            if ([oneObject isKindOfClass:[PRStationCell class]])
            {
                cell = (PRStationCell*)oneObject;
            }
        }
    }
    
    cell.textStationName.text = ((StationEntity*)[_cells objectAtIndex:indexPath.row]).stationName;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return PR_STATION_TABLE_CELL_HEIGHT;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_orderObject.stationFromToFill == YES)
    {
        _orderObject.stationFrom = ((StationEntity*)[_cells objectAtIndex:indexPath.row]);
        _orderObject.stationTo = nil;
    }
    else
    {
        _orderObject.stationTo = ((StationEntity*)[_cells objectAtIndex:indexPath.row]);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)searchTextChanged:(id)sender {
    [self fillTable];
}

- (IBAction)textDidEndOnExit:(id)sender {
    [_textStation resignFirstResponder];
}

#pragma mark - alert
- (void) connectionFailed
{
    _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertStatusError", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
    [_alert show];
}

- (void) connectionFailedAlertCallback:(NSNumber*) buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - stationTo connection


- (void) connectionSuccess:(PRAsyncConnection*) connection
{
    XLogTag(LogTag, @"%@", [[NSString alloc] initWithData:connection.responseData encoding:NSUTF8StringEncoding]);
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:connection.responseData
                          options:kNilOptions
                          error:&error];
    
    if(error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        [self connectionFailed];
        return;
    }
    
    NSDictionary *response = [json objectForKey:@"stations_response"];
    
    NSNumber *status = [response objectForKey:@"status"];
    
    if(status==nil)
    {
        [self connectionFailed];
        return;
    }
    
    switch (status.intValue) {
        case STATUS_OK:
        {
            [_stationsLoadedIndexes removeAllObjects];
            
            NSArray *stations = [response objectForKey:@"stations"];
            
            for(NSDictionary *station in stations)
            {
                if([station isEqual:@""])
                {
                    XLogTag(LogTag, @"empty list parameter");
                    continue;
                }
                
                NSNumber *stationId = [[station objectForKey:@"station"] objectForKey:@"id"];
                [_stationsLoadedIndexes addObject:stationId];
            }
            
        }
            break;
        case STATUS_USER_BLOCKED:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertUserBlocked", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
        case STATUS_NO_STATIONS:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertNoStation", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
        case STATUS_WRONG_DATE:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertWrongDate", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;

    }
    
    [self fillTable];
}

- (void) connectionFailed:(PRAsyncConnection*) connection
{
    [self connectionFailed];
}

- (void)viewDidUnload {
    [self setProgressIndicator:nil];
    [super viewDidUnload];
}


@end
