//
//  PRRelationDetailsViewController.m
//  pr
//
//  Created by Marcin Szulc on 22.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PRRelationDetailsViewController.h"
#import "PRUtils.h"
#import "PROrderObject.h"
#import "PRRelationObject.h"
#import "PRTrainAttributeCell.h"
#import "PRRelationObject.h"
#import "PRRelationBackDateViewController.h"
#import "PRStandardControl.h"
#import "PRStandardButton.h"
#import "Config.h"
#import "PRRelationsViewController.h"
#import "PRPassengerDetailsViewController.h"
#import "PRCardsProvider.h"
#import "PRCardObject.h"
#import "PRCardsContainer.h"
#import "PRAsyncConnection.h"
#import "PRAlertView.h"
#import "PRVerifyWarningViewController.h"
#import "XLog.h"
//#import "PRAppDelegate.h"
#import "SettingsEntity+Additions.h"

#import "PRDBManager.h"

static NSString * const LogTag = @"PRRelationDetailsViewController";

@interface PRRelationDetailsViewController () <PRCardsProviderDelegate, PRVerifyWarningDelegate>

@property (strong, nonatomic) IBOutlet PRStandardButton *buttonBuyTicket;
@property (nonatomic) PRCardsProvider *cardsProvider;
@property (strong, nonatomic) PRAlertView *alert;
@property (strong, nonatomic) PRCardObject *defaultCard;
@property (strong, nonatomic) NSNumber *amount;

@end

@implementation PRRelationDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIBarButtonItem * mcmLogo = [[UIBarButtonItem alloc] initWithCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mcm_icon"]]];
    self.navigationItem.rightBarButtonItem = mcmLogo;
    
    _cardsProvider = [[PRCardsProvider alloc] init];
    [_cardsProvider setDelegate:self];
    
    _viewRelationBack.view.frame = _controlRelationBackDetails.frame;
    
    [_viewRelationBack.buttonSearch addTarget:self action:@selector(buttonSearchBackRelationOnClick) forControlEvents:UIControlEventTouchUpInside];
    [_viewRelationBack.controlDate addTarget:self action:@selector(controlBackDateOnClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_viewRelationBack.view];
    
    _datePicker.delegate = self;
    [PRUtils setView:_datePicker.view atBottomOfView:self.view andAddAsSubview:YES];
    
    CGRect rect = _datePicker.view.frame;
    rect.size.width = self.view.bounds.size.width;
    _datePicker.view.frame = rect;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTableTrainAttributes:nil];
    [self setViewRelationBack:nil];
    [self setControlRelationBackDetails:nil];
    [self setDatePicker:nil];
    [self setTableBackTrainAttributes:nil];
    [self setLabelSelectedClass:nil];
    [self setLabelBackSelectedClass:nil];
    [self setButtonDeleteBackRelation:nil];
    [super viewDidUnload];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _labelCategory.text = [PRUtils getRelationCategoryNameForKey:_orderObject.relationFrom.relationCategory];
    _labelDepartureTime.text = _orderObject.relationFrom.relationDepartureTime;
    _labelArrivalTime.text = _orderObject.relationFrom.relationArrivalTime;
    _labelTravelDate.text = [PRUtils getFormattedDateFromDate:_orderObject.orderDateFrom];
    _labelSelectedClass.text = [NSString stringWithFormat:NSLocalizedString(@"klasa", nil), _orderObject.relationFrom.relationSelectedClass.intValue];
    
    _labelBackCategory.text = [PRUtils getRelationCategoryNameForKey:_orderObject.relationBack.relationCategory];
    _labelBackDepartureTime.text = _orderObject.relationBack.relationDepartureTime;
    _labelBackArrivalTime.text = _orderObject.relationBack.relationArrivalTime;
    _labelBackTravelDate.text = [PRUtils getFormattedDateFromDate:_orderObject.orderDateBack];
    _labelBackSelectedClass.text = [NSString stringWithFormat:NSLocalizedString(@"klasa", nil), _orderObject.relationBack.relationSelectedClass.intValue];
    
    
    _datePicker.datePicker.minimumDate = _orderObject.orderDateFrom;
    _datePicker.datePicker.maximumDate = [PRUtils addDays:CONFIG_MAX_DAYS_FORWARD toDate:[NSDate date]];
    
    _orderObject.orderDateBack = _orderObject.relationBack == nil ? _orderObject.orderDateFrom : _orderObject.orderDateBack;
    
    _viewRelationBack.labelBackDate.text = [PRUtils getFormattedDateFromDate:_orderObject.orderDateBack];
    
    [self handleRelationBackViews];
    
    [_tableBackTrainAttributes reloadData];
    [_tableTrainAttributes reloadData];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"buyTicket"])
    {
        PRPassengerDetailsViewController *destController = segue.destinationViewController;
        [destController setDefaultCard:_defaultCard];
        [destController setOrderObject:_orderObject];
    }
    else if([segue.identifier isEqualToString:@"showBackRelations"])
    {
        PRRelationsViewController *destController = segue.destinationViewController;
        [destController setOrderObject:_orderObject];
    }
    else if ([segue.identifier isEqualToString:@"showVerifyWarning"])
    {
        PRVerifyWarningViewController *destController = segue.destinationViewController;
        [destController setAmount:_amount.intValue];
        [destController setDelegate:self];
    }
}

#pragma mark - verify view controller delegate
- (void) buttonOkClickedInViewController:(UIViewController *)controller
{
//    PRAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    SettingsEntity *settings = [SettingsEntity fetchSettingsObjectWithManagedObjectContext:[[PRDBManager object]managedObjectContext]];
    settings.userAcceptedTermsOfVerification = [NSNumber numberWithBool:YES];
    NSError *error;
    if([[[PRDBManager object]managedObjectContext] save:&error] == NO)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        return;
    }
    
    NSMutableArray *controllers = [[NSMutableArray alloc] initWithArray:self.navigationController.viewControllers];
    [controllers removeObject:controller];
    
    self.navigationController.viewControllers = controllers;
    
    [self performSegueWithIdentifier:@"buyTicket" sender:self];
}

#pragma mark - connection getTotalTransactionsValues selectors
- (void) connectionTotalValuesSuccess:(PRAsyncConnection*) connection
{
    [_buttonBuyTicket setLockedForResponse:NO];
    
    XLogTag(LogTag, @"%@", [[NSString alloc] initWithData:connection.responseData encoding:NSUTF8StringEncoding]);
    
    [_buttonBuyTicket setLockedForResponse:NO];
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:connection.responseData
                          options:kNilOptions
                          error:&error];
    
    if(error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        [PRUtils displayStatusErrorDialog];
        return;
    }
    
    NSDictionary *response = [json objectForKey:@"total_transactions_value_response"];
    
    NSNumber *status = [response objectForKey:@"status"];
    
    if(status==nil)
    {
        [PRUtils displayStatusErrorDialog];
        return;
    }
    
    switch (status.intValue) {
        case STATUS_OK:
        {
            _amount = [NSNumber numberWithInt:(20000 - ((NSNumber*)[response objectForKey:@"value"]).intValue)];
//            PRAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
//            SettingsEntity *settings = [SettingsEntity fetchSettingsObjectWithManagedObjectContext:appDelegate.managedObjectContext];
//            if(settings.userAcceptedTermsOfVerification.boolValue == YES)
//            {
                _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:[NSString stringWithFormat:NSLocalizedString(@"alertVerifyWarning", nil), (_amount.intValue/100), (_amount.intValue%100)] withParent:self callbackSelector:@selector(userAcceptedTermsOfVerification:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
                [_alert show];
//            }
//            else
//            {
//                [self performSegueWithIdentifier:@"showVerifyWarning" sender:self];
//            }
        }
            return;
        case STATUS_WRONG_CARD_ID:
        {
           [PRUtils displayStandardAlertViewWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertWrongCardId", nil) andConfirmButtonTitle:NSLocalizedString(@"alertOk", nil)];
        }
            return;
    }
    
}

- (void) connectionTotalValuesFailed:(PRAsyncConnection*) connection
{
    [_buttonBuyTicket setLockedForResponse:NO];
    [PRUtils displayStatusErrorDialog];
}

- (void) userAcceptedTermsOfVerification:(NSInteger)buttonIndex
{
    [self performSegueWithIdentifier:@"buyTicket" sender:self];
}

#pragma mark - cards provider delegate
- (void)cardsProvider:(PRCardsProvider *)cardsProvider
         fetchedCards:(PRCardsContainer *)cardsContainer
{[self performSegueWithIdentifier:@"buyTicket" sender:self];return;
    [_buttonBuyTicket setLockedForResponse:NO];
    _defaultCard = [cardsContainer defaultCard];
    
    if(_defaultCard == nil)
    {
        _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertNoDefaultCard", nil) withParent:nil callbackSelector:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
        [_alert show];
        return;
    }
    else if(_defaultCard.isVerified == NO)
    {
        [_buttonBuyTicket setLockedForResponse:YES];
        PRAsyncConnection *connection = [[PRAsyncConnection alloc] initWithParent:self successSelector:@selector(connectionTotalValuesSuccess:) didFailSelector:@selector(connectionTotalValuesFailed:)];
        [connection sendTotalTransactionsValueRequestWithCardId:_defaultCard.identifier];
    }
    else
    {
        [self performSegueWithIdentifier:@"buyTicket" sender:self];
    }
    
}

- (void)cardsProviderUnableConnectToServer:(PRCardsProvider *)cardsProvider
{
    [_buttonBuyTicket setLockedForResponse:NO];
}

- (void)cardsProviderFailedToFetchCards:(PRCardsProvider *)cardsProvider
{
    [_buttonBuyTicket setLockedForResponse:NO];
}

- (void)cardsProviderFailedToFetchCards:(PRCardsProvider *)cardsProvider
                        withErrorStatus:(NSInteger)status
{
    [_buttonBuyTicket setLockedForResponse:NO];
    
    switch (status) {
        case STATUS_USER_BLOCKED:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertUserBlocked", nil) withParent:self callbackSelector:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
    }
}

#pragma mark - provate
- (void) handleRelationBackViews
{
    if(_orderObject.relationBack == nil)
    {
        _controlRelationBackDetails.hidden = YES;
        _viewRelationBack.view.hidden = NO;
        _buttonDeleteBackRelation.hidden = YES;
    }
    else
    {
        _controlRelationBackDetails.hidden = NO;
        _viewRelationBack.view.hidden = YES;
        _buttonDeleteBackRelation.hidden = NO;
    }

}

#pragma mark - date picker
- (void) showDateView
{
    [PRUtils slideView:_datePicker.view toY:(self.view.frame.size.height - _datePicker.view.frame.size.height) completion:nil];
}

- (void) hideDateView
{
    [PRUtils slideView:_datePicker.view toY:self.view.frame.size.height completion:nil];
}

- (void) dateSet:(NSDate*) date
{
    _orderObject.orderDateBack = date;
    
    _viewRelationBack.labelBackDate.text = [PRUtils getFormattedDateFromDate:_orderObject.orderDateBack];
    
    [self hideDateView];
}

- (void) dateSetCancelled
{
    [self hideDateView];
}

#pragma mark - subview back relation
- (void) buttonSearchBackRelationOnClick
{
    _orderObject.relationBackToFill = YES;
    [self performSegueWithIdentifier:@"showBackRelations" sender:self];
}

- (void) controlBackDateOnClick
{
    [self showDateView];
}

#pragma mark - table view delegate and datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableView isEqual:_tableTrainAttributes] ? _orderObject.relationFrom.relationAttributes.count : _orderObject.relationBack.relationAttributes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = [NSString stringWithFormat:@"attribute%d", indexPath.row];
    
    PRTrainAttributeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PRTrainAttributeCell" owner:self options:nil];
        for (id oneObject in nib)
        {
            if ([oneObject isKindOfClass:[PRTrainAttributeCell class]])
            {
                cell = (PRTrainAttributeCell*)oneObject;
            }
        }
    }
    
    if([tableView isEqual:_tableTrainAttributes])
    {
        cell.labelAttribute.text = ((PRRelationAttributeObject*)[_orderObject.relationFrom.relationAttributes objectAtIndex:indexPath.row]).attributeName;
        cell.labelAttributeDescription.text = ((PRRelationAttributeObject*)[_orderObject.relationFrom.relationAttributes objectAtIndex:indexPath.row]).attributeDescription;
    }
    else
    {
        cell.labelAttribute.text = ((PRRelationAttributeObject*)[_orderObject.relationBack.relationAttributes objectAtIndex:indexPath.row]).attributeName;
        cell.labelAttributeDescription.text = ((PRRelationAttributeObject*)[_orderObject.relationBack.relationAttributes objectAtIndex:indexPath.row]).attributeDescription;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return PR_TRAIN_ATTRIBUTE_CELL_HEIGHT;
}

#pragma mark - buttons

- (IBAction)buttonBuyTicketOnClick:(id)sender {
    [_buttonBuyTicket setLockedForResponse:YES];
    [_cardsProvider fetchCards];
}

- (IBAction)buttonDeleteBackRelationOnClick:(id)sender {
    _orderObject.relationBack = nil;
    
    [self handleRelationBackViews];
}
@end
