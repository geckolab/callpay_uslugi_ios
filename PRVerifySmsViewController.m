//
//  PRVerifySmsViewController.m
//  pr
//
//  Created by Marcin Szulc on 17.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PRVerifySmsViewController.h"
#import "PRUtils.h"

#import "PRAsyncConnection.h"
#import "XLog.h"

#import "SettingsEntity+Additions.h"
//#import "PRAppDelegate.h"
#import "PRStandardButton.h"

#import "PRTabBarViewController.h"

#import "PRDBManager.h"

NSString *const PRVerifySMSViewControllerVerifiedUserNotification = @"PRVerifySMSViewControllerVerifiedUserNotification";

static NSString * const LogTag = @"VerifySmsViewController";

@interface PRVerifySmsViewController ()

@end

@implementation PRVerifySmsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UIBarButtonItem * mcmLogo = [[UIBarButtonItem alloc] initWithCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mcm_icon"]]];
    self.navigationItem.rightBarButtonItem = mcmLogo;
    
    [self enableOrDisableVerifyButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setStringSmsCode:(NSString *)stringSmsCode
{
    _stringSmsCode = stringSmsCode;
    _textSmsCode.text = _stringSmsCode;
    [self enableOrDisableVerifyButton];
}

- (void) enableOrDisableVerifyButton
{
    if(_textSmsCode.text.length > 0)
    {
        _buttonVerify.enabled = YES;
    }
    else
    {
        _buttonVerify.enabled = NO;
    }
}

- (IBAction)buttonVerifyOnClick:(id)sender {
    PRAsyncConnection *connection = [[PRAsyncConnection alloc] initWithParent:self successSelector:@selector(connectionSucces:) didFailSelector:@selector(connectionFailed:)];
    [connection sendGetCertificateRequestWithCertToken:_cert_token andSMScode:_textSmsCode.text];
    [_buttonVerify setLockedForResponse:YES];
}

- (IBAction)backgroundTouched:(id)sender {
    [_textSmsCode resignFirstResponder];
    [self enableOrDisableVerifyButton];
}

#pragma mark - connection
- (void) connectionFailed:(PRAsyncConnection*) connection
{
    [_buttonVerify setLockedForResponse:NO];
    [PRUtils displayStandardAlertViewWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertConnectionFailed", nil) andConfirmButtonTitle:NSLocalizedString(@"alertOk", nil)];
}

- (void) connectionSucces:(PRAsyncConnection*) connection
{
    [_buttonVerify setLockedForResponse:NO];
    XLogTag(LogTag, @"%@", [[NSString alloc] initWithData:connection.responseData encoding:NSUTF8StringEncoding]);

    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:connection.responseData
                          options:kNilOptions
                          error:&error];

    if(error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        return;
    }

    NSDictionary *register_response = [json objectForKey:@"download_response"];

    NSNumber *status = [register_response objectForKey:@"status"];

    if(status==nil)
    {
        [PRUtils displayStatusErrorDialog];
        [self.navigationController popToRootViewControllerAnimated:YES];
        return;
    }

    switch (status.intValue) {
        case STATUS_OK:
        {
            NSString *certificate = [register_response objectForKey:@"certificate"];
            NSString *filename = [register_response objectForKey:@"filename"];
            NSString *checksum = [register_response objectForKey:@"checksum"];

            NSData *decoded = [PRUtils base64DataFromString:certificate];

            if([[checksum uppercaseString] isEqualToString:[[PRUtils md5StringFromData:decoded] uppercaseString]])
            {
                XLogTag(LogTag, @"Checksums ok");
                NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString *path = [applicationDocumentsDir stringByAppendingPathComponent:filename];

                [decoded writeToFile:path atomically:YES];
            }
            else
            {
                [PRUtils displayStandardAlertViewWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertVerifyError", nil) andConfirmButtonTitle:NSLocalizedString(@"alertOk", nil)];
                [self.navigationController popToRootViewControllerAnimated:YES];
                return;
            }

//            PRAppDelegate *appDelegate = (PRAppDelegate*)[UIApplication sharedApplication].delegate;
            SettingsEntity *settings = [SettingsEntity fetchSettingsObjectWithManagedObjectContext:[[PRDBManager object]managedObjectContext]];

            settings.userVerified = [NSNumber numberWithBool:YES];
            settings.userCertFileName = filename;

            NSError *error;
            [[[PRDBManager object]managedObjectContext] save:&error];

            if(error)
            {
                XLogTag(LogTag, @"%@", error.localizedDescription);
                return;
            }

            [[NSNotificationCenter defaultCenter]
                                   postNotificationName:PRVerifySMSViewControllerVerifiedUserNotification object:nil];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
            break;
        case STATUS_WRONG_CERT_ID:
        {
            [PRUtils displayStandardAlertViewWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertVerifyError", nil) andConfirmButtonTitle:NSLocalizedString(@"alertOk", nil)];
            break;
        }
        case STATUS_WRONG_CERT_TOKEN:
        {
            [PRUtils displayStandardAlertViewWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertVerifyError", nil) andConfirmButtonTitle:NSLocalizedString(@"alertOk", nil)];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
            break;
    }
}


@end
