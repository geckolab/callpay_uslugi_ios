//
//  PRRelationsCell.m
//  pr
//
//  Created by Marcin Szulc on 22.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PRRelationsCell.h"
#import "PRRelationObject.h"

#import "PRUtils.h"

@implementation PRRelationsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setRelationObject:(PRRelationObject *)relationObject
{
    [self bringSubviewToFront:_switchClass];
    
    _relationObject = relationObject;
    
    _labelCategory.text = [PRUtils getRelationCategoryNameForKey:_relationObject.relationCategory];
    _labelDepartureTime.text = _relationObject.relationDepartureTime;
    _labelArrivalTime.text = _relationObject.relationArrivalTime;
    _labelTravelTime.text = [PRUtils getTravelTimeForStartHour:_relationObject.relationDepartureTime andBackHour:_relationObject.relationArrivalTime];
    
    if(_relationObject.relationSelectedClass == nil)
    {
        _labelClass.text = NSLocalizedString(@"wybierzKlase", nil);
        _switchClass.hidden = NO;
    }
    else
    {
        _switchClass.hidden = YES;
        _labelClass.text = [NSString stringWithFormat:NSLocalizedString(@"klasa", nil), _relationObject.relationSelectedClass.intValue];
    }
    
}

- (IBAction)switchClassValueChanged:(id)sender {
    
    if(((UISegmentedControl*)sender).selectedSegmentIndex == 0)
    {
        _relationObject.relationSelectedClass = [NSNumber numberWithInt:1];
        _labelClass.text = [NSString stringWithFormat:NSLocalizedString(@"klasa", nil), 1];
    }
    else
    {
        _relationObject.relationSelectedClass = [NSNumber numberWithInt:2];
        _labelClass.text = [NSString stringWithFormat:NSLocalizedString(@"klasa", nil), 2];
    }
    
}
@end
