//
//  PRTabBarViewController.m
//  CallPay
//
//  Created by Jacek on 15.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "PRTabBarViewController.h"

#import "SettingsEntity+Additions.h"
#import "XLog.h"
#import "PRAlertView.h"
#import "PRAsyncConnection.h"

#import "PRVerifySmsViewController.h"
#import "PRRegisterCardViewController.h"
#import "PRUpdateViewController.h"

#import "PRDBManager.h"

static NSString * const LogTag = @"PRTabBarViewController";

@interface PRTabBarViewController ()

@property (strong, nonatomic) PRAlertView *alert;

@property (strong, nonatomic) NSNumber *tableStationsVersion;
@property (strong, nonatomic) NSNumber *tableDiscountsVersion;
@property (strong, nonatomic) NSNumber *tableTrainAttributesVersion;
@property (strong, nonatomic) NSNumber *tableIdTypesVersion;

@property (nonatomic) BOOL registerCardViewControllerShouldBeShown;

@end

@implementation PRTabBarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
//        [[NSNotificationCenter defaultCenter]
//         addObserver:self
//         selector:@selector(userWasVerifiedNotification)
//         name:PRVerifySMSViewControllerVerifiedUserNotification
//         object:nil];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
//        [[NSNotificationCenter defaultCenter]
//         addObserver:self
//         selector:@selector(userWasVerifiedNotification)
//         name:PRVerifySMSViewControllerVerifiedUserNotification
//         object:nil];
    }
    return self;
}

- (void)userWasVerifiedNotification
{
    if (![self showedRegisterCardViewController])
    {
        [self setRegisterCardViewControllerShouldBeShown:YES];
    }
    [self setThatShowedRegisterCardViewController];
}

- (BOOL)showedRegisterCardViewController
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"showedRegisterCardViewController"];
}

- (void)setThatShowedRegisterCardViewController
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"showedRegisterCardViewController"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)showRegisterCardViewController
{
    [self performSegueWithIdentifier:@"showRegisterCardViewController" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue
                 sender:(id)sender
{
    [super prepareForSegue:segue sender:sender];
    
    if ([[segue identifier] isEqualToString:@"showRegisterCardViewController"])
    {
        UINavigationController *navigationController = [segue destinationViewController];
        
        PRRegisterCardViewController *registerCardViewController = (PRRegisterCardViewController *)[navigationController topViewController];
        [registerCardViewController setDelegate:self];
        [registerCardViewController setUsedForInitialCardSelection:YES];
    }
    else if ([[segue identifier] isEqualToString:@"updateDatabase"])
    {
        PRUpdateViewController *updateViewController = [segue destinationViewController];
        [updateViewController setDelegate:self];
    }
}

- (void)registerCardViewControllerShouldBeDismissed:(PRRegisterCardViewController *)registerCardViewController
{
    [registerCardViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self verify];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    PRAsyncConnection *connection = [[PRAsyncConnection alloc] initWithParent:self successSelector:@selector(stationsDBSuccess:) didFailSelector:@selector(stationsDBFailed:)];
//    [connection sendUpdateStationsRequest];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) checkIfDataBaseUpdateNeeded
{
//    PRAppDelegate *appDelegtae = (PRAppDelegate*)[UIApplication sharedApplication].delegate;
    SettingsEntity *settings = [SettingsEntity fetchSettingsObjectWithManagedObjectContext:[[PRDBManager object] managedObjectContext]];
    
    if(settings.tableDiscountsVersion.intValue == -1 || settings.tableStationsVersion.intValue == -1 || settings.tableTrainAttributesVersion.intValue == -1 || settings.tableIdTypesVersion.intValue == -1)
    {
        _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertSyncDbNeeded", nil) withParent:self callbackSelector:@selector(updateDatabase:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
        [_alert show];
        return;
    }
    else
    {
        _tableStationsVersion = settings.tableStationsVersion;
        _tableDiscountsVersion = settings.tableDiscountsVersion;
        _tableTrainAttributesVersion = settings.tableTrainAttributesVersion;
        _tableIdTypesVersion = settings.tableIdTypesVersion;
        
        PRAsyncConnection *connection = [[PRAsyncConnection alloc] initWithParent:self successSelector:@selector(dbVersionCheckSuccess:) didFailSelector:@selector(dbVersionCheckFailed:)];
        [connection sendDBCheckVersionRequestWithPhoneNo:settings.userPhoneNo];
    }
    
}

- (void) updateDatabase:(NSNumber*) buttonIndex
{
    [self performSegueWithIdentifier:@"updateDatabase" sender:nil];
}

- (void)updateViewControllerShouldBeDismissed:(PRUpdateViewController *)updateViewController
{
    [updateViewController dismissViewControllerAnimated:YES completion:^
     {
         if ([self registerCardViewControllerShouldBeShown])
         {
             [self showRegisterCardViewController];
             [self setRegisterCardViewControllerShouldBeShown:NO];
         }
     }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) verify
{
    //[self setSelectedIndex:0];
    
//    PRAppDelegate *appDelegtae = (PRAppDelegate*)[UIApplication sharedApplication].delegate;
    
    SettingsEntity *settings = [SettingsEntity fetchSettingsObjectWithManagedObjectContext:[[PRDBManager object] managedObjectContext]];
    
    if(settings.userVerified.boolValue == NO)
    {
        //verify user here
        XLogTag(LogTag, @"User not verified");
        
        [self performSegueWithIdentifier:@"verifyUser" sender:nil];
        
        return;
        
    }
    else
    {
        XLogTag(LogTag, @"user verified");
        [self checkIfDataBaseUpdateNeeded];
        //        [self showRegisterCardViewController]; // TODO: Delete it
    }
    
    
}

- (void) dbVersionCheckSuccess:(PRAsyncConnection*) connection
{
    XLogTag(LogTag, @"%@", [[NSString alloc] initWithData:connection.responseData encoding:NSUTF8StringEncoding]);
    
    bool needsToUpdate = NO;
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:connection.responseData
                          options:kNilOptions
                          error:&error];
    
    if(error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        [self connectionFailed];
        return;
    }
    
    NSDictionary *response = [json objectForKey:@"db_sync_response"];
    
    NSNumber *status = [response objectForKey:@"status"];
    
    if(status==nil)
    {
        [self connectionFailed];
        return;
    }
    
    switch (status.intValue) {
        case STATUS_OK:
        {
            NSArray *tables = [response objectForKey:@"tables"];
            
            for(NSDictionary *table in tables)
            {
                if([table isEqual:@""])
                {
                    XLogTag(LogTag, @"empty list parameter");
                    continue;
                }
                
                NSString *tableName = [[table objectForKey:@"table"] objectForKey:@"name"];
                NSNumber *tableNumber = [[table objectForKey:@"table"] objectForKey:@"version"];
                
                if([tableName isEqualToString:@"stations"])
                {
                    if(tableNumber.intValue > _tableStationsVersion.intValue)
                    {
                        needsToUpdate = YES;
                        _tableStationsVersion = tableNumber;
                    }
                }
                else if([tableName isEqualToString:@"discounts"])
                {
                    if(tableNumber.intValue > _tableDiscountsVersion.intValue)
                    {
                        needsToUpdate = YES;
                        _tableDiscountsVersion = tableNumber;
                    }
                }
                else if([tableName isEqualToString:@"train_attributes"])
                {
                    if(tableNumber.intValue > _tableTrainAttributesVersion.intValue)
                    {
                        needsToUpdate = YES;
                        _tableTrainAttributesVersion = tableNumber;
                    }
                }
                else if([tableName isEqualToString:@"document_types"])
                {
                    if(tableNumber.intValue > _tableIdTypesVersion.intValue)
                    {
                        needsToUpdate = YES;
                        _tableIdTypesVersion = tableNumber;
                    }
                }
                
            }
            
        }
            break;
        case STATUS_USER_BLOCKED:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertUserBlocked", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
    }
    
    if(needsToUpdate == YES)
    {
        XLogTag(LogTag, @"Need to update database");
        _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertSyncDbNeeded", nil) withParent:self callbackSelector:@selector(updateDatabase:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
        [_alert show];
    }
    
}

- (void) dbVersionCheckFailed:(PRAsyncConnection*) connection
{
    [self connectionFailed];
}

- (void) connectionFailed
{
    _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertStatusError", nil) withParent:self callbackSelector:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
    [_alert show];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
