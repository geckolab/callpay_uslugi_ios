//
//  PROrderObject.h
//  pr
//
//  Created by Marcin Szulc on 21.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class StationEntity;
@class DiscountEntity;
@class PRRelationObject;
@class IdTypeEntity;

@interface PROrderObject : NSObject

@property (atomic, assign) bool passengerHasRegioCard;

@property (strong, nonatomic) NSString *passengerAuthCode;
@property (strong, nonatomic) NSString *passengerTransactionToken;
@property (strong, nonatomic) NSString *passengerAuthType;
@property (strong, nonatomic) NSString *passengerPhone;
@property (strong, nonatomic) NSString *passengerName;
@property (strong, nonatomic) NSString *passengerSurname;
@property (strong, nonatomic) IdTypeEntity *passengerIdType;
@property (strong, nonatomic) NSString *passengerIdNo;

@property (strong, nonatomic) DiscountEntity *discount;

@property (atomic, assign) bool relationBackToFill;
@property (atomic, assign) bool stationFromToFill;

@property (strong, nonatomic) NSDate *orderDateFrom;
@property (strong, nonatomic) NSDate *orderDateBack;

@property (strong, nonatomic) StationEntity *stationFrom;
@property (strong, nonatomic) StationEntity *stationTo;

@property (strong, nonatomic) PRRelationObject *relationFrom;
@property (strong, nonatomic) PRRelationObject *relationBack;

@property (nonatomic) NSString *cardID;

@property (strong, nonatomic) NSNumber *vehicleClass;

@end
