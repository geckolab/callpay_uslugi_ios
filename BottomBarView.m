//
//  BottomBarView.m
//  CallPay
//
//  Created by Jacek on 08.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "BottomBarView.h"

@implementation BottomBarView

@synthesize settingsButton = _settingsButton;
@synthesize historyButton = _historyButton;
@synthesize activeButton = _activeButton;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setup];
    }
    return self;
}
- (id)init
{
    self = [super init];
    if (self)
    {
        [self setFrame:CGRectMake(0, 0, 320, 50)];
        [self setup];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self setup];
    }
    return self;
}
-(void)setup
{
//    bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
//    [self addSubview:bgView];
//    [self setBackgroundImage:[UIImage imageNamed:@"bottom_bar"]];
    self.backgroundColor = [UIColor blackColor];
    
    _settingsButton = [[BottomBarButton alloc] initWithFrame:CGRectMake(0, 0, 94, 50)];
    [_settingsButton setImage:[UIImage imageNamed:@"bar_icon_kontrola"] forState:UIControlStateNormal];//btn_cards
//    [_settingsButton.titleLabel setFont:[UIFont fontWithName:@"Swis721LtEU" size:15]];
    _historyButton = [[BottomBarButton alloc] initWithFrame:CGRectMake(94, 0, 94, 50)];
    [_historyButton setImage:[UIImage imageNamed:@"bar_icon_historia"] forState:UIControlStateNormal];//bar_icon_historia//btn_tickets
    _activeButton = [[BottomBarButton alloc] initWithFrame:CGRectMake(2*94, 0, 94, 50)];
    [_activeButton setImage:[UIImage imageNamed:@"bar_icon_kontrola"] forState:UIControlStateNormal];//bar_icon_kontrola//btn_control
    buttons = [[NSArray alloc] initWithObjects:_settingsButton,_historyButton,_activeButton, nil];
    for (BottomBarButton *btn in buttons)
    {
        [self addSubview:btn];
    }
    [_settingsButton setTitle:@"Ustawienia" forState:UIControlStateNormal];
    [_historyButton setTitle:@"Historia biletów" forState:UIControlStateNormal];
    [_activeButton setTitle:@"Kontrola biletów" forState:UIControlStateNormal];
    [self setNeedsLayout];
}
-(void)setBackgroundImage:(UIImage*)image
{
    [bgView setImage:image];
}
-(void)layoutSubviews
{
    CGFloat totalWidth = 94 * [buttons count];
//    CGFloat fWidth = self.frame.size.width;
//    CGFloat dWitdth = fWidth - totalWidth;
    CGFloat dx = 0;
    if ([buttons count] > 1)
    {
        dx = (self.frame.size.width - totalWidth) / ([buttons count] + 0);
    }
    else
    {
        dx = (self.frame.size.width - 94) / 2;
    }
    
    CGFloat lastXPos = dx /2;
    
    for (int idx = 0; idx < [buttons count]; idx++)
    {
        BottomBarButton *btn = [buttons objectAtIndex:idx];
        if (idx > 0)
        {
//            [btn setFrame:CGRectMake(((dx + 94) * idx), btn.frame.origin.y, btn.frame.size.width, btn.frame.size.height)];
        }
        else
        {
//            [btn setFrame:CGRectMake((dx / 2), btn.frame.origin.y, btn.frame.size.width, btn.frame.size.height)];
        }
        
        [btn setFrame:CGRectMake(lastXPos, btn.frame.origin.y, btn.frame.size.width, btn.frame.size.height)];
        lastXPos = btn.frame.origin.x + btn.frame.size.width + dx;
        
    }
}
@end
