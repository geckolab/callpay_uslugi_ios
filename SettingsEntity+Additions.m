//
//  SettingsEntity+Additions.m
//  pr
//
//  Created by Marcin Szulc on 17.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "SettingsEntity+Additions.h"

@implementation SettingsEntity (Additions)

+ (SettingsEntity*) insertDefaultSettingsWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext
{
    
    NSArray *rows = [SettingsEntity fetchAllSettingsWithManagedObjectContext:managedObjectContext];
    
    if(rows.count>0)
    {
        return [self fetchSettingsObjectWithManagedObjectContext:managedObjectContext];
    }
    
    SettingsEntity *settings = [NSEntityDescription insertNewObjectForEntityForName:@"SettingsEntity" inManagedObjectContext:managedObjectContext];
    return settings;
}

+ (NSArray*) fetchAllSettingsWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SettingsEntity"];
    return [managedObjectContext executeFetchRequest:request error:nil];
}

+ (SettingsEntity*) fetchSettingsObjectWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext
{
    return [[self fetchAllSettingsWithManagedObjectContext:managedObjectContext] objectAtIndex:0];
}

@end
