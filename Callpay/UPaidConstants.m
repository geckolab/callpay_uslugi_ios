//
//  Constants.m
//  uPaid
//
//  Created by uPaid on 05.12.2011.
//  Copyright (c) 2011 uPaid All rights reserved.
//

#import "UPaidConstants.h"

NSString *const PHONE_NUM_KEY = @"PHONE_NUMBER_FROM_PREFERENCE";

NSString *const PHONE_CODE_ASKED = @"PHONE_CODEASKED";
NSString *const USER_HAS_CARDS = @"USER_HAS_CARDS";

NSString *const STORE_SERVICE = @"CALLPAY";
NSString *const DEVICE_TOKEN_FOR_PUSH = @"DEVICE_TOKEN_FOR_PUSH";

NSString *const UPAID_PHONE_CODE = @"phone_token";
NSString *const UPAID_PHONE_NUMBER = @"UPAID_PHONE_NUMBER";
NSString *const PHONE_NUM_KEY_TO_TOP_UP = @"PHONE_NUM_KEY_TO_TOP_UP";

NSString *const UPAID_LOGIN_STATUS = @"login_status";
NSString *const UPAID_TOKEN_TIME = @"token_time";

NSString *const UPAID_DEFAULT_CARD_ID = @"default_card_id";
NSString *const UPAID_DEFAULT_CARD_NICKNAME = @"default_card_pan";
NSString *const UPAID_DEFAULT_CARD_TYPE = @"default_card_type";

NSString *const UPAID_CARD_TO_PAY_ID = @"pay_card_id";
NSString *const UPAID_CARD_TO_PAY_PAN = @"pay_card_pan";
NSString *const UPAID_CARD_TO_PAY_TYPE = @"pay_card_type";

