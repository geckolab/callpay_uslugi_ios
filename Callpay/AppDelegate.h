//
//  AppDelegate.h
//  Sprytny Bill
//
//  Created by uPaid on 12.04.2012.
//  Copyright (c) 2012 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomAppDelegate.h"

@protocol PRAppDelegateProtocol <NSObject>

@optional
- (void) applicationBecameActive;

@end

@interface AppDelegate : CustomAppDelegate <UIApplicationDelegate, UITabBarControllerDelegate>

@property (strong, nonatomic) id delegate;

-(void) addLoginView;
-(void) removeLoginView;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *loginController;
@property (retain, nonatomic) UINavigationController *nav;
@property (retain, nonatomic) UINavigationController *mainController;

+ (UIBarButtonItem *) customLeftButton: (NSString *) title delegate: (id) delegate action: (SEL) action;

@end
