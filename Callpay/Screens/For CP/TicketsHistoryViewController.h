//
//  TicketsHistoryViewController.h
//  CallPay
//
//  Created by uPaid on 20.08.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "ProjectViewController.h"
#import "ActiveTicketsHeaderView.h"

#import "CellDeleteButton.h"

@interface TicketsHistoryViewController : ProjectViewController <UIAlertViewDelegate,UITableViewDataSource, UITableViewDelegate> {
  NSInteger fromScreen;
    ActiveTicketsHeaderView *headerView;
    UITableView *tableView;
}

@property (nonatomic, retain) NSArray *ticketsList;

typedef enum fromScreens {
  MAIN_SCREEN,
  TICKETS_SCREEN
} fromScreens;

- (id)initFrom: (NSInteger) screen;

-(IBAction)deleteAction:(id)sender;

@end
