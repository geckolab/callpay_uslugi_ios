//
// ProjectMainScreenViewController.m
//  CallPay
//
//  Created by uPaid on 06.06.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "ProjectMainScreenViewController.h"
#import "ProjectButton.h"
#import "ProjectViewHelper.h"
#import "TicketsMainScreenViewController.h"
#import "ASIHTTPRequest.h"
#import "ProjectLocations.h"
#import "TicketsHistoryViewController.h"
#import "OtherServicesViewController.h"
#import "MCMDetailsViewController.h"
#import "CardsListViewController.h"

#import "CardsListViewController.h"
#import "TicketsHistoryViewController.h"
#import "TicketsCheckListViewController.h"

#import "UpaidMPM.h"
#import "UpaidMPMDelegate.h"
#import "MPMBuyTicketDelegate.h"


@implementation ProjectMainScreenViewController

@synthesize topMarginView = _topMarginView;
@synthesize bottomBar = _bottomBar;

@synthesize ticketsButton = _ticketsButton;
@synthesize parkingButton = _parkingButton;
@synthesize prButton = _prButton;

- (id)init
{
  self = [super init];
  
  if (self) {
    [self getLocationsListFromServer];
    [self getParkingLocationsListFromServer];
  }
  
  return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        [self getLocationsListFromServer];
        [self getParkingLocationsListFromServer];
    }
    
    return self;
}
- (void) viewDidLoad
{
    [super viewDidLoad];
    [_topMarginView setFrame:CGRectMake(0, 0, self.view.frame.size.width, 20+[self getTopBarHeight])];
    [_bottomBar.settingsButton addTarget:self action:@selector(settingsAction:) forControlEvents:UIControlEventTouchUpInside];
    [_bottomBar.historyButton addTarget:self action:@selector(myTicketsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [_bottomBar.activeButton addTarget:self action:@selector(controlsTicketsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_ticketsButton attribute:NSLayoutAttributeWidth relatedBy: NSLayoutRelationEqual toItem:_ticketsButton attribute:NSLayoutAttributeWidth multiplier:0.5 constant:132.0]];
    
    [_ticketsButton setFrame:CGRectMake(14, [self getTopBarHeight], _ticketsButton.frame.size.width, _ticketsButton.frame.size.height)];
    [_parkingButton setFrame:CGRectMake(self.view.frame.size.width - _parkingButton.frame.size.width - 14, [self getTopBarHeight], _parkingButton.frame.size.width, _parkingButton.frame.size.height)];
    [_prButton setFrame:CGRectMake(14, [self getTopBarHeight] + _ticketsButton.frame.size.height + 20, _prButton.frame.size.width, _prButton.frame.size.height)];
}

- (void) getParkingLocationsListFromServer {
    NSString *url = [NSString stringWithFormat: @"%@/callpay/getParkingLocations%@", UPAID_URL_PREFIX,UPAID_URL_SUFIX];
//    NSString *url = [NSString stringWithFormat: @"%@/callpay/getLocations%@", UPAID_URL_PREFIX,UPAID_URL_SUFIX];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString: url]];
    [request setUseKeychainPersistence:YES];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestParkingLocationsFinished:)];
    [request setDidFailSelector:@selector(requestFailed:)];
    [request startAsynchronous];
}

- (void)requestParkingLocationsFinished:(ASIHTTPRequest *)request{
    NSData *data = [request.responseString dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"%@",[NSJSONSerialization JSONObjectWithData:data options:0 error:nil]);
    [[ProjectLocations object] setParkingLocationList: [NSJSONSerialization JSONObjectWithData:data options:0 error:nil]];
    UpaidLog(@"%@", [[ProjectLocations object] parkingLocationList]);
    
}

- (void) getLocationsListFromServer {
    NSString *url = [NSString stringWithFormat: @"%@/callpay/getLocations%@", UPAID_URL_PREFIX,UPAID_URL_SUFIX];
  ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString: url]];
  [request setUseKeychainPersistence:YES];
  
  [request setDelegate:self];
  [request setDidFinishSelector:@selector(requestFinished:)];
  [request setDidFailSelector:@selector(requestFailed:)];
  [request startAsynchronous];
}

- (void)requestFinished:(ASIHTTPRequest *)request{
  NSData *data = [request.responseString dataUsingEncoding:NSUTF8StringEncoding];
  [[ProjectLocations object] setLocationList: [NSJSONSerialization JSONObjectWithData:data options:0 error:nil]];
  UpaidLog(@"%@", [[ProjectLocations object] locationList]);
}

- (void)requestFailed:(ASIHTTPRequest *)request{
  UpaidLog(@"%@", [request error]);
    
}

#pragma mark buttons

- (IBAction)ticketsButtonClicked:(id)sender {
  TicketsMainScreenViewController *viewController = [[TicketsMainScreenViewController alloc] init];
  [self.navigationController pushViewController:viewController animated:YES];
  //[viewController release];
}

- (IBAction)mcmButtonClicked:(id)sender {
  MCMDetailsViewController *viewController = [[MCMDetailsViewController alloc] init];
  [self.navigationController pushViewController:viewController animated:YES];
  //[viewController release];
}

- (IBAction)lastTicketsButtonClicked:(id)sender {
  TicketsHistoryViewController *viewController = [[TicketsHistoryViewController alloc] initFrom: MAIN_SCREEN];
  [self.navigationController pushViewController:viewController animated:YES];
  //[viewController release];
}

- (IBAction)othersServicesButtonClicked:(id)sender {
  OtherServicesViewController *viewController = [[OtherServicesViewController alloc] init];
  [self.navigationController pushViewController:viewController animated:YES];
  //[viewController release];
}

#pragma mark -

//dolna belka
- (IBAction)cardsManagementButtonClicked:(id)sender {
  CardsListViewController *viewController = [[CardsListViewController alloc] initFrom: CardListFromMainView];
  [self.navigationController pushViewController:viewController animated:YES];
  //[viewController release];
}

- (IBAction)myTicketsButtonClicked:(id)sender {
  TicketsHistoryViewController *viewController = [[TicketsHistoryViewController alloc] initFrom: TICKETS_SCREEN]; //"MAIN_SCREEN" był do ekranu "ostatnio zakupione bilety"
  [self.navigationController pushViewController:viewController animated:YES];
  //[viewController release];
}

- (IBAction)controlsTicketsButtonClicked:(id)sender {
  TicketsCheckListViewController *viewController = [[TicketsCheckListViewController alloc] init];
  [self.navigationController pushViewController:viewController animated:YES];
  //[viewController release];
}

- (IBAction)inProgress:(id)sender {
  [ViewHelper showInfoAlert: @"Usługa w przygotowaniu."];
    
    [UpaidMPM sharedInstance].products = [[NSArray alloc] initWithObjects:[[UpaidMPMProduct alloc] initWithName:@"product name" andShortDescription:@"product short description" andImage: [UIImage imageNamed:@"phone"] andQuantity:1 andAmount:123], nil];
    [[UpaidMPM sharedInstance] setAmount:600 andItemNo:@"123" andAdditionalData:@""];
//    [UpaidMPM sharedInstance].initialPhone = @"604400097";
    [UpaidMPM sharedInstance].initialPhone = @"48123456789";
    [UpaidMPM sharedInstance].logsEnabled = YES;
    [UpaidMPM sharedInstance].delegate = [[MPMBuyTicketDelegate alloc] init];
    [[UpaidMPM sharedInstance] startPaymentForNavigationController:self.navigationController];
}
-(IBAction)testAction:(id)sender
{
    [UpaidMPM sharedInstance].products = [[NSArray alloc] initWithObjects:[[UpaidMPMProduct alloc] initWithName:@"product name" andShortDescription:@"product short description" andImage: [UIImage imageNamed:@"phone"] andQuantity:1 andAmount:123], nil];
    [[UpaidMPM sharedInstance] setAmount:600 andItemNo:@"123" andAdditionalData:@""];
    //    [UpaidMPM sharedInstance].initialPhone = @"604400097";
    [UpaidMPM sharedInstance].initialPhone = @"48123456789";
    [UpaidMPM sharedInstance].logsEnabled = YES;
//    [UpaidMPM sharedInstance].delegate = [[MPMBuyTicketDelegate alloc] init];
    [UpaidMPM sharedInstance].delegate = self;
    [[UpaidMPM sharedInstance] startPaymentForNavigationController:self.navigationController];
}

-(IBAction)settingsAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SettingsVC"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) afterSuccessfullPaymentWithNavigationController: (UINavigationController *) navigationController andId: (int) paymentId andAdditionalData: (NSString *) resultAdditionalData {
    
    NSLog(@"success");
    
    [navigationController popToRootViewControllerAnimated: YES];
    
}



- (void) afterUnsuccessfullPaymentWithNavigationController: (UINavigationController *) navigationController {
    
    NSLog(@"fail");
    
    [navigationController popToRootViewControllerAnimated: YES];
    
}
-(IBAction)parkingAcion:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ParkingMainVC"];
    [self.navigationController pushViewController:vc animated:YES];
}
-(IBAction)przewozyAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"PRMainVC"];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.view.alpha = 1.0;
    self.navigationController.navigationBar.translucent = YES;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    self.view.alpha = 0.0;
}
-(BOOL)shouldAutomaticallyForwardAppearanceMethods
{
    return NO;
}

@end
