//
//  TicketsHistoryViewController.m
//  CallPay
//
//  Created by uPaid on 20.08.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "TicketsHistoryViewController.h"
#import "Ticket+Extensions.h"
#import "AppDelegate.h"
#import "ProjectButton.h"
#import "TicketBuyBaseViewController.h"
#import "ProjectLabel.h"

#import "HitoryTicketCell.h"
#import "TicketPayViewController.h"

@interface TicketsHistoryViewController ()

@end

@implementation TicketsHistoryViewController

- (id)initFrom: (NSInteger) screen{
    self = [super init];
  
    if (self) {
      fromScreen = screen;
      AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
      NSManagedObjectContext *context = [appDelegate managedObjectContext];
      
      NSEntityDescription *eDescription = [NSEntityDescription entityForName:@"Ticket" inManagedObjectContext:context];
      NSFetchRequest *req = [[NSFetchRequest alloc] init];
      [req setEntity:eDescription];
      [req setPredicate:[NSPredicate predicateWithFormat:@"price != 0 and name != nil and status != 0"]];
      [req setShouldRefreshRefetchedObjects:YES];
      
      NSError *error;
      
      self.ticketsList = [context executeFetchRequest:req error:&error];
    }
  
    return self;
}

- (void)viewDidLoad
{
    headerView = [[ActiveTicketsHeaderView alloc] initWithFrame:CGRectMake(16, [self getTopBarHeight], self.view.bounds.size.width - 32, 55)];
    [headerView setIcon:[self getTableIcon]];
    [headerView setTitle:[self getTableTitle]];
    [self.view addSubview:headerView];
//  [self setupTableView];
  isBigTable = YES;
  //[self.view addSubview:self.tableView];
    tableView = [[UITableView alloc] initWithFrame:CGRectMake(16, headerView.frame.origin.y + headerView.frame.size.height, self.view.bounds.size.width - 32, [self getWindowContentHeight] - headerView.frame.size.height - 20)];
    
//    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(16, endSearchBarPosY, self.view.bounds.size.width - 32, [self getWindowContentHeight] - endSearchBarPosY - 20)];
//    [self.view addSubview:self.tableView];
    [tableView setDataSource:self];
    [tableView setDelegate:self];
    tableView.backgroundColor = [UIColor clearColor];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [tableView registerNib:[UINib nibWithNibName:@"HitoryTicketCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"HistoryTicketCell"];
    
    [self.view addSubview:tableView];
  [super viewDidLoad];
  
  if ([self.ticketsList count] == 0) {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:@"Brak historii zakupionych biletów." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    //[alert release];
  }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
  [self.navigationController popViewControllerAnimated:YES];
}

-(NSInteger) getRowsCount {
  int rowsCount = [self.ticketsList count];
  
  if (fromScreen == MAIN_SCREEN) {
    rowsCount = 5;
  } else if (fromScreen == TICKETS_SCREEN) {
    rowsCount = 20;
  }
  
  if (rowsCount > [self.ticketsList count]) {
    return [self.ticketsList count];
  }
  
  return rowsCount;
}

- (NSString *) getTableTitle {
  if (fromScreen == MAIN_SCREEN) {
    return @"Ostatnio zakupione bilety";
  } else {
    return @"Historia biletów";
  }
}

- (UIImage *) getTableIcon {
    return [UIImage imageNamed:@"new_historia_biletow"];
}

- (UIView *) getCellContent: (NSIndexPath *)indexPath {
  UIView *content = [[UIView alloc] init];
  //odwrócenie kolejności biletów poprzez (count - 1 - indexPath) zamiast (indexPath)
  Ticket *ticket = [self ticketAtRow: indexPath.row];
  
//  TFLog(@"HistoryTicket %d: %@", indexPath.row, ticket);
  
  ProjectLabel *nameLabel = [[ProjectLabel alloc] initWithFrame:CGRectMake(15, 5, 245, 22.5)];
  [nameLabel makeWithTitleAlignLeft: ticket.name andSize: 13];
    [nameLabel setupLabelHeight];
  [content addSubview:nameLabel];
  //[nameLabel release];
  
  ProjectLabel *createdLabel = [[ProjectLabel alloc] initWithFrame:CGRectMake(15, nameLabel.frame.origin.y + nameLabel.frame.size.height, 90, 22.5)];
  
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"HH:mm dd.MM.yyyy"];
  
  [createdLabel makeWithTitleAlignLeft: [dateFormatter stringFromDate:ticket.purchaseDate] andSize: 9];
  [content addSubview:createdLabel];
  //[createdLabel release];
  
    
    int createdLabelBottomEdge = createdLabel.frame.size.height + createdLabel.frame.origin.y + 10;
    [self setCellHeight: createdLabelBottomEdge forRowAtIndexPath:indexPath];
    
    ProjectButton *btnBuy = [[ProjectButton alloc] initWithFrame:CGRectMake(255, (createdLabelBottomEdge - 35) / 2 , 35, 35)];
    [btnBuy setBackgroundImage:[UIImage imageNamed:@"btn_buy"] forState:UIControlStateNormal];
    [btnBuy setBackgroundImage:[UIImage imageNamed:@"btn_buy_pressed"] forState:UIControlStateHighlighted];
    [btnBuy addTarget:self action:@selector(buyTicket:) forControlEvents:UIControlEventTouchUpInside];
    [btnBuy setTag:indexPath.row];
    [content addSubview:btnBuy];
  
  return content;
}

- (Ticket *) ticketAtRow: (NSInteger) row {
  return (Ticket *) [self.ticketsList objectAtIndex:[self.ticketsList count] - 1 - row];
}

#pragma mark buttons

- (void) buyTicket: (ProjectButton *) sender {
  Ticket *historyTicket = [self ticketAtRow:sender.tag];
  
  if (historyTicket.verificationTime == nil || [historyTicket.verificationTime intValue] <= 0) {
    [ProjectViewHelper showInfoAlert: @"Wybrana funkcja nie jest wspierana dla tego biletu."];
  } else {
    Ticket *newTicket = (Ticket *) [NSEntityDescription insertNewObjectForEntityForName: @"Ticket" inManagedObjectContext:[((AppDelegate *)[UIApplication sharedApplication].delegate) managedObjectContext]];
    
    [newTicket setSeller: historyTicket.seller];
    [newTicket setName: historyTicket.name];
    [newTicket setProductName: historyTicket.productName];
    [newTicket setProductCode: historyTicket.productCode];
    [newTicket setStatus: @(0)];
    [newTicket setVerificationPhone:historyTicket.verificationPhone];
    [newTicket setVerificationTime: historyTicket.verificationTime];
    [newTicket setCityIndex: historyTicket.cityIndex];
    
    TicketBuyBaseViewController *controller = [[TicketBuyBaseViewController alloc] initWithDictionary:@{} andTicket:newTicket];
    [controller setCustomNavigationController:self.navigationController];
    [controller startCallpayInit];
  }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self getRowsCount];
}
- (UITableViewCell *)tableView:(UITableView *)table cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"HistoryTicketCell";
    //    NSString *cellIdentifier = [NSString stringWithFormat:@"TicketCell", indexPath.row];
    HitoryTicketCell *cell = [table dequeueReusableCellWithIdentifier:cellIdentifier];
    
    //    if (cell == nil || IS_IOS7_OR_LATER)
    if (cell == nil)
    {
        cell = [[HitoryTicketCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    //    else {
    //        if ([cell subviews]){
    //            for (UIView *subview in [cell subviews]) {
    //                [subview removeFromSuperview];
    //            }
    //        }
    //    }
    
    //  static NSString *CellIdentifier = @"Cell";
    //
    //  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //
    //  if (cell == nil) {
    //    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    //  }
    Ticket *ticket = [self ticketAtRow: indexPath.row];
    
    
    cell.selectedBackgroundView.backgroundColor=[UIColor clearColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    //    cell.textLabel.text = [[self dataKeysToShow] objectAtIndex:indexPath.row];
//    NSDictionary *dict = (NSDictionary*)[self.dataToProcess valueForKey:[[self dataKeysToShow] objectAtIndex:indexPath.row]];
    //    cell.detailTextLabel.text = [dict valueForKey:@"price"];
    //        cell.textLabel.text = [cell.textLabel.text stringByAppendingString: [NSString stringWithFormat:@" %@",[dict valueForKey:@"price"]]];
//    cell.nameLabel.text = [[self dataKeysToShow] objectAtIndex:indexPath.row];
    //    [cell.nameLabel setTextColor:[UIColor redColor]];
    //    cell.priceLabel.text = [dict valueForKey:@"price"];
//    [cell setPrice:[dict valueForKey:@"price"]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm | dd.MM.yyyy"];
    cell.nameLabel.text = ticket.name;
    cell.detailTextLabel.text = [dateFormatter stringFromDate:ticket.purchaseDate];
    cell.buyDateLabel.text = [dateFormatter stringFromDate:ticket.purchaseDate];
    
    [cell.deleteButton setTag:indexPath.row];
    [cell.deleteButton addTarget:self action:@selector(deleteAction:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tabView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}
-(IBAction)deleteAction:(id)sender
{
    CellDeleteButton *btn = (CellDeleteButton*)sender;
    NSLog(@"delete action %d",btn.tag);
}
- (CGFloat)tableView:(UITableView *)tabView heightForHeaderInSection:(NSInteger)section
{
    return 2;
}
- (UIView *)tableView:(UITableView *)tabView viewForHeaderInSection:(NSInteger)section
{
    UIView *hv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tabView.frame.size.width, 2)];
    [hv setBackgroundColor:[UIColor clearColor]];
    return hv;
}
- (NSIndexPath *)tableView:(UITableView *)tabView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Ticket *historyTicket = [self ticketAtRow:indexPath.row];
    
    if (historyTicket.verificationTime == nil || [historyTicket.verificationTime intValue] <= 0) {
        [ProjectViewHelper showInfoAlert: @"Wybrana funkcja nie jest wspierana dla tego biletu."];
    } else {
        Ticket *newTicket = (Ticket *) [NSEntityDescription insertNewObjectForEntityForName: @"Ticket" inManagedObjectContext:[((AppDelegate *)[UIApplication sharedApplication].delegate) managedObjectContext]];
        
        [newTicket setSeller: historyTicket.seller];
        [newTicket setName: historyTicket.name];
        [newTicket setProductName: historyTicket.productName];
        [newTicket setProductCode: historyTicket.productCode];
        [newTicket setStatus: @(0)];
        [newTicket setVerificationPhone:historyTicket.verificationPhone];
        [newTicket setVerificationTime: historyTicket.verificationTime];
        [newTicket setCityIndex: historyTicket.cityIndex];
        
//        TicketBuyBaseViewController *controller = [[TicketBuyBaseViewController alloc] initWithDictionary:@{} andTicket:newTicket];
//        [controller setCustomNavigationController:self.navigationController];
//        [controller startCallpayInit];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        TicketPayViewController *vc = (TicketPayViewController*) [storyboard instantiateViewControllerWithIdentifier:@"PayTicketVC"];
        [vc setupWithTicket:newTicket];
//        vc.ticketName = historyTicket.name;
//        vc.price = 123;//[[dict valueForKey:@"price"] floatValue];
//        vc.productCode = historyTicket.productCode;
        [self.view endEditing:YES];
        //    [self goToNextStep:[[self dataKeysToShow] objectAtIndex:indexPath.row]];
        [self.navigationController pushViewController:vc animated:YES];
    }
    return indexPath;
}

@end
