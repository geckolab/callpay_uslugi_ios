//
// ProjectMainScreenViewController.h
//  CallPay
//
//  Created by uPaid on 06.06.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "ProjectViewController.h"

#import "BottomBarView.h"

#import "MainMenuButton.h"

@interface ProjectMainScreenViewController : ProjectViewController

@property (nonatomic, strong) IBOutlet MainMenuButton *ticketsButton;
@property (nonatomic, strong) IBOutlet MainMenuButton *parkingButton;
@property (nonatomic, strong) IBOutlet MainMenuButton *prButton;

@property (nonatomic, strong) IBOutlet UIView *topMarginView;

@property (nonatomic, strong) IBOutlet BottomBarView *bottomBar;

-(IBAction)testAction:(id)sender;

-(IBAction)parkingAcion:(id)sender;

-(IBAction)przewozyAction:(id)sender;

@end
