//
//  TicketsChooseCityViewController.h
//  CallPay
//
//  Created by uPaid on 13.06.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "ProjectViewController.h"
#import "ProjectLocations.h"
#import "ProjectLabel.h"
#import "CatalogsManager.h"

#import "CustomSwitch.h"
#import "LocalizationView.h"

@interface TicketsChooseCityViewController : ProjectViewController <UIAlertViewDelegate, CatalogsManagerDelegate,UITableViewDataSource, UITableViewDelegate> {
  id activeButton;
  NSString *_cityIndex;
  BOOL checkSelectedButtonClicked;
    UITableView *tableView;
}

@property (nonatomic, retain) NSArray *locationsKeys;
@property (nonatomic, retain) UISwitch *btnSwitch;
@property (nonatomic, retain) CustomSwitch *btnSwitch2;
@property (nonatomic, retain) LocalizationView *localizationHeaderView;
@property (nonatomic, retain) CatalogsManager *catalogsManager;

-(IBAction)newButtonDownloadAction:(id)sender;

@end
