//
//  TicketBuyLastStepViewController.m
//  CallPay
//
//  Created by uPaid on 17.06.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "TicketBuyLastStepViewController.h"
#import "ProjectLabel.h"
#import "ProjectButton.h"
#import "ProjectViewHelper.h"
#import "CardsListViewController.h"
#import "UIColor+HexString.h"
#import "AddCardViewController.h"
#import "AppDelegate.h"

@interface TicketBuyLastStepViewController ()

@property NSString *codeDescription;

@end

@implementation TicketBuyLastStepViewController

- (id) initWithTicket: (Ticket *) __ticket {
  self = [super init];
  
  if (self) {
    self.loginController = [[UPaidLogInController alloc] initWithDelegate:self];
    self.ticket = __ticket;
  }
  
  return self;
}

#pragma mark view management

- (void)viewDidLoad
{
  [self setupScrollView];
  [self.view addSubview:self.scrollView];
  ProjectLabel *label = [[ProjectLabel alloc] initWithFrame:CGRectMake(10, 20, self.view.bounds.size.width - 50, 30)];
  [label makeWithBoldTitleAlignLeft:@"Właśnie kupujesz następujący bilet" andSize:15];
  [self.scrollView addSubview:label];
  //[label release];
  
  
  label = [[ProjectLabel alloc] initWithFrame:CGRectMake(20, 55, 230, 23)];
  [label makeWithBoldTitleAlignLeft: self.ticket.name andSize:16];
    [label setupLabelHeight];
  [label setBackgroundColor:[UIColor clearColor]];
    
    
    UIImageView *imageViewBg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"list_grey"]];
    [imageViewBg setFrame: CGRectMake(10, 55, 300, label.frame.size.height + 3)];
    [self.scrollView addSubview:imageViewBg];
    
  [self.scrollView addSubview:label];
  //[label release];
  
  ProjectLabel *nameLabel = [[ProjectLabel alloc] initWithFrame:CGRectMake(250, 55, 65, 23)];
  [nameLabel makeWithTitleAlignLeft: [NSString stringWithFormat:@"%.2f zł", [self.ticket.price intValue] / 100.0] andSize: 14];
  [self.scrollView addSubview:nameLabel];
  //[nameLabel release];
  
  int cvcPosY = [ProjectViewHelper screenSize].height - 190;
  
  [self createCvcAndChangeCardButton: cvcPosY];
  
  ProjectButton *button = [ProjectButton buttonWithType:UIButtonTypeCustom];
  [button setFrame:CGRectMake(10, cvcPosY + 50, 300, 50)];
//  [button setFrame:CGRectMake(10, [ProjectViewHelper screenSize].height - 105, 300, 50)];
  [button setTitle:@"Kup bilet" forState:UIControlStateNormal];
  [button addImageToTitle:@"kup_bilet.png"];
  [button makeBigButton];
  [button setBackgroundImage:[UIImage imageNamed:@"btn_green.png"] forState:UIControlStateNormal];
  [button setBackgroundImage:[UIImage imageNamed:@"btn_green_pressed.png"] forState:UIControlStateHighlighted];
  [button addTarget:self action:@selector(payButtonClicked:) forControlEvents:UIControlEventTouchDown];
  [self.scrollView addSubview:button];
  [self.scrollView addSubview: [self poweredByOnPosY: button.frame.origin.y + 60]];
  
  [self createLogos:115];
  
  [self createPaymentLabel: MAX(105, imageViewBg.frame.size.height + imageViewBg.frame.origin.y + 15)];
  
  [super viewDidLoad];
}

- (void) viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];
  [self unregisterFromKeyboardNotifications];
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  [self registerForKeyboardNotifications];
  
  if (![self.ticket.securityCodeType isEqualToString:@"PIN"] && ![self.ticket.securityCodeType isEqualToString:@"none"]) {
    NSUserDefaults *p = [NSUserDefaults standardUserDefaults];
    
    if ([p objectForKey:UPAID_CARD_TO_PAY_PAN] == nil) {
      [self.cardNumberLabel setText:@"Brak karty..."];
    } else {
      [self.cardNumberLabel setText:[p objectForKey:UPAID_CARD_TO_PAY_PAN]];
    }
    
    if (![p boolForKey:USER_HAS_CARDS] && ![self.ticket.securityCodeType isEqualToString:@"none"] && ![self.ticket.securityCodeType isEqualToString:@"PIN"]) {
      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:@"Aby zakupić bilet, musisz dodać kartę płatniczą. Czy chcesz to zrobić teraz?" delegate:self cancelButtonTitle:@"Anuluj" otherButtonTitles:@"Dodaj kartę", nil];
      [alert setTag:100];
      [alert show];
      //[alert release];
    }
  }
}

- (void) createPaymentLabel: (int) posY {
  NSString *title = @"";
  NSString *titlePaymentType = @"Sposób płatności:";
  int height = 30;

  if ([self.ticket.securityCodeType isEqualToString:@"PIN"]) {
    height = 70;
    title = @"Płacisz z portmonetki Callpay. Zatwierdź płatność podając swój kod PIN.";
  } else if ([self.ticket.securityCodeType isEqualToString:@"none"]){
    title = @"Płacisz z portmonetki Callpay.";
  } else {
    titlePaymentType = @"Nazwa karty płatniczej:";
  }
  
  ProjectLabel *labelPaymentType = [[ProjectLabel alloc] initWithFrame:CGRectMake(10, posY, 200, 21)];
  [labelPaymentType makeWithTitleAlignLeft:titlePaymentType andSize:16];
  [self.scrollView addSubview:labelPaymentType];
  //[labelPaymentType release];
  
  self.cardNumberLabel = [[ProjectLabel alloc] initWithFrame:CGRectMake(10, posY + 25, 200, height)];
  [self.cardNumberLabel makeWithTitle:title andSize:14];
  [self.cardNumberLabel setBackgroundColor:[UIColor colorWithHexString:@"0c6bad"]];
  [self.cardNumberLabel setTextColor:[UIColor whiteColor]];
  [self.scrollView addSubview:self.cardNumberLabel];
  //[self.cardNumberLabel release];
}

- (void) createCvcAndChangeCardButton: (int) posY {
  if ([self.ticket.securityCodeType isEqualToString:@"none"] || [self.ticket.securityCodeType isEqualToString:@"NONE"]) {
    return;
  }
  
  if ([self.ticket.securityCodeType isEqualToString:@"CVC2"]) {
    self.codeDescription = @"CVC2/CVV2/mPIN";
  } else {
    self.codeDescription = self.ticket.securityCodeType;
  }
  
  NSString *placeholder = [self.codeDescription length] > 4 ? self.codeDescription : [NSString stringWithFormat: @"Kod %@", self.codeDescription];
  
  self.cvcTextField = [[ProjectTextField alloc] initWithPlaceholder:placeholder posY:posY];
  
  if ([self.ticket.securityCodeType isEqualToString:@"CVC2"] || [self.ticket.securityCodeType isEqualToString:@"mPIN"]) {
    [self.cvcTextField setHelp:@"CVC2/CVV2 - trzycyfrowy kod z tyłu karty\n\nmPIN - kod służący do zatwierdzania transakcji mobilnych i internetowych"];
  }
  
  if ([self.ticket.securityCodeValueType isEqualToString:@"NUMERIC"]) {
    [self.cvcTextField setKeyboardType:UIKeyboardTypeNumberPad];
  }
  
  [self.cvcTextField setDelegate:self];
  [self.cvcTextField setFrame: CGRectMake(10, posY, 140, 35)];
  [self.scrollView addSubview:self.cvcTextField];
  
  if (![self.ticket.securityCodeType isEqualToString:@"PIN"]) {
    ProjectButton *changeCardButton = [[ProjectButton alloc] initWithTitle:@"Zmień kartę" imageName:@"btn_zmien_karte" fontSize:16];
    [changeCardButton setFrame:CGRectMake(160, posY, 150, 35)];
    [changeCardButton addImageToTitle:@"zmien_karte"];
    [changeCardButton addTarget:self action:@selector(changeCard:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:changeCardButton];
    //[changeCardButton release];
  }
}

- (void) createLogos: (int) posY {
//  UIImageView *ecardView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"koszyk_logo_ecard.png"]];
//  [ecardView setFrame:CGRectMake(235, posY, 28, 32)];
//  [self.scrollView addSubview:ecardView];
  
//  UIImageView *masterLogo = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"koszyk_logo_mcm.png"]];
//  [masterLogo setFrame:CGRectMake(270, posY, 41, 32)];
//  [self.scrollView addSubview:masterLogo];
}

#pragma mark buttons management

-(IBAction)payButtonClicked: (id) sender {
  [self.view endEditing:YES];
  if ([[self.cvcTextField text] isEqualToString:@""] && ![self.ticket.securityCodeType isEqualToString:@"none"]) {
    [ProjectViewHelper showInfoAlert:[NSString stringWithFormat: @"Podaj kod %@", self.codeDescription]];
  } else {
    [self.loginController startAuthAction:@"callpayInit"];
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient networkIndicator:YES];
  }
}

-(IBAction)changeCard:(id)sender {
  CardsListViewController *lcc = [[CardsListViewController alloc] initFrom: CardListFromBuyView];
  [self.navigationController pushViewController:lcc animated:NO];
  //[lcc release];
}

#pragma mark uitextfield management
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
  if (range.length == 1) {
    return YES;
  }
  
  int codeLength = [self.ticket.securityCodeValueLength intValue];
  
  if ([self.ticket.securityCodeType isEqualToString:@"CVC2"]) {
    codeLength = 4;
  }
  
  if (textField.text.length == codeLength) {
    return NO;
  }
  
  return YES;
}

#pragma mark network management

-(void) authActionProcess:(NSString *)arg {
  NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
  NSString *cardId = [NSString stringWithFormat:@"%@", [prefs valueForKey:UPAID_CARD_TO_PAY_ID]];
  
  if ([arg isEqualToString:@"buy"]) {
    UPaidMyWsdl *s = [UPaidMyWsdl service];
    
    if ([self.ticket.securityCodeType isEqualToString: @"none"] || [self.ticket.securityCodeType isEqualToString: @"PIN"]) {
      cardId = @"-1";
    }
    
    UPaidbuyData *bd = [[UPaidbuyData alloc] init];
    [bd setAmount:0.0];
    [bd setCvc2:[self.cvcTextField text]];
    [bd setMerchant:@"callpay"];
    [bd setCard:cardId];
    [bd setItem_id: self.ticket.productCode];
    [bd setData:self.ticket.sessionToken];
    [s buy:self action:@selector(buy:) token:[UPaidLogInController presentToken] transactionData:bd force:YES];
    //[bd release];
  } else if ([arg isEqualToString:@"callpayInit"]) {
    if (!CP_API_IS_WORKING) {
      [self.loginController startAuthAction:@"buy"];
    } else {
      if ([self.ticket.securityCodeType isEqualToString: @"none"] || [self.ticket.securityCodeType isEqualToString: @"PIN"]) {
        cardId = @"";
      }
      
      UPaidMyWsdl *wsdl = [UPaidMyWsdl service];
      [wsdl callPayInitializePayment:self action:@selector(callpayInit:) token:[UPaidLogInController presentToken] productCode:self.ticket.productCode cardId:cardId];
    }
  }
}

-(void) callpayInit:(id) value {
  if([value isKindOfClass:[NSError class]]) {
    [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", value] afterDelay:3.0f];
		return;
  }
  
  if([value isKindOfClass:[SoapFault class]]) {
    [SVProgressHUD dismissWithError:[value string]];
    return;
  }
  
  UPaidcallPayInitializePaymentResult * result = (UPaidcallPayInitializePaymentResult *) value;
  
  if ([result status] == 1) {
    [self.ticket setSecurityCodeType:[result securityCodeType]];
    [self.ticket setSecurityCodeValueLength:[result securityCodeValueLength]];
    [self.ticket setSecurityCodeValueType:[result securityCodeValueType]];
    [self.ticket setTransactionId:[result transactionId]];
    [self.ticket setPrice:[NSNumber numberWithInt:[result amount]]];
    [self.ticket setSessionToken:[result sessionToken]];
    
    [self.loginController startAuthAction:@"buy"];
  } else {
    [SVProgressHUD dismissWithError:@"Błąd techniczny, spróbuj później." afterDelay:3.0f];
  }
}

-(void) buy:(id) value {
	if([value isKindOfClass:[NSError class]]) {
    [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", value] afterDelay:3.0f];
		return;
  }
  
  if([value isKindOfClass:[SoapFault class]]) {
    [SVProgressHUD dismissWithError:[value string]];
    return;
  }
  
  UPaidbuyResult *result = (UPaidbuyResult *) value;
  [self.ticket setPurchaseDate:[NSDate date]];

  AppDelegate *appDelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
  
  switch ([result status]) {
    case 1:
      {
      [SVProgressHUD dismiss];
      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:@"Bilet kupiony pomyślnie." delegate:self cancelButtonTitle:@"OK!" otherButtonTitles: nil];
      [self.ticket setStatus:@(1)];
      [alert show];
      //[alert release];
      break;
      }
    default:
      if (CP_API_IS_WORKING) {
//        [[appDelegate managedObjectContext] deleteObject:self.ticket];
      }
      
      [SVProgressHUD dismissWithSuccess:[ProjectViewHelper statusSubstatusInfo:[result status] substatus:[result substatus]] afterDelay:4.0f];
      break;
  }
  
  
  NSError *error = nil;
  [[appDelegate managedObjectContext] save:&error];
}

#pragma mark alert management

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
  if (alertView.tag == 100) {
    if (buttonIndex == 0) {
      [self.navigationController popViewControllerAnimated:YES];
    } else {
      AddCardViewController *rc = [[AddCardViewController alloc] initWithState:ADD_CARD_FIRST];
      [self.navigationController pushViewController:rc animated:NO];
      //[rc release];
    }
  } else {
      NSArray *viewControllers = [self.navigationController.viewControllers subarrayWithRange: NSMakeRange(0, 2)];
      [self.navigationController setViewControllers:viewControllers animated:YES];
  }
}

- (int) keyboardShift {
  return 60;
}

@end
