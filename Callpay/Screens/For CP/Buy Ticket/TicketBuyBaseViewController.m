//
//  TicketBuyBaseViewController.m
//  CallPay
//
//  Created by uPaid on 17.06.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "TicketBuyBaseViewController.h"
#import "ProjectLabel.h"
#import "ProjectButton.h"
#import "ProjectViewHelper.h"
#import "TicketBuyLastStepViewController.h"
#import "Ticket+Extensions.h"
#import "AppDelegate.h"

#import "BuyTicketCell.h"
#import "TicketGroupCell.h"
#import "TicketPayViewController.h"

@interface TicketBuyBaseViewController ()

@end

@implementation TicketBuyBaseViewController

- (id)initWithDictionary: (NSDictionary *) _dict andTicket: (Ticket *) newTicket {
    self = [super init];
  
    if (self) {
      self.ticket = newTicket;
      self.dataToProcess = _dict;
      self.allDataKeys = [[self.dataToProcess allKeys] sortedArrayUsingSelector:@selector(localizedStandardCompare:)];
      self.filteredDataKeys = [[NSMutableArray alloc] init];
      isFiltered = NO;
      
      if (!CP_API_IS_WORKING) {
        self.testSelectedDataIndex = [NSString stringWithFormat:@"%ld", (long)1];
      }
    }
  
    return self;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
  [self.searchBar resignFirstResponder];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSLog(@"class %@",[self class]);
}

- (void)viewDidLoad
{NSLog(@"class %@",[self class]);
  
  
    if([self isMemberOfClass:NSClassFromString(@"TicketBuyThirdStepViewController")])
    {
        NSLog(@"%@",self.dataToProcess);
        self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 55, [[UIScreen mainScreen] applicationFrame].size.width, 40)];
        [self.searchBar setBackgroundImage:[UIImage new]];
        [self.searchBar setTranslucent:YES];
        [self.searchBar setDelegate:self];
        [self.view addSubview:self.searchBar];
        float endSearchBarPosY = self.searchBar.frame.origin.y + self.searchBar.frame.size.height;
        
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(16, endSearchBarPosY, self.view.bounds.size.width - 32, [self getWindowContentHeight] - endSearchBarPosY - 20)];
        [self.view addSubview:self.tableView];
        [self.tableView setDataSource:self];
        [self.tableView setDelegate:self];
        self.tableView.backgroundColor = [UIColor clearColor];
        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        
        [self.tableView registerNib:[UINib nibWithNibName:@"BuyTicketCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TicketCell"];
    }
    else if ([self isMemberOfClass:NSClassFromString(@"TicketBuyFirstStepViewController")] || [self isMemberOfClass:NSClassFromString(@"TicketBuySecondStepViewController")])
    {
        self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 55, [[UIScreen mainScreen] applicationFrame].size.width, 40)];
        [self.searchBar setBackgroundImage:[UIImage new]];
        [self.searchBar setTranslucent:YES];
        [self.searchBar setDelegate:self];
        [self.view addSubview:self.searchBar];
        float endSearchBarPosY = self.searchBar.frame.origin.y + self.searchBar.frame.size.height;
        
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(16, endSearchBarPosY, self.view.bounds.size.width - 32, [self getWindowContentHeight] - endSearchBarPosY - 20)];
        [self.view addSubview:self.tableView];
        [self.tableView setDataSource:self];
        [self.tableView setDelegate:self];
        self.tableView.backgroundColor = [UIColor clearColor];
        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        
        [self.tableView registerNib:[UINib nibWithNibName:@"TicketGroupCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TicketGroupCell"];
    }
    else
    {
        self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 55, [[UIScreen mainScreen] applicationFrame].size.width, 40)];
        [self.searchBar setBackgroundImage:[UIImage new]];
        [self.searchBar setTranslucent:YES];
        [self.searchBar setDelegate:self];
        [self.view addSubview:self.searchBar];
        float endSearchBarPosY = self.searchBar.frame.origin.y + self.searchBar.frame.size.height;
  [self setupTableView: CGRectMake(0, endSearchBarPosY, [[UIScreen mainScreen] applicationFrame].size.width, [[UIScreen mainScreen] applicationFrame].size.height - self.navigationController.navigationBar.frame.size.height - endSearchBarPosY)];
//  [self.view addSubview:self.searchBar];
  
  [self.view addSubview: self.tableView];
    
    [self.tableView performSelector: @selector(reloadData) withObject:nil afterDelay:2.1f];
    }
  [super viewDidLoad];
}

-(void) didTapAnywhere: (UITapGestureRecognizer*) recognizer {
  [self.view endEditing:YES];
  [self.tableView endEditing:YES];
  [self.searchBar resignFirstResponder];
}

-(NSInteger) getRowsCount {
  return [[self dataKeysToShow] count];
}

- (NSString *) getTableTitle {
  return @"";
}

- (UIView *) getCellContent: (NSIndexPath *)indexPath {
  UIView *content = [[UIView alloc] init];
  
  ProjectLabel *nameLabel = [[ProjectLabel alloc] initWithFrame:CGRectMake(15, 7, 255, 22.5)];
  [nameLabel makeWithTitleAlignLeft: [[self dataKeysToShow] objectAtIndex:indexPath.row] andSize: 14];
  [nameLabel setupLabelHeight];
  [self setCellHeight:nameLabel.frame.size.height + 19 forRowAtIndexPath:indexPath];
  [content addSubview:nameLabel];
  //[nameLabel release];
    
    [self setupCellContent: content forIndexPath: indexPath];
  
  UIButton *selectRowButton = [UIButton buttonWithType:UIButtonTypeCustom];
  [selectRowButton setFrame:CGRectMake(0, 0, [ProjectViewHelper screenSize].width, [self contentHeightForIndexPath: indexPath])];
  [selectRowButton setTag:indexPath.row];
  
  [selectRowButton setImage:[UIImage imageNamed:@"btn_arrow2"] forState:UIControlStateNormal];
  [selectRowButton setImage:[UIImage imageNamed:@"btn_arrow2_pressed"] forState:UIControlStateHighlighted];
  [selectRowButton setImageEdgeInsets:UIEdgeInsetsMake(3, 250, 3, 15)];
  
  [selectRowButton addTarget:self action:@selector(buttonSelectPressed:) forControlEvents:UIControlEventTouchUpInside];
  [content addSubview:selectRowButton];
  
  return content;
}

- (void) setupCellContent: (UIView *) content forIndexPath: (NSIndexPath *)indexPath {
    
}


- (NSString *) nextStepClass {
  return @"";
}

- (TicketBuyBaseViewController *) nextStep: (NSString *) dataIndex  {
  return [[NSClassFromString([self nextStepClass]) alloc] initWithDictionary: [self.dataToProcess objectForKey:dataIndex] andTicket: self.ticket];
}

- (void) goToNextStep: (NSString *) dataIndex {
  TicketBuyBaseViewController *view = [[self nextStep:dataIndex] nextStepIfNeeded];
  
  [self.navigationController pushViewController:view animated:YES];
}

- (void) buttonSelectPressed: (id) sender {
  [self.view endEditing:YES];
  [self goToNextStep:[[self dataKeysToShow] objectAtIndex:[sender tag]]];
}

- (TicketBuyBaseViewController *) nextStepIfNeeded {
  if ([[self dataKeysToShow] count] == 1) {
    return [[self nextStep:[[self dataKeysToShow] lastObject]] nextStepIfNeeded];
  }
  
  return self;
}

#pragma mark searchBar

- (NSArray *) dataKeysToShow {
  return isFiltered ? self.filteredDataKeys : self.allDataKeys;
}

-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
  if(text.length == 0) {
    isFiltered = NO;
  } else {
    isFiltered = YES;
    [self.filteredDataKeys removeAllObjects];
    
    for (NSString* actualKey in self.allDataKeys) {
      if([actualKey rangeOfString:text options:NSCaseInsensitiveSearch].location != NSNotFound) {
        [self.filteredDataKeys addObject:actualKey];
      }
    }
  }
  
  [self.tableView reloadData];
}

#pragma mark test alert management

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
  if (buttonIndex != 0) {
    self.testSelectedDataIndex = [NSString stringWithFormat:@"%ld", (long)buttonIndex];
    [self startCallpayInit];
  }
}

#pragma mark network management

- (void) goToLastStep: (NSString *) dataIndex{
  [self goToLastStep:dataIndex orDictionary:nil];
}
  
- (void) goToLastStep: (NSString *) dataIndex orDictionary: (NSDictionary *) dict {
  self.selectedDataIndex = dataIndex;
  self.selectedDictionary = dict;

  if (CP_API_IS_WORKING) {
    [self startCallpayInit];
  } else {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:@"Rodzaj ekranu zakupu" delegate:self cancelButtonTitle:@"Anuluj" otherButtonTitles:@"CVC2", @"PIN", @"nic", @"mPIN literowy", nil];
    [alert show];
    //[alert release];
  }
}

- (void) startCallpayInit {
  UPaidLogInController *loginController = [[UPaidLogInController alloc] initWithDelegate:self];
  [loginController startAuthAction:@"callpayInit"];
  [SVProgressHUD showWithStatus:@"Proszę czekać..." maskType:SVProgressHUDMaskTypeGradient];
}

-(void) authActionProcess:(NSString *)arg {
  UPaidMyWsdl *wsdl = [UPaidMyWsdl service];
  if (self.selectedDataIndex != nil) { //dla kupowania z historii nie trzeba tego robic
    [self.ticket setProductCode:[[self selectedDictionaryForInit] valueForKey:@"product_code"]];
    NSString *verificationPhone = [[self selectedDictionaryForInit] valueForKey:@"verification_phone"];
    [self.ticket setVerificationTime: [[self selectedDictionaryForInit] valueForKey:@"verification_time"]];
    
    if (verificationPhone != nil && ![verificationPhone isEqualToString:@"-1"]) {
      [self.ticket setVerificationPhone:verificationPhone];
    }
  }
  
  if (CP_API_IS_WORKING) {
    [wsdl callPayInitializePayment:self action:@selector(callpayInit:) token:[UPaidLogInController presentToken] productCode:self.ticket.productCode cardId:@""];
  } else {
    [wsdl callPayInitializePayment:self action:@selector(callpayInit:) token:[UPaidLogInController presentToken] productCode:self.testSelectedDataIndex cardId:@""];
  }
}

- (NSDictionary *) selectedDictionaryForInit {
  if ([self.selectedDataIndex isEqualToString:@""]) {
    return self.selectedDictionary;
  } else {
    return [self.dataToProcess objectForKey:self.selectedDataIndex];
  }
}

-(void) callpayInit:(id) value {
  if([value isKindOfClass:[NSError class]]) {
    [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", value] afterDelay:3.0f];
		return;
  }
  
  if([value isKindOfClass:[SoapFault class]]) {
    [SVProgressHUD dismissWithError:[value string]];
    return;
  }
  
  [SVProgressHUD dismiss];
  UPaidcallPayInitializePaymentResult * result = (UPaidcallPayInitializePaymentResult *) value;
  
  if ([result status] == 1) {
    [self.ticket setSecurityCodeType:[result securityCodeType]];
    [self.ticket setSecurityCodeValueLength:[result securityCodeValueLength]];
    [self.ticket setSecurityCodeValueType:[result securityCodeValueType]];
    [self.ticket setTransactionId:[result transactionId]];
    [self.ticket setPrice:[NSNumber numberWithInt:[result amount]]];
    [self.ticket setSessionToken:[result sessionToken]];
    
    TicketBuyLastStepViewController *viewController = [[TicketBuyLastStepViewController alloc] initWithTicket: self.ticket];
    
    if (self.customNavigationController == nil) {
        [self.navigationController pushViewController:viewController animated:YES];
    } else {
      [self.customNavigationController pushViewController:viewController animated:YES];
    }
    
    //[viewController release];
  } else {
    [SVProgressHUD show];
    [SVProgressHUD dismissWithError:@"Błąd techniczny, spróbuj później." afterDelay:3.0f];
  }
  
}
/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self getRowsCount];
}


- (UITableViewCell *)tableView:(UITableView *)table cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"TicketCell";
//    NSString *cellIdentifier = [NSString stringWithFormat:@"TicketCell", indexPath.row];
    BuyTicketCell *cell = [table dequeueReusableCellWithIdentifier:cellIdentifier];
    
//    if (cell == nil || IS_IOS7_OR_LATER)
    if (cell == nil)
    {
        cell = [[BuyTicketCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
//    else {
//        if ([cell subviews]){
//            for (UIView *subview in [cell subviews]) {
//                [subview removeFromSuperview];
//            }
//        }
//    }
    
    //  static NSString *CellIdentifier = @"Cell";
    //
    //  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //
    //  if (cell == nil) {
    //    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    //  }
    
    cell.selectedBackgroundView.backgroundColor=[UIColor clearColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
//    cell.textLabel.text = [[self dataKeysToShow] objectAtIndex:indexPath.row];
    NSDictionary *dict = (NSDictionary*)[self.dataToProcess valueForKey:[[self dataKeysToShow] objectAtIndex:indexPath.row]];
//    cell.detailTextLabel.text = [dict valueForKey:@"price"];
//        cell.textLabel.text = [cell.textLabel.text stringByAppendingString: [NSString stringWithFormat:@" %@",[dict valueForKey:@"price"]]];
    cell.nameLabel.text = [[self dataKeysToShow] objectAtIndex:indexPath.row];
//    [cell.nameLabel setTextColor:[UIColor redColor]];
//    cell.priceLabel.text = [dict valueForKey:@"price"];
    [cell setPrice:[dict valueForKey:@"price"]];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tabView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 78;
}
- (NSIndexPath *)tableView:(UITableView *)tabView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    TicketPayViewController *vc = (TicketPayViewController*) [storyboard instantiateViewControllerWithIdentifier:@"PayTicketVC"];
    NSDictionary *dict = (NSDictionary*)[self.dataToProcess valueForKey:[[self dataKeysToShow] objectAtIndex:indexPath.row]];
    vc.ticketName = [[self dataKeysToShow] objectAtIndex:indexPath.row];
    vc.price = [[dict valueForKey:@"price"] floatValue];
    vc.productCode = [dict valueForKey:@"product_code"];
    [self.view endEditing:YES];
//    [self goToNextStep:[[self dataKeysToShow] objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:vc animated:YES];
    return indexPath;
}
*/

@end
