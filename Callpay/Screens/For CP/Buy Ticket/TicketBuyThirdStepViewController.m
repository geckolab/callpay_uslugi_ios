//
//  TicketBuyThirdStepViewController.m
//  CallPay
//
//  Created by uPaid on 17.06.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "TicketBuyThirdStepViewController.h"
#import "ProjectLabel.h"

#import "BuyTicketCell.h"

#import "TicketPayViewController.h"

@implementation TicketBuyThirdStepViewController


- (void)viewDidLoad
{
  [super viewDidLoad];
  
  isBigTable = YES;
}
- (void) setupCellContent: (UIView *) content forIndexPath: (NSIndexPath *)indexPath {
  
  ProjectLabel *nameLabel = [[ProjectLabel alloc] initWithFrame:CGRectMake(15, 25, 260, 22.5)];
  NSNumber *price = [[self.dataToProcess objectForKey:[[self dataKeysToShow] objectAtIndex:indexPath.row]] valueForKey: @"price"];
  [nameLabel makeWithTitleAlignLeft: [NSString stringWithFormat:@"%.2f zł", [price intValue] / 100.0] andSize: 14];
  [content addSubview:nameLabel];
  //[nameLabel release];
    [self setCellHeight: 58 forRowAtIndexPath:indexPath];
}

- (TicketBuyBaseViewController *) nextStep: (NSString *) dataIndex {
  [self.ticket setProductName:dataIndex];
  return [super nextStep:dataIndex];
}


- (void) goToNextStep: (NSString *) dataIndex {
  [self.ticket setProductName:dataIndex];
  [self goToLastStep:dataIndex];
}

- (TicketBuyBaseViewController *) nextStepIfNeeded {
  return self;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self getRowsCount];
}


- (UITableViewCell *)tableView:(UITableView *)table cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"TicketCell";
    //    NSString *cellIdentifier = [NSString stringWithFormat:@"TicketCell", indexPath.row];
    BuyTicketCell *cell = [table dequeueReusableCellWithIdentifier:cellIdentifier];
    
    //    if (cell == nil || IS_IOS7_OR_LATER)
    if (cell == nil)
    {
        cell = [[BuyTicketCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    //    else {
    //        if ([cell subviews]){
    //            for (UIView *subview in [cell subviews]) {
    //                [subview removeFromSuperview];
    //            }
    //        }
    //    }
    
    //  static NSString *CellIdentifier = @"Cell";
    //
    //  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //
    //  if (cell == nil) {
    //    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    //  }
    
    cell.selectedBackgroundView.backgroundColor=[UIColor clearColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    //    cell.textLabel.text = [[self dataKeysToShow] objectAtIndex:indexPath.row];
    NSDictionary *dict = (NSDictionary*)[self.dataToProcess valueForKey:[[self dataKeysToShow] objectAtIndex:indexPath.row]];
    //    cell.detailTextLabel.text = [dict valueForKey:@"price"];
    //        cell.textLabel.text = [cell.textLabel.text stringByAppendingString: [NSString stringWithFormat:@" %@",[dict valueForKey:@"price"]]];
    cell.nameLabel.text = [[self dataKeysToShow] objectAtIndex:indexPath.row];
    //    [cell.nameLabel setTextColor:[UIColor redColor]];
    //    cell.priceLabel.text = [dict valueForKey:@"price"];
    [cell setPrice:[dict valueForKey:@"price"]];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tabView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 78;
}
- (NSIndexPath *)tableView:(UITableView *)tabView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    TicketPayViewController *vc = (TicketPayViewController*) [storyboard instantiateViewControllerWithIdentifier:@"PayTicketVC"];
    NSDictionary *dict = (NSDictionary*)[self.dataToProcess valueForKey:[[self dataKeysToShow] objectAtIndex:indexPath.row]];
    
    if ([dict objectForKey:@"-1"] != nil)
    {
        dict = [dict objectForKey:@"-1"];
    }
    
    [self.ticket setPrice:[NSNumber numberWithInt:[[dict valueForKey:@"price"] integerValue]]];
    [self.ticket setProductCode:[dict valueForKey:@"product_code"]];
    [self.ticket setVerificationTime:[dict valueForKey:@"verification_time"]];
    [self.ticket setName:[[self dataKeysToShow] objectAtIndex:indexPath.row]];
    NSString *verificationPhone = [dict valueForKey:@"verification_phone"];
    if (verificationPhone != nil && ![verificationPhone isEqualToString:@"-1"]) {
        [self.ticket setVerificationPhone:verificationPhone];
    }
    
//    vc.ticketName = [[self dataKeysToShow] objectAtIndex:indexPath.row];
//    vc.price = [[dict valueForKey:@"price"] floatValue];
//    vc.productCode = [dict valueForKey:@"product_code"];
    [vc setupWithTicket:self.ticket];
    [self.view endEditing:YES];
    //    [self goToNextStep:[[self dataKeysToShow] objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:vc animated:YES];
    return indexPath;
}
- (CGFloat)tableView:(UITableView *)tabView heightForHeaderInSection:(NSInteger)section
{
    return 2;
}
- (UIView *)tableView:(UITableView *)tabView viewForHeaderInSection:(NSInteger)section
{
    UIView *hv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tabView.frame.size.width, 2)];
    [hv setBackgroundColor:[UIColor clearColor]];
    return hv;
}


@end
