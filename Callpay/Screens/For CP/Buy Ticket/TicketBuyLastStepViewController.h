//
//  TicketBuyLastStepViewController.h
//  CallPay
//
//  Created by uPaid on 17.06.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "ProjectViewController.h"
#import "UPaidcallPayInitializePaymentResult.h"
#import "ProjectTextField.h"
#import "ProjectLabel.h"
#import "Ticket+Extensions.h"
#import "UPaidLogInController.h"

@interface TicketBuyLastStepViewController : ProjectViewController

@property (retain, nonatomic) Ticket *ticket;
@property (retain, nonatomic) ProjectTextField* cvcTextField;
@property (retain, nonatomic) ProjectLabel *cardNumberLabel;
@property (retain, nonatomic) UPaidLogInController *loginController;

- (id) initWithTicket: (Ticket *) __ticket;

@end
