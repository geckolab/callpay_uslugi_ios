//
//  TicketBuyBaseViewController.h
//  CallPay
//
//  Created by uPaid on 17.06.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "ProjectViewController.h"
#import "UPaidMyWsdl.h"
#import "UPaidLogInController.h"
#import "Ticket+Extensions.h"

@interface TicketBuyBaseViewController : ProjectViewController <UIAlertViewDelegate, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate> {
  BOOL isFiltered;
  NSManagedObjectContext *context;
}

@property (nonatomic, retain) NSDictionary *dataToProcess;
@property (nonatomic, retain) UISearchBar *searchBar;
@property (nonatomic, retain) NSArray *allDataKeys;
@property (nonatomic, retain) NSMutableArray *filteredDataKeys;
@property (nonatomic, retain) NSString *selectedDataIndex;
@property (nonatomic, retain) NSDictionary *selectedDictionary;
@property (nonatomic, retain) NSString *testSelectedDataIndex;
@property (nonatomic, retain) Ticket *ticket;

- (id)initWithDictionary: (NSDictionary *) _dict andTicket: (Ticket *) newTicket;
- (NSString *) nextStepClass;
- (TicketBuyBaseViewController *) nextStepIfNeeded;
- (TicketBuyBaseViewController *) nextStep: (NSString *) dataIndex;
- (void) goToNextStep: (NSString *) dataIndex;
- (void) goToLastStep: (NSString *) dataIndex;
- (void) goToLastStep: (NSString *) dataIndex orDictionary: (NSDictionary *) dict;
- (NSArray *) dataKeysToShow;
- (void) startCallpayInit;
- (void) setupCellContent: (UIView *) content forIndexPath: (NSIndexPath *)indexPath;

@end
