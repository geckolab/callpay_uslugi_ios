//
//  TicketBuyFirstStepViewController.m
//  CallPay
//
//  Created by uPaid on 17.06.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "TicketBuyFirstStepViewController.h"


@interface TicketBuyFirstStepViewController ()

@end

@implementation TicketBuyFirstStepViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (NSString *) nextStepClass {
  return @"TicketBuySecondStepViewController";
}

- (TicketBuyBaseViewController *) nextStep: (NSString *) dataIndex {
  [self.ticket setSeller:dataIndex];
  return [super nextStep:dataIndex];
}
- (CGFloat)tableView:(UITableView *)tabView heightForHeaderInSection:(NSInteger)section
{
    return 2;
}
- (UIView *)tableView:(UITableView *)tabView viewForHeaderInSection:(NSInteger)section
{
    UIView *hv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tabView.frame.size.width, 2)];
    [hv setBackgroundColor:[UIColor clearColor]];
    return hv;
}
- (CGFloat)tableView:(UITableView *)tabView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}
- (UITableViewCell *)tableView:(UITableView *)table cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"TicketGroupCell";
    //    NSString *cellIdentifier = [NSString stringWithFormat:@"TicketCell", indexPath.row];
    TicketGroupCell *cell = [table dequeueReusableCellWithIdentifier:cellIdentifier];
    
    //    if (cell == nil || IS_IOS7_OR_LATER)
    if (cell == nil)
    {
        cell = [[TicketGroupCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    cell.groupNameLabel.text = [[self dataKeysToShow] objectAtIndex:indexPath.row];

    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self getRowsCount];
}
- (NSIndexPath *)tableView:(UITableView *)tabView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    [self goToNextStep:[[self dataKeysToShow] objectAtIndex:indexPath.row]];
    return indexPath;
}

@end
