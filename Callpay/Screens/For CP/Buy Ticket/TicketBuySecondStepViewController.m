//
//  TicketBuySecondStepViewController.m
//  CallPay
//
//  Created by uPaid on 17.06.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "TicketBuySecondStepViewController.h"

#import "TicketPayViewController.h"

@interface TicketBuySecondStepViewController ()

@end

@implementation TicketBuySecondStepViewController

- (NSString *) nextStepClass {
  return @"TicketBuyThirdStepViewController";
}

- (TicketBuyBaseViewController *) nextStep: (NSString *) dataIndex {
  [self.ticket setName:dataIndex];
  return [super nextStep:dataIndex];
}

- (void) goToNextStep: (NSString *) dataIndex {
  if ([[self.dataToProcess objectForKey:dataIndex] count] == 1) {
//    [self.ticket setName:dataIndex];
//    NSDictionary *dict = [self.dataToProcess objectForKey:dataIndex];
//    [self goToLastStep: @"" orDictionary: [dict objectForKey: [[dict allKeys] lastObject]]];
//      [super goToNextStep: dataIndex];
      
  } else {
    [super goToNextStep: dataIndex];
  }
}
- (CGFloat)tableView:(UITableView *)tabView heightForHeaderInSection:(NSInteger)section
{
    return 2;
}
- (UIView *)tableView:(UITableView *)tabView viewForHeaderInSection:(NSInteger)section
{
    UIView *hv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tabView.frame.size.width, 2)];
    [hv setBackgroundColor:[UIColor clearColor]];
    return hv;
}
- (CGFloat)tableView:(UITableView *)tabView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}
- (UITableViewCell *)tableView:(UITableView *)table cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"TicketGroupCell";
    //    NSString *cellIdentifier = [NSString stringWithFormat:@"TicketCell", indexPath.row];
    TicketGroupCell *cell = [table dequeueReusableCellWithIdentifier:cellIdentifier];
    
    //    if (cell == nil || IS_IOS7_OR_LATER)
    if (cell == nil)
    {
        cell = [[TicketGroupCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    cell.groupNameLabel.text = [[self dataKeysToShow] objectAtIndex:indexPath.row];

    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self getRowsCount];
}
- (NSIndexPath *)tableView:(UITableView *)tabView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    if ([[self.dataToProcess objectForKey:[[self dataKeysToShow] objectAtIndex:indexPath.row]] count] == 1)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        TicketPayViewController *vc = (TicketPayViewController*) [storyboard instantiateViewControllerWithIdentifier:@"PayTicketVC"];
        NSDictionary *dict = (NSDictionary*)[self.dataToProcess valueForKey:[[self dataKeysToShow] objectAtIndex:indexPath.row]];
//        NSLog(@"%@",self.dataToProcess);
//        NSLog(@"%@",dict);
        if ([dict objectForKey:@"-1"] != nil)
        {
            dict = [dict objectForKey:@"-1"];
        }
        
        [self.ticket setPrice:[NSNumber numberWithInt:[[dict valueForKey:@"price"] integerValue]]];
        [self.ticket setProductCode:[dict valueForKey:@"product_code"]];
        [self.ticket setVerificationTime:[dict valueForKey:@"verification_time"]];
        [self.ticket setName:[[self dataKeysToShow] objectAtIndex:indexPath.row]];
        NSString *verificationPhone = [dict valueForKey:@"verification_phone"];
        if (verificationPhone != nil && ![verificationPhone isEqualToString:@"-1"]) {
            [self.ticket setVerificationPhone:verificationPhone];
        }
        
        //    vc.ticketName = [[self dataKeysToShow] objectAtIndex:indexPath.row];
        //    vc.price = [[dict valueForKey:@"price"] floatValue];
        //    vc.productCode = [dict valueForKey:@"product_code"];
        [vc setupWithTicket:self.ticket];
        //    [self goToNextStep:[[self dataKeysToShow] objectAtIndex:indexPath.row]];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
        [self goToNextStep:[[self dataKeysToShow] objectAtIndex:indexPath.row]];
    return indexPath;
}

@end