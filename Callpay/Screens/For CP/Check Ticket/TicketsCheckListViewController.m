//
//  TicketsCheckViewController.m
//  CallPay
//
//  Created by uPaid on 24.06.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "TicketsCheckListViewController.h"
#import "ProjectLabel.h"
#import "AppDelegate.h"
#import "Ticket+Extensions.h"
#import "TicketsChecker.h"
#import "TicketsCheckSingleViewController.h"

#import "ActiveTicketCell.h"

@interface TicketsCheckListViewController ()

@end

@implementation TicketsCheckListViewController

- (id)init{
    self = [super init];
    if (self) {
      AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
      NSManagedObjectContext *context = [appDelegate managedObjectContext];
      
      NSEntityDescription *eDescription = [NSEntityDescription entityForName:@"Ticket" inManagedObjectContext:context];
      NSFetchRequest *request = [[NSFetchRequest alloc] init];
      [request setEntity:eDescription];
      NSPredicate *datePredicate = [NSPredicate predicateWithFormat: @"(verificationDate != nil AND verificationDate >= %@) OR (verificationDate = nil AND purchaseDate >= %@)", [NSDate date], [NSDate dateWithTimeIntervalSinceNow: -(3600 * 24 * 30)]];
      NSArray *predicatesArray = @[datePredicate, [NSPredicate predicateWithFormat:@"price != 0 and name != nil and status != 0"]];
      [request setPredicate: [NSCompoundPredicate andPredicateWithSubpredicates: predicatesArray]];
      [request setShouldRefreshRefetchedObjects:YES];
      [request setFetchLimit:15];
      NSError *error;
      
      self.ticketsList = [context executeFetchRequest:request error:&error];
    }
  
    return self;
}

- (void)viewDidLoad
{
    //CGRectMake(16, 20+(4*57)+60+90, self.view.bounds.size.width - 32, 65)
    headerView = [[ActiveTicketsHeaderView alloc] initWithFrame:CGRectMake(16, [self getTopBarHeight], self.view.bounds.size.width - 32, 55)];
    [headerView setIcon:[self getTableIcon]];
    [headerView setTitle:[self getTableTitle]];
    [self.view addSubview:headerView];
//  [self setupTableView];
  isBigTable = YES;
//  [self.view addSubview:self.tableView];
  
  
//  ProjectLabel *titleLabel = [[ProjectLabel alloc] initWithFrame:CGRectMake(10, 20, self.view.bounds.size.width - 20, self.view.bounds.size.height - 80)];
//  [titleLabel makeWithTitle: @"W przypadku kontroli, wykonujesz bezpłatne połączenie na numer, który poda Ci kontrolujący. Połączenie to zostanie automatycznie rozłączone a po kilku sekundach otrzymasz SMS z pełnymi danymi biletu. \n\n W przypadku nieotrzymania zwrotnej wiadomości SMS w przeciągu 30 sekund, wykonaj ponowne połączenie na wskazany przez kontrolera numer." andSize:18];
//  [self.view addSubview:titleLabel];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(16, headerView.frame.origin.y + headerView.frame.size.height, self.view.bounds.size.width - 32, [self getWindowContentHeight] - headerView.frame.size.height - 20)];
    
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ActiveTicketCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"ActiveTicketCell"];
    
    [self.view addSubview:self.tableView];
  
  [super viewDidLoad];
}

-(NSInteger) getRowsCount {
  return [self.ticketsList count];
}

- (NSString *) getTableTitle {
//  return @"Kontrola biletów";
    return @"Aktywne bilety";
}

- (UIImage *) getTableIcon {
    return [UIImage imageNamed:@"new_aktywne_bilety"];
}

- (UIView *) getCellContent: (NSIndexPath *)indexPath {
  UIView *content = [[UIView alloc] init];
  Ticket *ticket = [self ticketAtRow: indexPath.row];
//  TFLog(@"HistoryTicket %d: %@", indexPath.row, ticket);
  
  ProjectLabel *nameLabel = [[ProjectLabel alloc] initWithFrame:CGRectMake(15, 8, 260, 22.5)];
  [nameLabel makeWithTitleAlignLeft: ticket.name andSize: 14];
    [nameLabel setupLabelHeight];
  [content addSubview:nameLabel];
  
  ProjectLabel *createdLabel = [[ProjectLabel alloc] initWithFrame:CGRectMake(87, nameLabel.frame.origin.y + nameLabel.frame.size.height + 3, 200, 22.5)];
    [self setCellHeight: createdLabel.frame.origin.y + createdLabel.frame.size.height + 12 forRowAtIndexPath:indexPath];
  
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"HH:mm dd.MM.yyyy"];
  
  if (ticket.verificationDate == nil) {
    NSString *labelText = [NSString stringWithFormat: @"Zakupiony: %@", [dateFormatter stringFromDate:ticket.purchaseDate]];
    [createdLabel makeLightWithTitleAlignRight: labelText andSize: 11];
  } else {
    NSString *labelText = [NSString stringWithFormat: @"Ważny do: %@", [dateFormatter stringFromDate:ticket.verificationDate]];
    [createdLabel makeWithBoldTitleAlignRight: labelText andSize: 11];
    [createdLabel setFont: [UIFont boldSystemFontOfSize: 11]];
  }
  
  [content addSubview:createdLabel];
  //[createdLabel release];
    //[nameLabel release];
  
  return content;
}

- (Ticket *) ticketAtRow: (NSInteger) row {
  return (Ticket *) [self.ticketsList objectAtIndex:[self.ticketsList count] - 1 - row];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [[[TicketsChecker alloc] initWithTicket: self.ticketsList[indexPath.row]] checkTicketFromPhone];
//TicketsCheckSingleViewController *viewController = [[TicketsCheckSingleViewController alloc] initWithTicket: [self ticketAtRow: indexPath.row]];
//  [self.navigationController pushViewController: viewController animated:YES];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self getRowsCount];
}
- (CGFloat)tableView:(UITableView *)tabView heightForHeaderInSection:(NSInteger)section
{
    return 2;
}
- (UIView *)tableView:(UITableView *)tabView viewForHeaderInSection:(NSInteger)section
{
    UIView *hv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tabView.frame.size.width, 2)];
    [hv setBackgroundColor:[UIColor clearColor]];
    return hv;
}
- (CGFloat)tableView:(UITableView *)tabView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}
- (UITableViewCell *)tableView:(UITableView *)table cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"ActiveTicketCell";
    //    NSString *cellIdentifier = [NSString stringWithFormat:@"TicketCell", indexPath.row];
    ActiveTicketCell *cell = [table dequeueReusableCellWithIdentifier:cellIdentifier];
    
    //    if (cell == nil || IS_IOS7_OR_LATER)
    if (cell == nil)
    {
        cell = [[ActiveTicketCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    //    else {
    //        if ([cell subviews]){
    //            for (UIView *subview in [cell subviews]) {
    //                [subview removeFromSuperview];
    //            }
    //        }
    //    }
    
    //  static NSString *CellIdentifier = @"Cell";
    //
    //  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //
    //  if (cell == nil) {
    //    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    //  }
    Ticket *ticket = [self ticketAtRow: indexPath.row];
    
    
    cell.selectedBackgroundView.backgroundColor=[UIColor clearColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    //    cell.textLabel.text = [[self dataKeysToShow] objectAtIndex:indexPath.row];
    //    NSDictionary *dict = (NSDictionary*)[self.dataToProcess valueForKey:[[self dataKeysToShow] objectAtIndex:indexPath.row]];
    //    cell.detailTextLabel.text = [dict valueForKey:@"price"];
    //        cell.textLabel.text = [cell.textLabel.text stringByAppendingString: [NSString stringWithFormat:@" %@",[dict valueForKey:@"price"]]];
    //    cell.nameLabel.text = [[self dataKeysToShow] objectAtIndex:indexPath.row];
    //    [cell.nameLabel setTextColor:[UIColor redColor]];
    //    cell.priceLabel.text = [dict valueForKey:@"price"];
    //    [cell setPrice:[dict valueForKey:@"price"]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm | dd.MM.yyyy"];
//    cell.nameLabel.text = ticket.name;
//    cell.detailTextLabel.text = [dateFormatter stringFromDate:ticket.purchaseDate];
//    cell.buyDateLabel.text = [dateFormatter stringFromDate:ticket.purchaseDate];
    cell.nameLabel.text = ticket.name;
    cell.buyDateLabel.text = [dateFormatter stringFromDate:ticket.purchaseDate];

    return cell;
}

@end
