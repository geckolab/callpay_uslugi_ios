//
//  TicketsCheckSingleViewController.h
//  CallPay
//
//  Created by Michał Majewski on 26.06.2014.
//  Copyright (c) 2014 uPaid. All rights reserved.
//

#import "ProjectViewController.h"
@class Ticket;

@interface TicketsCheckSingleViewController : ProjectViewController

- (id)initWithTicket: (Ticket *) ticket;

@end
