//
//  TicketsCheckViewController.h
//  CallPay
//
//  Created by uPaid on 24.06.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "ProjectViewController.h"
#import "ActiveTicketsHeaderView.h"

@interface TicketsCheckListViewController : ProjectViewController<UIAlertViewDelegate> {
  NSInteger selectedIndex;
    
    ActiveTicketsHeaderView *headerView;
}

@property (nonatomic, retain) NSArray *ticketsList;

@end
