//
//  TicketsCheckSingleViewController.m
//  CallPay
//
//  Created by Michał Majewski on 26.06.2014.
//  Copyright (c) 2014 uPaid. All rights reserved.
//

#import "TicketsCheckSingleViewController.h"
#import "ProjectButton.h"
#import "Ticket.h"
#import "ProjectLabel.h"
#import "UIView+Extensions.h"
#import "UPaidLogInController.h"
#import "TicketsChecker.h"

@interface TicketsCheckSingleViewController ()
@property (retain, nonatomic) IBOutlet ProjectButton *checkTicketButton;
@property (retain, nonatomic) IBOutlet ProjectLabel *ticketDetailsLabel;
@property (retain, nonatomic) IBOutlet ProjectLabel *purchaseDateLabel;
@property (retain, nonatomic) IBOutlet ProjectLabel *verificationDateLabel;
@property (retain, nonatomic) IBOutlet UIImageView *ticketDetailsBackground;

@property (nonatomic) Ticket *ticket;
@property (nonatomic)  UPaidLogInController *upaidLogin;

@end

@implementation TicketsCheckSingleViewController

#pragma mark init

- (id)initWithTicket: (Ticket *) ticket {
  self = [super init];
  
  if (self) {
    self.ticket = ticket;
    self.upaidLogin = [[UPaidLogInController alloc] initWithDelegate:self];
  }
  
  return self;
}

#pragma mark view callbacks

- (void)viewDidLoad {
  [super viewDidLoad];
  [self.checkTicketButton makeBigButton];
  [self.checkTicketButton setBackgroundImage:@"btn_green"];
  
  if (self.ticket.productName != nil) {
    self.ticketDetailsLabel.text = [NSString stringWithFormat:@"%@\n%@", self.ticket.name, self.ticket.productName];
  } else {
    self.ticketDetailsLabel.text = [NSString stringWithFormat:@"%@\n ", self.ticket.name];
    [self.ticketDetailsBackground setFrameHeight: self.ticketDetailsBackground.frame.size.height - 20];
  }
  
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"HH:mm dd.MM.yyyy"];
  
  self.purchaseDateLabel.text = [dateFormatter stringFromDate: self.ticket.purchaseDate];
  self.verificationDateLabel.text = [dateFormatter stringFromDate: self.ticket.verificationDate];
}

#pragma mark buttons

- (IBAction)checkTicketFromPhoneButtonClicked:(id)sender {
    [[[TicketsChecker alloc] initWithTicket: self.ticket] checkTicketFromPhone];
}


#pragma mark -
#pragma mark network management

- (void)callPayVerification:(id) value {
  if([value isKindOfClass:[NSError class]]) {
    [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", value] afterDelay:3.0f];
    return;
  }
  
  if([value isKindOfClass:[SoapFault class]]) {
    [SVProgressHUD dismissWithError:[value string]];
    return;
  }
  
  UPaidbillByIBANResult *result = (UPaidbillByIBANResult *) value;
  NSString *message = nil;
  
  switch ([result status]) {
    case 1:;
      message = @"Wybrany bilet jest aktywny.";
      break;
    default:;
      message = @"Wybrany bilet jest nieaktywny.";
      break;
  }
  
  [SVProgressHUD dismiss];
  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil ];
  
  [alert show];
  //[alert release];
}

- (void)authActionProcess:(NSString *)arg {
  UPaidMyWsdl *w = [UPaidMyWsdl service];
  
  [w callPayVerification:self action:@selector(callPayVerification:) token:[UPaidLogInController presentToken] productCode:self.ticket.productCode sessionToken:self.ticket.sessionToken];
}

- (void)dealloc {
  //[_checkTicketButton release];
  //[_ticketDetailsLabel release];
  //[_purchaseDateLabel release];
  //[_verificationDateLabel release];
  //[_ticketDetailsBackground release];
  //[super dealloc];
}
@end
