//
//  TicketsMainScreenViewController.m
//  CallPay
//
//  Created by uPaid on 06.06.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "TicketsMainScreenViewController.h"
#import "ProjectButton.h"
#import "TicketsChooseCityViewController.h"
#import "TicketBuyFirstStepViewController.h"
#import "TicketBuySecondStepViewController.h"
#import "TicketBuyLastStepViewController.h"
#import "TicketsCheckListViewController.h"
#import "TicketsHistoryViewController.h"
#import "ProjectPopoverMenu.h"
#import "Ticket+Extensions.h"
#import "AppDelegate.h"

typedef enum _invalidCache {
  CacheOneCityCurrent = 0,
  CacheOneCityNotCurrent,
  CacheManyCitiesWithCurrent,
  CacheManyCitiesWithoutCurrent
} InvalidCache;

@implementation TicketsMainScreenViewController

- (id)init
{
    self = [super init];
  
    if (self) {
      [SVProgressHUD show];
      self.catalogsManager = [[CatalogsManager alloc] initWithDelegate:self];
    }



    return self;
}
//
//- (void) willShowPopoverMenu {
//  [ProjectPopoverMenu addMenuItem:@"Historia" delegate: self action:@selector(showHistory:) atIndex:0];
//}

#pragma mark catalogs management

- (void) catalogDownloadSuccess: (NSString *) cityIndex {
  
}
  
- (void) catalogDownloadFail: (NSString *) cityIndex {
  
}

- (void) startLocalize {
  [[ProjectLocations object] localizeCityWithDelegate:self];
}

- (void) checkCatalogsOrLocalize {
  if ([self.catalogsManager checkCatalogsCache]) {
    [self startLocalize];
  } else {
    UIAlertView *alert;
    
//    NSNumber *sizeToUpdate = 
    
    if ([self.catalogsManager.catalogsToUpdate count] == 1) {
      alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:[NSString stringWithFormat:@"Pojawiła się aktualizacja dla %@ (%@kB). Co chcesz zrobić?", [[self.catalogsManager dictionaryForCityIndex:[self.catalogsManager.catalogsToUpdate lastObject]] valueForKey: @"name"], [self.catalogsManager sizeToUpdate]] delegate:self cancelButtonTitle:@"Anuluj" otherButtonTitles:@"Aktualizuj", nil];
      
      if ([self.catalogsManager.catalogsToUpdate containsObject: [ProjectLocations object].currentCity]) {
        [alert setTag:CacheOneCityCurrent];
      } else {
        [alert setTag:CacheOneCityNotCurrent];
      }
    } else {
      alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:[NSString stringWithFormat:@"Pojawiła się aktualizacja dla %lu miast (łączny rozmiar aktualizacji: %@kB). Co chcesz zrobić?", (unsigned long)[self.catalogsManager.catalogsToUpdate count], [self.catalogsManager sizeToUpdate]] delegate:self cancelButtonTitle:@"Anuluj" otherButtonTitles:@"Aktualizuj wszystko", nil];
      
      if ([self.catalogsManager.catalogsToUpdate containsObject: [ProjectLocations object].currentCity]) {
        [alert addButtonWithTitle:[NSString stringWithFormat:@"Aktualizuj tylko %@", [self.catalogsManager nameForCurrentCity]]];
        [alert setTag:CacheManyCitiesWithCurrent];
      } else {
        [alert setTag:CacheManyCitiesWithoutCurrent];
      }
    }
    
    [alert show];
    //[alert release];
  } 
}

#pragma mark view management

- (void)viewDidLoad
{
  [super viewDidLoad];
  /*
  [self addButtonWithTitle:@"Zakup biletu" andImageName:@"new_zakup_biletu" andSelector:@selector(buyTicket:) atPosY:20+60];//bilety_
  [self addButtonWithTitle:@"Kontrola biletu" andImageName:@"new_kontrola_biletu" andSelector:@selector(checkTicket:) atPosY:90+60];
  [self addButtonWithTitle:@"Historia biletów" andImageName:@"new_historia_biletow" andSelector:@selector(showHistory:) atPosY:160+60];
//  [self addButtonWithTitle:@"Wybór miasta" andImageName:@"new_wybor_miasta" andSelector:@selector(changeCity:) atPosY:230+60];
    [self addButtonWithTitle:@"Wybór miasta" andImageName:@"new_wybor_miasta" andSelector:@selector(test:) atPosY:230+60];
    */
    
    //[self addButtonWithTitle:@"Zakup biletu" andImageName:@"new_zakup_biletu" andSelector:@selector(buyTicket:) atPosY:20+60];//bilety_
    //[self addButtonWithTitle:@"Kontrola biletu" andImageName:@"new_kontrola_biletu" andSelector:@selector(checkTicket:) atPosY:20+(1*57)+60];//checkTicket
    //[self addButtonWithTitle:@"Historia biletów" andImageName:@"new_historia_biletow" andSelector:@selector(showHistory:) atPosY:20+(3*57)+60];
      //[self addButtonWithTitle:@"Wybór miasta" andImageName:@"new_wybor_miasta" andSelector:@selector(changeCity:) atPosY:20+(4*57)+60];
    //[self addButtonWithTitle:@"Ulubione bilety" andImageName:@"new_ulubione_bilety" andSelector:@selector(test:) atPosY:20+(2*57)+60];
    
    [self addButtonWithTitle:@"Zakup biletu" andImageName:@"new_zakup_biletu" andSelector:@selector(buyTicket:) atPosY:20+60];//bilety_
    [self addButtonWithTitle:@"Kontrola biletu" andImageName:@"new_kontrola_biletu" andSelector:@selector(checkTicket:) atPosY:20+(1*57)+60];//checkTicket
    [self addButtonWithTitle:@"Historia biletów" andImageName:@"new_historia_biletow" andSelector:@selector(showHistory:) atPosY:20+(2*57)+60];
    [self addButtonWithTitle:@"Wybór miasta" andImageName:@"new_wybor_miasta" andSelector:@selector(changeCity:) atPosY:20+(3*57)+60];
    
    
    self.activeCity = [[ProjectLabel alloc] initWithFrame:CGRectMake(10, 290, self.view.bounds.size.width - 20, 55+60+90)];
  
  //self.activeCity = [[ProjectLabel alloc] initWithFrame:CGRectMake(10, 290, self.view.bounds.size.width - 20, 55+60+90)];
  [self.activeCity makeWithBoldTitle:@"Wybrane miasto:\n..." andSize:20];
  [self.activeCity setNumberOfLines:2];
//  [self.view addSubview:self.activeCity];
    
    self.activeCity2 = [[SelectedCityView alloc] initWithFrame:CGRectMake(16, 20+(3*57)+60+90, self.view.bounds.size.width - 32, 65)];
    [self.view addSubview:self.activeCity2];
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  [self checkCatalogsOrLocalize];
}

#pragma mark locations & alert management

-(void) onLocalizeCity: (NSString *) city {
  [SVProgressHUD dismiss];
  [[ProjectLocations object] setCurrentCity:city];
  
  NSDictionary *dictionary = [[[ProjectLocations object] locationList] objectForKey:city];
  
  if (dictionary == nil) {
    [self onLocalizeCityError:nil];
  } else {    
    NSString *cityToDisplay = [dictionary valueForKey:@"name"];
    
    [self.activeCity setText:[NSString stringWithFormat:@"Wybrane miasto:\n%@", cityToDisplay]];
      [self.activeCity2 setCity:cityToDisplay];
      [self.activeCity setTextColor:[UIColor whiteColor]];
    
    if (![ProjectViewHelper isSavedLocationForCity: city]) {
      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:@"Czy chcesz pobrać katalog biletów dla aktualnego miasta?" delegate:self cancelButtonTitle:@"Anuluj" otherButtonTitles:@"Pobierz", nil];
      [alert setTag:777];
      [alert show];
      //[alert release];
    }
  }
}

- (void) onLocalizeCityError: (NSError *)error {
  [SVProgressHUD dismiss];
  [self.activeCity setText:@"Wybrane miasto:\nBRAK"];
  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:@"Aplikacja nie była w stanie wykryć Twojego aktualnego połozenia. Chcesz wybrać miasto w którym się znajdujesz?" delegate:self cancelButtonTitle:@"Anuluj" otherButtonTitles:@"Wybierz", nil];
  [alert setTag:999];
  [alert show];
  //[alert release];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
  switch (alertView.tag) {
    case 999:
      if (buttonIndex == 0) {
        [self.navigationController popViewControllerAnimated:YES];
      } else {
        [self changeCity:nil];
      }
      
      return;
      break;
    case 777:
      if (buttonIndex == 0) {
        [self.navigationController popViewControllerAnimated:YES];
      } else {
        [self.catalogsManager startDownload:[ProjectLocations object].currentCity];
      }
      
      return;
      break;
      
    case CacheOneCityCurrent:
      if (buttonIndex == 0) {
        [self.navigationController popViewControllerAnimated:YES];
      } else {
        [self.catalogsManager updateCatalogsCache];
      }
      break;
    case CacheOneCityNotCurrent:
      if (buttonIndex == 0) {
        [self.catalogsManager deleteCatalogsCache];
      } else {
        [self.catalogsManager updateCatalogsCache];
      }
      break;
    case CacheManyCitiesWithCurrent:
      if (buttonIndex == 0) {
        [self.navigationController popViewControllerAnimated:YES];
      } else if (buttonIndex == 1) {
        [self.catalogsManager updateCatalogsCache];
      } else {
        [self.catalogsManager updateCurrentCityCache];
      }
      break;
    case CacheManyCitiesWithoutCurrent:
      if (buttonIndex == 0) {
        [self.catalogsManager deleteCatalogsCache];
      } else {
        [self.catalogsManager updateCatalogsCache];
      }
      break;
  }
  
  [self startLocalize];
}

# pragma mark - buttons management

- (void) buyTicket: (id) sender {
  AppDelegate *appDelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
  Ticket *newTicket = (Ticket *) [NSEntityDescription insertNewObjectForEntityForName: @"Ticket" inManagedObjectContext:[appDelegate managedObjectContext]];
  [newTicket setStatus: @(0)];
  [newTicket setCityIndex: [ProjectLocations object].currentCity];
  TicketBuyBaseViewController *viewController = [[TicketBuyFirstStepViewController alloc] initWithDictionary:[self.catalogsManager parseJsonForCurrentCity] andTicket: newTicket];
  [self.navigationController pushViewController:[viewController nextStepIfNeeded] animated:YES];
}

- (void) checkTicket: (id) sender {
  TicketsCheckListViewController *viewController = [[TicketsCheckListViewController alloc] init];
  [self.navigationController pushViewController:viewController animated:YES];
  //[viewController release];
}

- (void) changeCity: (id) sender {
  TicketsChooseCityViewController *viewController = [[TicketsChooseCityViewController alloc] init];
  [self.navigationController pushViewController:viewController animated:YES];
  //[viewController release];
}

- (void) showHistory: (id) sender {
  TicketsHistoryViewController *viewController = [[TicketsHistoryViewController alloc] initFrom:TICKETS_SCREEN];
  [self.navigationController pushViewController:viewController animated:YES];
  //[viewController release];
}
-(void)test:(id)sender
{
    
}

@end
