//
//  MCMDetailsViewController.m
//  CallPay
//
//  Created by uPaid on 21.11.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "MCMDetailsViewController.h"

@interface MCMDetailsViewController ()

@end

@implementation MCMDetailsViewController

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
  NSString *htmlPath = [[NSBundle mainBundle] pathForResource:@"mcm" ofType:@"html"];
  if (htmlPath == nil) {
    UpaidLog(@"niepoprawna sciezka");
  } else {
    NSURL *url = [NSURL fileURLWithPath:htmlPath];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webview loadRequest:request];
    [self.webview.scrollView setBounces:NO];
  }}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    /*
  [_webview release];
  [super dealloc];
     */
}
@end
