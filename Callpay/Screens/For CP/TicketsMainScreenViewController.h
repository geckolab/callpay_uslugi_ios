//
//  TicketsMainScreenViewController.h
//  CallPay
//
//  Created by uPaid on 06.06.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "ProjectViewController.h"
#import "ProjectLocations.h"
#import "ProjectLabel.h"
#import "CatalogsManager.h"
#import "SelectedCityView.h"

@interface TicketsMainScreenViewController : ProjectViewController <ProjectLocationsDelegate, UIAlertViewDelegate, CatalogsManagerDelegate>

@property (nonatomic, retain) ProjectLabel *activeCity;
@property (nonatomic, retain) SelectedCityView *activeCity2;
@property (nonatomic, retain) CatalogsManager *catalogsManager;

@end
