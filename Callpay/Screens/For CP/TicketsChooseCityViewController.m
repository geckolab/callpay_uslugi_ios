//
//  TicketsChooseCityViewController.m
//  CallPay
//
//  Created by uPaid on 13.06.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "TicketsChooseCityViewController.h"
#import "ProjectLabel.h"
#import "ASIHTTPRequest.h"
#import "ProjectViewHelper.h"

#import "CityCell.h"

@implementation TicketsChooseCityViewController

@synthesize locationsKeys;
@synthesize btnSwitch;
@synthesize localizationHeaderView;

typedef enum _ChooseCity {
  CatalogInfoAlert = 0,
  CatalogDelete = 1,
  CatalogDownload
} ChooseCity;

- (id) init
{
  self = [super init];
  
  if (self) {
    self.locationsKeys = [[[[ProjectLocations object] locationList] allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    checkSelectedButtonClicked = NO;
    self.catalogsManager = [[CatalogsManager alloc] initWithDelegate: self];
  }
  
  return self;
}

- (void)viewDidLoad
{
  [self setupTableView: CGRectMake(0, 45+45, 320, self.view.bounds.size.height - 90)];
  isBigTable = YES;
  //[self.view addSubview: self.tableView];
  [super viewDidLoad];
  
  self.btnSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(15,10+45,20,20)];
//    self.btnSwitch2 = [[CustomSwitch alloc] init];
//    self.btnSwitch2.frame = CGRectMake(80, 65, self.btnSwitch2.frame.size.width, self.btnSwitch2.frame.size.height);
//    [self.view addSubview:self.btnSwitch2];
  
    localizationHeaderView = [[LocalizationView alloc] initWithFrame:CGRectMake(16, 65, self.view.bounds.size.width - 32, 80)];
    [self.localizationHeaderView.btnSwith addTarget:self action:@selector(switchClicked:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:localizationHeaderView];
    
  NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
  
  if ([prefs objectForKey:LOCATIONS_SAVED_CITY] != nil) {
//    [self.btnSwitch setOn: YES];
      [self.localizationHeaderView.btnSwith setOn:YES];
  }
  
//  [self.btnSwitch addTarget:self action:@selector(switchClicked:) forControlEvents:UIControlEventTouchUpInside];
//  [self.view addSubview:self.btnSwitch];
  
//  ProjectLabel *switchLabel = [[ProjectLabel alloc] initWithFrame: CGRectMake(100, 10, 200, 30)];
//  [switchLabel makeWithTitle: @"Zaznacz tą opcję, jeśli nie chcesz aby aplikacja mogła zmieniać dane lokalizacyjne" andSize:10];
//
//  [self.view addSubview:switchLabel];
//  [switchLabel release];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(16, localizationHeaderView.frame.origin.y + localizationHeaderView.frame.size.height, self.view.bounds.size.width - 32, [self getWindowContentHeight] - localizationHeaderView.frame.size.height - 20)];
    
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CityCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"CityCell"];
    
    [self.view addSubview:self.tableView];
}

-(NSInteger) getRowsCount {
  return [self.locationsKeys count];
}

- (NSString *) getTableTitle {
  return @"";
}

- (UIView *) getCellContent: (NSIndexPath *)indexPath {
  UIView *content = [[UIView alloc] init];
  
  ProjectLabel *nameLabel = [[ProjectLabel alloc] initWithFrame:CGRectMake(15, 5, 260, 22.5)];
  
  NSDictionary *location = [[[ProjectLocations object] locationList] objectForKey: self.locationsKeys[indexPath.row]];
  [nameLabel makeWithTitleAlignLeft: [location valueForKey:@"name"] andSize: 16];
  [content addSubview:nameLabel];
  
  if ([[ProjectLocations object].currentCity isEqualToString: locationsKeys[indexPath.row]]) {
    ProjectLabel *label = [[ProjectLabel alloc] initWithFrame:CGRectMake(15, 23, 80, 22.5)];
    [label makeLightWithTitleAlignLeft: @"Aktualne" andSize: 10];
    [label setLineBreakMode:NSLineBreakByTruncatingTail];
    [content addSubview:label];
    //[label release];
  }
  
  ProjectLabel *sizeLabel = [[ProjectLabel alloc] initWithFrame:CGRectMake(150, 22, 80, 22.5)];
  [sizeLabel makeLightWithTitleAlignRight: [NSString stringWithFormat:@"%.2f kB", [[location objectForKey:@"size"] integerValue] / 1024.0] andSize: 11];
  [sizeLabel setLineBreakMode:NSLineBreakByTruncatingTail];
//  [content addSubview:sizeLabel];
  //[sizeLabel release];
  
  UIButton *selectRowButton = [UIButton buttonWithType:UIButtonTypeCustom];
  [selectRowButton setFrame:CGRectMake(0, 0, 240, 49)];
  [selectRowButton setTag:indexPath.row];
  [selectRowButton addTarget:self action:@selector(buttonSelectPressed:) forControlEvents:UIControlEventTouchUpInside];
  [content addSubview:selectRowButton];
  
  UIButton *billDetailBtn = [UIButton buttonWithType:UIButtonTypeCustom];
  [billDetailBtn setFrame:CGRectMake(240, 5, 49, 49)];
  [billDetailBtn addTarget:self action:@selector(buttonDownloadPressed:) forControlEvents:UIControlEventTouchUpInside];
  [billDetailBtn setBackgroundImage:[UIImage imageNamed:@"btn_downloaded"] forState:UIControlStateNormal];
  [billDetailBtn setBackgroundImage:[UIImage imageNamed:@"btn_download"] forState:UIControlStateSelected];
  [billDetailBtn setBackgroundImage:[UIImage imageNamed:@"btn_download_pressed"] forState:UIControlStateSelected | UIControlStateHighlighted];
  [billDetailBtn setBackgroundImage:[UIImage imageNamed:@"btn_downloaded_pressed"] forState:UIControlStateHighlighted];
  [billDetailBtn setTag:indexPath.row];
  
  if ([ProjectViewHelper isSavedLocationForCity:self.locationsKeys[indexPath.row]]) {
    [billDetailBtn setSelected:YES];
  }
  
  [content addSubview:billDetailBtn];
  
  return content;
}

- (void) buttonSelectPressed: (id) sender {
  checkSelectedButtonClicked = YES;
  activeButton = sender;
  
  NSString *cityIndex = self.locationsKeys[[sender tag]];
  
  if (![ProjectViewHelper isSavedLocationForCity: cityIndex]) {
    [self startDownload: cityIndex];
  } else {
    [self selectButtonClickedComplete];
  }
}

#pragma mark alert & switch management

- (void) switchClicked: (id) sender {
//  UISwitch *switchButton = (UISwitch *) sender;
    CustomSwitch *switchButton = (CustomSwitch *) sender;
  
  NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
  
  if (!switchButton.on) {
    [prefs removeObjectForKey:LOCATIONS_SAVED_CITY];
  } else {
    if ([[ProjectLocations object].currentCity isEqualToString:@""]) {
      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:@"Najpierw należy wybrać domyślne miasto." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
      [alert show];
      //[alert release];
      
      [switchButton setOn: NO animated:YES];
      return;
    } else {
      [prefs setObject: [ProjectLocations object].currentCity forKey:LOCATIONS_SAVED_CITY];
    }
  }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
  switch (alertView.tag) {
    case CatalogDelete: {
      if (buttonIndex == 1) {
        [self activeButtonChangeSelected];
        NSString *cityIndex = self.locationsKeys[[activeButton tag]];
        
        [self.catalogsManager deleteCatalogForCity:cityIndex];
      }
      break;
    }
    case CatalogDownload: {
      
    }
  }
}

#pragma mark buttons management

- (void) buttonDownloadPressed: (id) sender {
  activeButton = sender;
  
  if ([sender isSelected]) {
    if ([[ProjectLocations object].currentCity isEqualToString: self.locationsKeys[[activeButton tag]]]) {
      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message: @"Nie można usunąć katalogu dla miasta wybranego jako aktualne" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
      [alert setTag: CatalogInfoAlert];
      [alert show];
      //[alert release];
    } else {
      NSString *city = [[self dictionaryForActiveButton] valueForKey:@"name"];
      
      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message: [NSString stringWithFormat:@"Czy na pewno chcesz usunąć zapisany katalog dla miasta %@?", city] delegate:self cancelButtonTitle:@"Anuluj" otherButtonTitles:@"Usuń", nil];
      [alert setTag: CatalogDelete];
      [alert show];
      //[alert release];
    }
  } else {
    [self activeButtonChangeSelected];
    [self startDownload: self.locationsKeys[[sender tag]]];
  }
}

- (void) startDownload: (NSString *) cityIndex {
  _cityIndex = cityIndex;
  [self.catalogsManager startDownload: cityIndex];
}

- (NSDictionary *) dictionaryForActiveButton {
  return [self.catalogsManager dictionaryForCityIndex:self.locationsKeys[[activeButton tag]]];
}

- (void) selectButtonClickedComplete {
  if (checkSelectedButtonClicked) {
    checkSelectedButtonClicked = NO;
    [[ProjectLocations object] setCurrentCity:locationsKeys[[activeButton tag]]];
    [self.tableView reloadData];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
//    if ([prefs objectForKey:LOCATIONS_SAVED_CITY] != nil) {
      [prefs setObject: [ProjectLocations object].currentCity forKey:LOCATIONS_SAVED_CITY];
      [prefs synchronize];
//    }
      
//      [self.navigationController performSelector:@selector(popViewControllerAnimated:) withObject:@YES afterDelay:1.0];
      [self performSelector:@selector(popVC) withObject:nil afterDelay:1.0];
//      [self popVC];
  }
}

-(void)popVC
{
//    [self.navigationController popViewControllerAnimated:YES];
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count] - 2] animated:YES];
}

- (NSString *) cacheDirectory {
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
  
  return [NSString stringWithFormat:@"%@/JSON", [paths objectAtIndex:0]];
}

- (void) catalogDownloadSuccess: (NSString *) cityIndex {
  [self selectButtonClickedComplete];
}

- (void) catalogDownloadFail: (NSString *) cityIndex {
  [self activeButtonChangeSelected];
}

- (void) activeButtonChangeSelected {
  if (activeButton != nil) {
      if ([activeButton isMemberOfClass:NSClassFromString(@"CellConfirmButton")]) {
          NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[activeButton tag] inSection:0];

          CityCell *c = (CityCell*)[self.tableView cellForRowAtIndexPath:indexPath];
          [activeButton setActive:![activeButton isActive]];
          [c.cityStatus setActive:[activeButton isActive]];
      }
      else
          [activeButton setSelected:![activeButton isSelected]];
  }
}
- (CGFloat)tableView:(UITableView *)tabView heightForHeaderInSection:(NSInteger)section
{
    return 2;
}
- (UIView *)tableView:(UITableView *)tabView viewForHeaderInSection:(NSInteger)section
{
    UIView *hv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tabView.frame.size.width, 2)];
    [hv setBackgroundColor:[UIColor clearColor]];
    return hv;
}
- (CGFloat)tableView:(UITableView *)tabView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self getRowsCount];
}
- (UITableViewCell *)tableView:(UITableView *)table cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"CityCell";
    //    NSString *cellIdentifier = [NSString stringWithFormat:@"TicketCell", indexPath.row];
    CityCell *cell = [table dequeueReusableCellWithIdentifier:cellIdentifier];
    
    //    if (cell == nil || IS_IOS7_OR_LATER)
    if (cell == nil)
    {
        cell = [[CityCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    //    else {
    //        if ([cell subviews]){
    //            for (UIView *subview in [cell subviews]) {
    //                [subview removeFromSuperview];
    //            }
    //        }
    //    }
    
    //  static NSString *CellIdentifier = @"Cell";
    //
    //  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //
    //  if (cell == nil) {
    //    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    //  }
    
    
    cell.selectedBackgroundView.backgroundColor=[UIColor clearColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    NSDictionary *location = [[[ProjectLocations object] locationList] objectForKey: self.locationsKeys[indexPath.row]];
    [cell setTag:indexPath.row];
    [cell.downloadButton addTarget:self action:@selector(newButtonDownloadAction:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.cityNameLabel.text = [location valueForKey:@"name"];
    if ([[ProjectLocations object].currentCity isEqualToString: locationsKeys[indexPath.row]]) {
        cell.activeLabel.text = @"Aktualnie wybrane";
        [cell.cityStatus setActive:NO];
    }
    else
    {
        [cell.cityStatus setActive:NO];
    }
    
    if ([ProjectViewHelper isSavedLocationForCity:self.locationsKeys[indexPath.row]]) {
//        [cell.cityStatus setActive:YES];
//        [cell.downloadButton setSelected:YES];
        [cell.downloadButton setActive:YES];
    }
    else
    {
//        [cell.cityStatus setActive:NO];
//        [cell.downloadButton setSelected:NO];
        [cell.downloadButton setActive:NO];
    }

    return cell;
}
- (NSIndexPath *)tableView:(UITableView *)tabView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSLog(@"selected cell %d",indexPath.row);
    CityCell *c = (CityCell*)[self.tableView cellForRowAtIndexPath:indexPath];
    checkSelectedButtonClicked = YES;
    if (c != nil) {
        activeButton = c.barsView;
    }
    else
        activeButton = nil;
    
    NSString *cityIndex = self.locationsKeys[indexPath.row];
    
    if (![ProjectViewHelper isSavedLocationForCity: cityIndex]) {
        [self startDownload: cityIndex];
    } else {
        [self selectButtonClickedComplete];
    }
    return indexPath;
}
-(IBAction)newButtonDownloadAction:(id)sender
{
    activeButton = sender;
    
    if ([sender isActive]) {
        if ([[ProjectLocations object].currentCity isEqualToString: self.locationsKeys[[activeButton tag]]]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message: @"Nie można usunąć katalogu dla miasta wybranego jako aktualne" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert setTag: CatalogInfoAlert];
            [alert show];
            //[alert release];
        } else {
            NSString *city = [[self dictionaryForActiveButton] valueForKey:@"name"];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message: [NSString stringWithFormat:@"Czy na pewno chcesz usunąć zapisany katalog dla miasta %@?", city] delegate:self cancelButtonTitle:@"Anuluj" otherButtonTitles:@"Usuń", nil];
            [alert setTag: CatalogDelete];
            [alert show];
            //[alert release];
        }
    } else {
        [self activeButtonChangeSelected];
        [self startDownload: self.locationsKeys[[sender tag]]];
    }
}


@end