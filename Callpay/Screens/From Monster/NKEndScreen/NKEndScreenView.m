//
//  NKEndScreenView.m
//  Nakarm Prepaida
//
//  Created by Bartosz Wachowski on 27.02.2012.
//  Copyright (c) 2012 Mobiles4Fun. All rights reserved.
//

#import "NKEndScreenView.h"
#import "SFHFKeychainUtils.h"

@implementation NKEndScreenView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
      self.navigationItem.hidesBackButton = YES;
      
      UIImageView *readyTxt = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ready_txt.png"]];
      [readyTxt setFrame:CGRectMake(self.view.bounds.size.width/2 - readyTxt.bounds.size.width/2, self.view.bounds.size.height/2 - readyTxt.bounds.size.height/2, readyTxt.bounds.size.width, readyTxt.bounds.size.height)];
      
      self.navigationItem.titleView = readyTxt;
    }
    return self;
}

-(IBAction)next:(id)sender {
  [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
  
//  UIImageView *thanksTxt = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"thanks_txt.png"]];
//  [thanksTxt setFrame:CGRectMake(80, 30, thanksTxt.bounds.size.width, thanksTxt.bounds.size.height)];
//  [self.view addSubview:thanksTxt];
//  
//  UIImageView *monster = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"monster_thanks.png"]];
//  [monster setFrame:CGRectMake(self.view.bounds.size.width/2, self.view.bounds.size.height/5, monster.bounds.size.width, monster.bounds.size.height)];
//  [self.view addSubview:monster];
//  
//  NakarmNextButton *next = [NakarmNextButton buttonWithType:UIButtonTypeCustom];
//  [next makeNakarmNextLongButton];
//  [next setFrame:CGRectMake((self.view.frame.size.width -  next.bounds.size.width)/2, monster.frame.origin.y + monster.bounds.size.height, next.bounds.size.width, next.bounds.size.height)];
//  [next setTitle:@"Nakarm kolejnego mPotwora" forState:UIControlStateNormal];
//  [next addTarget:self action:@selector(oneMoreTime:) forControlEvents:UIControlEventTouchUpInside];
//  [self.view addSubview:next];
//  
//  UIImage *imLogOut = [UIImage imageNamed:@"logout_btn.png"];
//  // NSLog(@"s %f", imLogOut.size.width);
//  UIButton *logoutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//  [logoutBtn setTitle:@"Wyloguj" forState:UIControlStateNormal];
//  [logoutBtn setTitleColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] forState:UIControlStateNormal];
//  [logoutBtn.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:11.0]];
//  [logoutBtn setBackgroundImage:[UIImage imageNamed:@"logout_btn.png"] forState:UIControlStateNormal];
//  [logoutBtn setBackgroundImage:[UIImage imageNamed:@"logout_btn_pressed.png"] forState:UIControlStateSelected];
//  [logoutBtn setFrame:CGRectMake(self.view.bounds.size.width/2 - imLogOut.size.width/2, self.view.bounds.size.height - imLogOut.size.height * 2.5, imLogOut.size.width, imLogOut.size.height)];
//  [logoutBtn addTarget:self action:@selector(logOut:) forControlEvents:UIControlEventTouchUpInside];
//  [self.view addSubview:logoutBtn];

  [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(IBAction) oneMoreTime:(id)sender {
  
  NSMutableArray *viewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
  
//  NKChooseNumberView *nvc = [[NKChooseNumberView alloc] init];
//  [viewControllers replaceObjectAtIndex:0 withObject:nvc];
//  [self.navigationController setViewControllers:viewControllers];
  
  [self.navigationController popToRootViewControllerAnimated:NO];
}

-(IBAction) logOut:(id)sender {
  [self.navigationController popToRootViewControllerAnimated:NO];
  NSUserDefaults *p = [NSUserDefaults standardUserDefaults];
  [SFHFKeychainUtils deleteItemForUsername:[p objectForKey:PHONE_NUM_KEY] andServiceName:STORE_SERVICE error:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
