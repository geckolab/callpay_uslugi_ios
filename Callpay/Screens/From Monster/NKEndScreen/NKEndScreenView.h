//
//  NKEndScreenView.h
//  Nakarm Prepaida
//
//  Created by Bartosz Wachowski on 27.02.2012.
//  Copyright (c) 2012 Mobiles4Fun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProjectViewController.h"

@interface NKEndScreenView : ProjectViewController

@end
