//
//  TicketPayViewController.m
//  CallPay
//
//  Created by Jacek on 31.03.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "TicketPayViewController.h"

#import "UpaidMPM.h"

#import "AppDelegate.h"

@interface TicketPayViewController ()

@end

@implementation TicketPayViewController

@synthesize ticketName;
@synthesize verificationPhone;
@synthesize verificationTime;
@synthesize productCode;
//@synthesize price;
@synthesize headerView;

@synthesize ticketNameLabel;
@synthesize ticketPriceLabel;

- (id) initWithTicket: (Ticket *) __ticket {
    self = [super init];
    
    if (self) {
        self.ticket = __ticket;
        self.ticketName = self.ticket.name;
        self.price = [self.ticket.price floatValue];
        self.productCode = self.ticket.productCode;
    }
    
    return self;
}
-(void)setupWithTicket: (Ticket *) __ticket
{
    self.ticket = __ticket;
    self.ticketName = self.ticket.name;
    self.price = [self.ticket.price floatValue];
    self.productCode = self.ticket.productCode;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.headerView setIcon:[UIImage imageNamed:@"new_zakup_biletu"]];
    [self.headerView setTitle:@"Zakup biletu"];
    
    [self.ticketNameLabel setText:self.ticketName];
//    [self.ticketPriceLabel setText:self.price];
    [self setPriceLabel:self.price];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)setPriceLabel:(CGFloat)priceValue
{
    NSNumberFormatter * formatter = [[NSNumberFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"pl_PL"];
    formatter.numberStyle = NSNumberFormatterCurrencyStyle;
    [formatter setLocale:locale];
    
    CGFloat pr = priceValue / 100;
    
    [self.ticketPriceLabel setText:[formatter stringFromNumber:[[NSNumber alloc] initWithFloat:pr]]];
}
-(IBAction)buyAction:(id)sender
{
    [[CPApi sharedManager] setDelegateCheckExternalTransfer:self];
    [[CPApi sharedManager] checkExternalTransferWithPhone:@"48123456789" andProductCode:@"256.27740" andToken:@"token"];
    /*
    [UpaidMPM sharedInstance].products = [[NSArray alloc] initWithObjects:[[UpaidMPMProduct alloc] initWithName:@"product name" andShortDescription:self.ticketName andImage: [UIImage imageNamed:@"phone"] andQuantity:1 andAmount:(int)self.price], nil];
//    [[UpaidMPM sharedInstance] setAmount:(int)self.price andItemNo:self.productCode andAdditionalData:@""];
    [[UpaidMPM sharedInstance] setAmount:(int)self.price andItemNo:self.productCode andAdditionalData:@""];
    [UpaidMPM sharedInstance].delegate = self;
    self.navigationController.navigationBar.translucent = NO;
//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [[UpaidMPM sharedInstance] startPaymentForNavigationController:self.navigationController];
     */
}

- (void) afterSuccessfullPaymentWithNavigationController: (UINavigationController *) navigationController andId: (int) paymentId andAdditionalData: (NSString *) resultAdditionalData {
    
    NSLog(@"success");
//    [self.ticket setPurchaseDate:[NSDate date]];
//    AppDelegate *appDelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:@"Bilet kupiony pomyślnie." delegate:self cancelButtonTitle:@"OK!" otherButtonTitles: nil];
//    [self.ticket setStatus:@(1)];
//    [alert show];
//    NSError *error = nil;
//    [[appDelegate managedObjectContext] save:&error];
//    self.navigationController.navigationBar.translucent = YES;
//    self.navigationController.navigationBar.tintColor = [UIColor clearColor];
//    [navigationController popToRootViewControllerAnimated: YES];
    [[CPApi sharedManager] setDelegateTransactionStart:self];
    [[CPApi sharedManager] startTransactionNormalWithSessionToken:self.ticket.sessionToken];
    
}



- (void) afterUnsuccessfullPaymentWithNavigationController: (UINavigationController *) navigationController {
    
    NSLog(@"fail");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:@"Wystąpił bład techniczny. Spróbuj ponownie później." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    self.navigationController.navigationBar.translucent = YES;
    [navigationController popToRootViewControllerAnimated: YES];
    
}

- (void) checkExternalTransferDidSuccessWithDictionary:(NSDictionary*)dictionary
{
    NSLog(@"\namount: %@\nsession-token: %@\n",[dictionary valueForKey:@"amount"],[dictionary valueForKey:@"session-token"] );
    [self setPriceLabel:[[dictionary valueForKey:@"amount"] floatValue] ];
    [self.ticket setPrice:[NSNumber numberWithFloat:[[dictionary valueForKey:@"amount"] floatValue]]];
    [self.ticket setSessionToken:[dictionary valueForKey:@"session-token"]];
    NSLog(@"\nticket\n%@\n",self.ticket);
    
//    [[CPApi sharedManager] setDelegateTransactionStart:self];
//    [[CPApi sharedManager] startTransactionNormalWithSessionToken:self.ticket.sessionToken];
    
    [UpaidMPM sharedInstance].products = [[NSArray alloc] initWithObjects:[[UpaidMPMProduct alloc] initWithName:@"product name" andShortDescription:self.ticketName andImage: [UIImage imageNamed:@"phone"] andQuantity:1 andAmount:[self.ticket.price intValue]], nil];
    //    [[UpaidMPM sharedInstance] setAmount:(int)self.price andItemNo:self.productCode andAdditionalData:@""];
    [[UpaidMPM sharedInstance] setAmount:[self.ticket.price intValue] andItemNo:self.productCode andAdditionalData:@""];
    [UpaidMPM sharedInstance].delegate = self;
    self.navigationController.navigationBar.translucent = NO;
    //    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [[UpaidMPM sharedInstance] startPaymentForNavigationController:self.navigationController];
     
}
- (void) checkExternalTransferDidFail
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:@"Wystąpił bład techniczny. Spróbuj ponownie później." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}
- (void) transactionStartDidSuccessWithDictionary:(NSDictionary*)dictionary
{
    NSLog(@"\ndictionary:\n%@\n%@\n",dictionary,self.ticket);
    NSTimeInterval interval = [[dictionary valueForKey:@"transaction-time"] doubleValue];
    NSDate *dateTime = [NSDate dateWithTimeIntervalSince1970:interval];
    [self.ticket setPurchaseDate:dateTime];
    [self.ticket setStatus:[NSNumber numberWithInt:1]];
    NSLog(@"\nticket\n%@\n",self.ticket);
    AppDelegate *appDelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    NSError *error = nil;
    [[appDelegate managedObjectContext] save:&error];
    /*
    [self.ticket setPurchaseDate:[NSDate date]];
    AppDelegate *appDelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:@"Bilet kupiony pomyślnie." delegate:self cancelButtonTitle:@"OK!" otherButtonTitles: nil];
    [self.ticket setStatus:@(1)];
    [alert show];
    NSError *error = nil;
    [[appDelegate managedObjectContext] save:&error];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBar.tintColor = [UIColor clearColor];
//    [navigationController popToRootViewControllerAnimated: YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
     */
//    [self.navigationController popViewControllerAnimated:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:@"Bilet zakupiony pomyślnie." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    [self.navigationController popToViewController:self animated:NO];
    [self.navigationController performSelector:@selector(popToRootViewControllerAnimated:) withObject:@YES afterDelay:0.5];
//    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void) transactionStartDidFail
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:@"Wystąpił bład techniczny. Spróbuj ponownie później." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

@end
