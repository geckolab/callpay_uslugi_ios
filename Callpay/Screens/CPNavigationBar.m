//
//  CPNavigationBar.m
//  CallPay
//
//  Created by Jacek on 08.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "CPNavigationBar.h"

@implementation CPNavigationBar

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)setTranslucent:(BOOL)translucent
{
    [super setTranslucent:translucent];
    if (translucent)
    {
        [self setTintColor:[UIColor clearColor]];
    }
    else
    {
//        [self setTintColor:[UIColor colorWithHexString:@"075080"]];//64, 93, 135
        [self setTintColor:[UIColor colorWithRed:64/255.0 green:93/255.0 blue:135/255.0 alpha:1.0]];
        self.backgroundColor = [UIColor clearColor];
        self.opaque = YES;
//        self.shadowImage = [UIImage new];
//        [self setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    }
}
- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar {
    
    return UIBarPositionTopAttached;
}

@end
