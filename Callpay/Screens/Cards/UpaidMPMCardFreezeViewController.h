//
//  UpaidMPMCardFreezeViewController.h
//  EuroFlorist
//
//  Created by Michał Majewski on 03.03.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "ProjectViewController.h"
#import "UPaidCard.h"

@interface UpaidMPMCardFreezeViewController : ProjectViewController

- (id)initWithCard: (UPaidCard *) card;

@end
