//
//  CardsListViewController.m
//  Sprytny Bill
//
//  Created by uPaid on 19.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "CardsListViewController.h"
#import "AddCardViewController.h"
#import "ProjectButton.h"
#import "ProjectViewHelper.h"
#import "ProjectLabel.h"
#import "ProjectPopoverMenu.h"
#import "UpaidMPMCardVerifyViewController.h"
#import "UpaidMPMCardFreezeViewController.h"

@interface CardsListViewController ()

@property (nonatomic) CardListFrom from;

@end

@implementation CardsListViewController

typedef enum _CardListType {
  CardListInfo = 0, //zwykly alert informacyjny
  CardListAuthorizeInfo, //info startujace weryfikację
  CardListAuthorizeCVC,
  CardListAuthorizeCode,
  CardListNoCards
} CardListType;

@synthesize cardList;

- (id) init {
  return [self initFrom: CardListFromBuyView];
}

- (id) initFrom: (CardListFrom) from {
  self = [super init];
  
  if (self) {
    cardToAuthorize = @"";
    cardBlocked = NO;
    self.from = from;
  }
  
  return self;
}

- (void) dealloc {
  //[super dealloc];
  [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) startAuthorize: (NSNotification *) notification {
  NSUserDefaults *p = [NSUserDefaults standardUserDefaults];
  cardToAuthorize = [p objectForKey:UPAID_CARD_TO_PAY_ID];
}

#pragma mark Overlay
- (BOOL) overlayElements: (UIView *) container {
  [container addSubview:[ProjectViewHelper imageView:@"dodaj-karte.png" atPosY:[ProjectViewHelper screenSize].height - 130]];
  
  for (int i = 0; i < [self.cardList count]; i++) {
    if (![[self.cardList objectAtIndex:i] authorized]) {
      [container addSubview:[ProjectViewHelper imageView:@"autoryzuj-karte.png" atPosY:20 + i * 40]];
      
      return YES;
    }
  }
  
  if ([cardList count] == 0) { //zanim się karty wczytały
    return YES;
  } else {
    return NO;
  }
}

- (void) overlayAutoHide {}

#pragma mark view lifecycle

- (void) viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  upaidLogin = [[UPaidLogInController alloc] initWithDelegate:self];
  [upaidLogin startAuthAction:@"cardsList"];
}

- (void)viewDidLoad{
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startAuthorize:) name:@"startAuthorize" object:nil];
  
  [super viewDidLoad];
  [self setupView];
}

- (void) setupView {
  [self setupTableView:CGRectMake(0, 0, 320, [ProjectViewHelper screenSize].height - 105)];
  isBigTable = YES;
  [self.view addSubview: self.tableView];
  
  authorizeAlertDisplayed = NO;
  
  ProjectButton *addCard = [[ProjectButton alloc] init];
  [addCard setFrame:CGRectMake(10, [ProjectViewHelper screenSize].height - 100, 300, 50)];
  [addCard setTitle:@"Dodaj kartę" forState:UIControlStateNormal];
  [addCard makeBigButton];
  [addCard addImageToTitle:@"dodaj_nowa_karte"];
  [addCard addTarget:self action:@selector(addCard:) forControlEvents:UIControlEventTouchUpInside];
  [self.view addSubview:addCard];
}

#pragma mark - Table view data source
-(NSInteger) getRowsCount {
  return [[self cardList] count];
}

- (NSString *) getTableTitle {
  return @"Twoje karty";
}

- (UIView *) getCellContent: (NSIndexPath *)indexPath {
  UIView *content = [[UIView alloc] init];
  
  ProjectLabel *billLabel = [[ProjectLabel alloc] initWithFrame:CGRectMake(15, 5, 250, 22.5)];
  [billLabel makeWithTitleAlignLeft:[NSString stringWithFormat:@"%@", [[[self cardList] objectAtIndex:[indexPath row]] nickName]] andSize:15];
  
  [content addSubview:billLabel];
  
  ProjectLabel *authorizeLabel = [[ProjectLabel alloc] initWithFrame:CGRectMake(15, 22, 150, 22.5)];
  UPaidCard *card = [[self cardList] objectAtIndex:[indexPath row]];
  
  if (card.authorized) {
    [authorizeLabel makeLightWithTitleAlignLeft: @"zweryfikowana" andSize: 11];
  } else {
    [authorizeLabel makeMediumWithTitleAlignLeft: @"niezweryfikowana" andSize: 11];
  }
  
  if (card.ddefault == 1) {
    [authorizeLabel setText:[NSString stringWithFormat:@"domyślna, %@", authorizeLabel.text]];
  }
  
  if (card.locked) {
    billLabel.textColor = [UIColor grayColor];
    authorizeLabel.textColor = [UIColor grayColor];
  }
  
  [authorizeLabel setLineBreakMode:NSLineBreakByTruncatingTail];
  [content addSubview:authorizeLabel];
  
  UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
  [btn setFrame:CGRectMake(0, 0, 320, 40)];
  [btn setTag:indexPath.row];
  [btn addTarget:self action:@selector(selectCard:) forControlEvents:UIControlEventTouchUpInside];
  [content addSubview:btn];
  
  UIButton *authorizeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
  [authorizeBtn setFrame:CGRectMake(240, 12, 57, 27)];
  [authorizeBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 15.0, 0, 15.0)];
  
  if (card.authorized) {
    [authorizeBtn setImage:[UIImage imageNamed: @"icon_v.png"] forState:UIControlStateNormal];
    [authorizeBtn setImage:[UIImage imageNamed: @"icon_v.png"] forState:UIControlStateHighlighted];
    [authorizeBtn addTarget:self action:@selector(selectCard:) forControlEvents:UIControlEventTouchUpInside];
  } else {
    [authorizeBtn setImage:[UIImage imageNamed: @"btn_lock.png"] forState:UIControlStateNormal];
    [authorizeBtn setImage:[UIImage imageNamed: @"btn_lock_pressed.png"] forState:UIControlStateHighlighted];
    [authorizeBtn addTarget:self action:@selector(authorizeCardButton:) forControlEvents:UIControlEventTouchUpInside];
    [authorizeBtn setTag:indexPath.row];
    
    if ([card._id isEqualToString: cardToAuthorize]) {
      cardToAuthorize = @"";
      [authorizeBtn sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
  }
  
  [content addSubview:authorizeBtn];
  
  return content;
}

#pragma mark alert management

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
  if (alertView.tag == CardListNoCards) {
    if (buttonIndex == 1) {
      [self addCard:nil];
    } else {
      [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex: [self.navigationController.viewControllers count] - 2] animated:YES];
    }
  }
  
  if (buttonIndex == 1) {
  } else {
    [SVProgressHUD dismiss];
  }
}

#pragma mark buttons management

-(IBAction)authorizeCardButton: (id) sender {
  UIButton *btn = (UIButton *) sender;
  _card = [cardList objectAtIndex:btn.tag];
  [self verifyCard:_card];
}

- (void) verifyCard: (UPaidCard *) card {
  if (card.freezed) {
    //[self.navigationController pushViewController:[[UpaidMPMCardVerifyViewController alloc] initWithCard:card] animated:YES];
  } else {
    [self.navigationController pushViewController:[[UpaidMPMCardFreezeViewController alloc] initWithCard:card] animated:YES];
  }
}

-(IBAction)addCard:(id)sender {
  AddCardViewController *rc = [[AddCardViewController alloc] initWithState:ADD_CARD_FROM_OTHER_CARDS];
  [self.navigationController pushViewController:rc animated:NO];
  //[rc release];
}



-(IBAction)selectCard:(id)sender {
  UIButton *btn = (UIButton *) sender;
  
  NSUserDefaults *p = [NSUserDefaults standardUserDefaults];
  UPaidCard *card = [cardList objectAtIndex:btn.tag];
  
  if ([card locked]) {
    UIAlertView *a = [[UIAlertView alloc] initWithTitle:APP_NAME message:@"3 razy błędnie wprowadzono kod CVC2/CVV2/mPIN. Twoja karta została zablokowana na godzinę. Wybierz inną kartę!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
    [a setTag:CardListInfo];
    [a show];
    //[a release];
  } else {
    [p setValue:[card nickName] forKey:UPAID_CARD_TO_PAY_PAN];
    [p setValue:[card _id] forKey:UPAID_CARD_TO_PAY_ID];
    [p setValue:[card card_type] forKey:UPAID_CARD_TO_PAY_TYPE];
    [self.navigationController popViewControllerAnimated:NO];
  }
}

#pragma mark network management

-(void) authActionProcess:(NSString *) arg dictionary:(NSDictionary *)dictionary {
  UPaidMyWsdl * w = [UPaidMyWsdl service];
  
  if ([arg isEqualToString:@"cardsList"]) {
    [w cardslist:self action:@selector(cardlist:) token:[UPaidLogInController presentToken]];
  }
  
  [SVProgressHUD showWithStatus:@"Proszę czekać..." maskType:SVProgressHUDMaskTypeGradient];
}

-(void) cardlist:(id) value {
	if([value isKindOfClass:[NSError class]]) {
    [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", value] afterDelay:3.0f];
		return;
  }
  
  if([value isKindOfClass:[SoapFault class]]) {
    [SVProgressHUD dismissWithError:[value string]];
    return;
  }
  
  UPaidcardListResult *result = (UPaidcardListResult *) value;
  [SVProgressHUD dismiss];
  
  if ([result status] == 1) {
    [self setCardList:[result cards]];
    [self.tableView reloadData];
  } else if ([result status] == 2) {
    NSString *title;
    
    if (cardBlocked) {
      [[NSUserDefaults standardUserDefaults] setBool:NO forKey:USER_HAS_CARDS];
      title = @"Za dużo błędnych prób weryfikacji - Twoja jedyna karta została zablokowana. Aby płacić, dodaj kartę.";
    } else {
      title = @"Nie posiadasz żadnych kart. Aby płacić, dodaj kartę.";
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:title delegate:self cancelButtonTitle:@"Anuluj" otherButtonTitles:@"Dodaj", nil];
    [alert setTag: CardListNoCards];
    [alert show];
    //[alert release];
  }
}

@end