//
//  CardsListViewController.h
//  Sprytny Bill
//
//  Created by uPaid on 19.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "ProjectViewController.h"
#import "UPaidMyWsdl.h"
#import "UPaidLogInController.h"

@interface CardsListViewController : ProjectViewController {
  UPaidLogInController *upaidLogin;
  UPaidCard *_card;
  NSString *cardToAuthorize;
  BOOL authorizeAlertDisplayed, cardBlocked;
}

@property (nonatomic,retain) UPaidcardList *cardList;


typedef enum _CardListFrom {
  CardListFromBuyView = 0,
  CardListFromMainView
} CardListFrom;

- (void) setupView;
- (id) initFrom: (CardListFrom) from;

@end
