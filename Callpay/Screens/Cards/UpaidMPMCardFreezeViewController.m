//
//  UpaidMPMCardFreezeViewController.m
//  EuroFlorist
//
//  Created by Michał Majewski on 03.03.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidMPMCardFreezeViewController.h"
#import "UpaidMPMCardVerifyViewController.h"
#import "UPaidLogInController.h"

@interface UpaidMPMCardFreezeViewController () <UITextFieldDelegate, UPaidLogInControllerDelegate>

@property (nonatomic) UPaidCard *card;
@property (retain, nonatomic) IBOutlet ProjectTextField *cvcTextField;

@end

@implementation UpaidMPMCardFreezeViewController

- (id)initWithCard: (UPaidCard *) card {
  self = [super init];
  
  if (self) {
    self.card = card;
    [self registerForKeyboardNotifications];
  }
  
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
}

#pragma mark buttons

- (IBAction)nextButtonClicked:(id)sender {
  if ([[self.cvcTextField text] length] < 3) {
    [ProjectViewHelper showInfoAlert: @"Podaj kod CVC2/CVV2!"];
  } else {
    [[[UPaidLogInController alloc] initWithDelegate: self] startAuthAction:@"freezeMoney"];
  }
}

#pragma mark UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
  if (range.length == 0 && [textField.text length] > 3) {
    return NO;
  }
  
  return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
  self.activeTxtField = (UITextField *) textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
  self.activeTxtField = nil;
}

- (void)dealloc {
  //[_cvcTextField release];
  //[self.scrollView release];
  //[super dealloc];
}

#pragma mark network management

-(void) authActionProcess:(NSString *) arg {
  UPaidMyWsdl * w = [UPaidMyWsdl service];
  
  if ([arg isEqualToString: @"freezeMoney"]) {
    [w freezeMoney:self action:@selector(freezeMoney:) token:[UPaidLogInController presentToken] cvc2: [self.cvcTextField text] id:[self.card._id intValue] force:@"1"];
  }
  
  [SVProgressHUD showWithStatus:@"Proszę czekać..." maskType:SVProgressHUDMaskTypeGradient];
}

-(void) freezeMoney:(id) value {
	if([value isKindOfClass:[NSError class]]) {
    [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", value] afterDelay:3.0f];
		return;
  }
  
  if([value isKindOfClass:[SoapFault class]]) {
    [SVProgressHUD dismissWithError:[value string]];
    return;
  }
  
  UPaidbuyResult *result = (UPaidbuyResult *) value;
  [SVProgressHUD dismiss];
  
  if ([result status] == 1) {
      /*
    self.card.freezed = YES;
    UpaidMPMCardVerifyViewController *viewController = [[UpaidMPMCardVerifyViewController alloc] initWithCard: self.card];
    UINavigationController *navController = self.navigationController;
    //[[self retain] autorelease];
    
    [navController popViewControllerAnimated:NO];
    [navController pushViewController:viewController animated:YES];
    //[viewController release];
       */
  } else {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:[ProjectViewHelper statusSubstatusInfo:[result status] substatus:[result substatus]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    //[alert release];
    
    if ([result status] == 9) {
      [self.navigationController popViewControllerAnimated: YES];
    }
  }
}

@end
