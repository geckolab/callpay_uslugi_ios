//
//  TicketPayViewController.h
//  CallPay
//
//  Created by Jacek on 31.03.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActiveTicketsHeaderView.h"

#import "UpaidMPMDelegate.h"

#import "CPApi.h"

@interface TicketPayViewController : ProjectViewController <UpaidMPMDelegate, CheckExternalTransferDelegate, TransactionStartDelegate>


@property (nonatomic, strong) IBOutlet ActiveTicketsHeaderView *headerView;

@property (nonatomic, strong) NSString *ticketName;
@property (assign) CGFloat price;
@property (nonatomic, strong) NSString *productCode;
@property (nonatomic, strong) NSString *verificationPhone;
@property (nonatomic, strong) NSString *verificationTime;

@property (nonatomic, strong) IBOutlet UILabel *ticketNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *ticketPriceLabel;

@property (retain, nonatomic) Ticket *ticket;

-(void)setPriceLabel:(CGFloat)priceValue;

-(IBAction)buyAction:(id)sender;

- (id) initWithTicket: (Ticket *) __ticket;
-(void)setupWithTicket: (Ticket *) __ticket;

@end
