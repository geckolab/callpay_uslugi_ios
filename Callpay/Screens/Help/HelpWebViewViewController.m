//
//  HelpWebViewViewController.m
//  CallPay
//
//  Created by uPaid on 21.11.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "HelpWebViewViewController.h"

@interface HelpWebViewViewController () <UIWebViewDelegate>

@property (retain, nonatomic) IBOutlet UIWebView *webView;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *indicatorView;

@end

@implementation HelpWebViewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
  NSURL *url = [NSURL URLWithString:CALLPAY_FAQ_URL];
  NSURLRequest *request = [NSURLRequest requestWithURL:url];
  [self.webView loadRequest:request];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    /*
  [_webView release];
  [_indicatorView release];
  [super dealloc];
     */
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
  [self.indicatorView stopAnimating];
}
@end
