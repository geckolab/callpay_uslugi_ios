//
//  HelpViewController.m
//  Sprytny Bill
//
//  Created by uPaid on 15.04.2012.
//  Copyright (c) 2012 uPaid. All rights reserved.
//

#import "HelpDetailViewController.h"
#import "HelpViewController.h"
#import "ProjectButton.h"
#import "UIColor+HexString.h"
#import "ProjectPopoverMenu.h"

@implementation HelpViewController

- (id) init {
  self = [super init];
  
  if (self) {
    arrayWithValues = [[NSArray alloc] initWithObjects:
                       @"Sprytny Bill - korzyści",
                       @"O MasterCard Mobile",
                       @"Co mogę opłacać Sprytnym Billem?",
                       @"Opłacanie rachunków - sposoby",
                       @"Skanowanie rachunków",
                       @"Czym jest generator kodów QR?",
                       @"Co to jest szablon rachunku?",
                       @"Co muszę mieć aby skorzystać z Billa?",
                       @"Co to jest kod CVC2 i mPIN?",
                       @"Ręczne wprowadzanie rachunków",
                       @"Limity",
                       @"Prowizje",
                       @"Księgowanie",
                       @"Kontakt", nil];    
  }
  
  return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
  [super viewDidLoad];
  
  ProjectLabel *titleLabel = [[ProjectLabel alloc] initWithFrame:CGRectMake(30, 10, self.view.bounds.size.width - 60, 250)];
  [titleLabel makeWithTitle: @"W razie problemów z działaniem aplikacji prosimy o kontakt pod adresem bok@callpay.pl." andSize:18];
  [self.view addSubview:titleLabel];
  
  return;
  
  [self setupTableView];
  [self.view addSubview:self.tableView];
  
  [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  [[ProjectPopoverMenu menuItems] removeObjectAtIndex:0];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return [arrayWithValues count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
  
  if (cell == nil) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
  }

  ProjectButton *cardBtn;

  [cell.selectedBackgroundView setBackgroundColor:[UIColor clearColor]];
  [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
  
  cardBtn = [[ProjectButton alloc] initWithFrame:CGRectMake(cell.bounds.size.width/2 - 145, 5, 290, 50)];
  
  [cardBtn setTitle:[NSString stringWithFormat:@"%@", [arrayWithValues objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
  [cardBtn makeSmallButton];
  [cardBtn setTag:indexPath.row];
  [cardBtn addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
  [cell addSubview:cardBtn];
  //[cardBtn release];
  
  return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  return 58;
}

-(IBAction)buttonPressed:(id)sender {
  NSString *title, *body;
  
  switch ([sender tag]) {
    case 0: {
      title = @"Korzyści z płacenia Billem";
      body = @"korzysci";
    }
      break;
    case 1: {
      title = @"O MasterCard Mobile";
      body = @"masterCard";
    }
      break;
    case 2: {
      title = @"Co mogę opłacać Sprytnym Billem?";
      body = @"jakieRachunki";
    }
      break;
    case 3: {
      title = @"Opłacanie rachunków - sposoby";
      body = @"placenieRachunkow";
    }
      break;
    case 4: {
      title = @"Skanowanie rachunków";
      body = @"skanRachunkow";
    }
      break;
    case 5: {
      title = @"Czym jest generator kodów QR?";
      body = @"czymJestQR";
    }
      break;
    case 6: {
      title = @"Co to jest szablon rachunku?";
      body = @"szablony";
    }
      break;
    case 7: {
      title = @"Co muszę mieć aby skorzystać z Billa?";
      body = @"coMusze";
    }
      break;
    case 8: {
      title = @"Co to jest kod CVC2 i mPIN?";
      body = @"cvc2";
    }
      break;
    case 9: {
      title = @"Ręczne wprowadzanie rachunków";
      body = @"reczneRachunki";
    }
      break;
    case 10: {
      title = @"Limity";
      body = @"limity";
    }
      break;
    case 11: {
      title = @"Prowizje";
      body = @"prowizje";
    }
      break;
    case 12: {
      title = @"Ile trwa księgowanie płatności?";
      body = @"ksiegowanie";
    }
      break;
    case 13: {
      title = @"Kontakt";
      body = @"kontakt";
    }
      break;
  }
  
  HelpDetailViewController *hd = [[HelpDetailViewController alloc] initWithTitle:title andBody:body];
  [self.navigationController pushViewController:hd animated:NO];
  //[hd release];
  
}

@end
