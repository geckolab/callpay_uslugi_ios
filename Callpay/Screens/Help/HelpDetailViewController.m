//
//  HelpDetailViewController.m
//  Sprytny Bill
//
//  Created by uPaid on 16.05.2012.
//  Copyright (c) 2012 uPaid. All rights reserved.
//

#import "HelpDetailViewController.h"
#import "ProjectPopoverMenu.h"

@interface HelpDetailViewController ()

@end

@implementation HelpDetailViewController

@synthesize titleLabel;
@synthesize bodyLabel;

- (id) initWithTitle:(NSString *) title andBody:(NSString *) body {
  
  if (self) {
    _ttitle = title;
    _body = body;
  }
  
  return self;
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  [[ProjectPopoverMenu menuItems] removeObjectAtIndex:0];
}

- (void)viewDidLoad
{
  self.titleLabel = [[ProjectLabel alloc] initWithFrame:CGRectMake(30, 10, self.view.bounds.size.width - 60, 60)];
  [self.titleLabel makeWithTitle:_ttitle andSize:18];
  [self.view addSubview:self.titleLabel];
  
  UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(10, 65, self.view.bounds.size.width - 20, [[UIScreen mainScreen] applicationFrame].size.height - 115)];
  [webView setBackgroundColor:[UIColor clearColor]];
  webView.opaque = NO;
  webView.scrollView.bounces = NO;
  webView.dataDetectorTypes = UIDataDetectorTypeLink;
  [webView setDelegate:self];
  NSBundle *tBundle = [NSBundle mainBundle];
  NSString *path = [tBundle pathForResource:_body ofType:@"html"];
  
  NSURL *inUrl = [NSURL fileURLWithPath:path];
  
  [webView loadRequest:[NSURLRequest requestWithURL:inUrl]];
  [self.view addSubview:webView];
  //[webView release];
  
  [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
  if ( inType == UIWebViewNavigationTypeLinkClicked ) {
    [[UIApplication sharedApplication] openURL:[inRequest URL]];
    return NO;
  }
  
  return YES;
}

@end
