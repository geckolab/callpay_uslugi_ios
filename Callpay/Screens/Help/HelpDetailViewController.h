//
//  HelpDetailViewController.h
//  Sprytny Bill
//
//  Created by uPaid on 16.05.2012.
//  Copyright (c) 2012 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProjectLabel.h"
#import "ProjectViewController.h"

@interface HelpDetailViewController : ProjectViewController <UIWebViewDelegate> {
  
  NSString *_ttitle;
  NSString *_body;
  
}

- (id) initWithTitle:(NSString *) title andBody:(NSString *) body;

@property (retain) ProjectLabel *titleLabel, *bodyLabel;

@end
