//
//  IntroScreen.h
//  Nakarm Prepaida
//
//  Created by uPaid on 14.02.2012.
//  Copyright (c) 2012 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProjectTextField.h"
#import "UPaidLogInController.h"
#import "ProjectLabel.h"
#import "ProjectViewController.h"
#import "LogInAlertView.h"
#import "ProjectButton.h"

@interface IntroScreen : ProjectViewController <UITextFieldDelegate, UIAlertViewDelegate, UPaidLogInControllerDelegate> {
  UITextField *codeField;
  int tempStatus;
  BOOL showProgressHud;
}

@property (retain) UIView *contentView;
@property (retain) ProjectTextField *phoneNumTextField;
@property (retain) ProjectButton *loginJoin;
@property (retain) UIActivityIndicatorView *indicatorView;
@property (retain) UIView *overlayView;

- (void) sendPhoneNumberToCheckIfExists:(BOOL) askForNewCode;
- (void) showAlertForPhoneCode;
- (void) askForAds:(int) status;
- (void) processCompleted;

@end