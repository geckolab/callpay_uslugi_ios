//
//  IntroScreen.m
//  Nakarm Prepaida
//
//  Created by uPaid on 14.02.2012.
//  Copyright (c) 2012 uPaid. All rights reserved.
//

#import "IntroScreen.h"
#import "RegisterViewController.h"
#import "UPaidMyWsdl.h"
#import "AddCardViewController.h"
#import "SFHFKeychainUtils.h"
#import "AppDelegate.h"
#import "ProjectViewHelper.h"
#import "ProjectLocations.h"

@implementation IntroScreen

static int ALERT_VIEW_WRITE_CODE = 1;
static int ALERT_VIEW_MISTAKE = 2;
static int ALERT_VIEW_BAD_CODE = 3;

@synthesize phoneNumTextField;

- (id)init
{
    self = [super init];
    if (self) {
      
      showProgressHud = NO;
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backFromRegister:) name:@"backFromRegister" object:nil];
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResign) name:UIApplicationWillResignActiveNotification object:nil];
      
      UITapGestureRecognizer *tapScroll = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
      tapScroll.cancelsTouchesInView = NO;
      [self.view addGestureRecognizer:tapScroll];
      //[tapScroll release];
      
      NSUserDefaults *p = [NSUserDefaults standardUserDefaults];
      NSError *error;
      
      if ([p stringForKey:PHONE_NUM_KEY] != nil && ![[p stringForKey:PHONE_NUM_KEY] isEqualToString:@""] && [SFHFKeychainUtils getPasswordForUsername:[p stringForKey:PHONE_NUM_KEY] andServiceName:STORE_SERVICE error:&error] != nil) {
        [self startLogin];
      }
    }
  
    return self;
}

- (void) backFromRegister: (NSNotification *)notification {
  NSUserDefaults *p = [NSUserDefaults standardUserDefaults];
  [self.phoneNumTextField setText:[p stringForKey:PHONE_NUM_KEY]];
}

-(IBAction)tapped:(id)sender {
  [self.view endEditing:YES];
}

-(void) appWillResign {
  [self.view endEditing:YES];
}

-(IBAction)next:(id)sender {
  if ([[self.phoneNumTextField text] isEqualToString:@""] || 
      [[self.phoneNumTextField text] length] != 9) {
    UIAlertView *al = [[UIAlertView alloc] initWithTitle:APP_NAME message:@"Zła długość numeru telefonu. Wprowadź numer 9-znakowy." delegate:self cancelButtonTitle:@"Spróbuj jeszcze raz" otherButtonTitles:nil];
    [al show];
    //[al release];
  } else {
    [self sendPhoneNumberToCheckIfExists:NO];
  }
}

- (void) startLogin {
  [self showHUD];
  showProgressHud = YES;
  UPaidLogInController *lg = [[UPaidLogInController alloc] initWithDelegate:self];
  [lg startAuthAction:UPAID_LOGIN_STATUS];
}

-(void) sendPhoneNumberToCheckIfExists:(BOOL) askForNewCode {
  [self showHUD];
//  [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient networkIndicator:YES];
  UPaidMyWsdl *s = [UPaidMyWsdl service];
  NSError *error = nil;
  
  NSUserDefaults *p = [NSUserDefaults standardUserDefaults];
  
  if ([SFHFKeychainUtils getPasswordForUsername:[self.phoneNumTextField text]
                                andServiceName:STORE_SERVICE
                                            error:&error] == nil || askForNewCode) { 
      [SFHFKeychainUtils deleteItemForUsername:[p valueForKey:PHONE_NUM_KEY] 
                                andServiceName:STORE_SERVICE 
                                         error:&error];
    [p removeObjectForKey:UPAID_DEFAULT_CARD_NICKNAME];
    [p removeObjectForKey:UPAID_CARD_TO_PAY_PAN];
    [s check:self action:@selector(check:) partner_id:PARTNER_ID phone:[NSString stringWithFormat:@"48%@",[self.phoneNumTextField text]]];
  } else {
    
    if (![[p valueForKey:PHONE_NUM_KEY] isEqual:[self.phoneNumTextField text]]) {
      [SFHFKeychainUtils deleteItemForUsername:[p valueForKey:PHONE_NUM_KEY] 
                                andServiceName:STORE_SERVICE 
                                         error:&error];
    }
    
    [p setValue:[self.phoneNumTextField text] forKey:PHONE_NUM_KEY];
    [self startLogin];
  }
}

-(void) showAlertForPhoneCode {
  if ([[self.phoneNumTextField text] isEqualToString:@""]) {
    return;
  }
  
  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:@"Poniżej podaj kod weryfikacyjny, który otrzymasz na swój telefon." delegate:self cancelButtonTitle:@"Anuluj" otherButtonTitles:@"OK", nil];
  
  [self.view endEditing:YES];
  
  [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
  
  codeField = [alert textFieldAtIndex:0];
  codeField.keyboardType = UIKeyboardTypeNumberPad;
  [codeField setPlaceholder:@"Wpisz kod z telefonu"];
  codeField.delegate = self;
  
  if (IS_TEST) {
    NSString *url = [NSString stringWithFormat:@"http://test_upaid:sB6IA89@test.upaid.pl/users/user_mobile_pass/%d/%@", PARTNER_ID, [NSString stringWithFormat:@"48%@",[self.phoneNumTextField text]]];
    [codeField setText: [NSString stringWithContentsOfURL:[NSURL URLWithString: url] encoding:NSUTF8StringEncoding error:nil]];
  }
  
  [alert setTag:ALERT_VIEW_WRITE_CODE];
  [alert show];
  //[alert release];
  
  NSUserDefaults *p = [NSUserDefaults standardUserDefaults];
  [p setBool:YES forKey:PHONE_CODE_ASKED];
}

-(void) check:(id) value {
	if([value isKindOfClass:[NSError class]]) {
    [self dismissHUD];
//    [SVProgressHUD dismissWithError:[ NSString stringWithFormat:@"%@", value] afterDelay:3.0f];
		return;
  }

  if([value isKindOfClass:[SoapFault class]]) {
    [self dismissHUD];
//    [SVProgressHUD dismissWithError:[value string]];
    return;
  }		
  
  UPaidstatusResult *result = (UPaidstatusResult *) value;
  NSUserDefaults *p = [NSUserDefaults standardUserDefaults];
  if ([result status] == 2) {
    tempStatus = 2;
    [self askForAds:2];
  } else if ([result status] == kUserLoginUpdateApp) {
    [[LogInAlertView alloc] showLogInAlertUpdateApp];
  } else if ([result status] == 1) {
      [p setValue:[self.phoneNumTextField text] forKey:PHONE_NUM_KEY];
      NSError *error = nil;
    
      if ([SFHFKeychainUtils getPasswordForUsername:[self.phoneNumTextField text] andServiceName:STORE_SERVICE error:&error] == nil) {
        [self dismissHUD];
//        [SVProgressHUD dismiss];
        [self showAlertForPhoneCode];
      } else {
        tempStatus = 1;
        [self askForAds:1];
      }
  }
}

-(void) askForAds:(int) status {
  tempStatus = status;
  [self processCompleted];
}

-(void) processCompleted {
  [self dismissHUD];
//  [SVProgressHUD dismiss];
  NSUserDefaults *p = [NSUserDefaults standardUserDefaults];
  
  if (tempStatus == 2) {
    [self dismissHUD];
//    [SVProgressHUD dismissWithSuccess:@"Nie znaleźliśmy Twojego numeru w naszej bazie. Aby korzystać z aplikacji zarejestruj się." afterDelay:4.0f];
    [p removeObjectForKey:PHONE_NUM_KEY];
    
    RegisterViewController *rc = [[RegisterViewController alloc] initWithPhone: [self.phoneNumTextField text]];
    [self.navigationController pushViewController:rc animated:NO];
    //[rc release];
  } else{
    AppDelegate *d = (AppDelegate *) [[UIApplication sharedApplication] delegate] ;
    [self dismissHUD];
    [d removeLoginView];
    [p setBool:NO forKey:PHONE_CODE_ASKED];
    
    if (tempStatus == 1) {
      [p setBool:YES forKey:USER_HAS_CARDS];
    } else if (tempStatus == 3) {
      [p setBool:NO forKey:USER_HAS_CARDS];
    }
  }    
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
  if (alertView.tag == ALERT_VIEW_WRITE_CODE) {
    if (buttonIndex == 1) {
      if ([[codeField text] isEqualToString:@""]) {
        UIAlertView *a = [[UIAlertView alloc] initWithTitle:APP_NAME message:@"Kod nie może być pusty!" delegate:self cancelButtonTitle:@"Anuluj" otherButtonTitles:@"OK", nil];
        [a setTag:ALERT_VIEW_MISTAKE];
        [a show];
        //[a release];
      } else {
        NSUserDefaults *p = [NSUserDefaults standardUserDefaults];
        NSError *error = nil;
         
        [SFHFKeychainUtils storeUsername:[p objectForKey:PHONE_NUM_KEY] andPassword:[codeField text] forServiceName:STORE_SERVICE updateExisting:YES error:&error];
        [p setBool:NO forKey:PHONE_CODE_ASKED];
       [self startLogin];
      }
    } else {
      [self.phoneNumTextField becomeFirstResponder];
    }
  } else if (alertView.tag == ALERT_VIEW_MISTAKE) {
    if (buttonIndex == 1) {
      [self showAlertForPhoneCode];
    }
  } else if (alertView.tag == ALERT_VIEW_BAD_CODE) {
    if (buttonIndex == 1) {
      [self showAlertForPhoneCode];
    } else if (buttonIndex == 2) {
      [self sendPhoneNumberToCheckIfExists:YES];
    }
  }
  
}

-(void) authActionProcess:(NSString *) arg {
  UPaidMyWsdl * w = [UPaidMyWsdl service];
  [w cardslist:self action:@selector(cardlist:) token:[UPaidLogInController presentToken]];
}

-(void) cardlist:(id) value {
	if([value isKindOfClass:[NSError class]]) {
    [self dismissHUD];
//    [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", value] afterDelay:3.0f];
		return;
  }
  
  if([value isKindOfClass:[SoapFault class]]) {
    [self dismissHUD];
//    [SVProgressHUD dismissWithError:[value string]];
    return;
  }
  
  UPaidtokenResult * result = (UPaidtokenResult *) value;
  if ([result status] == 1) {
    UPaidcardListResult *clResult = (UPaidcardListResult *) value;
    UPaidcardList *cardList = [clResult cards];
    
    NSUserDefaults *p = [NSUserDefaults standardUserDefaults];
    for (int i=0; i< [cardList count]; i++) {
      UPaidCard *card = [cardList objectAtIndex:i];
      if (i == 0) {
        [p setValue:[card _id] forKey:UPAID_CARD_TO_PAY_ID];
        [p setValue:[card nickName] forKey:UPAID_CARD_TO_PAY_PAN];
        [p setValue:[card card_type] forKey:UPAID_CARD_TO_PAY_TYPE];
        [p synchronize];
      }
      if ([card ddefault] == 1) {
        [p setValue:[card nickName] forKey:UPAID_DEFAULT_CARD_NICKNAME];
        [p setValue:[card _id] forKey:UPAID_DEFAULT_CARD_ID];
        [p setValue:[card card_type] forKey:UPAID_DEFAULT_CARD_TYPE];
        [p setValue:[card _id] forKey:UPAID_CARD_TO_PAY_ID];
        [p setValue:[card nickName] forKey:UPAID_CARD_TO_PAY_PAN];
        [p setValue:[card card_type] forKey:UPAID_CARD_TO_PAY_TYPE];
        [p synchronize];
        break;
      }
    }
    [self askForAds:1];

  } else if ([result status] == 2) {
//    [SVProgressHUD dismiss];
    [self dismissHUD];
    [self askForAds:3];
  }
}

-(void) authActionError:(int) arg {
  [self dismissHUD];
  
  UIAlertView *a = [[UIAlertView alloc] initWithTitle:APP_NAME message:@"Podany kod jest nieprawidłowy! Sprawdź kod jeszcze raz i spróbuj wpisać jeszcze raz!" delegate:self cancelButtonTitle:@"Anuluj" otherButtonTitles:@"Wpisz jeszcze raz", @"Poproś o nowy kod", nil];
  [a setTag:ALERT_VIEW_BAD_CODE];
  [a show];
  //[a release];
  
  [self dismissHUD];
}

#pragma mark - View lifecycle

- (void) showHUD {
  [self.overlayView setHidden: NO];
  [self.indicatorView setHidden: NO];
  [self.indicatorView startAnimating];
}

- (void) dismissHUD {
  [self.overlayView setHidden: YES];
  [self.indicatorView stopAnimating];
  [self.indicatorView setHidden: YES];
}

- (void) viewWillDisappear:(BOOL)animated {
  [self unregisterFromKeyboardNotifications];
}

- (void) viewWillAppear:(BOOL)animated {
//  [super viewWillAppear:animated];
  [self registerForKeyboardNotifications];
  [self.navigationController.navigationBar setHidden:YES];
  [self refreshPhoneTextField];
  NSUserDefaults *p = [NSUserDefaults standardUserDefaults];
  
  if ([p stringForKey:PHONE_NUM_KEY] == nil) {
    showProgressHud = NO;
  }
  
  if (showProgressHud) {
    [self showHUD];
//    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient networkIndicator:YES];
  }
  
  if ([p boolForKey:PHONE_CODE_ASKED]) {
    [self showAlertForPhoneCode];
  }
}

- (void)viewDidLoad {
  [super setupScrollView:[[UIScreen mainScreen] applicationFrame].size.height frameHeight:[[UIScreen mainScreen] applicationFrame].size.height + 10];
  [self.view addSubview:self.scrollView];
  
  UIImage *logowanieImg = [UIImage imageNamed:@"img_logowanie.png"];
  
  int logowanieImgPosY;
  
  if([UIScreen mainScreen].bounds.size.height == 568) {
    logowanieImgPosY = 76;
  } else {
    logowanieImgPosY = 54;
  }
  
  UIImageView *logowanieImgBg = [[UIImageView alloc] initWithFrame: CGRectMake(0.5, logowanieImgPosY, logowanieImg.size.width, logowanieImg.size.height)];
  [logowanieImgBg setImage:logowanieImg];
  
  [self.scrollView addSubview:logowanieImgBg];
  
  int labelPosY = logowanieImgBg.frame.size.height + logowanieImgBg.frame.origin.y - 20;
  
  ProjectLabel *mainTextLabel = [[ProjectLabel alloc] initWithFrame:CGRectMake(10, labelPosY, self.view.bounds.size.width , 45)];
  [mainTextLabel makeWithTitle:@"Zaloguj się jako:" andSize:18];
  [mainTextLabel setTextAlignment:NSTextAlignmentLeft];
  [self.scrollView addSubview:mainTextLabel];
  
  self.phoneNumTextField = [[ProjectTextField alloc] initWithPlaceholder:@"Podaj numer telefonu - 600112233" posY:labelPosY + 50];
  [self.phoneNumTextField setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
  [self.phoneNumTextField setKeyboardType:UIKeyboardTypeNumberPad];
  [self.phoneNumTextField setReturnKeyType:UIReturnKeyDone];
  [self.phoneNumTextField setDelegate:self];
  
  [self.scrollView addSubview:self.phoneNumTextField];
  
  [self refreshPhoneTextField];
  
  self.loginJoin = [ProjectButton buttonWithType:UIButtonTypeCustom];
  [self.loginJoin setFrame:CGRectMake(self.view.bounds.size.width/2 - 145, labelPosY + 100, 290, 40)];
  [self.loginJoin setTitle:@"Przejdź dalej" forState:UIControlStateNormal];
  [self.loginJoin makeBigButton];
  [self.loginJoin addTarget:self action:@selector(next:) forControlEvents:UIControlEventTouchUpInside];
  
  [self.scrollView addSubview:self.loginJoin];
  
  self.indicatorView = [[UIActivityIndicatorView alloc] initWithFrame: CGRectMake(275, labelPosY + 110, 20, 20)];
  [self.scrollView bringSubviewToFront:self.indicatorView];
  [self.indicatorView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
  [self.indicatorView setHidden: YES];
  [self.scrollView addSubview: self.indicatorView];
  
  self.overlayView = [[UIView alloc] initWithFrame: self.scrollView.frame];
  [self.overlayView setHidden: YES];
  [self.scrollView bringSubviewToFront:self.overlayView];
  [self.scrollView addSubview: self.overlayView];
  
  //[mainTextLabel release];
  //[logowanieImgBg release];

  [super viewDidLoad];
}

- (void) refreshPhoneTextField {
  NSUserDefaults *p = [NSUserDefaults standardUserDefaults];
  
  if ([p stringForKey:PHONE_NUM_KEY] == nil || [[p stringForKey:PHONE_NUM_KEY] isEqualToString:@""]) {
    [self.phoneNumTextField setText: @""];
  } else {
    [self.phoneNumTextField setText:[p stringForKey:PHONE_NUM_KEY]];
  }
}

-(CGFloat)heightOf:(CGFloat)heightValue{
  CGRect screenBounds = [[UIScreen mainScreen] bounds];
  
  CGFloat height = screenBounds.size.height;
  return height;
}

-(BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
  if (range.length == 0) {
    if (textField.text.length >= 9)
      return NO;
    else
      return YES;
  } else
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) dealloc {
  
  //[self.phoneNumTextField release];
  //[self.overlayView release];
  //[self.loginJoin release];
  //[self.indicatorView release];
  //[super dealloc];
}

- (int) keyboardShift {
  return 60;
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
  if ([touch view] == self.loginJoin){
    [self next:nil];
  }

  return YES;
}

@end