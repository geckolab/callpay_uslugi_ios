//
//  RegisterViewController.m
//  Nakarm Prepaida
//
//  Created by uPaid on 15.02.2012.
//  Copyright (c) 2012 uPaid. All rights reserved.
//

#import "RegisterViewController.h"
#import "AddCardViewController.h"
#import "ProjectButton.h"
#import "AppDelegate.h"
#import "RegisterRulesViewController.h"
#import "ProjectViewHelper.h"
#import "UIColor+HexString.h"

@interface RegisterViewController ()

- (BOOL) checkIfDataOK;

@end

@implementation RegisterViewController

@synthesize nameTextField = _nameTextField;
@synthesize surnameTextField = _surnameTextField;
@synthesize emailTextField = _emailTextField;
@synthesize acceptMailingButton;
@synthesize acceptRulesButton;

static int NAME_T = 0;
static int SUR_NAME_T = 1;
static int EMAIL_T = 2;

///Duration of hiding/showing animation
#define ANIMATION_DURATION 0.25f

- (id)initWithPhone: (NSString *) __phone
{
    self = [super init];
    if (self) {
      self.phone = __phone;
//      [self registerForKeyboardNotifications];
      
      UITapGestureRecognizer *tapScroll = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
      tapScroll.cancelsTouchesInView = NO;
      [self.view addGestureRecognizer:tapScroll];
      //[tapScroll release];
    }
    return self;
}

-(IBAction)tapped:(id)sender {
  [self.view endEditing:YES];
}

-(BOOL) checkIfDataOK {
  NSString *message = @"";
  
  if ([[self.nameTextField text] isEqualToString:@""]) {
    message = @"Pole Imię jest puste";
  } else if ([[self.surnameTextField text] isEqualToString:@""]) {
    message = @"Pole Nazwisko jest puste";
  } else if ([[self.emailTextField text] isEqualToString:@""]) {
    message = @"Pole E-mail jest puste";
  }
//  else if (self.acceptRulesButton.selected == NO) {
//    message = @"Musisz wyrazić zgodę na przetwarzanie danych osobowych";
//  }
  
  if ([message isEqualToString: @""]) {
    return YES; 
  } else {
    UIAlertView *alerView;
    alerView = [[UIAlertView alloc] initWithTitle:@"Błąd podczas zakładania konta" message:message  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alerView show];
    //[alerView release];
    
    return NO;
  }
}

#pragma mark - View lifecycle

-(void) viewWillAppear:(BOOL)animated {
  [self.navigationController.navigationBar setHidden:NO];
//  [super viewWillAppear:animated];
}

- (void) prepareTextField: (ProjectTextField *) field{
  [field setReturnKeyType:UIReturnKeyDone];
  [field addTarget:self action:@selector(buttonDoneClicked:) forControlEvents:UIControlEventEditingDidEndOnExit];
  [field setText:@""];
}

- (void)viewDidLoad
{
  [self setupScrollView];
  [self.view addSubview:self.scrollView];
  
  ProjectLabel *labelField = [[ProjectLabel alloc] initWithFrame: CGRectMake(10, 15, 300, 30)];
  [labelField makeWithTitle: [NSString stringWithFormat: @"Rejestracja numeru %@", self.phone] andSize: 18];
  [self.scrollView addSubview:labelField];
  
  self.nameTextField = [[ProjectTextField alloc] initWithPlaceholder:@"Imię" posY:55];
  [self.nameTextField setTag:NAME_T];
  [self prepareTextField: self.nameTextField];
  [self.scrollView addSubview:self.nameTextField];
  
  self.surnameTextField = [[ProjectTextField alloc] initWithPlaceholder:@"Nazwisko" posY:95];
  [self.surnameTextField setTag:SUR_NAME_T];
  [self prepareTextField: self.surnameTextField];
  [self.scrollView addSubview:self.surnameTextField];
  
  self.emailTextField = [[ProjectTextField alloc] initWithPlaceholder:@"Adres e-mail" posY:135];
  [self.emailTextField setTag:EMAIL_T];
  [self prepareTextField: self.emailTextField];
  [self.emailTextField setKeyboardType:UIKeyboardTypeEmailAddress];
  self.emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
  
  [self.scrollView addSubview:self.emailTextField];
  
//  self.acceptRulesButton = [ProjectButton checkbox];
//  [self.acceptRulesButton setFrame:CGRectMake(10, 210, 30, 30)];
//  [self.acceptRulesButton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
//  [self.scrollView addSubview:self.acceptRulesButton];
  
//  UILabel *acceptRules = [[UILabel alloc] initWithFrame:CGRectMake(40, 170, 270, 120)];
//  [acceptRules setTextAlignment:NSTextAlignmentLeft];
//  [acceptRules setNumberOfLines:7];
//  [acceptRules setTextColor:[UIColor colorWithHexString:@"2f3234"]];
//  [acceptRules setBackgroundColor:[UIColor clearColor]];
//  [acceptRules setText:@"Wyrażam zgodę na przetwarzanie moich danych osobowych przez uPaid Sp. z o.o. z siedzibą w Warszawie 01-562 ul. Mickiewicza 27/179 do celów przesyłania drogą elektroniczną informacji w zakresie niezbędnym do obsługi transakcji oraz w calach statystycznych a także odnoszących się do usług świadczonych przez uPaid Sp. z o.o"];
//  [acceptRules setFont:[ProjectViewHelper fontNormalWithSize:12]];
//  [self.scrollView addSubview:acceptRules];
  
  self.acceptMailingButton = [ProjectButton checkbox];
  [self.acceptMailingButton setFrame:CGRectMake(10, 185, 40, 40)];
  [self.acceptMailingButton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
  [self.acceptMailingButton setSelected:YES];
  [self.scrollView addSubview:self.acceptMailingButton];
  
  UILabel *acceptMailing = [[UILabel alloc] initWithFrame:CGRectMake(50, 170, 260, 60)];
  [acceptMailing setTextAlignment:NSTextAlignmentLeft];
  [acceptMailing setNumberOfLines:2];
  [acceptMailing setTextColor:[UIColor colorWithHexString:@"2f3234"]];
  [acceptMailing setBackgroundColor:[UIColor clearColor]];
  [acceptMailing setText:@"Wyrażam zgodę na otrzymywanie od Callpay Sp. z o.o. informacji handlowych"];
  [acceptMailing setFont:[ProjectViewHelper fontNormalWithSize:12]];
  [self.scrollView addSubview:acceptMailing];
  
  ProjectButton *registerButton = [[ProjectButton alloc] init];
  [registerButton setFrame:CGRectMake(10, [ProjectViewHelper screenSize].height - 130, 300, 50)];
  [registerButton setTitle:@"Zarejestruj" forState:UIControlStateNormal];
  [registerButton makeBigButton];
  [registerButton addTarget:self action:@selector(goToRules:) forControlEvents:UIControlEventTouchUpInside];
  [self.scrollView addSubview:registerButton];
  
  [super viewDidLoad];
}

-(IBAction)goToRules:(id)sender {
  if ([self checkIfDataOK]) {
    NSDictionary *dict = @{
                           @"phone" : self.phone,
                           @"name" : self.nameTextField.text,
                           @"surname" : self.surnameTextField.text,
                           @"email" : self.emailTextField.text,
                           @"acceptMailing" : (self.acceptMailingButton.selected ? @"1" : @"0")
                           };
    RegisterRulesViewController *nkr = [[RegisterRulesViewController alloc] initWithData:
                                        dict];
    [self.navigationController pushViewController:nkr animated:YES];
    //[nkr release];
  }
}

-(IBAction)buttonPressed:(id)sender {
  UIButton *button = (UIButton *) sender;
  [button setSelected:!button.selected];
}

-(IBAction)buttonDoneClicked:(id)sender {
  [self.view endEditing:YES];
}

-(void) dealloc {
  //[self.nameTextField release];
  //[self.surnameTextField release];
  //[self.emailTextField release];
  //[self.scrollView release];
  //[self.contentView release];
  
  //[super dealloc];
}

@end