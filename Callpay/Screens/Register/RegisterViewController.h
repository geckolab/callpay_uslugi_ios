//
//  RegisterViewController.h
//  Nakarm Prepaida
//
//  Created by uPaid on 15.02.2012.
//  Copyright (c) 2012 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProjectTextField.h"
#import "UPaidLogInController.h"
#import "ProjectLabel.h"
#import "ProjectViewController.h"

@interface RegisterViewController : ProjectViewController <UITextFieldDelegate> {
  UITextField *codeField;
  UPaidLogInController *upaidLogin;
}
@property (retain) UIView *contentView;
@property (retain) ProjectTextField *nameTextField;
@property (retain) ProjectTextField *surnameTextField;
@property (retain) ProjectTextField *emailTextField;
@property (retain) UIButton *acceptMailingButton, *acceptRulesButton;
@property (retain) NSString *phone;

- (id)initWithPhone: (NSString *) __phone;

@end
