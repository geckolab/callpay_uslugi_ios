//
//  RegisterRulesViewController.m
//  Sprytny Bill
//
//  Created by uPaid on 4/7/13.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "RegisterRulesViewController.h"
#import "AddCardViewController.h"
#import "ProjectButton.h"
#import "ProjectViewHelper.h"

@interface RegisterRulesViewController ()

@end

@implementation RegisterRulesViewController

@synthesize dictionary;

- (id)initWithData:(NSDictionary *) _dictionary
{
    self = [super init];
    if (self) {
      self.dictionary = _dictionary;
    }
    return self;
}

#pragma mark - View lifecycle


-(void) viewWillAppear:(BOOL)animated {
  [self.navigationController.navigationBar setHidden:NO];
  //  [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  NSURL *url = [NSURL URLWithString:@"http://tools.upaid.pl/callpay/regulations/callpay_glowny.php?platform=ios"];
  NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];

	UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, [ProjectViewHelper screenSize].width, [ProjectViewHelper screenSize].height - 100)];
  [webView loadRequest:requestObj];
  webView.scrollView.bounces = NO;
  [self.view addSubview:webView];
  
  ProjectButton *notAcceptButton = [[ProjectButton alloc] initWithTitle:@"Nie akceptuję" imageName:@"btn_dodaj_do_koszyka" fontSize:16];
  [notAcceptButton addTarget:self action:@selector(notAccept:) forControlEvents:UIControlEventTouchUpInside];
  [notAcceptButton setFrame: CGRectMake(10, [ProjectViewHelper screenSize].height - 90, self.view.bounds.size.width / 2 - 20, [UIImage imageNamed:@"btn_dodaj_do_koszyka"].size.height)];
  [self.view addSubview:notAcceptButton];
  
  ProjectButton *acceptButton = [[ProjectButton alloc] initWithTitle:@"Akceptuję" imageName:@"btn_zaplac" fontSize:16];
  [acceptButton addTarget:self action:@selector(accept:) forControlEvents:UIControlEventTouchUpInside];
  [acceptButton setFrame: CGRectMake(self.view.bounds.size.width / 2 + 10, [ProjectViewHelper screenSize].height - 90, self.view.bounds.size.width / 2 - 20, [UIImage imageNamed:@"btn_zaplac"].size.height)];
  [self.view addSubview:acceptButton];
}

#pragma mark - Buttons management

- (void) notAccept: (id) sender {
  [SVProgressHUD show];
  [SVProgressHUD dismissWithError:@"Rejestracja anulowana." afterDelay:2.0f];
  [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:0] animated:YES];
}

-(void) accept:(id)sender {  
  [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient networkIndicator:YES];
  UPaidMyWsdl *s = [UPaidMyWsdl service];
  
  UPaidaddress *address = [[UPaidaddress alloc] init];
  [address setStreet:@""];
  [address setNumber:@""];
  [address setApartment:@""];
  [address setCity:@""];
  [address setZipcode:@""];
  
  [s register:self action:@selector(processRegister:) partner_id:PARTNER_ID
                    phone:[NSString stringWithFormat:@"48%@", [self.dictionary valueForKey: @"phone"]]
                    email:[self.dictionary valueForKey: @"email"]
                firstname:[self.dictionary valueForKey: @"name"]
                 lastname:[self.dictionary valueForKey: @"surname"]
                      pan:@""
                 exp_date:@""
                  mailing:[self.dictionary valueForKey: @"acceptMailing"]
                  address:address];
}

#pragma mark - Network management

-(void) processRegister:(id) value {
	if([value isKindOfClass:[NSError class]]) {
    [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", value] afterDelay:3.0f];
		return;
  }
  
  if([value isKindOfClass:[SoapFault class]]) {
    return;
  }
  
  UPaidtokenResult* result = (UPaidtokenResult*)value;
  NSString *message;
  
  switch ([result status]) {
    case kUserRegisterOK: {
      NSUserDefaults *p = [NSUserDefaults standardUserDefaults];
      [p setBool:YES forKey:PHONE_CODE_ASKED];
      [p setValue:[self.dictionary valueForKey: @"phone"] forKey:PHONE_NUM_KEY];
      [self makeCheck];
      
      return;
    }
      break;
    case kUserRegisterEmailBusy: {
      message= @"Rejestracja nie powiodła się. Taki e-mail już istnieje w bazie.";
    }
      break;
    case kUserRegisterWrongFormat: {
      message = @"Rejestracja nie powiodła się. Zły format e-mail.";
    }
      break;
    case kUserRegisterCardsIsBusy: {
      message = @"Rejestracja nie powiodła się. Karta znajduje się już w bazie.";
    }
      break;
    case kUserRegisterCardWrongNumb: {
      message = @"Rejestracja nie powiodła się. Numer karty nie jest prawidłowym numerem MC/Maestro.";
    }
      break;
    case kUserRegisterWrongPartnerID: {
      message = @"Rejestracja nie powiodła się. Błąd techniczny.";
    }
      break;
    case kUserRegisterNoPermToPlatform: {
      message = @"Rejestracja nie powiodła się. Brak uprawnień.";
    }
      break;
    case kUserRegisterEmptyObligatoryField: {
      message = @"Rejestracja nie powiodła się. Uzupełnij wszystkie obowiązkowe pola.";
    }
    default:
      message = @"Rejestracja nie powiodła się. Błąd techniczny.";
      break;
  }
  
  [SVProgressHUD dismiss];
  UIAlertView *a = [[UIAlertView alloc] initWithTitle:@"uPaid" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
  [a show];
  //[a release];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
  [self.navigationController popViewControllerAnimated:YES];
}

-(void) makeCheck{
  UPaidMyWsdl *s = [UPaidMyWsdl service];
  
  NSUserDefaults *p = [NSUserDefaults standardUserDefaults];
  
  [s check:self action:@selector(check:) partner_id:PARTNER_ID phone:[NSString stringWithFormat:@"48%@", [p stringForKey:PHONE_NUM_KEY]]];
}

-(void) check:(id) value {
	if([value isKindOfClass:[NSError class]]) {
    [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", value] afterDelay:3.0f];
		return;
  }
  
  if([value isKindOfClass:[SoapFault class]]) {
    [SVProgressHUD dismissWithError:[value string]];
    return;
  }
  
  UPaidstatusResult *result = (UPaidstatusResult *) value;
  
  if ([result status] == 1) {
    [SVProgressHUD dismissWithSuccess:@"Rejestracja zakończona pomyślnie" afterDelay:2.0f];
    [self performSelector:@selector(backWithNotification) withObject:nil afterDelay:2.0];
  }
}

- (void) backWithNotification {
  [[NSNotificationCenter defaultCenter] postNotificationName:@"backFromRegister" object:self];
  [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:0] animated:YES];
}

@end
