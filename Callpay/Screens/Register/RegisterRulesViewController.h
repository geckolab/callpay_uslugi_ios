//
//  RegisterRulesViewController.h
//  Sprytny Bill
//
//  Created by uPaid on 4/7/13.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProjectViewController.h"
#import "UPaidLogInController.h"

@interface RegisterRulesViewController : ProjectViewController <UIAlertViewDelegate>

- (id)initWithData:(NSDictionary *) _dictionary;
@property (nonatomic, retain) NSDictionary *dictionary;

@end
