  //
//  main.m
//  Sprytny Bill
//
//  Created by uPaid on 12.04.2012.
//  Copyright (c) 2012 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

#if DEBUG == 0
#define DebugLog(...)
#elif DEBUG == 1
#define DebugLog(...) NSLog(__VA_ARGS__)
#endif

int main(int argc, char *argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
