//
//  AppDelegate.m
//  Sprytny Bill
//
//  Created by uPaid on 12.04.2012.
//  Copyright (c) 2012 uPaid. All rights reserved.
//

#import "AppDelegate.h"
#import "IntroScreen.h"
#import "UIColor+HexString.h"
#import "ProjectPopoverMenu.h"
#import "ProjectViewHelper.h"
#import "Item.h"
#import "ProjectMainScreenViewController.h"

#import "UpaidMPM.h"

#import "CPNavigationController.h"

#import "PRDBManager.h"

@implementation AppDelegate

@synthesize nav = _nav;
@synthesize window = _window;
@synthesize loginController;
@synthesize mainController;

- (void)dealloc {
    /*
  [self.window release];
  [self.nav release];
  [self.loginController release];
  [self.mainController release];
  [super dealloc];
     */
}

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [[PRDBManager object] initializeDB];
  self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

  UIViewController *viewControllerMain;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    viewControllerMain = [storyboard instantiateViewControllerWithIdentifier:@"MainMenuVC"];
  
  if (IS_TEST) {
//    viewControllerMain = [[NSClassFromString(START_CLASS) alloc] init];
  } else {
//    viewControllerMain = [[ProjectMainScreenViewController alloc] init];
  }
  
  IntroScreen *nkis = [[IntroScreen alloc] init];
  
  self.loginController = [[UINavigationController alloc] initWithRootViewController:nkis];
  self.mainController = [[CPNavigationController alloc] initWithRootViewController:viewControllerMain];
  [self.loginController.navigationBar setTranslucent:NO];
  [self.mainController.navigationBar setTranslucent:YES];
//  self.loginController.navigationBar.tintColor = [UIColor colorWithHexString:@"075080"];
//  self.mainController.navigationBar.tintColor = [UIColor colorWithHexString:@"075080"];
    self.mainController.navigationBar.tintColor = [UIColor clearColor];
  
  [self setupNavigationBar];
    
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
  
  
  if (IS_TEST) {
//    [TestFlight takeOff:@"18612e5f-f045-42f5-a177-2ced669c2132"];
  }
  
  if (WITH_LOGIN_SCREEN) {
      self.window.rootViewController = self.loginController;
  } else {
      self.window.rootViewController = self.mainController;
  }
    
//  [[UpaidMPM sharedInstance] setPartnerId:110 andAppName:@"MPM_TEST" andMerchant:@"Przewozyregionalne"];
    [[UpaidMPM sharedInstance] setPartnerId:121 andAppName:@"CallpayUslugi_TEST" andMerchant:@"test_callpay_uslugi"];

    
  //test version
//  [[UpaidMPM sharedInstance] setTestPartnerId:110];
    [[UpaidMPM sharedInstance] setTestPartnerId:121];
    
    UpaidMPMUIConfig *uiConfig = [[UpaidMPMUIConfig alloc] init];
    uiConfig.backgroundColor = [UIColor clearColor];
    uiConfig.basicColor = [UIColor whiteColor];
    uiConfig.loginViewControllerNibName = @"CustomMPMLoginViewController";
    uiConfig.payViewControllerNibName = @"CustomMPMPayViewController";
    uiConfig.cardAddViewControllerNibName = @"CustomMPMCardAddViewController";
    uiConfig.cardsListViewControllerNibName = @"CustomMPMCardsListViewController";
    uiConfig.masterpassSelectorCellNibName = @"CustomMPMSelectMasterPassTableViewCell";
    uiConfig.cardsListCellNibName = @"CustomMPMCardsListCell";
    uiConfig.cardVerifyViewControllerNibName = @"CustomMPMCardVerifyViewController";
    uiConfig.footerNibName = @"CPMPMFooter";
    [UpaidMPM sharedInstance].uiConfig = uiConfig;

  [self.window makeKeyAndVisible];
  
  // Remote Push notifications
//  [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
  
//  [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
  
  return YES;
}

+ (UIBarButtonItem *) customLeftButton: (NSString *) title delegate: (id) delegate action: (SEL) action {
  if (IS_IOS7_OR_LATER) {
    return [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:delegate action:action];
  }
  
//  UIImage *backImage = [[UIImage imageNamed:@"btn_back.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 13, 0, 5)];
    UIImage *backImage = [[UIImage imageNamed:@"nav_bar_back"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
  UIImage *backImagePressed = [[UIImage imageNamed:@"btn_back_pressed.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 13, 0, 5)];
  UIButton *back = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, backImage.size.width, backImage.size.height)];
  [back addTarget:delegate action:action forControlEvents:UIControlEventTouchUpInside];
  [back setBackgroundImage:backImage forState:UIControlStateNormal];
  [back setBackgroundImage:backImagePressed forState:UIControlStateHighlighted];
  [back setTitle:title forState:UIControlStateNormal];
  [[back titleLabel] setFont:[ProjectViewHelper fontNormalWithSize:13]];
  [back setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  [back setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
  
  UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:back];
  
  return backButton;
}

-(void) setupNavigationBar {
  UIImage *nextButton = [[UIImage imageNamed:@"btn.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 0, 5)];
  UIImage *nextPressedButton = [[UIImage imageNamed:@"btn_pressed.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 0, 5)];
//  UIImage *backButton = [[UIImage imageNamed:@"btn_back.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 13, 0, 5)];
    UIImage *backButton = [[UIImage imageNamed:@"nav_bar_back"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
  UIImage *backPressedButton = [[UIImage imageNamed:@"btn_back_pressed.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 13, 0, 5)];
//  UIImage *navBgImage = [UIImage imageNamed:@"nav_bar7.png"];
    UIImage *navBgImage = [UIImage imageNamed:@"nav_bar_callpay2"];
    [[UINavigationBar appearance] setBackgroundColor:[UIColor clearColor]];
    
  [[UINavigationBar appearance] setBackgroundImage:navBgImage forBarMetrics:UIBarMetricsDefault];
  
  
  if (!IS_IOS7_OR_LATER) {
//    [[UIBarButtonItem appearance] setBackgroundImage:nextButton forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
//    [[UIBarButtonItem appearance] setBackgroundImage:nextPressedButton forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
//    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:backButton forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
//    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:backPressedButton forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
    
//    [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:33.0/255.0 green:34.0/255.0 blue:35.0/255.0 alpha:1.0]];
      [[UINavigationBar appearance] setTintColor:[UIColor redColor]];
    
    [[UIBarButtonItem appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor], UITextAttributeTextColor,
      [ProjectViewHelper fontNormalWithSize:13.0], UITextAttributeFont,
      nil] forState:UIControlStateNormal];
    [[UIBarButtonItem appearance] setTitleTextAttributes:
   [NSDictionary dictionaryWithObjectsAndKeys:
    [UIColor whiteColor], UITextAttributeTextColor,
    [ProjectViewHelper fontNormalWithSize:13.0], UITextAttributeFont,
    nil] forState:UIControlStateHighlighted];
  }
}

//-(void) application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
//  NSUserDefaults *p = [NSUserDefaults standardUserDefaults];
//  
//  [p setValue:[[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] forKey:DEVICE_TOKEN_FOR_PUSH];
//  // UpaidLog(@"push ok %@", [p stringForKey:DEVICE_TOKEN_FOR_PUSH]);
//}
//
//-(void) application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
//  NSUserDefaults *p = [NSUserDefaults standardUserDefaults];
//  [p setValue:@"" forKey:DEVICE_TOKEN_FOR_PUSH];
//  // UpaidLog(@"push fail");
//}


-(void) removeLoginView {
    self.window.rootViewController = self.mainController;
}

-(void) addLoginView {
    self.window.rootViewController = self.loginController;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    if([_delegate respondsToSelector:@selector(applicationBecameActive)])
    {
        [_delegate applicationBecameActive];
    }
}

@end