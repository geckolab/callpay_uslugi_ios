//
//  Constants.h
//  uPaid
//
//  Created by uPaid on 05.12.2011.
//  Copyright (c) 2011 uPaid All rights reserved.
//

#import <Foundation/Foundation.h>

#define ENV_IS_TEST 1
#define APP_NAME @"CallPay"
#define BOK_EMAIL @"bok@callpay.pl"
#define APP_STORE_URL @"https://itunes.apple.com/us/app/callpay-us-ugi/id791490110?mt=8"
#define IS_IOS7_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
#define IS_IOS7 ([[UIDevice currentDevice].systemVersion hasPrefix:@"7"])
#define CALLPAY_FAQ_URL @"http://tools.upaid.pl/callpay/faq/faq.php"
#define CP_API_IS_WORKING NO

#ifdef ENV_IS_TEST
  #define PARTNER_ID 28
  #define WSDL_ADDRESS @"http://test.upaid.pl/soap/internal/v3.2/application:callpay/os:ios"
  #define IS_TEST YES
  #define SOAP_LOGGING YES
  #import "TicketBuyLastStepViewController.h"
  #define START_CLASS @"ProjectMainScreenViewController"
  #define WITH_LOGIN_SCREEN NO
//#define UPAID_URL_PREFIX @"https://upaid.pl"
//  #define UPAID_URL_PREFIX @"http://test_upaid:sB6IA89@test.upaid.pl"
//    #define UPAID_URL_PREFIX @"http://callpaytest.pl"
//#define UPAID_URL_PREFIX @"http://callpaytest.pl.192.168.0.201.xip.io"
#define UPAID_URL_PREFIX @"http://91.199.95.209:9090"
//#define UPAID_URL_SUFIX @"?dev=1"
#define UPAID_URL_SUFIX @""
  #define UpaidLog(...) NSLog(__VA_ARGS__)
  #define UpaidLogStack(...) NSLog(@"%s %@", __PRETTY_FUNCTION__, [NSString stringWithFormat:__VA_ARGS__])
#else
  #define UpaidLog(...) do { } while(0)
  #define UpaidLogStack(...) do { } while(0)
  #define PARTNER_ID 59
  #define WSDL_ADDRESS @"https://upaid.pl/soap/internal/v3.2/os:ios/application:callpay"
  #define IS_TEST NO
  #define SOAP_LOGGING NO
  #define START_CLASS @"ProjectMainScreenViewController"
  #define WITH_LOGIN_SCREEN YES
  #define UPAID_URL_PREFIX @"https://upaid.pl"
#define UPAID_URL_SUFIX @""
#endif

#define ADD_CARD_FROM_OTHER_CARDS 2
#define ADD_CARD_FIRST 3

UIKIT_EXTERN NSString *const PHONE_NUM_KEY;

UIKIT_EXTERN NSString *const PHONE_CODE_ASKED;
UIKIT_EXTERN NSString *const USER_HAS_CARDS;

UIKIT_EXTERN NSString *const STORE_SERVICE;
UIKIT_EXTERN NSString *const DEVICE_TOKEN_FOR_PUSH;

UIKIT_EXTERN NSString *const UPAID_PHONE_CODE;
UIKIT_EXTERN NSString *const UPAID_PHONE_NUMBER;
UIKIT_EXTERN NSString *const PHONE_NUM_KEY_TO_TOP_UP;
UIKIT_EXTERN NSString *const UPAID_LOGIN_STATUS;
UIKIT_EXTERN NSString *const UPAID_TOKEN_TIME;

UIKIT_EXTERN NSString *const UPAID_DEFAULT_CARD_ID;
UIKIT_EXTERN NSString *const UPAID_DEFAULT_CARD_NICKNAME;
UIKIT_EXTERN NSString *const UPAID_DEFAULT_CARD_TYPE;

UIKIT_EXTERN NSString *const UPAID_CARD_TO_PAY_ID;
UIKIT_EXTERN NSString *const UPAID_CARD_TO_PAY_PAN;
UIKIT_EXTERN NSString *const UPAID_CARD_TO_PAY_TYPE;
