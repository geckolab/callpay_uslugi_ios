//
//  ProjectTextField.m
//  Nakarm Prepaida
//
//  Created by uPaid on 18.02.2012.
//  Copyright (c) 2012 uPaid. All rights reserved.
//

#import "ProjectTextField.h"
//#import <UpaidExtensions/UITextField+Extensions.h>
#import "UITextField+Extensions.h"

@implementation ProjectTextField


- (UIColor *) textColor {
  return [UIColor colorWithHexString:@"2f3234"];
}

- (UIColor *) placeholderColor {
  return [UIColor colorWithHexString:@"bdbdbd"];
}

-(void)awakeFromNib {
  [self configWithPlaceholder: self.placeholder];
}

@end