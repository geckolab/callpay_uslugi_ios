//
//  ProjectPopoverMenu.m
//  Sprytny Bill
//
//  Created by uPaid on 21.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "ProjectPopoverMenu.h"
#import "SFHFKeychainUtils.h"
#import "AppDelegate.h"
#import "HelpWebViewViewController.h"
#import "OverlayHelp.h"

@implementation ProjectPopoverMenu


- (id) initWithDelegate: (UIViewController *) _delegate {
  self = [super initWithDelegate:_delegate];
  
  if (self) {
    [[ProjectPopoverMenu menuItems] removeAllObjects];
    
//    if ([OverlayHelp constructViewForController:_delegate]) {
//      [ProjectPopoverMenu addMenuItem:@"O ekranie" delegate: [OverlayHelp object] action:@selector(show:)];
//    }
    
    [ProjectPopoverMenu addMenuItem:@"Pomoc" delegate: self action:@selector(showHelp:)];
    [ProjectPopoverMenu addMenuItem:@"O aplikacji" delegate: self action:@selector(showInfo:)];
    [ProjectPopoverMenu addMenuItem:@"Wyloguj" delegate: self action:@selector(logOut:)];
  }
  
  return self;
}

- (void) showHelp: (id) sender {
  HelpWebViewViewController *help = [[HelpWebViewViewController alloc] init];
  [self.delegate.navigationController pushViewController:help animated:YES];
  //[help release];
}

- (void) showInfo: (id) sender {
  NSString *betaString = IS_TEST ? @"BETA " : @"";
  [ViewHelper showInfoAlert:[NSString stringWithFormat:@"Callpay %@ %@build %@\nhttp://www.callpay.pl\nDeveloper: partnerSheep.pl", [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"], betaString, [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey]]];
}

- (void) logOut: (id) sender {
  NSUserDefaults *p = [NSUserDefaults standardUserDefaults];
  [SFHFKeychainUtils deleteItemForUsername:[p objectForKey:PHONE_NUM_KEY] andServiceName:STORE_SERVICE error:nil];
  [p removeObjectForKey:UPAID_DEFAULT_CARD_NICKNAME];
  [p removeObjectForKey:UPAID_CARD_TO_PAY_PAN];
  [p removeObjectForKey:PHONE_NUM_KEY];
  
  AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
  
  [self.delegate.navigationController popToRootViewControllerAnimated:YES];
  
  [appDelegate addLoginView];
}

@end
