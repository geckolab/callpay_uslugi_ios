//
//  MobiLocationsDelegate.h
//  R-Mobi
//
//  Created by uPaid on 16.01.2013.
//  Copyright (c) 2013 Appocalypse.mobi Bartosz Wachowski. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ProjectLocations;
@protocol ProjectLocationsDelegate

- (void) onLocalizeCity: (NSString *) city;
- (void) onLocalizeCityError: (NSError *)error;

@end
