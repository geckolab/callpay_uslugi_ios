//
//  Created by uPaid on 19.02.2012.
//  Copyright (c) 2012 uPaid. All rights reserved.
//

#import "ProjectButton.h"

@implementation ProjectButton

- (UIColor *) labelShadowColor {
  return [UIColor colorWithHexString:@"f8f8f8"];
}

-(void)awakeFromNib {
  if (![self.titleLabel.text isEqualToString: @""]) {
    [self makeBigButton];
  }
}

@end