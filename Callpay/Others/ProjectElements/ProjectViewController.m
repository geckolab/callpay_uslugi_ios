//
//  ProjectViewController.m
//  Sprytny Bill
//
//  Created by uPaid on 27.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "ProjectViewController.h"
#import "ProjectPopoverMenu.h"
#import "UIColor+HexString.h"
#import "ProjectLabel.h"
#import "ProjectButton.h"
#import "UIView+Extensions.h"

#import "TicketsButton.h"

#import "AppDelegate.h"

@interface ProjectViewController ()

@property (nonatomic) NSMutableDictionary *cellHeights;

@end

@implementation ProjectViewController

- (void) viewDidLoad {
    self.cellHeights = @{}.mutableCopy;
  [super viewDidLoad];
//  self.view.backgroundColor = [UIColor colorWithHexString:@"ecebeb"];
    self.view.backgroundColor = [UIColor clearColor];
//    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Bg4"]];
}

- (void) addMenuBarButton {
//  ProjectPopoverMenu *menu = [[ProjectPopoverMenu alloc] initWithDelegate:self];
//  self.navigationItem.rightBarButtonItem = [menu createBarButton];
}

- (void) drawRectangle: (int) contentHeight posY: (int) posY inView: (UIView *) view {
  _posY = posY;
  [self addBackgroundImage: @"bg_list_top2.png" height:12 inView: view];
  [self addBackgroundImage: @"bg_list_center.png" height:contentHeight inView: view];
  [self addBackgroundImage: @"bg_list_bottom2.png" height:12 inView: view];
}

- (void) setupScrollView {
  [super setupScrollView:[[UIScreen mainScreen] applicationFrame].size.height - 45 frameHeight:[[UIScreen mainScreen] applicationFrame].size.height - 45];
}

- (void) drawRectangle: (int) contentHeight posY: (int) posY {
  [self drawRectangle:contentHeight posY:posY inView:self.view];
}

- (void) addBackgroundImage: (NSString *) image height: (int) height inView: (UIView *) view {
  UIImageView *bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed: image]];
  [bgImage setFrame:CGRectMake(10, _posY, 300, height)];
  [view addSubview:bgImage];
  _posY += height;
}

-(void) authActionError:(int)arg {
  [SVProgressHUD dismissWithError:@"Wystąpił błąd!" afterDelay:3.0f];
  [self.navigationController popViewControllerAnimated:YES];
}

- (void) setupTableView: (CGRect) rect {
  isBigTable = NO;
  [super setupTableView: rect];
  self.tableView.backgroundColor = [UIColor colorWithHexString:@"ecebeb"];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
  [cell setBackgroundColor:[UIColor clearColor]];
}

- (UITableViewCell *)tableView:(UITableView *)table cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  UITableViewCell *cell = [super tableView:table cellForRowAtIndexPath:indexPath];
  
  UIImageView *bgImage;
  int posY = 0;
  int posX = 10;
  int width = 300;
  
  if (indexPath.row == 0) {
    if ([[self getTableTitle] isEqualToString:@""]) {
      posY += 14;
    } else {
      posY += 44;
      ProjectLabel *titleLabel = [[ProjectLabel alloc] initWithFrame:CGRectMake(posX, 4, width, 27)];
      [titleLabel makeWithTitle: [self getTableTitle] andSize: 18];
      [titleLabel setLineBreakMode:NSLineBreakByTruncatingTail];
      [cell addSubview: titleLabel];
    }

  }
  
  [self setCellHeight: isBigTable ? 58 : 40 forRowAtIndexPath:indexPath];
    
  
  UIView *content = [self getCellContent:indexPath];
  [content setFrame:CGRectMake(posX, posY, width, [self contentHeightForIndexPath: indexPath])];
    
//    if (isBigTable) {
//        bgImage = [[[UIImageView alloc] initWithImage:[UIImage imageNamed: @"list_big.png"]] autorelease];
//    } else {
        bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed: @"list.png"]];
//    }
    
    [bgImage setFrame:CGRectMake(posX, posY, width, [self contentHeightForIndexPath: indexPath])];
    [cell addSubview:bgImage];
    [cell addSubview:content];
    //[content release];
  
  return cell;
}

- (void) setCellHeight: (CGFloat) height forRowAtIndexPath: (NSIndexPath *) indexPath {
    self.cellHeights[indexPath] = @(height);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  CGFloat height = [self contentHeightForIndexPath: indexPath];
  
  if (indexPath.row == 0) {
    if ([[self getTableTitle] isEqualToString:@""]) {
      height += 13;
    } else {
      height += 43;
    }
  }
    
  return height;
}

- (CGFloat) contentHeightForIndexPath: (NSIndexPath *) indexPath {
    if (self.cellHeights[indexPath] == nil && IS_IOS7) {
        [self getCellContent: indexPath];
    }
    
    if (self.cellHeights[indexPath] != nil) {
        return  (CGFloat) [self.cellHeights[indexPath] floatValue];
    }
    
    return isBigTable ? 58 : 40;
}

- (void) addButtonWithTitle: (NSString *) title andImageName: (NSString *) imageName andSelector: (SEL) selector atPosY: (int) posY {
//  ProjectButton *button = [[ProjectButton buttonWithType:UIButtonTypeCustom] autorelease];
  TicketsButton *button = [TicketsButton buttonWithType:UIButtonTypeCustom];
//  [button setFrame:CGRectMake(self.view.bounds.size.width/2 - 62, posY, 300, 50)];// - 150
    [button setFrame:CGRectMake(16, posY, (self.view.bounds.size.width - 32), 58)];// - 150
  [button setTitle:title forState:UIControlStateNormal];
  [button addImageToTitle:imageName];
//  [button makeBigButton];
    [button makeSmallButton];
  [button addTarget:self action:selector forControlEvents:UIControlEventTouchDown];
  [self.view addSubview:button];
}

- (UIImageView *) poweredBy {
  return [self poweredByWithShift:0];
}

- (UIImageView *) poweredByOnPosY: (int) posY {
  UIImage *powered = [UIImage imageNamed:@"powered-by"];
  return [self poweredByOnPosY:posY andPosX: [ProjectViewHelper screenSize].width - powered.size.width - 20];
}

- (UIImageView *) poweredByOnPosY: (int) posY andPosX: (int) posX {
  UIImage *powered = [UIImage imageNamed:@"powered-by"];
  UIImageView *view = [[UIImageView alloc] initWithFrame:CGRectMake(posX, posY, powered.size.width, powered.size.height)];
  [view setImage:powered];
  
  return view;
}

- (UIImageView *) poweredByWithShift: (int) shift {
  UIImage *powered = [UIImage imageNamed:@"powered-by"];
  int posY = [ProjectViewHelper screenSize].height - powered.size.height - 60 - shift;
  
  return [self poweredByOnPosY:posY];
}

-(CGFloat) getTopBarHeight
{
//    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
//    return appDelegate.window.frame.size.height - ([UIApplication sharedApplication].statusBarFrame.size.height + self.navigationController.navigationBar.frame.size.height);
    return ([UIApplication sharedApplication].statusBarFrame.size.height + self.navigationController.navigationBar.frame.size.height);
}
-(CGFloat) getWindowContentHeight
{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    return appDelegate.window.frame.size.height - [self getTopBarHeight];
}

@end
