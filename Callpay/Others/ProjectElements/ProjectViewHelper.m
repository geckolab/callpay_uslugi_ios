//
//  ProjectViewHelper .m
//  Sprytny Bill
//
//  Created by uPaid on 29.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "ProjectViewHelper.h"
#import "ProjectLabel.h"

@implementation ProjectViewHelper 

+ (UIFont *) fontNormalWithSize: (CGFloat) size {
  return [UIFont fontWithName:@"SegoeWP" size:size];
}

+ (UIFont *) fontBoldWithSize: (CGFloat) size {
  return [UIFont fontWithName:@"SegoeWP" size:size];
}

+ (UIFont *) fontMediumWithSize: (CGFloat) size {
  return [UIFont fontWithName:@"SegoeWP" size:size];
}

+ (UIFont *) fontItalicWithSize: (CGFloat) size {
  return [UIFont fontWithName:@"SegoeWP" size:size];
}

+ (BOOL) isSavedLocationForCity: (NSString *) cityIndex {
  if ([[NSUserDefaults standardUserDefaults] valueForKey:[NSString stringWithFormat: @"location_%@", cityIndex]] != nil) {
    return YES;
  } else {
    return NO;
  }
}

+ (BOOL) isSavedLocationForCityParking: (NSString *) cityIndex {
    if ([[NSUserDefaults standardUserDefaults] valueForKey:[NSString stringWithFormat: @"parking_location_%@", cityIndex]] != nil) {
        return YES;
    } else {
        return NO;
    }
}

+ (ProjectLabel *) pciDss: (int) posY {
  ProjectLabel *label = [[ProjectLabel alloc] initWithFrame:CGRectMake(5, posY, [self screenSize].width - 10, 25)];
  [label makeLightWithTitle:@"Twoje dane są bezpieczne. Gwarant bezpieczeństwa      PCI DSS" andSize:10];
  
  UIImageView *lock = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pci_lock.png"]];
  [lock setFrame:CGRectMake(248, 5, 8, 12)];
  [label addSubview:lock];
  
  return label;
}

+ (int) bottomBarShift {
  return IS_IOS7_OR_LATER ? 114 : 93;
}

@end