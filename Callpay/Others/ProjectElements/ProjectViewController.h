//
//  ProjectViewController.h
//  Sprytny Bill
//
//  Created by uPaid on 27.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "CustomViewController.h"

@interface ProjectViewController : CustomViewController {
    int _posY;
    BOOL isBigTable;
}

- (void) drawRectangle: (int) contentHeight posY: (int) posY;
- (void) drawRectangle: (int) contentHeight posY: (int) posY inView: (UIView *) view;
- (void) setupScrollView;
- (void) addMenuBarButton;
- (void) addButtonWithTitle: (NSString *) title andImageName: (NSString *) imageName andSelector: (SEL) selector atPosY: (int) posY;
- (CGFloat) contentHeightForIndexPath: (NSIndexPath *) indexPath;
- (UIImageView *) poweredByWithShift: (int) shift;
- (UIImageView *) poweredByOnPosY: (int) posY;
- (UIImageView *) poweredBy;
- (UIImageView *) poweredByOnPosY: (int) posY andPosX: (int) posX;
- (void) setCellHeight: (CGFloat) height forRowAtIndexPath: (NSIndexPath *) indexPath;

-(CGFloat) getTopBarHeight;
-(CGFloat) getWindowContentHeight;

@end
