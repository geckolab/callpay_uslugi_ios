//
//  CatalogsManagerDelegate.h
//  CallPay
//
//  Created by uPaid on 17.06.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CatalogsManager;
@protocol CatalogsManagerDelegate

- (void) catalogDownloadSuccess: (NSString *) cityIndex;
- (void) catalogDownloadFail: (NSString *) cityIndex;

@end
