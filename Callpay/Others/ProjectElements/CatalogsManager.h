//
//  CatalogsManager.h
//  CallPay
//
//  Created by uPaid on 17.06.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"
#import "CatalogsManagerDelegate.h"

@interface CatalogsManager : NSObject  {
  NSString *_cityIndex, *_currentCity;
  id <CatalogsManagerDelegate> _delegate;
}

@property (nonatomic, retain) NSMutableArray *catalogsToUpdate;

- (id) initWithDelegate: (id) delegate;
- (void) startDownload: (NSString *) cityIndex;
- (NSDictionary *) dictionaryForCityIndex: (NSString *) cityIndex;
- (void) deleteCatalogForCity: (NSString *) cityIndex;
- (BOOL) checkCatalogsCache;
- (NSString *) sizeToUpdate;
- (NSString *) nameForCurrentCity;
- (void) deleteCatalogsCache;
- (void) updateCatalogsCache;
- (void) updateCurrentCityCache;

- (NSDictionary *) parseJsonForCurrentCity ;

@end
