//
//  MobiLocations.h
//  R-Mobi
//
//  Created by uPaid on 16.01.2013.
//  Copyright (c) 2013 Appocalypse.mobi Bartosz Wachowski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "ProjectLocationsDelegate.h"

@interface ProjectLocations : NSObject <CLLocationManagerDelegate> {
  id __delegate;
}

#define LOCATIONS_SAVED_CITY @"LOCATIONS_SAVED_CITY"
#define LOCATIONS_LAST_USED @"LOCATIONS_LAST_USED"

#define LOCATIONS_SAVED_CITY_PARKING @"LOCATIONS_SAVED_CITY_PARKING"

@property (nonatomic,retain) IBOutlet CLLocationManager *locationManager;
@property (nonatomic,retain) IBOutlet CLGeocoder *geoCoder;

@property (nonatomic,retain) IBOutlet NSDictionary *locationList;
@property (nonatomic, strong) NSDictionary *parkingLocationList;
@property (nonatomic, strong) NSString *currentParkingCity;
@property (nonatomic,retain) NSString *currentCity;

@property (nonatomic, strong) NSString *currentCar;

- (void) localizeCityWithDelegate: (id) delegate;
+ (ProjectLocations *) object;

@end
