//
//  NakarmTitleLabel.m
//  Nakarm Prepaida
//
//  Created by uPaid on 19.02.2012.
//  Copyright (c) 2012 uPaid. All rights reserved.
//

#import "ProjectLabel.h"

@implementation ProjectLabel

- (UIColor *) normalColor {
  return [UIColor colorWithHexString:@"2f3234"];
}

- (UIColor *) lightColor {
  return [UIColor colorWithHexString:@"2f3234"];
}

- (UIColor *) mediumColor {
  return [UIColor colorWithHexString:@"2f3234"];
}

- (void)awakeFromNib {
  self.font = [UIFont fontWithName:@"SegoeWP" size:[self.font pointSize]];
  self.textColor = [self normalColor];
}

@end
