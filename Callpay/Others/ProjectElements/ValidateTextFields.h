//
//  ValidateTextFields.h
//  Sprytny Bill
//
//  Created by uPaid on 10.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ValidateTextFields : NSObject

typedef enum fieldNames {
  fieldTransfer = 1000,
  fieldPrice,
  fieldBillerName,
  fieldBillerStreet,
  fieldBillerStreetNumber,
  fieldBillerApartment,
  fieldBillerZipcode,
  fieldBillerCity,
  fieldBillerBankAcc,
  fieldClientName,
  fieldClientSurname,
  fieldClientStreet,
  fieldClientStreetNumber,
  fieldClientApartment,
  fieldClientZipcode,
  fieldClientCity,
} fieldNames;


+ (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
@end
