//
//  MobiLocations.m
//  R-Mobi
//
//  Created by uPaid on 16.01.2013.
//  Copyright (c) 2013 Appocalypse.mobi Bartosz Wachowski. All rights reserved.
//

#import "ProjectLocations.h"

//@implementation CLLocationManager (TemporaryHack)
//
//- (void)hackLocationFix
//{
//  CLLocation *location = [[CLLocation alloc] initWithLatitude:42 longitude:-50];
//  [[self delegate] locationManager:self didUpdateToLocation:location fromLocation:nil];
//  [location release];
//}
//
//- (void)startUpdatingLocation {
//  [self performSelector:@selector(hackLocationFix) withObject:nil afterDelay:0.1];
//}
//
//@end

@implementation ProjectLocations

@synthesize locationManager;
@synthesize geoCoder;
@synthesize locationList;
@synthesize currentCity = _currentCity;

@synthesize parkingLocationList;
@synthesize currentParkingCity = _currentParkingCity;

static ProjectLocations *object;

+ (ProjectLocations *) object {
  if (!object) {
    object = [[ProjectLocations alloc] init];
  }
  
  return object;
}

-(id) init {
  self = [super init];
  
    self.parkingLocationList = @{};
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if ([prefs objectForKey:LOCATIONS_SAVED_CITY_PARKING] != nil)
    {
        self.currentParkingCity = [prefs valueForKey:LOCATIONS_SAVED_CITY_PARKING];
    }
    else
    {
        self.currentParkingCity = @"";
    }
    self.currentCar = @"";
  self.locationList = @{};
  self.currentCity = @"";
  self.locationManager = [[CLLocationManager alloc] init];
  
  return self;
}

- (void) dealloc {
  self.locationManager = nil;
  self.geoCoder = nil;
  self.locationList = nil;
  
  //[super dealloc];
}

-(void) localizeCityWithDelegate: (id) delegate {
  __delegate = delegate;
  NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
  
  if ([prefs objectForKey:LOCATIONS_SAVED_CITY] != nil) {
    [__delegate onLocalizeCity: [prefs objectForKey:LOCATIONS_SAVED_CITY]];
  } else if (![self.currentCity isEqualToString:@""] && self.currentCity != nil) {
    [__delegate onLocalizeCity:self.currentCity];
  } else if ([prefs objectForKey:LOCATIONS_LAST_USED] != nil) {
    [__delegate onLocalizeCity: [prefs objectForKey:LOCATIONS_LAST_USED]];
  } else {
      if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
      {
          [self.locationManager requestWhenInUseAuthorization];
      }
    self.locationManager.delegate = self;
    [self.locationManager startUpdatingLocation];
  }
}

- (void) setCurrentCity:(NSString *) currentCity {
  if (![currentCity isEqualToString:@""]) {
    [self updateLastUsedCity:currentCity];
  }
  
  _currentCity = currentCity;
}

- (void) locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
  [self onLocalize];
}

- (void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
  [self onLocalize];
}

- (void) locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
  [self.locationManager stopUpdatingLocation];
  [__delegate onLocalizeCityError:error];
}

- (void) onLocalize {
  [self.locationManager stopUpdatingLocation];
  self.geoCoder = [[CLGeocoder alloc] init];
  [self.geoCoder reverseGeocodeLocation: self.locationManager.location completionHandler:
   ^(NSArray *placemarks, NSError *error) {
       NSString *city = [self getCityFromPlacemarks:placemarks];NSLog(@"placemarks %@",placemarks);
     
     [__delegate onLocalizeCity:city];
   }];
}

- (void) updateLastUsedCity: (NSString *) city {
  [[NSUserDefaults standardUserDefaults] setValue:city forKey:LOCATIONS_LAST_USED];
}

-(NSString *) getCityFromPlacemarks: (NSArray *) placemarks {
  CLPlacemark *placemark = [placemarks objectAtIndex:0];
  
  return [self replacePolishChars: placemark.locality];
}

-(NSString *) replacePolishChars: (NSString *) text {
  text = [text stringByReplacingOccurrencesOfString:@"ą" withString:@"a"];
  text = [text stringByReplacingOccurrencesOfString:@"ć" withString:@"c"];
  text = [text stringByReplacingOccurrencesOfString:@"Ć" withString:@"C"];
  text = [text stringByReplacingOccurrencesOfString:@"ę" withString:@"e"];
  text = [text stringByReplacingOccurrencesOfString:@"ł" withString:@"l"];
  text = [text stringByReplacingOccurrencesOfString:@"Ł" withString:@"L"];
  text = [text stringByReplacingOccurrencesOfString:@"ń" withString:@"n"];
  text = [text stringByReplacingOccurrencesOfString:@"Ń" withString:@"N"];
  text = [text stringByReplacingOccurrencesOfString:@"ó" withString:@"o"];
  text = [text stringByReplacingOccurrencesOfString:@"Ó" withString:@"O"];
  text = [text stringByReplacingOccurrencesOfString:@"ś" withString:@"s"];
  text = [text stringByReplacingOccurrencesOfString:@"Ś" withString:@"S"];
  text = [text stringByReplacingOccurrencesOfString:@"ź" withString:@"z"];
  text = [text stringByReplacingOccurrencesOfString:@"Ź" withString:@"Z"];
  text = [text stringByReplacingOccurrencesOfString:@"ż" withString:@"z"];
  text = [text stringByReplacingOccurrencesOfString:@"Ż" withString:@"Z"];
  text = [text stringByReplacingOccurrencesOfString:@" " withString:@"_"];
  
  return text;
}

-(void)setCurrentParkingCity:(NSString*)newCity
{
    _currentParkingCity = newCity;
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setValue:_currentParkingCity forKey:LOCATIONS_SAVED_CITY_PARKING];
    [prefs synchronize];
}

@end
