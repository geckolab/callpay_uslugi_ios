//
//  ValidateTextFields.m
//  Sprytny Bill
//
//  Created by uPaid on 10.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "ValidateTextFields.h"

@implementation ValidateTextFields

+ (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
  if ([string isEqualToString:@"|"] || [string isEqualToString:@"'"]){
    return NO;
  }
  
  //delete key pressed
  if ([string isEqualToString:@""]) {
    return YES;
  }
  
  if ([textField tag] == fieldBillerBankAcc) {
    if (textField.text.length == 26) {
      return NO;
    }
    
    return YES;
  }
  
  if ([textField tag] == fieldPrice) {
    NSMutableCharacterSet *priceDelimiters = [NSMutableCharacterSet characterSetWithCharactersInString:@".,"];
    NSInteger priceDelimitersLocation = [textField.text rangeOfCharacterFromSet:priceDelimiters].location;
    
    if (textField.text.length == 8 || (textField.text.length == 0 && [string isEqualToString:@"0"])) {
      return NO;
    }
    
    if ([[NSScanner scannerWithString:string] scanInt:nil]) {
      if (priceDelimitersLocation != NSNotFound && textField.text.length - priceDelimitersLocation == 3) {
        return NO;
      }
      
      return YES;
    }
    
    if ([[string stringByTrimmingCharactersInSet:priceDelimiters] isEqualToString:@""]
        && priceDelimitersLocation == NSNotFound) {
      return YES;
    }
    
    return NO;
  }
  
  if ([textField tag] == fieldClientZipcode || [textField tag] == fieldBillerZipcode) {
    if (textField.text.length == 6) {
      return NO;
    }
    
    if ([[NSScanner scannerWithString:string] scanInt:nil]) {
      return YES;
    }
    
    if ([string isEqualToString:@"-"] && [textField.text rangeOfString:@"-"].location == NSNotFound) {
      return YES;
    }
    
    return NO;
  }
  
  return YES;
}

@end
