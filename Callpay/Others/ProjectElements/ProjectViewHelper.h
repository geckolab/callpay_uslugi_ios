//
//  ProjectViewHelper .h
//  Sprytny Bill
//
//  Created by uPaid on 29.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "ViewHelper.h"
@class ProjectLabel;


@interface ProjectViewHelper : ViewHelper

+ (BOOL) isSavedLocationForCity: (NSString *) cityIndex;
+ (BOOL) isSavedLocationForCityParking: (NSString *) cityIndex;
+ (int) bottomBarShift;
+ (ProjectLabel *) pciDss: (int) posY;
@end
