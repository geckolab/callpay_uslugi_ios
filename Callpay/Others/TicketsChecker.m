//
//  TicketsChecker.m
//  CallPay
//
//  Created by Michał Majewski on 14.11.2014.
//  Copyright (c) 2014 uPaid. All rights reserved.
//

#import "TicketsChecker.h"
#import "ViewHelper.h"

@interface TicketsChecker ()

@property (nonatomic, retain) NSDictionary *numbersList;
@property (nonatomic) Ticket *ticket;
@property (nonatomic, retain) NSString *phoneNumber;

@end

@implementation TicketsChecker


- (instancetype)initWithTicket: (Ticket *) ticket {
    self = [super init];
    
    if (self) {
        self.ticket = ticket;
        self.numbersList = @{
                             @"Legnica" : @"767454155",
                             @"Koleje Śląskie" : @"327007939",
                             @"KZK GOP" : @"172509009",
                             @"ZTM Poznań" : @"616661065",
                             @"MZK Zamość" : @"845343033",
                             @"MZK Gorzów Wielkopolski" : @"957835059",
                             @"UM Grudziądz" : @"566812028",
                             @"ZTM Rzeszów" : @"172509009",
                             @"MPK Łódź" : @"422797624",
                             @"ZGK Swarzędz" : @"614151224",
                             @"MZK Ostrów" : @"625940049",
                             @"MPK Częstochowa" : @"343735129",
                             @"MZK Wejherowo" : @"586008020",
                             @"ZKM Olsztyn" : @"897228313",
                             @"ZMK Olsztyn 2" : @"897228313",
                             @"MKS Skarżysko Kamienna" : @"412412935",
                             @"MZK Piotrków Trybunalski" : @"447441170",
                             @"PGK Suwałki" : @"877351251",
                             @"ZTM Lublin" : @"818228469",
                             @"MZK Koszalin" : @"947213419",
                             @"KM Kołobrzeg" : @"947166819",
                             @"MZK Piła" : @"673450065",
                             @"ZTZ Rybnik" : @"324453934"
                             };

    }
    return self;
}

- (void) checkTicketFromPhone {
    if (self.ticket.verificationPhone != nil && ![self.ticket.verificationPhone isEqualToString:@""] && ![self.ticket.verificationPhone isEqualToString:@"-1"]) {
        return [self showInfoAlertForNumber:self.ticket.verificationPhone];
    }
    
    NSMutableString *numbersListToShow = [NSMutableString stringWithString: @"Znajdź swojego przewoźnika, a następnie zadzwoń pod bezpłatny numer podany przy przewoźniku. Po kilku sekundach otrzymasz SMS z pełnymi danymi biletu.\n"];
    
    for (NSString* key in self.numbersList) {
        if (self.ticket.seller != nil && [self.ticket.seller rangeOfString: key].location != NSNotFound) {
            return [self showInfoAlertForNumber:self.numbersList[key]];
        }
        
        [numbersListToShow appendFormat:@"\n%@ - %@", key, self.numbersList[key]];
    }
    
    [ViewHelper showInfoAlert:numbersListToShow];
}

- (void) showInfoAlertForNumber: (NSString *) number {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:[NSString stringWithFormat: @"Zadzwoń pod bezpłatny numer %@. Po kilku sekundach otrzymasz SMS z pełnymi danymi biletu.", number] delegate:self cancelButtonTitle:@"Anuluj" otherButtonTitles:@"Zadzwoń", nil];
    [alert show];
    //[alert release];
    self.phoneNumber = number;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", self.phoneNumber]]];
    }
}

@end
