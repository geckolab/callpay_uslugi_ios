//
//  TicketsChecker.h
//  CallPay
//
//  Created by Michał Majewski on 14.11.2014.
//  Copyright (c) 2014 uPaid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Ticket.h"

@interface TicketsChecker : NSObject

- (instancetype)initWithTicket: (Ticket *) ticket;
- (void) checkTicketFromPhone;

@end
