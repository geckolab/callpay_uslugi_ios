//
//  LogInAlertView.m
//  uPaid
//
//  Created by uPaid on 26.11.2011.
//  Copyright (c) 2011 uPaid. All rights reserved.
//

#import "LogInAlertView.h"
#import "UPaidMyWsdl.h"
#import "UPaidLogInController.h"
#import "SFHFKeychainUtils.h"


@interface LogInAlertView()

-(void) showLogInAlertPhoneNumber;
-(void) showLogInAlertPhoneNumberAfterError;
  
@end

@implementation LogInAlertView

-(id) initWithDelegate:(id) delegate {
  
  if (self = [super init]) {  
    __delegate = delegate; 
  }
  
  return self;
}

-(void) decideWhatToShowAtLogin {
  
  NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
  
  if ([prefs stringForKey:UPAID_PHONE_NUMBER] == nil) {
    [self showLogInAlertPhoneNumber];
  } else {
    [self showLogInAlertWithNameAndPassword];
  }
  
}

-(void) showLogInAlertPhoneNumber {
  
  [self initWithTitle:@"uPaid logowanie" 
              message:@"\n\n" 
             delegate:self 
    cancelButtonTitle:@"Anuluj" 
    otherButtonTitles:@"OK", nil];
  [self setTag:kAlertShowPhoneNumber];
  
  phoneField = [[UITextField alloc] initWithFrame:CGRectMake(16,65,252,25)];
  phoneField.font = [UIFont systemFontOfSize:18];
  phoneField.backgroundColor = [UIColor whiteColor];
  phoneField.keyboardAppearance = UIKeyboardAppearanceAlert;
  phoneField.placeholder = @"Wpisz numer telefonu";
  phoneField.keyboardType = UIKeyboardTypePhonePad;
  phoneField.delegate = __delegate;
  [self addSubview:phoneField];
    
  [self show];
}

-(void) showLogInAlertPhoneNumberAfterError {
  
  [self initWithTitle:@"uPaid logowanie" 
              message:@"\n\n\n" 
             delegate:self 
    cancelButtonTitle:@"Anuluj" 
    otherButtonTitles:@"OK", nil];
  [self setTag:kAlertShowPhoneNumber];
  
  UILabel *passwordLabel = [[UILabel alloc] initWithFrame:CGRectMake(12,40,260,25)];
  passwordLabel.font = [UIFont systemFontOfSize:16];
  passwordLabel.textColor = [UIColor whiteColor];
  passwordLabel.backgroundColor = [UIColor clearColor];
  passwordLabel.shadowColor = [UIColor blackColor];
  passwordLabel.shadowOffset = CGSizeMake(0,-1);
  passwordLabel.textAlignment = NSTextAlignmentCenter;
  passwordLabel.text = @"Zły numer telefonu.";
  [self addSubview:passwordLabel];
  
  phoneField = [[UITextField alloc] initWithFrame:CGRectMake(20,65,252,25)];
  phoneField.font = [UIFont systemFontOfSize:18];
  phoneField.backgroundColor = [UIColor whiteColor];
  phoneField.keyboardAppearance = UIKeyboardAppearanceAlert;
  phoneField.placeholder = @"Wpisz numer telefonu";
  phoneField.keyboardType = UIKeyboardTypePhonePad;
  phoneField.delegate = __delegate;
  [self addSubview:phoneField];

  
  [self show];
}

-(void) showLogInAlertWithNameAndPassword {

  [self initWithTitle:@"uPaid logowanie" 
                message:@"\n" 
               delegate:self 
      cancelButtonTitle:@"Anuluj" 
      otherButtonTitles:@"Zaloguj się", nil];
  [self setTag:kAlertWithNameAndPassword];  
  
  NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
  UILabel *passwordLabel = [[UILabel alloc] initWithFrame:CGRectMake(12,40,260,25)];
  passwordLabel.font = [UIFont systemFontOfSize:16];
  passwordLabel.textColor = [UIColor whiteColor];
  passwordLabel.backgroundColor = [UIColor clearColor];
  passwordLabel.shadowColor = [UIColor blackColor];
  passwordLabel.shadowOffset = CGSizeMake(0,-1);
  passwordLabel.textAlignment = NSTextAlignmentCenter;
  passwordLabel.text = [NSString stringWithFormat:@"+%@", [prefs stringForKey:UPAID_PHONE_NUMBER]];
  [self addSubview:passwordLabel];
    
  UIImageView *loginImage = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"passwordfield" ofType:@"png"]]];
  loginImage.frame = CGRectMake(11,79,262,31);
//  [self addSubview:loginImage];
    
  UIImageView *passwordImage = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"passwordfield" ofType:@"png"]]];
  passwordImage.frame = CGRectMake(11,116,262,31);
//  [self addSubview:passwordImage];
  
  nickField = [[UITextField alloc] initWithFrame:CGRectMake(16,83,252,25)];
  nickField.font = [UIFont systemFontOfSize:18];
  nickField.backgroundColor = [UIColor whiteColor];
  nickField.keyboardAppearance = UIKeyboardAppearanceAlert;
  nickField.enabled = NO;
  nickField.borderStyle = UITextBorderStyleRoundedRect;
  [nickField setText:[prefs stringForKey:UPAID_PHONE_NUMBER]];
  nickField.keyboardType = UIKeyboardTypePhonePad;
  nickField.delegate = __delegate;
  
//  [nickField becomeFirstResponder];
//  [self addSubview:nickField];
    
  passwordField = [[UITextField alloc] initWithFrame:CGRectMake(16,83,252,25)];
  passwordField.font = [UIFont systemFontOfSize:18];
  passwordField.backgroundColor = [UIColor whiteColor];
  passwordField.secureTextEntry = YES;
  passwordField.keyboardAppearance = UIKeyboardAppearanceAlert;
  passwordField.borderStyle = UITextBorderStyleRoundedRect;
  passwordField.placeholder = @"Wpisz hasło";
  passwordField.delegate = __delegate;
//  [self addSubview:passwordField];
  
  [self addButtonWithTitle:@"Załóż konto"];
  
  [self show];
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
  // UpaidLog(@"c %i", buttonIndex);
  NSUserDefaults *p = [NSUserDefaults standardUserDefaults];
  
  switch ([alertView tag]) {
      case kAlertShowPhoneNumber:
        if (buttonIndex == 1) {
          UPaidMyWsdl *w = [UPaidMyWsdl service];
          [w check:self 
            action:@selector(check:) 
        partner_id:PARTNER_ID 
             phone:phoneField.text];
        } 
    case 0:{
    }
      break;
    case kAlertWithNameAndPassword: {
      switch (buttonIndex) {
        case 1: {
          
          UPaidloginData *logData = [[UPaidloginData alloc] init];
          [logData setDevice_token:[p stringForKey:DEVICE_TOKEN_FOR_PUSH]];
          NSError *error;
          
          UPaidMyWsdl * w = [[UPaidMyWsdl alloc] init];
          NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
          [w login:self
            action:@selector(login:)
        partner_id:PARTNER_ID
             phone:[NSString stringWithFormat:@"48%@", [prefs stringForKey:PHONE_NUM_KEY]]
              code:[SFHFKeychainUtils getPasswordForUsername:[p objectForKey:PHONE_NUM_KEY] andServiceName:STORE_SERVICE error:&error]
            extras:logData];

//          [SVProgressHUD showWithStatus:@"Proszę czekać..." maskType:SVProgressHUDMaskTypeGradient];
            /*
          [w release];
          [logData release];
             */
        }
        break;
        case 2:{
          [__delegate createAccountViewStart];
        }
          break;
      default:
        break;
      }
      
    }
      break;
    case kUserLoginUpdateApp: {
      switch (buttonIndex) {
        case 1: {
          [[UIApplication sharedApplication] openURL:[NSURL URLWithString:APP_STORE_URL]];
        }
      }
    }
      break;
    default:
      break;
  }
  
}

-(void) login:(id) value {
	if([value isKindOfClass:[NSError class]]) {
//    [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", value] afterDelay:3.0f];
		return;
  }
  
  if([value isKindOfClass:[SoapFault class]]) {
//    [SVProgressHUD dismissWithError:[value string]];
    return;
  }			
  
  UPaidtokenResult* result = (UPaidtokenResult*)value;
    switch ([result status]) {
      case kUserLoginOK: {
//          [SVProgressHUD dismiss];
         NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setInteger:[result status] forKey:UPAID_LOGIN_STATUS];
        [UPaidLogInController writeTokenTimeToMemory:[result token]];

        if ([__delegate respondsToSelector:@selector(loggedInOK:)]) {
          [__delegate loggedInOK:value];
        }
      }
        break;
        
      case kUserLoginUpdateApp: {
        [self showLogInAlertUpdateApp];
      }
      default:
        [__delegate loggedInWithError:value];
//        [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"Błąd %i", [result status]] afterDelay:3.0f];
        break;
    }
  
}

-(void) check:(id) value {
	if([value isKindOfClass:[NSError class]]) {
		return;
  }
    
  if([value isKindOfClass:[SoapFault class]]) {
    return;
  }				
  
//  UPaidstatusResult* result = (UPaidstatusResult*)value;
}


-(void) showLogInAlertUpdateAppWithDelegate:(id)delegate {
//  [SVProgressHUD dismiss];
  UIAlertView *a = [[UIAlertView alloc] initWithTitle:@"" message:@"Aby skorzystać z aplikacji, zaktualizuj ją do najnowszej wersji!" delegate:delegate cancelButtonTitle:@"Anuluj" otherButtonTitles:@"Aktualizuj", nil];
  [a show];
  a.tag = kUserLoginUpdateApp;
  //[a release];
}

-(void) showLogInAlertUpdateApp {
  [self showLogInAlertUpdateAppWithDelegate:self];
}

-(void) decideWhatToDo:(int) status{}

@end
