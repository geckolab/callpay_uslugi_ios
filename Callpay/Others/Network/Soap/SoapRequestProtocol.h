//
//  SoapRequestProtocol.h
//  Fastino
//
//  Created by uPaid on 9/18/12.
//
//

#import <UIKit/UIKit.h>

@protocol SoapRequestProtocol <NSObject>

- (void)requestNeedsUserInteraction;
- (void)requestUserInteractionEnds;

@end