//
//  UPaidLogInControllerDelegate.h
//  uPaid
//
//  Created by uPaid on 15.12.2011.
//  Copyright (c) 2011 uPaid.pl All rights reserved.
//

#import <Foundation/Foundation.h>

@class UPaidLogInController;

@protocol UPaidLogInControllerDelegate

@optional
- (void) authActionProcess:(NSString *) arg;
- (void) authActionProcess:(NSString *) arg dictionary: (NSDictionary *) dictionary;
- (void) authActionError:(int) arg;

@end
