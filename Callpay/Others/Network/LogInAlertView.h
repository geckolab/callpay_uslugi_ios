//
//  LogInAlertView.h
//  uPaid
//
//  Created by uPaid on 26.11.2011.
//  Copyright (c) 2011 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LogInAlertViewDelegate.h"

typedef enum {
  kAlertDonTHaveAccount = 0,
  kAlertWithNameAndPassword = 1,
  kAlertShowPhoneNumber = 2,
  kAlertWrongPhoneNumber = 3,
} typeOfLogAlerts;

typedef enum {
  kUserIsInUPaid = 1,
  kUserNotFound = 2,
  kWrongPartnerId = 6,
  kNoPermissionsToPlatform = 7
} typeOfCheckResponse;

typedef enum {
  kUserLoginOK = 1,
  kUserLoginWrongCode = 2,
  kUserLoginAccountStopped = 3,
  kUserLoginNoUserWithPhoneNumb = 4,
  kUserLoginUserWithoutMobileAccess = 5,
  kUserLoginWrongPartnerId = 6,
  kUserLoginNoPermissionsToPlaform = 7,
  kUserLoginUpdateApp = 9
} typeOfLoginResponse;


@interface LogInAlertView : UIAlertView <UIAlertViewDelegate> {
  
  id __delegate;
  
  UITextField *nickField;  
  UITextField *passwordField;
  UITextField *phoneField;
  
  NSMutableString *currentElementValue, *currentElement;
  NSMutableString *status, *token;
}

-(id) initWithDelegate:(id) delegate;

-(void) decideWhatToShowAtLogin;

-(void) showLogInAlertPhoneNumber;
-(void) showLogInAlertPhoneNumberAfterError;

-(void) showLogInAlertWithNameAndPassword;
-(void) showLogInAlertUpdateAppWithDelegate: (id) delegate;
-(void) showLogInAlertUpdateApp;

@end
