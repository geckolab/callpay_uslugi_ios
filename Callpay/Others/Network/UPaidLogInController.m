//
//  UPaidLogInController.m
//  uPaid
//
//  Created by uPaid on 14.12.2011.
//  Copyright (c) 2011 uPaid.pl All rights reserved.
//

#import "UPaidLogInController.h"
#import "Reachability.h"
#import "SFHFKeychainUtils.h"
#import "LogInAlertView.h"

@implementation UPaidLogInController

NSString *const UPAID_TOKEN = @"token";

-(id) initWithDelegate:(id) delegate {
  if (self = [super init]) {  
    __delegate = delegate;
    _arg = nil;
    _dictionary = nil;
  }
  return self;
}

-(void) startAuthAction:(NSString *) arg{
  [self startAuthAction:arg dictionary:nil];
}

-(void) startAuthAction:(NSString *) arg dictionary: (NSDictionary *) dictionary{
  if ([arg isEqualToString:UPAID_LOGIN_STATUS]) {
    [self soapLogin];
    
  } else {
    if ([UPaidLogInController isTokenValid]) {
//      [SVProgressHUD showWithStatus:@"Proszę czekać..." maskType:SVProgressHUDMaskTypeGradient];
      
      if ([__delegate respondsToSelector:@selector(authActionProcess:dictionary:)]) {
        [__delegate authActionProcess:arg dictionary: dictionary];
      } else {
        [__delegate authActionProcess:arg];
      }
    } else {
      _arg = arg;
      _dictionary = dictionary;
      
      [self soapLogin];
    }
  }
}

- (void) soapLogin {
//  [SVProgressHUD showWithStatus:@"Proszę czekać..." maskType:SVProgressHUDMaskTypeGradient];
  NSError *error;
  NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
  //[p stringForKey:DEVICE_TOKEN_FOR_PUSH]
  UPaidMyWsdl *w = [UPaidMyWsdl service];
  UPaidloginData *logData = [[UPaidloginData alloc] init];
  [logData setDevice_token:[prefs stringForKey:DEVICE_TOKEN_FOR_PUSH]];
  
  [w login:self
    action:@selector(login:)
partner_id:PARTNER_ID
     phone:[NSString stringWithFormat:@"48%@", [prefs stringForKey:PHONE_NUM_KEY]]
      code:[SFHFKeychainUtils getPasswordForUsername:[prefs objectForKey:PHONE_NUM_KEY] andServiceName:STORE_SERVICE error:&error]
    extras:logData];
  
  //[logData release];
}

-(void) login:(id) value {
  //  [SVProgressHUD dismiss];
	if([value isKindOfClass:[NSError class]]) {
		return;
  }

  if([value isKindOfClass:[SoapFault class]]) {
    return;
  }			
  
  UPaidtokenResult * result = (UPaidtokenResult *) value;

  switch ([result status]) {
    case kUserLoginOK :{
      [UPaidLogInController writeTokenTimeToMemory:[result token]];
      if ([__delegate respondsToSelector:@selector(authActionProcess:dictionary:)]) {
        [__delegate authActionProcess:_arg dictionary: _dictionary];
      } else {
        [__delegate authActionProcess:_arg];
      }
    }
      break;
    case kUserLoginWrongCode: {
      if ([__delegate respondsToSelector:@selector(authActionError:)]) {
        [__delegate authActionError:[result status]];
      }
    }
      break;
    case kUserLoginUpdateApp: {
//      [SVProgressHUD dismiss];
      UIAlertView *a = [[UIAlertView alloc] initWithTitle:@"" message:@"Aby skorzystać z aplikacji, zaktualizuj ją do najnowszej wersji!" delegate:self cancelButtonTitle:@"Anuluj" otherButtonTitles:@"Aktualizuj", nil];
      [a show];
      a.tag = kUserLoginUpdateApp;
      //[a release];
    }
      break;
    default:
      if ([__delegate respondsToSelector:@selector(authActionError:)]) {
        [__delegate authActionError:[result status]];
      }
      break;
  }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
  if (alertView.tag == kUserLoginUpdateApp) {
    switch (buttonIndex) {
      case 1: {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:APP_STORE_URL]];
      }
    }
  }
}

+(NSString *) presentToken{
  NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
  return [prefs stringForKey:UPAID_TOKEN];
}

+(void) writeTokenTimeToMemory:(NSString *) token {
  
  NSDate *d = [[NSDate alloc] init];
  NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
  [prefs setValue:token forKey:UPAID_TOKEN];
  [prefs setValue:d forKey:UPAID_TOKEN_TIME];

}

+(BOOL) isTokenValid {
  NSDate *presentDate = [[NSDate alloc] init];
  NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
  NSDate *lastTime = [prefs valueForKey:UPAID_TOKEN_TIME];
  
  NSTimeInterval ti = [presentDate timeIntervalSinceDate:lastTime];

  if (ti/60.0 < 9.0)
    return YES;
  else
    return NO;

}

@end
