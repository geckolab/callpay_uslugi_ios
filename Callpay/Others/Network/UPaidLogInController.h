//
//  UPaidLogInController.h
//  uPaid
//
//  Created by uPaid on 14.12.2011.
//  Copyright (c) 2011 uPaid.pl All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UPaidLogInControllerDelegate.h"
#import "UPaidMyWsdl.h"

@interface UPaidLogInController : NSObject <UIAlertViewDelegate> {
  id __delegate;
  
  NSString *_arg;
  NSDictionary *_dictionary;
}

-(id) initWithDelegate:(id) delegate;

-(void) startAuthAction:(NSString *) arg;
-(void) startAuthAction:(NSString *) arg dictionary: (NSDictionary *) dictionary;

+(void) writeTokenTimeToMemory:(NSString *) token;
+(BOOL) isTokenValid;
+(NSString *) presentToken;

@end
