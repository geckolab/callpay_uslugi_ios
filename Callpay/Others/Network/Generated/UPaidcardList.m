/*
 UPaidcardList.h
 The implementation of properties and methods for the UPaidcardList array.
 Generated by SudzC.com
 */
#import "UPaidcardList.h"
#import "UPaidCard.h"

@implementation UPaidcardList

+ (id) newWithNode: (CXMLNode*) node
{
  return [[UPaidcardList alloc] initWithNode: node];
}

- (id) initWithNode: (CXMLNode*) node
{
  if(self = [self init]) {
    for(CXMLElement* child in [node children])
    {
      UPaidCard* value = [[UPaidCard newWithNode: child] object];
      if(value != nil) {
        [self addObject: value];
      }
    }
  }
  return self;
}


+ (NSMutableString*) serialize: (NSArray*) array
{
  NSMutableString* s = [NSMutableString string];
  for(id item in array) {
    [s appendString: [item serialize: @"Card"]];
  }
  return s;
}
@end
