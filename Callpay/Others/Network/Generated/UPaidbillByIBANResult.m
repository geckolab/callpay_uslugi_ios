/*
	UPaidbillByIBANResult.h
	The implementation of properties and methods for the UPaidbillByIBANResult object.
	Generated by SudzC.com
*/
#import "UPaidbillByIBANResult.h"

@implementation UPaidbillByIBANResult
	@synthesize status = _status;
	@synthesize ctn = _ctn;
  @synthesize amount = _amount;

	- (id) init
	{
		if(self = [super init])
		{

		}
		return self;
	}

	+ (UPaidbillByIBANResult*) newWithNode: (CXMLNode*) node
	{
		if(node == nil) { return nil; }
		return (UPaidbillByIBANResult*)[[UPaidbillByIBANResult alloc] initWithNode: node];
	}

	- (id) initWithNode: (CXMLNode*) node {
		if(self = [super initWithNode: node])
		{
			self.status = [[Soap getNodeValue: node withName: @"status"] intValue];
			self.ctn = [[Soap getNodeValue: node withName: @"ctn"] intValue];
      self.amount = [[Soap getNodeValue: node withName: @"amount"] intValue];
		}
		return self;
	}

	- (NSMutableString*) serialize
	{
		return [self serialize: @"billByIBANResult"];
	}
  
	- (NSMutableString*) serialize: (NSString*) nodeName
	{
		NSMutableString* s = [NSMutableString string];
		[s appendFormat: @"<%@", nodeName];
		[s appendString: [self serializeAttributes]];
		[s appendString: @">"];
		[s appendString: [self serializeElements]];
		[s appendFormat: @"</%@>", nodeName];
		return s;
	}
	
	- (NSMutableString*) serializeElements
	{
		NSMutableString* s = [super serializeElements];
		[s appendFormat: @"<status>%@</status>", [NSString stringWithFormat: @"%i", self.status]];
		[s appendFormat: @"<ctn>%@</ctn>", [NSString stringWithFormat: @"%i", self.ctn]];
    [s appendFormat: @"<amount>%@</amount>", [NSString stringWithFormat: @"%i", self.amount]];

		return s;
	}
	
	- (NSMutableString*) serializeAttributes
	{
		NSMutableString* s = [super serializeAttributes];

		return s;
	}
	
	-(BOOL)isEqual:(id)object{
		if(object != nil && [object isKindOfClass:[UPaidbillByIBANResult class]]) {
			return [[self serialize] isEqualToString:[object serialize]];
		}
		return NO;
	}
	
	-(NSUInteger)hash{
		return [Soap generateHash:self];

	}
	
	- (void) dealloc
	{
        /*
		[super dealloc];
         */
	}

@end
