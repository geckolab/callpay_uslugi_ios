/*
	UPaidtransactionRelated.h
	The implementation of properties and methods for the UPaidtransactionRelated object.
	Generated by SudzC.com
*/
#import "UPaidtransactionRelated.h"

@implementation UPaidtransactionRelated
	@synthesize product_name = _product_name;
	@synthesize service_provider_name = _service_provider_name;
	@synthesize amount = _amount;
	@synthesize barcode = _barcode;
	@synthesize basket = _basket;

	- (id) init
	{
		if(self = [super init])
		{
			self.product_name = nil;
			self.service_provider_name = nil;
			self.barcode = nil;

		}
		return self;
	}

	+ (UPaidtransactionRelated*) newWithNode: (CXMLNode*) node
	{
		if(node == nil) { return nil; }
		return (UPaidtransactionRelated*)[[UPaidtransactionRelated alloc] initWithNode: node];
	}

	- (id) initWithNode: (CXMLNode*) node {
		if(self = [super initWithNode: node])
		{
			self.product_name = [Soap getNodeValue: node withName: @"product_name"];
			self.service_provider_name = [Soap getNodeValue: node withName: @"service_provider_name"];
			self.amount = [[Soap getNodeValue: node withName: @"amount"] intValue];
			self.barcode = [Soap getNodeValue: node withName: @"barcode"];
			self.basket = [[Soap getNodeValue: node withName: @"basket"] intValue];
		}
		return self;
	}

	- (NSMutableString*) serialize
	{
		return [self serialize: @"transactionRelated"];
	}
  
	- (NSMutableString*) serialize: (NSString*) nodeName
	{
		NSMutableString* s = [NSMutableString string];
		[s appendFormat: @"<%@", nodeName];
		[s appendString: [self serializeAttributes]];
		[s appendString: @">"];
		[s appendString: [self serializeElements]];
		[s appendFormat: @"</%@>", nodeName];
		return s;
	}
	
	- (NSMutableString*) serializeElements
	{
		NSMutableString* s = [super serializeElements];
		if (self.product_name != nil) [s appendFormat: @"<product_name>%@</product_name>", [[self.product_name stringByReplacingOccurrencesOfString:@"\"" withString:@"&quot;"] stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"]];
		if (self.service_provider_name != nil) [s appendFormat: @"<service_provider_name>%@</service_provider_name>", [[self.service_provider_name stringByReplacingOccurrencesOfString:@"\"" withString:@"&quot;"] stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"]];
		[s appendFormat: @"<amount>%@</amount>", [NSString stringWithFormat: @"%i", self.amount]];
		if (self.barcode != nil) [s appendFormat: @"<barcode>%@</barcode>", [[self.barcode stringByReplacingOccurrencesOfString:@"\"" withString:@"&quot;"] stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"]];
		[s appendFormat: @"<basket>%@</basket>", [NSString stringWithFormat: @"%i", self.basket]];

		return s;
	}
	
	- (NSMutableString*) serializeAttributes
	{
		NSMutableString* s = [super serializeAttributes];

		return s;
	}
	
	-(BOOL)isEqual:(id)object{
		if(object != nil && [object isKindOfClass:[UPaidtransactionRelated class]]) {
			return [[self serialize] isEqualToString:[object serialize]];
		}
		return NO;
	}
	
	-(NSUInteger)hash{
		return [Soap generateHash:self];

	}
	
	- (void) dealloc
	{
		//if(self.product_name != nil) { [self.product_name release]; }
		//if(self.service_provider_name != nil) { [self.service_provider_name release]; }
		//if(self.barcode != nil) { [self.barcode release]; }
		//[super dealloc];
	}

@end
