//
//  UPaidloginData.h
//  R-Mobi
//
//  Created by uPaid on 30.01.2013.
//  Copyright (c) 2013 Appocalypse.mobi uPaid. All rights reserved.
//

#import "Soap.h"

@interface UPaidloginData : SoapObject {
	NSString* _device_token;
}

@property (retain, nonatomic) NSString* device_token;
@property BOOL terms_and_conditions;
@property (retain, nonatomic) NSString* currency;

+ (UPaidloginData*) newWithNode: (CXMLNode*) node;
- (id) initWithNode: (CXMLNode*) node;
- (NSMutableString*) serialize;
- (NSMutableString*) serialize: (NSString*) nodeName;
- (NSMutableString*) serializeAttributes;
- (NSMutableString*) serializeElements;

@end
