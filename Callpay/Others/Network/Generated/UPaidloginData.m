//
//  UPaidloginData.m
//  R-Mobi
//
//  Created by uPaid on 30.01.2013.
//  Copyright (c) 2013 Appocalypse.mobi uPaid. All rights reserved.
//

#import "UPaidloginData.h"

@implementation UPaidloginData

@synthesize device_token = _device_token;
@synthesize terms_and_conditions = _terms_and_conditions;
@synthesize currency = _currency;

- (id) init {
  
  if(self = [super init]) {
    self.device_token = nil;
    self.currency = nil;
  }
  
  return self;
}

+ (UPaidloginData*) newWithNode: (CXMLNode*) node
{
  if(node == nil) { return nil; }
  return (UPaidloginData*)[[UPaidloginData alloc] initWithNode: node];
}

- (id) initWithNode: (CXMLNode*) node {
  if(self = [super initWithNode: node]) {
    self.device_token = [Soap getNodeValue: node withName: @"device_token"];
  }
  
  return self;
}

- (NSMutableString*) serialize
{
  return [self serialize: @"extras"];
}

- (NSMutableString*) serialize: (NSString*) nodeName
{
  NSMutableString* s = [NSMutableString string];
  [s appendFormat: @"<%@", nodeName];
  [s appendString: [self serializeAttributes]];
  [s appendString: @">"];
  [s appendString: [self serializeElements]];
  [s appendFormat: @"</%@>", nodeName];
  return s;
}

- (NSMutableString*) serializeElements
{
  NSMutableString* s = [super serializeElements];
  
  if (self.device_token != nil) [s appendFormat: @"<userParam><key>device_token</key><value>%@</value></userParam>", [[self.device_token stringByReplacingOccurrencesOfString:@"\"" withString:@"&quot;"] stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"]];
  
  return s;
}

- (NSMutableString*) serializeAttributes
{
  NSMutableString* s = [super serializeAttributes];
  
  return s;
}

-(BOOL)isEqual:(id)object{
  if(object != nil && [object isKindOfClass:[UPaidloginData class]]) {
    return [[self serialize] isEqualToString:[object serialize]];
  }
  return NO;
}

-(NSUInteger)hash{
  return [Soap generateHash:self];
  
}

- (void) dealloc
{
    /*
  if(self.device_token != nil) { [self.device_token release]; }
  if(self.currency != nil) { [self.currency release]; }
  [super dealloc];
     */
}
@end
