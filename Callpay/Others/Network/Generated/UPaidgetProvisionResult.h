/*
	UPaidgetProvisionResult.h
	The interface definition of properties and methods for the UPaidgetProvisionResult object.
	Generated by SudzC.com
*/

#import "Soap.h"
	

@interface UPaidgetProvisionResult : SoapObject
{
	int _status;
	int _provision;
	BOOL _badCard;
	
}
		
	@property int status;
	@property int provision;
	@property BOOL badCard;

	+ (UPaidgetProvisionResult*) newWithNode: (CXMLNode*) node;
	- (id) initWithNode: (CXMLNode*) node;
	- (NSMutableString*) serialize;
	- (NSMutableString*) serialize: (NSString*) nodeName;
	- (NSMutableString*) serializeAttributes;
	- (NSMutableString*) serializeElements;

@end
