/*
	UPaidCard.h
	The interface definition of properties and methods for the UPaidCard object.
	Generated by SudzC.com
*/

#import "Soap.h"
	

@interface UPaidCard : SoapObject
{
	NSString *__id;
	NSString* _pan;
	int _default;
	BOOL _auth_type;
  NSString* _card_type;
  BOOL _authorized;
  BOOL _freezed;
  NSString *_nickName;
  NSString *_expDate;
  NSString *_smName;
  BOOL _locked;
	
}
		
	@property (retain, nonatomic) NSString *_id;
	@property (retain, nonatomic) NSString* pan;
  @property (retain, nonatomic) NSString* nickName;
  @property (retain, nonatomic) NSString* expDate;
  @property (retain, nonatomic) NSString* smName;
	@property int ddefault;
	@property BOOL auth_type;
  @property BOOL authorized;
  @property BOOL freezed;
  @property (retain, nonatomic) NSString* card_type;
  @property BOOL locked;

	+ (UPaidCard*) newWithNode: (CXMLNode*) node;
	- (id) initWithNode: (CXMLNode*) node;
	- (NSMutableString*) serialize;
	- (NSMutableString*) serialize: (NSString*) nodeName;
	- (NSMutableString*) serializeAttributes;
	- (NSMutableString*) serializeElements;

@end
