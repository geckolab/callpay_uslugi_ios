//
//  LogInAlertViewDelegate.h
//  uPaid
//
//  Created by uPaid on 26.11.2011.
//  Copyright (c) 2011 uPaid. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LogInAlertView;

@protocol LogInAlertViewDelegate

@optional
-(void) createAccountViewStart;

-(void) loggedInOK:(id) value;
-(void) loggedInWithError:(id) value;

@end