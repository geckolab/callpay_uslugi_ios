//
//  AddCardViewController.m
//  Nakarm Prepaida
//
//  Created by uPaid on 16.02.2012.
//  Copyright (c) 2012 uPaid. All rights reserved.
//

#import "BaseAddCardViewController.h"
#import "ProjectLabel.h"
#import "ProjectButton.h"
#import "UPaidLogInController.h"
#import "AppDelegate.h"
#import "ProjectViewHelper.h"

@implementation BaseAddCardViewController

@synthesize cardNumberField;
@synthesize cardMonthField;
@synthesize cardYearField;
@synthesize state;
@synthesize cardNicknameField;

static int CARD_NUMBER_T = 0;
static int CARD_DATE_T = 1;
static int CARD_DATE_Y = 2;
static int CARD_DATE_NICKNAME = 3;

///Duration of hiding/showing animation
#define ANIMATION_DURATION 0.25f

//do testów
- (id) init {
  self = [self initWithState:ADD_CARD_FIRST];
  
  return self;
}

- (id)initWithState:(int) _state
{
  self = [super init];
  
  if (self) {
    [self setState:_state];
    
    NSDate *date = [NSDate date];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSYearCalendarUnit | NSMonthCalendarUnit fromDate:date];
    currentYear = [components year];
    currentMonth = [components month];
    months = [[NSMutableArray alloc] init];
    
    for (int i = 1; i <= 12; i++) {
      [months addObject: [NSString stringWithFormat:@"%02d", i]];
    }
    
    years = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < 100; i++) {
      [years addObject: [NSString stringWithFormat:@"%02d", currentYear + i]];
    }
    
    UITapGestureRecognizer *tapScroll = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
    tapScroll.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapScroll];
    //[tapScroll release];
  }
  
  return self;
}

-(IBAction)addCardClicked:(id)sender {
  if (([self state] == ADD_CARD_FROM_OTHER_CARDS || [self state] == ADD_CARD_FIRST) && [self checkIfDataOK]) {
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient networkIndicator:YES];
    upaidLogin = [[UPaidLogInController alloc] initWithDelegate:self];
    [upaidLogin startAuthAction:[NSString stringWithFormat:@"%i",[self state]]];
  }
}

-(BOOL) checkIfDataOK {
  NSString *message = @"";
  
  if ([[self.cardMonthField text] isEqualToString:@""]) {
    message = @"Pole Miesiąc jest puste";
  } else if ([[self.cardYearField text] isEqualToString:@""]) {
    message = @"Pole Rok jest puste";
  } else if ([[self.cardNumberField text] isEqualToString:@""]) {
    message = @"Pole Numer karty jest puste";
  } else if ([[self.cardNicknameField text] isEqualToString:@""]) {
    message = @"Pole Nazwa karty jest puste";
  }
  
  if ([message isEqualToString:@""]) {
    return YES;
  } else {
    UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:@"Błąd podczas dodawania karty" message: message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alerView show];
    //[alerView release];
    
    return NO;
  }
}

-(void) authActionProcess:(NSString *)arg {
  UPaidMyWsdl *s = [UPaidMyWsdl service];
  [s cardAdd:self action:@selector(addCard:)
       token:[UPaidLogInController presentToken]
         pan:self.cardNumberField.text
    exp_date:[NSString stringWithFormat:@"%@/%@", self.cardMonthField.text, [self.cardYearField.text substringFromIndex:[self.cardYearField.text length] - 2]]
    nickname: self.cardNicknameField.text];
}

-(void) addCard:(id) value {
	if([value isKindOfClass:[NSError class]]) {
    [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", value] afterDelay:3.0f];
		return;
  }
  
  if([value isKindOfClass:[SoapFault class]]) {
    return;
  }
  
  UPaidcardAddResult *result = (UPaidcardAddResult *) value;
  
  switch ([result status]) {
    case kUserAddCardOK: {
      addedCardId = [result _id];
      NSUserDefaults *p = [NSUserDefaults standardUserDefaults];
      [p setValue:[NSString stringWithFormat:@"%i", [result _id]] forKey:UPAID_CARD_TO_PAY_ID];
      [p setValue:self.cardNicknameField.text forKey:UPAID_CARD_TO_PAY_PAN];
      [p setValue: [result cardType] forKey:UPAID_CARD_TO_PAY_TYPE];
      [p setBool:YES forKey:USER_HAS_CARDS];
      
      if (state == ADD_CARD_FIRST) {
        [SVProgressHUD dismissWithSuccess:@"Karta dodana pomyślnie." afterDelay:3.0f];
        [self performSelector:@selector(back) withObject:nil afterDelay:3.0f];
      } else if (state == ADD_CARD_FROM_OTHER_CARDS) {
        [SVProgressHUD dismiss];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:APP_NAME message: @"Karta dodana pomyślnie. Czy chcesz ją teraz zweryfikować?" delegate:self cancelButtonTitle:@"Anuluj" otherButtonTitles:@"OK", nil];
        [alertView setTag: 99];
        [alertView show];
        //[alertView release];
      }
    }
      break;
    case kUserAddCardCardWrongNumb: {
      [SVProgressHUD dismissWithError:@"Nie udało się dodać karty - nieprawidłowe dane" afterDelay:3.0f];
    }
      break;
    case kUserAddCardCardsIsBusy: {
      [SVProgressHUD dismissWithError:@"Nieprawidłowy numer karty" afterDelay:3.0f];
    }
      break;
  }
}

- (void) back {
  [self.navigationController popViewControllerAnimated:YES];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
  if (alertView.tag == 99) { //czy chcesz autoryzowac kartę
    if (buttonIndex == 0) { //przycisk anuluj
      [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex: [self.navigationController.viewControllers count] - 3] animated:YES];
    } else { //przycisk ok
      [[NSNotificationCenter defaultCenter] postNotificationName:@"startAuthorize" object:self];
      [self.navigationController popViewControllerAnimated:YES];
    }
  }
}

#pragma mark - View lifecycle

- (void)viewDidLoad{
  ProjectLabel *nb = [[ProjectLabel alloc] initWithFrame:CGRectMake(30, 10, self.view.bounds.size.width - 60, 30)];
  [nb setNumberOfLines:2];
  [nb makeWithTitle:@"Dodaj kartę" andSize:18];
  
  [self.view addSubview:nb];
  //[nb release];
  
  self.cardNumberField = [[ProjectTextField alloc] initWithPlaceholder:@"Numer karty" posY:50];
  [self.cardNumberField setKeyboardType:UIKeyboardTypeNumberPad];
  [self.cardNumberField setPlaceholder:@"Numer karty"];
  [self.cardNumberField setTag:CARD_NUMBER_T andDelegate:self];
  [self.cardNumberField setText:@""];
  [self.cardNumberField addTarget:self action:@selector(buttonDoneClicked:) forControlEvents:UIControlEventEditingDidEndOnExit];
  [self.view addSubview:self.cardNumberField];
  
  self.cardMonthField = [[ProjectTextField alloc] initWithPlaceholder:@"Miesiąc ważności karty" posY:90];
  [self.cardMonthField setFrame:CGRectMake(self.cardMonthField.frame.origin.x, self.cardMonthField.frame.origin.y, self.cardMonthField.frame.size.width / 2 - 5, self.cardMonthField.frame.size.height)];
  [self.cardMonthField setKeyboardType:UIKeyboardTypeNumberPad];
  [self.cardMonthField setTag:CARD_DATE_T andDelegate:self];
  [self.cardMonthField setText:@""];
  [self.cardMonthField addTarget:self action:@selector(buttonDoneClicked:) forControlEvents:UIControlEventEditingDidEndOnExit];
  [self.view addSubview:self.cardMonthField];
  
  self.cardYearField = [[ProjectTextField alloc] initWithPlaceholder:@"Rok ważności karty" posY:90];
  [self.cardYearField setFrame:CGRectMake(self.cardYearField.frame.size.width / 2 + 15, self.cardYearField.frame.origin.y, self.cardYearField.frame.size.width / 2 - 5, self.cardYearField.frame.size.height)];
  [self.cardYearField setKeyboardType:UIKeyboardTypeNumberPad];
  [self.cardYearField setTag:CARD_DATE_Y andDelegate:self];
  [self.cardYearField setText:@""];
  [self.cardYearField addTarget:self action:@selector(buttonDoneClicked:) forControlEvents:UIControlEventEditingDidEndOnExit];
  [self.view addSubview:self.cardYearField];
  
  self.cardNicknameField = [[ProjectTextField alloc] initWithPlaceholder:@"Nazwa karty np. kredytówka" posY:130];
  [self.cardNicknameField setTag:CARD_DATE_NICKNAME andDelegate:self];
  [self.cardNicknameField setText:@""];
  [self.cardNicknameField addTarget:self action:@selector(buttonDoneClicked:) forControlEvents:UIControlEventEditingDidEndOnExit];
  [self.view addSubview:self.cardNicknameField];
  
  [self createLogos:175];
  
//  datePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 210, 135, 162)];
//  [datePicker setDataSource:self];
//  [datePicker setDelegate:self];
//  [datePicker selectRow:currentMonth - 1 inComponent:0 animated:NO];
//  [self.view addSubview:datePicker];
  
  ProjectLabel *label = [[ProjectLabel alloc] initWithFrame:CGRectMake(10, 210, 300, 120)];
  [label setNumberOfLines:0];
  [label makeMediumWithTitleAlignLeft:@"Rejestracja tej karty umożliwi dokonanie tylko jednej płatności. Za chwilę otrzymasz maila, w którym wytłumaczymy jak zweryfikować Twoją kartę, abyś mógł płacić nią bez żadnych ograniczeń." andSize:15];
  [self.view addSubview:label];

  [self.view addSubview:[ProjectViewHelper pciDss:[ProjectViewHelper screenSize].height - 122]];

  ProjectButton *addCard = [[ProjectButton alloc] init];
  [addCard setFrame:CGRectMake(10, [ProjectViewHelper screenSize].height - 100, 300, 50)];
  [addCard setTitle:@"Dodaj" forState:UIControlStateNormal];
  [addCard makeBigButton];
  [addCard addTarget:self action:@selector(addCardClicked:) forControlEvents:UIControlEventTouchDown];
  [self.view addSubview:addCard];
  
  [super viewDidLoad];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
  return 2;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
  if (component == 0) {
    return 45;
  } else {
    return 70;
  }
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
  if (component == 0 && [datePicker selectedRowInComponent:1] == 0 && row < currentMonth - 1) {
    [datePicker selectRow:(currentMonth - 1) inComponent:component animated:YES];
  }
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
  if (component == 0) {
    return [months count];
  } else {
    return [years count];
  }
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
  if (component == 0) {
    return [months objectAtIndex:row];
  } else {
    return [years objectAtIndex:row];
  }
}

- (void) createLogos: (int) posY {
  UIImageView *ecardView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"koszyk_logo_ecard.png"]];
  [ecardView setFrame:CGRectMake(20, posY, 28, 32)];
  [self.view addSubview:ecardView];
  
//  UIImageView *masterLogo = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"koszyk_logo_mcm.png"]];
//  [masterLogo setFrame:CGRectMake(55, posY, 41, 32)];
//  [self.view addSubview:masterLogo];
}

-(IBAction)tapped:(id)sender {
  [self.view endEditing:YES];
}

-(BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
  if ([string length] == 0) {
    return YES;
  }
  
  if ([textField tag] == CARD_NUMBER_T && textField.text.length == 16) {
    return NO;
  }
  
  if ([textField tag] == CARD_DATE_T && textField.text.length == 2) {
    return NO;
  }
  
  if ([textField tag] == CARD_DATE_Y && textField.text.length == 4) {
    return NO;
  }
  
  return YES;
}

-(IBAction)buttonDoneClicked:(id)sender {
  [self.view endEditing:YES];
}

- (void) dealloc {
  //[self.cardMonthField  release];
  //[self.cardNicknameField release];
  //[self.cardNumberField release];
  //[self.cardYearField release];

  //upaidLogin = nil;
  
  //[super dealloc];
}

@end