//
//  AddCardViewController.h
//  Nakarm Prepaida
//
//  Created by uPaid on 16.02.2012.
//  Copyright (c) 2012 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UPaidLogInController.h"
#import "UPaidMyWsdl.h"
#import "ProjectTextField.h"
#import "UPaidLogInController.h"
#import "ProjectViewController.h"

typedef enum {
  kUserRegisterOK = 1,
  kUserRegisterEmailBusy = 2,
  kUserRegisterWrongFormat = 3,
  kUserRegisterCardsIsBusy = 4,
  kUserRegisterCardWrongNumb = 5,
  kUserRegisterWrongPartnerID = 6,
  kUserRegisterNoPermToPlatform = 7,
  kUserRegisterEmptyObligatoryField = 8
} typeOfRegisterResponse;

typedef enum {
  kUserAddCardOK = 1,
  kUserAddCardCardWrongNumb = 2,
  kUserAddCardCardsIsBusy = 3,
  kUserAddCardSessionDead = 5,
} typeOfAddCardResponse;

@interface BaseAddCardViewController : ProjectViewController <UITextFieldDelegate, UIAlertViewDelegate, UPaidLogInControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource> {
  UPaidLogInController *upaidLogin;
  int addedCardId;
  NSMutableArray *dates, *years, *months;
  NSInteger currentYear, currentMonth;
  UIPickerView *datePicker;
}

- (id)initWithState:(int) _state;
- (BOOL) checkIfDataOK;

@property (nonatomic, readwrite) int state;
@property (retain) ProjectTextField *cardNumberField,  *cardMonthField, *cardYearField, *cardNicknameField;

@end
