//
//  NSData+AES256.h
//  MobileTournament
//
//  Created by Michał Majewski on 19.07.2013.
//  Copyright (c) 2013 Michał Majewski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (AES256)

- (NSData *)AES256EncryptWithKey:(NSString *)key;
- (NSData *)AES256DecryptWithKey:(NSString *)key;

@end
