//
//  CustomAppDelegate.h
//  Sprytny Bill
//
//  Created by uPaid on 31.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAppDelegate : UIResponder

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

- (void) checkVersion;
- (void) updateToVersion: (NSString *) newVersion fromVersion: (NSString *) previousVersion;

@end
