//
//  ViewHelper.h
//  Sprytny Bill
//
//  Created by uPaid on 11.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ProjectButton;
@class ProjectLabel;
@class ProjectTextField;

@interface ViewHelper : NSObject


+ (CGSize) screenSize;
+ (NSString *) statusSubstatusInfo: (int) status substatus: (int) substatus;
+ (void) showInfoAlert: (NSString *) message;
+ (NSString *) cuttedNameWithAmount: (NSString *) name price: (NSString *) price cutNameAfter: (int) cutAfterLength;

+ (UIImage *)imageWithImage:(UIImage *)image scaledToMaxWidth:(CGFloat)width maxHeight:(CGFloat)height;
+ (UIImageView *) imageView: (NSString *) imageName atPosY: (int) posY;

+ (UIFont *) fontNormalWithSize: (CGFloat) size;
+ (UIFont *) fontBoldWithSize: (CGFloat) size;
+ (UIFont *) fontMediumWithSize: (CGFloat) size;
+ (UIFont *) fontItalicWithSize: (CGFloat) size;


+ (NSString *) formattedPhoneNumber: (NSString *) phone;
+ (UIImageView *) addLine: (int) posY;

@end
