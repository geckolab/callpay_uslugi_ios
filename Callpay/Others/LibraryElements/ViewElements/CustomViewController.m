//
//  CustomViewController.m
//  Sprytny Bill
//
//  Created by uPaid on 12.04.2012.
//  Copyright (c) 2012 uPaid. All rights reserved.

#import "CustomViewController.h"


@implementation CustomViewController

@synthesize scrollView;
@synthesize tableView;
@synthesize registeredForKeyboardNotifications;

- (id)init
{
  self = [super init];
  if (self) {
    currentTag = 0;
  }
  return self;
}

- (void)dealloc {
  tapRecognizer = nil;
  self.activeTxtField = nil;
  
  if (self.scrollView != nil) {
    //[self.scrollView release];
  }
  
  if (self.tableView != nil) {
    //[self.tableView release];
  }
  
  //[super dealloc];
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
  [self addBackButton];
  
  [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  [self.view endEditing:YES];
  [self addMenuBarButton];
}

- (void)viewDidUnload
{
  [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
  return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) addBackButton {
  UIBarButtonItem *temporaryBarButtonItem = [[UIBarButtonItem alloc] init];
  temporaryBarButtonItem.title = @"";//@"Powrót";
//  [temporaryBarButtonItem setImage:[[UIImage imageNamed:@"new_navi_back"] resizableImageWithCapInsets:UIEdgeInsetsMake(-10,0,0,0)]];
    [temporaryBarButtonItem setImage:[UIImage imageNamed:@"new_navi_back"]];
  self.navigationItem.backBarButtonItem = temporaryBarButtonItem;
  //[temporaryBarButtonItem   release];
}

- (void) addMenuBarButton {}

#pragma mark -
#pragma mark UITextField Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
  self.activeTxtField = (UITextField *) textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
  self.activeTxtField = nil;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
  NSInteger nextTag = textField.tag + 1;
  UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
  [self.view endEditing:YES];
  
  if (nextResponder) {
    [nextResponder becomeFirstResponder];
  }
  
  return NO;
}

#pragma mark -
#pragma mark Keyboard Management

- (void)keyboardDidShow:(NSNotification *)notification {}
- (void)keyboardDidHide:(NSNotification *)notification {}

- (void)keyboardWillShow:(NSNotification *)notification {
  [self.scrollView addGestureRecognizer: tapRecognizer];
  
  CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
  
  UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardFrame.size.height + [self keyboardShift], 0.0);
  self.scrollView.contentInset = contentInsets;
  self.scrollView.scrollIndicatorInsets = contentInsets;
  
//  [self.view addGestureRecognizer:tapRecognizer];
//  
//  NSDictionary* info = [notification userInfo];
//  CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//  
//  UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
//  self.scrollView.contentInset = contentInsets;
//  self.scrollView.scrollIndicatorInsets = contentInsets;
//  
//  // If active text field is hidden by keyboard, scroll it so it's visible
//  // Your application might not need or want this behavior.
//  CGRect aRect = self.view.frame;
//  aRect.size.height -= kbSize.height;
//  CGPoint origin = self.activeTxtField.frame.origin;
//  origin.y -= self.scrollView.contentOffset.y;
//  
//  if (!CGRectContainsPoint(aRect, origin) ) { 
//    CGPoint scrollPoint = CGPointMake(0.0, self.activeTxtField.frame.origin.y-(aRect.size.height) + 35 + [self keyboardShift]);
//    if (scrollPoint.x < 0) {
//      scrollPoint.x = 0;
//    }
//    [self.scrollView setContentOffset:scrollPoint animated:NO];
//  }
}

- (int) keyboardShift {
  return 0;
}

- (void)keyboardWillHide:(NSNotification *)notification {
  [self.scrollView removeGestureRecognizer:tapRecognizer];
  self.scrollView.contentInset = UIEdgeInsetsZero;
  self.scrollView.scrollIndicatorInsets = UIEdgeInsetsZero;
}

- (void) registerForKeyboardNotifications {
  if (!self.registeredForKeyboardNotifications) {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
    self.registeredForKeyboardNotifications = YES;
    
    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapAnywhere:)];
    tapRecognizer.delegate = self;
  }
}

- (void) unregisterFromKeyboardNotifications {
  if (self.registeredForKeyboardNotifications) {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
    self.registeredForKeyboardNotifications = NO;
  }
}

-(void) didTapAnywhere: (UITapGestureRecognizer*) recognizer {
  [self.view endEditing:YES];
}

#pragma mark -
#pragma mark TextField

- (void) setTextFieldTagAndDelegate: (UITextField *) field {
  [field setTag: currentTag];
  [field setDelegate:self];
  [field setReturnKeyType:UIReturnKeyDone];
  currentTag++;
}

#pragma mark -
#pragma mark ScrollView

- (void) setupScrollView: (int) contentHeight frameHeight: (int) frameHeight {
  [self setupScrollView:contentHeight frameHeight:frameHeight frameWidth:[[UIScreen mainScreen] applicationFrame].size.width];
}

- (void) setupScrollView: (int) contentHeight frameHeight: (int) frameHeight frameWidth: (int) frameWidth {
  self.scrollView=[[UIScrollView alloc] initWithFrame: CGRectMake(0, 0, frameWidth, frameHeight)];
  self.scrollView.contentSize=CGSizeMake(320, contentHeight);
}

#pragma mark -
#pragma mark TableView

- (void) setupTableView {
  [self setupTableView:CGRectMake(0, 0, [[UIScreen mainScreen] applicationFrame].size.width, [[UIScreen mainScreen] applicationFrame].size.height - self.navigationController.navigationBar.frame.size.height)];
}

- (void) setupTableView: (CGRect) rect {
  self.tableView = [[UITableView alloc] initWithFrame: rect];
  self.tableView.contentMode = UIViewContentModeScaleAspectFit;
  [self.tableView setDataSource:self];
  [self.tableView setDelegate:self];
  self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
  [self.tableView setUserInteractionEnabled:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

-(NSInteger) getRowsCount {
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return [self getRowsCount];
}

- (NSString *) getTableTitle {
  return @"";
}

- (UITableViewCell *)tableView:(UITableView *)table cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  NSString *cellIdentifier = [NSString stringWithFormat:@"Cell", indexPath.row];
  UITableViewCell *cell = [table dequeueReusableCellWithIdentifier:cellIdentifier];
  
  if (cell == nil || IS_IOS7_OR_LATER) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
  } else {
    if ([cell subviews]){
      for (UIView *subview in [cell subviews]) {
        [subview removeFromSuperview];
      }
    }
  }
  
//  static NSString *CellIdentifier = @"Cell";
//  
//  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//  
//  if (cell == nil) {
//    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//  }
  
  cell.selectedBackgroundView.backgroundColor=[UIColor clearColor];
  [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
  
  return cell;
}

- (UIView *) getCellContent: (NSIndexPath *)indexPath {
  return nil;
}


@end
