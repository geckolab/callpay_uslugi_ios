//
//  PopoverViewCustom.h
//  Sprytny Bill
//
//  Created by uPaid on 21.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FPPopoverController.h"

@interface PopoverMenu : UITableViewController

@property (nonatomic, retain) UIViewController *delegate;
@property (nonatomic, retain) FPPopoverController *popover;

- (void) refresh;
- (void) showMenu: (id) sender;
+ (void) addMenuItem: (NSString *) title delegate: (id) delegate action: (SEL) action;
+ (void) addMenuItem: (NSString *) title delegate: (id) delegate action: (SEL) action atIndex: (int) index;

- (void) prepareMenu;
- (id) initWithDelegate: (id) _delegate;
- (UIBarButtonItem *) createBarButton;
+ (NSMutableArray*)menuItems;

@end
