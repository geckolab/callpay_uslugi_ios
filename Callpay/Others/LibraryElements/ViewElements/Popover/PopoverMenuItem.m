//
//  PopoverMenuItem.m
//  Sprytny Bill
//
//  Created by uPaid on 21.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "PopoverMenuItem.h"

@implementation PopoverMenuItem

@synthesize action;
@synthesize delegate;
@synthesize title;

- (id) initWithTitle: (NSString *) _title delegate: (id) _delegate action: (SEL) _action {
  self = [super init];
  
  if (self) {
    self.action = _action;
    self.delegate = _delegate;
    self.title = _title;
  }
  
  return self;
}

- (void) dealloc {
  //self.action = nil;
  //self.delegate = nil;
  //[self.title release];
  //[super dealloc];
}

@end
