//
//  PopoverMenuItem.h
//  Sprytny Bill
//
//  Created by uPaid on 21.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PopoverMenuItem : NSObject

@property (nonatomic) SEL action;
@property (nonatomic, retain) id delegate;
@property (nonatomic, retain) NSString *title;

- (id) initWithTitle: (NSString *) _title delegate: (id) _delegate action: (SEL) _action;

@end
