//
//  PopoverViewCustom.m
//  Sprytny Bill
//
//  Created by uPaid on 21.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "PopoverMenu.h"
#import "HelpViewController.h"
#import "PopoverMenuItem.h"

@implementation PopoverMenu

@synthesize delegate;
@synthesize popover;

static NSMutableArray *menuItems;

+ (NSMutableArray*)menuItems
{
  if (!menuItems) {
    menuItems = [[NSMutableArray alloc] init];
  }
  
  return menuItems;
}

- (id) initWithDelegate: (UIViewController *) _delegate {
  self = [super init];
  
  if (self) {
    self.delegate = _delegate;
    self.title = nil;
  }
  
  return self;
}

- (UIBarButtonItem *) createBarButton{
  UIImage *optionsImage = [UIImage imageNamed:@"btn_menu.png"];
  UIButton *optionsButton = [UIButton buttonWithType:UIButtonTypeCustom];
  [optionsButton addTarget:self action:@selector(showMenu:) forControlEvents:UIControlEventTouchUpInside];
  optionsButton.bounds = CGRectMake( 0, 0, optionsImage.size.width, optionsImage.size.height );
  [optionsButton setImage:optionsImage forState:UIControlStateNormal];
  [optionsButton setImage:[UIImage imageNamed:@"btn_menu_pressed.png"] forState:UIControlStateHighlighted];
  UIBarButtonItem *optionsBarButton = [[UIBarButtonItem alloc] initWithCustomView:optionsButton];
  
  return optionsBarButton;
}

- (void) refresh {
  [self.tableView reloadData];
}

+ (void) addMenuItem: (NSString *) title delegate: (id) _delegate action: (SEL) action atIndex: (int) index {
  [[PopoverMenu menuItems] insertObject:[[PopoverMenuItem alloc] initWithTitle:title delegate:_delegate action:action] atIndex: index];
}

+ (void) addMenuItem: (NSString *) title delegate: (id) _delegate action: (SEL) action {
  [[PopoverMenu menuItems] addObject: [[PopoverMenuItem alloc] initWithTitle:title delegate:_delegate action:action]];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return [[PopoverMenu menuItems] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *CellIdentifier = @"Cell";
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
  
  if(cell == nil)
  {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
  }
  
  cell.textLabel.text = [[[PopoverMenu menuItems] objectAtIndex:indexPath.row] title];
  
  return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  return 45;
}

#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  PopoverMenuItem *item = [[PopoverMenu menuItems] objectAtIndex:indexPath.row];
  [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO];
  
  [SVProgressHUD dismiss];
  [self.popover dismissPopoverAnimated:YES];
  [item.delegate performSelector:item.action];
}

- (void) prepareMenu {
  if ([self.delegate respondsToSelector:@selector(willShowPopoverMenu)]) {
    [self.delegate performSelector:@selector(willShowPopoverMenu)];
  }
       
  self.popover = [[FPPopoverController alloc] initWithViewController:self];
  self.popover.tint = FPPopoverWhiteTint;
  self.popover.border = 0;
  self.popover.contentSize = CGSizeMake(140, 40 + [[PopoverMenu menuItems] count] * 45);
  
}

- (void) showMenu: (id) sender {
  [self prepareMenu];  
  if ([sender isKindOfClass: [UIView class]] ) {
    [self.popover presentPopoverFromView:sender];
  } else if ([[sender view] isKindOfClass: [UIView class]]) {
    [self.popover presentPopoverFromView:[sender view]];
  } 
}


@end
