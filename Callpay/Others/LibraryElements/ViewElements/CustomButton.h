//
//  CustomButton.h
//  Sprytny Bill
//
//  Created by uPaid on 29.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+HexString.h"
@class ProjectViewHelper;

@interface CustomButton : UIButton

+ (UIButton *) checkbox;
- (void) addImageToTitle: (NSString *) imageName;
- (void) makeSmallFont;
- (void) makeSmallButton;
- (void) makeBigButton;

- (id) initWithTitle: (NSString *)title imageName: (NSString *) imageName fontSize: (float) fontSize;
- (void) setBackgroundImage:(NSString *) imageName;
- (UIColor *) labelShadowColor;

@end
