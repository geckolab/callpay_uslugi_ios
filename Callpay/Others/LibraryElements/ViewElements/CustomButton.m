//
//  CustomButton.m
//  Sprytny Bill
//
//  Created by uPaid on 29.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "CustomButton.h"
#import "ProjectViewHelper.h"

@implementation CustomButton

+ (UIButton *) checkbox {
  UIButton *checkbox = [UIButton buttonWithType:UIButtonTypeCustom];
  [checkbox setImage:[UIImage imageNamed:@"check_off.png"] forState:UIControlStateNormal];
  [checkbox setImage:[UIImage imageNamed:@"check_on.png"] forState:UIControlStateSelected];
  [checkbox setImage:[UIImage imageNamed:@"check_off_pressed.png"] forState:UIControlStateHighlighted];
  [checkbox setImage:[UIImage imageNamed:@"check_on_pressed.png"] forState:UIControlStateHighlighted | UIControlStateSelected];
  
  return checkbox;
}

- (void) addImageToTitle: (NSString *) imageName {
  [self setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
  self.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
}


-(void) makeSmallFont {
  [[self titleLabel] setFont:[ProjectViewHelper fontBoldWithSize:10]];
  [self setTitleEdgeInsets:UIEdgeInsetsMake(0, -5, 1, 30)];
}

-(void) makeBigButton {
  [self makeButton:YES];
}

-(void) makeSmallButton {
  [self makeButton:NO];
}

-(void) makeButton:(BOOL) big {
  float fontSize;
  
  if (big) {
    fontSize = 18.0;
  } else {
    fontSize = 15.0;
  }
  
//  [[self titleLabel] setFont:[ProjectViewHelper fontBoldWithSize:fontSize]];
     [[self titleLabel] setFont:[UIFont fontWithName:@"Swis721LtEU" size:fontSize]];
  [[self titleLabel] setLineBreakMode:NSLineBreakByTruncatingTail];
  [self setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
  [self setTitleColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] forState:UIControlStateNormal];
  [self setBackgroundImage:@"btn_big"];
  [[self titleLabel] setShadowColor:[self labelShadowColor]];
  [[self titleLabel] setShadowOffset:CGSizeMake(0, 1)];
}

- (id) initWithTitle: (NSString *)title imageName: (NSString *) imageName fontSize: (float) fontSize{
  self = [super init];
  
  if (self) {
    [[self titleLabel] setFont:[ProjectViewHelper fontBoldWithSize:fontSize]];
    [[self titleLabel] setLineBreakMode:NSLineBreakByTruncatingTail];
    [[self titleLabel] setShadowColor:[self labelShadowColor]];
    [[self titleLabel] setShadowOffset:CGSizeMake(0, 1)];
    [self setTitle:title forState:UIControlStateNormal];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [self setBackgroundImage:imageName];
  }
  
  return self;
}

- (void) setBackgroundImage:(NSString *) imageName {
//  [self setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@", imageName]] forState:UIControlStateNormal];
//  [self setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_pressed", imageName]] forState:UIControlStateHighlighted];
}

- (UIColor *) labelShadowColor {
  return [UIColor colorWithHexString:@"f8f8f8"];
}

@end
