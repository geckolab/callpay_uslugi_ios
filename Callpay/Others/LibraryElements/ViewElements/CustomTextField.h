//
//  CustomTextField.h
//  Sprytny Bill
//
//  Created by uPaid on 29.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ProjectViewHelper;
#import "UIColor+HexString.h"

@interface CustomTextField : UITextField{
  NSString *helpText;
  UIButton *helpButton;
}

- (id) initWithoutBackground: (NSString *) placeholder;
- (id) initWithPlaceholder: (NSString *) title posY: (int) posY;
- (id) initWithPlaceholder: (NSString *) placeholder posY: (int) posY help: (NSString *) help;

- (void) setTag:(NSInteger)tag andDelegate: (id) delegate;
- (void) setHelp: (NSString *) help;
- (void) setHelp: (NSString *) help rightMargin: (int) rightMargin;
- (void) removeMargins;

- (UIColor *) placeholderColor;
- (void) configWithPlaceholder: (NSString *) placeholder;

@end
