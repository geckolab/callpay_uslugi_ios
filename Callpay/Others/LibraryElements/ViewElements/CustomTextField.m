//
//  CustomTextField.m
//  Sprytny Bill
//
//  Created by uPaid on 29.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "CustomTextField.h"
#import "ProjectViewHelper.h"

@implementation CustomTextField

-(void) removeMargins {
  UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
  [self setLeftView:paddingView];
  [self setLeftViewMode:UITextFieldViewModeAlways];
}

- (id) initWithoutBackground: (NSString *) placeholder{
  self = [self init];
  
  [self setPlaceholder: placeholder];
  
  return self;
}

- (id) init {
  self = [super init];
  
  [self setBackgroundColor:[UIColor clearColor]];
  [self setBorderStyle:UITextBorderStyleNone];
  [self setFont:[ProjectViewHelper fontNormalWithSize:13.0]];
  [self setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
  [self setTextColor:[self textColor]];
  [self setText:@""];
  
  return self;
}

- (id) initWithPlaceholder: (NSString *) placeholder posY: (int) posY {
  self = [self init];
  
  [self setFrame: CGRectMake(10, posY, 300, 34)];
  [self configWithPlaceholder: placeholder];
  
  return self;
}

- (void) configWithPlaceholder: (NSString *) placeholder {
  [self setBackground:[UIImage imageNamed:@"textfield.png"]];
  UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
  [self setLeftView:paddingView];
  [self setLeftViewMode:UITextFieldViewModeAlways];
  [self setPlaceholder: placeholder];
}

- (void) drawPlaceholderInRect:(CGRect)rect {
  [[self placeholderColor] setFill];
  UIFont *placeholderFont = [ProjectViewHelper fontItalicWithSize:12.0];
  CGRect placeholderRect = CGRectMake(rect.origin.x, (rect.size.height- self.font.pointSize)/2, rect.size.width, self.font.pointSize);
  
  if(IS_IOS7_OR_LATER) {
    [[self placeholder] drawInRect:placeholderRect withFont:placeholderFont];
  } else {
    [[self placeholder] drawInRect:rect withFont:placeholderFont];
  }
}

- (id) initWithPlaceholder: (NSString *) placeholder posY: (int) posY help: (NSString *) help {
  self = [self initWithPlaceholder: placeholder posY: posY];
  
  [self setHelp:help];
  
  return self;
}

- (void) setHelp: (NSString *) help {
  [self setHelp:help rightMargin:0];
}

- (void) setHelp: (NSString *) help rightMargin: (int) rightMargin{
  helpText = help;
  
  UIView *view = [[UIView alloc] initWithFrame:CGRectMake(5, 5, 30 + rightMargin, 25)];
  helpButton = [[UIButton alloc] initWithFrame: CGRectMake(rightMargin, 0, 25, 25)];
  [helpButton setBackgroundImage:[UIImage imageNamed: @"btn_help"] forState:UIControlStateNormal];
  [helpButton setBackgroundImage:[UIImage imageNamed: @"btn_help_pressed"] forState:UIControlStateHighlighted];
  [helpButton addTarget:self action:@selector(showHelp:) forControlEvents:UIControlEventTouchUpInside];
  [view addSubview:helpButton];
  
  if (rightMargin != 0) {
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rightMargin, 25)];
    [view addSubview:paddingView];
  }
  
  [self setRightView: view];
  [self setRightViewMode:UITextFieldViewModeAlways];
}

-(IBAction)showHelp:(id)sender {
  UIAlertView *a = [[UIAlertView alloc] initWithTitle:APP_NAME message:helpText delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
  [a show];
  //[a release];
}

- (void) setTag:(NSInteger)tag andDelegate: (id) delegate {
  [self setReturnKeyType:UIReturnKeyDone];
  [self setTag:tag];
  [self setDelegate:delegate];
}

- (void) setEnabled:(BOOL)enabled {
  [super setEnabled:enabled];
  
  if (!enabled) {
    [self setRightViewMode:UITextFieldViewModeNever];
    [self setBackground:[UIImage imageNamed: @"textfield_inactive"]];
  }
}

- (UIColor *) placeholderColor {
  return [UIColor colorWithHexString:@"bdbdbd"];
}

@end
