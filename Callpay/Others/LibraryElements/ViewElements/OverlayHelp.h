//
//  OverlayHelp.h
//  Sprytny Bill
//
//  Created by uPaid on 24.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OverlayHelp : NSObject

@property (nonatomic, retain) UIViewController *delegate;

+ (BOOL) constructViewForController: (UIViewController *) controller;
+ (void) show;
+ (void) hide;
+ (BOOL) isShowed;
+ (BOOL) isShowedFirstTimeForView;
+ (void) forgotShowedState;

+ (OverlayHelp *) object;

- (void) show: (id) sender;
- (void) hide: (id) sender;


@end
