//
//  OverlayHelp.m
//  Sprytny Bill
//
//  Created by uPaid on 24.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "OverlayHelp.h"

@implementation OverlayHelp

@synthesize delegate;
static OverlayHelp *help;
static BOOL showedFirstTime;

#define OVERLAY_TAG 10101

+ (OverlayHelp *) object {
  if (!help) {
    help = [[OverlayHelp alloc] init];
  }
  
  return help;
}

+ (BOOL) constructViewForController: (UIViewController *) controller {
  if (![controller respondsToSelector:@selector(overlayElements:)]) {

    return NO;
  } else {
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
    UIView *overlayElementsContainer = [[UIView alloc] initWithFrame:CGRectMake(0, window.bounds.size.height - controller.view.bounds.size.height, window.bounds.size.width, window.bounds.size.height)];
    
    if (![controller performSelector:@selector(overlayElements:) withObject:overlayElementsContainer]) {
      return NO;
    }
    
    if ([window viewWithTag:OVERLAY_TAG] != nil) {
      [[window viewWithTag:OVERLAY_TAG] removeFromSuperview];
    }
    
    UIView *overlayBg = [[UIView alloc] initWithFrame:window.bounds];
    overlayBg.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7f];
    [overlayBg setTag: OVERLAY_TAG];
    
    [overlayBg addSubview:overlayElementsContainer];
    [[OverlayHelp object] setDelegate:controller];
    
    UIButton *overlayButton = [[UIButton alloc] initWithFrame:window.bounds];
    [overlayButton addTarget:[OverlayHelp object] action:@selector(hide:) forControlEvents:UIControlEventTouchUpInside];
    [overlayBg addSubview:overlayButton];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *className = [NSString stringWithFormat:@"overlay_%@", NSStringFromClass([controller class])];

    if ([prefs boolForKey:className]) {
      showedFirstTime = NO;
      [overlayBg setHidden:YES];
    } else {
      showedFirstTime = YES;
      
      if ([controller respondsToSelector:@selector(overlayAutoHide)]) {
        [controller performSelector:@selector(overlayAutoHide)];
        [overlayBg setHidden: YES];
      }
      
      [prefs setBool:YES forKey:className];
    }
    
    [window addSubview:overlayBg];
    
    return YES;
  }
}

+ (void) show {
  UIViewController *controller = [[OverlayHelp object] delegate];
  [controller.view endEditing:YES];
  
  if (controller != nil && [controller respondsToSelector:@selector(overlayOnShow)]) {
    [controller performSelector:@selector(overlayOnShow)];
  }
  
  [[[UIApplication sharedApplication].delegate.window viewWithTag:OVERLAY_TAG] setHidden:NO];
  
}

- (void) show: (id) sender {
  [OverlayHelp show];
}

+ (void) hide {
  UIViewController *controller = [[OverlayHelp object] delegate];
  [[[UIApplication sharedApplication].delegate.window viewWithTag:OVERLAY_TAG] setHidden:YES];
  
  if (controller != nil && [controller respondsToSelector:@selector(overlayOnHide)]) {
    [controller performSelector:@selector(overlayOnHide)];
  }
}

+ (void) forgotShowedState {
  NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
  NSString *className = [NSString stringWithFormat:@"overlay_%@", NSStringFromClass([[[OverlayHelp object] delegate] class])];
  
  [prefs removeObjectForKey:className];
}

- (void) hide: (id) sender {
  [OverlayHelp hide];
}

+ (BOOL) isShowed {
  return ![[UIApplication sharedApplication].delegate.window viewWithTag:OVERLAY_TAG].hidden;
}

+ (BOOL) isShowedFirstTimeForView {
  return showedFirstTime;
}

@end