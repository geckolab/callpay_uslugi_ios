//
//  ViewHelper.m
//  Sprytny Bill
//
//  Created by uPaid on 11.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "ViewHelper.h"
#import "ProjectButton.h"
#import "ProjectLabel.h"
#import "ProjectTextField.h"


@implementation ViewHelper

+ (UIFont *) fontNormalWithSize: (CGFloat) size {
  return [UIFont systemFontOfSize:size];
}

+ (UIFont *) fontBoldWithSize: (CGFloat) size {
  return [UIFont boldSystemFontOfSize:size];
}

+ (UIFont *) fontMediumWithSize: (CGFloat) size {
  return [UIFont systemFontOfSize:size];
}

+ (UIFont *) fontItalicWithSize: (CGFloat) size {
  return [UIFont italicSystemFontOfSize:size];
}

+ (CGSize) screenSize {
  return [[UIScreen mainScreen] applicationFrame].size;
}

+ (NSString *) statusSubstatusInfo: (int) status substatus: (int) substatus {
  switch (substatus) {
    case 1:
      return @"Nieprawidłowy CVC2/CVV2/mPIN";
    case 2:
      return @"Karta wygasła";
    case 4:
      return @"Brak środków";
    case 5:
      return @"Przekroczona wysokość limitu lub wyłączone transakcje MOTO";
    case 6:
      return @"Karta jest zablokowana";
  }
  
  switch (status) {
    case 1:
      return @"Transakcja zakończona pomyślnie";
    case 2:
      return @"Transakcja odrzucona przez bank";
    case 3:
      return @"Brak wybranej karty w bazie lub błąd pobrania karty";
    case 4:
      return @"Niepoprawne dane weryfikacyjne";
    case 5:
      return @"Sesja wygasła, zaloguj się ponownie.";
    case 6:
      return @"Karta nie jest zweryfikowana.";
    case 7:
      return @"Wystąpił techniczny. Spróbuj później.";
    case 8:
      return [NSString stringWithFormat:@"Wystąpił błąd, który wymaga weryfikacji. Skontaktuj się z nami: %@ lub (22)6334996.", BOK_EMAIL];
    case 9:
      return @"Twoja karta została na godzinę zablokowana z powodu wprowadzenia 3 błędnych CVC2/CVV2/mPIN. Spróbuj ponownie później.";
    case 10:
      return @"Wykryta zdublowana płatność za ten sam rachunek.";
  }
  
  return [NSString stringWithFormat:@"Nieznany błąd, skontaktuj się z %@.", BOK_EMAIL];
}

+ (void) showInfoAlert: (NSString *) message {
  UIAlertView *a = [[UIAlertView alloc] initWithTitle:APP_NAME message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
  [a show];
  //[a release];
}

+ (NSString *) cuttedNameWithAmount: (NSString *) name price: (NSString *) price cutNameAfter: (int) cutAfterLength {
  NSString *amount = [NSString stringWithFormat:@"%.2f", [price floatValue]/100.0];
  
  if ([amount length] > 5) {
    cutAfterLength -= [amount length] - 5;
  }
  
  if ([name length] > cutAfterLength) {
    name = [[name substringToIndex:cutAfterLength - 4] stringByAppendingFormat:@"..."];
  }
  
  return [name stringByAppendingFormat:@" (%@ zł)", amount];
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)size {
  if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
    UIGraphicsBeginImageContextWithOptions(size, NO, [[UIScreen mainScreen] scale]);
  } else {
    UIGraphicsBeginImageContext(size);
  }
  [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
  UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  
  return newImage;
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToMaxWidth:(CGFloat)width maxHeight:(CGFloat)height {
  CGFloat oldWidth = image.size.width;
  CGFloat oldHeight = image.size.height;
  
  CGFloat scaleFactor;
  
  if (oldWidth > oldHeight) {
    scaleFactor = width / oldWidth;
  } else {
    scaleFactor = height / oldHeight;
  }
  
  CGFloat newHeight = oldHeight * scaleFactor;
  CGFloat newWidth = oldWidth * scaleFactor;
  CGSize newSize = CGSizeMake(newWidth, newHeight);
  
  return [self imageWithImage:image scaledToSize:newSize];
}

+ (UIImageView *) imageView: (NSString *) imageName atPosY: (int) posY {
  UIImage *image = [UIImage imageNamed:imageName];
  UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
  [imageView setFrame:CGRectMake(0, posY, image.size.width, image.size.height)];
  
  return imageView;
}

+ (NSString *) formattedPhoneNumber: (NSString *) phone {
  return [NSString stringWithFormat:@"%@ %@ %@", [phone substringWithRange:NSMakeRange(0, 3)], [phone substringWithRange:NSMakeRange(3, 3)], [phone substringWithRange:NSMakeRange(6, 3)]];
}

+ (UIImageView *) addLine: (int) posY {
  UIImageView *line = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"line.png"]];
  [line setFrame:CGRectMake(10, posY, 300, 1)];
  //[line autorelease];
  
  return line;
}

@end
