//
//  CustomViewController.h
//  Sprytny Bill
//
//  Created by uPaid on 12.04.2012.
//  Copyright (c) 2012 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProjectViewHelper.h"

@interface CustomViewController : UIViewController <UITextFieldDelegate, UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate> {
  UITapGestureRecognizer *tapRecognizer;
  NSInteger currentTag;
}

- (void) addBackButton;
- (void) registerForKeyboardNotifications;
- (void) unregisterFromKeyboardNotifications;
- (void) setTextFieldTagAndDelegate: (UITextField *) field;
- (void) setupScrollView: (int) contentHeight frameHeight: (int) frameHeight;
- (void) setupScrollView: (int) contentHeight frameHeight: (int) frameHeight frameWidth: (int) frameWidth;
- (void) setupTableView: (CGRect) rect;
- (void) setupTableView;
- (NSInteger) getRowsCount;
- (NSString *) getTableTitle;
- (UIView *) getCellContent: (NSIndexPath *)indexPath;
- (void) addMenuBarButton;
- (int) keyboardShift;

@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong) UITextField *activeTxtField;
@property BOOL registeredForKeyboardNotifications;
@property (retain) UITableView *tableView;
@property (strong) UINavigationController *customNavigationController;

@end
