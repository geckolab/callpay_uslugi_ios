//
//  CustomLabel.m
//  Sprytny Bill
//
//  Created by uPaid on 29.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "CustomLabel.h"

@implementation CustomLabel

-(void) _prepare: (NSString *) title {
  [self setBackgroundColor:[UIColor clearColor]];
  [self setText:title];
  self.lineBreakMode = NSLineBreakByWordWrapping;
  self.numberOfLines = 0;
}

-(void) makeLightWithTitle: (NSString *) title  {
  [self makeLightWithTitle:title andSize: 12];
}

-(void) makeLightWithTitle: (NSString *) title andSize:(CGFloat)size{
  [self _prepare:title];
  [self setFont:[ProjectViewHelper fontNormalWithSize:size]];
  [self setTextColor:[self lightColor]];
  [self setTextAlignment:NSTextAlignmentCenter];
}

-(void) makeWithTitle: (NSString *) title  {
  [self makeLightWithTitle:title andSize: 12];
}

-(void) makeWithTitle: (NSString *) title andSize:(CGFloat)size{
  [self _prepare:title];
  [self setFont:[ProjectViewHelper fontMediumWithSize:size]];
  [self setTextColor:[self normalColor]];
  [self setTextAlignment:NSTextAlignmentCenter];
}

-(void) makeWithBoldTitle: (NSString *) title andSize:(CGFloat)size{
  [self _prepare:title];
  [self setFont:[ProjectViewHelper fontBoldWithSize:size]];
  [self setTextColor:[self normalColor]];
  [self setTextAlignment:NSTextAlignmentCenter];
  [self setTextColor:[UIColor blackColor]];
}

-(void) makeWithTitleAlignLeft: (NSString *) title andSize:(CGFloat)size{
  [self _prepare:title];
  [self setFont:[ProjectViewHelper fontNormalWithSize:size]];
  [self setTextColor:[self normalColor]];
  [self setTextAlignment:NSTextAlignmentLeft];
}

-(void) makeWithBoldTitleAlignLeft: (NSString *) title andSize:(CGFloat)size{
  [self _prepare:title];
  [self setFont:[ProjectViewHelper fontMediumWithSize:size]];
  [self setTextColor:[self normalColor]];
  [self setTextAlignment:NSTextAlignmentLeft];
  [self setTextColor:[UIColor blackColor]];
}

-(void) makeWithBoldTitleAlignRight: (NSString *) title andSize:(CGFloat)size{
  [self _prepare:title];
  [self setFont:[ProjectViewHelper fontMediumWithSize:size]];
  [self setTextColor:[self normalColor]];
  [self setTextAlignment:NSTextAlignmentRight];
  [self setTextColor:[UIColor blackColor]];
}

-(void) makeLightWithTitleAlignLeft: (NSString *) title andSize:(CGFloat)size{
  [self _prepare:title];
  [self setFont:[ProjectViewHelper fontNormalWithSize:size]];
  [self setTextColor:[self lightColor]];
  [self setTextAlignment:NSTextAlignmentLeft];
}

-(void) makeLightWithTitleAlignRight: (NSString *) title andSize:(CGFloat)size{
  [self makeLightWithTitleAlignLeft: title andSize:size];
  [self setTextAlignment:NSTextAlignmentRight];
}

-(void) makeMediumWithTitleAlignLeft: (NSString *) title andSize:(CGFloat)size{
  [self _prepare:title];
  [self setFont:[ProjectViewHelper fontNormalWithSize:size]];
  [self setTextColor:[self mediumColor]];
  [self setTextAlignment:NSTextAlignmentLeft];
}

- (void) setupLabelHeight {
    CGSize constraint = CGSizeMake(self.frame.size.width, 20000.0f);
    
    CGSize size = [self.text sizeWithFont: self.font constrainedToSize:constraint lineBreakMode: NSLineBreakByWordWrapping];
    CGRect frame = self.frame;
    frame.size.height = size.height;
    self.frame = frame;
}

- (UIColor *) normalColor {
  return [UIColor colorWithHexString:@"000000"];
}

- (UIColor *) lightColor {
  return [UIColor colorWithHexString:@"000000"];
}

- (UIColor *) mediumColor {
  return [UIColor colorWithHexString:@"000000"];
}

@end
