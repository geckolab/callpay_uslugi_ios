//
//  CustomLabel.h
//  Sprytny Bill
//
//  Created by uPaid on 29.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+HexString.h"
#import "ProjectViewHelper.h"

@interface CustomLabel : UILabel

-(void) _prepare: (NSString *) title;
-(void) makeLightWithTitle: (NSString *) title andSize:(CGFloat)size;
-(void) makeLightWithTitle: (NSString *) title;
-(void) makeLightWithTitleAlignLeft: (NSString *) title andSize:(CGFloat)size;
-(void) makeMediumWithTitleAlignLeft: (NSString *) title andSize:(CGFloat)size;
-(void) makeLightWithTitleAlignRight: (NSString *) title andSize:(CGFloat)size;
-(void) makeWithTitle: (NSString *) title andSize:(CGFloat)size;
-(void) makeWithTitle: (NSString *) title;
-(void) makeWithTitleAlignLeft: (NSString *) title andSize:(CGFloat)size;
-(void) makeWithBoldTitle: (NSString *) title andSize:(CGFloat)size;
-(void) makeWithBoldTitleAlignLeft: (NSString *) title andSize:(CGFloat)size;
-(void) makeWithBoldTitleAlignRight: (NSString *) title andSize:(CGFloat)size;

- (UIColor *) normalColor;
- (UIColor *) lightColor;
- (UIColor *) mediumColor;

- (void) setupLabelHeight;

@end
