//
//  NSManagedObject+Clone.h
//  Sprytny Bill
//
//  Created by uPaid on 22.04.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

//
//  NSManagedObject+Clone.h
//

#import <CoreData/CoreData.h>

#ifndef CD_CUSTOM_DEBUG_LOG
#define CD_CUSTOM_DEBUG_LOG UpaidLog
#endif

@interface NSManagedObject (Clone)

- (NSManagedObject *)cloneInContext:(NSManagedObjectContext *)context
                    excludeEntities:(NSArray *)namesOfEntitiesToExclude;

- (NSManagedObject *)cloneInContext:(NSManagedObjectContext *)context;

@end
