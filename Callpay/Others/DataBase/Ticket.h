//
//  Ticket.h
//  CallPay
//
//  Created by uPaid on 29.08.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Ticket : NSManagedObject

@property (nonatomic, retain) NSString * productName;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * seller;
@property (nonatomic, retain) NSString * productCode;
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic, retain) NSString * transactionId;
@property (nonatomic, retain) NSDate * purchaseDate;
@property (nonatomic, retain) NSString * securityCodeType;
@property (nonatomic, retain) NSString * securityCodeValueType;
@property (nonatomic, retain) NSString * sessionToken;
@property (nonatomic, retain) NSString * securityCodeValueLength;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSString * verificationPhone;
@property (nonatomic, retain) NSDate * verificationDate;
@property (nonatomic, retain) NSNumber * verificationTime;
@property (nonatomic, retain) NSString * cityIndex;

@end
