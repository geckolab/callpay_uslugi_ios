//
//  Item.m
//  Sprytny Bill
//
//  Created by uPaid on 09.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "Item.h"
#import "AppDelegate.h"
#import "UPaidaddress.h"
#import "NSManagedObject+Clone.h"
#import "ProjectViewHelper.h"

@implementation Item

@dynamic amount;
@dynamic canZero;
@dynamic ctn;
@dynamic forceToBuy;
@dynamic isTemplate;
@dynamic isEditable;
@dynamic itemName;
@dynamic priveVat;
@dynamic providerName;
@dynamic qrCode;
@dynamic serviceID;
@dynamic totalAmount;
@dynamic transferTitle;
@dynamic type;
@dynamic clientFirstName;
@dynamic clientLastName;
@dynamic clientApartment;
@dynamic clientStreet;
@dynamic clientZipcode;
@dynamic clientCity;
@dynamic clientStreetNumber;
@dynamic billerBankAccount;
@dynamic billerName;
@dynamic billerApartment;
@dynamic billerCity;
@dynamic billerStreet;
@dynamic billerStreetNumber;
@dynamic billerZipcode;
@dynamic isTemplateComplete;
@dynamic serverPaymentId;
@dynamic serverTemplateId;

@synthesize buyStatus;
@synthesize buySubstatus;

+(Item*) createFromTemplate: (Item*) template {
  AppDelegate *appDelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
  NSManagedObjectContext *context = [appDelegate managedObjectContext];
//  NSError *error = nil;
  
  Item *newItem = (Item *) [template cloneInContext:context];

  [newItem setForceToBuy:[NSNumber numberWithInt:0]];
//  [newItem setIsTemplate:[NSNumber numberWithInt:0]];
  [newItem setIsEditable:[NSNumber numberWithInt:0]];
  
//  [context save:&error];
  
  return newItem;
}

+ (NSMutableArray *) getItems: (NSString *) from {
  AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
  NSManagedObjectContext *context = [appDelegate managedObjectContext];
  
  NSEntityDescription *eDescription = [NSEntityDescription entityForName:@"Item" inManagedObjectContext:context];
  NSFetchRequest *req = [[NSFetchRequest alloc] init];
  [req setEntity:eDescription];
  [req setShouldRefreshRefetchedObjects:YES];
  NSError *error;
  
  if ([from isEqualToString:GET_ITEMS_BASKET]) {
    [req setPredicate:[NSPredicate predicateWithFormat:@"ctn != 0 AND serverPaymentId = 0"]];
  } else if ([from isEqualToString:GET_ITEMS_TEMPLATES]){
    [req setPredicate:[NSPredicate predicateWithFormat:@"isTemplate = 1"]];
    [req setPredicate:[NSPredicate predicateWithFormat:@"ctn = 0"]];
  }
  
  return [NSMutableArray arrayWithArray:[context executeFetchRequest:req error:&error]];
}


-(UPaidclientData *) getSoapClientData{
  UPaidaddress *uaddress = [[UPaidaddress alloc] init];
  [uaddress setApartment: self.clientApartment];
  [uaddress setCity:self.clientCity];
  [uaddress setNumber:self.clientStreetNumber];
  [uaddress setZipcode:self.clientZipcode];
  [uaddress setStreet:self.clientStreet];
  
  UPaidclientData *cd = [[UPaidclientData alloc] init];
  [cd setAddress: uaddress];
  [cd setFirst_name:self.clientFirstName];
  [cd setLast_name:self.clientLastName];
  
  return cd;
}

-(UPaidbillerData *) getSoapBillerData {
  UPaidaddress *uaddress = [[UPaidaddress alloc] init];
  [uaddress setApartment: self.billerApartment];
  [uaddress setCity:self.billerCity];
  [uaddress setNumber:self.billerStreetNumber];
  [uaddress setZipcode:self.billerZipcode];
  [uaddress setStreet:self.billerStreet];
  
  UPaidbillerData *bd = [[UPaidbillerData alloc] init];
  [bd setName: self.billerName];
  [bd setAddress: uaddress];
  
  return bd;
}

- (NSString *) billerNameWithPrice: (int) cutAfterLength {
  if ([self.totalAmount isEqualToString:@""]) {
    return self.billerName;
  }
  
  return [ProjectViewHelper cuttedNameWithAmount:self.billerName price:self.totalAmount cutNameAfter:cutAfterLength];
}

@end
