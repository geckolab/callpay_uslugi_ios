//
//  Ticket.m
//  CallPay
//
//  Created by uPaid on 29.08.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "Ticket.h"


@implementation Ticket

@dynamic productName;
@dynamic name;
@dynamic seller;
@dynamic productCode;
@dynamic price;
@dynamic transactionId;
@dynamic purchaseDate;
@dynamic securityCodeType;
@dynamic securityCodeValueType;
@dynamic securityCodeValueLength;
@dynamic sessionToken;
@dynamic status;
@dynamic verificationPhone;
@dynamic verificationDate;
@dynamic verificationTime;
@dynamic cityIndex;

@end
