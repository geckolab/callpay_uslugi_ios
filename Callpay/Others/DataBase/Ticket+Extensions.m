//
//  Ticket+Extensions.m
//  CallPay
//
//  Created by Michał Majewski on 17.04.2014.
//  Copyright (c) 2014 uPaid. All rights reserved.
//

#import "Ticket+Extensions.h"
//#import <UpaidExtensions/NSManagedObject+Extensions.h>
#import "NSManagedObject+Extensions.h"

@implementation Ticket (Extensions)

- (void) setVerificationTime:(NSNumber *)verificationTime {
  if (verificationTime != nil && [verificationTime intValue] == 0) {
    verificationTime = nil;
  }
  
  [self updateValue:verificationTime forKey:@"verificationTime"];
  
  if (verificationTime != nil) {
    self.verificationDate = [[NSDate date] dateByAddingTimeInterval: [verificationTime integerValue]];
  }
}

@end
