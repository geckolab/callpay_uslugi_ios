//
//  Item.h
//  Sprytny Bill
//
//  Created by uPaid on 09.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "UPaidclientData.h"
#import "UPaidbillerData.h"


#define GET_ITEMS_TEMPLATES @"templates"
#define GET_ITEMS_BASKET @"basket"

@interface Item : NSManagedObject

@property (nonatomic, retain) NSNumber * amount;
@property (nonatomic, retain) NSNumber * canZero;
@property (nonatomic, retain) NSNumber * ctn;
@property (nonatomic, retain) NSNumber * forceToBuy;
@property (nonatomic, retain) NSNumber * isTemplate;
@property (nonatomic, retain) NSNumber * isEditable;
@property (nonatomic, retain) NSString * itemName;
@property (nonatomic, retain) NSNumber * priveVat;
@property (nonatomic, retain) NSString * providerName;
@property (nonatomic, retain) NSString * qrCode;
@property (nonatomic, retain) NSNumber * serviceID;
@property (nonatomic, retain) NSString * totalAmount;
@property (nonatomic, retain) NSString * transferTitle;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * clientFirstName;
@property (nonatomic, retain) NSString * clientLastName;
@property (nonatomic, retain) NSString * clientApartment;
@property (nonatomic, retain) NSString * clientStreet;
@property (nonatomic, retain) NSString * clientZipcode;
@property (nonatomic, retain) NSString * clientCity;
@property (nonatomic, retain) NSString * clientStreetNumber;
@property (nonatomic, retain) NSString * billerBankAccount;
@property (nonatomic, retain) NSString * billerName;
@property (nonatomic, retain) NSString * billerApartment;
@property (nonatomic, retain) NSString * billerCity;
@property (nonatomic, retain) NSString * billerStreet;
@property (nonatomic, retain) NSString * billerStreetNumber;
@property (nonatomic, retain) NSString * billerZipcode;
@property (nonatomic, retain) NSNumber * isTemplateComplete;
@property (nonatomic, retain) NSNumber * serverPaymentId;
@property (nonatomic, retain) NSNumber * serverTemplateId;

@property (nonatomic) int buyStatus;
@property (nonatomic) int buySubstatus;

- (UPaidclientData *) getSoapClientData;
- (UPaidbillerData *) getSoapBillerData;
- (NSString *) billerNameWithPrice: (int) cutAfterLength;
+ (Item*) createFromTemplate: (Item*) template;
+ (NSMutableArray *) getItems: (NSString *) from;

@end
