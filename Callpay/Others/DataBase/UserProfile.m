//
//  UserProfile.m
//  Sprytny Bill
//
//  Created by uPaid on 10.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import "UserProfile.h"


@implementation UserProfile

@dynamic apartment;
@dynamic street;
@dynamic streetNumber;
@dynamic city;
@dynamic zipcode;
@dynamic firstName;
@dynamic lastName;
@dynamic serverId;
@dynamic profileName;
@dynamic isDefault;

@end
