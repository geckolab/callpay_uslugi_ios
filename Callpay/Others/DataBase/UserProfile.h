//
//  UserProfile.h
//  Sprytny Bill
//
//  Created by uPaid on 10.05.2013.
//  Copyright (c) 2013 uPaid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface UserProfile : NSManagedObject

@property (nonatomic, retain) NSString * apartment;
@property (nonatomic, retain) NSString * street;
@property (nonatomic, retain) NSString * streetNumber;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * zipcode;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * serverId;
@property (nonatomic, retain) NSString * profileName;
@property (nonatomic, retain) NSNumber * isDefault;

@end
