//
//  DiscountEntity.m
//  CallPay
//
//  Created by Jacek on 15.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "DiscountEntity.h"


@implementation DiscountEntity

@dynamic discountId;
@dynamic discountName;
@dynamic discountSortIndex;
@dynamic discountVehicleClass;

@end
