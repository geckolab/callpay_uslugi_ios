//
//  UILabel+CustomFont.h
//  CallPay
//
//  Created by Jacek on 01.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (CustomFont)

@property (nonatomic, copy) NSString* fontName;

@end
