//
//  TicketEntity.m
//  CallPay
//
//  Created by Jacek on 15.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "TicketEntity.h"
#import "TransactionEntity.h"


@implementation TicketEntity

@dynamic ticketCarrier;
@dynamic ticketCategoryReturn;
@dynamic ticketCategoryTo;
@dynamic ticketClassReturn;
@dynamic ticketClassTo;
@dynamic ticketCode;
@dynamic ticketDescription;
@dynamic ticketDiscount;
@dynamic ticketDistanceReturn;
@dynamic ticketDistanceTo;
@dynamic ticketFrom;
@dynamic ticketIsRefundable;
@dynamic ticketOffer;
@dynamic ticketOwnerDocument;
@dynamic ticketOwnerName;
@dynamic ticketOwnerSerial;
@dynamic ticketOwnerSurname;
@dynamic ticketPrice;
@dynamic ticketSerial;
@dynamic ticketSignature;
@dynamic ticketThrough;
@dynamic ticketTo;
@dynamic ticketValidReturn;
@dynamic ticketValidReturnHours;
@dynamic ticketValidTo;
@dynamic ticketValidToHours;
@dynamic ticketValue;
@dynamic ticketVatRate;
@dynamic ticketVatValue;
@dynamic transaction;

@end
