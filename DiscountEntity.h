//
//  DiscountEntity.h
//  CallPay
//
//  Created by Jacek on 15.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DiscountEntity : NSManagedObject

@property (nonatomic, retain) NSNumber * discountId;
@property (nonatomic, retain) NSString * discountName;
@property (nonatomic, retain) NSNumber * discountSortIndex;
@property (nonatomic, retain) NSNumber * discountVehicleClass;

@end
