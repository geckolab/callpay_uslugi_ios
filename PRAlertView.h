//
//  PRAlertView.h
//  pr
//
//  Created by Marcin Szulc on 17.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PRAlertView : NSObject <UIAlertViewDelegate>

- (id) initWithTitle:(NSString *)title message:(NSString *)message withParent:(id)parent callbackSelector:(SEL) selector cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ...;

- (void) show;

@end
