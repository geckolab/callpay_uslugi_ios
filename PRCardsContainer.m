//
//  PRCardsContainer.m
//  pr
//
//  Created by Alexander Pimenov on 24.06.13.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved. 
//

#import "PRCardsContainer.h"
#import "PRCardObject.h"


@interface PRCardsContainer ()

@property (nonatomic) NSMutableArray *cards;

@end

@implementation PRCardsContainer

- (void)addCard:(PRCardObject *)card
{
    [[self cards] addObject:card];
}

- (BOOL)containsDefaultCard
{
    return !![self defaultCard];
}

- (BOOL)containsOtherCards
{
    for (PRCardObject *card in [self cards])
    {
        if (![card isDefaultForPurchases])
        {
            return YES;
        }
    }
    return NO;
}

- (NSMutableArray *)cards
{
    if (!_cards)
    {
        _cards = [[NSMutableArray alloc] init];
    }
    return _cards;
}

- (PRCardObject *)defaultCard
{
    for (PRCardObject *card in [self cards])
    {
        if ([card isDefaultForPurchases])
        {
            return card;
        }
    }
    return nil;
}

- (PRCardObject *)otherCardAtIndex:(NSUInteger)index
{
    NSUInteger otherCardIndex = index;

    if ([self containsDefaultCard])
    {
        NSUInteger defaultCardIndex = [[self cards] indexOfObject:[self defaultCard]];

        if (index >= defaultCardIndex)
        {
            otherCardIndex++;
        }
    }

    if (otherCardIndex >= [[self cards] count])
    {
        [NSException raise:NSRangeException format:nil];
    }
    return [[self cards] objectAtIndex:otherCardIndex];
}

- (void)removeAllCards
{
    [[self cards] removeAllObjects];
}

- (NSUInteger)count
{
    return [[self cards] count];
}

- (NSString *)description
{
    NSMutableString *description = [NSMutableString stringWithFormat:@"<%@: ", NSStringFromClass([self class])];

    [description appendString:[NSString stringWithFormat:@"%@", [self cards]]];

    [description appendString:@">"];
    return description;
}

@end