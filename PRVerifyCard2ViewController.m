//
//  PRVerifyCard2ViewController.m
//  pr
//
//  Created by Marcin Szulc on 02.10.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PRVerifyCard2ViewController.h"
#import "PRStandardButton.h"
#import "PRAsyncConnection.h"
#import "PRAlertView.h"
#import "XLog.h"

static NSString * const LogTag = @"VerifyCardAmountViewController";

@interface PRVerifyCard2ViewController () <UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet PRStandardButton *okButton;
@property (strong, nonatomic) IBOutlet UITextField *editAmount;
@property (strong, nonatomic) PRAlertView *alert;
@end

@implementation PRVerifyCard2ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIBarButtonItem * mcmLogo = [[UIBarButtonItem alloc] initWithCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mcm_icon"]]];
    self.navigationItem.rightBarButtonItem = mcmLogo;
    
    [_okButton setEnabled:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonOkOnClick:(id)sender {
    [_okButton setLockedForResponse:YES];

    
    PRAsyncConnection *connection = [[PRAsyncConnection alloc] initWithParent:self successSelector:@selector(connectionSuccess:) didFailSelector:@selector(connectionFailed:)];
    [connection sendVerifyCardRequestWithCardId:_card.identifier andAmount:_editAmount.text.intValue];
}

- (void) connectionSuccess:(PRAsyncConnection*) connection
{
    [_okButton setLockedForResponse:NO];
    
    XLogTag(LogTag, @"Response: %@", [[NSString alloc] initWithData:connection.responseData encoding:NSUTF8StringEncoding]);
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:connection.responseData
                          options:kNilOptions
                          error:&error];
    
    if(error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        [self connectionFailed:connection];
        return;
    }
    
    NSDictionary *response = [json objectForKey:@"verify_card_response"];
    
    NSNumber *status = [response objectForKey:@"status"];
    
    if(status==nil)
    {
        [self connectionFailed:connection];
        return;
    }
    
    switch (status.intValue) {
        case STATUS_OK:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertCardVerified", nil) withParent:self callbackSelector:@selector(connectionSuccessAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
            
        case STATUS_USER_BLOCKED:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertUserBlocked", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
        case STATUS_NO_MONEY:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertNoMoney", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
        case STATUS_WRONG_CARD_ID:
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertWrongCardId", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
            return;
        case STATUS_WRONG_AMOUNT:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertWrongAmount", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            break;
        case STATUS_CARD_ALREADY_VERIFIED:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertCardAlreadyVerified", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            break;
        case STATUS_NO_MONEY_TO_VERIFY_CARD:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertCardNoMoney", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            break;
        case STATUS_CARD_BLOCKED:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertCardBlocked", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            break;
    }
    
}

- (void) connectionFailedAlertCallback:(NSNumber*) buttonIndex
{
    
}

- (void) connectionSuccessAlertCallback:(NSNumber*) buttonIndex
{
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
}

- (void) connectionFailed:(PRAsyncConnection*) connection
{
    [_okButton setLockedForResponse:NO];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertStatusError", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil), nil];
    [alert show];
}

- (IBAction)editAmountDidChange:(id)sender {
    if(_editAmount.text.length>=1)
    {
        [_okButton setEnabled:YES];
    }
    else
    {
        [_okButton setEnabled:NO];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return range.location<2;
}

@end
