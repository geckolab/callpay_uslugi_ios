//
//  PRCardObject.m
//  pr
//
//  Created by Alexander Pimenov on 24.06.13.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved. 
//

#import "PRCardObject.h"

static NSString *const PRDefaultCardNumberKey = @"defaultCardNumber";


@implementation PRCardObject

- (BOOL)isDefaultForPurchases
{
    NSString *defaultCardNumber = [[NSUserDefaults standardUserDefaults] objectForKey:PRDefaultCardNumberKey];

    if (defaultCardNumber && [defaultCardNumber isEqualToString:[self number]])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void)setDefaultForPurchases
{
    [[NSUserDefaults standardUserDefaults] setObject:[self number] forKey:PRDefaultCardNumberKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)description
{
    NSMutableString *description = [NSMutableString stringWithFormat:@"<%@: ", NSStringFromClass([self class])];

    [description appendString:[NSString stringWithFormat:@"identifier: %@"
                                                                 "number: %@,"
                                                                 "expirtion date: %@,"
                                                                 "active : %@,"
                                                                 "default: %@",
                                                         [self identifier],
                                                         [self number],
                                                         [self expirationDate],
                                                         [self isActive] ? @"YES" : @"NO",
                                                         [self isDefaultForPurchases] ? @"YES" : @"NO"]];

    [description appendString:@">"];
    return description;
}

@end