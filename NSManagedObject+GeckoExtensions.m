//
//  NSManagedObjectContext+GeckoExtensions.m
//  CallPay
//
//  Created by Jacek on 21.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "NSManagedObject+GeckoExtensions.h"

#import "CarsManager.h"

#ifndef MyLog
#define MyLog(...) NSLog(__VA_ARGS__)
#endif

@implementation NSManagedObject (GeckoExtensions)

+ (NSString *)entityName {
    return NSStringFromClass([self class]);
}

- (instancetype) onCreate {
    return self;
}

+ (instancetype)createEntityInContext:(NSManagedObjectContext *)context {
    
    return [[NSEntityDescription insertNewObjectForEntityForName:[self entityName]
                                          inManagedObjectContext:context] onCreate];
}

+ (NSEntityDescription *) entityDescriptionInContext:(NSManagedObjectContext *)context {
    return [NSEntityDescription entityForName:[self entityName] inManagedObjectContext: context];
}

+ (NSFetchRequest *) requestInContext: (NSManagedObjectContext *)context {
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:[self entityName]];
    [request setEntity:[self entityDescriptionInContext:context]];
    
    return request;
}

+ (NSArray *)allEntitiesInContext:(NSManagedObjectContext *)context withSortKey:(NSString *)sortKey {
    
    NSFetchRequest *request     = [[NSFetchRequest alloc] initWithEntityName:[self entityName]];
    
    [request setFetchBatchSize:10];
    
    NSSortDescriptor *sortDesc  = [NSSortDescriptor sortDescriptorWithKey:sortKey ascending:YES];
    
    [request setSortDescriptors:@[sortDesc]];
    
    NSError *error = nil;
    NSArray *array = [context executeFetchRequest:request error:&error];
    
    if (error) {
        MyLog(@"error = %@", error);
    }
    
    return array;
}

- (void) updateValue: (id) value forKey: (NSString *) key {
    [self willChangeValueForKey:key];
    [self setPrimitiveValue:value forKey:key];
    [self didChangeValueForKey:key];
}

+ (NSArray *)findAllObjects
{
    return [self findAllObjectsInContext:[[CarsManager object] managedObjectContext]];
}

+ (NSArray *)findAllObjectsInContext:(NSManagedObjectContext *)context;
{
    NSEntityDescription *entity = [self entityDescriptionInContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    if (error != nil)
    {
        MyLog(@"error = %@", error);
    }
    return results;
}

+ (NSArray *)findAllOther:(NSManagedObject*) entity
{
//    NSLog(@"motor do pominiecia %@", moto);
//    NSSet *retSet = [_userData.motorcycles filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"self != %@", moto]];
//    NSLog(@"moto set %@", retSet);
//    NSArray *retArr = [retSet allObjects];
//    return retArr;//[retSet allObjects];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:[self entityName] inManagedObjectContext: [[CarsManager object] managedObjectContext]];
    [fetchRequest setEntity:entityDesc];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"self != %@", entity]];
    
    NSError *error = nil;
    NSArray *result = [[[CarsManager object] managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    NSLog(@"\nselected %@\nother %@\n",entity,result);
    if (error != nil)
    {
        MyLog(@"error = %@", error);
    }
    
    return result;
}

@end
