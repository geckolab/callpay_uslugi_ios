//
//  PRVerifyWarningViewController.m
//  pr
//
//  Created by Marcin Szulc on 04.10.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PRVerifyWarningViewController.h"
#import "PRStandardButton.h"
#import "PRAsyncConnection.h"
#import <QuartzCore/QuartzCore.h>

@interface PRVerifyWarningViewController ()
@property (strong, nonatomic) IBOutlet UIView *viewBackground;
@property (strong, nonatomic) IBOutlet UILabel *labelWarning;
@property (strong, nonatomic) IBOutlet PRStandardButton *buttonOk;

@end

@implementation PRVerifyWarningViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [_buttonOk setEnabled:NO];
    [_labelWarning setText:[NSString stringWithFormat:NSLocalizedString(@"verifyWarning", nil), (_amount/100), (_amount%100)]];
    
    _viewBackground.layer.cornerRadius = 8.0;
    _viewBackground.layer.borderWidth = 1.0;
    _viewBackground.layer.borderColor = [UIColor colorWithWhite:0.66 alpha:0.6].CGColor;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)switcherValueChanged:(id)sender {
    UISwitch *switcher = sender;
    if(switcher.isOn == YES)
    {
        [_buttonOk setEnabled:YES];
    }
    else
    {
        [_buttonOk setEnabled:NO];
    }
}

- (IBAction)buttonOkOnClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        if([_delegate respondsToSelector:@selector(buttonOkClickedInViewController:)] == YES)
        {
            [_delegate buttonOkClickedInViewController:self];
        }
    }];
}

@end
