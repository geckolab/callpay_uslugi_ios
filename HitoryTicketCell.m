//
//  HitoryTicketCell.m
//  CallPay
//
//  Created by Jacek on 02.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "HitoryTicketCell.h"

@implementation HitoryTicketCell

@synthesize deleteButton = _deleteButton;

@synthesize nameLabel = _nameLabel;
@synthesize buyDateLabel = _buyDateLabel;

- (void)awakeFromNib
{
//    [_deleteButton setTranslatesAutoresizingMaskIntoConstraints:NO];
//    [_deleteButton setBackgroundColor:[UIColor redColor]];
//    barsView = [[RectangleWhiteBarsView alloc] initWithFrame:self.bounds];
    barsView = [[RectangleWhiteBarsView alloc] initWithFrame:CGRectMake(0, -1, self.bounds.size.width, self.bounds.size.height+1)];
//    [self.backgroundView addSubview:barsView];
//    [self.contentView addSubview:barsView];
    [self.contentView insertSubview:barsView atIndex:0];
    [_nameLabel setText:@""];
    [_buyDateLabel setText:@""];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [barsView setSelected:selected];
    // Configure the view for the selected state
}
-(void)prepareForReuse
{
    [super prepareForReuse];
    [_nameLabel setText:@""];
    [_buyDateLabel setText:@""];
    [_deleteButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
}

@end
