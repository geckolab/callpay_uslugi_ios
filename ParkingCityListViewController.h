//
//  ParkingCityListViewController.h
//  CallPay
//
//  Created by Jacek on 10.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "ProjectViewController.h"

#import "CatalogsManager.h"
#import "ParkingCatalogsManager.h"

@interface ParkingCityListViewController : ProjectViewController
{
    id activeButton;
    NSString *_cityIndex;
    BOOL checkSelectedButtonClicked;
}

@property (nonatomic, strong) IBOutlet UITableView *tableV;
@property (nonatomic, retain) NSArray *locationsKeys;
@property (nonatomic, retain) ParkingCatalogsManager *catalogsManager;

-(IBAction)newButtonDownloadAction:(id)sender;

@end
