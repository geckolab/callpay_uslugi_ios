//
//  RectangleWhiteBarsView.m
//  CallPay
//
//  Created by Jacek on 31.03.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "RectangleWhiteBarsView.h"

@implementation RectangleWhiteBarsView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setup];
    }
    return self;
}
- (id)init
{
    self = [super init];
    if (self)
    {
        [self setFrame:CGRectMake(0, 0, 300, 55)];
        [self setup];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self setup];
    }
    return self;
}

-(void)setup
{
    self.backgroundColor = [UIColor clearColor];
    NSLog(@"layer %@",self.layer);
    [self.layer setDelegate:self];
    [self.layer setNeedsDisplay];
}
- (void) drawLayer:(CALayer*) layer inContext:(CGContextRef) ctx
{
    NSLog(@"draw layer");
    layer.contentsScale = [UIScreen mainScreen].scale;
    if ([self isHighlighted] || [self isSelected])
    {
        [layer setBackgroundColor:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.48].CGColor];
    }
    else
    {
        [layer setBackgroundColor:[UIColor clearColor].CGColor];
    }
    
    CGContextSetStrokeColorWithColor(ctx, [UIColor whiteColor].CGColor);
    CGContextSetLineWidth(ctx, 2);
    
    CGContextMoveToPoint(ctx, 0, 0);
    CGContextAddLineToPoint(ctx, layer.frame.size.width, 0);
    
    CGContextStrokePath(ctx);
    
    CGContextSetLineWidth(ctx, 2);
    
    CGContextMoveToPoint(ctx, 0, layer.frame.size.height);
    CGContextAddLineToPoint(ctx, layer.frame.size.width, layer.frame.size.height);
    
    CGContextStrokePath(ctx);
    
    
}
-(BOOL)isHighlighted
{
    return isHighlighted;
}
-(BOOL)isSelected
{
    return isSelected;
}
-(void)setSelected:(BOOL)selected
{
    isSelected = selected;
    [self.layer setNeedsDisplay];
}
-(void)setHighlited:(BOOL)highlited
{
    isHighlighted = highlited;
    [self.layer setNeedsDisplay];
}

@end
