//
//  PRAsyncConnection.m
//  pr
//
//  Created by Marcin Szulc on 17.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#define METHOD_POST @"POST"
#define METHOD_GET @"GET"
#define METHOD_DELETE @"DELETE"

#import "PRAsyncConnection.h"
#import "XLog.h"
#import "PRUtils.h"
#import "PROrderObject.h"
#import "DiscountEntity+Additions.h"
#import "PRRelationObject.h"
//#import "PRAppDelegate.h"
#import "SettingsEntity+Additions.h"
#import "IdTypeEntity+Additions.h"
#import "StationEntity+Additions.h"

#import "PRDBManager.h"

static NSString * const LogTag = @"AsyncConnection";

@interface PRAsyncConnection()

@property (strong, nonatomic) id parent;
@property (nonatomic) SEL success;
@property (nonatomic) SEL didFail;

@end

@implementation PRAsyncConnection

- (id) initWithParent:(id) parent successSelector:(SEL) success didFailSelector:(SEL) didFail
{
    self = [self init];
    if(self)
    {
        _parent = parent;
        _success = success;
        _didFail = didFail;
    }

    return self;
}

- (id) init
{
    self = [super init];
    if(self)
    {
        _responseData = [NSMutableData data];
    }

    return self;
}

#pragma mark - private
+ (id) convertNilToNull:(id) object
{
    return object == nil ? [NSNull null] : object;
}

+ (void)clearCaches {
	// clear any URL caches
	[[NSURLCache sharedURLCache] removeAllCachedResponses];

	// clear any cookie caches
	NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/", CONNECTION_CALLPAY_URL]];
	for (NSHTTPCookie *cookie in [cookieStorage cookiesForURL:url]) {
		[cookieStorage deleteCookie:cookie];
	}

	// attempt to clear any credential caches
	NSURLCredentialStorage *credentialStorage = [NSURLCredentialStorage sharedCredentialStorage];
	NSDictionary *credentialsDicationary = [credentialStorage allCredentials];

	for (NSURLProtectionSpace *space in [credentialsDicationary allKeys]) {
		NSDictionary *spaceDictionary = [credentialsDicationary objectForKey:space];
		for (id userName in [spaceDictionary allKeys]) {
			NSURLCredential *credential = [spaceDictionary objectForKey:userName];
			[credentialStorage removeCredential:credential forProtectionSpace:space];
		}
	}
}

- (void) eraseCredentials{
    NSURLCredentialStorage *credentialsStorage = [NSURLCredentialStorage sharedCredentialStorage];
    NSDictionary *allCredentials = [credentialsStorage allCredentials];
    //iterate through all credentials to find the twitter host
    for (NSURLProtectionSpace *protectionSpace in allCredentials)
    {
        NSLog(@"%@",[protectionSpace host]);
        if ([[protectionSpace host] isEqualToString:@"twitter.com"]){
            //to get the twitter's credentials
            NSDictionary *credentials = [credentialsStorage credentialsForProtectionSpace:protectionSpace];
            //iterate through twitter's credentials, and erase them all
            for (NSString *credentialKey in credentials)
                [credentialsStorage removeCredential:[credentials objectForKey:credentialKey] forProtectionSpace:protectionSpace];
        }
    }
}

- (void)clearCookiesForURL {
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray *cookies = [cookieStorage cookiesForURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@/certman/register", CONNECTION_CALLPAY_URL] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    for (NSHTTPCookie *cookie in cookies) {
        [cookieStorage deleteCookie:cookie];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - requests
- (void) sendIsTicketRefundableRequestWithTicketSignature:(NSString*) signature andSerial: (NSString*) serial
{
    NSString *URL = [[NSString stringWithFormat:@"%@/pr_mobile/is_ticket_refundable/%@/%@", CONNECTION_CALLPAY_URL, signature, serial] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    XLogTag(LogTag, @"%@", URL);
    
    NSMutableURLRequest *request = [self setupDefaultURLRequestWIthURL:URL andHttpMethod:METHOD_GET];
    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void) sendRegisterRequestWithPhoneNo:(NSString*) phoneNo andPassword:(NSString*) password andName:(NSString*) name andSurname:(NSString*) surname
{

    NSString *URL = [[NSString stringWithFormat:@"%@/certman/register", CONNECTION_RAPID_URL] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    NSMutableURLRequest *request = [self setupDefaultURLRequestWIthURL:URL andHttpMethod:METHOD_POST];

    NSDictionary *json = [NSDictionary dictionaryWithObjectsAndKeys:
                          [NSDictionary dictionaryWithObjectsAndKeys:phoneNo, @"phone",
                           password, @"cert_pass",
                           name, @"firstname",
                           surname, @"lastname",
                           nil],
                          @"register_request"
                          , nil];

    NSError *error;

    NSData *requestData = [NSJSONSerialization dataWithJSONObject:json options:NSJSONWritingPrettyPrinted error:&error];

    if(error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        return;
    }

    [request setHTTPBody:requestData];

    XLogTag(LogTag, @"%@", [[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding]);

    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void) sendGetCertificateRequestWithCertToken:(NSString*) cert_token andSMScode:(NSString*) smsCode
{
    NSString *URL = [[NSString stringWithFormat:@"%@/certman/download/%@/%@", CONNECTION_RAPID_URL, cert_token, smsCode] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [self setupDefaultURLRequestWIthURL:URL andHttpMethod:METHOD_GET];
    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void) sendDBCheckVersionRequestWithPhoneNo:(NSString *)phoneNo
{
    NSString *URL = [[NSString stringWithFormat:@"%@/pr_mobile/db_sync/%@", CONNECTION_CALLPAY_URL, phoneNo] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [self setupDefaultURLRequestWIthURL:URL andHttpMethod:METHOD_GET];
    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void) sendUpdateStationsRequest
{
    NSString *URL = [[NSString stringWithFormat:@"%@/pr_mobile/v2/stations/%@", CONNECTION_CALLPAY_URL, [PRUtils getFormattedDateFromDateForConnection:[NSDate date]]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [self setupDefaultURLRequestWIthURL:URL andHttpMethod:METHOD_GET];
    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void) sendUpdateDiscountsRequest
{
    NSString *URL = [[NSString stringWithFormat:@"%@/pr_mobile/discounts", CONNECTION_CALLPAY_URL] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [self setupDefaultURLRequestWIthURL:URL andHttpMethod:METHOD_GET];
    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void) sendUpdateTrainAttributesRequest
{
    NSString *URL = [[NSString stringWithFormat:@"%@/pr_mobile/train_attributes", CONNECTION_CALLPAY_URL] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [self setupDefaultURLRequestWIthURL:URL andHttpMethod:METHOD_GET];
    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void) sendUpdateIdTypesRequest
{
    NSString *URL = [[NSString stringWithFormat:@"%@/pr_mobile/document_types", CONNECTION_CALLPAY_URL] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [self setupDefaultURLRequestWIthURL:URL andHttpMethod:METHOD_GET];
    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void) sendGetStationsToRequestWithDate:(NSString*) date andStationFromIndex:(int) index
{
    NSString *URL = [[NSString stringWithFormat:@"%@/pr_mobile/stations/%@/%d", CONNECTION_CALLPAY_URL, date, index] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [self setupDefaultURLRequestWIthURL:URL andHttpMethod:METHOD_GET];
    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void) sendGetRelationsRequestWithDate:(NSString*) date stationFromIndex:(int) stationFrom andStationToIndex:(int) stationTo
{
    NSString *URL = [[NSString stringWithFormat:@"%@/pr_mobile/journeys/%@/%d/%d", CONNECTION_CALLPAY_URL, date, stationFrom, stationTo] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [self setupDefaultURLRequestWIthURL:URL andHttpMethod:METHOD_GET];

    XLogTag(LogTag, @"%@", URL);

    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void) sendGetUpaidTokenWithPhoneNo:(NSString*) phoneNo
{
    NSString *URL = [[NSString stringWithFormat:@"%@/pr_mobile/upaid_token/%@", CONNECTION_CALLPAY_URL, phoneNo] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [self setupDefaultURLRequestWIthURL:URL andHttpMethod:METHOD_GET];

    XLogTag(LogTag, @"%@", URL);

    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void) sendStartJourneyRequestWithOrderObject:(PROrderObject*) orderObject
{


    NSString *URL = [[NSString stringWithFormat:@"%@/pr_mobile/start_journey", CONNECTION_CALLPAY_URL] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    NSMutableURLRequest *request = [self setupDefaultURLRequestWIthURL:URL andHttpMethod:METHOD_POST];

    NSDictionary *owner = [NSDictionary dictionaryWithObjectsAndKeys:
                           [PRAsyncConnection convertNilToNull:orderObject.passengerName], @"name",
                           [PRAsyncConnection convertNilToNull:orderObject.passengerSurname], @"surname",
                           [PRAsyncConnection convertNilToNull:orderObject.passengerIdType.idTypeId], @"document",
                           [PRAsyncConnection convertNilToNull:orderObject.passengerIdNo], @"serial",
                           nil];
    NSDictionary *owner_data = [NSDictionary dictionaryWithObject:owner forKey:@"owner"];

    NSDictionary *passenger = [NSDictionary dictionaryWithObjectsAndKeys:
                               [PRAsyncConnection convertNilToNull:orderObject.discount.discountId], @"discount",
                               [PRAsyncConnection convertNilToNull:[NSNumber numberWithBool:orderObject.passengerHasRegioCard]], @"regio",
                               nil];

    NSArray *passengers = [NSArray arrayWithObject:[NSDictionary dictionaryWithObject:passenger forKey:@"passenger"]];

    NSString *journeyToTime = orderObject.relationFrom.relationDepartureTime;
    NSString *journeyReturnTime = orderObject.relationBack == nil ? nil : orderObject.relationBack.relationDepartureTime;
    
    journeyToTime = [journeyToTime stringByReplacingOccurrencesOfString:@":" withString:@""];
    if(journeyReturnTime != nil)
    {
        journeyReturnTime = [journeyReturnTime stringByReplacingOccurrencesOfString:@":" withString:@""];
    }
    
    NSDictionary *start_journey_request = [NSDictionary dictionaryWithObjectsAndKeys:
                                           [PRAsyncConnection convertNilToNull:orderObject.passengerPhone], @"phone",
                                           [PRAsyncConnection convertNilToNull:orderObject.cardID], @"card_id",
                                           [PRAsyncConnection convertNilToNull:orderObject.relationFrom.relationId], @"journey_to",
                                           [PRAsyncConnection convertNilToNull:orderObject.relationBack.relationId], @"journey_return",
                                           [PRAsyncConnection convertNilToNull:orderObject.stationFrom.stationId], @"station_from",
                                           [PRAsyncConnection convertNilToNull:orderObject.stationTo.stationId], @"station_to",
                                           
                                           [PRAsyncConnection convertNilToNull:journeyToTime], @"journey_to_time",
                                           orderObject.relationBack == nil ? [NSNull null] : [PRAsyncConnection convertNilToNull:journeyReturnTime], @"journey_return_time",
                                           
                                           [PRAsyncConnection convertNilToNull:[PRUtils getFormattedDateFromDateForConnection:orderObject.orderDateFrom]], @"journey_to_date",
                                           orderObject.relationBack == nil ? [NSNull null] : [PRAsyncConnection convertNilToNull:[PRUtils getFormattedDateFromDateForConnection:orderObject.orderDateBack]], @"journey_return_date",
                                           [PRAsyncConnection convertNilToNull: orderObject.relationFrom.relationSelectedClass], @"vehicle_to_class",
                                           [PRAsyncConnection convertNilToNull:orderObject.relationBack.relationSelectedClass], @"vehicle_return_class",
                                           [PRAsyncConnection convertNilToNull:owner_data], @"owner_data",
                                           [PRAsyncConnection convertNilToNull:passengers], @"passengers",
                                           nil];
    
    NSDictionary *json = [NSDictionary dictionaryWithObjectsAndKeys:
                          [PRAsyncConnection convertNilToNull:start_journey_request], @"start_journey_request"
                          , nil];



    NSError *error;

    NSData *requestData = [NSJSONSerialization dataWithJSONObject:json options:NSJSONWritingPrettyPrinted error:&error];

    XLogTag(LogTag, @"%@", [[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding]);

    if(error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        return;
    }

    [request setHTTPBody:requestData];

    [NSURLConnection connectionWithRequest:request delegate:self];

}

- (void) sendAuthPaymentRequestWithOrderObject:(PROrderObject*) orderObject
{

    NSString *URL = [[NSString stringWithFormat:@"%@/pr_mobile/auth_payment", CONNECTION_CALLPAY_URL] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    NSMutableURLRequest *request = [self setupDefaultURLRequestWIthURL:URL andHttpMethod:METHOD_POST];

    NSDictionary *json = [NSDictionary dictionaryWithObjectsAndKeys:
                              [NSDictionary dictionaryWithObjectsAndKeys:
                               [PRAsyncConnection convertNilToNull:orderObject.passengerAuthCode], @"auth_code",
                               [PRAsyncConnection convertNilToNull:orderObject.passengerTransactionToken], @"transaction_token",
                               nil],
                          @"auth_payment_request"
                          , nil];

    NSError *error;

    NSData *requestData = [NSJSONSerialization dataWithJSONObject:json options:NSJSONWritingPrettyPrinted error:&error];

    XLogTag(LogTag, @"%@", [[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding]);

    if(error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        return;
    }

    [request setHTTPBody:requestData];

    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void) sendGetListOfCards
{
    NSString *URL = [[NSString stringWithFormat:@"%@/pr_mobile/cards", CONNECTION_CALLPAY_URL] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [self setupDefaultURLRequestWIthURL:URL andHttpMethod:METHOD_GET];
    
    XLogTag(LogTag, @"%@", URL);
    
    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void) sendRegisterCardWithNumber:(NSString *)cardNumber
                     expirationDate:(NSString *)expirationDate
{
    NSString *URL = [[NSString stringWithFormat:@"%@/pr_mobile/cards", CONNECTION_CALLPAY_URL]
                               stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    NSMutableURLRequest *request = [self setupDefaultURLRequestWIthURL:URL andHttpMethod:METHOD_POST];

    NSDictionary *json = [NSDictionary dictionaryWithObjectsAndKeys:[NSDictionary dictionaryWithObjectsAndKeys:cardNumber, @"pan",
                                                                                                               expirationDate, @"exp_date",
                                                                                                               nil], @"register_card_request",
                                                                    nil];

    NSError *error;

    NSData *requestData = [NSJSONSerialization dataWithJSONObject:json options:NSJSONWritingPrettyPrinted error:&error];

    if (error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        return;
    }

    [request setHTTPBody:requestData];

    XLogTag(LogTag, @"%@", [[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding]);

    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void)sendDeleteCardWithNumber:(NSString *)cardNumber
{
    NSString *URL = [[NSString stringWithFormat:@"%@/pr_mobile/cards/%@", CONNECTION_CALLPAY_URL, cardNumber]
                               stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    NSMutableURLRequest *request = [self setupDefaultURLRequestWIthURL:URL andHttpMethod:METHOD_DELETE];

    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void)sendRefundTicketWithSerialNumber:(NSString *)serial
                               signature:(NSString *)signature
{
    NSString *URL = [[NSString stringWithFormat:@"%@/pr_mobile/ticket_refund", CONNECTION_CALLPAY_URL]
                               stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    NSMutableURLRequest *request = [self setupDefaultURLRequestWIthURL:URL andHttpMethod:METHOD_POST];

    NSDictionary *json = [NSDictionary dictionaryWithObjectsAndKeys:[NSDictionary dictionaryWithObjectsAndKeys:signature, @"signature",
                                                                                                               serial, @"serial",
                                                                                                               nil], @"ticket_refund_request",
                                                                    nil];

    NSError *error;

    NSData *requestData = [NSJSONSerialization dataWithJSONObject:json options:NSJSONWritingPrettyPrinted error:&error];

    if (error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        return;
    }

    [request setHTTPBody:requestData];

    XLogTag(LogTag, @"%@", [[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding]);

    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void)sendFreezeMoneyRequestWithCardId:(NSString*) cardId andAuthCode:(NSString*) authCode
{
    NSString *URL = [[NSString stringWithFormat:@"%@/pr_mobile/freeze_money", CONNECTION_CALLPAY_URL] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [self setupDefaultURLRequestWIthURL:URL andHttpMethod:METHOD_POST];
    
    NSDictionary *json = [NSDictionary dictionaryWithObjectsAndKeys:
                          [NSDictionary dictionaryWithObjectsAndKeys:cardId, @"id", authCode, @"auth_code", nil],
                          @"freeze_money_request"
                          , nil];
    
    NSError *error;
    
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:json options:NSJSONWritingPrettyPrinted error:&error];
    
    if(error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        return;
    }
    
    [request setHTTPBody:requestData];
    
    XLogTag(LogTag, @"%@", [[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding]);
    
    [NSURLConnection connectionWithRequest:request delegate:self];

}

- (void)sendVerifyCardRequestWithCardId:(NSString*) cardId andAmount:(int) amount
{
    
    NSString *URL = [[NSString stringWithFormat:@"%@/pr_mobile/verify_card", CONNECTION_CALLPAY_URL] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [self setupDefaultURLRequestWIthURL:URL andHttpMethod:METHOD_POST];
    
    NSDictionary *json = [NSDictionary dictionaryWithObjectsAndKeys:
                          [NSDictionary dictionaryWithObjectsAndKeys: cardId, @"id", [NSNumber numberWithInt:amount], @"amount", nil],
                          @"verify_card_request",
                          nil];
    
    NSError *error;
    
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:json options:NSJSONWritingPrettyPrinted error:&error];
    
    if(error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        return;
    }
    
    [request setHTTPBody:requestData];
    
    XLogTag(LogTag, @"%@", [[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding]);
    
    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void)sendTotalTransactionsValueRequestWithCardId:(NSString*) cardId
{
    NSString *URL = [[NSString stringWithFormat:@"%@/pr_mobile/total_transactions_value/%@", CONNECTION_CALLPAY_URL, cardId] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [self setupDefaultURLRequestWIthURL:URL andHttpMethod:METHOD_GET];
    
    XLogTag(LogTag, @"%@", URL);
    
    [NSURLConnection connectionWithRequest:request delegate:self];
}

#pragma mark - connection methods

- (NSMutableURLRequest*) setupDefaultURLRequestWIthURL:(NSString*) URL andHttpMethod:(NSString*) method
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:URL] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30.0];
    [request setHTTPShouldHandleCookies:NO];
    [request setHTTPMethod:method];
    [request setValue:@"application/json;charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    return request;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[_responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[_responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    XLogTag(LogTag, @"%@", error.localizedDescription);
    
	if([_parent respondsToSelector:_didFail])
    {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [_parent performSelector:_didFail withObject:self];
#pragma clang diagnostic pop 
    }
    else
    {
        [PRUtils displayStandardAlertViewWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertConnectionFailed", nil) andConfirmButtonTitle:NSLocalizedString(@"alertOk", nil)];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {

    if([_parent respondsToSelector:_success])
    {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [_parent performSelector:_success withObject:self];
#pragma clang diagnostic pop        
    }
}

#pragma mark - https
- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse
{
    return nil;
}

- (BOOL)connectionShouldUseCredentialStorage:(NSURLConnection *)connection
{
    return NO;
}
- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    XLogTag(LogTag, @"Authentication challenge");

    NSString *filename = @"gecko_cert_1";
    NSString *path = [[NSBundle mainBundle] pathForResource:filename ofType:@"p12"];

//    PRAppDelegate *appDelegate = (PRAppDelegate*)[UIApplication sharedApplication].delegate;
    SettingsEntity *settings = [SettingsEntity fetchSettingsObjectWithManagedObjectContext:[[PRDBManager object] managedObjectContext]];

    if(settings.userVerified.boolValue == YES)
    {
        filename = settings.userCertFileName;
        NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        path = [applicationDocumentsDir stringByAppendingPathComponent:filename];
    }

    XLogTag(LogTag, @"%@", filename);

    // load cert
    NSData *p12data = [NSData dataWithContentsOfFile:path];

    CFDataRef inP12data = (__bridge CFDataRef)p12data;

    SecIdentityRef myIdentity = NULL;
    SecTrustRef myTrust;
    //OSStatus status = extractIdentityAndTrust(inP12data, &myIdentity, &myTrust);

    extractIdentityAndTrust(inP12data, &myIdentity, &myTrust);

    SecCertificateRef myCertificate;
    SecIdentityCopyCertificate(myIdentity, &myCertificate);
    const void *certs[] = { myCertificate };
    CFArrayRef certsArray = CFArrayCreate(NULL, certs, 1, NULL);

    NSURLCredential *credential = [NSURLCredential credentialWithIdentity:myIdentity certificates:(__bridge NSArray*)certsArray persistence:NSURLCredentialPersistenceNone];

    [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];


}

- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
{
    return [protectionSpace.authenticationMethod
			isEqualToString:NSURLAuthenticationMethodServerTrust];
    //return YES;
}

OSStatus extractIdentityAndTrust(CFDataRef inP12data, SecIdentityRef *identity, SecTrustRef *trust)
{
    NSString *stringPassword = @"fK5gq@12P%z";

//    PRAppDelegate *appDelegate = (PRAppDelegate*)[UIApplication sharedApplication].delegate;
    SettingsEntity *settings = [SettingsEntity fetchSettingsObjectWithManagedObjectContext:[[PRDBManager object] managedObjectContext]];

    if(settings.userVerified.boolValue == YES)
    {
        stringPassword = settings.userPassword;
    }

    OSStatus securityError = errSecSuccess;

    CFStringRef password = (__bridge CFStringRef) stringPassword;//CFSTR("gecko_cert_1");
    const void *keys[] = { kSecImportExportPassphrase };
    const void *values[] = { password };

    CFDictionaryRef options = CFDictionaryCreate(NULL, keys, values, 1, NULL, NULL);

    CFArrayRef items = CFArrayCreate(NULL, 0, 0, NULL);
    securityError = SecPKCS12Import(inP12data, options, &items);

    if (securityError == 0) {
        CFDictionaryRef myIdentityAndTrust = CFArrayGetValueAtIndex(items, 0);
        const void *tempIdentity = NULL;
        tempIdentity = CFDictionaryGetValue(myIdentityAndTrust, kSecImportItemIdentity);
        *identity = (SecIdentityRef)tempIdentity;
        const void *tempTrust = NULL;
        tempTrust = CFDictionaryGetValue(myIdentityAndTrust, kSecImportItemTrust);
        *trust = (SecTrustRef)tempTrust;
    }

    if (options) {
        CFRelease(options);
    }

    return securityError;
}

@end
