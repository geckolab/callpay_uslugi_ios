//
//  PRPinViewController.h
//  pr
//
//  Created by Marcin Szulc on 24.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PRStandardButton;
@class PROrderObject;
@class PRCardObject;

@interface PRPinViewController : UIViewController

@property (strong, nonatomic) PROrderObject *orderObject;
@property (strong, nonatomic) PRCardObject *defaultCard;

@property (strong, nonatomic) IBOutlet UILabel *labelPin;
@property (strong, nonatomic) IBOutlet UITextField *textPin;
@property (strong, nonatomic) IBOutlet PRStandardButton *buttonBuyTicket;
@property (strong, nonatomic) IBOutlet UILabel *labelDescription;
@property (strong, nonatomic) IBOutlet UIImageView *imageCvC;
@property (strong, nonatomic) IBOutlet UILabel *labelYouPayByCard;

- (IBAction)backgroundOnClick:(id)sender;
- (IBAction)buttonBuyTicketOnClick:(id)sender;

@end
