//
//  NSManagedObject+Extensions.h
//
//  Created by Michał Majewski on 05.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (Extensions)

+ (NSString *)entityName;
+ (NSEntityDescription *) entityDescriptionInContext:(NSManagedObjectContext *)context;

/**
 *  Tworzy nowy obiekt encji. Context zazwyczaj jest trzymany w AppDelegate
 */
+ (instancetype)createEntityInContext:(NSManagedObjectContext *)context;

/**
 *  Metoda zwraca obiekt requesta, umozliwiający wyszukiwanie encji w bazie
 */
+ (NSFetchRequest *) requestInContext: (NSManagedObjectContext *)context;

/**
 *  Metoda zwraca wszystkie encje
 */
+ (NSArray *)allEntitiesInContext:(NSManagedObjectContext *)context withSortKey:(NSString *)sortKey;

/**
 *  Metoda umożliwiająca stworzenie customowego settera w obiekcie encji.
 *
 *  Przykłady:
 *
 *   - (void)setEndPrice:(NSNumber *)endPrice {
 *     [self updateValue: endPrice forKey: @"endPrice"];
 *     //...
 *   }
 *
 *   - (void) setFirstName:(NSString *)firstName {
 *     [self updateValue:firstName forKey:@"firstName"];
 *     //...
 *   }
 */
- (void) updateValue: (id) value forKey: (NSString *) key;

/**
 *  Metoda wywoływana automatycznie w metodzie createEntityInContext:
 *  Umożliwia wstępną inicjalizację pól w encji.
 *
 *  Do wykorzystania tak samo jak metoda init w zwykłych klasach
 */
- (instancetype) onCreate;

@end