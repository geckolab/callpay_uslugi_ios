//
//  UIAlertView+Extensions.h
//  Moneyupp
//
//  Created by Paweł Grzmil on 27.03.2014.
//  Copyright (c) 2014 Fenige Sp. z o.o. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (Extensions)
+ (void)showWithTitle:(NSString *)title message:(NSString *)message;
+ (void)showWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate;
+ (void)showWithTitle:(NSString *)title message:(NSString *)message tag:(int)tag delegate:(id)delegate;
+ (void)showWithTitle:(NSString *)title message:(NSString *)message okButton:(NSString *)okButtonText cancelButton:(NSString *)cancelButtonText delegate:(id)delegate;
+ (void)showWithTitle:(NSString *)title message:(NSString *)message okButton:(NSString *)okButtonText cancelButton:(NSString *)cancelButtonText delegate:(id)delegate tag:(NSInteger)tag;
+ (void)showInputWithTitle:(NSString *)title message:(NSString *)message okButton:(NSString *)okButton cancelButton:(NSString *)cancelButtonText delegate:(id)delegate;
+ (void)showInputWithTitle:(NSString *)title message:(NSString *)message okButton:(NSString *)okButtonText cancelButton:(NSString *)cancelButtonText keyboardType:(UIKeyboardType)keyboardType delegate:(id)delegate tag:(NSInteger)tag;

@end
