//
//  NSString+Extensions.h
//
//  Created by Michał Majewski on 10.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extensions)

- (NSAttributedString *) addColor: (UIColor *) color toSubstring: (NSString *) string;
- (NSString *)md5;

+ (BOOL) isNilOrEmpty: (NSString *) stringToValidate;
- (NSString *)urlEncode;
- (NSString *)urlDecode;

@end
