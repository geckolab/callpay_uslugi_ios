//
//  UINavigationController+Extensions.m
//  uPaidWallet
//
//  Created by Paweł Grzmil on 21.02.2014.
//  Copyright (c) 2014 uPaid Sp. z o.o. All rights reserved.
//

#import "UINavigationController+Extensions.h"

@implementation UINavigationController (Extensions)

- (void)popNumberOfViewControllers:(int)number Animated:(BOOL)animated {
	if (number <= 0)
		return;

	NSMutableArray *newViewControllers = [NSMutableArray arrayWithArray:self.viewControllers];
	while (newViewControllers.count > 1 && number > 1) {
		[newViewControllers removeObjectAtIndex:newViewControllers.count - 2];
		number--;
	}

	[self setViewControllers:newViewControllers animated:NO];
	[self popViewControllerAnimated:animated];
}

- (void)setTitle:(NSString *)title {
	self.navigationBar.topItem.title = title;
}

#pragma mark - Set button methods

- (void)setBackButtonImage:(UIImage *)image pressedImage:(UIImage *)pressedImage {
	self.navigationBar.backIndicatorImage = image;
	self.navigationBar.backIndicatorTransitionMaskImage = pressedImage;
}

- (void)setRightButtonWithIcon:(UIImage *)icon andPressedIcon:(UIImage *)iconPressed andTarget:(id)target andOnClickMethod:(SEL)action {
	CGRect frameimg = CGRectMake(0, 0, icon.size.width, icon.size.height);
	UIButton *button = [[UIButton alloc] initWithFrame:frameimg];
	[button setBackgroundImage:icon forState:UIControlStateNormal];
	[button setBackgroundImage:iconPressed forState:UIControlStateHighlighted];
	[button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];

	UIBarButtonItem *rightNavButton = [[UIBarButtonItem alloc] initWithCustomView:button];
	((UIViewController *)self.viewControllers[self.viewControllers.count - 1]).navigationItem.rightBarButtonItem = rightNavButton;
}

- (void)setRightButtonWithTitle:(NSString *)title andTarget:(id)target andOnClickMethod:(SEL)action {
	UIBarButtonItem *rightNavButton = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:target action:action];
	((UIViewController *)self.viewControllers[self.viewControllers.count - 1]).navigationItem.rightBarButtonItem = rightNavButton;
}

#pragma mark - Remove navigation items

- (void)removeAllBackEntries {
	NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray:self.viewControllers];
	if (navigationArray.count > 1) {
		while (navigationArray.count > 1)
			[navigationArray removeObjectAtIndex:navigationArray.count - 2];
	}

	[self setViewControllers:navigationArray animated:NO];
}

- (void)removeBackNavigationBackEntry {
	NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray:self.viewControllers];
	if (navigationArray.count > 1) {
		[navigationArray removeObjectAtIndex:navigationArray.count - 2];
		self.viewControllers = navigationArray;
	}
}

@end
