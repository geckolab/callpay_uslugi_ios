//
//  UIWebView+Extensions.m
//  uPaidWallet
//
//  Created by Paweł Grzmil on 30.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o. All rights reserved.
//

#import "UIWebView+Extensions.h"

@implementation UIWebView (Extensions)

- (void)navigateToPage:(NSString *)stringUrl {
  [self navigateToURL: [NSURL URLWithString:stringUrl]];
}

- (void)navigateToURL:(NSURL *)url {
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
	[self loadRequest:urlRequest];
}

@end
