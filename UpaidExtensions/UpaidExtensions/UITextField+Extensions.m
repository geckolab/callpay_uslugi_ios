//
//  UITextField+Extensions.m
//  uPaidWallet
//
//  Created by Paweł Grzmil on 24.02.2014.
//  Copyright (c) 2014 uPaid Sp. z o.o. All rights reserved.
//

#import "UITextField+Extensions.h"

@implementation UITextField (Extensions)

- (void)setPlaceholderColor:(UIColor *)color {
	[self setValue:color forKeyPath:@"_placeholderLabel.textColor"];
}

@end
