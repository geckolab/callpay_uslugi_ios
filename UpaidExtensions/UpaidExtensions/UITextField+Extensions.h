//
//  UITextField+Extensions.h
//  uPaidWallet
//
//  Created by Paweł Grzmil on 24.02.2014.
//  Copyright (c) 2014 uPaid Sp. z o.o. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Extensions)
- (void)setPlaceholderColor:(UIColor *)color;
@end
