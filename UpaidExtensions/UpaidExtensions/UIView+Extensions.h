//
//  UIView+Extensions.h
//
//  Created by Michał Majewski on 04.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Extensions)

/**
 *  Metody ułatwiająca zmianę wymiarow frame
 */
- (void) setFrameHeight: (CGFloat) height;
- (void) setFrameWidth: (CGFloat) width;
- (void) setFrameOriginX: (CGFloat) x;
- (void) setFrameOriginY: (CGFloat) y;

/**
 *  Metoda pomocnicza, listująca rekursywnie (za pomocą NSLog) wszystkie subviews w danym widoku.
 */
- (void) listSubviews;

/**
 *  Metoda ułatwiająca stworzenie własnej kontrolki z xibem. Należy ją wywołać w metodzie initWithCoder:
 *
 *  @return BOOL czy udało się załadować niba
 */
- (BOOL) setupNib;

/**
 *  Metoda ułatwiająca stworzenie własnej kontrolki z xibem. Należy ją wywołać w metodzie initWithCoder:
 *  Pozwala na podanie xiba o innej nazwie, niż nazwa klasy obsługującej xiba.
 *
 *  @return BOOL czy udało się załadować niba
 */
- (BOOL) setupWithNibName: (NSString *) nibName;

#pragma mark viewController callbacks

//metody do umieszczenia w BaseViewController, w analogicznych metodach w kontrolerze. Dzięki temu wszystkie własne kontrolki mogą reagowac na zmiany w cyklu życia ViewController
- (void) viewDidLoad;
- (void) viewDidDisappear;
- (void) viewDidAppear;
- (void) viewWillAppear;

@end
