//
//  UIWebView+Extensions.h
//  uPaidWallet
//
//  Created by Paweł Grzmil on 30.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWebView (Extensions)
- (void)navigateToPage:(NSString *)stringUrl;
- (void)navigateToURL:(NSURL *)url;
@end
