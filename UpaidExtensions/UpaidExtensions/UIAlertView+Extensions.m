//
//  UIAlertView+Extensions.m
//  Moneyupp
//
//  Created by Paweł Grzmil on 27.03.2014.
//  Copyright (c) 2014 Fenige Sp. z o.o. All rights reserved.
//

#import "UIAlertView+Extensions.h"

@implementation UIAlertView (Extensions)
+ (void)showWithTitle:(NSString *)title message:(NSString *)message {
	[self showWithTitle:title message:message okButton:@"OK" cancelButton:nil delegate:nil tag:0];
}

+ (void)showWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate {
	[self showWithTitle:title message:message okButton:@"OK" cancelButton:nil delegate:delegate tag:0];
}

+ (void)showWithTitle:(NSString *)title message:(NSString *)message tag:(int)tag delegate:(id)delegate {
	[self showWithTitle:title message:message okButton:@"OK" cancelButton:nil delegate:delegate tag:tag];
}

+ (void)showWithTitle:(NSString *)title message:(NSString *)message okButton:(NSString *)okButtonText cancelButton:(NSString *)cancelButtonText delegate:(id)delegate {
	[self showWithTitle:title message:message okButton:okButtonText cancelButton:cancelButtonText delegate:delegate tag:0];
}

+ (void)showWithTitle:(NSString *)title message:(NSString *)message okButton:(NSString *)okButtonText cancelButton:(NSString *)cancelButtonText delegate:(id)delegate tag:(NSInteger)tag {
	cancelButtonText =  cancelButtonText == nil ? nil : NSLocalizedString(cancelButtonText, nil);

	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(title, nil) message:NSLocalizedString(message, nil) delegate:delegate cancelButtonTitle:cancelButtonText otherButtonTitles:NSLocalizedString(okButtonText, nil), nil];
	alert.tag = tag;
	[alert show];
}

+ (void)showInputWithTitle:(NSString *)title message:(NSString *)message okButton:(NSString *)okButtonText cancelButton:(NSString *)cancelButtonText delegate:(id)delegate {
	[self showInputWithTitle:title message:message okButton:okButtonText cancelButton:cancelButtonText keyboardType:UIKeyboardTypeNumberPad delegate:delegate tag:0];
}

+ (void)showInputWithTitle:(NSString *)title message:(NSString *)message okButton:(NSString *)okButtonText cancelButton:(NSString *)cancelButtonText keyboardType:(UIKeyboardType)keyboardType delegate:(id)delegate tag:(NSInteger)tag {
	cancelButtonText =  cancelButtonText == nil ? nil : NSLocalizedString(cancelButtonText, nil);

	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(title, nil) message:NSLocalizedString(message, nil) delegate:self cancelButtonTitle:cancelButtonText otherButtonTitles:NSLocalizedString(okButtonText, nil), nil];
	alert.alertViewStyle = UIAlertViewStylePlainTextInput;
	alert.tag = tag;
	[[alert textFieldAtIndex:0] setKeyboardType:keyboardType];
	[[alert textFieldAtIndex:0] becomeFirstResponder];
	[alert show];
}

@end
