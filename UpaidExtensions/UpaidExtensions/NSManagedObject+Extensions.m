//
//  NSManagedObject+Extensions.m
//
//  Created by Michał Majewski on 05.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "NSManagedObject+Extensions.h"

#ifndef MyLog
#define MyLog(...) NSLog(__VA_ARGS__)
#endif

@implementation NSManagedObject (Extensions)

+ (NSString *)entityName {
  return NSStringFromClass([self class]);
}

- (instancetype) onCreate {
  return self;
}

+ (instancetype)createEntityInContext:(NSManagedObjectContext *)context {
  
  return [[NSEntityDescription insertNewObjectForEntityForName:[self entityName]
                                        inManagedObjectContext:context] onCreate];
}

+ (NSEntityDescription *) entityDescriptionInContext:(NSManagedObjectContext *)context {
  return [NSEntityDescription entityForName:[self entityName] inManagedObjectContext: context];
}

+ (NSFetchRequest *) requestInContext: (NSManagedObjectContext *)context {
  NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:[self entityName]];
  [request setEntity:[self entityDescriptionInContext:context]];
  
  return request;
}

+ (NSArray *)allEntitiesInContext:(NSManagedObjectContext *)context withSortKey:(NSString *)sortKey {
  
  NSFetchRequest *request     = [[NSFetchRequest alloc] initWithEntityName:[self entityName]];
  
  [request setFetchBatchSize:10];
  
  NSSortDescriptor *sortDesc  = [NSSortDescriptor sortDescriptorWithKey:sortKey ascending:YES];
  
  [request setSortDescriptors:@[sortDesc]];
  
  NSError *error = nil;
  NSArray *array = [context executeFetchRequest:request error:&error];
  
  if (error) {
    MyLog(@"error = %@", error);
  }
  
  return array;
}

- (void) updateValue: (id) value forKey: (NSString *) key {
  [self willChangeValueForKey:key];
  [self setPrimitiveValue:value forKey:key];
  [self didChangeValueForKey:key];
}

@end
