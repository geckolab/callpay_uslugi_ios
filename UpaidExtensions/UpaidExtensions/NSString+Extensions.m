//
//  NSString+Extensions.m
//
//  Created by Michał Majewski on 10.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "NSString+Extensions.h"
#import <CommonCrypto/CommonCrypto.h>

@implementation NSString (Extensions)

- (NSAttributedString *)addColor:(UIColor *)color toSubstring:(NSString *)string {
	NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:self];
	[attrString addAttribute:NSForegroundColorAttributeName value:color range:[self rangeOfString:string]];

	return attrString;
}

+ (BOOL)isNilOrEmpty:(NSString *)stringToValidate {
	return stringToValidate == nil || [stringToValidate isEqualToString:@""];
}

- (NSString *)md5 {
	const char *cStr = [self UTF8String];
	unsigned char digest[16];
	CC_MD5(cStr, strlen(cStr), digest);   // This is the md5 call

	NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];

	for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
		[output appendFormat:@"%02x", digest[i]];

	return output;
}

- (NSString *)urlEncode {
  return [self urlEncodeUsingEncoding:NSUTF8StringEncoding];
}

- (NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding {
  return (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                               (__bridge CFStringRef)self,
                                                                               NULL,
                                                                               (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
                                                                               CFStringConvertNSStringEncodingToEncoding(encoding));
}

- (NSString *)urlDecode {
  return [self urlDecodeUsingEncoding:NSUTF8StringEncoding];
}

- (NSString *)urlDecodeUsingEncoding:(NSStringEncoding)encoding {
  return (__bridge_transfer NSString *)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL,
                                                                                               (__bridge CFStringRef)self,
                                                                                               CFSTR(""),
                                                                                               CFStringConvertNSStringEncodingToEncoding(encoding));
}

@end
