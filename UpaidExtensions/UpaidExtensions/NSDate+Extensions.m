//
//  NSDate+Extensions.m
//  Moneyupp
//
//  Created by Paweł Grzmil on 27.03.2014.
//  Copyright (c) 2014 Fenige Sp. z o.o. All rights reserved.
//

#import "NSDate+Extensions.h"

@implementation NSDate (Extensions)
+ (NSInteger)currentDay {
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDate *now = [NSDate date];
	NSDateComponents *comps = [gregorian components:NSDayCalendarUnit fromDate:now];
	return [comps day];
}

+ (NSInteger)currentMonth {
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDate *now = [NSDate date];
	NSDateComponents *comps = [gregorian components:NSMonthCalendarUnit fromDate:now];
	return [comps month];
}

+ (NSInteger)currentYear {
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDate *now = [NSDate date];
	NSDateComponents *comps = [gregorian components:NSYearCalendarUnit fromDate:now];
	return [comps year];
}

@end
