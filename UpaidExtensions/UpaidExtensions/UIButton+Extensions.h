//
//  UIButton+Extensions.h
//  Moneyupp
//
//  Created by Paweł Grzmil on 20.03.2014.
//  Copyright (c) 2014 Fenige Sp. z o.o. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Extensions)
- (void)setImageForStateNormal:(UIImage *)normalImage andSelectedState:(UIImage *)selectedImage;
- (void)setTitleForAllStates:(NSString *)text;
@end
