//
//  UIButton+Extensions.m
//  Moneyupp
//
//  Created by Paweł Grzmil on 20.03.2014.
//  Copyright (c) 2014 Fenige Sp. z o.o. All rights reserved.
//

#import "UIButton+Extensions.h"

@implementation UIButton (Extensions)
- (void)setImageForStateNormal:(UIImage *)normalImage andSelectedState:(UIImage *)selectedImage {
	[self setBackgroundImage:normalImage forState:UIControlStateNormal];
	[self setBackgroundImage:selectedImage forState:UIControlStateSelected];
}

- (void)setTitleForAllStates:(NSString *)text {
	[self setTitle:text forState:UIControlStateNormal];
	[self setTitle:text forState:UIControlStateSelected];
}

@end
