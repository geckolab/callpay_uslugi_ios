//
//  NSDate+Extensions.h
//  Moneyupp
//
//  Created by Paweł Grzmil on 27.03.2014.
//  Copyright (c) 2014 Fenige Sp. z o.o. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Extensions)
+ (NSInteger)currentDay;
+ (NSInteger)currentMonth;
+ (NSInteger)currentYear;
@end
