//
//  UINavigationController+Extensions.h
//  uPaidWallet
//
//  Created by Paweł Grzmil on 21.02.2014.
//  Copyright (c) 2014 uPaid Sp. z o.o. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (Extensions)
- (void)popNumberOfViewControllers:(int)number Animated:(BOOL)animated;
- (void)setBackButtonImage:(UIImage *)image pressedImage:(UIImage *)pressedImage;
- (void)setRightButtonWithIcon:(UIImage *)icon andPressedIcon:(UIImage *)iconPressed andTarget:(id)target andOnClickMethod:(SEL)action;
- (void)setRightButtonWithTitle:(NSString *)title andTarget:(id)target andOnClickMethod:(SEL)action;
- (void)removeBackNavigationBackEntry;
- (void)removeAllBackEntries;
- (void)setTitle:(NSString *)title;
@end
