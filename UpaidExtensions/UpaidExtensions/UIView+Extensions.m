//
//  UIView+Extensions.m
//
//  Created by Michał Majewski on 04.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UIView+Extensions.h"

@implementation UIView (Extensions)
  
- (void) setFrameHeight: (CGFloat) height {
  CGRect newFrame = self.frame;
  newFrame.size.height = height;
  self.frame = newFrame;
}

- (void) setFrameWidth: (CGFloat) width {
  CGRect newFrame = self.frame;
  newFrame.size.width = width;
  self.frame = newFrame;
}


- (void) setFrameOriginX: (CGFloat) x {
  CGRect newFrame = self.frame;
  newFrame.origin.x = x;
  self.frame = newFrame;
}


- (void) setFrameOriginY: (CGFloat) y {
  CGRect newFrame = self.frame;
  newFrame.origin.y = y;
  self.frame = newFrame;
}

  
- (void) listSubviews {
  [self listSubviews:self level:0];
}

- (void) listSubviews: (UIView *) view level: (int) level {
  level++;

  if ([view subviews].count > 0) {
    for (id subview in [view subviews]) {
      NSLog(@"subview level %d: %@ [%@]", level, [[subview class] description], NSStringFromCGRect([subview frame]));
      
      [self listSubviews:subview level:level];
    }
  }
}

- (BOOL) setupNib {
  return [self setupWithNibName:NSStringFromClass([self class])];
}

- (BOOL) setupWithNibName: (NSString *) nibName {
  if (self.subviews.count == 0) {
    UIView *subview = [[[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil] objectAtIndex:0];
    subview.frame = self.bounds;
    [self addSubview:subview];

    return YES;
  }
  
  return NO;
}

- (void) viewDidLoad {
  for (UIView *subview in [self subviews]) {
    [subview viewDidLoad];
  }
}

- (void) viewDidDisappear {
  for (UIView *subview in [self subviews]) {
    [subview viewDidDisappear];
  }
}

- (void) viewWillAppear {
  for (UIView *subview in [self subviews]) {
    [subview viewWillAppear];
  }
}

- (void) viewDidAppear {
  for (UIView *subview in [self subviews]) {
    [subview viewDidAppear];
  }
}

@end