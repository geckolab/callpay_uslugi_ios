//
//  UITableViewCell+Extensions.m
//
//  Created by Michał Majewski on 06.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UITableViewCell+Extensions.h"

@implementation UITableViewCell (Extensions)

- (UITableView *)parentTableView {
  UITableView *tableView = nil;
  UIView *view = self;
  while(view != nil) {
    if([view isKindOfClass:[UITableView class]]) {
      tableView = (UITableView *)view;
      break;
    }
    view = [view superview];
  }
  return tableView;
}


@end
