//
//  UITableViewCell+Extensions.h
//
//  Created by Michał Majewski on 06.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (Extensions)

- (UITableView *)parentTableView;

@end
