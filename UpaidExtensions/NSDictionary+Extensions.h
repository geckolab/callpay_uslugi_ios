//
//  NSDictionary+Extensions.h
//  Pods
//
//  Created by Michał Majewski on 18.04.2014.
//
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Extensions)

- (NSString *) queryString;

@end
