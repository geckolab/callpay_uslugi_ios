//
//  NSManagedObjectContext+GeckoExtensions.h
//  CallPay
//
//  Created by Jacek on 21.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (GeckoExtensions)

+ (NSString *)entityName;
+ (NSEntityDescription *) entityDescriptionInContext:(NSManagedObjectContext *)context;

/**
 *  Tworzy nowy obiekt encji. Context zazwyczaj jest trzymany w AppDelegate
 */
+ (instancetype)createEntityInContext:(NSManagedObjectContext *)context;

/**
 *  Metoda zwraca obiekt requesta, umozliwiający wyszukiwanie encji w bazie
 */
+ (NSFetchRequest *) requestInContext: (NSManagedObjectContext *)context;

/**
 *  Metoda zwraca wszystkie encje
 */
+ (NSArray *)allEntitiesInContext:(NSManagedObjectContext *)context withSortKey:(NSString *)sortKey;

/**
 *  Metoda umożliwiająca stworzenie customowego settera w obiekcie encji.
 *
 *  Przykłady:
 *
 *   - (void)setEndPrice:(NSNumber *)endPrice {
 *     [self updateValue: endPrice forKey: @"endPrice"];
 *     //...
 *   }
 *
 *   - (void) setFirstName:(NSString *)firstName {
 *     [self updateValue:firstName forKey:@"firstName"];
 *     //...
 *   }
 */
- (void) updateValue: (id) value forKey: (NSString *) key;

/**
 *  Metoda wywoływana automatycznie w metodzie createEntityInContext:
 *  Umożliwia wstępną inicjalizację pól w encji.
 *
 *  Do wykorzystania tak samo jak metoda init w zwykłych klasach
 */
- (instancetype) onCreate;

+ (NSArray *)findAllObjects;
+ (NSArray *)findAllObjectsInContext:(NSManagedObjectContext *)context;
+ (NSArray *)findAllOther:(NSManagedObject*) entity;

@end
