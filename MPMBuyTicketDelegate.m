//
//  MPMBuyTicketDelegate.m
//  CallPay
//
//  Created by Jacek on 13.03.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "MPMBuyTicketDelegate.h"

@implementation MPMBuyTicketDelegate

- (void) afterSuccessfullPaymentWithNavigationController: (UINavigationController *) navigationController andId: (int) paymentId andAdditionalData: (NSString *) resultAdditionalData {
    
    NSLog(@"success");
    
    [navigationController popToRootViewControllerAnimated: YES];
    
}



- (void) afterUnsuccessfullPaymentWithNavigationController: (UINavigationController *) navigationController {
    
    NSLog(@"fail");
    
    [navigationController popToRootViewControllerAnimated: YES];
    
}

@end
