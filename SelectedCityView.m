//
//  SelectedCityView.m
//  CallPay
//
//  Created by Jacek on 26.03.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "SelectedCityView.h"

@implementation SelectedCityView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setup];
    }
    return self;
}
- (id)init
{
    self = [super init];
    if (self)
    {
        [self setFrame:CGRectMake(0, 0, 320, 65)];
        [self setup];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self setup];
    }
    return self;
}

-(void)setup
{//[self setBackgroundColor:[UIColor greenColor]];
    UIImage *icon = [UIImage imageNamed:@"selected_city_icon"];
    iconView = [[UIImageView alloc] initWithFrame:CGRectMake((85 - icon.size.width) / 2, 0, icon.size.width, icon.size.height)];
    [iconView setImage:icon];
    [self addSubview:iconView];
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(85, ((icon.size.height / 2) - 18) + 5, self.frame.size.width - 85, 18)];
    [titleLabel setFont:[UIFont fontWithName:@"Swis721LtEU" size:15]];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [self addSubview:titleLabel];
    [titleLabel setText:@"Aktualnie wybrane miasto:"];
    
    cityLabel = [[UILabel alloc] initWithFrame:CGRectMake(85, titleLabel.frame.origin.y + titleLabel.frame.size.height, titleLabel.frame.size.width, 24)];
    [cityLabel setFont:[UIFont fontWithName:@"Swis721LtEU" size:18]];
    [cityLabel setTextColor:[UIColor whiteColor]];
    [self addSubview:cityLabel];
    [cityLabel setText:@""];//[cityLabel setBackgroundColor:[UIColor yellowColor]];
//    NSLog(@"%f",cityLabel.frame.origin.y+cityLabel.frame.size.height);
    
}
-(void)setCity:(NSString*)name
{
    [cityLabel setText:name];
}
-(void)setCar:(BOOL)car
{
    [titleLabel setText:@"Aktualnie wybrany pojazd:"];
    UIImage *icon = [UIImage imageNamed:@"icon_pojazd_duzy"];
    [iconView setFrame:CGRectMake((85 - icon.size.width) / 2, 0, icon.size.width, icon.size.height)];
    [iconView setImage:icon];
}
-(void)setCenterLabels:(BOOL)center
{
    if (center)
    {
        [titleLabel setFrame:CGRectMake(85, ((iconView.image.size.height / 2) - 18) + 5, self.frame.size.width - 85, 18)];
        [cityLabel setFrame:CGRectMake(85, titleLabel.frame.origin.y + titleLabel.frame.size.height, titleLabel.frame.size.width, 24)];
    }
    else
    {
        [titleLabel setFrame:CGRectMake(85, ((iconView.image.size.height - 18) / 2) + 5, self.frame.size.width - 85, 18)];
        [cityLabel setFrame:CGRectMake(85, titleLabel.frame.origin.y + titleLabel.frame.size.height, titleLabel.frame.size.width, 24)];
    }
}

@end

