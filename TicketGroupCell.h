//
//  TicketGroupCell.h
//  CallPay
//
//  Created by Jacek on 03.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RectangleWhiteBarsView.h"

@interface TicketGroupCell : UITableViewCell
{
    RectangleWhiteBarsView *barsView;
}

@property (nonatomic, weak) IBOutlet UILabel *groupNameLabel;

@end
