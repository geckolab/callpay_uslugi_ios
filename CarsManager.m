//
//  CarsManager.m
//  CallPay
//
//  Created by Jacek on 14.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "CarsManager.h"

//#import "Car.h"
#import "Car+Extensions.h"

@implementation CarsManager

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize managedObjectContextPrivateQueue = _managedObjectContextPrivateQueue;

@synthesize carsList = _carsList;

static CarsManager *object;

+ (CarsManager *) object {
    if (!object) {
        object = [[CarsManager alloc] init];
    }
    
    return object;
}

-(id)init
{
    self = [super init];
    if (self)
    {NSLog(@"\n\ngetDefault %@\n",[self getDefault]);
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setValue:[self getDefault] forKey:@""];
        if ([prefs objectForKey:CURRENT_CAR] != nil)
        {
            _currentCar = [prefs valueForKey:CURRENT_CAR];
        }
        else
        {
            _currentCar = @"";
        }
        _carsList = [[NSMutableArray alloc] init];
        [self reloadData];
    }
    return self;
}

-(void)addCar:(NSString*)plateNumber
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Car" inManagedObjectContext:[self managedObjectContext]];
    [fetchRequest setEntity:entityDesc];
    NSPredicate *predicatePN = [NSPredicate predicateWithFormat:@"plateNumber == %@", plateNumber];
    [fetchRequest setPredicate:predicatePN];
    
    NSError *error = nil;
    NSArray *result = [[[CarsManager object] managedObjectContext] executeFetchRequest:fetchRequest error:&error];NSLog(@"res %@",result);
    
    if ([result count] == 1)
    {
        Car *c = [result firstObject];
        if ([c.plateNumber isEqualToString:plateNumber])
        {
            [self reloadData];
            return;
        }
    }
    
    int count = [self countAll];
    Car *newCar = [NSEntityDescription insertNewObjectForEntityForName:@"Car" inManagedObjectContext:[self managedObjectContext]];
    [newCar setPlateNumber:[plateNumber uppercaseString]];
//    NSError *error;
    if (![_managedObjectContext save:&error])
    {
        NSLog(@"Blad dodawania samochodu: %@", [error localizedDescription]);
    }
    if (count == 0)
    {
        [self setCurrentCar:plateNumber];
    }
    [self reloadData];
}

-(int)countAll
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityItem = [NSEntityDescription entityForName:@"Car" inManagedObjectContext:[self managedObjectContext]];
    [fetchRequest setEntity:entityItem];
    [fetchRequest setIncludesPropertyValues:NO];
    NSError *error = nil;
    NSArray *result = [[[CarsManager object] managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    return result.count;
}

-(Car *)findWithPlateNumber:(NSString*)plateNumer
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Car" inManagedObjectContext:[self managedObjectContext]];
    [fetchRequest setEntity:entityDesc];
    NSPredicate *predicatePN = [NSPredicate predicateWithFormat:@"plateNumber == %@", plateNumer];
    [fetchRequest setPredicate:predicatePN];
    
    NSError *error = nil;
    NSArray *result = [[[CarsManager object] managedObjectContext] executeFetchRequest:fetchRequest error:&error];NSLog(@"res %@",result);
    
    if (error != nil)
    {
        NSLog(@"error = %@", error);
        return nil;
    }
    else
    {
        if ([result count] >0)
        {
            return [result firstObject];
        }
        else
        {
            return nil;
        }
    }
}

-(void)removeCarWithEntity:(Car*)entity
{
    [[self managedObjectContext] deleteObject:entity];
    NSError *saveError = nil;
    [[self managedObjectContext] save:&saveError];
    if ([self countAll] == 1)
    {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entityItem = [NSEntityDescription entityForName:@"Car" inManagedObjectContext:[self managedObjectContext]];
        [fetchRequest setEntity:entityItem];
        [fetchRequest setIncludesPropertyValues:YES];
        NSError *error = nil;
        NSArray *result = [[[CarsManager object] managedObjectContext] executeFetchRequest:fetchRequest error:&error];
        Car *c = [result firstObject];
        [self setCurrentCar:c.plateNumber];
    }
    [self reloadData];
}

-(void)setCurrentCar:(NSString *)newCar
{
//    self.currentCar = newCar;//[newCar uppercaseString];
    _currentCar = [newCar uppercaseString];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setValue:_currentCar forKey:CURRENT_CAR];
    [prefs synchronize];
    Car *c = [self findWithPlateNumber:newCar];
    if (c != nil)
    {
        [c markDefault];
    }
    [self reloadData];
}
-(NSString*)getCurrentCar
{
    NSString *defCar = [self getDefault];
    if (defCar != nil)
    {
        return defCar;
    }
    else
        return @"";
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];//NSLog(@"has changes %hhd",[managedObjectContext hasChanges]);
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Parking" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"ParkingSQ.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@ \n%@", error, [error userInfo],[self applicationDocumentsDirectory]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

-(void)reloadData
{
    NSFetchRequest *fetchRequestItems = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityItem = [NSEntityDescription entityForName:@"Car" inManagedObjectContext:[self managedObjectContext]];
    [fetchRequestItems setEntity:entityItem];
    NSError *error = nil;
    NSArray *result = [_managedObjectContext executeFetchRequest:fetchRequestItems error:&error];
    [_carsList removeAllObjects];
//    for (Car *c in result)
    for (int idx = 0; idx < [result count]; idx++)
    {
        Car *c = (Car*)[result objectAtIndex:idx];
        [_carsList addObject:@{@"number":c.plateNumber, @"idx":[NSString stringWithFormat:@"%d",idx], @"obj":c}];
    }//NSLog(@"list %@",_carsList);
    
    if (carListVCDelegate != nil && [carListVCDelegate respondsToSelector:@selector(reloadData)])
    {
        [carListVCDelegate reloadData];
    }
}

-(void)setCarListVCDelegate:(id)delegate
{
    carListVCDelegate = delegate;
}

-(NSString*)getDefault
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Car" inManagedObjectContext:[self managedObjectContext]];
    [fetchRequest setEntity:entityDesc];
//    NSPredicate *predicatePN = [NSPredicate predicateWithFormat:@"defaultCar == %@", @"1"];
//    [fetchRequest setPredicate:predicatePN];
    NSSortDescriptor *defaultSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"defaultCar" ascending:NO];
    [fetchRequest setSortDescriptors:@[defaultSortDescriptor]];
    
    NSError *error = nil;
    NSArray *result = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    if (error != nil)
    {
        NSLog(@"error = %@", error);
        return nil;
    }
    else
    {
        if ([result count] > 0)
        {
            return ((Car*)[result firstObject]).plateNumber;
        }
        return nil;
    }
}
@end
