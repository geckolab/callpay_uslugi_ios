//
//  PRRelationsCell.h
//  pr
//
//  Created by Marcin Szulc on 22.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PRRelationObject;

#define PR_RELATION_TABLE_CELL_HEIGHT 100

@interface PRRelationsCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *labelCategory;
@property (strong, nonatomic) IBOutlet UILabel *labelDepartureTime;
@property (strong, nonatomic) IBOutlet UILabel *labelArrivalTime;
@property (strong, nonatomic) IBOutlet UILabel *labelTravelTime;
@property (strong, nonatomic) IBOutlet UILabel *labelClass;
@property (strong, nonatomic) IBOutlet UISegmentedControl *switchClass;

@property (weak, nonatomic) IBOutlet PRRelationObject *relationObject;
- (IBAction)switchClassValueChanged:(id)sender;

@end
