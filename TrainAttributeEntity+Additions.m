//
//  TrainAttributeEntity+Additions.m
//  pr
//
//  Created by Marcin Szulc on 28.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "TrainAttributeEntity+Additions.h"

@implementation TrainAttributeEntity (Additions)

+ (TrainAttributeEntity*) insertTrainAttributeWithId:(NSNumber*) index andName:(NSString*) name andDescription:(NSString*) description withManagedObjectContext:(NSManagedObjectContext*) managedObjectContext
{
    TrainAttributeEntity *trainAttribute = [NSEntityDescription insertNewObjectForEntityForName:@"TrainAttributeEntity" inManagedObjectContext:managedObjectContext];
    
    trainAttribute.trainAttributeId = index;
    trainAttribute.trainAttributeName = name;
    trainAttribute.trainAttributeDescription = description;
    
    return trainAttribute;
}

+ (void) clearAllRecordsWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext
{
    NSArray *trainAttributes = [self fetchAllTrainAttributesWithManagedObjectContext:managedObjectContext];
    
    for(TrainAttributeEntity *trainAttribute in trainAttributes)
    {
        [managedObjectContext deleteObject:trainAttribute];
    }
}

+ (NSArray*) fetchAllTrainAttributesWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"TrainAttributeEntity"];
    return [managedObjectContext executeFetchRequest:request error:nil];
}

+ (TrainAttributeEntity*) fetchTrainAttributeWithId:(NSNumber*) trainAttributeId withManagedObjectContext:(NSManagedObjectContext*) managedObjectContext
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"TrainAttributeEntity"];
    [request setPredicate:[NSPredicate predicateWithFormat:@"trainAttributeId = %@", trainAttributeId]];
    
    NSArray *result = [managedObjectContext executeFetchRequest:request error:nil];
    
    if(result == nil || result.count == 0)
    {
        return nil;
    }
    return [result objectAtIndex:0];

}

@end
