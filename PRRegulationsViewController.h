//
//  PRRegulationsViewController.h
//  pr
//
//  Created by Marcin Szulc on 06.05.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRRegulationsViewController : ProjectViewController
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end
