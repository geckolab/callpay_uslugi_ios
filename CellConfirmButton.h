//
//  CellConfirmButton.h
//  CallPay
//
//  Created by Jacek on 02.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellConfirmButton : UIButton
{
    UIImage *accessoryImage;
    UIImageView *accessoryImageView;
    BOOL isActive;
}

-(void)setup;
-(void)setActive:(BOOL)active;

@end
