//
//  TrainAttributeEntity.h
//  CallPay
//
//  Created by Jacek on 15.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TrainAttributeEntity : NSManagedObject

@property (nonatomic, retain) NSString * trainAttributeDescription;
@property (nonatomic, retain) NSNumber * trainAttributeId;
@property (nonatomic, retain) NSString * trainAttributeName;

@end
