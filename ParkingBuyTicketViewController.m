//
//  ParkingBuyTicketViewController.m
//  CallPay
//
//  Created by Jacek on 20.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "ParkingBuyTicketViewController.h"

#import "UpaidMPM.h"

#import "CarsManager.h"

#import "CPWsdl.h"

#import "NSString+Extensions.h"


@interface ParkingBuyTicketViewController ()

@end

@implementation ParkingBuyTicketViewController

@synthesize numberLabel;

@synthesize ticketName;
@synthesize verificationPhone;
@synthesize verificationTime;
@synthesize productCode;
//@synthesize price;
@synthesize headerView;

@synthesize ticketNameLabel;
@synthesize ticketPriceLabel;

- (id) initWithTicket: (ParkingTicket *) __ticket {
    self = [super init];
    
    if (self) {
        self.ticket = __ticket;
        self.ticketName = self.ticket.name;
        self.price = [self.ticket.price floatValue];
        self.productCode = self.ticket.productCode;
    }
    
    return self;
}

-(void)setupWithTicket: (ParkingTicket *) __ticket
{
    self.ticket = __ticket;
    self.ticketName = self.ticket.name;
    self.price = [self.ticket.price floatValue];
    self.productCode = self.ticket.productCode;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.headerView setIcon:[UIImage imageNamed:@"new_zakup_biletu"]];
    [self.headerView setTitle:@"Zakup czasu parkowania"];
    
    [self.ticketNameLabel setText:self.ticketName];
    //    [self.ticketPriceLabel setText:self.price];
    [self setPriceLabel:self.price];
//    [self.numberLabel setText:self.ticket.productCode];
    [self.numberLabel setText:self.ticket.car.plateNumber];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)setPriceLabel:(CGFloat)priceValue
{
    NSNumberFormatter * formatter = [[NSNumberFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"pl_PL"];
    formatter.numberStyle = NSNumberFormatterCurrencyStyle;
    [formatter setLocale:locale];
    
    CGFloat pr = priceValue / 100;
    
    [self.ticketPriceLabel setText:[formatter stringFromNumber:[[NSNumber alloc] initWithFloat:pr]]];
}
-(IBAction)buyAction:(id)sender
{
    NSString *token = [[NSString stringWithFormat:@"%ld",time(NULL)] md5];
//    [[CPWsdl object] checkExternalTransfer];
//    [[CPApi sharedManager] checkExternalTransfer];//@"48123456789", @"256",@"27740", @"token"
    [[CPApi sharedManager] setDelegateCheckExternalTransfer:self];
//    [[CPApi sharedManager] checkExternalTransferWithPhone:@"48123456789" andProductCode:@"256.27740" andToken:@"token"];
    [[CPApi sharedManager] checkExternalTransferWithPhone:@"48123456789" andProductCode:self.ticket.productCode andToken:token];
    
    /*
    [UpaidMPM sharedInstance].products = [[NSArray alloc] initWithObjects:[[UpaidMPMProduct alloc] initWithName:@"product name" andShortDescription:self.ticketName andImage: [UIImage imageNamed:@"phone"] andQuantity:1 andAmount:(int)self.price], nil];
    //    [[UpaidMPM sharedInstance] setAmount:(int)self.price andItemNo:self.productCode andAdditionalData:@""];
    [[UpaidMPM sharedInstance] setAmount:(int)self.price andItemNo:self.productCode andAdditionalData:@""];
    [UpaidMPM sharedInstance].delegate = self;
    self.navigationController.navigationBar.translucent = NO;
    //    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [[UpaidMPM sharedInstance] startPaymentForNavigationController:self.navigationController];*/
}
- (void) afterSuccessfullPaymentWithNavigationController: (UINavigationController *) navigationController andId: (int) paymentId andAdditionalData: (NSString *) resultAdditionalData {
    
    NSLog(@"success");
    [self.ticket setPurchaseDate:[NSDate date]];

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:@"Czas parkowania kupiony pomyślnie." delegate:self cancelButtonTitle:@"OK!" otherButtonTitles: nil];
    [self.ticket setStatus:@(1)];
    [alert show];
    NSError *error = nil;
    [[[CarsManager object] managedObjectContext] save:&error];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBar.tintColor = [UIColor clearColor];
    [navigationController popToRootViewControllerAnimated: YES];
    
}



- (void) afterUnsuccessfullPaymentWithNavigationController: (UINavigationController *) navigationController {
    
    NSLog(@"fail");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:@"Wystąpił bład techniczny. Spróbuj ponownie później." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    self.navigationController.navigationBar.translucent = YES;
    [navigationController popToRootViewControllerAnimated: YES];
    
}

- (void) checkExternalTransferDidSuccessWithDictionary:(NSDictionary*)dictionary
{
    NSLog(@"\namount: %@\nsession-token: %@\n",[dictionary valueForKey:@"amount"],[dictionary valueForKey:@"session-token"] );
    [self setPriceLabel:[[dictionary valueForKey:@"amount"] floatValue] ];
    [self.ticket setPrice:[NSNumber numberWithFloat:[[dictionary valueForKey:@"amount"] floatValue]]];
}
- (void) checkExternalTransferDidFail
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:@"Wystąpił bład techniczny. Spróbuj ponownie później." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

@end
