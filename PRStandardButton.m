//
//  PRStandardButton.m
//  pr
//
//  Created by Marcin Szulc on 18.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PRStandardButton.h"

#import <QuartzCore/QuartzCore.h>

@interface PRStandardButton()

@property (strong, nonatomic) UIColor *originalBackgroundColor;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

@end

@implementation PRStandardButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initialize];
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        [self initialize];
    }
    
    return self;
}

- (void) initialize
{
    self.layer.cornerRadius = 8.0;
    
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    
    _originalBackgroundColor = self.backgroundColor;
    
    _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    _activityIndicator.hidden = YES;
    
    CGRect frame = _activityIndicator.frame;
    frame.origin.x = self.frame.size.width/2 - frame.size.width/2;
    frame.origin.y = self.frame.size.height/2 - frame.size.height/2;
    
    _activityIndicator.frame = frame;
    
    [self addSubview:_activityIndicator];
}

- (void)setHighlighted:(BOOL)highlighted
{
    if(highlighted)
    {
        [self setAlpha:0.8];
        //[self setBackgroundColor:[UIColor colorWithRed:0.0 green:70.0/255.0 blue:124.0/255.0 alpha:0.5]];
    }
    else
    {
        [self setAlpha:1.0];
        //[self setBackgroundColor:[UIColor colorWithRed:0.0 green:70.0/255.0 blue:124.0/255.0 alpha:1.0]];
    }
    
    [super setHighlighted:highlighted];
}

- (void)setEnabled:(BOOL)enabled
{
    [super setEnabled:enabled];
    CGFloat r = 0;
    CGFloat g = 0;
    CGFloat b = 0;
    [_originalBackgroundColor getRed:&r green:&g blue:&b alpha:nil];
    
    if(enabled == NO)
    {
        [self setBackgroundColor:[UIColor colorWithRed:r green:g blue:b alpha:0.5]];
    }
    else
    {
        [self setBackgroundColor:[UIColor colorWithRed:r green:g blue:b alpha:1.0]];
    }
    
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
}

- (void) setTitle:(NSString *)title
{
    [self setTitle:title forState:UIControlStateNormal];
    [self setTitle:title forState:UIControlStateHighlighted];
    [self setTitle:title forState:UIControlStateSelected];
    [self setTitle:title forState:UIControlStateDisabled];
}

- (void)setLockedForResponse:(BOOL)lockedForResponse
{
    if(lockedForResponse == _lockedForResponse)
    {
        return;
    }
    [self setEnabled:!lockedForResponse];
    
    _lockedForResponse = lockedForResponse;

    
    
    if(_lockedForResponse == YES)
    {
        _activityIndicator.hidden = NO;
        [_activityIndicator startAnimating];
        self.titleLabel.hidden = YES;
        [self setTitleColor:[UIColor clearColor] forState:UIControlStateDisabled];
    }
    else
    {
        _activityIndicator.hidden = YES;
        [_activityIndicator stopAnimating];
        self.titleLabel.hidden = NO;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
