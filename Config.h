//
//  Config.h
//  pr
//
//  Created by Marcin Szulc on 17.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#ifndef pr_Config_h
#define pr_Config_h

#define CONFIG_MAX_DAYS_FORWARD 30
#define CONFIG_PASSWORD_LENGTH 10

#endif
