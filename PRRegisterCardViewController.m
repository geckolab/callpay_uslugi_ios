//
//  PRRegisterCardViewController.m
//  pr
//
//  Created by Marcin Szulc on 25.03.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PRRegisterCardViewController.h"
#import "PRAsyncConnection.h"
#import "XLog.h"
#import "PRUtils.h"
#import "PRCardsTableCell.h"
#import "PRCardObject.h"
#import "PRCardsContainer.h"
#import "PREditCardViewController.h"
#import "PRAddCardViewController.h"
#import "PRCardsProvider.h"
#import "PRAlertView.h"
//#import "PRAppDelegate.h"
#import "SettingsEntity+Additions.h"

#import "PRDBManager.h"

static NSString *const LogTag = @"PRRegisterCardViewController";


@interface PRRegisterCardViewController () <UITableViewDataSource, UITableViewDelegate, PRAddCardViewControllerDelegate, PREditCardViewControllerDelegate, PRCardsProviderDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic) PRCardsContainer *cardsContainer;

@property (weak, nonatomic) IBOutlet UILabel *hintLabel;

@property (nonatomic) BOOL cardsWasFetched;

@property (nonatomic) BOOL newCardWasAdded;

@property (nonatomic) PRCardsProvider *cardsProvider;

@property (weak, nonatomic) IBOutlet UIView *viewWaiting;

@property (strong, nonatomic) PRAlertView *alert;

@end


@implementation PRRegisterCardViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

//    UIBarButtonItem * mcmLogo = [[UIBarButtonItem alloc] initWithCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mcm_icon"]]];
//    self.navigationItem.rightBarButtonItem = mcmLogo;
    
    [[self tableView] setBackgroundView:nil];

    if ([self isUsedForInitialCardSelection])
    {
        UIBarButtonItem *doneButtonItem = [[UIBarButtonItem alloc]
                                                            initWithTitle:@"Gotowe"
                                                                    style:UIBarButtonItemStyleDone
                                                                   target:self
                                                                   action:@selector(doneButtonClicked)];

        [self.navigationItem setLeftBarButtonItem:doneButtonItem animated:NO];
    }
}

- (void)doneButtonClicked
{
    [[self delegate] registerCardViewControllerShouldBeDismissed:self];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(_cardsWasFetched == NO)
    {
        [_hintLabel setText:@""];
        [_tableView reloadData];
        _viewWaiting.hidden = NO;
        [[self cardsProvider] fetchCards];
    }

}

- (void)cardsProvider:(PRCardsProvider *)cardsProvider
         fetchedCards:(PRCardsContainer *)cardsContainer
{
    _viewWaiting.hidden = YES;
    [self setCardsContainer:cardsContainer];
    [self cardsWasReplacedWithNewOne];
}

- (void)cardsProviderUnableConnectToServer:(PRCardsProvider *)cardsProvider
{
    _viewWaiting.hidden = YES;
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Brak połączenia z internetem" delegate:nil
                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
}

- (void)cardsProviderFailedToFetchCards:(PRCardsProvider *)cardsProvider
{
    _viewWaiting.hidden = YES;
    [PRUtils displayStatusErrorDialog];
}

- (void)cardsProviderFailedToFetchCards:(PRCardsProvider *)cardsProvider
                        withErrorStatus:(NSInteger)status
{
    _viewWaiting.hidden = YES;
    if (status == STATUS_USER_BLOCKED)
    {
        [PRUtils displayStandardAlertViewWithTitle:NSLocalizedString(@"alertAttention", nil)
                                           message:NSLocalizedString(@"alertUserBlocked", nil)
                             andConfirmButtonTitle:NSLocalizedString(@"alertOk", nil)];
    }
}

- (PRCardsProvider *)cardsProvider
{
    if (!_cardsProvider)
    {
        _cardsProvider = [[PRCardsProvider alloc] init];
        [_cardsProvider setDelegate:self];
    }

    return _cardsProvider;
}

- (void)cardsWasReplacedWithNewOne
{
    if (![[self cardsContainer] containsDefaultCard] && [[self cardsContainer] count] == 1)
    {
        PRCardObject *card = [[self cardsContainer] otherCardAtIndex:0];
        [card setDefaultForPurchases];

        NSString *message;

        if ([self newCardWasAdded])
        {
            message = @"Karta została dodana i ustawiona jako karta domyślna";
        }
        else
        {
            message = @"Masz zarejestrowaną jedną kartę. Karta została ustawiona jako karta domyślna";
        }

        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                            message:message
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
    
//    PRAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    SettingsEntity *settings = [SettingsEntity fetchSettingsObjectWithManagedObjectContext:[[PRDBManager object]managedObjectContext]];
    
    if(settings.userAcceptedCardsWarning.boolValue == NO)
    {
        _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertCardsScreen", nil) withParent:self callbackSelector:@selector(alertCardsSelector) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil), nil];
        [_alert show];
    }
    
    [self setCardsWasFetched:YES];
    [self updateHintLabel];
    [[self tableView] reloadData];
}

- (void) alertCardsSelector
{
//    PRAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    SettingsEntity *settings = [SettingsEntity fetchSettingsObjectWithManagedObjectContext:[[PRDBManager object]managedObjectContext]];
    settings.userAcceptedCardsWarning = [NSNumber numberWithBool:YES];
    NSError *error;
    if([[[PRDBManager object]managedObjectContext] save:&error] == NO)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        return;
    }
}

- (void)updateHintLabel
{
    if ([self cardsWasFetched])
    {
        if ([[self cardsContainer] containsDefaultCard])
        {
            [[self hintLabel]
                   setText:@"Karta domyślna to ta, z której pobierane\nbędą środki na zakup Twoich biletów."];
        }
        else if ([[self cardsContainer] count] == 0)
        {
            [[self hintLabel] setText:@"Nie masz zarejestrowanej żadnej karty. Dodaj kartę aby móc kupować bilety."];
        }
        else
        {
            [[self hintLabel] setText:@"Aby móc kupować bilety ustaw jedną z kart jako kartę domyślną"];
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return (NSUInteger)[[self cardsContainer] containsDefaultCard] +
           (NSUInteger)[[self cardsContainer] containsOtherCards];
}

- (NSString *)tableView:(UITableView *)tableView
titleForHeaderInSection:(NSInteger)section
{
    if (section == 0 && [[self cardsContainer] containsDefaultCard])
    {
        return @"Karta domyślna";
    }
    else
    {
        return @"Pozostałe karty";
    }
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    if(_cardsContainer == nil)
    {
        return 0;
    }
    
    if ([[self cardsContainer] containsDefaultCard])
    {
        if (section == 0)
        {
            return 1;
        }
        else
        {
            return [[self cardsContainer] count] - 1;
        }
    }
    else
    {
        return [[self cardsContainer] count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cardsTableCell";
    PRCardsTableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PRCardsTableCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }

    PRCardObject *card = [self cardForIndexPath:indexPath];

    [cell setCardNumber:[card number]];
    [cell setExpirationDate:[card expirationDate]];
    [cell setActive:[card isActive]];
    [cell setVerified:[card isVerified]];
    return cell;
}

- (PRCardObject *)cardForIndexPath:(NSIndexPath *)indexPath
{
    if ([[self cardsContainer] containsDefaultCard] && [indexPath indexAtPosition:0] == 0)
    {
        return [[self cardsContainer] defaultCard];
    }
    else
    {
        return [[self cardsContainer] otherCardAtIndex:[indexPath indexAtPosition:1]];
    }
}

- (CGFloat)   tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (void)      tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"editCardSegue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue
                 sender:(id)sender
{
    [super prepareForSegue:segue sender:sender];

    if ([[segue identifier] isEqualToString:@"editCardSegue"])
    {

        NSIndexPath *indexPath = [[self tableView] indexPathForSelectedRow];
        PRCardObject *card = [self cardForIndexPath:indexPath];

        PREditCardViewController *editCardViewController = (PREditCardViewController *)[segue destinationViewController];
        [editCardViewController setDelegate:self];
        [editCardViewController setCard:card];

        [[self tableView] deselectRowAtIndexPath:indexPath animated:YES];
        
        [self setCardsWasFetched:NO];
        _cardsContainer = nil;
        _cardsProvider = nil;
    }
    else if ([[segue identifier] isEqualToString:@"addCardSegue"])
    {
        [self setCardsWasFetched:NO];
        _cardsContainer = nil;
        _cardsProvider = nil;
        
        PRAddCardViewController *addCardViewController = (PRAddCardViewController *)[segue destinationViewController];
        [addCardViewController setDelegate:self];
    }
}

- (void)addCardViewControllerAddedNewCard:(PRAddCardViewController *)addCardViewController
{
    [self setNewCardWasAdded:YES];

    [self popViewControllerIfNotAlready:addCardViewController];
}

- (void)editCardViewControllerDeletedCard:(PREditCardViewController *)editCardViewController
{
    [self popViewControllerIfNotAlready:editCardViewController];

    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Karta została wyrejestrowana z systemu CallPay" delegate:nil
                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
}

- (void)popViewControllerIfNotAlready:(UIViewController *)viewController
{
    if ([[[self navigationController] visibleViewController] isEqual:viewController])
    {
        [[self navigationController] popViewControllerAnimated:YES];
    }
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [self setHintLabel:nil];
    [super viewDidUnload];
}

- (PRCardsContainer *)cardsContainer
{
    if (!_cardsContainer)
    {
        _cardsContainer = [[PRCardsContainer alloc] init];
    }
    return _cardsContainer;
}

@end
