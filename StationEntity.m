//
//  StationEntity.m
//  CallPay
//
//  Created by Jacek on 15.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "StationEntity.h"


@implementation StationEntity

@dynamic stationId;
@dynamic stationName;

@end
