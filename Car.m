//
//  Car.m
//  CallPay
//
//  Created by Jacek on 21.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "Car.h"
#import "ParkingTicket.h"


@implementation Car

@dynamic plateNumber;
@dynamic defaultCar;
@dynamic tickets;

@end
