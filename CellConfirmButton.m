//
//  CellConfirmButton.m
//  CallPay
//
//  Created by Jacek on 02.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "CellConfirmButton.h"

@implementation CellConfirmButton

- (void)drawRect:(CGRect)rect {
//    if (self.enabled)
    {
        if (!(self.highlighted || self.selected))
        {
            [self.titleLabel setTextColor:[UIColor whiteColor]];
            self.imageView.image = [self.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        }
        else
        {
            [self.titleLabel setTextColor:[UIColor colorWithRed:88/255.0 green:89/255.0 blue:91/255.0 alpha:1.0]];
            self.imageView.image = [self.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [self.imageView setTintColor:[UIColor colorWithRed:88/255.0 green:89/255.0 blue:91/255.0 alpha:1.0]];
        }
    }
//    else
    {
        
    }
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setup];
    }
    return self;
}
- (id)init
{
    self = [super init];
    if (self)
    {
        [self setFrame:CGRectMake(0, 0, 19, 19)];
        [self setup];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self setup];
    }
    return self;
}
-(void)setup
{
//    accessoryImage = [UIImage imageNamed:@"new_icon_yes"];
//    accessoryImageView = [[UIImageView alloc] initWithImage:accessoryImage];
//    [accessoryImageView setFrame:CGRectMake(0, (30 / 2) - (accessoryImage.size.height / 2), accessoryImageView.frame.size.width, accessoryImageView.frame.size.height)];
//    [self insertSubview:accessoryImageView atIndex:0];
    
//    [self setImage:[UIImage imageNamed:@"new_icon_yes"] forState:UIControlStateNormal];
//    [self setImage:[UIImage imageNamed:@"new_button_pobierz"] forState:UIControlStateNormal];
    
//    [self setImage:[UIImage imageNamed:@"new_button_usun"] forState:UIControlStateNormal];
//    [self setImage:[UIImage imageNamed:@"new_button_pobierz"] forState:UIControlStateSelected];
//    [self setImage:[UIImage imageNamed:@"new_button_pobierz_active"] forState:UIControlStateSelected | UIControlStateHighlighted];
//    [self setImage:[UIImage imageNamed:@"new_button_usun_active"] forState:UIControlStateHighlighted];
    
//    [self setImage:[UIImage imageNamed:@"btn_downloaded"] forState:UIControlStateNormal];
//    [self setImage:[UIImage imageNamed:@"btn_download"] forState:UIControlStateSelected];
//    [self setImage:[UIImage imageNamed:@"btn_download_pressed"] forState:UIControlStateSelected | UIControlStateHighlighted];
//    [self setImage:[UIImage imageNamed:@"btn_downloaded_pressed"] forState:UIControlStateHighlighted];
    
    /*
    [billDetailBtn setBackgroundImage:[UIImage imageNamed:@"btn_downloaded"] forState:UIControlStateNormal];
    [billDetailBtn setBackgroundImage:[UIImage imageNamed:@"btn_download"] forState:UIControlStateSelected];
    [billDetailBtn setBackgroundImage:[UIImage imageNamed:@"btn_download_pressed"] forState:UIControlStateSelected | UIControlStateHighlighted];
    [billDetailBtn setBackgroundImage:[UIImage imageNamed:@"btn_downloaded_pressed"] forState:UIControlStateHighlighted];
    */
    
    [self setAdjustsImageWhenHighlighted:YES];
    [self setTitle:@"" forState:UIControlStateNormal];
    
    [self setActive:NO];
}
- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
//    return CGRectMake((self.frame.size.width - self.currentImage.size.width) / 2, 0, self.currentImage.size.width, self.currentImage.size.height);
        return CGRectMake((contentRect.size.width - 30) / 2, (contentRect.size.height - 30) / 2, 30, 30);
    //    return CGRectMake(0, 0, 19, 19);
}
-(void)setActive:(BOOL)active
{
    isActive = active;
    if (active)
    {
        [self setImage:[UIImage imageNamed:@"new_button_usun"] forState:UIControlStateNormal];
        [self setImage:[UIImage imageNamed:@"new_button_usun_active"] forState:UIControlStateSelected | UIControlStateHighlighted];
    }
    else
    {
        [self setImage:[UIImage imageNamed:@"new_button_pobierz"] forState:UIControlStateNormal];
        [self setImage:[UIImage imageNamed:@"new_button_pobierz_active"] forState:UIControlStateSelected | UIControlStateHighlighted];
    }
}
-(BOOL)isActive
{
    return isActive;
}

@end
