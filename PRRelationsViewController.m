//
//  PRRelationsViewController.m
//  pr
//
//  Created by Marcin Szulc on 21.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PRRelationsViewController.h"

#import "PRRelationObject.h"
#import "PRAsyncConnection.h"
#import "PRAlertView.h"
#import "StationEntity+Additions.h"
#import "TrainAttributeEntity+Additions.h"
#import "PROrderObject.h"
#import "XLog.h"
#import "PRRelationsCell.h"
#import "PRRelationObject.h"
#import "PRRelationDetailsViewController.h"
//#import "PRAppDelegate.h"
#import "PRUtils.h"

#import "PRDBManager.h"

static NSString * const LogTag = @"RelationsViewController";

@interface PRRelationsViewController ()

@property (strong, nonatomic) PRAlertView *alert;
@property (strong, nonatomic) NSMutableArray *cells;

@end

@implementation PRRelationsViewController

@synthesize titleLabel = _titleLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIBarButtonItem * mcmLogo = [[UIBarButtonItem alloc] initWithCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mcm_icon"]]];
    self.navigationItem.rightBarButtonItem = mcmLogo;
    _cells = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTableRelations:nil];
    [self setActivityIndicator:nil];
    [super viewDidUnload];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _activityIndicator.hidden = NO;
    [_cells removeAllObjects];
    [_tableRelations reloadData];
    
    PRAsyncConnection *connection = [[PRAsyncConnection alloc] initWithParent:self successSelector:@selector(connectionSuccess:) didFailSelector:@selector(connectionFailed:)];

    if(_orderObject.relationBackToFill == YES)
    {
        self.title = NSLocalizedString(@"polaczeniePowrot", nil);
        [_titleLabel setText:NSLocalizedString(@"polaczeniePowrot", nil)];
        [connection sendGetRelationsRequestWithDate:[PRUtils getFormattedDateFromDateForReturnConnection:_orderObject] stationFromIndex:_orderObject.stationTo.stationId.intValue andStationToIndex:_orderObject.stationFrom.stationId.intValue];
    }
    else
    {
        self.title = NSLocalizedString(@"polaczenieTam", nil);
        [_titleLabel setText:NSLocalizedString(@"polaczenieTam", nil)];
        [connection sendGetRelationsRequestWithDate:[PRUtils getFormattedDateFromDateForConnection:_orderObject.orderDateFrom] stationFromIndex:_orderObject.stationFrom.stationId.intValue andStationToIndex:_orderObject.stationTo.stationId.intValue];
    }
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"relationDetails"])
    {
        
        self.title = NSLocalizedString(@"polaczenie", nil);
        [_titleLabel setText:NSLocalizedString(@"polaczenie", nil)];
        
        PRRelationDetailsViewController *destController = [segue destinationViewController];
        destController.orderObject = _orderObject;
    }
}

#pragma mark - private
- (void) buttonBackOnClick
{
}

- (void) showChooseClassAlert
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertChooseClass", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil), nil];
    [alert show];
}

- (void) fillDataWithDictionary:(NSArray*) journeys
{
    _activityIndicator.hidden = YES;
    
    [_cells removeAllObjects];
    
//    PRAppDelegate *appDelegate = (PRAppDelegate*)[UIApplication sharedApplication].delegate;
    
    for(NSDictionary *dict in journeys)
    {
        if([dict isEqual:@""])
        {
            XLogTag(LogTag, @"empty list parameter");
            continue;
        }
        
        PRRelationObject *relationObject = [[PRRelationObject alloc] init];
        
        NSDictionary *journey = [dict objectForKey:@"journey"];
        
        relationObject.relationId = [journey objectForKey:@"id"];
        relationObject.relationDepartureTime = [journey objectForKey:@"departure_time"];
        relationObject.relationArrivalTime = [journey objectForKey:@"destination_time"];
        relationObject.relationCategory = [journey objectForKey:@"category"];
        
        NSArray *train_attributes = [journey objectForKey:@"train_attributes"];
        if(train_attributes !=nil && train_attributes.count>0)
        {
            relationObject.relationAttributes = [[NSMutableArray alloc] init];
            
            for(NSDictionary *train_attribute in train_attributes)
            {
                
                if([train_attribute isEqual:@""])
                {
                    XLogTag(LogTag, @"empty list parameter");
                    continue;
                }
                
                NSNumber *trainAttribute = [[train_attribute objectForKey:@"train_attribute"] objectForKey:@"id"];
                
                PRRelationAttributeObject *attributeObject = [[PRRelationAttributeObject alloc] init];
                attributeObject.attributeId = trainAttribute;
                
                switch (attributeObject.attributeId.intValue) {
                    case ATTRIBUTE_ID_CLASS_1_AND_2:
                    {
                        relationObject.relationSelectedClass = nil;
                    }
                        break;
                    case ATTRIBUTE_ID_CLASS_1_ONLY:
                    {
                        relationObject.relationSelectedClass = [NSNumber numberWithInt:1];
                    }
                        break;
                    case ATTRIBUTE_ID_CLASS_2_ONLY:
                    {
                        relationObject.relationSelectedClass = [NSNumber numberWithInt:2];
                    }
                        break;
                };
                
                TrainAttributeEntity *ta = [TrainAttributeEntity fetchTrainAttributeWithId:attributeObject.attributeId withManagedObjectContext:[[PRDBManager object]managedObjectContext]];
                
                if(ta == nil)
                {
                    XLogTag(LogTag, @"attribute not found: %d", attributeObject.attributeId.intValue);
                    continue;
                }
                
                attributeObject.attributeDescription = ta.trainAttributeDescription;
                attributeObject.attributeName = ta.trainAttributeName;
                
                [relationObject.relationAttributes addObject:attributeObject];
            }
            
        }
        
        [_cells addObject:relationObject];
    }
    
    [_tableRelations reloadData];
}

#pragma mark - tableview delegate and datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return _cells.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = [NSString stringWithFormat:@"relation%d", indexPath.row];
    
    PRRelationsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PRRelationsCell" owner:self options:nil];
        for (id oneObject in nib)
        {
            if ([oneObject isKindOfClass:[PRRelationsCell class]])
            {
                cell = (PRRelationsCell*)oneObject;
            }
        }
    }
    
    cell.relationObject = [_cells objectAtIndex:indexPath.row];
    
    return cell;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return PR_RELATION_TABLE_CELL_HEIGHT;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PRRelationObject *relation = [_cells objectAtIndex:indexPath.row];
    
    if(relation.relationSelectedClass == nil)
    {
        [self showChooseClassAlert];
        [_tableRelations deselectRowAtIndexPath:indexPath animated:YES];
        return;
    }
    
    if(_orderObject.relationBackToFill == YES)
    {
        _orderObject.relationBack = relation;
        _orderObject.relationBackToFill = NO;
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else
    {
        _orderObject.relationBackToFill = NO;
        _orderObject.relationBack = nil;
        _orderObject.relationFrom = relation;
        [self performSegueWithIdentifier:@"relationDetails" sender:self];
    }
}

#pragma mark - alert
- (void) connectionFailed
{
    _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertStatusError", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
    [_alert show];
}

- (void) connectionFailedAlertCallback:(NSNumber*) buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - connections
- (void) connectionSuccess:(PRAsyncConnection*) connection
{
    XLogTag(LogTag, @"%@", [[NSString alloc] initWithData:connection.responseData encoding:NSUTF8StringEncoding]);
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:connection.responseData
                          options:kNilOptions
                          error:&error];
    
    if(error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        [self connectionFailed];
        return;
    }

    NSDictionary *response = [json objectForKey:@"journeys_response"];
    
    NSNumber *status = [response objectForKey:@"status"];
    
    if(status==nil)
    {
        [self connectionFailed];
        return;
    }
    
    switch (status.intValue) {
        case STATUS_OK:
        {
            NSArray *journeys = [response objectForKey:@"journeys"];
            
            if(journeys.count == 0)
            {
                _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertNoJourneys", nil) withParent:self callbackSelector:@selector(alertNoJouneys) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
                [_alert show];
            }
            else
            {
                [self fillDataWithDictionary:journeys];
            }
        }
            break;
        case STATUS_USER_BLOCKED:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertUserBlocked", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
        case STATUS_NO_STATIONS:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertNoStation", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
        case STATUS_NO_RELATION_BETWEEN_STATIONS:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertNoRelation", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
            
        case STATUS_WRONG_DATE:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertWrongDate", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
            
    }
    
}

- (void) alertNoJouneys
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) connectionFailed:(PRAsyncConnection*) connection
{
    [self connectionFailed];
}

@end
