//
//  PRVerifyCardPinViewController.h
//  pr
//
//  Created by Marcin Szulc on 24.09.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PRCardObject.h"

@interface PRVerifyCardPinViewController : UIViewController

@property (strong, nonatomic) PRCardObject *card;

@end
