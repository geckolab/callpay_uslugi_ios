//
//  PRStandardControl.m
//  pr
//
//  Created by Marcin Szulc on 18.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PRStandardControl.h"

#import <QuartzCore/QuartzCore.h>


@implementation PRStandardControl

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initialize];
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
//    self.layer.cornerRadius = 8.0;
//    self.layer.borderWidth = 1.0;
//    self.layer.borderColor = [UIColor colorWithWhite:0.66 alpha:0.6].CGColor;
    self.layer.cornerRadius = 8.0;
    self.layer.borderWidth = 2.0;
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    [self setBackgroundColor:[UIColor clearColor]];
}

- (void)setHighlighted:(BOOL)highlighted
{
    if(highlighted)
    {
//        [self setBackgroundColor:[UIColor colorWithRed:0.0 green:70.0/255.0 blue:124.0/255.0 alpha:0.5]];
        [self setBackgroundColor:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.48]];
    }
    else
    {
//        [self setBackgroundColor:[UIColor whiteColor]];
        [self setBackgroundColor:[UIColor clearColor]];
    }
    
    [super setHighlighted:highlighted];
}

- (void)setEnabled:(BOOL)enabled
{
    
    if(enabled)
    {
        self.alpha = 1.0;
    }
    else
    {
        self.alpha = 0.5;
    }
    
    [super setEnabled:enabled];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
