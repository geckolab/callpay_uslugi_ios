//
//  PRRelationDetailsViewController.h
//  pr
//
//  Created by Marcin Szulc on 22.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PRDatePickerViewController.h"

@class PROrderObject;
@class PRRelationBackDateViewController;
@class PRStandardControl;

@interface PRRelationDetailsViewController : ProjectViewController <UITableViewDataSource, UITableViewDelegate, PRDatePickerProtocol>

@property (strong, nonatomic) PROrderObject *orderObject;

@property (strong, nonatomic) IBOutlet UILabel *labelCategory;
@property (strong, nonatomic) IBOutlet UILabel *labelDepartureTime;
@property (strong, nonatomic) IBOutlet UILabel *labelArrivalTime;
@property (strong, nonatomic) IBOutlet UILabel *labelTravelDate;
@property (strong, nonatomic) IBOutlet UILabel *labelSelectedClass;

@property (strong, nonatomic) IBOutlet UILabel *labelBackCategory;
@property (strong, nonatomic) IBOutlet UILabel *labelBackDepartureTime;
@property (strong, nonatomic) IBOutlet UILabel *labelBackArrivalTime;
@property (strong, nonatomic) IBOutlet UILabel *labelBackTravelDate;
@property (strong, nonatomic) IBOutlet UILabel *labelBackSelectedClass;

@property (strong, nonatomic) IBOutlet UITableView *tableTrainAttributes;
@property (strong, nonatomic) IBOutlet UITableView *tableBackTrainAttributes;

@property (strong, nonatomic) IBOutlet PRRelationBackDateViewController *viewRelationBack;
@property (strong, nonatomic) IBOutlet PRStandardControl *controlRelationBackDetails;
@property (strong, nonatomic) IBOutlet PRDatePickerViewController *datePicker;

@property (strong, nonatomic) IBOutlet UIButton *buttonDeleteBackRelation;


- (IBAction)buttonBuyTicketOnClick:(id)sender;
- (IBAction)buttonDeleteBackRelationOnClick:(id)sender;

@end
