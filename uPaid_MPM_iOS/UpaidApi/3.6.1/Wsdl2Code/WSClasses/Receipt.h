//------------------------------------------------------------------------------
// <wsdl2code-generated>
// This code was generated by http://www.wsdl2code.com iPhone version 2.0
// Date Of Creation: 3/11/2014 4:21:42 PM
//
//  Please dont change this code, regeneration will override your changes
//</wsdl2code-generated>
//
//------------------------------------------------------------------------------
//
//This source code was auto-generated by Wsdl2Code Version
//

#import <Foundation/Foundation.h>
#import "Photo.h"


@interface Receipt : NSObject
{
}
@property (nonatomic, copy) NSString *iId;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *amount;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *created;
@property (nonatomic, strong) NSMutableArray *photos;

- (NSString *)toString:(BOOL)addNameWrap;
- (id)initWithArray:(NSArray *)array;
- (void)encodeWithCoder:(NSCoder *)encoder;
- (id)copyWithZone:(NSZone *)zone;
- (id)initWithCoder:(NSCoder *)decoder;
@end
