//------------------------------------------------------------------------------
// <wsdl2code-generated>
// This code was generated by http://www.wsdl2code.com iPhone version 2.0
// Date Of Creation: 3/11/2014 4:21:42 PM
//
//  Please dont change this code, regeneration will override your changes
//</wsdl2code-generated>
//
//------------------------------------------------------------------------------
//
//This source code was auto-generated by Wsdl2Code Version
//

#import "transactionData.h"


@implementation transactionData

- (id)initWithArray:(NSArray *)array {
	self = [super init];
	if (self) {
		@try {
			for (int i0 = 0; i0 < [array count]; i0++) {
				if (([[array objectAtIndex:i0] objectForKey:@"nodeContent"] != nil) &&  ([[[array objectAtIndex:i0]objectForKey:@"nodeName"]caseInsensitiveCompare:@"amount"] == NSOrderedSame)) {
					NSString *nodeContentValue = [[NSString alloc] initWithString:[[array objectAtIndex:i0] objectForKey:@"nodeContent"]];
					if (nodeContentValue != nil)
						[self setAmount:nodeContentValue];
				}
				else if (([[array objectAtIndex:i0] objectForKey:@"nodeContent"] != nil) &&  ([[[array objectAtIndex:i0]objectForKey:@"nodeName"]caseInsensitiveCompare:@"transaction_id"] == NSOrderedSame)) {
					NSString *nodeContentValue = [[NSString alloc] initWithString:[[array objectAtIndex:i0] objectForKey:@"nodeContent"]];
					if (nodeContentValue != nil)
						[self setTransaction_id:nodeContentValue];
				}
				else if (([[array objectAtIndex:i0] objectForKey:@"nodeContent"] != nil) &&  ([[[array objectAtIndex:i0]objectForKey:@"nodeName"]caseInsensitiveCompare:@"card"] == NSOrderedSame)) {
					NSString *nodeContentValue = [[NSString alloc] initWithString:[[array objectAtIndex:i0] objectForKey:@"nodeContent"]];
					if (nodeContentValue != nil)
						[self setCard:nodeContentValue];
				}
				else if (([[array objectAtIndex:i0] objectForKey:@"nodeContent"] != nil) &&  ([[[array objectAtIndex:i0]objectForKey:@"nodeName"]caseInsensitiveCompare:@"cvc2"] == NSOrderedSame)) {
					NSString *nodeContentValue = [[NSString alloc] initWithString:[[array objectAtIndex:i0] objectForKey:@"nodeContent"]];
					if (nodeContentValue != nil)
						[self setCvc2:nodeContentValue];
				}
			}
		}
		@catch (NSException *ex)
		{
		}
	}
	return self;
}

- (NSString *)toString:(BOOL)addNameWrap {
	NSMutableString *nsString = [NSMutableString string];
	if (addNameWrap == YES)
		[nsString appendString:@"<transactionData>"];
	if (self.amount != nil) {
		[nsString appendFormat:@"<amount>%@</amount>", [self amount]];
	}
	if (self.transaction_id != nil) {
		[nsString appendFormat:@"<transaction_id>%@</transaction_id>", [self transaction_id]];
	}
	if (self.card != nil) {
		[nsString appendFormat:@"<card>%@</card>", [self card]];
	}
	if (self.cvc2 != nil) {
		[nsString appendFormat:@"<cvc2>%@</cvc2>", [self cvc2]];
	}
	if (addNameWrap == YES)
		[nsString appendString:@"</transactionData>"];
	return nsString;
}

#pragma mark - NSCoding
- (id)initWithCoder:(NSCoder *)decoder {
	self = [super init];
	if (self) {
		self.amount = [decoder decodeObjectForKey:@"amount"];
		self.transaction_id = [decoder decodeObjectForKey:@"transaction_id"];
		self.card = [decoder decodeObjectForKey:@"card"];
		self.cvc2 = [decoder decodeObjectForKey:@"cvc2"];
	}
	return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
	[encoder encodeObject:self.amount forKey:@"amount"];
	[encoder encodeObject:self.transaction_id forKey:@"transaction_id"];
	[encoder encodeObject:self.card forKey:@"card"];
	[encoder encodeObject:self.cvc2 forKey:@"cvc2"];
}

- (id)copyWithZone:(NSZone *)zone {
	transactionData *finalCopy = [[[self class] allocWithZone:zone] init];

	NSString *copy1 = [self.amount copy];
	finalCopy.amount = copy1;

	NSString *copy2 = [self.transaction_id copy];
	finalCopy.transaction_id = copy2;

	NSString *copy3 = [self.card copy];
	finalCopy.card = copy3;

	NSString *copy4 = [self.cvc2 copy];
	finalCopy.cvc2 = copy4;

	return finalCopy;
}

@end
