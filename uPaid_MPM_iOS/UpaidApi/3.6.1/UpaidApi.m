//
//  UpaidApi.m
//  mPotwor
//
//  Created by Michał Majewski on 27.12.2013.
//  Copyright (c) 2013 Michał Majewski. All rights reserved.
//

#import "UpaidApi.h"
#import "NSString+urlencode.h"
#import "UpaidApiConstans.h"
#import "SSKeychain.h"

@interface UpaidApi ()

@property int partnerId;
@property NSString *appName;
@property BOOL testEnv;
@property BOOL logsEnabled;
@property NSString *platform;
@property NSMutableDictionary *pointersToTasks;
@property NSString *deviceToken;
@property NSString *sessionToken;
@property BOOL smsDisabled;

@end


static int pointersCount = 0;

@implementation UpaidApi

#pragma mark singleton init

+ (UpaidApi *)sharedInstance {
	static UpaidApi *sharedInstance = nil;
	static dispatch_once_t onceToken;

	dispatch_once(&onceToken, ^{
	    sharedInstance = [[UpaidApi alloc] init];
	});

	return sharedInstance;
}

- (id)init {
	self = [super init];

	if (self) {
		[self setTestEnv:NO];
		[self setLogsEnabled:NO];
		[self setPlatform:@"mobile"];
		[self setPointersToTasks:[[NSMutableDictionary alloc] init]];
		[self setSmsDisabled:NO];
	}

	return self;
}

#pragma mark public methods

+ (void)setPartnerId:(int)partnerId andAppName:(NSString *)appName {
	NSString *trimmedAppName = [appName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

	if ([trimmedAppName isEqualToString:@""]) {
		NSLog(@"UpaidApi.appName is incorrect");
		abort();
	}

	[[UpaidApi sharedInstance] setAppName:appName];
	[[UpaidApi sharedInstance] setPartnerId:partnerId];
}

+ (void)setTestPartnerId:(int)testPartnerId {
	[[UpaidApi sharedInstance] setPartnerId:testPartnerId];
	[[UpaidApi sharedInstance] setTestEnv:YES];
}

+ (void)setDeviceToken:(NSData *)deviceToken {
	NSString *deviceTokenString = @"";

	if (deviceToken != nil) {
		deviceTokenString = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
	}

	[[UpaidApi sharedInstance] setDeviceToken:deviceTokenString];
}

+ (NSString *)smsCodeForPhone:(NSString *)phone {
	if (![UpaidApi phoneLoggedBefore:phone]) {
		return @"";
	}

	return [SSKeychain passwordForService:[UpaidApi sskeychainService] account:phone];
}

+ (NSString *)testSmsCodeForPhone:(NSString *)phone {
	if ([UpaidApi isTestEnv]) {
		NSString *url = [NSString stringWithFormat:@"%@/%@/%@", UPAID_TEST_URL_USER_MOBILE_PASS, [UpaidApi partnerId], phone];

		return [NSString stringWithContentsOfURL:[NSURL URLWithString:url] encoding:NSUTF8StringEncoding error:nil];
	}

	return @"";
}

+ (BOOL)phoneLoggedBefore:(NSString *)phone {
	NSString *smsCode = [SSKeychain passwordForService:[UpaidApi sskeychainService] account:phone];

	return !(smsCode == nil || [smsCode isEqualToString:@""]);
}

+ (void)logout {
	[SSKeychain deletePasswordForService:[UpaidApi sskeychainService] account:[UpaidApi lastUsedPhone]];
	[[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"UpaidApi.lastUsedPhone"];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)lastUsedPhone {
	return [[NSUserDefaults standardUserDefaults] stringForKey:@"UpaidApi.lastUsedPhone"];
}

#pragma mark write methods

+ (void)enableLogging {
	[[UpaidApi sharedInstance] setLogsEnabled:YES];
}

+ (void)setLastUsedPhone:(NSString *)phone {
	[[NSUserDefaults standardUserDefaults] setObject:phone forKey:@"UpaidApi.lastUsedPhone"];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setPlatform:(NSString *)platform {
	[[UpaidApi sharedInstance] setPlatform:platform];
}

+ (void)setSessionToken:(NSString *)sessionToken {
	[[UpaidApi sharedInstance] setSessionToken:sessionToken];
}

+ (void)setSmsCode:(NSString *)smsCode forPhone:(NSString *)phone {
	[SSKeychain setPassword:smsCode forService:[UpaidApi sskeychainService] account:phone];
}

+ (void)disableSmsOnTestEnv {
	[[UpaidApi sharedInstance] setSmsDisabled:YES];
}

#pragma mark read methods

+ (NSString *)sskeychainService {
	return [NSString stringWithFormat:@"%@%@", [[UpaidApi sharedInstance] appName], [UpaidApi partnerId]];
}

+ (NSString *)deviceToken {
	return [[UpaidApi sharedInstance] deviceToken];
}

+ (NSString *)sessionToken {
	return [[UpaidApi sharedInstance] sessionToken];
}

+ (NSString *)partnerId {
	return [NSString stringWithFormat:@"%i", [[UpaidApi sharedInstance] partnerId]];
}

+ (BOOL)isTestEnv {
	return [[UpaidApi sharedInstance] testEnv];
}

+ (BOOL)logsEnabled {
	return [[UpaidApi sharedInstance] logsEnabled];
}

+ (NSString *)wsdlUrl {
	//[prefix]/[platform]/[version]/[os]/[application]/[applicationVersion]?wsdl
	return [NSString stringWithFormat:@"%@/%@/v%@/os:ios/application:%@/version:%@/%@?wsdl",
	        [self wsdlPrefix],
	        [[UpaidApi sharedInstance] platform],
	        UPAID_SOAP_VERSION,
	        [[[UpaidApi sharedInstance] appName] urlencode],
	        [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"],
	        [UpaidApi disableSmsWsdlUrlPart]
	];
}

+ (NSString *)disableSmsWsdlUrlPart {
	return [[UpaidApi sharedInstance] smsDisabled] && [UpaidApi isTestEnv] ? @"nosms:1" : @"";
}

+ (NSString *)wsdlPrefix {
	return [self isTestEnv] ? UPAID_SOAP_URL_TEST_PREFIX : UPAID_SOAP_URL_PREFIX;
}

#pragma mark pointers to tasks
//metody potrzebne do poprawnego zarządzania pamięcią w przypadku tasków
//bez uzycia tych metod nie byłoby możliwe wywołanie [[[task alloc] init] execute],
//ponieważ przed odebraniem response, task byłby już po deallocu

//zwrócenie unikalnego "id" taska, przypisanie taska do tablicy wskaźników do tasków
+ (NSString *)pointerIdToTask:(UpaidApiTask *)task {
	NSString *pointerId = [NSString stringWithFormat:@"%i", pointersCount];
	[[[UpaidApi sharedInstance] pointersToTasks] setObject:task forKey:pointerId];

	if ([[[UpaidApi sharedInstance] pointersToTasks] count] > 5) {
		UpaidLog(@"UPAID API pointers warning");
	}

	pointersCount++;

	return pointerId;
}

//usunięcie obiektu z tablicy wskaźników do tasków
+ (void)releasePointerToTaskId:(NSString *)taskId {
	[[[UpaidApi sharedInstance] pointersToTasks] removeObjectForKey:taskId];
}

@end
