//
//  UpaidApi.h
//  mPotwor
//
//  Created by Michał Majewski on 27.12.2013.
//  Copyright (c) 2013 Michał Majewski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpaidApi : NSObject

+ (void)setPartnerId:(int)partnerId andAppName:(NSString *)appName;
+ (void)setTestPartnerId:(int)testPartnerId;
+ (void)setDeviceToken:(NSData *)deviceToken;
+ (BOOL)phoneLoggedBefore:(NSString *)phone;
+ (void)logout;
+ (NSString *)lastUsedPhone;
+ (NSString *)smsCodeForPhone:(NSString *)phone;
+ (void)setLastUsedPhone:(NSString *)phone;
+ (BOOL)isTestEnv;
+ (NSString *)partnerId;
//available only on test environment!
+ (NSString *)testSmsCodeForPhone:(NSString *)phone;

@end
