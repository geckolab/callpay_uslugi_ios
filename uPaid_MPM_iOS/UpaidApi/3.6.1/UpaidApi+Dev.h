//
//  UpaidApi+Dev.h
//  mPotwor
//
//  Created by Michał Majewski on 27.12.2013.
//  Copyright (c) 2013 Michał Majewski. All rights reserved.
//

#import "UpaidApi.h"
#import "UpaidApiTask.h"

@interface UpaidApi (Dev)

+ (UpaidApi *)sharedInstance;

+ (BOOL)logsEnabled;
+ (NSString *)wsdlUrl;
+ (NSString *)deviceToken;
+ (NSString *)sessionToken;

+ (void)setPlatform:(NSString *)platform;
+ (void)setSessionToken:(NSString *)sessionToken;
+ (void)setSmsCode:(NSString *)smsCode forPhone:(NSString *)phone;
+ (void)enableLogging;
+ (void)disableSmsOnTestEnv;


+ (NSString *)pointerIdToTask:(UpaidApiTask *)task;
+ (void)releasePointerToTaskId:(NSString *)taskId;

@end
