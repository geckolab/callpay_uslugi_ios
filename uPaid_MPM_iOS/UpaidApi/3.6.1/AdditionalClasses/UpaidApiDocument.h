//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiPhoto.h"
#import "UpaidApiPhotoArray.h"

    
@interface UpaidApiDocument : NSObject

@property int iid; 
@property NSString *userId; 
@property NSString *name; 
@property NSString *type; 
@property NSString *barcode; 
@property UpaidApiPhotoArray *photos; 


//only required params
- (id) initWithIid: (int ) iid andUserId: (NSString *) userId andName: (NSString *) name andType: (NSString *) type andBarcode: (NSString *) barcode andPhotos: (UpaidApiPhotoArray *) photos;


@end