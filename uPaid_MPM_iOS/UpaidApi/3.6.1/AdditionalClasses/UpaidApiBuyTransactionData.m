//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiBuyTransactionData.h"
#import "NSObject+WSClass.h"
#import "buyData.h"

@implementation UpaidApiBuyTransactionData
- (id) initWithAmount: (int ) amount andItemId: (NSString *) itemId andMerchant: (NSString *) merchant andData: (NSString *) data andCard: (NSString *) card andCvc2: (NSString *) cvc2 {
  self = [super init];
  
  if (self) {
    self.amount = amount;
    self.itemId = itemId;
    self.merchant = merchant;
    self.data = data;
    self.card = card;
    self.cvc2 = cvc2;
  }
  
  return self;
}

- (id) initWithAmount: (int ) amount andItemId: (NSString *) itemId andMerchant: (NSString *) merchant andCvc2: (NSString *) cvc2{
  self = [super init];
  
  if (self) {
    self.amount = amount;
    self.itemId = itemId;
    self.merchant = merchant;
    self.data = @"";
    self.card = @"";
    self.cvc2 = cvc2;
  }
  
  return self;
}


- (NSString *) description {
    return [NSString stringWithFormat: @"amount=%@\nitemId=%@\nmerchant=%@\ndata=%@\ncard=%@\ncvc2=%@", [NSString stringWithFormat: @"%d", self.amount], self.itemId, self.merchant, self.data, self.card, self.cvc2];
}

- (id) wsClass {
    buyData *wsClass = [[buyData alloc] init];
    wsClass.amount = [NSString stringWithFormat: @"%d", self.amount];
    wsClass.item_id = self.itemId;
    wsClass.merchant = self.merchant;
    wsClass.data = self.data;
    wsClass.card = self.card;
    wsClass.cvc2 = self.cvc2;

    return wsClass;
}

- (id) initWithWsClass: (id) wsObject {
    self = [super init];
    
    if (self) {
        buyData *wsClass = (buyData *) wsObject;
        self.amount = [wsClass.amount intValue];
        self.itemId = wsClass.item_id;
        self.merchant = wsClass.merchant;
        self.data = wsClass.data;
        self.card = wsClass.card;
        self.cvc2 = wsClass.cvc2;
    }
    
    return self;
}



@end