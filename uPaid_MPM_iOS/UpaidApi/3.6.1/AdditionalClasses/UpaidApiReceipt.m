//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiReceipt.h"
#import "NSObject+WSClass.h"
#import "Receipt.h"

@implementation UpaidApiReceipt
- (id) initWithIid: (int ) iid andUserId: (NSString *) userId andAmount: (int ) amount andName: (NSString *) name andCreated: (NSString *) created andPhotos: (UpaidApiPhotoArray *) photos {
  self = [super init];
  
  if (self) {
    self.iid = iid;
    self.userId = userId;
    self.amount = amount;
    self.name = name;
    self.created = created;
    self.photos = photos;
  }
  
  return self;
}



- (NSString *) description {
    return [NSString stringWithFormat: @"iid=%@\nuserId=%@\namount=%@\nname=%@\ncreated=%@\nphotos=%@", [NSString stringWithFormat: @"%d", self.iid], self.userId, [NSString stringWithFormat: @"%d", self.amount], self.name, self.created, self.photos];
}

- (id) wsClass {
    Receipt *wsClass = [[Receipt alloc] init];
    wsClass.iId = [NSString stringWithFormat: @"%d", self.iid];
    wsClass.user_id = self.userId;
    wsClass.amount = [NSString stringWithFormat: @"%d", self.amount];
    wsClass.name = self.name;
    wsClass.created = self.created;
    wsClass.photos = (NSMutableArray *)[self.photos wsClass];

    return wsClass;
}

- (id) initWithWsClass: (id) wsObject {
    self = [super init];
    
    if (self) {
        Receipt *wsClass = (Receipt *) wsObject;
        self.iid = [wsClass.iId intValue];
        self.userId = wsClass.user_id;
        self.amount = [wsClass.amount intValue];
        self.name = wsClass.name;
        self.created = wsClass.created;
        self.photos = [[UpaidApiPhotoArray alloc] initWithWsClass: wsClass.photos];
    }
    
    return self;
}



@end