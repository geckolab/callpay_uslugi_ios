//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

    
@interface UpaidApiShoppingCartItem : NSObject

@property NSString *dDescription;
@property int quantity; 
@property int amount; 
@property NSString *image_url; 


//only required params
- (id) initWithDescription: (NSString *) description andQuantity: (int ) quantity andAmount: (int ) amount andImage_url: (NSString *) image_url;


@end