//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

    
@interface UpaidApiCard : NSObject

@property NSString *iid; 
@property NSString *pan; 
@property BOOL ddefault; 
@property NSString *authType; 
@property NSString *expDate; 
@property BOOL authorized; 
@property BOOL used; 
@property BOOL freezed; 
@property NSString *nickname; 
@property NSString *cardType; 
@property NSString *smName; 
@property BOOL locked; 
@property NSString *brand; 
@property NSString *frontImg; 
@property NSString *backImg; 
@property NSString *country; 


//only required params
- (id) initWithIid: (NSString *) iid andPan: (NSString *) pan andDdefault: (BOOL ) ddefault andAuthType: (NSString *) authType andExpDate: (NSString *) expDate andAuthorized: (BOOL ) authorized andUsed: (BOOL ) used andFreezed: (BOOL ) freezed andNickname: (NSString *) nickname andCardType: (NSString *) cardType andSmName: (NSString *) smName andLocked: (BOOL ) locked andBrand: (NSString *) brand andFrontImg: (NSString *) frontImg andBackImg: (NSString *) backImg andCountry: (NSString *) country;


@end