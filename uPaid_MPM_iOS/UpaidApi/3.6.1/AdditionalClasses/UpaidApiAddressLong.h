//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

    
@interface UpaidApiAddressLong : NSObject

@property int iid; 
@property NSString *street; 
@property NSString *number; 
@property NSString *apartment; 
@property NSString *city; 
@property NSString *zipcode; 
@property NSString *countryCode; 
@property NSString *countrySubdivision; 


//only required params
- (id) initWithIid: (int ) iid andStreet: (NSString *) street andNumber: (NSString *) number andApartment: (NSString *) apartment andCity: (NSString *) city andZipcode: (NSString *) zipcode andCountryCode: (NSString *) countryCode andCountrySubdivision: (NSString *) countrySubdivision;


@end