//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

    
@interface UpaidApiBuyTransactionData : NSObject

@property int amount; 
@property NSString *itemId; 
@property NSString *merchant; 
@property NSString *data; //optional
@property NSString *card; //optional
@property NSString *cvc2; 


//only required params
- (id) initWithAmount: (int ) amount andItemId: (NSString *) itemId andMerchant: (NSString *) merchant andCvc2: (NSString *) cvc2;
//required + optional params
- (id) initWithAmount: (int ) amount andItemId: (NSString *) itemId andMerchant: (NSString *) merchant andData: (NSString *) data andCard: (NSString *) card andCvc2: (NSString *) cvc2;


@end