//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiShoppingCartItem.h"
#import "NSObject+WSClass.h"
#import "singleShoppingCartItem.h"

@implementation UpaidApiShoppingCartItem
- (id) initWithDescription: (NSString *) description andQuantity: (int ) quantity andAmount: (int ) amount andImage_url: (NSString *) image_url {
  self = [super init];
  
  if (self) {
    self.dDescription = description;
    self.quantity = quantity;
    self.amount = amount;
    self.image_url = image_url;
  }
  
  return self;
}



- (NSString *) description {
    return [NSString stringWithFormat: @"description=%@\nquantity=%@\namount=%@\nimage_url=%@", self.dDescription, [NSString stringWithFormat: @"%d", self.quantity], [NSString stringWithFormat: @"%d", self.amount], self.image_url];
}

- (id) wsClass {
    singleShoppingCartItem *wsClass = [[singleShoppingCartItem alloc] init];
    wsClass.dDescription = self.dDescription;
    wsClass.quantity = [NSString stringWithFormat: @"%d", self.quantity];
    wsClass.amount = [NSString stringWithFormat: @"%d", self.amount];
    wsClass.image_url = self.image_url;

    return wsClass;
}

- (id) initWithWsClass: (id) wsObject {
    self = [super init];
    
    if (self) {
        singleShoppingCartItem *wsClass = (singleShoppingCartItem *) wsObject;
        self.dDescription = wsClass.dDescription;
        self.quantity = [wsClass.quantity intValue];
        self.amount = [wsClass.amount intValue];
        self.image_url = wsClass.image_url;
    }
    
    return self;
}



@end