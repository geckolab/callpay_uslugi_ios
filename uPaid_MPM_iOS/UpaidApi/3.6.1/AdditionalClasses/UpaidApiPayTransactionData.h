//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

    
@interface UpaidApiPayTransactionData : NSObject

@property int amount; 
@property NSString *transactionId; 
@property NSString *card; 
@property NSString *cvc2; 


//only required params
- (id) initWithAmount: (int ) amount andTransactionId: (NSString *) transactionId andCard: (NSString *) card andCvc2: (NSString *) cvc2;


@end