//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiPayTransactionData.h"
#import "NSObject+WSClass.h"
#import "transactionData.h"

@implementation UpaidApiPayTransactionData
- (id) initWithAmount: (int ) amount andTransactionId: (NSString *) transactionId andCard: (NSString *) card andCvc2: (NSString *) cvc2 {
  self = [super init];
  
  if (self) {
    self.amount = amount;
    self.transactionId = transactionId;
    self.card = card;
    self.cvc2 = cvc2;
  }
  
  return self;
}



- (NSString *) description {
    return [NSString stringWithFormat: @"amount=%@\ntransactionId=%@\ncard=%@\ncvc2=%@", [NSString stringWithFormat: @"%d", self.amount], self.transactionId, self.card, self.cvc2];
}

- (id) wsClass {
    transactionData *wsClass = [[transactionData alloc] init];
    wsClass.amount = [NSString stringWithFormat: @"%d", self.amount];
    wsClass.transaction_id = self.transactionId;
    wsClass.card = self.card;
    wsClass.cvc2 = self.cvc2;

    return wsClass;
}

- (id) initWithWsClass: (id) wsObject {
    self = [super init];
    
    if (self) {
        transactionData *wsClass = (transactionData *) wsObject;
        self.amount = [wsClass.amount intValue];
        self.transactionId = wsClass.transaction_id;
        self.card = wsClass.card;
        self.cvc2 = wsClass.cvc2;
    }
    
    return self;
}



@end