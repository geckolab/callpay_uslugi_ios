//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiPhotoArray.h"
#import "UpaidApiPhoto.h"
#import "Photo.h"
#import "NSObject+WSClass.h"


@interface UpaidApiPhotoArray ()

@property NSMutableArray *array;

@end

@implementation UpaidApiPhotoArray

- (id) init {
    self = [super init];
    
    if (self) {
        self.array = [[NSMutableArray alloc] init];
    }

    return self;
}

- (UpaidApiPhoto *) objectAtIndex:(NSUInteger)index {
    return (UpaidApiPhoto *)[self.array objectAtIndex: index];
}

- (void) addObject: (UpaidApiPhoto *)object {
    [self.array addObject: object];
}

- (NSUInteger) count {
    return [self.array count];
}

- (id) wsClass {
    NSMutableArray *arrayOfWsObjects = [[NSMutableArray alloc] init];

    for (UpaidApiPhoto *singleObject in self.array) {
        [arrayOfWsObjects addObject: [singleObject wsClass]];
    }

    return arrayOfWsObjects;
}

- (id) initWithWsClass: (id) wsObject {
    self = [self init];
    
    if (self) {
        for (Photo *wsSingleObject in (NSMutableArray *) wsObject) {
            UpaidApiPhoto *object = [[UpaidApiPhoto alloc] initWithWsClass: wsSingleObject];
            [self.array addObject: object];
        }
    }
    
    return self;
}

- (NSString *) description {
  NSMutableString *description = [[NSMutableString alloc] init];
  
  for (int i = 0; i < [self.array count]; i++) {
    [description appendFormat: @"\nUpaidApiPhotoArray[%d]:\n%@\n", i, [[self.array objectAtIndex:i] description]];
  }
  
  return description;
}



@end