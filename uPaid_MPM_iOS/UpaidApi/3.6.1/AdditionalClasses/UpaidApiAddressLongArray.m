//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiAddressLongArray.h"
#import "UpaidApiAddressLong.h"
#import "AddressLong.h"
#import "NSObject+WSClass.h"


@interface UpaidApiAddressLongArray ()

@property NSMutableArray *array;

@end

@implementation UpaidApiAddressLongArray

- (id) init {
    self = [super init];
    
    if (self) {
        self.array = [[NSMutableArray alloc] init];
    }

    return self;
}

- (UpaidApiAddressLong *) objectAtIndex:(NSUInteger)index {
    return (UpaidApiAddressLong *)[self.array objectAtIndex: index];
}

- (void) addObject: (UpaidApiAddressLong *)object {
    [self.array addObject: object];
}

- (NSUInteger) count {
    return [self.array count];
}

- (id) wsClass {
    NSMutableArray *arrayOfWsObjects = [[NSMutableArray alloc] init];

    for (UpaidApiAddressLong *singleObject in self.array) {
        [arrayOfWsObjects addObject: [singleObject wsClass]];
    }

    return arrayOfWsObjects;
}

- (id) initWithWsClass: (id) wsObject {
    self = [self init];
    
    if (self) {
        for (AddressLong *wsSingleObject in (NSMutableArray *) wsObject) {
            UpaidApiAddressLong *object = [[UpaidApiAddressLong alloc] initWithWsClass: wsSingleObject];
            [self.array addObject: object];
        }
    }
    
    return self;
}

- (NSString *) description {
  NSMutableString *description = [[NSMutableString alloc] init];
  
  for (int i = 0; i < [self.array count]; i++) {
    [description appendFormat: @"\nUpaidApiAddressLongArray[%d]:\n%@\n", i, [[self.array objectAtIndex:i] description]];
  }
  
  return description;
}



@end