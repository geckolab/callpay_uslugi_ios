//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

    
@interface UpaidApiAddress : NSObject

@property NSString *street; 
@property NSString *number; 
@property NSString *apartment; 
@property NSString *city; 
@property NSString *zipcode; 


//only required params
- (id) initWithStreet: (NSString *) street andNumber: (NSString *) number andApartment: (NSString *) apartment andCity: (NSString *) city andZipcode: (NSString *) zipcode;


@end