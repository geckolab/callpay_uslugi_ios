//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

    
@interface UpaidApiPhoto : NSObject

@property int iid; 
@property NSString *name; 


//only required params
- (id) initWithIid: (int ) iid andName: (NSString *) name;


@end