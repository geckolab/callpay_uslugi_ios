//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiAddress.h"
#import "NSObject+WSClass.h"
#import "address.h"

@implementation UpaidApiAddress
- (id) initWithStreet: (NSString *) street andNumber: (NSString *) number andApartment: (NSString *) apartment andCity: (NSString *) city andZipcode: (NSString *) zipcode {
  self = [super init];
  
  if (self) {
    self.street = street;
    self.number = number;
    self.apartment = apartment;
    self.city = city;
    self.zipcode = zipcode;
  }
  
  return self;
}



- (NSString *) description {
    return [NSString stringWithFormat: @"street=%@\nnumber=%@\napartment=%@\ncity=%@\nzipcode=%@", self.street, self.number, self.apartment, self.city, self.zipcode];
}

- (id) wsClass {
    address *wsClass = [[address alloc] init];
    wsClass.street = self.street;
    wsClass.number = self.number;
    wsClass.apartment = self.apartment;
    wsClass.city = self.city;
    wsClass.zipcode = self.zipcode;

    return wsClass;
}

- (id) initWithWsClass: (id) wsObject {
    self = [super init];
    
    if (self) {
        address *wsClass = (address *) wsObject;
        self.street = wsClass.street;
        self.number = wsClass.number;
        self.apartment = wsClass.apartment;
        self.city = wsClass.city;
        self.zipcode = wsClass.zipcode;
    }
    
    return self;
}



@end