//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiCard.h"
#import "NSObject+WSClass.h"
#import "Card.h"

@implementation UpaidApiCard
- (id) initWithIid: (NSString *) iid andPan: (NSString *) pan andDdefault: (BOOL ) ddefault andAuthType: (NSString *) authType andExpDate: (NSString *) expDate andAuthorized: (BOOL ) authorized andUsed: (BOOL ) used andFreezed: (BOOL ) freezed andNickname: (NSString *) nickname andCardType: (NSString *) cardType andSmName: (NSString *) smName andLocked: (BOOL ) locked andBrand: (NSString *) brand andFrontImg: (NSString *) frontImg andBackImg: (NSString *) backImg andCountry: (NSString *) country {
  self = [super init];
  
  if (self) {
    self.iid = iid;
    self.pan = pan;
    self.ddefault = ddefault;
    self.authType = authType;
    self.expDate = expDate;
    self.authorized = authorized;
    self.used = used;
    self.freezed = freezed;
    self.nickname = nickname;
    self.cardType = cardType;
    self.smName = smName;
    self.locked = locked;
    self.brand = brand;
    self.frontImg = frontImg;
    self.backImg = backImg;
    self.country = country;
  }
  
  return self;
}



- (NSString *) description {
    return [NSString stringWithFormat: @"iid=%@\npan=%@\nddefault=%@\nauthType=%@\nexpDate=%@\nauthorized=%@\nused=%@\nfreezed=%@\nnickname=%@\ncardType=%@\nsmName=%@\nlocked=%@\nbrand=%@\nfrontImg=%@\nbackImg=%@\ncountry=%@", self.iid, self.pan, [NSString stringWithFormat: @"%d", self.ddefault], self.authType, self.expDate, [NSString stringWithFormat: @"%d", self.authorized], [NSString stringWithFormat: @"%d", self.used], [NSString stringWithFormat: @"%d", self.freezed], self.nickname, self.cardType, self.smName, [NSString stringWithFormat: @"%d", self.locked], self.brand, self.frontImg, self.backImg, self.country];
}

- (id) wsClass {
    Card *wsClass = [[Card alloc] init];
    wsClass.iId = self.iid;
    wsClass.pan = self.pan;
    wsClass.ddefault = self.ddefault;
    wsClass.auth_type = self.authType;
    wsClass.exp_date = self.expDate;
    wsClass.authorized = self.authorized;
    wsClass.used = self.used;
    wsClass.freezed = self.freezed;
    wsClass.nickname = self.nickname;
    wsClass.card_type = self.cardType;
    wsClass.sM_name = self.smName;
    wsClass.locked = self.locked;
    wsClass.brand = self.brand;
    wsClass.front_img = self.frontImg;
    wsClass.back_img = self.backImg;
    wsClass.country = self.country;

    return wsClass;
}

- (id) initWithWsClass: (id) wsObject {
    self = [super init];
    
    if (self) {
        Card *wsClass = (Card *) wsObject;
        self.iid = wsClass.iId;
        self.pan = wsClass.pan;
        self.ddefault = wsClass.ddefault;
        self.authType = wsClass.auth_type;
        self.expDate = wsClass.exp_date;
        self.authorized = wsClass.authorized;
        self.used = wsClass.used;
        self.freezed = wsClass.freezed;
        self.nickname = wsClass.nickname;
        self.cardType = wsClass.card_type;
        self.smName = wsClass.sM_name;
        self.locked = wsClass.locked;
        self.brand = wsClass.brand;
        self.frontImg = wsClass.front_img;
        self.backImg = wsClass.back_img;
        self.country = wsClass.country;
    }
    
    return self;
}



@end