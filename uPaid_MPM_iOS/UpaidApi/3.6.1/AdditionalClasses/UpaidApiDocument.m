//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiDocument.h"
#import "NSObject+WSClass.h"
#import "Document.h"

@implementation UpaidApiDocument
- (id) initWithIid: (int ) iid andUserId: (NSString *) userId andName: (NSString *) name andType: (NSString *) type andBarcode: (NSString *) barcode andPhotos: (UpaidApiPhotoArray *) photos {
  self = [super init];
  
  if (self) {
    self.iid = iid;
    self.userId = userId;
    self.name = name;
    self.type = type;
    self.barcode = barcode;
    self.photos = photos;
  }
  
  return self;
}



- (NSString *) description {
    return [NSString stringWithFormat: @"iid=%@\nuserId=%@\nname=%@\ntype=%@\nbarcode=%@\nphotos=%@", [NSString stringWithFormat: @"%d", self.iid], self.userId, self.name, self.type, self.barcode, self.photos];
}

- (id) wsClass {
    Document *wsClass = [[Document alloc] init];
    wsClass.iId = [NSString stringWithFormat: @"%d", self.iid];
    wsClass.user_id = self.userId;
    wsClass.name = self.name;
    wsClass.type = self.type;
    wsClass.barcode = self.barcode;
    wsClass.photos = (NSMutableArray *)[self.photos wsClass];

    return wsClass;
}

- (id) initWithWsClass: (id) wsObject {
    self = [super init];
    
    if (self) {
        Document *wsClass = (Document *) wsObject;
        self.iid = [wsClass.iId intValue];
        self.userId = wsClass.user_id;
        self.name = wsClass.name;
        self.type = wsClass.type;
        self.barcode = wsClass.barcode;
        self.photos = [[UpaidApiPhotoArray alloc] initWithWsClass: wsClass.photos];
    }
    
    return self;
}



@end