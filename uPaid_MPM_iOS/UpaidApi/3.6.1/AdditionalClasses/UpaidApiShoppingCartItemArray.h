//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiShoppingCartItem.h"

@interface UpaidApiShoppingCartItemArray : NSObject

- (UpaidApiShoppingCartItem *) objectAtIndex:(NSUInteger)index;
- (void) addObject: (UpaidApiShoppingCartItem *)object;
- (NSUInteger) count;



@end