//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiAddress.h"

    
@interface UpaidApiEditProfile : NSObject

@property NSString *phone; //optional
@property NSString *email; //optional
@property NSString *firstName; //optional
@property NSString *lastName; //optional
@property UpaidApiAddress *address; //optional


//required + optional params
- (id) initWithPhone: (NSString *) phone andEmail: (NSString *) email andFirstName: (NSString *) firstName andLastName: (NSString *) lastName andAddress: (UpaidApiAddress *) address;


@end