//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiExtrasSingleParam.h"
#import "NSObject+WSClass.h"
#import "SingleParam.h"

@implementation UpaidApiExtrasSingleParam
- (id) initWithKey: (NSString *) key andValue: (NSString *) value {
  self = [super init];
  
  if (self) {
    self.key = key;
    self.value = value;
  }
  
  return self;
}



- (NSString *) description {
    return [NSString stringWithFormat: @"key=%@\nvalue=%@", self.key, self.value];
}

- (id) wsClass {
    SingleParam *wsClass = [[SingleParam alloc] init];
    wsClass.key = self.key;
    wsClass.value = self.value;

    return wsClass;
}

- (id) initWithWsClass: (id) wsObject {
    self = [super init];
    
    if (self) {
        SingleParam *wsClass = (SingleParam *) wsObject;
        self.key = wsClass.key;
        self.value = wsClass.value;
    }
    
    return self;
}

-(NSString*)toString:(BOOL)addNameWrap {
    return [[self wsClass] toString:addNameWrap];
}

@end