//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiShowProfile.h"
#import "NSObject+WSClass.h"
#import "showProfileData.h"

@implementation UpaidApiShowProfile
- (id) initWithPhone: (NSString *) phone andEmail: (NSString *) email andFirstName: (NSString *) firstName andLastName: (NSString *) lastName andLimit: (NSString *) limit andIbansLimit: (float ) ibansLimit andIbansLimitSpecified: (BOOL ) ibansLimitSpecified andAddress: (UpaidApiAddress *) address {
  self = [super init];
  
  if (self) {
    self.phone = phone;
    self.email = email;
    self.firstName = firstName;
    self.lastName = lastName;
    self.limit = limit;
    self.ibansLimit = ibansLimit;
    self.ibansLimitSpecified = ibansLimitSpecified;
    self.address = address;
  }
  
  return self;
}



- (NSString *) description {
    return [NSString stringWithFormat: @"phone=%@\nemail=%@\nfirstName=%@\nlastName=%@\nlimit=%@\nibansLimit=%@\nibansLimitSpecified=%@\naddress=%@", self.phone, self.email, self.firstName, self.lastName, self.limit, [NSString stringWithFormat: @"%.2f", self.ibansLimit], [NSString stringWithFormat: @"%d", self.ibansLimitSpecified], self.address];
}

- (id) wsClass {
    showProfileData *wsClass = [[showProfileData alloc] init];
    wsClass.phone = self.phone;
    wsClass.email = self.email;
    wsClass.first_name = self.firstName;
    wsClass.last_name = self.lastName;
    wsClass.limit = self.limit;
    wsClass.ibans_limit = self.ibansLimit;
    wsClass.ibans_limitSpecified = self.ibansLimitSpecified;
    wsClass.address = (address *)[self.address wsClass];

    return wsClass;
}

- (id) initWithWsClass: (id) wsObject {
    self = [super init];
    
    if (self) {
        showProfileData *wsClass = (showProfileData *) wsObject;
        self.phone = wsClass.phone;
        self.email = wsClass.email;
        self.firstName = wsClass.first_name;
        self.lastName = wsClass.last_name;
        self.limit = wsClass.limit;
        self.ibansLimit = wsClass.ibans_limit;
        self.ibansLimitSpecified = wsClass.ibans_limitSpecified;
        self.address = [[UpaidApiAddress alloc] initWithWsClass: wsClass.address];
    }
    
    return self;
}



@end