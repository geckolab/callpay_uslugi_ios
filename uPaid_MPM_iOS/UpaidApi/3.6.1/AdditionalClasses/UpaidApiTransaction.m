//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiTransaction.h"
#import "NSObject+WSClass.h"
#import "transactionStatus.h"

@implementation UpaidApiTransaction
- (id) initWithIid: (NSString *) iid andPhone: (NSString *) phone andCardId: (NSString *) cardId andAmount: (NSString *) amount andTransactionId: (NSString *) transactionId andDdescription: (NSString *) ddescription andChannel: (NSString *) channel andStatus: (NSString *) status andMore: (NSString *) more andCreated: (NSString *) created andCurrency: (NSString *) currency andAcceptableCardTypes: (NSString *) acceptableCardTypes andSuppressShippingAddress: (BOOL ) suppressShippingAddress {
  self = [super init];
  
  if (self) {
    self.iid = iid;
    self.phone = phone;
    self.cardId = cardId;
    self.amount = amount;
    self.transactionId = transactionId;
    self.ddescription = ddescription;
    self.channel = channel;
    self.status = status;
    self.more = more;
    self.created = created;
    self.currency = currency;
    self.acceptableCardTypes = acceptableCardTypes;
    self.suppressShippingAddress = suppressShippingAddress;
  }
  
  return self;
}



- (NSString *) description {
    return [NSString stringWithFormat: @"iid=%@\nphone=%@\ncardId=%@\namount=%@\ntransactionId=%@\nddescription=%@\nchannel=%@\nstatus=%@\nmore=%@\ncreated=%@\ncurrency=%@\nacceptableCardTypes=%@\nsuppressShippingAddress=%@", self.iid, self.phone, self.cardId, self.amount, self.transactionId, self.ddescription, self.channel, self.status, self.more, self.created, self.currency, self.acceptableCardTypes, [NSString stringWithFormat: @"%d", self.suppressShippingAddress]];
}

- (id) wsClass {
    transactionStatus *wsClass = [[transactionStatus alloc] init];
    wsClass.iId = self.iid;
    wsClass.phone = self.phone;
    wsClass.card_id = self.cardId;
    wsClass.amount = self.amount;
    wsClass.transaction_id = self.transactionId;
    wsClass.ddescription = self.ddescription;
    wsClass.channel = self.channel;
    wsClass.status = self.status;
    wsClass.more = self.more;
    wsClass.created = self.created;
    wsClass.currency = self.currency;
    wsClass.acceptable_card_types = self.acceptableCardTypes;
    wsClass.suppress_shipping_address = self.suppressShippingAddress;

    return wsClass;
}

- (id) initWithWsClass: (id) wsObject {
    self = [super init];
    
    if (self) {
        transactionStatus *wsClass = (transactionStatus *) wsObject;
        self.iid = wsClass.iId;
        self.phone = wsClass.phone;
        self.cardId = wsClass.card_id;
        self.amount = wsClass.amount;
        self.transactionId = wsClass.transaction_id;
        self.ddescription = wsClass.ddescription;
        self.channel = wsClass.channel;
        self.status = wsClass.status;
        self.more = wsClass.more;
        self.created = wsClass.created;
        self.currency = wsClass.currency;
        self.acceptableCardTypes = wsClass.acceptable_card_types;
        self.suppressShippingAddress = wsClass.suppress_shipping_address;
    }
    
    return self;
}



@end