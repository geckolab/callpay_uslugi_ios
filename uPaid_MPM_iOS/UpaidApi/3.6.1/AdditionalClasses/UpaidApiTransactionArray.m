//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiTransactionArray.h"
#import "UpaidApiTransaction.h"
#import "transactionStatus.h"
#import "NSObject+WSClass.h"


@interface UpaidApiTransactionArray ()

@property NSMutableArray *array;

@end

@implementation UpaidApiTransactionArray

- (id) init {
    self = [super init];
    
    if (self) {
        self.array = [[NSMutableArray alloc] init];
    }

    return self;
}

- (UpaidApiTransaction *) objectAtIndex:(NSUInteger)index {
    return (UpaidApiTransaction *)[self.array objectAtIndex: index];
}

- (void) addObject: (UpaidApiTransaction *)object {
    [self.array addObject: object];
}

- (NSUInteger) count {
    return [self.array count];
}

- (id) wsClass {
    NSMutableArray *arrayOfWsObjects = [[NSMutableArray alloc] init];

    for (UpaidApiTransaction *singleObject in self.array) {
        [arrayOfWsObjects addObject: [singleObject wsClass]];
    }

    return arrayOfWsObjects;
}

- (id) initWithWsClass: (id) wsObject {
    self = [self init];
    
    if (self) {
        for (transactionStatus *wsSingleObject in (NSMutableArray *) wsObject) {
            UpaidApiTransaction *object = [[UpaidApiTransaction alloc] initWithWsClass: wsSingleObject];
            [self.array addObject: object];
        }
    }
    
    return self;
}

- (NSString *) description {
  NSMutableString *description = [[NSMutableString alloc] init];
  
  for (int i = 0; i < [self.array count]; i++) {
    [description appendFormat: @"\nUpaidApiTransactionArray[%d]:\n%@\n", i, [[self.array objectAtIndex:i] description]];
  }
  
  return description;
}



@end