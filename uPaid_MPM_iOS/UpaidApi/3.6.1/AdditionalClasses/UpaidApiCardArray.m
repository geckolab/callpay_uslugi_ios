//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiCardArray.h"
#import "UpaidApiCard.h"
#import "Card.h"
#import "NSObject+WSClass.h"


@interface UpaidApiCardArray ()

@property NSMutableArray *array;

@end

@implementation UpaidApiCardArray

- (id) init {
    self = [super init];
    
    if (self) {
        self.array = [[NSMutableArray alloc] init];
    }

    return self;
}

- (UpaidApiCard *) objectAtIndex:(NSUInteger)index {
    return (UpaidApiCard *)[self.array objectAtIndex: index];
}

- (void) addObject: (UpaidApiCard *)object {
    [self.array addObject: object];
}

- (NSUInteger) count {
    return [self.array count];
}

- (id) wsClass {
    NSMutableArray *arrayOfWsObjects = [[NSMutableArray alloc] init];

    for (UpaidApiCard *singleObject in self.array) {
        [arrayOfWsObjects addObject: [singleObject wsClass]];
    }

    return arrayOfWsObjects;
}

- (id) initWithWsClass: (id) wsObject {
    self = [self init];
    
    if (self) {
        for (Card *wsSingleObject in (NSMutableArray *) wsObject) {
            UpaidApiCard *object = [[UpaidApiCard alloc] initWithWsClass: wsSingleObject];
            [self.array addObject: object];
        }
    }
    
    return self;
}

- (NSString *) description {
  NSMutableString *description = [[NSMutableString alloc] init];
  
  for (int i = 0; i < [self.array count]; i++) {
    [description appendFormat: @"\nUpaidApiCardArray[%d]:\n%@\n", i, [[self.array objectAtIndex:i] description]];
  }
  
  return description;
}

- (NSArray *) readyToUseCards {
  return [self.array filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"locked = 0 and authorized = 1"]];
}

- (NSUInteger) readyToUseCardsCount {
  return [[self readyToUseCards] count];
}

- (UpaidApiCard *) defaultCard {
    if ([self readyToUseCardsCount] == 0) {
        return nil;
    }
    
    for (UpaidApiCard *card in [self readyToUseCards]) {
        if (card.ddefault) {
            return card;
        }
    }
  
    return [[self readyToUseCards] firstObject];
}

- (void) prepareArray {
  NSMutableArray *newArray = [[NSMutableArray alloc] init];
  UpaidApiCard *defaultCard = [self defaultCard];
  
  if (defaultCard == nil) {
    return;
  }
  
  [newArray addObject: defaultCard];
  
  for (int i = 0; i < [self.array count]; i++) {
    if (![[((UpaidApiCard*)[self.array objectAtIndex:i]) iid] isEqualToString:[defaultCard iid]]) {
      [newArray addObject:[self.array objectAtIndex:i]];
    }
  }
  
  self.array = newArray;
}



@end