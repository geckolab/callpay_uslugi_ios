//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

    
@interface UpaidApiExtrasSingleParam : NSObject

@property NSString *key; 
@property NSString *value; 


//only required params
- (id) initWithKey: (NSString *) key andValue: (NSString *) value;


@end