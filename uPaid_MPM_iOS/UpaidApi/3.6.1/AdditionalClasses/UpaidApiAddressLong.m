//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiAddressLong.h"
#import "NSObject+WSClass.h"
#import "AddressLong.h"

@implementation UpaidApiAddressLong
- (id) initWithIid: (int ) iid andStreet: (NSString *) street andNumber: (NSString *) number andApartment: (NSString *) apartment andCity: (NSString *) city andZipcode: (NSString *) zipcode andCountryCode: (NSString *) countryCode andCountrySubdivision: (NSString *) countrySubdivision {
  self = [super init];
  
  if (self) {
    self.iid = iid;
    self.street = street;
    self.number = number;
    self.apartment = apartment;
    self.city = city;
    self.zipcode = zipcode;
    self.countryCode = countryCode;
    self.countrySubdivision = countrySubdivision;
  }
  
  return self;
}



- (NSString *) description {
    return [NSString stringWithFormat: @"iid=%@\nstreet=%@\nnumber=%@\napartment=%@\ncity=%@\nzipcode=%@\ncountryCode=%@\ncountrySubdivision=%@", [NSString stringWithFormat: @"%d", self.iid], self.street, self.number, self.apartment, self.city, self.zipcode, self.countryCode, self.countrySubdivision];
}

- (id) wsClass {
    AddressLong *wsClass = [[AddressLong alloc] init];
    wsClass.iId = [NSString stringWithFormat: @"%d", self.iid];
    wsClass.street = self.street;
    wsClass.number = self.number;
    wsClass.apartment = self.apartment;
    wsClass.city = self.city;
    wsClass.zipcode = self.zipcode;
    wsClass.countryCode = self.countryCode;
    wsClass.countrySubdivision = self.countrySubdivision;

    return wsClass;
}

- (id) initWithWsClass: (id) wsObject {
    self = [super init];
    
    if (self) {
        AddressLong *wsClass = (AddressLong *) wsObject;
        self.iid = [wsClass.iId intValue];
        self.street = wsClass.street;
        self.number = wsClass.number;
        self.apartment = wsClass.apartment;
        self.city = wsClass.city;
        self.zipcode = wsClass.zipcode;
        self.countryCode = wsClass.countryCode;
        self.countrySubdivision = wsClass.countrySubdivision;
    }
    
    return self;
}



@end