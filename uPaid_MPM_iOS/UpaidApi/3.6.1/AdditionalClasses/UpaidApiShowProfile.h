//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiAddress.h"

    
@interface UpaidApiShowProfile : NSObject

@property NSString *phone; 
@property NSString *email; 
@property NSString *firstName; 
@property NSString *lastName; 
@property NSString *limit; 
@property float ibansLimit; 
@property BOOL ibansLimitSpecified; 
@property UpaidApiAddress *address; 


//only required params
- (id) initWithPhone: (NSString *) phone andEmail: (NSString *) email andFirstName: (NSString *) firstName andLastName: (NSString *) lastName andLimit: (NSString *) limit andIbansLimit: (float ) ibansLimit andIbansLimitSpecified: (BOOL ) ibansLimitSpecified andAddress: (UpaidApiAddress *) address;


@end