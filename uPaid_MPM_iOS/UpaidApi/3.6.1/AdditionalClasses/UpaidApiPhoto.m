//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiPhoto.h"
#import "NSObject+WSClass.h"
#import "Photo.h"

@implementation UpaidApiPhoto
- (id) initWithIid: (int ) iid andName: (NSString *) name {
  self = [super init];
  
  if (self) {
    self.iid = iid;
    self.name = name;
  }
  
  return self;
}



- (NSString *) description {
    return [NSString stringWithFormat: @"iid=%@\nname=%@", [NSString stringWithFormat: @"%d", self.iid], self.name];
}

- (id) wsClass {
    Photo *wsClass = [[Photo alloc] init];
    wsClass.iId = [NSString stringWithFormat: @"%d", self.iid];
    wsClass.name = self.name;

    return wsClass;
}

- (id) initWithWsClass: (id) wsObject {
    self = [super init];
    
    if (self) {
        Photo *wsClass = (Photo *) wsObject;
        self.iid = [wsClass.iId intValue];
        self.name = wsClass.name;
    }
    
    return self;
}



@end