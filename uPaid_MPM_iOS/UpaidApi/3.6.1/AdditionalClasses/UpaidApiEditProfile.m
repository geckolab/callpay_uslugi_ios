//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiEditProfile.h"
#import "NSObject+WSClass.h"
#import "editProfileData.h"

@implementation UpaidApiEditProfile
- (id) initWithPhone: (NSString *) phone andEmail: (NSString *) email andFirstName: (NSString *) firstName andLastName: (NSString *) lastName andAddress: (UpaidApiAddress *) address {
  self = [super init];
  
  if (self) {
    self.phone = phone;
    self.email = email;
    self.firstName = firstName;
    self.lastName = lastName;
    self.address = address;
  }
  
  return self;
}

- (id) init{
  self = [super init];
  
  if (self) {
    self.phone = @"";
    self.email = @"";
    self.firstName = @"";
    self.lastName = @"";
    self.address = [[UpaidApiAddress alloc] init];
  }
  
  return self;
}


- (NSString *) description {
    return [NSString stringWithFormat: @"phone=%@\nemail=%@\nfirstName=%@\nlastName=%@\naddress=%@", self.phone, self.email, self.firstName, self.lastName, self.address];
}

- (id) wsClass {
    editProfileData *wsClass = [[editProfileData alloc] init];
    wsClass.phone = self.phone;
    wsClass.email = self.email;
    wsClass.first_name = self.firstName;
    wsClass.last_name = self.lastName;
    wsClass.address = (address *)[self.address wsClass];

    return wsClass;
}

- (id) initWithWsClass: (id) wsObject {
    self = [super init];
    
    if (self) {
        editProfileData *wsClass = (editProfileData *) wsObject;
        self.phone = wsClass.phone;
        self.email = wsClass.email;
        self.firstName = wsClass.first_name;
        self.lastName = wsClass.last_name;
        self.address = [[UpaidApiAddress alloc] initWithWsClass: wsClass.address];
    }
    
    return self;
}



@end