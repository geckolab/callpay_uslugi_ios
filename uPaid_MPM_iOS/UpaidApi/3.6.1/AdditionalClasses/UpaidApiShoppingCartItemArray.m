//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiShoppingCartItemArray.h"
#import "UpaidApiShoppingCartItem.h"
#import "singleShoppingCartItem.h"
#import "NSObject+WSClass.h"


@interface UpaidApiShoppingCartItemArray ()

@property NSMutableArray *array;

@end

@implementation UpaidApiShoppingCartItemArray

- (id) init {
    self = [super init];
    
    if (self) {
        self.array = [[NSMutableArray alloc] init];
    }

    return self;
}

- (UpaidApiShoppingCartItem *) objectAtIndex:(NSUInteger)index {
    return (UpaidApiShoppingCartItem *)[self.array objectAtIndex: index];
}

- (void) addObject: (UpaidApiShoppingCartItem *)object {
    [self.array addObject: object];
}

- (NSUInteger) count {
    return [self.array count];
}

- (id) wsClass {
    NSMutableArray *arrayOfWsObjects = [[NSMutableArray alloc] init];

    for (UpaidApiShoppingCartItem *singleObject in self.array) {
        [arrayOfWsObjects addObject: [singleObject wsClass]];
    }

    return arrayOfWsObjects;
}

- (id) initWithWsClass: (id) wsObject {
    self = [self init];
    
    if (self) {
        for (singleShoppingCartItem *wsSingleObject in (NSMutableArray *) wsObject) {
            UpaidApiShoppingCartItem *object = [[UpaidApiShoppingCartItem alloc] initWithWsClass: wsSingleObject];
            [self.array addObject: object];
        }
    }
    
    return self;
}

- (NSString *) description {
  NSMutableString *description = [[NSMutableString alloc] init];
  
  for (int i = 0; i < [self.array count]; i++) {
    [description appendFormat: @"\nUpaidApiShoppingCartItemArray[%d]:\n%@\n", i, [[self.array objectAtIndex:i] description]];
  }
  
  return description;
}



@end