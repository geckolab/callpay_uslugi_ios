//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiPhoto.h"
#import "UpaidApiPhotoArray.h"

    
@interface UpaidApiReceipt : NSObject

@property int iid; 
@property NSString *userId; 
@property int amount; 
@property NSString *name; 
@property NSString *created; 
@property UpaidApiPhotoArray *photos; 


//only required params
- (id) initWithIid: (int ) iid andUserId: (NSString *) userId andAmount: (int ) amount andName: (NSString *) name andCreated: (NSString *) created andPhotos: (UpaidApiPhotoArray *) photos;


@end