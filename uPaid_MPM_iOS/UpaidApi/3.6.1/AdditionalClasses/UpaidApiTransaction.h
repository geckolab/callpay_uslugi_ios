//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

    
@interface UpaidApiTransaction : NSObject

@property NSString *iid; 
@property NSString *phone; 
@property NSString *cardId; 
@property NSString *amount; 
@property NSString *transactionId; 
@property NSString *ddescription; 
@property NSString *channel; 
@property NSString *status; 
@property NSString *more; 
@property NSString *created; 
@property NSString *currency; 
@property NSString *acceptableCardTypes; 
@property BOOL suppressShippingAddress; 
@property BOOL isPending;



//only required params
- (id) initWithIid: (NSString *) iid andPhone: (NSString *) phone andCardId: (NSString *) cardId andAmount: (NSString *) amount andTransactionId: (NSString *) transactionId andDdescription: (NSString *) ddescription andChannel: (NSString *) channel andStatus: (NSString *) status andMore: (NSString *) more andCreated: (NSString *) created andCurrency: (NSString *) currency andAcceptableCardTypes: (NSString *) acceptableCardTypes andSuppressShippingAddress: (BOOL ) suppressShippingAddress;


@end