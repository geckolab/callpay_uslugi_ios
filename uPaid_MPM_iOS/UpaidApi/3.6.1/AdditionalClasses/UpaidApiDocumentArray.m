//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiDocumentArray.h"
#import "UpaidApiDocument.h"
#import "Document.h"
#import "NSObject+WSClass.h"


@interface UpaidApiDocumentArray ()

@property NSMutableArray *array;

@end

@implementation UpaidApiDocumentArray

- (id) init {
    self = [super init];
    
    if (self) {
        self.array = [[NSMutableArray alloc] init];
    }

    return self;
}

- (UpaidApiDocument *) objectAtIndex:(NSUInteger)index {
    return (UpaidApiDocument *)[self.array objectAtIndex: index];
}

- (void) addObject: (UpaidApiDocument *)object {
    [self.array addObject: object];
}

- (NSUInteger) count {
    return [self.array count];
}

- (id) wsClass {
    NSMutableArray *arrayOfWsObjects = [[NSMutableArray alloc] init];

    for (UpaidApiDocument *singleObject in self.array) {
        [arrayOfWsObjects addObject: [singleObject wsClass]];
    }

    return arrayOfWsObjects;
}

- (id) initWithWsClass: (id) wsObject {
    self = [self init];
    
    if (self) {
        for (Document *wsSingleObject in (NSMutableArray *) wsObject) {
            UpaidApiDocument *object = [[UpaidApiDocument alloc] initWithWsClass: wsSingleObject];
            [self.array addObject: object];
        }
    }
    
    return self;
}

- (NSString *) description {
  NSMutableString *description = [[NSMutableString alloc] init];
  
  for (int i = 0; i < [self.array count]; i++) {
    [description appendFormat: @"\nUpaidApiDocumentArray[%d]:\n%@\n", i, [[self.array objectAtIndex:i] description]];
  }
  
  return description;
}



@end