//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiDocument.h"

@interface UpaidApiDocumentArray : NSObject

- (UpaidApiDocument *) objectAtIndex:(NSUInteger)index;
- (void) addObject: (UpaidApiDocument *)object;
- (NSUInteger) count;



@end