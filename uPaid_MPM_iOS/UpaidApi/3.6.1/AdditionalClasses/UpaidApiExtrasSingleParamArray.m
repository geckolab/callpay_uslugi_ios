//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiExtrasSingleParamArray.h"
#import "UpaidApiExtrasSingleParam.h"
#import "SingleParam.h"
#import "NSObject+WSClass.h"


@interface UpaidApiExtrasSingleParamArray ()

@property NSMutableArray *array;

@end

@implementation UpaidApiExtrasSingleParamArray

- (id) init {
    self = [super init];
    
    if (self) {
        self.array = [[NSMutableArray alloc] init];
    }

    return self;
}

- (UpaidApiExtrasSingleParam *) objectAtIndex:(NSUInteger)index {
    return (UpaidApiExtrasSingleParam *)[self.array objectAtIndex: index];
}

- (void) addObject: (UpaidApiExtrasSingleParam *)object {
    [self.array addObject: object];
}

- (NSUInteger) count {
    return [self.array count];
}

- (id) wsClass {
    NSMutableArray *arrayOfWsObjects = [[NSMutableArray alloc] init];

    for (UpaidApiExtrasSingleParam *singleObject in self.array) {
        [arrayOfWsObjects addObject: [singleObject wsClass]];
    }

    return arrayOfWsObjects;
}

- (id) initWithWsClass: (id) wsObject {
    self = [self init];
    
    if (self) {
        for (SingleParam *wsSingleObject in (NSMutableArray *) wsObject) {
            UpaidApiExtrasSingleParam *object = [[UpaidApiExtrasSingleParam alloc] initWithWsClass: wsSingleObject];
            [self.array addObject: object];
        }
    }
    
    return self;
}

- (NSString *) description {
  NSMutableString *description = [[NSMutableString alloc] init];
  
  for (int i = 0; i < [self.array count]; i++) {
    [description appendFormat: @"\nUpaidApiExtrasSingleParamArray[%d]:\n%@\n", i, [[self.array objectAtIndex:i] description]];
  }
  
  return description;
}



@end