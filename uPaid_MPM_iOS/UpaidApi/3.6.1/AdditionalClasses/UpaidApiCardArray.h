//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiCard.h"

@interface UpaidApiCardArray : NSObject

- (UpaidApiCard *) objectAtIndex:(NSUInteger)index;
- (void) addObject: (UpaidApiCard *)object;
- (NSUInteger) count;

- (UpaidApiCard *) defaultCard;

//change order - default card will be on first position of array
//execute automatically
- (void) prepareArray;
- (NSUInteger) readyToUseCardsCount;

@end