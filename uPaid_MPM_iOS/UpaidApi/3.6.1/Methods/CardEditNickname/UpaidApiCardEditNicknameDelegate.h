//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiCardEditNicknameDelegate <NSObject>

- (void) onUpaidApiCardEditNicknameSuccess: (UpaidApiCardEditNicknameResult *) result;
- (void) onUpaidApiCardEditNicknameFail: (UpaidApiCardEditNicknameResult *) result;

@optional
- (void) onUpaidApiCardEditNicknameOperationFailed: (UpaidApiCardEditNicknameResult *) result;
- (void) onUpaidApiCardEditNicknameCardNotFound: (UpaidApiCardEditNicknameResult *) result;
- (void) onUpaidApiCardEditNicknameTechnicalError: (UpaidApiCardEditNicknameResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiCardEditNicknameDelegate implementation

- (void) onUpaidApiCardEditNicknameSuccess: (UpaidApiCardEditNicknameResult *) result {

}

- (void) onUpaidApiCardEditNicknameFail: (UpaidApiCardEditNicknameResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiCardEditNicknameDelegate implementation

- (void) onUpaidApiCardEditNicknameSuccess: (UpaidApiCardEditNicknameResult *) result {

}

- (void) onUpaidApiCardEditNicknameFail: (UpaidApiCardEditNicknameResult *) result {

}

- (void) onUpaidApiCardEditNicknameOperationFailed: (UpaidApiCardEditNicknameResult *) result {

}

- (void) onUpaidApiCardEditNicknameCardNotFound: (UpaidApiCardEditNicknameResult *) result {

}

- (void) onUpaidApiCardEditNicknameTechnicalError: (UpaidApiCardEditNicknameResult *) result {

}


//-------END SELECT-------
*/
@end
