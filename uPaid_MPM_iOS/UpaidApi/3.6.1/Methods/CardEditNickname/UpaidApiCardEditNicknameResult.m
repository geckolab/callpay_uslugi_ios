//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiCardEditNicknameResult.h"
#import "UpaidApiCardEditNicknameTask.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@implementation UpaidApiCardEditNicknameResult

@synthesize status = _status;

- (id) init {
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

- (void) setStatus: (NSInteger) status {
  _status = status;
  
  if (self.status == kUpaidApiCardEditNicknameSuccess) {
    self.statusDescription = @"";
  }

  if (self.status == kUpaidApiCardEditNicknameOperationFailed) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.CardEditNickname.OperationFailed", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiCardEditNicknameCardNotFound) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.CardEditNickname.CardNotFound", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiCardEditNicknameTechnicalError) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.CardEditNickname.TechnicalError", @"UpaidApi", nil);
  }
}

- (NSString *) description {
    return [NSString stringWithFormat: @"status=%ld\n%@", (long)self.status, @""];
}

@end
