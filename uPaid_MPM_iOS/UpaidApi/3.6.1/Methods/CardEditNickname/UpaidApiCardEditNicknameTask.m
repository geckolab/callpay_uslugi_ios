//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiCardEditNicknameTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiCardEditNicknameTask ()

@property UpaidApiCardEditNicknameData *data;
@property id<UpaidApiCardEditNicknameDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiCardEditNicknameTask


- (id) initWithData: (UpaidApiCardEditNicknameData *) taskData andDelegate: (id<UpaidApiCardEditNicknameDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"CardEditNickname data is empty.");
  } else {
    UpaidLog(@"CardEditNickname data:\n%@", self.data);
  }
  
  [self.proxy cardEditNickname: [UpaidApi sessionToken] : [NSString stringWithFormat: @"%d", self.data.cardId] : self.data.nickname ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiCardEditNicknameResult *result = [[UpaidApiCardEditNicknameResult alloc] init];
  result.status = kUpaidApiCardEditNicknameTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiCardEditNicknameTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiCardEditNicknameTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiCardEditNicknameFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiCardEditNicknameFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiCardEditNicknameResult *result = [[UpaidApiCardEditNicknameResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiCardEditNicknameSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiCardEditNicknameSuccess:)]) {

        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiCardEditNicknameSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiCardEditNicknameOperationFailed && [self.delegate respondsToSelector:@selector(onUpaidApiCardEditNicknameOperationFailed:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardEditNicknameOperationFailed:) withObject: result];
      } else if (result.status == kUpaidApiCardEditNicknameCardNotFound && [self.delegate respondsToSelector:@selector(onUpaidApiCardEditNicknameCardNotFound:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardEditNicknameCardNotFound:) withObject: result];
      } else if (result.status == kUpaidApiCardEditNicknameTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiCardEditNicknameTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardEditNicknameTechnicalError:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiCardEditNicknameFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardEditNicknameFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiCardEditNicknameResult *) result  {
    UpaidLog(@"CardEditNickname result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiCardEditNicknameResult *result = [[UpaidApiCardEditNicknameResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiCardEditNicknameFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardEditNicknameFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"CardEditNickname" withStatus:@"fail"];
    }
}

@end
