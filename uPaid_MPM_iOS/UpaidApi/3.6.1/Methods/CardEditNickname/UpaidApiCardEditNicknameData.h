//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpaidApiCardEditNicknameData : NSObject

@property int cardId; 
@property NSString *nickname; 


//only required params
- (id) initWithCardId: (int ) cardId andNickname: (NSString *) nickname;


@end