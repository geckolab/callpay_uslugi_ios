//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiCardEditNicknameData.h"
#import "UpaidApiCardEditNicknameResult.h"
#import "UpaidApiCardEditNicknameDelegate.h"

@interface UpaidApiCardEditNicknameTask : UpaidApiTask

- (id) initWithData: (UpaidApiCardEditNicknameData *) taskData andDelegate: (id<UpaidApiCardEditNicknameDelegate>) taskDelegate;

enum UpaidApiCardEditNicknameStatuses : NSUInteger{
  kUpaidApiCardEditNicknameSuccess = 1,
  kUpaidApiCardEditNicknameOperationFailed = 2,
  kUpaidApiCardEditNicknameCardNotFound = 3,
  kUpaidApiCardEditNicknameTechnicalError = 7,
};
        
@end
