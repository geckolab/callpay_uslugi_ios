//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiCardEditNicknameData.h"
#import "NSObject+WSClass.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@implementation UpaidApiCardEditNicknameData

- (id) initWithCardId: (int ) cardId andNickname: (NSString *) nickname {
  self = [super init];
  
  if (self) {
    self.cardId = cardId;
    self.nickname = nickname;
  }
  
  return self;
}



- (NSString *) description {
    return [NSString stringWithFormat: @"cardId=%@\nnickname=%@", [NSString stringWithFormat: @"%d", self.cardId], self.nickname];
}

@end