//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiCardDefaultResult.h"
#import "UpaidApiCardDefaultTask.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@implementation UpaidApiCardDefaultResult

@synthesize status = _status;

- (id) init {
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

- (void) setStatus: (NSInteger) status {
  _status = status;
  
  if (self.status == kUpaidApiCardDefaultSuccess) {
    self.statusDescription = @"";
  }

  if (self.status == kUpaidApiCardDefaultOperationFailed) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.CardDefault.OperationFailed", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiCardDefaultCardNotMC) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.CardDefault.CardNotMC", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiCardDefaultTechnicalError) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.CardDefault.TechnicalError", @"UpaidApi", nil);
  }
}

- (NSString *) description {
    return [NSString stringWithFormat: @"status=%ld\n%@", (long)self.status, @""];
}

@end
