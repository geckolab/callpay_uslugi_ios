//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiCardDefaultTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiCardDefaultTask ()

@property UpaidApiCardDefaultData *data;
@property id<UpaidApiCardDefaultDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiCardDefaultTask


- (id) initWithData: (UpaidApiCardDefaultData *) taskData andDelegate: (id<UpaidApiCardDefaultDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"CardDefault data is empty.");
  } else {
    UpaidLog(@"CardDefault data:\n%@", self.data);
  }
  
  [self.proxy cardDefault: [UpaidApi sessionToken] : self.data.cardId ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiCardDefaultResult *result = [[UpaidApiCardDefaultResult alloc] init];
  result.status = kUpaidApiCardDefaultTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiCardDefaultTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiCardDefaultTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiCardDefaultFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiCardDefaultFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiCardDefaultResult *result = [[UpaidApiCardDefaultResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiCardDefaultSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiCardDefaultSuccess:)]) {

        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiCardDefaultSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiCardDefaultOperationFailed && [self.delegate respondsToSelector:@selector(onUpaidApiCardDefaultOperationFailed:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardDefaultOperationFailed:) withObject: result];
      } else if (result.status == kUpaidApiCardDefaultCardNotMC && [self.delegate respondsToSelector:@selector(onUpaidApiCardDefaultCardNotMC:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardDefaultCardNotMC:) withObject: result];
      } else if (result.status == kUpaidApiCardDefaultTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiCardDefaultTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardDefaultTechnicalError:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiCardDefaultFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardDefaultFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiCardDefaultResult *) result  {
    UpaidLog(@"CardDefault result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiCardDefaultResult *result = [[UpaidApiCardDefaultResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiCardDefaultFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardDefaultFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"CardDefault" withStatus:@"fail"];
    }
}

@end
