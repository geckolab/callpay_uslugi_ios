//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiCardDefaultDelegate <NSObject>

- (void) onUpaidApiCardDefaultSuccess: (UpaidApiCardDefaultResult *) result;
- (void) onUpaidApiCardDefaultFail: (UpaidApiCardDefaultResult *) result;

@optional
- (void) onUpaidApiCardDefaultOperationFailed: (UpaidApiCardDefaultResult *) result;
- (void) onUpaidApiCardDefaultCardNotMC: (UpaidApiCardDefaultResult *) result;
- (void) onUpaidApiCardDefaultTechnicalError: (UpaidApiCardDefaultResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiCardDefaultDelegate implementation

- (void) onUpaidApiCardDefaultSuccess: (UpaidApiCardDefaultResult *) result {

}

- (void) onUpaidApiCardDefaultFail: (UpaidApiCardDefaultResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiCardDefaultDelegate implementation

- (void) onUpaidApiCardDefaultSuccess: (UpaidApiCardDefaultResult *) result {

}

- (void) onUpaidApiCardDefaultFail: (UpaidApiCardDefaultResult *) result {

}

- (void) onUpaidApiCardDefaultOperationFailed: (UpaidApiCardDefaultResult *) result {

}

- (void) onUpaidApiCardDefaultCardNotMC: (UpaidApiCardDefaultResult *) result {

}

- (void) onUpaidApiCardDefaultTechnicalError: (UpaidApiCardDefaultResult *) result {

}


//-------END SELECT-------
*/
@end
