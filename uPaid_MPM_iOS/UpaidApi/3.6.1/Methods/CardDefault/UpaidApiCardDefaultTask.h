//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiCardDefaultData.h"
#import "UpaidApiCardDefaultResult.h"
#import "UpaidApiCardDefaultDelegate.h"

@interface UpaidApiCardDefaultTask : UpaidApiTask

- (id) initWithData: (UpaidApiCardDefaultData *) taskData andDelegate: (id<UpaidApiCardDefaultDelegate>) taskDelegate;

enum UpaidApiCardDefaultStatuses : NSUInteger{
  kUpaidApiCardDefaultSuccess = 1,
  kUpaidApiCardDefaultOperationFailed = 2,
  kUpaidApiCardDefaultCardNotMC = 3,
  kUpaidApiCardDefaultTechnicalError = 7,
};
        
@end
