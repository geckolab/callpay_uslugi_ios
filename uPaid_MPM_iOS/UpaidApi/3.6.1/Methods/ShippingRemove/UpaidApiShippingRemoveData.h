//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpaidApiShippingRemoveData : NSObject

@property int addressId; 


//only required params
- (id) initWithAddressId: (int ) addressId;


@end