//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiShippingRemoveData.h"
#import "NSObject+WSClass.h"
#import "AddressLong.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiAddressLong.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "singleShoppingCartItem.h"
#import "transactionData.h"
#import "transactionStatus.h"


@implementation UpaidApiShippingRemoveData

- (id) initWithAddressId: (int ) addressId {
  self = [super init];
  
  if (self) {
    self.addressId = addressId;
  }
  
  return self;
}



- (NSString *) description {
    return [NSString stringWithFormat: @"addressId=%@", [NSString stringWithFormat: @"%d", self.addressId]];
}

@end