//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiShippingRemoveTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "AddressLong.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiAddressLong.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "singleShoppingCartItem.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiShippingRemoveTask ()

@property UpaidApiShippingRemoveData *data;
@property id<UpaidApiShippingRemoveDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiShippingRemoveTask


- (id) initWithData: (UpaidApiShippingRemoveData *) taskData andDelegate: (id<UpaidApiShippingRemoveDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"ShippingRemove data is empty.");
  } else {
    UpaidLog(@"ShippingRemove data:\n%@", self.data);
  }
  
  [self.proxy shippingRemove: [UpaidApi sessionToken] : self.data.addressId ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiShippingRemoveResult *result = [[UpaidApiShippingRemoveResult alloc] init];
  result.status = kUpaidApiShippingRemoveTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiShippingRemoveTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiShippingRemoveTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiShippingRemoveFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiShippingRemoveFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiShippingRemoveResult *result = [[UpaidApiShippingRemoveResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiShippingRemoveSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiShippingRemoveSuccess:)]) {

        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiShippingRemoveSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiShippingRemoveOperationFailed && [self.delegate respondsToSelector:@selector(onUpaidApiShippingRemoveOperationFailed:)]) {
        [self.delegate performSelector:@selector(onUpaidApiShippingRemoveOperationFailed:) withObject: result];
      } else if (result.status == kUpaidApiShippingRemoveAddressNotFound && [self.delegate respondsToSelector:@selector(onUpaidApiShippingRemoveAddressNotFound:)]) {
        [self.delegate performSelector:@selector(onUpaidApiShippingRemoveAddressNotFound:) withObject: result];
      } else if (result.status == kUpaidApiShippingRemoveTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiShippingRemoveTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiShippingRemoveTechnicalError:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiShippingRemoveFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiShippingRemoveFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiShippingRemoveResult *) result  {
    UpaidLog(@"ShippingRemove result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiShippingRemoveResult *result = [[UpaidApiShippingRemoveResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiShippingRemoveFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiShippingRemoveFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"ShippingRemove" withStatus:@"fail"];
    }
}

@end
