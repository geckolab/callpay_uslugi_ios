//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiShippingRemoveDelegate <NSObject>

- (void) onUpaidApiShippingRemoveSuccess: (UpaidApiShippingRemoveResult *) result;
- (void) onUpaidApiShippingRemoveFail: (UpaidApiShippingRemoveResult *) result;

@optional
- (void) onUpaidApiShippingRemoveOperationFailed: (UpaidApiShippingRemoveResult *) result;
- (void) onUpaidApiShippingRemoveAddressNotFound: (UpaidApiShippingRemoveResult *) result;
- (void) onUpaidApiShippingRemoveTechnicalError: (UpaidApiShippingRemoveResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiShippingRemoveDelegate implementation

- (void) onUpaidApiShippingRemoveSuccess: (UpaidApiShippingRemoveResult *) result {

}

- (void) onUpaidApiShippingRemoveFail: (UpaidApiShippingRemoveResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiShippingRemoveDelegate implementation

- (void) onUpaidApiShippingRemoveSuccess: (UpaidApiShippingRemoveResult *) result {

}

- (void) onUpaidApiShippingRemoveFail: (UpaidApiShippingRemoveResult *) result {

}

- (void) onUpaidApiShippingRemoveOperationFailed: (UpaidApiShippingRemoveResult *) result {

}

- (void) onUpaidApiShippingRemoveAddressNotFound: (UpaidApiShippingRemoveResult *) result {

}

- (void) onUpaidApiShippingRemoveTechnicalError: (UpaidApiShippingRemoveResult *) result {

}


//-------END SELECT-------
*/
@end
