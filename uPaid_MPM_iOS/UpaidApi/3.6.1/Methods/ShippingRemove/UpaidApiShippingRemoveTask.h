//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiShippingRemoveData.h"
#import "UpaidApiShippingRemoveResult.h"
#import "UpaidApiShippingRemoveDelegate.h"

@interface UpaidApiShippingRemoveTask : UpaidApiTask

- (id) initWithData: (UpaidApiShippingRemoveData *) taskData andDelegate: (id<UpaidApiShippingRemoveDelegate>) taskDelegate;

enum UpaidApiShippingRemoveStatuses : NSUInteger{
  kUpaidApiShippingRemoveSuccess = 1,
  kUpaidApiShippingRemoveOperationFailed = 2,
  kUpaidApiShippingRemoveAddressNotFound = 3,
  kUpaidApiShippingRemoveTechnicalError = 7,
};
        
@end
