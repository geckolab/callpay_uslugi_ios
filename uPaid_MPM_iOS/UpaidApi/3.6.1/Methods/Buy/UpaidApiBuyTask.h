//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiBuyData.h"
#import "UpaidApiBuyResult.h"
#import "UpaidApiBuyDelegate.h"

@interface UpaidApiBuyTask : UpaidApiTask

- (id) initWithData: (UpaidApiBuyData *) taskData andDelegate: (id<UpaidApiBuyDelegate>) taskDelegate;

enum UpaidApiBuyStatuses : NSUInteger{
  kUpaidApiBuySuccess = 1,
  kUpaidApiBuyOperationFailed = 2,
  kUpaidApiBuyCardNotFound = 3,
  kUpaidApiBuyIncorrectMpin = 4,
  kUpaidApiBuyUnverifiedCard = 6,
  kUpaidApiBuyTechnicalError = 7,
  kUpaidApiBuyTransactionRejectedLimit = 8,
  kUpaidApiBuyCardBlocked = 9,
  kUpaidApiBuySameTransaction = 10,
};
        
@end
