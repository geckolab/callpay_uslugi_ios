//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiResult.h"
#import "UpaidApiBuyTransactionData.h"

@interface UpaidApiBuyResult : UpaidApiResult

@property int upaidId;

@property int substatus;

@property BOOL authorized;

@property NSString *more;



@end
