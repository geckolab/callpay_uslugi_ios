//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiBuyData.h"
#import "NSObject+WSClass.h"
#import "AddressLong.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "singleShoppingCartItem.h"
#import "transactionData.h"
#import "transactionStatus.h"


@implementation UpaidApiBuyData

- (id) initWithBuyData: (UpaidApiBuyTransactionData *) buyData andForce: (int ) force andPartnerId: (int ) partnerId {
  self = [super init];
  
  if (self) {
    self.buyData = buyData;
    self.force = force;
    self.partnerId = partnerId;
  }
  
  return self;
}



- (NSString *) description {
    return [NSString stringWithFormat: @"buyData=%@\nforce=%@\npartnerId=%@", self.buyData, [NSString stringWithFormat: @"%d", self.force], [NSString stringWithFormat: @"%d", self.partnerId]];
}

@end