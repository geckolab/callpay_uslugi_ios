//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiBuyResult.h"
#import "UpaidApiBuyTask.h"
#import "AddressLong.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "singleShoppingCartItem.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiBuyResult ()

enum UpaidApiSubstatuses : NSUInteger{
  kUpaidApiSubstatusCvcProblem = 1,
  kUpaidApiSubstatusCardExpired = 2,
  kUpaidApiSubstatusLackOfFounds = 4,
  kUpaidApiSubstatusLimitExceeded = 5,
  kUpaidApiSubstatusCardLocked = 6,
};

@end

@implementation UpaidApiBuyResult

@synthesize status = _status;

- (id) init {
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

- (void) setStatus: (NSInteger) status {
  _status = status;
  
  if (self.status == kUpaidApiBuySuccess) {
    self.statusDescription = @"";
  }

  if (self.status == kUpaidApiBuyOperationFailed) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Buy.OperationFailed", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiBuyCardNotFound) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Buy.CardNotFound", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiBuyIncorrectMpin) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Buy.IncorrectMpin", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiBuyUnverifiedCard) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Buy.UnverifiedCard", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiBuyTechnicalError) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Buy.TechnicalError", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiBuyTransactionRejectedLimit) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Buy.TransactionRejectedLimit", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiBuyCardBlocked) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Buy.CardBlocked", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiBuySameTransaction) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Buy.SameTransaction", @"UpaidApi", nil);
  }


  if (self.substatus == kUpaidApiSubstatusCvcProblem) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.substatus.CvcProblem", @"UpaidApi", nil);
  }

  if (self.substatus == kUpaidApiSubstatusCardExpired) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.substatus.CardExpired", @"UpaidApi", nil);
  }

  if (self.substatus == kUpaidApiSubstatusLackOfFounds) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.substatus.LackOfFounds", @"UpaidApi", nil);
  }

  if (self.substatus == kUpaidApiSubstatusLimitExceeded) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.substatus.LimitExceeded", @"UpaidApi", nil);
  }

  if (self.substatus == kUpaidApiSubstatusCardLocked) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.substatus.CardLocked", @"UpaidApi", nil);
  }
}

- (NSString *) description {
    return [NSString stringWithFormat: @"status=%ld\n%@", (long)self.status, [NSString stringWithFormat: @"upaidId=%@\nsubstatus=%@\nauthorized=%@\nmore=%@", [NSString stringWithFormat: @"%d", self.upaidId], [NSString stringWithFormat: @"%d", self.substatus], [NSString stringWithFormat: @"%d", self.authorized], self.more]];
}

@end
