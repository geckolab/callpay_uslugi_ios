//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiBuyDelegate <NSObject>

- (void) onUpaidApiBuySuccess: (UpaidApiBuyResult *) result;
- (void) onUpaidApiBuyFail: (UpaidApiBuyResult *) result;

@optional
- (void) onUpaidApiBuyOperationFailed: (UpaidApiBuyResult *) result;
- (void) onUpaidApiBuyCardNotFound: (UpaidApiBuyResult *) result;
- (void) onUpaidApiBuyIncorrectMpin: (UpaidApiBuyResult *) result;
- (void) onUpaidApiBuyUnverifiedCard: (UpaidApiBuyResult *) result;
- (void) onUpaidApiBuyTechnicalError: (UpaidApiBuyResult *) result;
- (void) onUpaidApiBuyTransactionRejectedLimit: (UpaidApiBuyResult *) result;
- (void) onUpaidApiBuyCardBlocked: (UpaidApiBuyResult *) result;
- (void) onUpaidApiBuySameTransaction: (UpaidApiBuyResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiBuyDelegate implementation

- (void) onUpaidApiBuySuccess: (UpaidApiBuyResult *) result {

}

- (void) onUpaidApiBuyFail: (UpaidApiBuyResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiBuyDelegate implementation

- (void) onUpaidApiBuySuccess: (UpaidApiBuyResult *) result {

}

- (void) onUpaidApiBuyFail: (UpaidApiBuyResult *) result {

}

- (void) onUpaidApiBuyOperationFailed: (UpaidApiBuyResult *) result {

}

- (void) onUpaidApiBuyCardNotFound: (UpaidApiBuyResult *) result {

}

- (void) onUpaidApiBuyIncorrectMpin: (UpaidApiBuyResult *) result {

}

- (void) onUpaidApiBuyUnverifiedCard: (UpaidApiBuyResult *) result {

}

- (void) onUpaidApiBuyTechnicalError: (UpaidApiBuyResult *) result {

}

- (void) onUpaidApiBuyTransactionRejectedLimit: (UpaidApiBuyResult *) result {

}

- (void) onUpaidApiBuyCardBlocked: (UpaidApiBuyResult *) result {

}

- (void) onUpaidApiBuySameTransaction: (UpaidApiBuyResult *) result {

}


//-------END SELECT-------
*/
@end
