//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiBuyTransactionData.h"

@interface UpaidApiBuyData : NSObject

@property UpaidApiBuyTransactionData *buyData; 
@property int force; 
@property int partnerId; 


//only required params
- (id) initWithBuyData: (UpaidApiBuyTransactionData *) buyData andForce: (int ) force andPartnerId: (int ) partnerId;


@end