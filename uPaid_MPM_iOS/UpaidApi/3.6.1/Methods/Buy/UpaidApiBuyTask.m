//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiBuyTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "AddressLong.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "singleShoppingCartItem.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiBuyTask ()

@property UpaidApiBuyData *data;
@property id<UpaidApiBuyDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiBuyTask


- (id) initWithData: (UpaidApiBuyData *) taskData andDelegate: (id<UpaidApiBuyDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"Buy data is empty.");
  } else {
    UpaidLog(@"Buy data:\n%@", self.data);
  }
  
  [self.proxy buy: [UpaidApi sessionToken] : (buyData *)[self.data.buyData wsClass] : [NSString stringWithFormat: @"%d", self.data.force] : [NSString stringWithFormat: @"%d", self.data.partnerId] ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiBuyResult *result = [[UpaidApiBuyResult alloc] init];
  result.status = kUpaidApiBuyTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiBuyTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiBuyTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiBuyFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiBuyFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiBuyResult *result = [[UpaidApiBuyResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiBuySuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiBuySuccess:)]) {
        if (data[@"upaidId"] != nil) {
            result.upaidId = [data[@"upaidId"] intValue];
        }
        if (data[@"upaid_id"] != nil) {
            result.upaidId = [data[@"upaid_id"] intValue];
        }

        if (data[@"substatus"] != nil) {
            result.substatus = [data[@"substatus"] intValue];
        }

        if (data[@"authorized"] != nil) {
            result.authorized = [data[@"authorized"] boolValue];
        }

        if (data[@"more"] != nil) {
            result.more = data[@"more"];
        }


        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiBuySuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiBuyOperationFailed && [self.delegate respondsToSelector:@selector(onUpaidApiBuyOperationFailed:)]) {
        [self.delegate performSelector:@selector(onUpaidApiBuyOperationFailed:) withObject: result];
      } else if (result.status == kUpaidApiBuyCardNotFound && [self.delegate respondsToSelector:@selector(onUpaidApiBuyCardNotFound:)]) {
        [self.delegate performSelector:@selector(onUpaidApiBuyCardNotFound:) withObject: result];
      } else if (result.status == kUpaidApiBuyIncorrectMpin && [self.delegate respondsToSelector:@selector(onUpaidApiBuyIncorrectMpin:)]) {
        [self.delegate performSelector:@selector(onUpaidApiBuyIncorrectMpin:) withObject: result];
      } else if (result.status == kUpaidApiBuyUnverifiedCard && [self.delegate respondsToSelector:@selector(onUpaidApiBuyUnverifiedCard:)]) {
        [self.delegate performSelector:@selector(onUpaidApiBuyUnverifiedCard:) withObject: result];
      } else if (result.status == kUpaidApiBuyTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiBuyTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiBuyTechnicalError:) withObject: result];
      } else if (result.status == kUpaidApiBuyTransactionRejectedLimit && [self.delegate respondsToSelector:@selector(onUpaidApiBuyTransactionRejectedLimit:)]) {
        [self.delegate performSelector:@selector(onUpaidApiBuyTransactionRejectedLimit:) withObject: result];
      } else if (result.status == kUpaidApiBuyCardBlocked && [self.delegate respondsToSelector:@selector(onUpaidApiBuyCardBlocked:)]) {
        [self.delegate performSelector:@selector(onUpaidApiBuyCardBlocked:) withObject: result];
      } else if (result.status == kUpaidApiBuySameTransaction && [self.delegate respondsToSelector:@selector(onUpaidApiBuySameTransaction:)]) {
        [self.delegate performSelector:@selector(onUpaidApiBuySameTransaction:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiBuyFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiBuyFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiBuyResult *) result  {
    UpaidLog(@"Buy result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiBuyResult *result = [[UpaidApiBuyResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiBuyFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiBuyFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"Buy" withStatus:@"fail"];
    }
}

@end
