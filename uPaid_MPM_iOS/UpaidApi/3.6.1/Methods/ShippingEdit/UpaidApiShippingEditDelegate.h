//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiShippingEditDelegate <NSObject>

- (void) onUpaidApiShippingEditSuccess: (UpaidApiShippingEditResult *) result;
- (void) onUpaidApiShippingEditFail: (UpaidApiShippingEditResult *) result;

@optional
- (void) onUpaidApiShippingEditOperationFailed: (UpaidApiShippingEditResult *) result;
- (void) onUpaidApiShippingEditAddressNotFound: (UpaidApiShippingEditResult *) result;
- (void) onUpaidApiShippingEditTechnicalError: (UpaidApiShippingEditResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiShippingEditDelegate implementation

- (void) onUpaidApiShippingEditSuccess: (UpaidApiShippingEditResult *) result {

}

- (void) onUpaidApiShippingEditFail: (UpaidApiShippingEditResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiShippingEditDelegate implementation

- (void) onUpaidApiShippingEditSuccess: (UpaidApiShippingEditResult *) result {

}

- (void) onUpaidApiShippingEditFail: (UpaidApiShippingEditResult *) result {

}

- (void) onUpaidApiShippingEditOperationFailed: (UpaidApiShippingEditResult *) result {

}

- (void) onUpaidApiShippingEditAddressNotFound: (UpaidApiShippingEditResult *) result {

}

- (void) onUpaidApiShippingEditTechnicalError: (UpaidApiShippingEditResult *) result {

}


//-------END SELECT-------
*/
@end
