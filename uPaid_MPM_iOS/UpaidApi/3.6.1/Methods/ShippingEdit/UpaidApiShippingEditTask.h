//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiShippingEditData.h"
#import "UpaidApiShippingEditResult.h"
#import "UpaidApiShippingEditDelegate.h"

@interface UpaidApiShippingEditTask : UpaidApiTask

- (id) initWithData: (UpaidApiShippingEditData *) taskData andDelegate: (id<UpaidApiShippingEditDelegate>) taskDelegate;

enum UpaidApiShippingEditStatuses : NSUInteger{
  kUpaidApiShippingEditSuccess = 1,
  kUpaidApiShippingEditOperationFailed = 2,
  kUpaidApiShippingEditAddressNotFound = 3,
  kUpaidApiShippingEditTechnicalError = 7,
};
        
@end
