//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiShippingEditTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "AddressLong.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiAddressLong.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "singleShoppingCartItem.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiShippingEditTask ()

@property UpaidApiShippingEditData *data;
@property id<UpaidApiShippingEditDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiShippingEditTask


- (id) initWithData: (UpaidApiShippingEditData *) taskData andDelegate: (id<UpaidApiShippingEditDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"ShippingEdit data is empty.");
  } else {
    UpaidLog(@"ShippingEdit data:\n%@", self.data);
  }
  
  [self.proxy shippingEdit: [UpaidApi sessionToken] : (AddressLong *)[self.data.address wsClass] ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiShippingEditResult *result = [[UpaidApiShippingEditResult alloc] init];
  result.status = kUpaidApiShippingEditTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiShippingEditTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiShippingEditTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiShippingEditFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiShippingEditFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiShippingEditResult *result = [[UpaidApiShippingEditResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiShippingEditSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiShippingEditSuccess:)]) {

        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiShippingEditSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiShippingEditOperationFailed && [self.delegate respondsToSelector:@selector(onUpaidApiShippingEditOperationFailed:)]) {
        [self.delegate performSelector:@selector(onUpaidApiShippingEditOperationFailed:) withObject: result];
      } else if (result.status == kUpaidApiShippingEditAddressNotFound && [self.delegate respondsToSelector:@selector(onUpaidApiShippingEditAddressNotFound:)]) {
        [self.delegate performSelector:@selector(onUpaidApiShippingEditAddressNotFound:) withObject: result];
      } else if (result.status == kUpaidApiShippingEditTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiShippingEditTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiShippingEditTechnicalError:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiShippingEditFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiShippingEditFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiShippingEditResult *) result  {
    UpaidLog(@"ShippingEdit result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiShippingEditResult *result = [[UpaidApiShippingEditResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiShippingEditFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiShippingEditFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"ShippingEdit" withStatus:@"fail"];
    }
}

@end
