//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiShippingEditResult.h"
#import "UpaidApiShippingEditTask.h"
#import "AddressLong.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiAddressLong.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "singleShoppingCartItem.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiShippingEditResult ()


@end

@implementation UpaidApiShippingEditResult

@synthesize status = _status;

- (id) init {
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

- (void) setStatus: (NSInteger) status {
  _status = status;
  
  if (self.status == kUpaidApiShippingEditSuccess) {
    self.statusDescription = @"";
  }

  if (self.status == kUpaidApiShippingEditOperationFailed) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.ShippingEdit.OperationFailed", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiShippingEditAddressNotFound) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.ShippingEdit.AddressNotFound", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiShippingEditTechnicalError) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.ShippingEdit.TechnicalError", @"UpaidApi", nil);
  }

}

- (NSString *) description {
    return [NSString stringWithFormat: @"status=%ld\n%@", (long)self.status, @""];
}

@end
