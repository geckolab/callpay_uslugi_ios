//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiAddressLong.h"

@interface UpaidApiShippingEditData : NSObject

@property UpaidApiAddressLong *address; 


//only required params
- (id) initWithAddress: (UpaidApiAddressLong *) address;


@end