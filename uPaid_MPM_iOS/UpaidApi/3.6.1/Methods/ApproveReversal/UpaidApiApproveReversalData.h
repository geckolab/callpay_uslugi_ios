//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpaidApiApproveReversalData : NSObject

@property NSString *transactionId; 
@property BOOL isUpaid; 


//only required params
- (id) initWithTransactionId: (NSString *) transactionId andIsUpaid: (BOOL ) isUpaid;


@end