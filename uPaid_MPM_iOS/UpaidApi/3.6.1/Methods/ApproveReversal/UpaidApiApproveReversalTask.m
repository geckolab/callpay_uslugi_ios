//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiApproveReversalTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiApproveReversalTask ()

@property UpaidApiApproveReversalData *data;
@property id<UpaidApiApproveReversalDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiApproveReversalTask


- (id) initWithData: (UpaidApiApproveReversalData *) taskData andDelegate: (id<UpaidApiApproveReversalDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"ApproveReversal data is empty.");
  } else {
    UpaidLog(@"ApproveReversal data:\n%@", self.data);
  }
  
  [self.proxy approveReversal: [UpaidApi sessionToken] : self.data.transactionId : self.data.isUpaid ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiApproveReversalResult *result = [[UpaidApiApproveReversalResult alloc] init];
  result.status = kUpaidApiApproveReversalTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiApproveReversalTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiApproveReversalTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiApproveReversalFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiApproveReversalFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiApproveReversalResult *result = [[UpaidApiApproveReversalResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiApproveReversalSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiApproveReversalSuccess:)]) {

        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiApproveReversalSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiApproveReversalTransactionRejected && [self.delegate respondsToSelector:@selector(onUpaidApiApproveReversalTransactionRejected:)]) {
        [self.delegate performSelector:@selector(onUpaidApiApproveReversalTransactionRejected:) withObject: result];
      } else if (result.status == kUpaidApiApproveReversalTransactionNotFound && [self.delegate respondsToSelector:@selector(onUpaidApiApproveReversalTransactionNotFound:)]) {
        [self.delegate performSelector:@selector(onUpaidApiApproveReversalTransactionNotFound:) withObject: result];
      } else if (result.status == kUpaidApiApproveReversalTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiApproveReversalTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiApproveReversalTechnicalError:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiApproveReversalFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiApproveReversalFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiApproveReversalResult *) result  {
    UpaidLog(@"ApproveReversal result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiApproveReversalResult *result = [[UpaidApiApproveReversalResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiApproveReversalFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiApproveReversalFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"ApproveReversal" withStatus:@"fail"];
    }
}

@end
