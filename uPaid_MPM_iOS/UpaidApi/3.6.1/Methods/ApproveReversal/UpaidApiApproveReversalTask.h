//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiApproveReversalData.h"
#import "UpaidApiApproveReversalResult.h"
#import "UpaidApiApproveReversalDelegate.h"

@interface UpaidApiApproveReversalTask : UpaidApiTask

- (id) initWithData: (UpaidApiApproveReversalData *) taskData andDelegate: (id<UpaidApiApproveReversalDelegate>) taskDelegate;

enum UpaidApiApproveReversalStatuses : NSUInteger{
  kUpaidApiApproveReversalSuccess = 1,
  kUpaidApiApproveReversalTransactionRejected = 2,
  kUpaidApiApproveReversalTransactionNotFound = 3,
  kUpaidApiApproveReversalTechnicalError = 7,
};
        
@end
