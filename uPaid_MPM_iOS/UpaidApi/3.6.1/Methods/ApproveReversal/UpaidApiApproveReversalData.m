//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiApproveReversalData.h"
#import "NSObject+WSClass.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@implementation UpaidApiApproveReversalData

- (id) initWithTransactionId: (NSString *) transactionId andIsUpaid: (BOOL ) isUpaid {
  self = [super init];
  
  if (self) {
    self.transactionId = transactionId;
    self.isUpaid = isUpaid;
  }
  
  return self;
}



- (NSString *) description {
    return [NSString stringWithFormat: @"transactionId=%@\nisUpaid=%@", self.transactionId, [NSString stringWithFormat: @"%d", self.isUpaid]];
}

@end