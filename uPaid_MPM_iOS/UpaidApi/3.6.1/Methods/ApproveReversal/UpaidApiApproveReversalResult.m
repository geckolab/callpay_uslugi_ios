//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiApproveReversalResult.h"
#import "UpaidApiApproveReversalTask.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@implementation UpaidApiApproveReversalResult

@synthesize status = _status;

- (id) init {
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

- (void) setStatus: (NSInteger) status {
  _status = status;
  
  if (self.status == kUpaidApiApproveReversalSuccess) {
    self.statusDescription = @"";
  }

  if (self.status == kUpaidApiApproveReversalTransactionRejected) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.ApproveReversal.TransactionRejected", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiApproveReversalTransactionNotFound) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.ApproveReversal.TransactionNotFound", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiApproveReversalTechnicalError) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.ApproveReversal.TechnicalError", @"UpaidApi", nil);
  }
}

- (NSString *) description {
    return [NSString stringWithFormat: @"status=%ld\n%@", (long)self.status, @""];
}

@end
