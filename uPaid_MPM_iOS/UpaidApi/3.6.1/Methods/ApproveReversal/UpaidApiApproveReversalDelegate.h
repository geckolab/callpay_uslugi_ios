//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiApproveReversalDelegate <NSObject>

- (void) onUpaidApiApproveReversalSuccess: (UpaidApiApproveReversalResult *) result;
- (void) onUpaidApiApproveReversalFail: (UpaidApiApproveReversalResult *) result;

@optional
- (void) onUpaidApiApproveReversalTransactionRejected: (UpaidApiApproveReversalResult *) result;
- (void) onUpaidApiApproveReversalTransactionNotFound: (UpaidApiApproveReversalResult *) result;
- (void) onUpaidApiApproveReversalTechnicalError: (UpaidApiApproveReversalResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiApproveReversalDelegate implementation

- (void) onUpaidApiApproveReversalSuccess: (UpaidApiApproveReversalResult *) result {

}

- (void) onUpaidApiApproveReversalFail: (UpaidApiApproveReversalResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiApproveReversalDelegate implementation

- (void) onUpaidApiApproveReversalSuccess: (UpaidApiApproveReversalResult *) result {

}

- (void) onUpaidApiApproveReversalFail: (UpaidApiApproveReversalResult *) result {

}

- (void) onUpaidApiApproveReversalTransactionRejected: (UpaidApiApproveReversalResult *) result {

}

- (void) onUpaidApiApproveReversalTransactionNotFound: (UpaidApiApproveReversalResult *) result {

}

- (void) onUpaidApiApproveReversalTechnicalError: (UpaidApiApproveReversalResult *) result {

}


//-------END SELECT-------
*/
@end
