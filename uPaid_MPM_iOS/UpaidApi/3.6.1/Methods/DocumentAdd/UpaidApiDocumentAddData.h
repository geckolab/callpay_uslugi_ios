//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiExtrasSingleParam.h"
#import "UpaidApiExtrasSingleParamArray.h"

@interface UpaidApiDocumentAddData : NSObject

@property UpaidApiExtrasSingleParamArray *photos; 
@property NSString *name; 
@property NSString *type; 
@property NSString *barcode; 


//only required params
- (id) initWithPhotos: (UpaidApiExtrasSingleParamArray *) photos andName: (NSString *) name andType: (NSString *) type andBarcode: (NSString *) barcode;


@end