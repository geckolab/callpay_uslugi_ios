//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiDocumentAddTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiDocumentAddTask ()

@property UpaidApiDocumentAddData *data;
@property id<UpaidApiDocumentAddDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiDocumentAddTask


- (id) initWithData: (UpaidApiDocumentAddData *) taskData andDelegate: (id<UpaidApiDocumentAddDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"DocumentAdd data is empty.");
  } else {
    UpaidLog(@"DocumentAdd data:\n%@", self.data);
  }
  
  [self.proxy documentAdd: [UpaidApi sessionToken] : (NSMutableArray *)[self.data.photos wsClass] : self.data.name : self.data.type : self.data.barcode ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiDocumentAddResult *result = [[UpaidApiDocumentAddResult alloc] init];
  result.status = kUpaidApiDocumentAddTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiDocumentAddTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiDocumentAddTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiDocumentAddFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiDocumentAddFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiDocumentAddResult *result = [[UpaidApiDocumentAddResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiDocumentAddSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiDocumentAddSuccess:)]) {

        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiDocumentAddSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiDocumentAddOperationFailed && [self.delegate respondsToSelector:@selector(onUpaidApiDocumentAddOperationFailed:)]) {
        [self.delegate performSelector:@selector(onUpaidApiDocumentAddOperationFailed:) withObject: result];
      } else if (result.status == kUpaidApiDocumentAddFiletypeIncorrect && [self.delegate respondsToSelector:@selector(onUpaidApiDocumentAddFiletypeIncorrect:)]) {
        [self.delegate performSelector:@selector(onUpaidApiDocumentAddFiletypeIncorrect:) withObject: result];
      } else if (result.status == kUpaidApiDocumentAddDocumentTypeIncorrect && [self.delegate respondsToSelector:@selector(onUpaidApiDocumentAddDocumentTypeIncorrect:)]) {
        [self.delegate performSelector:@selector(onUpaidApiDocumentAddDocumentTypeIncorrect:) withObject: result];
      } else if (result.status == kUpaidApiDocumentAddTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiDocumentAddTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiDocumentAddTechnicalError:) withObject: result];
      } else if (result.status == kUpaidApiDocumentAddDocumentNameIncorrect && [self.delegate respondsToSelector:@selector(onUpaidApiDocumentAddDocumentNameIncorrect:)]) {
        [self.delegate performSelector:@selector(onUpaidApiDocumentAddDocumentNameIncorrect:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiDocumentAddFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiDocumentAddFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiDocumentAddResult *) result  {
    UpaidLog(@"DocumentAdd result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiDocumentAddResult *result = [[UpaidApiDocumentAddResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiDocumentAddFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiDocumentAddFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"DocumentAdd" withStatus:@"fail"];
    }
}

@end
