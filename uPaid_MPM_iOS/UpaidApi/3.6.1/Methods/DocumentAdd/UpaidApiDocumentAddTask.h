//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiDocumentAddData.h"
#import "UpaidApiDocumentAddResult.h"
#import "UpaidApiDocumentAddDelegate.h"

@interface UpaidApiDocumentAddTask : UpaidApiTask

- (id) initWithData: (UpaidApiDocumentAddData *) taskData andDelegate: (id<UpaidApiDocumentAddDelegate>) taskDelegate;

enum UpaidApiDocumentAddStatuses : NSUInteger{
  kUpaidApiDocumentAddSuccess = 1,
  kUpaidApiDocumentAddOperationFailed = 2,
  kUpaidApiDocumentAddFiletypeIncorrect = 3,
  kUpaidApiDocumentAddDocumentTypeIncorrect = 6,
  kUpaidApiDocumentAddTechnicalError = 7,
  kUpaidApiDocumentAddDocumentNameIncorrect = 8,
};
        
@end
