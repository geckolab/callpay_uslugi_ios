//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiDocumentAddResult.h"
#import "UpaidApiDocumentAddTask.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@implementation UpaidApiDocumentAddResult

@synthesize status = _status;

- (id) init {
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

- (void) setStatus: (NSInteger) status {
  _status = status;
  
  if (self.status == kUpaidApiDocumentAddSuccess) {
    self.statusDescription = @"";
  }

  if (self.status == kUpaidApiDocumentAddOperationFailed) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.DocumentAdd.OperationFailed", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiDocumentAddFiletypeIncorrect) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.DocumentAdd.FiletypeIncorrect", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiDocumentAddDocumentTypeIncorrect) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.DocumentAdd.DocumentTypeIncorrect", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiDocumentAddTechnicalError) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.DocumentAdd.TechnicalError", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiDocumentAddDocumentNameIncorrect) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.DocumentAdd.DocumentNameIncorrect", @"UpaidApi", nil);
  }
}

- (NSString *) description {
    return [NSString stringWithFormat: @"status=%ld\n%@", (long)self.status, @""];
}

@end
