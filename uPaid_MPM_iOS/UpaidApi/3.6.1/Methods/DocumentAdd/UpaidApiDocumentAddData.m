//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiDocumentAddData.h"
#import "NSObject+WSClass.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@implementation UpaidApiDocumentAddData

- (id) initWithPhotos: (UpaidApiExtrasSingleParamArray *) photos andName: (NSString *) name andType: (NSString *) type andBarcode: (NSString *) barcode {
  self = [super init];
  
  if (self) {
    self.photos = photos;
    self.name = name;
    self.type = type;
    self.barcode = barcode;
  }
  
  return self;
}



- (NSString *) description {
    return [NSString stringWithFormat: @"photos=%@\nname=%@\ntype=%@\nbarcode=%@", self.photos, self.name, self.type, self.barcode];
}

@end