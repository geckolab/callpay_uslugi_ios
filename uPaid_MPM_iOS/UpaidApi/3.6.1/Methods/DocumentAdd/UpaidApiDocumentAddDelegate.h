//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiDocumentAddDelegate <NSObject>

- (void) onUpaidApiDocumentAddSuccess: (UpaidApiDocumentAddResult *) result;
- (void) onUpaidApiDocumentAddFail: (UpaidApiDocumentAddResult *) result;

@optional
- (void) onUpaidApiDocumentAddOperationFailed: (UpaidApiDocumentAddResult *) result;
- (void) onUpaidApiDocumentAddFiletypeIncorrect: (UpaidApiDocumentAddResult *) result;
- (void) onUpaidApiDocumentAddDocumentTypeIncorrect: (UpaidApiDocumentAddResult *) result;
- (void) onUpaidApiDocumentAddTechnicalError: (UpaidApiDocumentAddResult *) result;
- (void) onUpaidApiDocumentAddDocumentNameIncorrect: (UpaidApiDocumentAddResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiDocumentAddDelegate implementation

- (void) onUpaidApiDocumentAddSuccess: (UpaidApiDocumentAddResult *) result {

}

- (void) onUpaidApiDocumentAddFail: (UpaidApiDocumentAddResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiDocumentAddDelegate implementation

- (void) onUpaidApiDocumentAddSuccess: (UpaidApiDocumentAddResult *) result {

}

- (void) onUpaidApiDocumentAddFail: (UpaidApiDocumentAddResult *) result {

}

- (void) onUpaidApiDocumentAddOperationFailed: (UpaidApiDocumentAddResult *) result {

}

- (void) onUpaidApiDocumentAddFiletypeIncorrect: (UpaidApiDocumentAddResult *) result {

}

- (void) onUpaidApiDocumentAddDocumentTypeIncorrect: (UpaidApiDocumentAddResult *) result {

}

- (void) onUpaidApiDocumentAddTechnicalError: (UpaidApiDocumentAddResult *) result {

}

- (void) onUpaidApiDocumentAddDocumentNameIncorrect: (UpaidApiDocumentAddResult *) result {

}


//-------END SELECT-------
*/
@end
