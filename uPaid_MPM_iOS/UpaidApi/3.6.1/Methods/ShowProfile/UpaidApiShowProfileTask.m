//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiShowProfileTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiShowProfileTask ()

@property UpaidApiShowProfileData *data;
@property id<UpaidApiShowProfileDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiShowProfileTask


- (id) initWithData: (UpaidApiShowProfileData *) taskData andDelegate: (id<UpaidApiShowProfileDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"ShowProfile data is empty.");
  } else {
    UpaidLog(@"ShowProfile data:\n%@", self.data);
  }
  
  [self.proxy showProfile: [UpaidApi sessionToken] ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiShowProfileResult *result = [[UpaidApiShowProfileResult alloc] init];
  result.status = kUpaidApiShowProfileTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiShowProfileTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiShowProfileTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiShowProfileFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiShowProfileFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiShowProfileResult *result = [[UpaidApiShowProfileResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiShowProfileSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiShowProfileSuccess:)]) {
        if (data[@"profileData"] != nil) {
            showProfileData *wsObject = [[showProfileData alloc] initWithArray: data[@"profileData"]];
            result.profileData = [[UpaidApiShowProfile alloc] initWithWsClass: wsObject];
        }
        if (data[@"profile_data"] != nil) {
            showProfileData *wsObject = [[showProfileData alloc] initWithArray: data[@"profile_data"]];
            result.profileData = [[UpaidApiShowProfile alloc] initWithWsClass: wsObject];
        }


        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiShowProfileSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiShowProfileError && [self.delegate respondsToSelector:@selector(onUpaidApiShowProfileError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiShowProfileError:) withObject: result];
      } else if (result.status == kUpaidApiShowProfileTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiShowProfileTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiShowProfileTechnicalError:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiShowProfileFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiShowProfileFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiShowProfileResult *) result  {
    UpaidLog(@"ShowProfile result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiShowProfileResult *result = [[UpaidApiShowProfileResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiShowProfileFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiShowProfileFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"ShowProfile" withStatus:@"fail"];
    }
}

@end
