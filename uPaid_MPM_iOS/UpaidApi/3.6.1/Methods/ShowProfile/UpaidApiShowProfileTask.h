//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiShowProfileData.h"
#import "UpaidApiShowProfileResult.h"
#import "UpaidApiShowProfileDelegate.h"

@interface UpaidApiShowProfileTask : UpaidApiTask

- (id) initWithData: (UpaidApiShowProfileData *) taskData andDelegate: (id<UpaidApiShowProfileDelegate>) taskDelegate;

enum UpaidApiShowProfileStatuses : NSUInteger{
  kUpaidApiShowProfileSuccess = 1,
  kUpaidApiShowProfileError = 2,
  kUpaidApiShowProfileTechnicalError = 7,
};
        
@end
