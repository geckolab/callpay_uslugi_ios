//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiShowProfileDelegate <NSObject>

- (void) onUpaidApiShowProfileSuccess: (UpaidApiShowProfileResult *) result;
- (void) onUpaidApiShowProfileFail: (UpaidApiShowProfileResult *) result;

@optional
- (void) onUpaidApiShowProfileError: (UpaidApiShowProfileResult *) result;
- (void) onUpaidApiShowProfileTechnicalError: (UpaidApiShowProfileResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiShowProfileDelegate implementation

- (void) onUpaidApiShowProfileSuccess: (UpaidApiShowProfileResult *) result {

}

- (void) onUpaidApiShowProfileFail: (UpaidApiShowProfileResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiShowProfileDelegate implementation

- (void) onUpaidApiShowProfileSuccess: (UpaidApiShowProfileResult *) result {

}

- (void) onUpaidApiShowProfileFail: (UpaidApiShowProfileResult *) result {

}

- (void) onUpaidApiShowProfileError: (UpaidApiShowProfileResult *) result {

}

- (void) onUpaidApiShowProfileTechnicalError: (UpaidApiShowProfileResult *) result {

}


//-------END SELECT-------
*/
@end
