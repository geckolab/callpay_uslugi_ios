//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiCardEditData.h"
#import "NSObject+WSClass.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@implementation UpaidApiCardEditData

- (id) initWithCardId: (int ) cardId andExpMonth: (int ) expMonth andExpYear: (int ) expYear andNickname: (NSString *) nickname andPan: (NSString *) pan {
  self = [super init];
  
  if (self) {
    self.cardId = cardId;
    self.expMonth = expMonth;
    self.expYear = expYear;
    self.nickname = nickname;
    self.pan = pan;
  }
  
  return self;
}



- (NSString *) description {
    return [NSString stringWithFormat: @"cardId=%@\nexpMonth=%@\nexpYear=%@\nnickname=%@\npan=%@", [NSString stringWithFormat: @"%d", self.cardId], [NSString stringWithFormat: @"%d", self.expMonth], [NSString stringWithFormat: @"%d", self.expYear], self.nickname, self.pan];
}

@end