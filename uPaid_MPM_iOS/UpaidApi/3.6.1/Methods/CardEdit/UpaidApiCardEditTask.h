//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiCardEditData.h"
#import "UpaidApiCardEditResult.h"
#import "UpaidApiCardEditDelegate.h"

@interface UpaidApiCardEditTask : UpaidApiTask

- (id) initWithData: (UpaidApiCardEditData *) taskData andDelegate: (id<UpaidApiCardEditDelegate>) taskDelegate;

enum UpaidApiCardEditStatuses : NSUInteger{
  kUpaidApiCardEditSuccess = 1,
  kUpaidApiCardEditIncorrectData = 2,
  kUpaidApiCardEditCardNotFound = 3,
  kUpaidApiCardEditTechnicalError = 7,
};
        
@end
