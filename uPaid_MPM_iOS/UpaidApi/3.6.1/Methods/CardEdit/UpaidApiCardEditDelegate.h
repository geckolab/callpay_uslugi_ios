//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiCardEditDelegate <NSObject>

- (void) onUpaidApiCardEditSuccess: (UpaidApiCardEditResult *) result;
- (void) onUpaidApiCardEditFail: (UpaidApiCardEditResult *) result;

@optional
- (void) onUpaidApiCardEditIncorrectData: (UpaidApiCardEditResult *) result;
- (void) onUpaidApiCardEditCardNotFound: (UpaidApiCardEditResult *) result;
- (void) onUpaidApiCardEditTechnicalError: (UpaidApiCardEditResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiCardEditDelegate implementation

- (void) onUpaidApiCardEditSuccess: (UpaidApiCardEditResult *) result {

}

- (void) onUpaidApiCardEditFail: (UpaidApiCardEditResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiCardEditDelegate implementation

- (void) onUpaidApiCardEditSuccess: (UpaidApiCardEditResult *) result {

}

- (void) onUpaidApiCardEditFail: (UpaidApiCardEditResult *) result {

}

- (void) onUpaidApiCardEditIncorrectData: (UpaidApiCardEditResult *) result {

}

- (void) onUpaidApiCardEditCardNotFound: (UpaidApiCardEditResult *) result {

}

- (void) onUpaidApiCardEditTechnicalError: (UpaidApiCardEditResult *) result {

}


//-------END SELECT-------
*/
@end
