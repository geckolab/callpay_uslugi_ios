//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiCardEditTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiCardEditTask ()

@property UpaidApiCardEditData *data;
@property id<UpaidApiCardEditDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiCardEditTask


- (id) initWithData: (UpaidApiCardEditData *) taskData andDelegate: (id<UpaidApiCardEditDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"CardEdit data is empty.");
  } else {
    UpaidLog(@"CardEdit data:\n%@", self.data);
  }
  
  [self.proxy cardEdit: [UpaidApi sessionToken] : [NSString stringWithFormat: @"%d", self.data.cardId] : [self expDate] : self.data.nickname : self.data.pan ];
}

- (NSString *) expDate {
  if (self.data.expYear > 100) {
    int x = self.data.expYear / 100;
    self.data.expYear -= x * 100;
  }
  
  return [NSString stringWithFormat: @"%d/%d", self.data.expMonth, self.data.expYear];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiCardEditResult *result = [[UpaidApiCardEditResult alloc] init];
  result.status = kUpaidApiCardEditTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiCardEditTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiCardEditTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiCardEditFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiCardEditFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiCardEditResult *result = [[UpaidApiCardEditResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiCardEditSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiCardEditSuccess:)]) {

        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiCardEditSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiCardEditIncorrectData && [self.delegate respondsToSelector:@selector(onUpaidApiCardEditIncorrectData:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardEditIncorrectData:) withObject: result];
      } else if (result.status == kUpaidApiCardEditCardNotFound && [self.delegate respondsToSelector:@selector(onUpaidApiCardEditCardNotFound:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardEditCardNotFound:) withObject: result];
      } else if (result.status == kUpaidApiCardEditTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiCardEditTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardEditTechnicalError:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiCardEditFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardEditFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiCardEditResult *) result  {
    UpaidLog(@"CardEdit result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiCardEditResult *result = [[UpaidApiCardEditResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiCardEditFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardEditFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"CardEdit" withStatus:@"fail"];
    }
}

@end
