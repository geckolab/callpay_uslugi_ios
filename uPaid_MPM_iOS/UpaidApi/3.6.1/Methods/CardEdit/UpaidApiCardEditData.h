//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpaidApiCardEditData : NSObject

@property int cardId; 
@property int expMonth; 
@property int expYear; 
@property NSString *nickname; 
@property NSString *pan; 


//only required params
- (id) initWithCardId: (int ) cardId andExpMonth: (int ) expMonth andExpYear: (int ) expYear andNickname: (NSString *) nickname andPan: (NSString *) pan;


@end