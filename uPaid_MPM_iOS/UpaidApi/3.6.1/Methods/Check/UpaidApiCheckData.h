//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpaidApiCheckData : NSObject

@property NSString *phone; 


//only required params
- (id) initWithPhone: (NSString *) phone;


@end