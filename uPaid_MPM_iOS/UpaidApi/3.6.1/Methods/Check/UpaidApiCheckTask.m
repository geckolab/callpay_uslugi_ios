//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiCheckTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiCheckTask ()

@property UpaidApiCheckData *data;
@property id<UpaidApiCheckDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiCheckTask


- (id) initWithData: (UpaidApiCheckData *) taskData andDelegate: (id<UpaidApiCheckDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"Check data is empty.");
  } else {
    UpaidLog(@"Check data:\n%@", self.data);
  }
  
  [self.proxy check: [UpaidApi partnerId] : self.data.phone ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiCheckResult *result = [[UpaidApiCheckResult alloc] init];
  result.status = kUpaidApiCheckTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiCheckTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiCheckTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiCheckFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiCheckFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiCheckResult *result = [[UpaidApiCheckResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiCheckSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiCheckSuccess:)]) {
        result.isPhoneLoggedBefore = [UpaidApi phoneLoggedBefore: self.data.phone];
        result.smsCode = [UpaidApi smsCodeForPhone: self.data.phone];
        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiCheckSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiCheckNoUser && [self.delegate respondsToSelector:@selector(onUpaidApiCheckNoUser:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCheckNoUser:) withObject: result];
      } else if (result.status == kUpaidApiCheckTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiCheckTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCheckTechnicalError:) withObject: result];
      } else if (result.status == kUpaidApiCheckUnsupportedVersion && [self.delegate respondsToSelector:@selector(onUpaidApiCheckUnsupportedVersion:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCheckUnsupportedVersion:) withObject: result];
      } else if (result.status == kUpaidApiCheckBadPartnerId && [self.delegate respondsToSelector:@selector(onUpaidApiCheckBadPartnerId:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCheckBadPartnerId:) withObject: result];
      } else if (result.status == kUpaidApiCheckBadPlatform && [self.delegate respondsToSelector:@selector(onUpaidApiCheckBadPlatform:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCheckBadPlatform:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiCheckFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCheckFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiCheckResult *) result  {
    UpaidLog(@"Check result:\n%@", result);
}


@end
