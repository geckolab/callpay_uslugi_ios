//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiCheckResult.h"
#import "UpaidApiCheckTask.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@implementation UpaidApiCheckResult

@synthesize status = _status;

- (id) init {
    self = [super init];
    
    if (self) {
        self.isPhoneLoggedBefore = NO;
        self.smsCode = @"";
    }
    
    return self;
}

- (void) setStatus: (NSInteger) status {
  _status = status;
  
  if (self.status == kUpaidApiCheckSuccess) {
    self.statusDescription = @"";
  }

  if (self.status == kUpaidApiCheckNoUser) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Check.NoUser", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiCheckTechnicalError) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Check.TechnicalError", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiCheckUnsupportedVersion) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Check.UnsupportedVersion", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiCheckBadPartnerId) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Check.BadPartnerId", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiCheckBadPlatform) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Check.BadPlatform", @"UpaidApi", nil);
  }
}

- (NSString *) description {
    return [NSString stringWithFormat: @"status=%ld\n%@", (long)self.status, @""];
}

@end
