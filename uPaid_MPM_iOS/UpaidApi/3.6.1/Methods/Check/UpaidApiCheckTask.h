//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiCheckData.h"
#import "UpaidApiCheckResult.h"
#import "UpaidApiCheckDelegate.h"

@interface UpaidApiCheckTask : UpaidApiTask

- (id) initWithData: (UpaidApiCheckData *) taskData andDelegate: (id<UpaidApiCheckDelegate>) taskDelegate;

enum UpaidApiCheckStatuses : NSUInteger{
  kUpaidApiCheckSuccess = 1,
  kUpaidApiCheckNoUser = 2,
  kUpaidApiCheckTechnicalError = 7,
  kUpaidApiCheckUnsupportedVersion = 9,
  kUpaidApiCheckBadPartnerId = 721,
  kUpaidApiCheckBadPlatform = 722,
};
        
@end
