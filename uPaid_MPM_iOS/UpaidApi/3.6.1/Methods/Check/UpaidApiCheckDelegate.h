//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiCheckDelegate <NSObject>

- (void) onUpaidApiCheckSuccess: (UpaidApiCheckResult *) result;
- (void) onUpaidApiCheckFail: (UpaidApiCheckResult *) result;

@optional
- (void) onUpaidApiCheckNoUser: (UpaidApiCheckResult *) result;
- (void) onUpaidApiCheckTechnicalError: (UpaidApiCheckResult *) result;
- (void) onUpaidApiCheckUnsupportedVersion: (UpaidApiCheckResult *) result;
- (void) onUpaidApiCheckBadPartnerId: (UpaidApiCheckResult *) result;
- (void) onUpaidApiCheckBadPlatform: (UpaidApiCheckResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiCheckDelegate implementation

- (void) onUpaidApiCheckSuccess: (UpaidApiCheckResult *) result {

}

- (void) onUpaidApiCheckFail: (UpaidApiCheckResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiCheckDelegate implementation

- (void) onUpaidApiCheckSuccess: (UpaidApiCheckResult *) result {

}

- (void) onUpaidApiCheckFail: (UpaidApiCheckResult *) result {

}

- (void) onUpaidApiCheckNoUser: (UpaidApiCheckResult *) result {

}

- (void) onUpaidApiCheckTechnicalError: (UpaidApiCheckResult *) result {

}

- (void) onUpaidApiCheckUnsupportedVersion: (UpaidApiCheckResult *) result {

}

- (void) onUpaidApiCheckBadPartnerId: (UpaidApiCheckResult *) result {

}

- (void) onUpaidApiCheckBadPlatform: (UpaidApiCheckResult *) result {

}


//-------END SELECT-------
*/
@end
