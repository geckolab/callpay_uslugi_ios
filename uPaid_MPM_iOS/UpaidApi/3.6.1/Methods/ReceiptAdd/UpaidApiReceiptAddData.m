//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiReceiptAddData.h"
#import "NSObject+WSClass.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@implementation UpaidApiReceiptAddData

- (id) initWithPhotos: (UpaidApiExtrasSingleParamArray *) photos andName: (NSString *) name andAmount: (int ) amount andCreated: (NSString *) created {
  self = [super init];
  
  if (self) {
    self.photos = photos;
    self.name = name;
    self.amount = amount;
    self.created = created;
  }
  
  return self;
}



- (NSString *) description {
    return [NSString stringWithFormat: @"photos=%@\nname=%@\namount=%@\ncreated=%@", self.photos, self.name, [NSString stringWithFormat: @"%d", self.amount], self.created];
}

@end