//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiReceiptAddData.h"
#import "UpaidApiReceiptAddResult.h"
#import "UpaidApiReceiptAddDelegate.h"

@interface UpaidApiReceiptAddTask : UpaidApiTask

- (id) initWithData: (UpaidApiReceiptAddData *) taskData andDelegate: (id<UpaidApiReceiptAddDelegate>) taskDelegate;

enum UpaidApiReceiptAddStatuses : NSUInteger{
  kUpaidApiReceiptAddSuccess = 1,
  kUpaidApiReceiptAddOperationFailed = 2,
  kUpaidApiReceiptAddFiletypeIncorrect = 3,
  kUpaidApiReceiptAddAmountIncorrect = 4,
  kUpaidApiReceiptAddTechnicalError = 7,
  kUpaidApiReceiptAddDateIncorrect = 8,
  kUpaidApiReceiptAddReceiptNameIncorrect = 9,
};
        
@end
