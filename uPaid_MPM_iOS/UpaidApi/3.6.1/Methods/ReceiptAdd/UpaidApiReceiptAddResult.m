//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiReceiptAddResult.h"
#import "UpaidApiReceiptAddTask.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@implementation UpaidApiReceiptAddResult

@synthesize status = _status;

- (id) init {
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

- (void) setStatus: (NSInteger) status {
  _status = status;
  
  if (self.status == kUpaidApiReceiptAddSuccess) {
    self.statusDescription = @"";
  }

  if (self.status == kUpaidApiReceiptAddOperationFailed) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.ReceiptAdd.OperationFailed", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiReceiptAddFiletypeIncorrect) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.ReceiptAdd.FiletypeIncorrect", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiReceiptAddAmountIncorrect) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.ReceiptAdd.AmountIncorrect", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiReceiptAddTechnicalError) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.ReceiptAdd.TechnicalError", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiReceiptAddDateIncorrect) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.ReceiptAdd.DateIncorrect", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiReceiptAddReceiptNameIncorrect) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.ReceiptAdd.ReceiptNameIncorrect", @"UpaidApi", nil);
  }
}

- (NSString *) description {
    return [NSString stringWithFormat: @"status=%ld\n%@", (long)self.status, @""];
}

@end
