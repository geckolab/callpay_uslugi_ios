//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiReceiptAddDelegate <NSObject>

- (void) onUpaidApiReceiptAddSuccess: (UpaidApiReceiptAddResult *) result;
- (void) onUpaidApiReceiptAddFail: (UpaidApiReceiptAddResult *) result;

@optional
- (void) onUpaidApiReceiptAddOperationFailed: (UpaidApiReceiptAddResult *) result;
- (void) onUpaidApiReceiptAddFiletypeIncorrect: (UpaidApiReceiptAddResult *) result;
- (void) onUpaidApiReceiptAddAmountIncorrect: (UpaidApiReceiptAddResult *) result;
- (void) onUpaidApiReceiptAddTechnicalError: (UpaidApiReceiptAddResult *) result;
- (void) onUpaidApiReceiptAddDateIncorrect: (UpaidApiReceiptAddResult *) result;
- (void) onUpaidApiReceiptAddReceiptNameIncorrect: (UpaidApiReceiptAddResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiReceiptAddDelegate implementation

- (void) onUpaidApiReceiptAddSuccess: (UpaidApiReceiptAddResult *) result {

}

- (void) onUpaidApiReceiptAddFail: (UpaidApiReceiptAddResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiReceiptAddDelegate implementation

- (void) onUpaidApiReceiptAddSuccess: (UpaidApiReceiptAddResult *) result {

}

- (void) onUpaidApiReceiptAddFail: (UpaidApiReceiptAddResult *) result {

}

- (void) onUpaidApiReceiptAddOperationFailed: (UpaidApiReceiptAddResult *) result {

}

- (void) onUpaidApiReceiptAddFiletypeIncorrect: (UpaidApiReceiptAddResult *) result {

}

- (void) onUpaidApiReceiptAddAmountIncorrect: (UpaidApiReceiptAddResult *) result {

}

- (void) onUpaidApiReceiptAddTechnicalError: (UpaidApiReceiptAddResult *) result {

}

- (void) onUpaidApiReceiptAddDateIncorrect: (UpaidApiReceiptAddResult *) result {

}

- (void) onUpaidApiReceiptAddReceiptNameIncorrect: (UpaidApiReceiptAddResult *) result {

}


//-------END SELECT-------
*/
@end
