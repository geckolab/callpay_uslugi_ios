//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiReceiptAddTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiReceiptAddTask ()

@property UpaidApiReceiptAddData *data;
@property id<UpaidApiReceiptAddDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiReceiptAddTask


- (id) initWithData: (UpaidApiReceiptAddData *) taskData andDelegate: (id<UpaidApiReceiptAddDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"ReceiptAdd data is empty.");
  } else {
    UpaidLog(@"ReceiptAdd data:\n%@", self.data);
  }
  
  [self.proxy receiptAdd: [UpaidApi sessionToken] : (NSMutableArray *)[self.data.photos wsClass] : self.data.name : [NSString stringWithFormat: @"%d", self.data.amount] : self.data.created ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiReceiptAddResult *result = [[UpaidApiReceiptAddResult alloc] init];
  result.status = kUpaidApiReceiptAddTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiReceiptAddTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiReceiptAddTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiReceiptAddFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiReceiptAddFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiReceiptAddResult *result = [[UpaidApiReceiptAddResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiReceiptAddSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiReceiptAddSuccess:)]) {

        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiReceiptAddSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiReceiptAddOperationFailed && [self.delegate respondsToSelector:@selector(onUpaidApiReceiptAddOperationFailed:)]) {
        [self.delegate performSelector:@selector(onUpaidApiReceiptAddOperationFailed:) withObject: result];
      } else if (result.status == kUpaidApiReceiptAddFiletypeIncorrect && [self.delegate respondsToSelector:@selector(onUpaidApiReceiptAddFiletypeIncorrect:)]) {
        [self.delegate performSelector:@selector(onUpaidApiReceiptAddFiletypeIncorrect:) withObject: result];
      } else if (result.status == kUpaidApiReceiptAddAmountIncorrect && [self.delegate respondsToSelector:@selector(onUpaidApiReceiptAddAmountIncorrect:)]) {
        [self.delegate performSelector:@selector(onUpaidApiReceiptAddAmountIncorrect:) withObject: result];
      } else if (result.status == kUpaidApiReceiptAddTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiReceiptAddTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiReceiptAddTechnicalError:) withObject: result];
      } else if (result.status == kUpaidApiReceiptAddDateIncorrect && [self.delegate respondsToSelector:@selector(onUpaidApiReceiptAddDateIncorrect:)]) {
        [self.delegate performSelector:@selector(onUpaidApiReceiptAddDateIncorrect:) withObject: result];
      } else if (result.status == kUpaidApiReceiptAddReceiptNameIncorrect && [self.delegate respondsToSelector:@selector(onUpaidApiReceiptAddReceiptNameIncorrect:)]) {
        [self.delegate performSelector:@selector(onUpaidApiReceiptAddReceiptNameIncorrect:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiReceiptAddFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiReceiptAddFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiReceiptAddResult *) result  {
    UpaidLog(@"ReceiptAdd result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiReceiptAddResult *result = [[UpaidApiReceiptAddResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiReceiptAddFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiReceiptAddFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"ReceiptAdd" withStatus:@"fail"];
    }
}

@end
