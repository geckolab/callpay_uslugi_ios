//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiGetPendingTransactionsDelegate <NSObject>

- (void) onUpaidApiGetPendingTransactionsSuccess: (UpaidApiGetPendingTransactionsResult *) result;
- (void) onUpaidApiGetPendingTransactionsFail: (UpaidApiGetPendingTransactionsResult *) result;

@optional
- (void) onUpaidApiGetPendingTransactionsNoTransactions: (UpaidApiGetPendingTransactionsResult *) result;
- (void) onUpaidApiGetPendingTransactionsTechnicalError: (UpaidApiGetPendingTransactionsResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiGetPendingTransactionsDelegate implementation

- (void) onUpaidApiGetPendingTransactionsSuccess: (UpaidApiGetPendingTransactionsResult *) result {

}

- (void) onUpaidApiGetPendingTransactionsFail: (UpaidApiGetPendingTransactionsResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiGetPendingTransactionsDelegate implementation

- (void) onUpaidApiGetPendingTransactionsSuccess: (UpaidApiGetPendingTransactionsResult *) result {

}

- (void) onUpaidApiGetPendingTransactionsFail: (UpaidApiGetPendingTransactionsResult *) result {

}

- (void) onUpaidApiGetPendingTransactionsNoTransactions: (UpaidApiGetPendingTransactionsResult *) result {

}

- (void) onUpaidApiGetPendingTransactionsTechnicalError: (UpaidApiGetPendingTransactionsResult *) result {

}


//-------END SELECT-------
*/
@end
