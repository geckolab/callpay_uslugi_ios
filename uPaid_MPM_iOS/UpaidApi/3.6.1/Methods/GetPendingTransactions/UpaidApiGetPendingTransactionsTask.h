//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiGetPendingTransactionsData.h"
#import "UpaidApiGetPendingTransactionsResult.h"
#import "UpaidApiGetPendingTransactionsDelegate.h"

@interface UpaidApiGetPendingTransactionsTask : UpaidApiTask

- (id) initWithData: (UpaidApiGetPendingTransactionsData *) taskData andDelegate: (id<UpaidApiGetPendingTransactionsDelegate>) taskDelegate;

enum UpaidApiGetPendingTransactionsStatuses : NSUInteger{
  kUpaidApiGetPendingTransactionsSuccess = 1,
  kUpaidApiGetPendingTransactionsNoTransactions = 0,
  kUpaidApiGetPendingTransactionsTechnicalError = 7,
};
        
@end
