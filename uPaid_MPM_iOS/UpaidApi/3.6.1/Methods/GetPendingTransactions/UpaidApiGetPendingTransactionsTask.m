//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiGetPendingTransactionsTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiGetPendingTransactionsTask ()

@property UpaidApiGetPendingTransactionsData *data;
@property id<UpaidApiGetPendingTransactionsDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiGetPendingTransactionsTask


- (id) initWithData: (UpaidApiGetPendingTransactionsData *) taskData andDelegate: (id<UpaidApiGetPendingTransactionsDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"GetPendingTransactions data is empty.");
  } else {
    UpaidLog(@"GetPendingTransactions data:\n%@", self.data);
  }
  
  [self.proxy getPendingTransactions: [UpaidApi sessionToken] ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiGetPendingTransactionsResult *result = [[UpaidApiGetPendingTransactionsResult alloc] init];
  result.status = kUpaidApiGetPendingTransactionsTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiGetPendingTransactionsTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiGetPendingTransactionsTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiGetPendingTransactionsFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiGetPendingTransactionsFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiGetPendingTransactionsResult *result = [[UpaidApiGetPendingTransactionsResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiGetPendingTransactionsSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiGetPendingTransactionsSuccess:)]) {
        if (data[@"transactions"] != nil) {
            result.transactions = [[UpaidApiTransactionArray alloc] init];

            for (int i = 0; i < [(NSDictionary *)data[@"transactions"] count]; i++) {
                transactionStatus *wsObject = [[transactionStatus alloc] initWithArray: [[data[@"transactions"] objectAtIndex: i] objectForKey:@"nodeChildArray"]];
                UpaidApiTransaction * object = [[UpaidApiTransaction alloc] initWithWsClass: wsObject];
                [object setIsPending: YES];
                [result.transactions addObject: object];
            }

        }


        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiGetPendingTransactionsSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiGetPendingTransactionsNoTransactions && [self.delegate respondsToSelector:@selector(onUpaidApiGetPendingTransactionsNoTransactions:)]) {
        [self.delegate performSelector:@selector(onUpaidApiGetPendingTransactionsNoTransactions:) withObject: result];
      } else if (result.status == kUpaidApiGetPendingTransactionsTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiGetPendingTransactionsTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiGetPendingTransactionsTechnicalError:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiGetPendingTransactionsFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiGetPendingTransactionsFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiGetPendingTransactionsResult *) result  {
    UpaidLog(@"GetPendingTransactions result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiGetPendingTransactionsResult *result = [[UpaidApiGetPendingTransactionsResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiGetPendingTransactionsFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiGetPendingTransactionsFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"GetPendingTransactions" withStatus:@"fail"];
    }
}

@end
