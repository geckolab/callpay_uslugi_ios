//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiLoginResult.h"
#import "UpaidApiLoginTask.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@implementation UpaidApiLoginResult

@synthesize status = _status;

- (id) init {
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

- (void) setStatus: (NSInteger) status {
  _status = status;
  
  if (self.status == kUpaidApiLoginSuccess) {
    self.statusDescription = @"";
  }

  if (self.status == kUpaidApiLoginBadCode) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Login.BadCode", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiLoginBannedUser) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Login.BannedUser", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiLoginNoUser) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Login.NoUser", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiLoginTechnicalError) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Login.TechnicalError", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiLoginUnsupportedVersion) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Login.UnsupportedVersion", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiLoginBadPartnerId) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Login.BadPartnerId", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiLoginBadPlatform) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Login.BadPlatform", @"UpaidApi", nil);
  }
}

- (NSString *) description {
    return [NSString stringWithFormat: @"status=%ld\n%@", (long)self.status, [NSString stringWithFormat: @"termsAndConditions=%@", [NSString stringWithFormat: @"%d", self.termsAndConditions]]];
}

@end
