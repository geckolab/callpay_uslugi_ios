//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiLoginTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiLoginTask ()

@property UpaidApiLoginData *data;
@property id<UpaidApiLoginDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiLoginTask


- (id) initWithData: (UpaidApiLoginData *) taskData andDelegate: (id<UpaidApiLoginDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"Login data is empty.");
  } else {
    UpaidLog(@"Login data:\n%@", self.data);
  }
  
  [self.proxy login: [UpaidApi partnerId] : self.data.phone : self.data.code : [self extras]];
}

- (NSMutableArray *) extras {
  NSMutableArray *extras = [NSMutableArray array];
  
  if ([UpaidApi deviceToken] != nil && ![[UpaidApi deviceToken] isEqualToString:@""]) {
    SingleParam *param = [[SingleParam alloc] init];
    [param setKey:@"device_token"];
    [param setValue:[UpaidApi deviceToken]];
    [extras addObject:param];
  }
  
  return extras;
}
#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiLoginResult *result = [[UpaidApiLoginResult alloc] init];
  result.status = kUpaidApiLoginTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiLoginTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiLoginTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiLoginFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiLoginFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiLoginResult *result = [[UpaidApiLoginResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiLoginSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiLoginSuccess:)]) {
        if (data[@"termsAndConditions"] != nil) {
            result.termsAndConditions = [data[@"termsAndConditions"] boolValue];
        }
        if (data[@"terms_and_conditions"] != nil) {
            result.termsAndConditions = [data[@"terms_and_conditions"] boolValue];
        }

        [UpaidApi setSessionToken: data[@"token"]];
        [UpaidApi setSmsCode: self.data.code forPhone: self.data.phone];
        [UpaidApi setLastUsedPhone: self.data.phone];


        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiLoginSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiLoginBadCode && [self.delegate respondsToSelector:@selector(onUpaidApiLoginBadCode:)]) {
        [self.delegate performSelector:@selector(onUpaidApiLoginBadCode:) withObject: result];
      } else if (result.status == kUpaidApiLoginBannedUser && [self.delegate respondsToSelector:@selector(onUpaidApiLoginBannedUser:)]) {
        [self.delegate performSelector:@selector(onUpaidApiLoginBannedUser:) withObject: result];
      } else if (result.status == kUpaidApiLoginNoUser && [self.delegate respondsToSelector:@selector(onUpaidApiLoginNoUser:)]) {
        [self.delegate performSelector:@selector(onUpaidApiLoginNoUser:) withObject: result];
      } else if (result.status == kUpaidApiLoginTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiLoginTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiLoginTechnicalError:) withObject: result];
      } else if (result.status == kUpaidApiLoginUnsupportedVersion && [self.delegate respondsToSelector:@selector(onUpaidApiLoginUnsupportedVersion:)]) {
        [self.delegate performSelector:@selector(onUpaidApiLoginUnsupportedVersion:) withObject: result];
      } else if (result.status == kUpaidApiLoginBadPartnerId && [self.delegate respondsToSelector:@selector(onUpaidApiLoginBadPartnerId:)]) {
        [self.delegate performSelector:@selector(onUpaidApiLoginBadPartnerId:) withObject: result];
      } else if (result.status == kUpaidApiLoginBadPlatform && [self.delegate respondsToSelector:@selector(onUpaidApiLoginBadPlatform:)]) {
        [self.delegate performSelector:@selector(onUpaidApiLoginBadPlatform:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiLoginFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiLoginFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiLoginResult *) result  {
    UpaidLog(@"Login result:\n%@", result);
}


@end
