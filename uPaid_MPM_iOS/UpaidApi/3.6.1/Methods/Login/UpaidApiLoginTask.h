//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiLoginData.h"
#import "UpaidApiLoginResult.h"
#import "UpaidApiLoginDelegate.h"

@interface UpaidApiLoginTask : UpaidApiTask

- (id) initWithData: (UpaidApiLoginData *) taskData andDelegate: (id<UpaidApiLoginDelegate>) taskDelegate;

enum UpaidApiLoginStatuses : NSUInteger{
  kUpaidApiLoginSuccess = 1,
  kUpaidApiLoginBadCode = 2,
  kUpaidApiLoginBannedUser = 3,
  kUpaidApiLoginNoUser = 4,
  kUpaidApiLoginTechnicalError = 7,
  kUpaidApiLoginUnsupportedVersion = 9,
  kUpaidApiLoginBadPartnerId = 721,
  kUpaidApiLoginBadPlatform = 722,
};
        
@end
