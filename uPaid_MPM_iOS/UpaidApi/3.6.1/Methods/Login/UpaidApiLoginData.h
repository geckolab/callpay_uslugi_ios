//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpaidApiLoginData : NSObject

@property NSString *phone; 
@property NSString *code; 


//only required params
- (id) initWithPhone: (NSString *) phone andCode: (NSString *) code;


@end