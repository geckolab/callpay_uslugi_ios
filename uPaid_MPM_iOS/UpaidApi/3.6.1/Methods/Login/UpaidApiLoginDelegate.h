//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiLoginDelegate <NSObject>

- (void) onUpaidApiLoginSuccess: (UpaidApiLoginResult *) result;
- (void) onUpaidApiLoginFail: (UpaidApiLoginResult *) result;

@optional
- (void) onUpaidApiLoginBadCode: (UpaidApiLoginResult *) result;
- (void) onUpaidApiLoginBannedUser: (UpaidApiLoginResult *) result;
- (void) onUpaidApiLoginNoUser: (UpaidApiLoginResult *) result;
- (void) onUpaidApiLoginTechnicalError: (UpaidApiLoginResult *) result;
- (void) onUpaidApiLoginUnsupportedVersion: (UpaidApiLoginResult *) result;
- (void) onUpaidApiLoginBadPartnerId: (UpaidApiLoginResult *) result;
- (void) onUpaidApiLoginBadPlatform: (UpaidApiLoginResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiLoginDelegate implementation

- (void) onUpaidApiLoginSuccess: (UpaidApiLoginResult *) result {

}

- (void) onUpaidApiLoginFail: (UpaidApiLoginResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiLoginDelegate implementation

- (void) onUpaidApiLoginSuccess: (UpaidApiLoginResult *) result {

}

- (void) onUpaidApiLoginFail: (UpaidApiLoginResult *) result {

}

- (void) onUpaidApiLoginBadCode: (UpaidApiLoginResult *) result {

}

- (void) onUpaidApiLoginBannedUser: (UpaidApiLoginResult *) result {

}

- (void) onUpaidApiLoginNoUser: (UpaidApiLoginResult *) result {

}

- (void) onUpaidApiLoginTechnicalError: (UpaidApiLoginResult *) result {

}

- (void) onUpaidApiLoginUnsupportedVersion: (UpaidApiLoginResult *) result {

}

- (void) onUpaidApiLoginBadPartnerId: (UpaidApiLoginResult *) result {

}

- (void) onUpaidApiLoginBadPlatform: (UpaidApiLoginResult *) result {

}


//-------END SELECT-------
*/
@end
