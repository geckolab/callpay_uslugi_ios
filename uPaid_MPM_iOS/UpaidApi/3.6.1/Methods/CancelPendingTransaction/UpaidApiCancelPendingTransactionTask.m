//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiCancelPendingTransactionTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "AddressLong.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "singleShoppingCartItem.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiCancelPendingTransactionTask ()

@property UpaidApiCancelPendingTransactionData *data;
@property id<UpaidApiCancelPendingTransactionDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiCancelPendingTransactionTask


- (id) initWithData: (UpaidApiCancelPendingTransactionData *) taskData andDelegate: (id<UpaidApiCancelPendingTransactionDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"CancelPendingTransaction data is empty.");
  } else {
    UpaidLog(@"CancelPendingTransaction data:\n%@", self.data);
  }
  
  [self.proxy cancelPendingTransaction: [UpaidApi sessionToken] : self.data.paymentId ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiCancelPendingTransactionResult *result = [[UpaidApiCancelPendingTransactionResult alloc] init];
  result.status = kUpaidApiCancelPendingTransactionTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiCancelPendingTransactionTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiCancelPendingTransactionTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiCancelPendingTransactionFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiCancelPendingTransactionFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiCancelPendingTransactionResult *result = [[UpaidApiCancelPendingTransactionResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiCancelPendingTransactionSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiCancelPendingTransactionSuccess:)]) {

        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiCancelPendingTransactionSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiCancelPendingTransactionRejected && [self.delegate respondsToSelector:@selector(onUpaidApiCancelPendingTransactionRejected:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCancelPendingTransactionRejected:) withObject: result];
      } else if (result.status == kUpaidApiCancelPendingTransactionTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiCancelPendingTransactionTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCancelPendingTransactionTechnicalError:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiCancelPendingTransactionFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCancelPendingTransactionFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiCancelPendingTransactionResult *) result  {
    UpaidLog(@"CancelPendingTransaction result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiCancelPendingTransactionResult *result = [[UpaidApiCancelPendingTransactionResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiCancelPendingTransactionFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCancelPendingTransactionFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"CancelPendingTransaction" withStatus:@"fail"];
    }
}

@end
