//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiCancelPendingTransactionData.h"
#import "UpaidApiCancelPendingTransactionResult.h"
#import "UpaidApiCancelPendingTransactionDelegate.h"

@interface UpaidApiCancelPendingTransactionTask : UpaidApiTask

- (id) initWithData: (UpaidApiCancelPendingTransactionData *) taskData andDelegate: (id<UpaidApiCancelPendingTransactionDelegate>) taskDelegate;

enum UpaidApiCancelPendingTransactionStatuses : NSUInteger{
  kUpaidApiCancelPendingTransactionSuccess = 1,
  kUpaidApiCancelPendingTransactionRejected = 2,
  kUpaidApiCancelPendingTransactionTechnicalError = 7,
};
        
@end
