//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiCancelPendingTransactionDelegate <NSObject>

- (void) onUpaidApiCancelPendingTransactionSuccess: (UpaidApiCancelPendingTransactionResult *) result;
- (void) onUpaidApiCancelPendingTransactionFail: (UpaidApiCancelPendingTransactionResult *) result;

@optional
- (void) onUpaidApiCancelPendingTransactionRejected: (UpaidApiCancelPendingTransactionResult *) result;
- (void) onUpaidApiCancelPendingTransactionTechnicalError: (UpaidApiCancelPendingTransactionResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiCancelPendingTransactionDelegate implementation

- (void) onUpaidApiCancelPendingTransactionSuccess: (UpaidApiCancelPendingTransactionResult *) result {

}

- (void) onUpaidApiCancelPendingTransactionFail: (UpaidApiCancelPendingTransactionResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiCancelPendingTransactionDelegate implementation

- (void) onUpaidApiCancelPendingTransactionSuccess: (UpaidApiCancelPendingTransactionResult *) result {

}

- (void) onUpaidApiCancelPendingTransactionFail: (UpaidApiCancelPendingTransactionResult *) result {

}

- (void) onUpaidApiCancelPendingTransactionRejected: (UpaidApiCancelPendingTransactionResult *) result {

}

- (void) onUpaidApiCancelPendingTransactionTechnicalError: (UpaidApiCancelPendingTransactionResult *) result {

}


//-------END SELECT-------
*/
@end
