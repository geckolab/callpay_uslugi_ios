//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiChangeLimitTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiChangeLimitTask ()

@property UpaidApiChangeLimitData *data;
@property id<UpaidApiChangeLimitDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiChangeLimitTask


- (id) initWithData: (UpaidApiChangeLimitData *) taskData andDelegate: (id<UpaidApiChangeLimitDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"ChangeLimit data is empty.");
  } else {
    UpaidLog(@"ChangeLimit data:\n%@", self.data);
  }
  
  [self.proxy changeLimit: [UpaidApi sessionToken] : [NSString stringWithFormat: @"%d", self.data.limit] ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiChangeLimitResult *result = [[UpaidApiChangeLimitResult alloc] init];
  result.status = kUpaidApiChangeLimitTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiChangeLimitTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiChangeLimitTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiChangeLimitFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiChangeLimitFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiChangeLimitResult *result = [[UpaidApiChangeLimitResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiChangeLimitSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiChangeLimitSuccess:)]) {

        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiChangeLimitSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiChangeLimitFailed && [self.delegate respondsToSelector:@selector(onUpaidApiChangeLimitFailed:)]) {
        [self.delegate performSelector:@selector(onUpaidApiChangeLimitFailed:) withObject: result];
      } else if (result.status == kUpaidApiChangeLimitTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiChangeLimitTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiChangeLimitTechnicalError:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiChangeLimitFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiChangeLimitFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiChangeLimitResult *) result  {
    UpaidLog(@"ChangeLimit result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiChangeLimitResult *result = [[UpaidApiChangeLimitResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiChangeLimitFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiChangeLimitFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"ChangeLimit" withStatus:@"fail"];
    }
}

@end
