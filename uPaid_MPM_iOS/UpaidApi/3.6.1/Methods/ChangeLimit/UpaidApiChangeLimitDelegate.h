//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiChangeLimitDelegate <NSObject>

- (void) onUpaidApiChangeLimitSuccess: (UpaidApiChangeLimitResult *) result;
- (void) onUpaidApiChangeLimitFail: (UpaidApiChangeLimitResult *) result;

@optional
- (void) onUpaidApiChangeLimitFailed: (UpaidApiChangeLimitResult *) result;
- (void) onUpaidApiChangeLimitTechnicalError: (UpaidApiChangeLimitResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiChangeLimitDelegate implementation

- (void) onUpaidApiChangeLimitSuccess: (UpaidApiChangeLimitResult *) result {

}

- (void) onUpaidApiChangeLimitFail: (UpaidApiChangeLimitResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiChangeLimitDelegate implementation

- (void) onUpaidApiChangeLimitSuccess: (UpaidApiChangeLimitResult *) result {

}

- (void) onUpaidApiChangeLimitFail: (UpaidApiChangeLimitResult *) result {

}

- (void) onUpaidApiChangeLimitFailed: (UpaidApiChangeLimitResult *) result {

}

- (void) onUpaidApiChangeLimitTechnicalError: (UpaidApiChangeLimitResult *) result {

}


//-------END SELECT-------
*/
@end
