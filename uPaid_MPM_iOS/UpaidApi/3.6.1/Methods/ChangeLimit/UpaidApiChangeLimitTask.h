//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiChangeLimitData.h"
#import "UpaidApiChangeLimitResult.h"
#import "UpaidApiChangeLimitDelegate.h"

@interface UpaidApiChangeLimitTask : UpaidApiTask

- (id) initWithData: (UpaidApiChangeLimitData *) taskData andDelegate: (id<UpaidApiChangeLimitDelegate>) taskDelegate;

enum UpaidApiChangeLimitStatuses : NSUInteger{
  kUpaidApiChangeLimitSuccess = 1,
  kUpaidApiChangeLimitFailed = 2,
  kUpaidApiChangeLimitTechnicalError = 7,
};
        
@end
