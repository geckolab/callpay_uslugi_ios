//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiSetDeviceTokenData.h"
#import "UpaidApiSetDeviceTokenResult.h"
#import "UpaidApiSetDeviceTokenDelegate.h"

@interface UpaidApiSetDeviceTokenTask : UpaidApiTask

- (id) initWithData: (UpaidApiSetDeviceTokenData *) taskData andDelegate: (id<UpaidApiSetDeviceTokenDelegate>) taskDelegate;

enum UpaidApiSetDeviceTokenStatuses : NSUInteger{
  kUpaidApiSetDeviceTokenSuccess = 1,
  kUpaidApiSetDeviceTokenTechnicalError = 7,
};
        
@end
