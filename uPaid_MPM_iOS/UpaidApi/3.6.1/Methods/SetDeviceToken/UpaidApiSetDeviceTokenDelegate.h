//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiSetDeviceTokenDelegate <NSObject>

- (void) onUpaidApiSetDeviceTokenSuccess: (UpaidApiSetDeviceTokenResult *) result;
- (void) onUpaidApiSetDeviceTokenFail: (UpaidApiSetDeviceTokenResult *) result;

@optional
- (void) onUpaidApiSetDeviceTokenTechnicalError: (UpaidApiSetDeviceTokenResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiSetDeviceTokenDelegate implementation

- (void) onUpaidApiSetDeviceTokenSuccess: (UpaidApiSetDeviceTokenResult *) result {

}

- (void) onUpaidApiSetDeviceTokenFail: (UpaidApiSetDeviceTokenResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiSetDeviceTokenDelegate implementation

- (void) onUpaidApiSetDeviceTokenSuccess: (UpaidApiSetDeviceTokenResult *) result {

}

- (void) onUpaidApiSetDeviceTokenFail: (UpaidApiSetDeviceTokenResult *) result {

}

- (void) onUpaidApiSetDeviceTokenTechnicalError: (UpaidApiSetDeviceTokenResult *) result {

}


//-------END SELECT-------
*/
@end
