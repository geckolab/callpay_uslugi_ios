//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiSetDeviceTokenTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiSetDeviceTokenTask ()

@property UpaidApiSetDeviceTokenData *data;
@property id<UpaidApiSetDeviceTokenDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiSetDeviceTokenTask


- (id) initWithData: (UpaidApiSetDeviceTokenData *) taskData andDelegate: (id<UpaidApiSetDeviceTokenDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"SetDeviceToken data is empty.");
  } else {
    UpaidLog(@"SetDeviceToken data:\n%@", self.data);
  }
  
  [self.proxy setDeviceToken: [UpaidApi sessionToken] : self.data.deviceToken ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiSetDeviceTokenResult *result = [[UpaidApiSetDeviceTokenResult alloc] init];
  result.status = kUpaidApiSetDeviceTokenTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiSetDeviceTokenTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiSetDeviceTokenTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiSetDeviceTokenFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiSetDeviceTokenFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiSetDeviceTokenResult *result = [[UpaidApiSetDeviceTokenResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiSetDeviceTokenSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiSetDeviceTokenSuccess:)]) {

        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiSetDeviceTokenSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiSetDeviceTokenTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiSetDeviceTokenTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiSetDeviceTokenTechnicalError:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiSetDeviceTokenFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiSetDeviceTokenFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiSetDeviceTokenResult *) result  {
    UpaidLog(@"SetDeviceToken result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiSetDeviceTokenResult *result = [[UpaidApiSetDeviceTokenResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiSetDeviceTokenFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiSetDeviceTokenFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"SetDeviceToken" withStatus:@"fail"];
    }
}

@end
