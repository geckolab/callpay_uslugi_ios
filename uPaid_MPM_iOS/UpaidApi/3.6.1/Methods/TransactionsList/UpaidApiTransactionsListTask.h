//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiTransactionsListData.h"
#import "UpaidApiTransactionsListResult.h"
#import "UpaidApiTransactionsListDelegate.h"

@interface UpaidApiTransactionsListTask : UpaidApiTask

- (id) initWithData: (UpaidApiTransactionsListData *) taskData andDelegate: (id<UpaidApiTransactionsListDelegate>) taskDelegate;

enum UpaidApiTransactionsListStatuses : NSUInteger{
  kUpaidApiTransactionsListSuccess = 1,
  kUpaidApiTransactionsListNoTransactions = 0,
  kUpaidApiTransactionsListTechnicalError = 7,
};
        
@end
