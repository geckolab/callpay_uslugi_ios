//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiTransactionsListData.h"
#import "NSObject+WSClass.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@implementation UpaidApiTransactionsListData

- (id) initWithChannel: (NSString *) channel andSourceSm: (NSString *) sourceSm andOffset: (int ) offset andLimit: (int ) limit {
  self = [super init];
  
  if (self) {
    self.channel = channel;
    self.sourceSm = sourceSm;
    self.offset = offset;
    self.limit = limit;
  }
  
  return self;
}

- (id) init{
  self = [super init];
  
  if (self) {
    self.channel = @"";
    self.sourceSm = @"";
    self.offset = 0;
    self.limit = 0;
  }
  
  return self;
}


- (NSString *) description {
    return [NSString stringWithFormat: @"channel=%@\nsourceSm=%@\noffset=%@\nlimit=%@", self.channel, self.sourceSm, [NSString stringWithFormat: @"%d", self.offset], [NSString stringWithFormat: @"%d", self.limit]];
}

@end