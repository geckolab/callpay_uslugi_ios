//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiTransactionsListDelegate <NSObject>

- (void) onUpaidApiTransactionsListSuccess: (UpaidApiTransactionsListResult *) result;
- (void) onUpaidApiTransactionsListFail: (UpaidApiTransactionsListResult *) result;

@optional
- (void) onUpaidApiTransactionsListNoTransactions: (UpaidApiTransactionsListResult *) result;
- (void) onUpaidApiTransactionsListTechnicalError: (UpaidApiTransactionsListResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiTransactionsListDelegate implementation

- (void) onUpaidApiTransactionsListSuccess: (UpaidApiTransactionsListResult *) result {

}

- (void) onUpaidApiTransactionsListFail: (UpaidApiTransactionsListResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiTransactionsListDelegate implementation

- (void) onUpaidApiTransactionsListSuccess: (UpaidApiTransactionsListResult *) result {

}

- (void) onUpaidApiTransactionsListFail: (UpaidApiTransactionsListResult *) result {

}

- (void) onUpaidApiTransactionsListNoTransactions: (UpaidApiTransactionsListResult *) result {

}

- (void) onUpaidApiTransactionsListTechnicalError: (UpaidApiTransactionsListResult *) result {

}


//-------END SELECT-------
*/
@end
