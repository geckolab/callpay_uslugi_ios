//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTransaction.h"
#import "UpaidApiTransactionArray.h"

@interface UpaidApiTransactionsListData : NSObject

@property NSString *channel; //optional
@property NSString *sourceSm; //optional
@property int offset; //optional
@property int limit; //optional


//required + optional params
- (id) initWithChannel: (NSString *) channel andSourceSm: (NSString *) sourceSm andOffset: (int ) offset andLimit: (int ) limit;


@end