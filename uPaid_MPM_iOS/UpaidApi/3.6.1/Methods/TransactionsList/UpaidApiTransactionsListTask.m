//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiTransactionsListTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiTransactionsListTask ()

@property UpaidApiTransactionsListData *data;
@property id<UpaidApiTransactionsListDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiTransactionsListTask


- (id) initWithData: (UpaidApiTransactionsListData *) taskData andDelegate: (id<UpaidApiTransactionsListDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"TransactionsList data is empty.");
  } else {
    UpaidLog(@"TransactionsList data:\n%@", self.data);
  }
  
  [self.proxy transactionsList: [UpaidApi sessionToken] : self.data.channel : self.data.sourceSm : self.data.offset : self.data.limit ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiTransactionsListResult *result = [[UpaidApiTransactionsListResult alloc] init];
  result.status = kUpaidApiTransactionsListTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiTransactionsListTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiTransactionsListTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiTransactionsListFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiTransactionsListFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiTransactionsListResult *result = [[UpaidApiTransactionsListResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiTransactionsListSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiTransactionsListSuccess:)]) {
        if (data[@"transactions"] != nil) {
            result.transactions = [[UpaidApiTransactionArray alloc] init];

            for (int i = 0; i < [(NSDictionary *)data[@"transactions"] count]; i++) {
                transactionStatus *wsObject = [[transactionStatus alloc] initWithArray: [[data[@"transactions"] objectAtIndex: i] objectForKey:@"nodeChildArray"]];
                UpaidApiTransaction * object = [[UpaidApiTransaction alloc] initWithWsClass: wsObject];

                [result.transactions addObject: object];
            }

        }


        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiTransactionsListSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiTransactionsListNoTransactions && [self.delegate respondsToSelector:@selector(onUpaidApiTransactionsListNoTransactions:)]) {
        [self.delegate performSelector:@selector(onUpaidApiTransactionsListNoTransactions:) withObject: result];
      } else if (result.status == kUpaidApiTransactionsListTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiTransactionsListTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiTransactionsListTechnicalError:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiTransactionsListFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiTransactionsListFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiTransactionsListResult *) result  {
    UpaidLog(@"TransactionsList result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiTransactionsListResult *result = [[UpaidApiTransactionsListResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiTransactionsListFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiTransactionsListFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"TransactionsList" withStatus:@"fail"];
    }
}

@end
