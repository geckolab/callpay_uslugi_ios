//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiTransactionsListResult.h"
#import "UpaidApiTransactionsListTask.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@implementation UpaidApiTransactionsListResult

@synthesize status = _status;

- (id) init {
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

- (void) setStatus: (NSInteger) status {
  _status = status;
  
  if (self.status == kUpaidApiTransactionsListSuccess) {
    self.statusDescription = @"";
  }

  if (self.status == kUpaidApiTransactionsListNoTransactions) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.TransactionsList.NoTransactions", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiTransactionsListTechnicalError) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.TransactionsList.TechnicalError", @"UpaidApi", nil);
  }
}

- (NSString *) description {
    return [NSString stringWithFormat: @"status=%ld\n%@", (long)self.status, [NSString stringWithFormat: @"transactions=%@", self.transactions]];
}

@end
