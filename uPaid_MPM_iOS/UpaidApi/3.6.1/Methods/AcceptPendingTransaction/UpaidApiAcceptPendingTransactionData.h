//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpaidApiAcceptPendingTransactionData : NSObject

@property int paymentId; 
@property int cvc2; 
@property int shippingId; 
@property NSString *cardId; 


//only required params
- (id) initWithPaymentId: (int ) paymentId andCvc2: (int ) cvc2 andShippingId: (int ) shippingId andCardId: (NSString *) cardId;


@end