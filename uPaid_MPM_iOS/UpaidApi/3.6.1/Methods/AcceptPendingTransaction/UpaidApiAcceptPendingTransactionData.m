//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiAcceptPendingTransactionData.h"
#import "NSObject+WSClass.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@implementation UpaidApiAcceptPendingTransactionData

- (id) initWithPaymentId: (int ) paymentId andCvc2: (int ) cvc2 andShippingId: (int ) shippingId andCardId: (NSString *) cardId {
  self = [super init];
  
  if (self) {
    self.paymentId = paymentId;
    self.cvc2 = cvc2;
    self.shippingId = shippingId;
    self.cardId = cardId;
  }
  
  return self;
}



- (NSString *) description {
    return [NSString stringWithFormat: @"paymentId=%@\ncvc2=%@\nshippingId=%@\ncardId=%@", [NSString stringWithFormat: @"%d", self.paymentId], [NSString stringWithFormat: @"%d", self.cvc2], [NSString stringWithFormat: @"%d", self.shippingId], self.cardId];
}

@end