//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiAcceptPendingTransactionResult.h"
#import "UpaidApiAcceptPendingTransactionTask.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@implementation UpaidApiAcceptPendingTransactionResult

@synthesize status = _status;

- (id) init {
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

- (void) setStatus: (NSInteger) status {
  _status = status;
  
  if (self.status == kUpaidApiAcceptPendingTransactionSuccess) {
    self.statusDescription = @"";
  }

  if (self.status == kUpaidApiAcceptPendingTransactionRejected) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.AcceptPendingTransaction.Rejected", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiAcceptPendingTransactionBadCvc) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.AcceptPendingTransaction.BadCvc", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiAcceptPendingTransactionCardNoVerified) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.AcceptPendingTransaction.CardNoVerified", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiAcceptPendingTransactionTechnicalError) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.AcceptPendingTransaction.TechnicalError", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiAcceptPendingTransactionTransactionRejected) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.AcceptPendingTransaction.TransactionRejected", @"UpaidApi", nil);
  }
}

- (NSString *) description {
    return [NSString stringWithFormat: @"status=%ld\n%@", (long)self.status, @""];
}

@end
