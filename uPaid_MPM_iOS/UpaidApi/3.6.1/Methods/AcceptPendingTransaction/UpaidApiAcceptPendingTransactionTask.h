//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiAcceptPendingTransactionData.h"
#import "UpaidApiAcceptPendingTransactionResult.h"
#import "UpaidApiAcceptPendingTransactionDelegate.h"

@interface UpaidApiAcceptPendingTransactionTask : UpaidApiTask

- (id) initWithData: (UpaidApiAcceptPendingTransactionData *) taskData andDelegate: (id<UpaidApiAcceptPendingTransactionDelegate>) taskDelegate;

enum UpaidApiAcceptPendingTransactionStatuses : NSUInteger{
  kUpaidApiAcceptPendingTransactionSuccess = 1,
  kUpaidApiAcceptPendingTransactionRejected = 2,
  kUpaidApiAcceptPendingTransactionBadCvc = 3,
  kUpaidApiAcceptPendingTransactionCardNoVerified = 6,
  kUpaidApiAcceptPendingTransactionTechnicalError = 7,
  kUpaidApiAcceptPendingTransactionTransactionRejected = 8,
};
        
@end
