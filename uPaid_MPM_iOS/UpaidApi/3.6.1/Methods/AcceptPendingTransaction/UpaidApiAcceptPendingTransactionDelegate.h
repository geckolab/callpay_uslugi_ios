//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiAcceptPendingTransactionDelegate <NSObject>

- (void) onUpaidApiAcceptPendingTransactionSuccess: (UpaidApiAcceptPendingTransactionResult *) result;
- (void) onUpaidApiAcceptPendingTransactionFail: (UpaidApiAcceptPendingTransactionResult *) result;

@optional
- (void) onUpaidApiAcceptPendingTransactionRejected: (UpaidApiAcceptPendingTransactionResult *) result;
- (void) onUpaidApiAcceptPendingTransactionBadCvc: (UpaidApiAcceptPendingTransactionResult *) result;
- (void) onUpaidApiAcceptPendingTransactionCardNoVerified: (UpaidApiAcceptPendingTransactionResult *) result;
- (void) onUpaidApiAcceptPendingTransactionTechnicalError: (UpaidApiAcceptPendingTransactionResult *) result;
- (void) onUpaidApiAcceptPendingTransactionTransactionRejected: (UpaidApiAcceptPendingTransactionResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiAcceptPendingTransactionDelegate implementation

- (void) onUpaidApiAcceptPendingTransactionSuccess: (UpaidApiAcceptPendingTransactionResult *) result {

}

- (void) onUpaidApiAcceptPendingTransactionFail: (UpaidApiAcceptPendingTransactionResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiAcceptPendingTransactionDelegate implementation

- (void) onUpaidApiAcceptPendingTransactionSuccess: (UpaidApiAcceptPendingTransactionResult *) result {

}

- (void) onUpaidApiAcceptPendingTransactionFail: (UpaidApiAcceptPendingTransactionResult *) result {

}

- (void) onUpaidApiAcceptPendingTransactionRejected: (UpaidApiAcceptPendingTransactionResult *) result {

}

- (void) onUpaidApiAcceptPendingTransactionBadCvc: (UpaidApiAcceptPendingTransactionResult *) result {

}

- (void) onUpaidApiAcceptPendingTransactionCardNoVerified: (UpaidApiAcceptPendingTransactionResult *) result {

}

- (void) onUpaidApiAcceptPendingTransactionTechnicalError: (UpaidApiAcceptPendingTransactionResult *) result {

}

- (void) onUpaidApiAcceptPendingTransactionTransactionRejected: (UpaidApiAcceptPendingTransactionResult *) result {

}


//-------END SELECT-------
*/
@end
