//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiAcceptPendingTransactionTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiAcceptPendingTransactionTask ()

@property UpaidApiAcceptPendingTransactionData *data;
@property id<UpaidApiAcceptPendingTransactionDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiAcceptPendingTransactionTask


- (id) initWithData: (UpaidApiAcceptPendingTransactionData *) taskData andDelegate: (id<UpaidApiAcceptPendingTransactionDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"AcceptPendingTransaction data is empty.");
  } else {
    UpaidLog(@"AcceptPendingTransaction data:\n%@", self.data);
  }
  
  [self.proxy acceptPendingTransaction: [UpaidApi sessionToken] : [NSString stringWithFormat: @"%d", self.data.paymentId] : [NSString stringWithFormat: @"%d", self.data.cvc2] : [NSString stringWithFormat: @"%d", self.data.shippingId] : self.data.cardId ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiAcceptPendingTransactionResult *result = [[UpaidApiAcceptPendingTransactionResult alloc] init];
  result.status = kUpaidApiAcceptPendingTransactionTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiAcceptPendingTransactionTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiAcceptPendingTransactionTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiAcceptPendingTransactionFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiAcceptPendingTransactionFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiAcceptPendingTransactionResult *result = [[UpaidApiAcceptPendingTransactionResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiAcceptPendingTransactionSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiAcceptPendingTransactionSuccess:)]) {

        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiAcceptPendingTransactionSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiAcceptPendingTransactionRejected && [self.delegate respondsToSelector:@selector(onUpaidApiAcceptPendingTransactionRejected:)]) {
        [self.delegate performSelector:@selector(onUpaidApiAcceptPendingTransactionRejected:) withObject: result];
      } else if (result.status == kUpaidApiAcceptPendingTransactionBadCvc && [self.delegate respondsToSelector:@selector(onUpaidApiAcceptPendingTransactionBadCvc:)]) {
        [self.delegate performSelector:@selector(onUpaidApiAcceptPendingTransactionBadCvc:) withObject: result];
      } else if (result.status == kUpaidApiAcceptPendingTransactionCardNoVerified && [self.delegate respondsToSelector:@selector(onUpaidApiAcceptPendingTransactionCardNoVerified:)]) {
        [self.delegate performSelector:@selector(onUpaidApiAcceptPendingTransactionCardNoVerified:) withObject: result];
      } else if (result.status == kUpaidApiAcceptPendingTransactionTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiAcceptPendingTransactionTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiAcceptPendingTransactionTechnicalError:) withObject: result];
      } else if (result.status == kUpaidApiAcceptPendingTransactionTransactionRejected && [self.delegate respondsToSelector:@selector(onUpaidApiAcceptPendingTransactionTransactionRejected:)]) {
        [self.delegate performSelector:@selector(onUpaidApiAcceptPendingTransactionTransactionRejected:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiAcceptPendingTransactionFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiAcceptPendingTransactionFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiAcceptPendingTransactionResult *) result  {
    UpaidLog(@"AcceptPendingTransaction result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiAcceptPendingTransactionResult *result = [[UpaidApiAcceptPendingTransactionResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiAcceptPendingTransactionFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiAcceptPendingTransactionFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"AcceptPendingTransaction" withStatus:@"fail"];
    }
}

@end
