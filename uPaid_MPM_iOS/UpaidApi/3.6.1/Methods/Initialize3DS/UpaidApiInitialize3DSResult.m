//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiInitialize3DSResult.h"
#import "UpaidApiInitialize3DSTask.h"
#import "AddressLong.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "singleShoppingCartItem.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiInitialize3DSResult ()


@end

@implementation UpaidApiInitialize3DSResult

@synthesize status = _status;

- (id) init {
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

- (void) setStatus: (NSInteger) status {
  _status = status;
  
  if (self.status == kUpaidApiInitialize3DSSuccess) {
    self.statusDescription = @"";
  }

  if (self.status == kUpaidApiInitialize3DSRejected) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Initialize3DS.Rejected", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiInitialize3DSCardNoExist) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Initialize3DS.CardNoExist", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiInitialize3DSVerificationImpossible) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Initialize3DS.VerificationImpossible", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiInitialize3DSTechnicalError) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Initialize3DS.TechnicalError", @"UpaidApi", nil);
  }

}

- (NSString *) description {
    return [NSString stringWithFormat: @"status=%ld\n%@", (long)self.status, [NSString stringWithFormat: @"paymentId=%@\npageContent=%@", [NSString stringWithFormat: @"%d", self.paymentId], self.pageContent]];
}

@end
