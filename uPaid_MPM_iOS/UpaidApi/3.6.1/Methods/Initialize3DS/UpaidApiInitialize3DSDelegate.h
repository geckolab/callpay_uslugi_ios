//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiInitialize3DSDelegate <NSObject>

- (void) onUpaidApiInitialize3DSSuccess: (UpaidApiInitialize3DSResult *) result;
- (void) onUpaidApiInitialize3DSFail: (UpaidApiInitialize3DSResult *) result;

@optional
- (void) onUpaidApiInitialize3DSRejected: (UpaidApiInitialize3DSResult *) result;
- (void) onUpaidApiInitialize3DSCardNoExist: (UpaidApiInitialize3DSResult *) result;
- (void) onUpaidApiInitialize3DSVerificationImpossible: (UpaidApiInitialize3DSResult *) result;
- (void) onUpaidApiInitialize3DSTechnicalError: (UpaidApiInitialize3DSResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiInitialize3DSDelegate implementation

- (void) onUpaidApiInitialize3DSSuccess: (UpaidApiInitialize3DSResult *) result {

}

- (void) onUpaidApiInitialize3DSFail: (UpaidApiInitialize3DSResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiInitialize3DSDelegate implementation

- (void) onUpaidApiInitialize3DSSuccess: (UpaidApiInitialize3DSResult *) result {

}

- (void) onUpaidApiInitialize3DSFail: (UpaidApiInitialize3DSResult *) result {

}

- (void) onUpaidApiInitialize3DSRejected: (UpaidApiInitialize3DSResult *) result {

}

- (void) onUpaidApiInitialize3DSCardNoExist: (UpaidApiInitialize3DSResult *) result {

}

- (void) onUpaidApiInitialize3DSVerificationImpossible: (UpaidApiInitialize3DSResult *) result {

}

- (void) onUpaidApiInitialize3DSTechnicalError: (UpaidApiInitialize3DSResult *) result {

}


//-------END SELECT-------
*/
@end
