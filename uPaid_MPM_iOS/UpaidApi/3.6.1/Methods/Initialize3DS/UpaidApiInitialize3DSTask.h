//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiInitialize3DSData.h"
#import "UpaidApiInitialize3DSResult.h"
#import "UpaidApiInitialize3DSDelegate.h"

@interface UpaidApiInitialize3DSTask : UpaidApiTask

- (id) initWithData: (UpaidApiInitialize3DSData *) taskData andDelegate: (id<UpaidApiInitialize3DSDelegate>) taskDelegate;

enum UpaidApiInitialize3DSStatuses : NSUInteger{
  kUpaidApiInitialize3DSSuccess = 1,
  kUpaidApiInitialize3DSRejected = 2,
  kUpaidApiInitialize3DSCardNoExist = 3,
  kUpaidApiInitialize3DSVerificationImpossible = 6,
  kUpaidApiInitialize3DSTechnicalError = 7,
};
        
@end
