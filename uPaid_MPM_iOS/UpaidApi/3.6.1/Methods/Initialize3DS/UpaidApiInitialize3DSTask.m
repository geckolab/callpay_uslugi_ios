//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiInitialize3DSTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "AddressLong.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "singleShoppingCartItem.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiInitialize3DSTask ()

@property UpaidApiInitialize3DSData *data;
@property id<UpaidApiInitialize3DSDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiInitialize3DSTask


- (id) initWithData: (UpaidApiInitialize3DSData *) taskData andDelegate: (id<UpaidApiInitialize3DSDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"Initialize3DS data is empty.");
  } else {
    UpaidLog(@"Initialize3DS data:\n%@", self.data);
  }
  
  [self.proxy initialize3DS: [UpaidApi sessionToken] : self.data.cardId ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiInitialize3DSResult *result = [[UpaidApiInitialize3DSResult alloc] init];
  result.status = kUpaidApiInitialize3DSTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiInitialize3DSTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiInitialize3DSTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiInitialize3DSFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiInitialize3DSFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiInitialize3DSResult *result = [[UpaidApiInitialize3DSResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiInitialize3DSSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiInitialize3DSSuccess:)]) {
        if (data[@"paymentId"] != nil) {
            result.paymentId = [data[@"paymentId"] intValue];
        }
        if (data[@"payment_id"] != nil) {
            result.paymentId = [data[@"payment_id"] intValue];
        }

        if (data[@"pageContent"] != nil) {
            result.pageContent = data[@"pageContent"];
        }
        if (data[@"page_content"] != nil) {
            result.pageContent = data[@"page_content"];
        }


        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiInitialize3DSSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiInitialize3DSRejected && [self.delegate respondsToSelector:@selector(onUpaidApiInitialize3DSRejected:)]) {
        [self.delegate performSelector:@selector(onUpaidApiInitialize3DSRejected:) withObject: result];
      } else if (result.status == kUpaidApiInitialize3DSCardNoExist && [self.delegate respondsToSelector:@selector(onUpaidApiInitialize3DSCardNoExist:)]) {
        [self.delegate performSelector:@selector(onUpaidApiInitialize3DSCardNoExist:) withObject: result];
      } else if (result.status == kUpaidApiInitialize3DSVerificationImpossible && [self.delegate respondsToSelector:@selector(onUpaidApiInitialize3DSVerificationImpossible:)]) {
        [self.delegate performSelector:@selector(onUpaidApiInitialize3DSVerificationImpossible:) withObject: result];
      } else if (result.status == kUpaidApiInitialize3DSTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiInitialize3DSTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiInitialize3DSTechnicalError:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiInitialize3DSFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiInitialize3DSFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiInitialize3DSResult *) result  {
    UpaidLog(@"Initialize3DS result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiInitialize3DSResult *result = [[UpaidApiInitialize3DSResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiInitialize3DSFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiInitialize3DSFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"Initialize3DS" withStatus:@"fail"];
    }
}

@end
