//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiRefundTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiRefundTask ()

@property UpaidApiRefundData *data;
@property id<UpaidApiRefundDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiRefundTask


- (id) initWithData: (UpaidApiRefundData *) taskData andDelegate: (id<UpaidApiRefundDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"Refund data is empty.");
  } else {
    UpaidLog(@"Refund data:\n%@", self.data);
  }
  
  [self.proxy refund: [UpaidApi sessionToken] : self.data.transactionId : self.data.isUpaid : [NSString stringWithFormat: @"%d", self.data.amount] ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiRefundResult *result = [[UpaidApiRefundResult alloc] init];
  result.status = kUpaidApiRefundTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiRefundTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiRefundTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiRefundFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiRefundFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiRefundResult *result = [[UpaidApiRefundResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiRefundSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiRefundSuccess:)]) {

        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiRefundSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiRefundTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiRefundTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiRefundTechnicalError:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiRefundFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiRefundFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiRefundResult *) result  {
    UpaidLog(@"Refund result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiRefundResult *result = [[UpaidApiRefundResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiRefundFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiRefundFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"Refund" withStatus:@"fail"];
    }
}

@end
