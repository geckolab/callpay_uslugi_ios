//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiRefundDelegate <NSObject>

- (void) onUpaidApiRefundSuccess: (UpaidApiRefundResult *) result;
- (void) onUpaidApiRefundFail: (UpaidApiRefundResult *) result;

@optional
- (void) onUpaidApiRefundTechnicalError: (UpaidApiRefundResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiRefundDelegate implementation

- (void) onUpaidApiRefundSuccess: (UpaidApiRefundResult *) result {

}

- (void) onUpaidApiRefundFail: (UpaidApiRefundResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiRefundDelegate implementation

- (void) onUpaidApiRefundSuccess: (UpaidApiRefundResult *) result {

}

- (void) onUpaidApiRefundFail: (UpaidApiRefundResult *) result {

}

- (void) onUpaidApiRefundTechnicalError: (UpaidApiRefundResult *) result {

}


//-------END SELECT-------
*/
@end
