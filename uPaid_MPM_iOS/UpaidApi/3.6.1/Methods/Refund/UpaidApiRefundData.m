//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiRefundData.h"
#import "NSObject+WSClass.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@implementation UpaidApiRefundData

- (id) initWithTransactionId: (NSString *) transactionId andIsUpaid: (BOOL ) isUpaid andAmount: (int ) amount {
  self = [super init];
  
  if (self) {
    self.transactionId = transactionId;
    self.isUpaid = isUpaid;
    self.amount = amount;
  }
  
  return self;
}



- (NSString *) description {
    return [NSString stringWithFormat: @"transactionId=%@\nisUpaid=%@\namount=%@", self.transactionId, [NSString stringWithFormat: @"%d", self.isUpaid], [NSString stringWithFormat: @"%d", self.amount]];
}

@end