//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiRefundData.h"
#import "UpaidApiRefundResult.h"
#import "UpaidApiRefundDelegate.h"

@interface UpaidApiRefundTask : UpaidApiTask

- (id) initWithData: (UpaidApiRefundData *) taskData andDelegate: (id<UpaidApiRefundDelegate>) taskDelegate;

enum UpaidApiRefundStatuses : NSUInteger{
  kUpaidApiRefundSuccess = 1,
  kUpaidApiRefundTechnicalError = 7,
};
        
@end
