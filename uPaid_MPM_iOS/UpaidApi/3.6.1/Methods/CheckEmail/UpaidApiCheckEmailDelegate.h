//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiCheckEmailDelegate <NSObject>

- (void) onUpaidApiCheckEmailSuccess: (UpaidApiCheckEmailResult *) result;
- (void) onUpaidApiCheckEmailFail: (UpaidApiCheckEmailResult *) result;

@optional
- (void) onUpaidApiCheckEmailEmailNotFound: (UpaidApiCheckEmailResult *) result;
- (void) onUpaidApiCheckEmailTechnicalError: (UpaidApiCheckEmailResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiCheckEmailDelegate implementation

- (void) onUpaidApiCheckEmailSuccess: (UpaidApiCheckEmailResult *) result {

}

- (void) onUpaidApiCheckEmailFail: (UpaidApiCheckEmailResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiCheckEmailDelegate implementation

- (void) onUpaidApiCheckEmailSuccess: (UpaidApiCheckEmailResult *) result {

}

- (void) onUpaidApiCheckEmailFail: (UpaidApiCheckEmailResult *) result {

}

- (void) onUpaidApiCheckEmailEmailNotFound: (UpaidApiCheckEmailResult *) result {

}

- (void) onUpaidApiCheckEmailTechnicalError: (UpaidApiCheckEmailResult *) result {

}


//-------END SELECT-------
*/
@end
