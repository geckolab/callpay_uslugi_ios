//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiCheckEmailTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiCheckEmailTask ()

@property UpaidApiCheckEmailData *data;
@property id<UpaidApiCheckEmailDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiCheckEmailTask


- (id) initWithData: (UpaidApiCheckEmailData *) taskData andDelegate: (id<UpaidApiCheckEmailDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"CheckEmail data is empty.");
  } else {
    UpaidLog(@"CheckEmail data:\n%@", self.data);
  }
  
  [self.proxy checkEmail: [UpaidApi partnerId] : self.data.email ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiCheckEmailResult *result = [[UpaidApiCheckEmailResult alloc] init];
  result.status = kUpaidApiCheckEmailTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiCheckEmailTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiCheckEmailTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiCheckEmailFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiCheckEmailFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiCheckEmailResult *result = [[UpaidApiCheckEmailResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiCheckEmailSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiCheckEmailSuccess:)]) {

        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiCheckEmailSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiCheckEmailEmailNotFound && [self.delegate respondsToSelector:@selector(onUpaidApiCheckEmailEmailNotFound:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCheckEmailEmailNotFound:) withObject: result];
      } else if (result.status == kUpaidApiCheckEmailTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiCheckEmailTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCheckEmailTechnicalError:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiCheckEmailFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCheckEmailFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiCheckEmailResult *) result  {
    UpaidLog(@"CheckEmail result:\n%@", result);
}


@end
