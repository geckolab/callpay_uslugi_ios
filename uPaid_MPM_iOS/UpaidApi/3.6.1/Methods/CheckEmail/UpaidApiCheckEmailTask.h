//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiCheckEmailData.h"
#import "UpaidApiCheckEmailResult.h"
#import "UpaidApiCheckEmailDelegate.h"

@interface UpaidApiCheckEmailTask : UpaidApiTask

- (id) initWithData: (UpaidApiCheckEmailData *) taskData andDelegate: (id<UpaidApiCheckEmailDelegate>) taskDelegate;

enum UpaidApiCheckEmailStatuses : NSUInteger{
  kUpaidApiCheckEmailSuccess = 1,
  kUpaidApiCheckEmailEmailNotFound = 2,
  kUpaidApiCheckEmailTechnicalError = 7,
};
        
@end
