//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpaidApiCheckEmailData : NSObject

@property NSString *email; 


//only required params
- (id) initWithEmail: (NSString *) email;


@end