//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiGetMasterPassSelectorLinkData.h"
#import "UpaidApiGetMasterPassSelectorLinkResult.h"
#import "UpaidApiGetMasterPassSelectorLinkDelegate.h"

@interface UpaidApiGetMasterPassSelectorLinkTask : UpaidApiTask

- (id) initWithData: (UpaidApiGetMasterPassSelectorLinkData *) taskData andDelegate: (id<UpaidApiGetMasterPassSelectorLinkDelegate>) taskDelegate;

enum UpaidApiGetMasterPassSelectorLinkStatuses : NSUInteger{
  kUpaidApiGetMasterPassSelectorLinkSuccess = 1,
  kUpaidApiGetMasterPassSelectorLinkUnknownError = 2,
  kUpaidApiGetMasterPassSelectorLinkIncorrecPartnerId = 3,
  kUpaidApiGetMasterPassSelectorLinkTechnicalError = 7,
};
        
@end
