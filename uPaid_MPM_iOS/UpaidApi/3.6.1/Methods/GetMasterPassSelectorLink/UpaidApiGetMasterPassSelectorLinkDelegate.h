//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiGetMasterPassSelectorLinkDelegate <NSObject>

- (void) onUpaidApiGetMasterPassSelectorLinkSuccess: (UpaidApiGetMasterPassSelectorLinkResult *) result;
- (void) onUpaidApiGetMasterPassSelectorLinkFail: (UpaidApiGetMasterPassSelectorLinkResult *) result;

@optional
- (void) onUpaidApiGetMasterPassSelectorLinkUnknownError: (UpaidApiGetMasterPassSelectorLinkResult *) result;
- (void) onUpaidApiGetMasterPassSelectorLinkIncorrecPartnerId: (UpaidApiGetMasterPassSelectorLinkResult *) result;
- (void) onUpaidApiGetMasterPassSelectorLinkTechnicalError: (UpaidApiGetMasterPassSelectorLinkResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiGetMasterPassSelectorLinkDelegate implementation

- (void) onUpaidApiGetMasterPassSelectorLinkSuccess: (UpaidApiGetMasterPassSelectorLinkResult *) result {

}

- (void) onUpaidApiGetMasterPassSelectorLinkFail: (UpaidApiGetMasterPassSelectorLinkResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiGetMasterPassSelectorLinkDelegate implementation

- (void) onUpaidApiGetMasterPassSelectorLinkSuccess: (UpaidApiGetMasterPassSelectorLinkResult *) result {

}

- (void) onUpaidApiGetMasterPassSelectorLinkFail: (UpaidApiGetMasterPassSelectorLinkResult *) result {

}

- (void) onUpaidApiGetMasterPassSelectorLinkUnknownError: (UpaidApiGetMasterPassSelectorLinkResult *) result {

}

- (void) onUpaidApiGetMasterPassSelectorLinkIncorrecPartnerId: (UpaidApiGetMasterPassSelectorLinkResult *) result {

}

- (void) onUpaidApiGetMasterPassSelectorLinkTechnicalError: (UpaidApiGetMasterPassSelectorLinkResult *) result {

}


//-------END SELECT-------
*/
@end
