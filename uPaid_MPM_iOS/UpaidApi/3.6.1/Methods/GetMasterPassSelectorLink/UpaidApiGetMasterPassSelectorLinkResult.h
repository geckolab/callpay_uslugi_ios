//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiResult.h"
#import "UpaidApiShoppingCartItem.h"
#import "UpaidApiShoppingCartItemArray.h"

@interface UpaidApiGetMasterPassSelectorLinkResult : UpaidApiResult

@property NSString *callbackConfirmed;

@property int oauthExpireIn;

@property NSString *authorizeUrl;

@property NSString *requestToken;

@property NSString *redirectUrl;



@end
