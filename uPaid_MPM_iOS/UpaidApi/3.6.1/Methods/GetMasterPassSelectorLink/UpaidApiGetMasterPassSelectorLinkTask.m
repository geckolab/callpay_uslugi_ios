//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiGetMasterPassSelectorLinkTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "AddressLong.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiAddressLong.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "singleShoppingCartItem.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiGetMasterPassSelectorLinkTask ()

@property UpaidApiGetMasterPassSelectorLinkData *data;
@property id<UpaidApiGetMasterPassSelectorLinkDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiGetMasterPassSelectorLinkTask


- (id) initWithData: (UpaidApiGetMasterPassSelectorLinkData *) taskData andDelegate: (id<UpaidApiGetMasterPassSelectorLinkDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"GetMasterPassSelectorLink data is empty.");
  } else {
    UpaidLog(@"GetMasterPassSelectorLink data:\n%@", self.data);
  }
  
  [self.proxy getMasterPassSelectorLink: self.data.callbackUrl : [UpaidApi partnerId] : self.data.currency : (NSMutableArray *)[self.data.shoppingCart wsClass] ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiGetMasterPassSelectorLinkResult *result = [[UpaidApiGetMasterPassSelectorLinkResult alloc] init];
  result.status = kUpaidApiGetMasterPassSelectorLinkTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiGetMasterPassSelectorLinkTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiGetMasterPassSelectorLinkTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiGetMasterPassSelectorLinkFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiGetMasterPassSelectorLinkFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiGetMasterPassSelectorLinkResult *result = [[UpaidApiGetMasterPassSelectorLinkResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiGetMasterPassSelectorLinkSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiGetMasterPassSelectorLinkSuccess:)]) {
        if (data[@"callbackConfirmed"] != nil) {
            result.callbackConfirmed = data[@"callbackConfirmed"];
        }
        if (data[@"callback_confirmed"] != nil) {
            result.callbackConfirmed = data[@"callback_confirmed"];
        }

        if (data[@"oauthExpireIn"] != nil) {
            result.oauthExpireIn = [data[@"oauthExpireIn"] intValue];
        }
        if (data[@"oauth_expire_in"] != nil) {
            result.oauthExpireIn = [data[@"oauth_expire_in"] intValue];
        }

        if (data[@"authorizeUrl"] != nil) {
            result.authorizeUrl = data[@"authorizeUrl"];
        }
        if (data[@"authorize_url"] != nil) {
            result.authorizeUrl = data[@"authorize_url"];
        }

        if (data[@"requestToken"] != nil) {
            result.requestToken = data[@"requestToken"];
        }
        if (data[@"request_token"] != nil) {
            result.requestToken = data[@"request_token"];
        }

        if (data[@"redirectUrl"] != nil) {
            result.redirectUrl = data[@"redirectUrl"];
        }
        if (data[@"redirect_url"] != nil) {
            result.redirectUrl = data[@"redirect_url"];
        }


        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiGetMasterPassSelectorLinkSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiGetMasterPassSelectorLinkUnknownError && [self.delegate respondsToSelector:@selector(onUpaidApiGetMasterPassSelectorLinkUnknownError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiGetMasterPassSelectorLinkUnknownError:) withObject: result];
      } else if (result.status == kUpaidApiGetMasterPassSelectorLinkIncorrecPartnerId && [self.delegate respondsToSelector:@selector(onUpaidApiGetMasterPassSelectorLinkIncorrecPartnerId:)]) {
        [self.delegate performSelector:@selector(onUpaidApiGetMasterPassSelectorLinkIncorrecPartnerId:) withObject: result];
      } else if (result.status == kUpaidApiGetMasterPassSelectorLinkTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiGetMasterPassSelectorLinkTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiGetMasterPassSelectorLinkTechnicalError:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiGetMasterPassSelectorLinkFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiGetMasterPassSelectorLinkFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiGetMasterPassSelectorLinkResult *) result  {
    UpaidLog(@"GetMasterPassSelectorLink result:\n%@", result);
}


@end
