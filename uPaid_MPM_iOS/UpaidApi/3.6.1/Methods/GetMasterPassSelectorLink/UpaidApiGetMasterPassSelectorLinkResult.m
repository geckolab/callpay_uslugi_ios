//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiGetMasterPassSelectorLinkResult.h"
#import "UpaidApiGetMasterPassSelectorLinkTask.h"
#import "AddressLong.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiAddressLong.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "singleShoppingCartItem.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiGetMasterPassSelectorLinkResult ()


@end

@implementation UpaidApiGetMasterPassSelectorLinkResult

@synthesize status = _status;

- (id) init {
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

- (void) setStatus: (NSInteger) status {
  _status = status;
  
  if (self.status == kUpaidApiGetMasterPassSelectorLinkSuccess) {
    self.statusDescription = @"";
  }

  if (self.status == kUpaidApiGetMasterPassSelectorLinkUnknownError) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.GetMasterPassSelectorLink.UnknownError", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiGetMasterPassSelectorLinkIncorrecPartnerId) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.GetMasterPassSelectorLink.IncorrecPartnerId", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiGetMasterPassSelectorLinkTechnicalError) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.GetMasterPassSelectorLink.TechnicalError", @"UpaidApi", nil);
  }

}

- (NSString *) description {
    return [NSString stringWithFormat: @"status=%ld\n%@", (long)self.status, [NSString stringWithFormat: @"callbackConfirmed=%@\noauthExpireIn=%@\nauthorizeUrl=%@\nrequestToken=%@\nredirectUrl=%@", self.callbackConfirmed, [NSString stringWithFormat: @"%d", self.oauthExpireIn], self.authorizeUrl, self.requestToken, self.redirectUrl]];
}

@end
