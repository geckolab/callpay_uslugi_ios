//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiCardAddData.h"
#import "UpaidApiCardAddResult.h"
#import "UpaidApiCardAddDelegate.h"

@interface UpaidApiCardAddTask : UpaidApiTask

- (id) initWithData: (UpaidApiCardAddData *) taskData andDelegate: (id<UpaidApiCardAddDelegate>) taskDelegate;

enum UpaidApiCardAddStatuses : NSUInteger{
  kUpaidApiCardAddSuccess = 1,
  kUpaidApiCardAddIncorrectExpDateOrPAN = 2,
  kUpaidApiCardAddCardAlreadyRegistered = 3,
  kUpaidApiCardAddCardsLimitReached = 4,
  kUpaidApiCardAddCardOutsidePoland = 6,
  kUpaidApiCardAddTechnicalError = 7,
};
        
@end
