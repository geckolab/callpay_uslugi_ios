//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiCardAddDelegate <NSObject>

- (void) onUpaidApiCardAddSuccess: (UpaidApiCardAddResult *) result;
- (void) onUpaidApiCardAddFail: (UpaidApiCardAddResult *) result;

@optional
- (void) onUpaidApiCardAddIncorrectExpDateOrPAN: (UpaidApiCardAddResult *) result;
- (void) onUpaidApiCardAddCardAlreadyRegistered: (UpaidApiCardAddResult *) result;
- (void) onUpaidApiCardAddCardsLimitReached: (UpaidApiCardAddResult *) result;
- (void) onUpaidApiCardAddCardOutsidePoland: (UpaidApiCardAddResult *) result;
- (void) onUpaidApiCardAddTechnicalError: (UpaidApiCardAddResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiCardAddDelegate implementation

- (void) onUpaidApiCardAddSuccess: (UpaidApiCardAddResult *) result {

}

- (void) onUpaidApiCardAddFail: (UpaidApiCardAddResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiCardAddDelegate implementation

- (void) onUpaidApiCardAddSuccess: (UpaidApiCardAddResult *) result {

}

- (void) onUpaidApiCardAddFail: (UpaidApiCardAddResult *) result {

}

- (void) onUpaidApiCardAddIncorrectExpDateOrPAN: (UpaidApiCardAddResult *) result {

}

- (void) onUpaidApiCardAddCardAlreadyRegistered: (UpaidApiCardAddResult *) result {

}

- (void) onUpaidApiCardAddCardsLimitReached: (UpaidApiCardAddResult *) result {

}

- (void) onUpaidApiCardAddCardOutsidePoland: (UpaidApiCardAddResult *) result {

}

- (void) onUpaidApiCardAddTechnicalError: (UpaidApiCardAddResult *) result {

}


//-------END SELECT-------
*/
@end
