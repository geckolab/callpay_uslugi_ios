//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiCardAddTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "AddressLong.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "singleShoppingCartItem.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiCardAddTask ()

@property UpaidApiCardAddData *data;
@property id<UpaidApiCardAddDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiCardAddTask


- (id) initWithData: (UpaidApiCardAddData *) taskData andDelegate: (id<UpaidApiCardAddDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"CardAdd data is empty.");
  } else {
    UpaidLog(@"CardAdd data:\n%@", self.data);
  }
  
  [self.proxy cardAdd: [UpaidApi sessionToken] : self.data.pan : [self expDate] : self.data.nickname ];
}

- (NSString *) expDate {
  if (self.data.expYear > 100) {
    int x = self.data.expYear / 100;
    self.data.expYear -= x * 100;
  }
  
  return [NSString stringWithFormat: @"%d/%d", self.data.expMonth, self.data.expYear];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiCardAddResult *result = [[UpaidApiCardAddResult alloc] init];
  result.status = kUpaidApiCardAddTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiCardAddTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiCardAddTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiCardAddFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiCardAddFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiCardAddResult *result = [[UpaidApiCardAddResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiCardAddSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiCardAddSuccess:)]) {
        if (data[@"id"] != nil) {
            result.cardId = [data[@"id"] intValue];
        }

        if (data[@"card"] != nil) {
            Card *wsObject = [[Card alloc] initWithArray: data[@"card"]];
            result.card = [[UpaidApiCard alloc] initWithWsClass: wsObject];
        }


        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiCardAddSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiCardAddIncorrectExpDateOrPAN && [self.delegate respondsToSelector:@selector(onUpaidApiCardAddIncorrectExpDateOrPAN:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardAddIncorrectExpDateOrPAN:) withObject: result];
      } else if (result.status == kUpaidApiCardAddCardAlreadyRegistered && [self.delegate respondsToSelector:@selector(onUpaidApiCardAddCardAlreadyRegistered:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardAddCardAlreadyRegistered:) withObject: result];
      } else if (result.status == kUpaidApiCardAddCardsLimitReached && [self.delegate respondsToSelector:@selector(onUpaidApiCardAddCardsLimitReached:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardAddCardsLimitReached:) withObject: result];
      } else if (result.status == kUpaidApiCardAddCardOutsidePoland && [self.delegate respondsToSelector:@selector(onUpaidApiCardAddCardOutsidePoland:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardAddCardOutsidePoland:) withObject: result];
      } else if (result.status == kUpaidApiCardAddTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiCardAddTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardAddTechnicalError:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiCardAddFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardAddFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiCardAddResult *) result  {
    UpaidLog(@"CardAdd result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiCardAddResult *result = [[UpaidApiCardAddResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiCardAddFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardAddFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"CardAdd" withStatus:@"fail"];
    }
}

@end
