//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiCard.h"

@interface UpaidApiCardAddData : NSObject

@property NSString *pan; 
@property int expMonth; 
@property int expYear; 
@property NSString *nickname; 


//only required params
- (id) initWithPan: (NSString *) pan andExpMonth: (int ) expMonth andExpYear: (int ) expYear andNickname: (NSString *) nickname;


@end