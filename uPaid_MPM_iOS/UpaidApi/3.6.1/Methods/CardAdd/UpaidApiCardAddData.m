//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiCardAddData.h"
#import "NSObject+WSClass.h"
#import "AddressLong.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "singleShoppingCartItem.h"
#import "transactionData.h"
#import "transactionStatus.h"


@implementation UpaidApiCardAddData

- (id) initWithPan: (NSString *) pan andExpMonth: (int ) expMonth andExpYear: (int ) expYear andNickname: (NSString *) nickname {
  self = [super init];
  
  if (self) {
    self.pan = pan;
    self.expMonth = expMonth;
    self.expYear = expYear;
    self.nickname = nickname;
  }
  
  return self;
}



- (NSString *) description {
    return [NSString stringWithFormat: @"pan=%@\nexpMonth=%@\nexpYear=%@\nnickname=%@", self.pan, [NSString stringWithFormat: @"%d", self.expMonth], [NSString stringWithFormat: @"%d", self.expYear], self.nickname];
}

@end