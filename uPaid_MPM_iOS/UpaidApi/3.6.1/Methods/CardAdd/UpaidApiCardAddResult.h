//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiResult.h"
#import "UpaidApiCard.h"

@interface UpaidApiCardAddResult : UpaidApiResult

@property int cardId;

@property UpaidApiCard *card;



@end
