//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiCardAddResult.h"
#import "UpaidApiCardAddTask.h"
#import "AddressLong.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "singleShoppingCartItem.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiCardAddResult ()


@end

@implementation UpaidApiCardAddResult

@synthesize status = _status;

- (id) init {
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

- (void) setStatus: (NSInteger) status {
  _status = status;
  
  if (self.status == kUpaidApiCardAddSuccess) {
    self.statusDescription = @"";
  }

  if (self.status == kUpaidApiCardAddIncorrectExpDateOrPAN) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.CardAdd.IncorrectExpDateOrPAN", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiCardAddCardAlreadyRegistered) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.CardAdd.CardAlreadyRegistered", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiCardAddCardsLimitReached) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.CardAdd.CardsLimitReached", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiCardAddCardOutsidePoland) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.CardAdd.CardOutsidePoland", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiCardAddTechnicalError) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.CardAdd.TechnicalError", @"UpaidApi", nil);
  }

}

- (NSString *) description {
    return [NSString stringWithFormat: @"status=%ld\n%@", (long)self.status, [NSString stringWithFormat: @"cardId=%@\ncard=%@", [NSString stringWithFormat: @"%d", self.cardId], self.card]];
}

@end
