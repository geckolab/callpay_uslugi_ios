//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiRegisterTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "AddressLong.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "singleShoppingCartItem.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiRegisterTask ()

@property UpaidApiRegisterData *data;
@property id<UpaidApiRegisterDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiRegisterTask


- (id) initWithData: (UpaidApiRegisterData *) taskData andDelegate: (id<UpaidApiRegisterDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"Register data is empty.");
  } else {
    UpaidLog(@"Register data:\n%@", self.data);
  }
  
  [self.proxy register: [UpaidApi partnerId] : self.data.phone : self.data.email : self.data.firstName : self.data.lastName : self.data.pan : [self expDate] : [NSString stringWithFormat: @"%d", self.data.mailing] : self.data.birthDate : self.data.gender : self.data.password ];
}

- (NSString *) expDate {
  if (self.data.expYear > 100) {
    int x = self.data.expYear / 100;
    self.data.expYear -= x * 100;
  }
  
  return [NSString stringWithFormat: @"%d/%d", self.data.expMonth, self.data.expYear];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiRegisterResult *result = [[UpaidApiRegisterResult alloc] init];
  result.status = kUpaidApiRegisterTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiRegisterTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiRegisterTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiRegisterFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiRegisterFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiRegisterResult *result = [[UpaidApiRegisterResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiRegisterSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiRegisterSuccess:)]) {

        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiRegisterSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiRegisterEmailExist && [self.delegate respondsToSelector:@selector(onUpaidApiRegisterEmailExist:)]) {
        [self.delegate performSelector:@selector(onUpaidApiRegisterEmailExist:) withObject: result];
      } else if (result.status == kUpaidApiRegisterEmailIsIncorrect && [self.delegate respondsToSelector:@selector(onUpaidApiRegisterEmailIsIncorrect:)]) {
        [self.delegate performSelector:@selector(onUpaidApiRegisterEmailIsIncorrect:) withObject: result];
      } else if (result.status == kUpaidApiRegisterCardAlreadyRegistered && [self.delegate respondsToSelector:@selector(onUpaidApiRegisterCardAlreadyRegistered:)]) {
        [self.delegate performSelector:@selector(onUpaidApiRegisterCardAlreadyRegistered:) withObject: result];
      } else if (result.status == kUpaidApiRegisterIncorrectPAN && [self.delegate respondsToSelector:@selector(onUpaidApiRegisterIncorrectPAN:)]) {
        [self.delegate performSelector:@selector(onUpaidApiRegisterIncorrectPAN:) withObject: result];
      } else if (result.status == kUpaidApiRegisterTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiRegisterTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiRegisterTechnicalError:) withObject: result];
      } else if (result.status == kUpaidApiRegisterIncorrectInputData && [self.delegate respondsToSelector:@selector(onUpaidApiRegisterIncorrectInputData:)]) {
        [self.delegate performSelector:@selector(onUpaidApiRegisterIncorrectInputData:) withObject: result];
      } else if (result.status == kUpaidApiRegisterIncorrectPassword && [self.delegate respondsToSelector:@selector(onUpaidApiRegisterIncorrectPassword:)]) {
        [self.delegate performSelector:@selector(onUpaidApiRegisterIncorrectPassword:) withObject: result];
      } else if (result.status == kUpaidApiRegisterNoPartnerFound && [self.delegate respondsToSelector:@selector(onUpaidApiRegisterNoPartnerFound:)]) {
        [self.delegate performSelector:@selector(onUpaidApiRegisterNoPartnerFound:) withObject: result];
      } else if (result.status == kUpaidApiRegisterBadPlatform && [self.delegate respondsToSelector:@selector(onUpaidApiRegisterBadPlatform:)]) {
        [self.delegate performSelector:@selector(onUpaidApiRegisterBadPlatform:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiRegisterFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiRegisterFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiRegisterResult *) result  {
    UpaidLog(@"Register result:\n%@", result);
}


@end
