//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiRegisterResult.h"
#import "UpaidApiRegisterTask.h"
#import "AddressLong.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "singleShoppingCartItem.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiRegisterResult ()


@end

@implementation UpaidApiRegisterResult

@synthesize status = _status;

- (id) init {
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

- (void) setStatus: (NSInteger) status {
  _status = status;
  
  if (self.status == kUpaidApiRegisterSuccess) {
    self.statusDescription = @"";
  }

  if (self.status == kUpaidApiRegisterEmailExist) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Register.EmailExist", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiRegisterEmailIsIncorrect) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Register.EmailIsIncorrect", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiRegisterCardAlreadyRegistered) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Register.CardAlreadyRegistered", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiRegisterIncorrectPAN) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Register.IncorrectPAN", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiRegisterTechnicalError) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Register.TechnicalError", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiRegisterIncorrectInputData) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Register.IncorrectInputData", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiRegisterIncorrectPassword) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Register.IncorrectPassword", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiRegisterNoPartnerFound) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Register.NoPartnerFound", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiRegisterBadPlatform) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.Register.BadPlatform", @"UpaidApi", nil);
  }

}

- (NSString *) description {
    return [NSString stringWithFormat: @"status=%ld\n%@", (long)self.status, @""];
}

@end
