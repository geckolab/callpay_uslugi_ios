//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiRegisterData.h"
#import "UpaidApiRegisterResult.h"
#import "UpaidApiRegisterDelegate.h"

@interface UpaidApiRegisterTask : UpaidApiTask

- (id) initWithData: (UpaidApiRegisterData *) taskData andDelegate: (id<UpaidApiRegisterDelegate>) taskDelegate;

enum UpaidApiRegisterStatuses : NSUInteger{
  kUpaidApiRegisterSuccess = 1,
  kUpaidApiRegisterEmailExist = 2,
  kUpaidApiRegisterEmailIsIncorrect = 3,
  kUpaidApiRegisterCardAlreadyRegistered = 4,
  kUpaidApiRegisterIncorrectPAN = 5,
  kUpaidApiRegisterTechnicalError = 7,
  kUpaidApiRegisterIncorrectInputData = 8,
  kUpaidApiRegisterIncorrectPassword = 9,
  kUpaidApiRegisterNoPartnerFound = 721,
  kUpaidApiRegisterBadPlatform = 722,
};
        
@end
