//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiRegisterData.h"
#import "NSObject+WSClass.h"
#import "AddressLong.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "singleShoppingCartItem.h"
#import "transactionData.h"
#import "transactionStatus.h"


@implementation UpaidApiRegisterData

- (id) initWithPhone: (NSString *) phone andEmail: (NSString *) email andFirstName: (NSString *) firstName andLastName: (NSString *) lastName andPan: (NSString *) pan andExpMonth: (int ) expMonth andExpYear: (int ) expYear andMailing: (int ) mailing andBirthDate: (NSString *) birthDate andGender: (NSString *) gender andPassword: (NSString *) password {
  self = [super init];
  
  if (self) {
    self.phone = phone;
    self.email = email;
    self.firstName = firstName;
    self.lastName = lastName;
    self.pan = pan;
    self.expMonth = expMonth;
    self.expYear = expYear;
    self.mailing = mailing;
    self.birthDate = birthDate;
    self.gender = gender;
    self.password = password;
  }
  
  return self;
}

- (id) initWithPhone: (NSString *) phone andEmail: (NSString *) email andFirstName: (NSString *) firstName andLastName: (NSString *) lastName andMailing: (int ) mailing{
  self = [super init];
  
  if (self) {
    self.phone = phone;
    self.email = email;
    self.firstName = firstName;
    self.lastName = lastName;
    self.pan = @"";
    self.expMonth = 0;
    self.expYear = 0;
    self.mailing = mailing;
    self.birthDate = @"";
    self.gender = @"";
    self.password = @"";
  }
  
  return self;
}


- (NSString *) description {
    return [NSString stringWithFormat: @"phone=%@\nemail=%@\nfirstName=%@\nlastName=%@\npan=%@\nexpMonth=%@\nexpYear=%@\nmailing=%@\nbirthDate=%@\ngender=%@\npassword=%@", self.phone, self.email, self.firstName, self.lastName, self.pan, [NSString stringWithFormat: @"%d", self.expMonth], [NSString stringWithFormat: @"%d", self.expYear], [NSString stringWithFormat: @"%d", self.mailing], self.birthDate, self.gender, self.password];
}

@end