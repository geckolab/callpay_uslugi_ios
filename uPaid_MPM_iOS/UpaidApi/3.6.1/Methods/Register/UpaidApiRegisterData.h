//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpaidApiRegisterData : NSObject

@property NSString *phone; 
@property NSString *email; 
@property NSString *firstName; 
@property NSString *lastName; 
@property NSString *pan; //optional
@property int expMonth; //optional
@property int expYear; //optional
@property int mailing; 
@property NSString *birthDate; //optional
@property NSString *gender; //optional
@property NSString *password; //optional


//only required params
- (id) initWithPhone: (NSString *) phone andEmail: (NSString *) email andFirstName: (NSString *) firstName andLastName: (NSString *) lastName andMailing: (int ) mailing;
//required + optional params
- (id) initWithPhone: (NSString *) phone andEmail: (NSString *) email andFirstName: (NSString *) firstName andLastName: (NSString *) lastName andPan: (NSString *) pan andExpMonth: (int ) expMonth andExpYear: (int ) expYear andMailing: (int ) mailing andBirthDate: (NSString *) birthDate andGender: (NSString *) gender andPassword: (NSString *) password;


@end