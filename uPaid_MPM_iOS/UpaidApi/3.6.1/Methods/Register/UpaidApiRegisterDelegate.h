//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiRegisterDelegate <NSObject>

- (void) onUpaidApiRegisterSuccess: (UpaidApiRegisterResult *) result;
- (void) onUpaidApiRegisterFail: (UpaidApiRegisterResult *) result;

@optional
- (void) onUpaidApiRegisterEmailExist: (UpaidApiRegisterResult *) result;
- (void) onUpaidApiRegisterEmailIsIncorrect: (UpaidApiRegisterResult *) result;
- (void) onUpaidApiRegisterCardAlreadyRegistered: (UpaidApiRegisterResult *) result;
- (void) onUpaidApiRegisterIncorrectPAN: (UpaidApiRegisterResult *) result;
- (void) onUpaidApiRegisterTechnicalError: (UpaidApiRegisterResult *) result;
- (void) onUpaidApiRegisterIncorrectInputData: (UpaidApiRegisterResult *) result;
- (void) onUpaidApiRegisterIncorrectPassword: (UpaidApiRegisterResult *) result;
- (void) onUpaidApiRegisterNoPartnerFound: (UpaidApiRegisterResult *) result;
- (void) onUpaidApiRegisterBadPlatform: (UpaidApiRegisterResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiRegisterDelegate implementation

- (void) onUpaidApiRegisterSuccess: (UpaidApiRegisterResult *) result {

}

- (void) onUpaidApiRegisterFail: (UpaidApiRegisterResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiRegisterDelegate implementation

- (void) onUpaidApiRegisterSuccess: (UpaidApiRegisterResult *) result {

}

- (void) onUpaidApiRegisterFail: (UpaidApiRegisterResult *) result {

}

- (void) onUpaidApiRegisterEmailExist: (UpaidApiRegisterResult *) result {

}

- (void) onUpaidApiRegisterEmailIsIncorrect: (UpaidApiRegisterResult *) result {

}

- (void) onUpaidApiRegisterCardAlreadyRegistered: (UpaidApiRegisterResult *) result {

}

- (void) onUpaidApiRegisterIncorrectPAN: (UpaidApiRegisterResult *) result {

}

- (void) onUpaidApiRegisterTechnicalError: (UpaidApiRegisterResult *) result {

}

- (void) onUpaidApiRegisterIncorrectInputData: (UpaidApiRegisterResult *) result {

}

- (void) onUpaidApiRegisterIncorrectPassword: (UpaidApiRegisterResult *) result {

}

- (void) onUpaidApiRegisterNoPartnerFound: (UpaidApiRegisterResult *) result {

}

- (void) onUpaidApiRegisterBadPlatform: (UpaidApiRegisterResult *) result {

}


//-------END SELECT-------
*/
@end
