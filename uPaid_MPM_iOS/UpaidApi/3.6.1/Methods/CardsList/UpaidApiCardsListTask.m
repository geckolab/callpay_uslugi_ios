//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiCardsListTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiCardsListTask ()

@property UpaidApiCardsListData *data;
@property id<UpaidApiCardsListDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiCardsListTask


- (id) initWithData: (UpaidApiCardsListData *) taskData andDelegate: (id<UpaidApiCardsListDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"CardsList data is empty.");
  } else {
    UpaidLog(@"CardsList data:\n%@", self.data);
  }
  
  [self.proxy cardsList: [UpaidApi sessionToken] ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiCardsListResult *result = [[UpaidApiCardsListResult alloc] init];
  result.status = kUpaidApiCardsListTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiCardsListTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiCardsListTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiCardsListFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiCardsListFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiCardsListResult *result = [[UpaidApiCardsListResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiCardsListSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiCardsListSuccess:)]) {
        if (data[@"cards"] != nil) {
            result.cards = [[UpaidApiCardArray alloc] init];

            for (int i = 0; i < [(NSDictionary *)data[@"cards"] count]; i++) {
                Card *wsObject = [[Card alloc] initWithArray: [[data[@"cards"] objectAtIndex: i] objectForKey:@"nodeChildArray"]];
                UpaidApiCard * object = [[UpaidApiCard alloc] initWithWsClass: wsObject];

                [result.cards addObject: object];
            }
            [result.cards prepareArray];
        }


        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiCardsListSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiCardsListNoCards && [self.delegate respondsToSelector:@selector(onUpaidApiCardsListNoCards:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardsListNoCards:) withObject: result];
      } else if (result.status == kUpaidApiCardsListTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiCardsListTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardsListTechnicalError:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiCardsListFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardsListFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiCardsListResult *) result  {
    UpaidLog(@"CardsList result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiCardsListResult *result = [[UpaidApiCardsListResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiCardsListFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardsListFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"CardsList" withStatus:@"fail"];
    }
}

@end
