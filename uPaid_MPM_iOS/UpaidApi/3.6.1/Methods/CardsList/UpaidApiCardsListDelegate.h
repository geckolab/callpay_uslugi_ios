//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiCardsListDelegate <NSObject>

- (void) onUpaidApiCardsListSuccess: (UpaidApiCardsListResult *) result;
- (void) onUpaidApiCardsListFail: (UpaidApiCardsListResult *) result;

@optional
- (void) onUpaidApiCardsListNoCards: (UpaidApiCardsListResult *) result;
- (void) onUpaidApiCardsListTechnicalError: (UpaidApiCardsListResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiCardsListDelegate implementation

- (void) onUpaidApiCardsListSuccess: (UpaidApiCardsListResult *) result {

}

- (void) onUpaidApiCardsListFail: (UpaidApiCardsListResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiCardsListDelegate implementation

- (void) onUpaidApiCardsListSuccess: (UpaidApiCardsListResult *) result {

}

- (void) onUpaidApiCardsListFail: (UpaidApiCardsListResult *) result {

}

- (void) onUpaidApiCardsListNoCards: (UpaidApiCardsListResult *) result {

}

- (void) onUpaidApiCardsListTechnicalError: (UpaidApiCardsListResult *) result {

}


//-------END SELECT-------
*/
@end
