//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiCardsListData.h"
#import "UpaidApiCardsListResult.h"
#import "UpaidApiCardsListDelegate.h"

@interface UpaidApiCardsListTask : UpaidApiTask

- (id) initWithData: (UpaidApiCardsListData *) taskData andDelegate: (id<UpaidApiCardsListDelegate>) taskDelegate;

enum UpaidApiCardsListStatuses : NSUInteger{
  kUpaidApiCardsListSuccess = 1,
  kUpaidApiCardsListNoCards = 2,
  kUpaidApiCardsListTechnicalError = 7,
};
        
@end
