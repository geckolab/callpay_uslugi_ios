//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiReceiptRemoveTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiReceiptRemoveTask ()

@property UpaidApiReceiptRemoveData *data;
@property id<UpaidApiReceiptRemoveDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiReceiptRemoveTask


- (id) initWithData: (UpaidApiReceiptRemoveData *) taskData andDelegate: (id<UpaidApiReceiptRemoveDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"ReceiptRemove data is empty.");
  } else {
    UpaidLog(@"ReceiptRemove data:\n%@", self.data);
  }
  
  [self.proxy receiptRemove: [UpaidApi sessionToken] : [NSString stringWithFormat: @"%d", self.data.receiptId] : (NSMutableArray *)[self.data.extras wsClass] ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiReceiptRemoveResult *result = [[UpaidApiReceiptRemoveResult alloc] init];
  result.status = kUpaidApiReceiptRemoveTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiReceiptRemoveTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiReceiptRemoveTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiReceiptRemoveFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiReceiptRemoveFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiReceiptRemoveResult *result = [[UpaidApiReceiptRemoveResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiReceiptRemoveSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiReceiptRemoveSuccess:)]) {

        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiReceiptRemoveSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiReceiptRemoveOperationFailed && [self.delegate respondsToSelector:@selector(onUpaidApiReceiptRemoveOperationFailed:)]) {
        [self.delegate performSelector:@selector(onUpaidApiReceiptRemoveOperationFailed:) withObject: result];
      } else if (result.status == kUpaidApiReceiptRemoveReceiptIdIncorrect && [self.delegate respondsToSelector:@selector(onUpaidApiReceiptRemoveReceiptIdIncorrect:)]) {
        [self.delegate performSelector:@selector(onUpaidApiReceiptRemoveReceiptIdIncorrect:) withObject: result];
      } else if (result.status == kUpaidApiReceiptRemoveTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiReceiptRemoveTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiReceiptRemoveTechnicalError:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiReceiptRemoveFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiReceiptRemoveFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiReceiptRemoveResult *) result  {
    UpaidLog(@"ReceiptRemove result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiReceiptRemoveResult *result = [[UpaidApiReceiptRemoveResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiReceiptRemoveFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiReceiptRemoveFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"ReceiptRemove" withStatus:@"fail"];
    }
}

@end
