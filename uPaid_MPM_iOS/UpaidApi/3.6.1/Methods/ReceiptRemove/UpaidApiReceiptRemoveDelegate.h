//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiReceiptRemoveDelegate <NSObject>

- (void) onUpaidApiReceiptRemoveSuccess: (UpaidApiReceiptRemoveResult *) result;
- (void) onUpaidApiReceiptRemoveFail: (UpaidApiReceiptRemoveResult *) result;

@optional
- (void) onUpaidApiReceiptRemoveOperationFailed: (UpaidApiReceiptRemoveResult *) result;
- (void) onUpaidApiReceiptRemoveReceiptIdIncorrect: (UpaidApiReceiptRemoveResult *) result;
- (void) onUpaidApiReceiptRemoveTechnicalError: (UpaidApiReceiptRemoveResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiReceiptRemoveDelegate implementation

- (void) onUpaidApiReceiptRemoveSuccess: (UpaidApiReceiptRemoveResult *) result {

}

- (void) onUpaidApiReceiptRemoveFail: (UpaidApiReceiptRemoveResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiReceiptRemoveDelegate implementation

- (void) onUpaidApiReceiptRemoveSuccess: (UpaidApiReceiptRemoveResult *) result {

}

- (void) onUpaidApiReceiptRemoveFail: (UpaidApiReceiptRemoveResult *) result {

}

- (void) onUpaidApiReceiptRemoveOperationFailed: (UpaidApiReceiptRemoveResult *) result {

}

- (void) onUpaidApiReceiptRemoveReceiptIdIncorrect: (UpaidApiReceiptRemoveResult *) result {

}

- (void) onUpaidApiReceiptRemoveTechnicalError: (UpaidApiReceiptRemoveResult *) result {

}


//-------END SELECT-------
*/
@end
