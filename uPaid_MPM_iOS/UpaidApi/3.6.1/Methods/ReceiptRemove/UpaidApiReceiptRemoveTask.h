//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiReceiptRemoveData.h"
#import "UpaidApiReceiptRemoveResult.h"
#import "UpaidApiReceiptRemoveDelegate.h"

@interface UpaidApiReceiptRemoveTask : UpaidApiTask

- (id) initWithData: (UpaidApiReceiptRemoveData *) taskData andDelegate: (id<UpaidApiReceiptRemoveDelegate>) taskDelegate;

enum UpaidApiReceiptRemoveStatuses : NSUInteger{
  kUpaidApiReceiptRemoveSuccess = 1,
  kUpaidApiReceiptRemoveOperationFailed = 2,
  kUpaidApiReceiptRemoveReceiptIdIncorrect = 3,
  kUpaidApiReceiptRemoveTechnicalError = 7,
};
        
@end
