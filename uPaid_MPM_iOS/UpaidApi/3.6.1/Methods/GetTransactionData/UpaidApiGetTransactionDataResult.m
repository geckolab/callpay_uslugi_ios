//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiGetTransactionDataResult.h"
#import "UpaidApiGetTransactionDataTask.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@implementation UpaidApiGetTransactionDataResult

@synthesize status = _status;

- (id) init {
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

- (void) setStatus: (NSInteger) status {
  _status = status;
  
  if (self.status == kUpaidApiGetTransactionDataSuccess) {
    self.statusDescription = @"";
  }

  if (self.status == kUpaidApiGetTransactionDataTechnicalError) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.GetTransactionData.TechnicalError", @"UpaidApi", nil);
  }
}

- (NSString *) description {
    return [NSString stringWithFormat: @"status=%ld\n%@", (long)self.status, [NSString stringWithFormat: @"upaidTransactionId=%@\nphone=%@\ncardId=%@\ntransactionId=%@\namount=%@\nmore=%@", [NSString stringWithFormat: @"%d", self.upaidTransactionId], self.phone, self.cardId, self.transactionId, [NSString stringWithFormat: @"%d", self.amount], self.more]];
}

@end
