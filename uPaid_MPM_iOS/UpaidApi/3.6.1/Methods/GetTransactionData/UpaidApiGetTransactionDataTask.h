//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiGetTransactionDataData.h"
#import "UpaidApiGetTransactionDataResult.h"
#import "UpaidApiGetTransactionDataDelegate.h"

@interface UpaidApiGetTransactionDataTask : UpaidApiTask

- (id) initWithData: (UpaidApiGetTransactionDataData *) taskData andDelegate: (id<UpaidApiGetTransactionDataDelegate>) taskDelegate;

enum UpaidApiGetTransactionDataStatuses : NSUInteger{
  kUpaidApiGetTransactionDataSuccess = 1,
  kUpaidApiGetTransactionDataTechnicalError = 7,
};
        
@end
