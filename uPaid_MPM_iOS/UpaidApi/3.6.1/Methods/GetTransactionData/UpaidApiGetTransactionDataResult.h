//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiResult.h"

@interface UpaidApiGetTransactionDataResult : UpaidApiResult

@property int upaidTransactionId;

@property NSString *phone;

@property NSString *cardId;

@property NSString *transactionId;

@property int amount;

@property NSString *more;



@end
