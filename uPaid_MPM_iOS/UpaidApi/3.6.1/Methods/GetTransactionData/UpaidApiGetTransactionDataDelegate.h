//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiGetTransactionDataDelegate <NSObject>

- (void) onUpaidApiGetTransactionDataSuccess: (UpaidApiGetTransactionDataResult *) result;
- (void) onUpaidApiGetTransactionDataFail: (UpaidApiGetTransactionDataResult *) result;

@optional
- (void) onUpaidApiGetTransactionDataTechnicalError: (UpaidApiGetTransactionDataResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiGetTransactionDataDelegate implementation

- (void) onUpaidApiGetTransactionDataSuccess: (UpaidApiGetTransactionDataResult *) result {

}

- (void) onUpaidApiGetTransactionDataFail: (UpaidApiGetTransactionDataResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiGetTransactionDataDelegate implementation

- (void) onUpaidApiGetTransactionDataSuccess: (UpaidApiGetTransactionDataResult *) result {

}

- (void) onUpaidApiGetTransactionDataFail: (UpaidApiGetTransactionDataResult *) result {

}

- (void) onUpaidApiGetTransactionDataTechnicalError: (UpaidApiGetTransactionDataResult *) result {

}


//-------END SELECT-------
*/
@end
