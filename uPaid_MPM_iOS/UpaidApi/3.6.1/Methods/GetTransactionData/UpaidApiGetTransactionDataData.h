//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpaidApiGetTransactionDataData : NSObject

@property BOOL isUpaid; 


//only required params
- (id) initWithIsUpaid: (BOOL ) isUpaid;


@end