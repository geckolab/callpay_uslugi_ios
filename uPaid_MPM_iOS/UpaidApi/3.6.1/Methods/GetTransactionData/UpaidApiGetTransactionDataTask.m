//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiGetTransactionDataTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiGetTransactionDataTask ()

@property UpaidApiGetTransactionDataData *data;
@property id<UpaidApiGetTransactionDataDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiGetTransactionDataTask


- (id) initWithData: (UpaidApiGetTransactionDataData *) taskData andDelegate: (id<UpaidApiGetTransactionDataDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"GetTransactionData data is empty.");
  } else {
    UpaidLog(@"GetTransactionData data:\n%@", self.data);
  }
  
  [self.proxy getTransactionData: [UpaidApi sessionToken] : [NSString stringWithFormat: @"%d", self.data.isUpaid] ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiGetTransactionDataResult *result = [[UpaidApiGetTransactionDataResult alloc] init];
  result.status = kUpaidApiGetTransactionDataTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiGetTransactionDataTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiGetTransactionDataTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiGetTransactionDataFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiGetTransactionDataFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiGetTransactionDataResult *result = [[UpaidApiGetTransactionDataResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiGetTransactionDataSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiGetTransactionDataSuccess:)]) {
        if (data[@"id"] != nil) {
            result.upaidTransactionId = [data[@"id"] intValue];
        }

        if (data[@"phone"] != nil) {
            result.phone = data[@"phone"];
        }

        if (data[@"cardId"] != nil) {
            result.cardId = data[@"cardId"];
        }
        if (data[@"card_id"] != nil) {
            result.cardId = data[@"card_id"];
        }

        if (data[@"transactionId"] != nil) {
            result.transactionId = data[@"transactionId"];
        }
        if (data[@"transaction_id"] != nil) {
            result.transactionId = data[@"transaction_id"];
        }

        if (data[@"amount"] != nil) {
            result.amount = [data[@"amount"] intValue];
        }

        if (data[@"more"] != nil) {
            result.more = data[@"more"];
        }


        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiGetTransactionDataSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiGetTransactionDataTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiGetTransactionDataTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiGetTransactionDataTechnicalError:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiGetTransactionDataFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiGetTransactionDataFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiGetTransactionDataResult *) result  {
    UpaidLog(@"GetTransactionData result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiGetTransactionDataResult *result = [[UpaidApiGetTransactionDataResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiGetTransactionDataFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiGetTransactionDataFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"GetTransactionData" withStatus:@"fail"];
    }
}

@end
