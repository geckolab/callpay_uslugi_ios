//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiCardAuthorizeDelegate <NSObject>

- (void) onUpaidApiCardAuthorizeSuccess: (UpaidApiCardAuthorizeResult *) result;
- (void) onUpaidApiCardAuthorizeFail: (UpaidApiCardAuthorizeResult *) result;

@optional
- (void) onUpaidApiCardAuthorizeRejected: (UpaidApiCardAuthorizeResult *) result;
- (void) onUpaidApiCardAuthorizeCardNoExist: (UpaidApiCardAuthorizeResult *) result;
- (void) onUpaidApiCardAuthorizeCardVerified: (UpaidApiCardAuthorizeResult *) result;
- (void) onUpaidApiCardAuthorizeTechnicalError: (UpaidApiCardAuthorizeResult *) result;
- (void) onUpaidApiCardAuthorizeIncorrectCode: (UpaidApiCardAuthorizeResult *) result;
- (void) onUpaidApiCardAuthorizeCardBlocked: (UpaidApiCardAuthorizeResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiCardAuthorizeDelegate implementation

- (void) onUpaidApiCardAuthorizeSuccess: (UpaidApiCardAuthorizeResult *) result {

}

- (void) onUpaidApiCardAuthorizeFail: (UpaidApiCardAuthorizeResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiCardAuthorizeDelegate implementation

- (void) onUpaidApiCardAuthorizeSuccess: (UpaidApiCardAuthorizeResult *) result {

}

- (void) onUpaidApiCardAuthorizeFail: (UpaidApiCardAuthorizeResult *) result {

}

- (void) onUpaidApiCardAuthorizeRejected: (UpaidApiCardAuthorizeResult *) result {

}

- (void) onUpaidApiCardAuthorizeCardNoExist: (UpaidApiCardAuthorizeResult *) result {

}

- (void) onUpaidApiCardAuthorizeCardVerified: (UpaidApiCardAuthorizeResult *) result {

}

- (void) onUpaidApiCardAuthorizeTechnicalError: (UpaidApiCardAuthorizeResult *) result {

}

- (void) onUpaidApiCardAuthorizeIncorrectCode: (UpaidApiCardAuthorizeResult *) result {

}

- (void) onUpaidApiCardAuthorizeCardBlocked: (UpaidApiCardAuthorizeResult *) result {

}


//-------END SELECT-------
*/
@end
