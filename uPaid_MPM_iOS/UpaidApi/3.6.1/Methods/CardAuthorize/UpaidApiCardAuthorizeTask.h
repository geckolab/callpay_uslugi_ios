//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiCardAuthorizeData.h"
#import "UpaidApiCardAuthorizeResult.h"
#import "UpaidApiCardAuthorizeDelegate.h"

@interface UpaidApiCardAuthorizeTask : UpaidApiTask

- (id) initWithData: (UpaidApiCardAuthorizeData *) taskData andDelegate: (id<UpaidApiCardAuthorizeDelegate>) taskDelegate;

enum UpaidApiCardAuthorizeStatuses : NSUInteger{
  kUpaidApiCardAuthorizeSuccess = 1,
  kUpaidApiCardAuthorizeRejected = 2,
  kUpaidApiCardAuthorizeCardNoExist = 3,
  kUpaidApiCardAuthorizeCardVerified = 4,
  kUpaidApiCardAuthorizeTechnicalError = 7,
  kUpaidApiCardAuthorizeIncorrectCode = 8,
  kUpaidApiCardAuthorizeCardBlocked = 9,
};
        
@end
