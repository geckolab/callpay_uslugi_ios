//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiCardAuthorizeTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiCardAuthorizeTask ()

@property UpaidApiCardAuthorizeData *data;
@property id<UpaidApiCardAuthorizeDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiCardAuthorizeTask


- (id) initWithData: (UpaidApiCardAuthorizeData *) taskData andDelegate: (id<UpaidApiCardAuthorizeDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"CardAuthorize data is empty.");
  } else {
    UpaidLog(@"CardAuthorize data:\n%@", self.data);
  }
  
  [self.proxy cardAuthorize: [UpaidApi sessionToken] : [NSString stringWithFormat: @"%d", self.data.code] : self.data.cardId : (self.data.mpin == 0 ? @"" : [NSString stringWithFormat: @"%d", self.data.mpin]) ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiCardAuthorizeResult *result = [[UpaidApiCardAuthorizeResult alloc] init];
  result.status = kUpaidApiCardAuthorizeTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiCardAuthorizeTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiCardAuthorizeTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiCardAuthorizeFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiCardAuthorizeFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiCardAuthorizeResult *result = [[UpaidApiCardAuthorizeResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiCardAuthorizeSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiCardAuthorizeSuccess:)]) {

        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiCardAuthorizeSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiCardAuthorizeRejected && [self.delegate respondsToSelector:@selector(onUpaidApiCardAuthorizeRejected:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardAuthorizeRejected:) withObject: result];
      } else if (result.status == kUpaidApiCardAuthorizeCardNoExist && [self.delegate respondsToSelector:@selector(onUpaidApiCardAuthorizeCardNoExist:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardAuthorizeCardNoExist:) withObject: result];
      } else if (result.status == kUpaidApiCardAuthorizeCardVerified && [self.delegate respondsToSelector:@selector(onUpaidApiCardAuthorizeCardVerified:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardAuthorizeCardVerified:) withObject: result];
      } else if (result.status == kUpaidApiCardAuthorizeTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiCardAuthorizeTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardAuthorizeTechnicalError:) withObject: result];
      } else if (result.status == kUpaidApiCardAuthorizeIncorrectCode && [self.delegate respondsToSelector:@selector(onUpaidApiCardAuthorizeIncorrectCode:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardAuthorizeIncorrectCode:) withObject: result];
      } else if (result.status == kUpaidApiCardAuthorizeCardBlocked && [self.delegate respondsToSelector:@selector(onUpaidApiCardAuthorizeCardBlocked:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardAuthorizeCardBlocked:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiCardAuthorizeFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardAuthorizeFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiCardAuthorizeResult *) result  {
    UpaidLog(@"CardAuthorize result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiCardAuthorizeResult *result = [[UpaidApiCardAuthorizeResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiCardAuthorizeFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardAuthorizeFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"CardAuthorize" withStatus:@"fail"];
    }
}

@end
