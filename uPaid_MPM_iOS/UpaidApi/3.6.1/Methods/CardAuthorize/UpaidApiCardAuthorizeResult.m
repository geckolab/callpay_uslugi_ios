//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiCardAuthorizeResult.h"
#import "UpaidApiCardAuthorizeTask.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@implementation UpaidApiCardAuthorizeResult

@synthesize status = _status;

- (id) init {
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

- (void) setStatus: (NSInteger) status {
  _status = status;
  
  if (self.status == kUpaidApiCardAuthorizeSuccess) {
    self.statusDescription = @"";
  }

  if (self.status == kUpaidApiCardAuthorizeRejected) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.CardAuthorize.Rejected", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiCardAuthorizeCardNoExist) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.CardAuthorize.CardNoExist", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiCardAuthorizeCardVerified) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.CardAuthorize.CardVerified", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiCardAuthorizeTechnicalError) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.CardAuthorize.TechnicalError", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiCardAuthorizeIncorrectCode) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.CardAuthorize.IncorrectCode", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiCardAuthorizeCardBlocked) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.CardAuthorize.CardBlocked", @"UpaidApi", nil);
  }
}

- (NSString *) description {
    return [NSString stringWithFormat: @"status=%ld\n%@", (long)self.status, @""];
}

@end
