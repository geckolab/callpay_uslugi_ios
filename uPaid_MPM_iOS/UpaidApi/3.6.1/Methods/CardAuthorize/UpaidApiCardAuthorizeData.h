//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpaidApiCardAuthorizeData : NSObject

@property int code; 
@property NSString *cardId; 
@property int mpin; 


//only required params
- (id) initWithCode: (int ) code andCardId: (NSString *) cardId andMpin: (int ) mpin;


@end