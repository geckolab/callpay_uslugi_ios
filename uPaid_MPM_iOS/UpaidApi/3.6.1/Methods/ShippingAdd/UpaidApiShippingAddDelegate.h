//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiShippingAddDelegate <NSObject>

- (void) onUpaidApiShippingAddSuccess: (UpaidApiShippingAddResult *) result;
- (void) onUpaidApiShippingAddFail: (UpaidApiShippingAddResult *) result;

@optional
- (void) onUpaidApiShippingAddOperationFailed: (UpaidApiShippingAddResult *) result;
- (void) onUpaidApiShippingAddTechnicalError: (UpaidApiShippingAddResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiShippingAddDelegate implementation

- (void) onUpaidApiShippingAddSuccess: (UpaidApiShippingAddResult *) result {

}

- (void) onUpaidApiShippingAddFail: (UpaidApiShippingAddResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiShippingAddDelegate implementation

- (void) onUpaidApiShippingAddSuccess: (UpaidApiShippingAddResult *) result {

}

- (void) onUpaidApiShippingAddFail: (UpaidApiShippingAddResult *) result {

}

- (void) onUpaidApiShippingAddOperationFailed: (UpaidApiShippingAddResult *) result {

}

- (void) onUpaidApiShippingAddTechnicalError: (UpaidApiShippingAddResult *) result {

}


//-------END SELECT-------
*/
@end
