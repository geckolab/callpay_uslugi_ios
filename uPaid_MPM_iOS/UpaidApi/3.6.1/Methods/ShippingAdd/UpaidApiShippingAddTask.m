//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiShippingAddTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "AddressLong.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiAddressLong.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "singleShoppingCartItem.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiShippingAddTask ()

@property UpaidApiShippingAddData *data;
@property id<UpaidApiShippingAddDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiShippingAddTask


- (id) initWithData: (UpaidApiShippingAddData *) taskData andDelegate: (id<UpaidApiShippingAddDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"ShippingAdd data is empty.");
  } else {
    UpaidLog(@"ShippingAdd data:\n%@", self.data);
  }
  
  [self.proxy shippingAdd: [UpaidApi sessionToken] : (AddressLong *)[self.data.address wsClass] ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiShippingAddResult *result = [[UpaidApiShippingAddResult alloc] init];
  result.status = kUpaidApiShippingAddTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiShippingAddTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiShippingAddTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiShippingAddFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiShippingAddFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiShippingAddResult *result = [[UpaidApiShippingAddResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiShippingAddSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiShippingAddSuccess:)]) {

        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiShippingAddSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiShippingAddOperationFailed && [self.delegate respondsToSelector:@selector(onUpaidApiShippingAddOperationFailed:)]) {
        [self.delegate performSelector:@selector(onUpaidApiShippingAddOperationFailed:) withObject: result];
      } else if (result.status == kUpaidApiShippingAddTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiShippingAddTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiShippingAddTechnicalError:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiShippingAddFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiShippingAddFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiShippingAddResult *) result  {
    UpaidLog(@"ShippingAdd result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiShippingAddResult *result = [[UpaidApiShippingAddResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiShippingAddFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiShippingAddFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"ShippingAdd" withStatus:@"fail"];
    }
}

@end
