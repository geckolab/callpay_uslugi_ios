//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiShippingAddData.h"
#import "UpaidApiShippingAddResult.h"
#import "UpaidApiShippingAddDelegate.h"

@interface UpaidApiShippingAddTask : UpaidApiTask

- (id) initWithData: (UpaidApiShippingAddData *) taskData andDelegate: (id<UpaidApiShippingAddDelegate>) taskDelegate;

enum UpaidApiShippingAddStatuses : NSUInteger{
  kUpaidApiShippingAddSuccess = 1,
  kUpaidApiShippingAddOperationFailed = 2,
  kUpaidApiShippingAddTechnicalError = 7,
};
        
@end
