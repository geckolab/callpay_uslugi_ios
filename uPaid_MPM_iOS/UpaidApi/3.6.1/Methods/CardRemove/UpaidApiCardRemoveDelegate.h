//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiCardRemoveDelegate <NSObject>

- (void) onUpaidApiCardRemoveSuccess: (UpaidApiCardRemoveResult *) result;
- (void) onUpaidApiCardRemoveFail: (UpaidApiCardRemoveResult *) result;

@optional
- (void) onUpaidApiCardRemoveCantRemove: (UpaidApiCardRemoveResult *) result;
- (void) onUpaidApiCardRemoveTechnicalError: (UpaidApiCardRemoveResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiCardRemoveDelegate implementation

- (void) onUpaidApiCardRemoveSuccess: (UpaidApiCardRemoveResult *) result {

}

- (void) onUpaidApiCardRemoveFail: (UpaidApiCardRemoveResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiCardRemoveDelegate implementation

- (void) onUpaidApiCardRemoveSuccess: (UpaidApiCardRemoveResult *) result {

}

- (void) onUpaidApiCardRemoveFail: (UpaidApiCardRemoveResult *) result {

}

- (void) onUpaidApiCardRemoveCantRemove: (UpaidApiCardRemoveResult *) result {

}

- (void) onUpaidApiCardRemoveTechnicalError: (UpaidApiCardRemoveResult *) result {

}


//-------END SELECT-------
*/
@end
