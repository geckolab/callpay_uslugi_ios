//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiCardRemoveTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiCardRemoveTask ()

@property UpaidApiCardRemoveData *data;
@property id<UpaidApiCardRemoveDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiCardRemoveTask


- (id) initWithData: (UpaidApiCardRemoveData *) taskData andDelegate: (id<UpaidApiCardRemoveDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"CardRemove data is empty.");
  } else {
    UpaidLog(@"CardRemove data:\n%@", self.data);
  }
  
  [self.proxy cardRemove: [UpaidApi sessionToken] : self.data.cardId ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiCardRemoveResult *result = [[UpaidApiCardRemoveResult alloc] init];
  result.status = kUpaidApiCardRemoveTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiCardRemoveTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiCardRemoveTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiCardRemoveFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiCardRemoveFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiCardRemoveResult *result = [[UpaidApiCardRemoveResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiCardRemoveSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiCardRemoveSuccess:)]) {

        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiCardRemoveSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiCardRemoveCantRemove && [self.delegate respondsToSelector:@selector(onUpaidApiCardRemoveCantRemove:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardRemoveCantRemove:) withObject: result];
      } else if (result.status == kUpaidApiCardRemoveTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiCardRemoveTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardRemoveTechnicalError:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiCardRemoveFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardRemoveFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiCardRemoveResult *) result  {
    UpaidLog(@"CardRemove result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiCardRemoveResult *result = [[UpaidApiCardRemoveResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiCardRemoveFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardRemoveFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"CardRemove" withStatus:@"fail"];
    }
}

@end
