//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiCardRemoveData.h"
#import "UpaidApiCardRemoveResult.h"
#import "UpaidApiCardRemoveDelegate.h"

@interface UpaidApiCardRemoveTask : UpaidApiTask

- (id) initWithData: (UpaidApiCardRemoveData *) taskData andDelegate: (id<UpaidApiCardRemoveDelegate>) taskDelegate;

enum UpaidApiCardRemoveStatuses : NSUInteger{
  kUpaidApiCardRemoveSuccess = 1,
  kUpaidApiCardRemoveCantRemove = 2,
  kUpaidApiCardRemoveTechnicalError = 7,
};
        
@end
