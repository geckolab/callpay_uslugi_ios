//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiDocumentRemoveTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiDocumentRemoveTask ()

@property UpaidApiDocumentRemoveData *data;
@property id<UpaidApiDocumentRemoveDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiDocumentRemoveTask


- (id) initWithData: (UpaidApiDocumentRemoveData *) taskData andDelegate: (id<UpaidApiDocumentRemoveDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"DocumentRemove data is empty.");
  } else {
    UpaidLog(@"DocumentRemove data:\n%@", self.data);
  }
  
  [self.proxy documentRemove: [UpaidApi sessionToken] : [NSString stringWithFormat: @"%d", self.data.documentId] : (NSMutableArray *)[self.data.extras wsClass] ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiDocumentRemoveResult *result = [[UpaidApiDocumentRemoveResult alloc] init];
  result.status = kUpaidApiDocumentRemoveTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiDocumentRemoveTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiDocumentRemoveTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiDocumentRemoveFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiDocumentRemoveFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiDocumentRemoveResult *result = [[UpaidApiDocumentRemoveResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiDocumentRemoveSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiDocumentRemoveSuccess:)]) {

        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiDocumentRemoveSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiDocumentRemoveOperationFailed && [self.delegate respondsToSelector:@selector(onUpaidApiDocumentRemoveOperationFailed:)]) {
        [self.delegate performSelector:@selector(onUpaidApiDocumentRemoveOperationFailed:) withObject: result];
      } else if (result.status == kUpaidApiDocumentRemoveDocumentIdIncorrect && [self.delegate respondsToSelector:@selector(onUpaidApiDocumentRemoveDocumentIdIncorrect:)]) {
        [self.delegate performSelector:@selector(onUpaidApiDocumentRemoveDocumentIdIncorrect:) withObject: result];
      } else if (result.status == kUpaidApiDocumentRemoveTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiDocumentRemoveTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiDocumentRemoveTechnicalError:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiDocumentRemoveFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiDocumentRemoveFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiDocumentRemoveResult *) result  {
    UpaidLog(@"DocumentRemove result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiDocumentRemoveResult *result = [[UpaidApiDocumentRemoveResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiDocumentRemoveFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiDocumentRemoveFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"DocumentRemove" withStatus:@"fail"];
    }
}

@end
