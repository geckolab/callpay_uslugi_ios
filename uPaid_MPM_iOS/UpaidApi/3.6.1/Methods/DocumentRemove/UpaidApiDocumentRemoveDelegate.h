//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiDocumentRemoveDelegate <NSObject>

- (void) onUpaidApiDocumentRemoveSuccess: (UpaidApiDocumentRemoveResult *) result;
- (void) onUpaidApiDocumentRemoveFail: (UpaidApiDocumentRemoveResult *) result;

@optional
- (void) onUpaidApiDocumentRemoveOperationFailed: (UpaidApiDocumentRemoveResult *) result;
- (void) onUpaidApiDocumentRemoveDocumentIdIncorrect: (UpaidApiDocumentRemoveResult *) result;
- (void) onUpaidApiDocumentRemoveTechnicalError: (UpaidApiDocumentRemoveResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiDocumentRemoveDelegate implementation

- (void) onUpaidApiDocumentRemoveSuccess: (UpaidApiDocumentRemoveResult *) result {

}

- (void) onUpaidApiDocumentRemoveFail: (UpaidApiDocumentRemoveResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiDocumentRemoveDelegate implementation

- (void) onUpaidApiDocumentRemoveSuccess: (UpaidApiDocumentRemoveResult *) result {

}

- (void) onUpaidApiDocumentRemoveFail: (UpaidApiDocumentRemoveResult *) result {

}

- (void) onUpaidApiDocumentRemoveOperationFailed: (UpaidApiDocumentRemoveResult *) result {

}

- (void) onUpaidApiDocumentRemoveDocumentIdIncorrect: (UpaidApiDocumentRemoveResult *) result {

}

- (void) onUpaidApiDocumentRemoveTechnicalError: (UpaidApiDocumentRemoveResult *) result {

}


//-------END SELECT-------
*/
@end
