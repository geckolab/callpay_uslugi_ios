//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiExtrasSingleParam.h"
#import "UpaidApiExtrasSingleParamArray.h"

@interface UpaidApiDocumentRemoveData : NSObject

@property int documentId; 
@property UpaidApiExtrasSingleParamArray *extras; 


//only required params
- (id) initWithDocumentId: (int ) documentId andExtras: (UpaidApiExtrasSingleParamArray *) extras;


@end