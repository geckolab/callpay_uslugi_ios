//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiDocumentRemoveData.h"
#import "UpaidApiDocumentRemoveResult.h"
#import "UpaidApiDocumentRemoveDelegate.h"

@interface UpaidApiDocumentRemoveTask : UpaidApiTask

- (id) initWithData: (UpaidApiDocumentRemoveData *) taskData andDelegate: (id<UpaidApiDocumentRemoveDelegate>) taskDelegate;

enum UpaidApiDocumentRemoveStatuses : NSUInteger{
  kUpaidApiDocumentRemoveSuccess = 1,
  kUpaidApiDocumentRemoveOperationFailed = 2,
  kUpaidApiDocumentRemoveDocumentIdIncorrect = 3,
  kUpaidApiDocumentRemoveTechnicalError = 7,
};
        
@end
