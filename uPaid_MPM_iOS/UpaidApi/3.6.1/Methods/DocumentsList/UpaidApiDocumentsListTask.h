//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiDocumentsListData.h"
#import "UpaidApiDocumentsListResult.h"
#import "UpaidApiDocumentsListDelegate.h"

@interface UpaidApiDocumentsListTask : UpaidApiTask

- (id) initWithData: (UpaidApiDocumentsListData *) taskData andDelegate: (id<UpaidApiDocumentsListDelegate>) taskDelegate;

enum UpaidApiDocumentsListStatuses : NSUInteger{
  kUpaidApiDocumentsListSuccess = 1,
  kUpaidApiDocumentsListOperationFailed = 2,
  kUpaidApiDocumentsListTechnicalError = 7,
};
        
@end
