//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiDocumentsListTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiDocumentsListTask ()

@property UpaidApiDocumentsListData *data;
@property id<UpaidApiDocumentsListDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiDocumentsListTask


- (id) initWithData: (UpaidApiDocumentsListData *) taskData andDelegate: (id<UpaidApiDocumentsListDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"DocumentsList data is empty.");
  } else {
    UpaidLog(@"DocumentsList data:\n%@", self.data);
  }
  
  [self.proxy documentsList: [UpaidApi sessionToken] : self.data.type : (NSMutableArray *)[self.data.extras wsClass] ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiDocumentsListResult *result = [[UpaidApiDocumentsListResult alloc] init];
  result.status = kUpaidApiDocumentsListTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiDocumentsListTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiDocumentsListTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiDocumentsListFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiDocumentsListFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiDocumentsListResult *result = [[UpaidApiDocumentsListResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiDocumentsListSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiDocumentsListSuccess:)]) {
        if (data[@"documents"] != nil) {
            result.documents = [[UpaidApiDocumentArray alloc] init];

            for (int i = 0; i < [(NSDictionary *)data[@"documents"] count]; i++) {
                Document *wsObject = [[Document alloc] initWithArray: [[data[@"documents"] objectAtIndex: i] objectForKey:@"nodeChildArray"]];
                UpaidApiDocument * object = [[UpaidApiDocument alloc] initWithWsClass: wsObject];

                [result.documents addObject: object];
            }

        }


        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiDocumentsListSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiDocumentsListOperationFailed && [self.delegate respondsToSelector:@selector(onUpaidApiDocumentsListOperationFailed:)]) {
        [self.delegate performSelector:@selector(onUpaidApiDocumentsListOperationFailed:) withObject: result];
      } else if (result.status == kUpaidApiDocumentsListTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiDocumentsListTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiDocumentsListTechnicalError:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiDocumentsListFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiDocumentsListFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiDocumentsListResult *) result  {
    UpaidLog(@"DocumentsList result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiDocumentsListResult *result = [[UpaidApiDocumentsListResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiDocumentsListFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiDocumentsListFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"DocumentsList" withStatus:@"fail"];
    }
}

@end
