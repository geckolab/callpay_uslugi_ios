//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiExtrasSingleParam.h"
#import "UpaidApiExtrasSingleParamArray.h"
#import "UpaidApiDocument.h"
#import "UpaidApiDocumentArray.h"

@interface UpaidApiDocumentsListData : NSObject

@property NSString *type; 
@property UpaidApiExtrasSingleParamArray *extras; 


//only required params
- (id) initWithType: (NSString *) type andExtras: (UpaidApiExtrasSingleParamArray *) extras;


@end