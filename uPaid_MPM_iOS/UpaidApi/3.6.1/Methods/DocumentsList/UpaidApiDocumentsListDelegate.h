//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiDocumentsListDelegate <NSObject>

- (void) onUpaidApiDocumentsListSuccess: (UpaidApiDocumentsListResult *) result;
- (void) onUpaidApiDocumentsListFail: (UpaidApiDocumentsListResult *) result;

@optional
- (void) onUpaidApiDocumentsListOperationFailed: (UpaidApiDocumentsListResult *) result;
- (void) onUpaidApiDocumentsListTechnicalError: (UpaidApiDocumentsListResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiDocumentsListDelegate implementation

- (void) onUpaidApiDocumentsListSuccess: (UpaidApiDocumentsListResult *) result {

}

- (void) onUpaidApiDocumentsListFail: (UpaidApiDocumentsListResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiDocumentsListDelegate implementation

- (void) onUpaidApiDocumentsListSuccess: (UpaidApiDocumentsListResult *) result {

}

- (void) onUpaidApiDocumentsListFail: (UpaidApiDocumentsListResult *) result {

}

- (void) onUpaidApiDocumentsListOperationFailed: (UpaidApiDocumentsListResult *) result {

}

- (void) onUpaidApiDocumentsListTechnicalError: (UpaidApiDocumentsListResult *) result {

}


//-------END SELECT-------
*/
@end
