//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiDocumentsListData.h"
#import "NSObject+WSClass.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@implementation UpaidApiDocumentsListData

- (id) initWithType: (NSString *) type andExtras: (UpaidApiExtrasSingleParamArray *) extras {
  self = [super init];
  
  if (self) {
    self.type = type;
    self.extras = extras;
  }
  
  return self;
}



- (NSString *) description {
    return [NSString stringWithFormat: @"type=%@\nextras=%@", self.type, self.extras];
}

@end