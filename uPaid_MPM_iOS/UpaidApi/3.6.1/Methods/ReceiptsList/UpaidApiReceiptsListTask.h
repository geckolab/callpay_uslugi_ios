//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiReceiptsListData.h"
#import "UpaidApiReceiptsListResult.h"
#import "UpaidApiReceiptsListDelegate.h"

@interface UpaidApiReceiptsListTask : UpaidApiTask

- (id) initWithData: (UpaidApiReceiptsListData *) taskData andDelegate: (id<UpaidApiReceiptsListDelegate>) taskDelegate;

enum UpaidApiReceiptsListStatuses : NSUInteger{
  kUpaidApiReceiptsListSuccess = 1,
  kUpaidApiReceiptsListOperationFailed = 2,
  kUpaidApiReceiptsListTechnicalError = 7,
};
        
@end
