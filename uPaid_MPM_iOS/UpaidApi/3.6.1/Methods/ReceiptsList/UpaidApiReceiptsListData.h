//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiReceipt.h"
#import "UpaidApiReceiptArray.h"

@interface UpaidApiReceiptsListData : NSObject

@property NSString *filter; 


//only required params
- (id) initWithFilter: (NSString *) filter;


@end