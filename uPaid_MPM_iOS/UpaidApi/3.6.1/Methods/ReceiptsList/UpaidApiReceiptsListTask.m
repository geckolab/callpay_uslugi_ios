//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiReceiptsListTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiReceiptsListTask ()

@property UpaidApiReceiptsListData *data;
@property id<UpaidApiReceiptsListDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiReceiptsListTask


- (id) initWithData: (UpaidApiReceiptsListData *) taskData andDelegate: (id<UpaidApiReceiptsListDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"ReceiptsList data is empty.");
  } else {
    UpaidLog(@"ReceiptsList data:\n%@", self.data);
  }
  
  [self.proxy receiptsList: [UpaidApi sessionToken] : self.data.filter ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiReceiptsListResult *result = [[UpaidApiReceiptsListResult alloc] init];
  result.status = kUpaidApiReceiptsListTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiReceiptsListTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiReceiptsListTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiReceiptsListFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiReceiptsListFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiReceiptsListResult *result = [[UpaidApiReceiptsListResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiReceiptsListSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiReceiptsListSuccess:)]) {
        if (data[@"receipts"] != nil) {
            result.receipts = [[UpaidApiReceiptArray alloc] init];

            for (int i = 0; i < [(NSDictionary *)data[@"receipts"] count]; i++) {
                Receipt *wsObject = [[Receipt alloc] initWithArray: [[data[@"receipts"] objectAtIndex: i] objectForKey:@"nodeChildArray"]];
                UpaidApiReceipt * object = [[UpaidApiReceipt alloc] initWithWsClass: wsObject];

                [result.receipts addObject: object];
            }

        }


        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiReceiptsListSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiReceiptsListOperationFailed && [self.delegate respondsToSelector:@selector(onUpaidApiReceiptsListOperationFailed:)]) {
        [self.delegate performSelector:@selector(onUpaidApiReceiptsListOperationFailed:) withObject: result];
      } else if (result.status == kUpaidApiReceiptsListTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiReceiptsListTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiReceiptsListTechnicalError:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiReceiptsListFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiReceiptsListFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiReceiptsListResult *) result  {
    UpaidLog(@"ReceiptsList result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiReceiptsListResult *result = [[UpaidApiReceiptsListResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiReceiptsListFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiReceiptsListFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"ReceiptsList" withStatus:@"fail"];
    }
}

@end
