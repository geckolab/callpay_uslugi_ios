//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiReceiptsListDelegate <NSObject>

- (void) onUpaidApiReceiptsListSuccess: (UpaidApiReceiptsListResult *) result;
- (void) onUpaidApiReceiptsListFail: (UpaidApiReceiptsListResult *) result;

@optional
- (void) onUpaidApiReceiptsListOperationFailed: (UpaidApiReceiptsListResult *) result;
- (void) onUpaidApiReceiptsListTechnicalError: (UpaidApiReceiptsListResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiReceiptsListDelegate implementation

- (void) onUpaidApiReceiptsListSuccess: (UpaidApiReceiptsListResult *) result {

}

- (void) onUpaidApiReceiptsListFail: (UpaidApiReceiptsListResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiReceiptsListDelegate implementation

- (void) onUpaidApiReceiptsListSuccess: (UpaidApiReceiptsListResult *) result {

}

- (void) onUpaidApiReceiptsListFail: (UpaidApiReceiptsListResult *) result {

}

- (void) onUpaidApiReceiptsListOperationFailed: (UpaidApiReceiptsListResult *) result {

}

- (void) onUpaidApiReceiptsListTechnicalError: (UpaidApiReceiptsListResult *) result {

}


//-------END SELECT-------
*/
@end
