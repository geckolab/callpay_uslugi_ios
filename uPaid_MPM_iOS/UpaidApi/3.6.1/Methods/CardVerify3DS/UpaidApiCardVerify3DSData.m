//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiCardVerify3DSData.h"
#import "NSObject+WSClass.h"
#import "AddressLong.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiAddressLong.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "singleShoppingCartItem.h"
#import "transactionData.h"
#import "transactionStatus.h"


@implementation UpaidApiCardVerify3DSData

- (id) initWithCardId: (NSString *) cardId andPaymentId: (int ) paymentId andCvc: (NSString *) cvc andMpin: (NSString *) mpin andPares: (NSString *) pares {
  self = [super init];
  
  if (self) {
    self.cardId = cardId;
    self.paymentId = paymentId;
    self.cvc = cvc;
    self.mpin = mpin;
    self.pares = pares;
  }
  
  return self;
}



- (NSString *) description {
    return [NSString stringWithFormat: @"cardId=%@\npaymentId=%@\ncvc=%@\nmpin=%@\npares=%@", self.cardId, [NSString stringWithFormat: @"%d", self.paymentId], self.cvc, self.mpin, self.pares];
}

@end