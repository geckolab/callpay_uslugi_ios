//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiCardVerify3DSTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "AddressLong.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiAddressLong.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "singleShoppingCartItem.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiCardVerify3DSTask ()

@property UpaidApiCardVerify3DSData *data;
@property id<UpaidApiCardVerify3DSDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiCardVerify3DSTask


- (id) initWithData: (UpaidApiCardVerify3DSData *) taskData andDelegate: (id<UpaidApiCardVerify3DSDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"CardVerify3DS data is empty.");
  } else {
    UpaidLog(@"CardVerify3DS data:\n%@", self.data);
  }
  
  [self.proxy cardVerify3DS: [UpaidApi sessionToken] : self.data.cardId : [NSString stringWithFormat: @"%d", self.data.paymentId] : self.data.cvc : self.data.mpin : self.data.pares ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiCardVerify3DSResult *result = [[UpaidApiCardVerify3DSResult alloc] init];
  result.status = kUpaidApiCardVerify3DSTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiCardVerify3DSTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiCardVerify3DSTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiCardVerify3DSFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiCardVerify3DSFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiCardVerify3DSResult *result = [[UpaidApiCardVerify3DSResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiCardVerify3DSSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiCardVerify3DSSuccess:)]) {

        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiCardVerify3DSSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiCardVerify3DSRejected && [self.delegate respondsToSelector:@selector(onUpaidApiCardVerify3DSRejected:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardVerify3DSRejected:) withObject: result];
      } else if (result.status == kUpaidApiCardVerify3DSCardNoExist && [self.delegate respondsToSelector:@selector(onUpaidApiCardVerify3DSCardNoExist:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardVerify3DSCardNoExist:) withObject: result];
      } else if (result.status == kUpaidApiCardVerify3DSAuthorizationFailed && [self.delegate respondsToSelector:@selector(onUpaidApiCardVerify3DSAuthorizationFailed:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardVerify3DSAuthorizationFailed:) withObject: result];
      } else if (result.status == kUpaidApiCardVerify3DSTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiCardVerify3DSTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardVerify3DSTechnicalError:) withObject: result];
      } else if (result.status == kUpaidApiCardVerify3DSCvcBadFormat && [self.delegate respondsToSelector:@selector(onUpaidApiCardVerify3DSCvcBadFormat:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardVerify3DSCvcBadFormat:) withObject: result];
      } else if (result.status == kUpaidApiCardVerify3DSMpinBadFormat && [self.delegate respondsToSelector:@selector(onUpaidApiCardVerify3DSMpinBadFormat:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardVerify3DSMpinBadFormat:) withObject: result];
      } else if (result.status == kUpaidApiCardVerify3DSNoPartnerFound && [self.delegate respondsToSelector:@selector(onUpaidApiCardVerify3DSNoPartnerFound:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardVerify3DSNoPartnerFound:) withObject: result];
      } else if (result.status == kUpaidApiCardVerify3DSBadPlatform && [self.delegate respondsToSelector:@selector(onUpaidApiCardVerify3DSBadPlatform:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardVerify3DSBadPlatform:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiCardVerify3DSFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardVerify3DSFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiCardVerify3DSResult *) result  {
    UpaidLog(@"CardVerify3DS result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiCardVerify3DSResult *result = [[UpaidApiCardVerify3DSResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiCardVerify3DSFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiCardVerify3DSFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"CardVerify3DS" withStatus:@"fail"];
    }
}

@end
