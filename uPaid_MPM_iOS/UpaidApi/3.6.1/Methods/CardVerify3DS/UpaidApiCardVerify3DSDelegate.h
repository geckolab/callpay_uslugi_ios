//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiCardVerify3DSDelegate <NSObject>

- (void) onUpaidApiCardVerify3DSSuccess: (UpaidApiCardVerify3DSResult *) result;
- (void) onUpaidApiCardVerify3DSFail: (UpaidApiCardVerify3DSResult *) result;

@optional
- (void) onUpaidApiCardVerify3DSRejected: (UpaidApiCardVerify3DSResult *) result;
- (void) onUpaidApiCardVerify3DSCardNoExist: (UpaidApiCardVerify3DSResult *) result;
- (void) onUpaidApiCardVerify3DSAuthorizationFailed: (UpaidApiCardVerify3DSResult *) result;
- (void) onUpaidApiCardVerify3DSTechnicalError: (UpaidApiCardVerify3DSResult *) result;
- (void) onUpaidApiCardVerify3DSCvcBadFormat: (UpaidApiCardVerify3DSResult *) result;
- (void) onUpaidApiCardVerify3DSMpinBadFormat: (UpaidApiCardVerify3DSResult *) result;
- (void) onUpaidApiCardVerify3DSNoPartnerFound: (UpaidApiCardVerify3DSResult *) result;
- (void) onUpaidApiCardVerify3DSBadPlatform: (UpaidApiCardVerify3DSResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiCardVerify3DSDelegate implementation

- (void) onUpaidApiCardVerify3DSSuccess: (UpaidApiCardVerify3DSResult *) result {

}

- (void) onUpaidApiCardVerify3DSFail: (UpaidApiCardVerify3DSResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiCardVerify3DSDelegate implementation

- (void) onUpaidApiCardVerify3DSSuccess: (UpaidApiCardVerify3DSResult *) result {

}

- (void) onUpaidApiCardVerify3DSFail: (UpaidApiCardVerify3DSResult *) result {

}

- (void) onUpaidApiCardVerify3DSRejected: (UpaidApiCardVerify3DSResult *) result {

}

- (void) onUpaidApiCardVerify3DSCardNoExist: (UpaidApiCardVerify3DSResult *) result {

}

- (void) onUpaidApiCardVerify3DSAuthorizationFailed: (UpaidApiCardVerify3DSResult *) result {

}

- (void) onUpaidApiCardVerify3DSTechnicalError: (UpaidApiCardVerify3DSResult *) result {

}

- (void) onUpaidApiCardVerify3DSCvcBadFormat: (UpaidApiCardVerify3DSResult *) result {

}

- (void) onUpaidApiCardVerify3DSMpinBadFormat: (UpaidApiCardVerify3DSResult *) result {

}

- (void) onUpaidApiCardVerify3DSNoPartnerFound: (UpaidApiCardVerify3DSResult *) result {

}

- (void) onUpaidApiCardVerify3DSBadPlatform: (UpaidApiCardVerify3DSResult *) result {

}


//-------END SELECT-------
*/
@end
