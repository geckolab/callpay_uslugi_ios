//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpaidApiCardVerify3DSData : NSObject

@property NSString *cardId; 
@property int paymentId; 
@property NSString *cvc; 
@property NSString *mpin; 
@property NSString *pares; 


//only required params
- (id) initWithCardId: (NSString *) cardId andPaymentId: (int ) paymentId andCvc: (NSString *) cvc andMpin: (NSString *) mpin andPares: (NSString *) pares;


@end