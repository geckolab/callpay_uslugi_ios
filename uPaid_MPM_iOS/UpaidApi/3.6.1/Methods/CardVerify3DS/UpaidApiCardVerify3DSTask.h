//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiCardVerify3DSData.h"
#import "UpaidApiCardVerify3DSResult.h"
#import "UpaidApiCardVerify3DSDelegate.h"

@interface UpaidApiCardVerify3DSTask : UpaidApiTask

- (id) initWithData: (UpaidApiCardVerify3DSData *) taskData andDelegate: (id<UpaidApiCardVerify3DSDelegate>) taskDelegate;

enum UpaidApiCardVerify3DSStatuses : NSUInteger{
  kUpaidApiCardVerify3DSSuccess = 1,
  kUpaidApiCardVerify3DSRejected = 2,
  kUpaidApiCardVerify3DSCardNoExist = 3,
  kUpaidApiCardVerify3DSAuthorizationFailed = 4,
  kUpaidApiCardVerify3DSTechnicalError = 7,
  kUpaidApiCardVerify3DSCvcBadFormat = 8,
  kUpaidApiCardVerify3DSMpinBadFormat = 9,
  kUpaidApiCardVerify3DSNoPartnerFound = 721,
  kUpaidApiCardVerify3DSBadPlatform = 722,
};
        
@end
