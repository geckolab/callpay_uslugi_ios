//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiCardVerify3DSResult.h"
#import "UpaidApiCardVerify3DSTask.h"
#import "AddressLong.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiAddressLong.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "singleShoppingCartItem.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiCardVerify3DSResult ()


@end

@implementation UpaidApiCardVerify3DSResult

@synthesize status = _status;

- (id) init {
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

- (void) setStatus: (NSInteger) status {
  _status = status;
  
  if (self.status == kUpaidApiCardVerify3DSSuccess) {
    self.statusDescription = @"";
  }

  if (self.status == kUpaidApiCardVerify3DSRejected) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.CardVerify3DS.Rejected", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiCardVerify3DSCardNoExist) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.CardVerify3DS.CardNoExist", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiCardVerify3DSAuthorizationFailed) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.CardVerify3DS.AuthorizationFailed", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiCardVerify3DSTechnicalError) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.CardVerify3DS.TechnicalError", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiCardVerify3DSCvcBadFormat) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.CardVerify3DS.CvcBadFormat", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiCardVerify3DSMpinBadFormat) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.CardVerify3DS.MpinBadFormat", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiCardVerify3DSNoPartnerFound) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.CardVerify3DS.NoPartnerFound", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiCardVerify3DSBadPlatform) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.CardVerify3DS.BadPlatform", @"UpaidApi", nil);
  }

}

- (NSString *) description {
    return [NSString stringWithFormat: @"status=%ld\n%@", (long)self.status, @""];
}

@end
