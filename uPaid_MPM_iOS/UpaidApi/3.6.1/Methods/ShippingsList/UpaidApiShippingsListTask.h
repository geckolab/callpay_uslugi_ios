//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiShippingsListData.h"
#import "UpaidApiShippingsListResult.h"
#import "UpaidApiShippingsListDelegate.h"

@interface UpaidApiShippingsListTask : UpaidApiTask

- (id) initWithData: (UpaidApiShippingsListData *) taskData andDelegate: (id<UpaidApiShippingsListDelegate>) taskDelegate;

enum UpaidApiShippingsListStatuses : NSUInteger{
  kUpaidApiShippingsListSuccess = 1,
  kUpaidApiShippingsListError = 2,
  kUpaidApiShippingsListTechnicalError = 7,
};
        
@end
