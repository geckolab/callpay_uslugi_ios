//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiShippingsListDelegate <NSObject>

- (void) onUpaidApiShippingsListSuccess: (UpaidApiShippingsListResult *) result;
- (void) onUpaidApiShippingsListFail: (UpaidApiShippingsListResult *) result;

@optional
- (void) onUpaidApiShippingsListError: (UpaidApiShippingsListResult *) result;
- (void) onUpaidApiShippingsListTechnicalError: (UpaidApiShippingsListResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiShippingsListDelegate implementation

- (void) onUpaidApiShippingsListSuccess: (UpaidApiShippingsListResult *) result {

}

- (void) onUpaidApiShippingsListFail: (UpaidApiShippingsListResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiShippingsListDelegate implementation

- (void) onUpaidApiShippingsListSuccess: (UpaidApiShippingsListResult *) result {

}

- (void) onUpaidApiShippingsListFail: (UpaidApiShippingsListResult *) result {

}

- (void) onUpaidApiShippingsListError: (UpaidApiShippingsListResult *) result {

}

- (void) onUpaidApiShippingsListTechnicalError: (UpaidApiShippingsListResult *) result {

}


//-------END SELECT-------
*/
@end
