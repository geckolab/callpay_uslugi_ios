//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiShippingsListTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "AddressLong.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiShippingsListTask ()

@property UpaidApiShippingsListData *data;
@property id<UpaidApiShippingsListDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiShippingsListTask


- (id) initWithData: (UpaidApiShippingsListData *) taskData andDelegate: (id<UpaidApiShippingsListDelegate>) taskDelegate {
    self = [self init];
    
    if (self) {
        [self setData:taskData];
        [self setDelegate:taskDelegate];
    }
    
    return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
    [super execute]; //potrzebne do poprawnego zarządzania pamięcią
    
    if ([[self.data description] isEqualToString: @""]) {
        UpaidLog(@"ShippingsList data is empty.");
    } else {
        UpaidLog(@"ShippingsList data:\n%@", self.data);
    }
    
    [self.proxy shippingsList: [UpaidApi sessionToken] ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    UpaidApiShippingsListResult *result = [[UpaidApiShippingsListResult alloc] init];
    result.status = kUpaidApiShippingsListTechnicalError;
    result.exception = ex;
    
    UpaidLog(@"Error: %@", result.exception);
    
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(onUpaidApiShippingsListTechnicalError:)]) {
            [self.delegate performSelector:@selector(onUpaidApiShippingsListTechnicalError:) withObject: result];
        } else if ([self.delegate respondsToSelector:@selector(onUpaidApiShippingsListFail:)]) {
            [self.delegate performSelector:@selector(onUpaidApiShippingsListFail:) withObject: result];
        } else {
            [self logUnusedCallback:method withStatus:@"error"];
        }
    }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
    UpaidApiShippingsListResult *result = [[UpaidApiShippingsListResult alloc] init];
    result.status = [(NSString *)data[@"status"] integerValue];
    
    if (self.delegate) {
        if (result.status == 5) {
            return [self.renewer renewSessionForTask: self];
        }
        
        [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
        
        if (result.status == kUpaidApiShippingsListSuccess) {
            if ([self.delegate respondsToSelector:@selector(onUpaidApiShippingsListSuccess:)]) {
                if (data[@"addresses"] != nil) {
                    result.addresses = [[UpaidApiAddressLongArray alloc] init];
                    
                    for (int i = 0; i < [(NSDictionary *)data[@"addresses"] count]; i++) {
                        AddressLong *wsObject = [[AddressLong alloc] initWithArray: [[data[@"addresses"] objectAtIndex: i] objectForKey:@"nodeChildArray"]];
                        UpaidApiAddressLong * object = [[UpaidApiAddressLong alloc] initWithWsClass: wsObject];
                        
                        [result.addresses addObject: object];
                    }
                    
                }
                
                
                [self logResult: result];
                [self.delegate performSelector:@selector(onUpaidApiShippingsListSuccess:) withObject: result];
            } else {
                [self logResult: result];
                [self logUnusedCallback:method withStatus:@"success"];
            }
        } else {
            [self logResult: result];
            if (result.status == kUpaidApiShippingsListError && [self.delegate respondsToSelector:@selector(onUpaidApiShippingsListError:)]) {
                [self.delegate performSelector:@selector(onUpaidApiShippingsListError:) withObject: result];
            } else if (result.status == kUpaidApiShippingsListTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiShippingsListTechnicalError:)]) {
                [self.delegate performSelector:@selector(onUpaidApiShippingsListTechnicalError:) withObject: result];
            } else if ([self.delegate respondsToSelector:@selector(onUpaidApiShippingsListFail:)]) {
                [self.delegate performSelector:@selector(onUpaidApiShippingsListFail:) withObject: result];
            } else {
                [self logUnusedCallback:method withStatus:@"fail"];
            }
        }
    }
    
}

- (void) logResult: (UpaidApiShippingsListResult *) result  {
    UpaidLog(@"ShippingsList result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    UpaidApiShippingsListResult *result = [[UpaidApiShippingsListResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiShippingsListFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiShippingsListFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"ShippingsList" withStatus:@"fail"];
    }
}

@end
