//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiFreezeMoneyData.h"
#import "UpaidApiFreezeMoneyResult.h"
#import "UpaidApiFreezeMoneyDelegate.h"

@interface UpaidApiFreezeMoneyTask : UpaidApiTask

- (id) initWithData: (UpaidApiFreezeMoneyData *) taskData andDelegate: (id<UpaidApiFreezeMoneyDelegate>) taskDelegate;

enum UpaidApiFreezeMoneyStatuses : NSUInteger{
  kUpaidApiFreezeMoneySuccess = 1,
  kUpaidApiFreezeMoneyNoFundsOrRejected = 2,
  kUpaidApiFreezeMoneyCardNotFound = 3,
  kUpaidApiFreezeMoneyMoneyAlreadyFreezed = 4,
  kUpaidApiFreezeMoneyTechnicalError = 7,
  kUpaidApiFreezeMoneyCardBlocked = 9,
};
        
@end
