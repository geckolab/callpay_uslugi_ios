//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiFreezeMoneyDelegate <NSObject>

- (void) onUpaidApiFreezeMoneySuccess: (UpaidApiFreezeMoneyResult *) result;
- (void) onUpaidApiFreezeMoneyFail: (UpaidApiFreezeMoneyResult *) result;

@optional
- (void) onUpaidApiFreezeMoneyNoFundsOrRejected: (UpaidApiFreezeMoneyResult *) result;
- (void) onUpaidApiFreezeMoneyCardNotFound: (UpaidApiFreezeMoneyResult *) result;
- (void) onUpaidApiFreezeMoneyMoneyAlreadyFreezed: (UpaidApiFreezeMoneyResult *) result;
- (void) onUpaidApiFreezeMoneyTechnicalError: (UpaidApiFreezeMoneyResult *) result;
- (void) onUpaidApiFreezeMoneyCardBlocked: (UpaidApiFreezeMoneyResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiFreezeMoneyDelegate implementation

- (void) onUpaidApiFreezeMoneySuccess: (UpaidApiFreezeMoneyResult *) result {

}

- (void) onUpaidApiFreezeMoneyFail: (UpaidApiFreezeMoneyResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiFreezeMoneyDelegate implementation

- (void) onUpaidApiFreezeMoneySuccess: (UpaidApiFreezeMoneyResult *) result {

}

- (void) onUpaidApiFreezeMoneyFail: (UpaidApiFreezeMoneyResult *) result {

}

- (void) onUpaidApiFreezeMoneyNoFundsOrRejected: (UpaidApiFreezeMoneyResult *) result {

}

- (void) onUpaidApiFreezeMoneyCardNotFound: (UpaidApiFreezeMoneyResult *) result {

}

- (void) onUpaidApiFreezeMoneyMoneyAlreadyFreezed: (UpaidApiFreezeMoneyResult *) result {

}

- (void) onUpaidApiFreezeMoneyTechnicalError: (UpaidApiFreezeMoneyResult *) result {

}

- (void) onUpaidApiFreezeMoneyCardBlocked: (UpaidApiFreezeMoneyResult *) result {

}


//-------END SELECT-------
*/
@end
