//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpaidApiFreezeMoneyData : NSObject

@property NSString *cvc2; 
@property NSString *cardId; 
@property int force; 


//only required params
- (id) initWithCvc2: (NSString *) cvc2 andCardId: (NSString *) cardId andForce: (int ) force;


@end