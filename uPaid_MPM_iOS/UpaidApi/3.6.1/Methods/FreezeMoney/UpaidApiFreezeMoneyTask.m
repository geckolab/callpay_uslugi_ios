//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiFreezeMoneyTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiFreezeMoneyTask ()

@property UpaidApiFreezeMoneyData *data;
@property id<UpaidApiFreezeMoneyDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiFreezeMoneyTask


- (id) initWithData: (UpaidApiFreezeMoneyData *) taskData andDelegate: (id<UpaidApiFreezeMoneyDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"FreezeMoney data is empty.");
  } else {
    UpaidLog(@"FreezeMoney data:\n%@", self.data);
  }
  
  [self.proxy freezeMoney: [UpaidApi sessionToken] : self.data.cvc2 : self.data.cardId : [NSString stringWithFormat: @"%d", self.data.force] ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiFreezeMoneyResult *result = [[UpaidApiFreezeMoneyResult alloc] init];
  result.status = kUpaidApiFreezeMoneyTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiFreezeMoneyTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiFreezeMoneyTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiFreezeMoneyFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiFreezeMoneyFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiFreezeMoneyResult *result = [[UpaidApiFreezeMoneyResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiFreezeMoneySuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiFreezeMoneySuccess:)]) {
        if (data[@"substatus"] != nil) {
            result.substatus = [data[@"substatus"] intValue];
        }


        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiFreezeMoneySuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiFreezeMoneyNoFundsOrRejected && [self.delegate respondsToSelector:@selector(onUpaidApiFreezeMoneyNoFundsOrRejected:)]) {
        [self.delegate performSelector:@selector(onUpaidApiFreezeMoneyNoFundsOrRejected:) withObject: result];
      } else if (result.status == kUpaidApiFreezeMoneyCardNotFound && [self.delegate respondsToSelector:@selector(onUpaidApiFreezeMoneyCardNotFound:)]) {
        [self.delegate performSelector:@selector(onUpaidApiFreezeMoneyCardNotFound:) withObject: result];
      } else if (result.status == kUpaidApiFreezeMoneyMoneyAlreadyFreezed && [self.delegate respondsToSelector:@selector(onUpaidApiFreezeMoneyMoneyAlreadyFreezed:)]) {
        [self.delegate performSelector:@selector(onUpaidApiFreezeMoneyMoneyAlreadyFreezed:) withObject: result];
      } else if (result.status == kUpaidApiFreezeMoneyTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiFreezeMoneyTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiFreezeMoneyTechnicalError:) withObject: result];
      } else if (result.status == kUpaidApiFreezeMoneyCardBlocked && [self.delegate respondsToSelector:@selector(onUpaidApiFreezeMoneyCardBlocked:)]) {
        [self.delegate performSelector:@selector(onUpaidApiFreezeMoneyCardBlocked:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiFreezeMoneyFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiFreezeMoneyFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiFreezeMoneyResult *) result  {
    UpaidLog(@"FreezeMoney result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiFreezeMoneyResult *result = [[UpaidApiFreezeMoneyResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiFreezeMoneyFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiFreezeMoneyFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"FreezeMoney" withStatus:@"fail"];
    }
}

@end
