//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiFreezeMoneyResult.h"
#import "UpaidApiFreezeMoneyTask.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@implementation UpaidApiFreezeMoneyResult

@synthesize status = _status;

- (id) init {
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

- (void) setStatus: (NSInteger) status {
  _status = status;
  
  if (self.status == kUpaidApiFreezeMoneySuccess) {
    self.statusDescription = @"";
  }

  if (self.status == kUpaidApiFreezeMoneyNoFundsOrRejected) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.FreezeMoney.NoFundsOrRejected", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiFreezeMoneyCardNotFound) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.FreezeMoney.CardNotFound", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiFreezeMoneyMoneyAlreadyFreezed) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.FreezeMoney.MoneyAlreadyFreezed", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiFreezeMoneyTechnicalError) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.FreezeMoney.TechnicalError", @"UpaidApi", nil);
  }

  if (self.status == kUpaidApiFreezeMoneyCardBlocked) {
    self.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.FreezeMoney.CardBlocked", @"UpaidApi", nil);
  }
}

- (NSString *) description {
    return [NSString stringWithFormat: @"status=%ld\n%@", (long)self.status, [NSString stringWithFormat: @"substatus=%@", [NSString stringWithFormat: @"%d", self.substatus]]];
}

@end
