//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpaidApiDepositData : NSObject

@property NSString *transactionId; 
@property BOOL isUpaid; 
@property int amount; 


//only required params
- (id) initWithTransactionId: (NSString *) transactionId andIsUpaid: (BOOL ) isUpaid andAmount: (int ) amount;


@end