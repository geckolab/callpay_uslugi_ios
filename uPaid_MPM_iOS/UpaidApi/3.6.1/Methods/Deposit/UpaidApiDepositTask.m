//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiDepositTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiDepositTask ()

@property UpaidApiDepositData *data;
@property id<UpaidApiDepositDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiDepositTask


- (id) initWithData: (UpaidApiDepositData *) taskData andDelegate: (id<UpaidApiDepositDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"Deposit data is empty.");
  } else {
    UpaidLog(@"Deposit data:\n%@", self.data);
  }
  
  [self.proxy deposit: [UpaidApi sessionToken] : self.data.transactionId : self.data.isUpaid : [NSString stringWithFormat: @"%d", self.data.amount] ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiDepositResult *result = [[UpaidApiDepositResult alloc] init];
  result.status = kUpaidApiDepositTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiDepositTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiDepositTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiDepositFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiDepositFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiDepositResult *result = [[UpaidApiDepositResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiDepositSuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiDepositSuccess:)]) {

        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiDepositSuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiDepositTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiDepositTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiDepositTechnicalError:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiDepositFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiDepositFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiDepositResult *) result  {
    UpaidLog(@"Deposit result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiDepositResult *result = [[UpaidApiDepositResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiDepositFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiDepositFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"Deposit" withStatus:@"fail"];
    }
}

@end
