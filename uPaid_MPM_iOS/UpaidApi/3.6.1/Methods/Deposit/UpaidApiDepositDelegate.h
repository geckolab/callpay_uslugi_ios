//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiDepositDelegate <NSObject>

- (void) onUpaidApiDepositSuccess: (UpaidApiDepositResult *) result;
- (void) onUpaidApiDepositFail: (UpaidApiDepositResult *) result;

@optional
- (void) onUpaidApiDepositTechnicalError: (UpaidApiDepositResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiDepositDelegate implementation

- (void) onUpaidApiDepositSuccess: (UpaidApiDepositResult *) result {

}

- (void) onUpaidApiDepositFail: (UpaidApiDepositResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiDepositDelegate implementation

- (void) onUpaidApiDepositSuccess: (UpaidApiDepositResult *) result {

}

- (void) onUpaidApiDepositFail: (UpaidApiDepositResult *) result {

}

- (void) onUpaidApiDepositTechnicalError: (UpaidApiDepositResult *) result {

}


//-------END SELECT-------
*/
@end
