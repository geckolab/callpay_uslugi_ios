//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiDepositData.h"
#import "UpaidApiDepositResult.h"
#import "UpaidApiDepositDelegate.h"

@interface UpaidApiDepositTask : UpaidApiTask

- (id) initWithData: (UpaidApiDepositData *) taskData andDelegate: (id<UpaidApiDepositDelegate>) taskDelegate;

enum UpaidApiDepositStatuses : NSUInteger{
  kUpaidApiDepositSuccess = 1,
  kUpaidApiDepositTechnicalError = 7,
};
        
@end
