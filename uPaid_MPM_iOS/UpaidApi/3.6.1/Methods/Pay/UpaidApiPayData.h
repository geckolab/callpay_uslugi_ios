//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiPayTransactionData.h"

@interface UpaidApiPayData : NSObject

@property UpaidApiPayTransactionData *payData; 


//only required params
- (id) initWithPayData: (UpaidApiPayTransactionData *) payData;


@end