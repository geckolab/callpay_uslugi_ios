//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiTask.h"
#import "UpaidApiPayData.h"
#import "UpaidApiPayResult.h"
#import "UpaidApiPayDelegate.h"

@interface UpaidApiPayTask : UpaidApiTask

- (id) initWithData: (UpaidApiPayData *) taskData andDelegate: (id<UpaidApiPayDelegate>) taskDelegate;

enum UpaidApiPayStatuses : NSUInteger{
  kUpaidApiPaySuccess = 1,
  kUpaidApiPayTechnicalError = 7,
};
        
@end
