//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpaidApiPayDelegate <NSObject>

- (void) onUpaidApiPaySuccess: (UpaidApiPayResult *) result;
- (void) onUpaidApiPayFail: (UpaidApiPayResult *) result;

@optional
- (void) onUpaidApiPayTechnicalError: (UpaidApiPayResult *) result;

//-------COPY&PASTE-------

//REQUIRED:

/*
//-------START SELECT-------

#pragma mark UpaidApiPayDelegate implementation

- (void) onUpaidApiPaySuccess: (UpaidApiPayResult *) result {

}

- (void) onUpaidApiPayFail: (UpaidApiPayResult *) result {

}

//-------END SELECT-------
*/


//REQUIRED + OPTIONAL

/*
//-------START SELECT-------

#pragma mark UpaidApiPayDelegate implementation

- (void) onUpaidApiPaySuccess: (UpaidApiPayResult *) result {

}

- (void) onUpaidApiPayFail: (UpaidApiPayResult *) result {

}

- (void) onUpaidApiPayTechnicalError: (UpaidApiPayResult *) result {

}


//-------END SELECT-------
*/
@end
