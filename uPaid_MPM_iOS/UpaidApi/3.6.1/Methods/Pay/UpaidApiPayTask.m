//
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o.. All rights reserved.
//

#import "UpaidApiPayTask.h"
#import "MyWsdlProxy.h"
#import "NSObject+WSClass.h"
#import "UpaidApiSessionRenewer.h"
#import "Card.h"
#import "Document.h"
#import "Photo.h"
#import "Receipt.h"
#import "SingleParam.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiAddress.h"
#import "UpaidApiBuyTransactionData.h"
#import "UpaidApiCard.h"
#import "UpaidApiConstans.h"
#import "UpaidApiPayTransactionData.h"
#import "UpaidApiShowProfile.h"
#import "address.h"
#import "buyData.h"
#import "editProfileData.h"
#import "showProfileData.h"
#import "transactionData.h"
#import "transactionStatus.h"


@interface UpaidApiPayTask ()

@property UpaidApiPayData *data;
@property id<UpaidApiPayDelegate> delegate;
@property MyWsdlProxy *proxy;
@property UpaidApiSessionRenewer *renewer;

@end

@implementation UpaidApiPayTask


- (id) initWithData: (UpaidApiPayData *) taskData andDelegate: (id<UpaidApiPayDelegate>) taskDelegate {
  self = [self init];
  
  if (self) {
    [self setData:taskData];
    [self setDelegate:taskDelegate];
  }
  
  return self;
}

- (id) init {
    self = [super init];
    
    if (self) {
        self.renewer = [[UpaidApiSessionRenewer alloc] init];
    }
    
    return self;
}

- (void) execute {
  [super execute]; //potrzebne do poprawnego zarządzania pamięcią
  
  if ([[self.data description] isEqualToString: @""]) {
    UpaidLog(@"Pay data is empty.");
  } else {
    UpaidLog(@"Pay data:\n%@", self.data);
  }
  
  [self.proxy pay: [UpaidApi sessionToken] : (transactionData *)[self.data.payData wsClass] ];
}


#pragma mark proxy management

-(void)proxyRecievedError:(NSException*)ex InMethod:(NSString*)method {
  [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
  
  UpaidApiPayResult *result = [[UpaidApiPayResult alloc] init];
  result.status = kUpaidApiPayTechnicalError;
  result.exception = ex;
  
  UpaidLog(@"Error: %@", result.exception);
  
  if (self.delegate) {
    if ([self.delegate respondsToSelector:@selector(onUpaidApiPayTechnicalError:)]) {
      [self.delegate performSelector:@selector(onUpaidApiPayTechnicalError:) withObject: result];
    } else if ([self.delegate respondsToSelector:@selector(onUpaidApiPayFail:)]) {
      [self.delegate performSelector:@selector(onUpaidApiPayFail:) withObject: result];
    } else {
      [self logUnusedCallback:method withStatus:@"error"];
    }
  }
}

-(void)proxydidFinishLoadingData:(NSMutableDictionary *)data InMethod:(NSString*)method {
  UpaidApiPayResult *result = [[UpaidApiPayResult alloc] init];
  result.status = [(NSString *)data[@"status"] integerValue];
  
  if (self.delegate) {
    if (result.status == 5) {
        return [self.renewer renewSessionForTask: self];
    }
    
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią
    
    if (result.status == kUpaidApiPaySuccess) {
      if ([self.delegate respondsToSelector:@selector(onUpaidApiPaySuccess:)]) {
        if (data[@"upaidId"] != nil) {
            result.upaidId = [data[@"upaidId"] intValue];
        }
        if (data[@"upaid_id"] != nil) {
            result.upaidId = [data[@"upaid_id"] intValue];
        }

        if (data[@"substatus"] != nil) {
            result.substatus = [data[@"substatus"] intValue];
        }


        [self logResult: result];
        [self.delegate performSelector:@selector(onUpaidApiPaySuccess:) withObject: result];
      } else {
        [self logResult: result];
        [self logUnusedCallback:method withStatus:@"success"];
      }
    } else {
      [self logResult: result];
      if (result.status == kUpaidApiPayTechnicalError && [self.delegate respondsToSelector:@selector(onUpaidApiPayTechnicalError:)]) {
        [self.delegate performSelector:@selector(onUpaidApiPayTechnicalError:) withObject: result];
      } else if ([self.delegate respondsToSelector:@selector(onUpaidApiPayFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiPayFail:) withObject: result];
      } else {
        [self logUnusedCallback:method withStatus:@"fail"];
      }
    }
  }

}

- (void) logResult: (UpaidApiPayResult *) result  {
    UpaidLog(@"Pay result:\n%@", result);
}

- (void) sessionRenewFailed {
    [self executeFinished]; //potrzebne do poprawnego zarządzania pamięcią

    UpaidApiPayResult *result = [[UpaidApiPayResult alloc] init];
    result.status = 7;
    result.statusDescription = NSLocalizedStringFromTable(@"UpaidApi.sessionExpired", @"UpaidApi", nil);
    
    if ([self.delegate respondsToSelector:@selector(onUpaidApiPayFail:)]) {
        [self.delegate performSelector:@selector(onUpaidApiPayFail:) withObject: result];
    } else {
        [self logUnusedCallback:@"Pay" withStatus:@"fail"];
    }
}

@end
