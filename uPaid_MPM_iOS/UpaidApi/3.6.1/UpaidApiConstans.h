//
//  UpaidApiConstans.h
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 Michał Majewski. All rights reserved.
//

#import "UpaidApi.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiTask+Dev.h"


#define UpaidLog(...) [UpaidApi logsEnabled] ? NSLog(__VA_ARGS__) : nil
#define UpaidLogStack(...) [UpaidApi logsEnabled] ? NSLog(@"%s\n%@", __PRETTY_FUNCTION__, [NSString stringWithFormat:__VA_ARGS__]) : nil

#define UPAID_SOAP_VERSION @"3.6"
#define UPAID_SOAP_URL_PREFIX @"https://upaid.pl/soap"
#define UPAID_SOAP_URL_TEST_PREFIX @"http://beta.upaid.pl/soap"

#define UPAID_TEST_URL_USER_MOBILE_PASS @"http://test_upaid:sB6IA89@test.upaid.pl/users/user_mobile_pass"
