//
//  UpaidMPMTexts.m
//  MpmTestApp
//
//  Created by Michał Majewski on 28.11.2014.
//  Copyright (c) 2014 uPaid. All rights reserved.
//

#import "UpaidMPMTexts.h"

@implementation UpaidMPMTexts

NSString *const Texts_RegistrationFirstNamePlaceholder = @"RegistrationFirstNamePlaceholder";
NSString *const Texts_RegistrationLastNamePlaceholder = @"RegistrationLastNamePlaceholder";
NSString *const Texts_RegistrationPhonePlaceholder = @"RegistrationPhonePlaceholder";
NSString *const Texts_RegistrationEmailPlaceholder = @"RegistrationEmailPlaceholder";
NSString *const Texts_RegistrationBirthDatePlaceholder = @"RegistrationBirthDatePlaceholder";
NSString *const Texts_RegistrationPasswordPlaceholder = @"RegistrationPasswordPlaceholder";
NSString *const Texts_RegistrationConfirmPasswordPlaceholder = @"RegistrationConfirmPasswordPlaceholder";
NSString *const Texts_RegistrationGenderMale = @"RegistrationGenderMale";
NSString *const Texts_RegistrationGenderFemale = @"RegistrationGenderFemale";
NSString *const Texts_RegistrationRulesText = @"RegistrationRulesText";
NSString *const Texts_RegistrationMarketingText = @"RegistrationMarketingText";
NSString *const Texts_RegistrationTitleLabel = @"RegistrationTitleLabel";
NSString *const Texts_RegistrationFirstNameError = @"RegistrationFirstNameError";
NSString *const Texts_RegistrationLastNameError = @"RegistrationLastNameError";
NSString *const Texts_RegistrationPhoneError = @"RegistrationPhoneError";
NSString *const Texts_RegistrationEmailError = @"RegistrationEmailError";
NSString *const Texts_RegistrationBirthDateError = @"RegistrationBirthDateError";
NSString *const Texts_RegistrationPasswordError = @"RegistrationPasswordError";
NSString *const Texts_RegistrationConfirmPasswordError = @"RegistrationConfirmPasswordError";
NSString *const Texts_RegistrationGenderdError = @"RegistrationGenderdError";
NSString *const Texts_RegistrationRulesError = @"RegistrationRulesError";
NSString *const Texts_LoginTitleLabel = @"LoginTitleLabel";
NSString *const Texts_LoginPhonePlaceholder = @"LoginPhonePlaceholder";
NSString *const Texts_LoginPhoneError = @"LoginPhoneError";
NSString *const Texts_LoginLogoutButtonText = @"LoginLogoutButtonText";
NSString *const Texts_LoginPhoneLabel = @"LoginPhoneLabel";
NSString *const Texts_Summary = @"Summary";
NSString *const Texts_Information = @"Information";
NSString *const Texts_PreRegisterInformationText = @"PreRegisterInformationText";
NSString *const Texts_PreRegisterRegisterButton = @"PreRegisterRegisterButton";
NSString *const Texts_CardAddTitleLabel = @"CardAddTitleLabel";
NSString *const Texts_CardAddCardNumberPlaceholder = @"CardAddCardNumberPlaceholder";
NSString *const Texts_CardAddCardNumberError = @"CardAddCardNumberError";
NSString *const Texts_CardAddExpirationDatePlaceholder = @"CardAddExpirationDatePlaceholder";
NSString *const Texts_CardAddExpirationDateError = @"CardAddExpirationDateError";
NSString *const Texts_CardAddNicknamePlaceholder = @"CardAddNicknamePlaceholder";
NSString *const Texts_CardAddNicknameError = @"CardAddNicknameError";
NSString *const Texts_CardAddCvcCvvPlaceholder = @"CardAddCvcCvvPlaceholder";
NSString *const Texts_CardAddCvcCvvError = @"CardAddCvcCvvError";
NSString *const Texts_CardEditTitleLabel = @"CardEditTitleLabel";
NSString *const Texts_Next = @"Next";
NSString *const Texts_CardsListTitleLabel = @"CardsListTitleLabel";
NSString *const Texts_AddCardButton = @"AddCardButton";
NSString *const Texts_CardVerifyInfo = @"CardVerifyInfo";
NSString *const Texts_CardVerifyTitleLabel = @"CardVerifyTitleLabel";
NSString *const Texts_CardVerifyMpinPlaceholder = @"CardVerifyMpinPlaceholder";
NSString *const Texts_CardVerifyMpinError = @"CardVerifyMpinError";
NSString *const Texts_CardVerifyConfirmMpinPlaceholder = @"CardVerifyConfirmMpinPlaceholder";
NSString *const Texts_CardVerifyConfirmMpinError = @"CardVerifyConfirmMpinError";
NSString *const Texts_CardVerifySuccessAlert = @"CardVerifySuccessAlert";
NSString *const Texts_PayChoosenCard = @"PayChoosenCard";
NSString *const Texts_PayAcceptTransaction = @"PayAcceptTransaction";
NSString *const Texts_PayMpinCvcPlaceholder = @"PayMpinCvcPlaceholder";
NSString *const Texts_PayNoCardsAlert = @"PayNoCardsAlert";
NSString *const Texts_PaySelectCardHint = @"PaySelectCardHint";
NSString *const Texts_PayMpinCvcError = @"PayMpinCvcError";
NSString *const Texts_PayAcceptRulesError = @"PayAcceptRulesError";
NSString *const Texts_PayCancelPaymentButton = @"PayCancelPaymentButton";
NSString *const Texts_PayRetryPaymentButton = @"PayRetryPaymentButton";
NSString *const Texts_PayRepeatedTransactionAlert = @"PayRepeatedTransactionAlert";
NSString *const Texts_Cancel = @"Cancel";
NSString *const Texts_LoggedAs = @"LoggedAs";
NSString *const Texts_Pay = @"Pay";
NSString *const Texts_Ok = @"Ok";
NSString *const Texts_LoginEmptyCodeError = @"LoginEmptyCodeError";
NSString *const Texts_LoginWrongCodeError = @"LoginWrongCodeError";
NSString *const Texts_LoginAskForNewCode = @"LoginAskForNewCode";
NSString *const Texts_LoginEnterCode = @"LoginEnterCode";
NSString *const Texts_LoginEnterCodePlaceholder = @"LoginEnterCodePlaceholder";
NSString *const Texts_LoginExitAlert = @"LoginExitAlert";
NSString *const Texts_LoginLogoutAlert = @"LoginLogoutAlert";
NSString *const Texts_Yes = @"Yes";
NSString *const Texts_No = @"No";
NSString *const Texts_Choose = @"Choose";
NSString *const Texts_Register = @"Register";
NSString *const Texts_TechnicalError = @"TechnicalError";
NSString *const Texts_CardAddGoToVerification = @"CardAddGoToVerification";
NSString *const Texts_CardsListCellVerified = @"CardsListCellVerified";
NSString *const Texts_CardsListCellUnverified = @"CardsListCellUnverified";
NSString *const Texts_CardsListCellChoosen = @"CardsListCellChoosen";
NSString *const Texts_CardsListCellBlocked = @"CardsListCellBlocked";
NSString *const Texts_CardsListCellDefault = @"CardsListCellDefault";
NSString *const Texts_CardsListCellBlockedError = @"CardsListCellBlockedError";
NSString *const Texts_CardsListCellVerifiedError = @"CardsListCellVerifiedError";
NSString *const Texts_CardsListCellAnotherCard = @"CardsListCellAnotherCard";
NSString *const Texts_RegistrationEmailHint = @"RegistrationEmailHint";
NSString *const Texts_RegistrationBirthDateHint = @"RegistrationBirthDateHint";
NSString *const Texts_RegistrationPasswordHint = @"RegistrationPasswordHint";
NSString *const Texts_CardVerifyMpinHint = @"CardVerifyMpinHint";
NSString *const Texts_CardAddCardNumberHint = @"CardAddCardNumberHint";
NSString *const Texts_CardAddExpirationDateHint = @"CardAddExpirationDateHint";
NSString *const Texts_CardAddNicknameHint = @"CardAddNicknameHint";
NSString *const Texts_CardCvcCvvHint = @"CardCvcCvvHint";
NSString *const Texts_LoginEnterCodeAgain = @"LoginEnterCodeAgain";
NSString *const Texts_PayNoVerifiedCardsAlert = @"PayNoVerifiedCardsAlert";
NSString *const Texts_FooterWalletInfo = @"FooterWalletInfo";
NSString *const Texts_FooterSecurityInfo = @"FooterSecurityInfo";
NSString *const Texts_PayCVCPlaceholder = @"PayCVCPlaceholder";
NSString *const Texts_PayMpinPlaceholder = @"PayMpinPlaceholder";
NSString *const Texts_PayMpinError = @"PayMpinError";
NSString *const Texts_PayCVCError = @"PayCVCError";

@end
NSString *const Texts_LoginInfoLabel = @"LoginInfoLabel";
NSString *const Texts_PayNoCardsLabel = @"PayNoCardsLabel";
NSString *const Texts_CardHintMpin = @"CardHintMpin";
