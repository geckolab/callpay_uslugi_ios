//
//  UpaidMPMTexts.h
//  MpmTestApp
//
//  Created by Michał Majewski on 28.11.2014.
//  Copyright (c) 2014 uPaid. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpaidMPMTexts : NSObject

extern NSString *const Texts_RegistrationFirstNamePlaceholder;
extern NSString *const Texts_RegistrationLastNamePlaceholder;
extern NSString *const Texts_RegistrationPhonePlaceholder;
extern NSString *const Texts_RegistrationEmailPlaceholder;
extern NSString *const Texts_RegistrationBirthDatePlaceholder;
extern NSString *const Texts_RegistrationPasswordPlaceholder;
extern NSString *const Texts_RegistrationConfirmPasswordPlaceholder;
extern NSString *const Texts_RegistrationGenderMale;
extern NSString *const Texts_RegistrationGenderFemale;
extern NSString *const Texts_RegistrationRulesText;
extern NSString *const Texts_RegistrationMarketingText;
extern NSString *const Texts_RegistrationTitleLabel;
extern NSString *const Texts_RegistrationFirstNameError;
extern NSString *const Texts_RegistrationLastNameError;
extern NSString *const Texts_RegistrationPhoneError;
extern NSString *const Texts_RegistrationEmailError;
extern NSString *const Texts_RegistrationBirthDateError;
extern NSString *const Texts_RegistrationPasswordError;
extern NSString *const Texts_RegistrationConfirmPasswordError;
extern NSString *const Texts_RegistrationGenderdError;
extern NSString *const Texts_RegistrationRulesError;
extern NSString *const Texts_LoginTitleLabel;
extern NSString *const Texts_LoginPhonePlaceholder;
extern NSString *const Texts_LoginPhoneError;
extern NSString *const Texts_LoginLogoutButtonText;
extern NSString *const Texts_LoginPhoneLabel;
extern NSString *const Texts_Summary;
extern NSString *const Texts_Information;
extern NSString *const Texts_PreRegisterInformationText;
extern NSString *const Texts_PreRegisterRegisterButton;
extern NSString *const Texts_CardAddTitleLabel;
extern NSString *const Texts_CardAddCardNumberPlaceholder;
extern NSString *const Texts_CardAddCardNumberError;
extern NSString *const Texts_CardAddExpirationDatePlaceholder;
extern NSString *const Texts_CardAddExpirationDateError;
extern NSString *const Texts_CardAddNicknamePlaceholder;
extern NSString *const Texts_CardAddNicknameError;
extern NSString *const Texts_CardAddCvcCvvPlaceholder;
extern NSString *const Texts_CardAddCvcCvvError;
extern NSString *const Texts_CardEditTitleLabel;
extern NSString *const Texts_Next;
extern NSString *const Texts_CardsListTitleLabel;
extern NSString *const Texts_AddCardButton;
extern NSString *const Texts_CardVerifyInfo;
extern NSString *const Texts_CardVerifyTitleLabel;
extern NSString *const Texts_CardVerifyMpinPlaceholder;
extern NSString *const Texts_CardVerifyMpinError;
extern NSString *const Texts_CardVerifyConfirmMpinPlaceholder;
extern NSString *const Texts_CardVerifyConfirmMpinError;
extern NSString *const Texts_CardVerifySuccessAlert;
extern NSString *const Texts_PayChoosenCard;
extern NSString *const Texts_PayAcceptTransaction;
extern NSString *const Texts_PayMpinCvcPlaceholder;
extern NSString *const Texts_PayNoCardsAlert;
extern NSString *const Texts_PaySelectCardHint;
extern NSString *const Texts_PayMpinCvcError;
extern NSString *const Texts_PayAcceptRulesError;
extern NSString *const Texts_PayCancelPaymentButton;
extern NSString *const Texts_PayRetryPaymentButton;
extern NSString *const Texts_PayRepeatedTransactionAlert;
extern NSString *const Texts_Cancel;
extern NSString *const Texts_LoggedAs;
extern NSString *const Texts_Pay;
extern NSString *const Texts_Ok;
extern NSString *const Texts_LoginEmptyCodeError;
extern NSString *const Texts_LoginWrongCodeError;
extern NSString *const Texts_LoginAskForNewCode;
extern NSString *const Texts_LoginEnterCode;
extern NSString *const Texts_LoginEnterCodePlaceholder;
extern NSString *const Texts_LoginExitAlert;
extern NSString *const Texts_LoginLogoutAlert;
extern NSString *const Texts_Yes;
extern NSString *const Texts_No;
extern NSString *const Texts_Choose;
extern NSString *const Texts_Register;
extern NSString *const Texts_TechnicalError;
extern NSString *const Texts_CardAddGoToVerification;
extern NSString *const Texts_CardsListCellVerified;
extern NSString *const Texts_CardsListCellUnverified;
extern NSString *const Texts_CardsListCellChoosen;
extern NSString *const Texts_CardsListCellBlocked;
extern NSString *const Texts_CardsListCellDefault;
extern NSString *const Texts_CardsListCellBlockedError;
extern NSString *const Texts_CardsListCellVerifiedError;
extern NSString *const Texts_CardsListCellAnotherCard;
extern NSString *const Texts_RegistrationEmailHint;
extern NSString *const Texts_RegistrationBirthDateHint;
extern NSString *const Texts_RegistrationPasswordHint;
extern NSString *const Texts_CardVerifyMpinHint;
extern NSString *const Texts_CardAddCardNumberHint;
extern NSString *const Texts_CardAddExpirationDateHint;
extern NSString *const Texts_CardAddNicknameHint;
extern NSString *const Texts_CardCvcCvvHint;
extern NSString *const Texts_LoginEnterCodeAgain;
extern NSString *const Texts_PayNoVerifiedCardsAlert;
extern NSString *const Texts_FooterWalletInfo;
extern NSString *const Texts_FooterSecurityInfo;
extern NSString *const Texts_PayCVCPlaceholder;
extern NSString *const Texts_PayMpinPlaceholder;
extern NSString *const Texts_PayMpinError;
extern NSString *const Texts_PayCVCError;

@end

extern NSString *const Texts_LoginInfoLabel;
extern NSString *const Texts_PayNoCardsLabel;
extern NSString *const Texts_CardHintMpin;
