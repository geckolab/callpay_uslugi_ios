//
//  UpaidMPMProduct.h
//  mPotwor
//
//  Created by Michał Majewski on 04.05.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpaidMPMProduct : NSObject

@property (nonatomic) UIImage *image;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *shortDescription;
@property (nonatomic) NSString *imageUrl;
@property int quantity;
@property int amount;

- (instancetype)initWithName:(NSString *)name andShortDescription:(NSString *)shortDescription andImage:(UIImage *)image andQuantity:(int)quantity andAmount:(int)amount;
- (instancetype)initWithName:(NSString *)name andShortDescription:(NSString *)shortDescription andImageUrl:(NSString *)imageUrl andQuantity:(int)quantity andAmount:(int)amount;
- (NSString *) summaryDescription;
- (NSString *) summaryDescriptionForPayment;

@end
