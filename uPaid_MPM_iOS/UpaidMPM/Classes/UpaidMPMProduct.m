//
//  UpaidMPMProduct.m
//  mPotwor
//
//  Created by Michał Majewski on 04.05.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidMPMProduct.h"
#import "UpaidMPM.h"

@implementation UpaidMPMProduct

- (instancetype)initWithName:(NSString *)name andShortDescription:(NSString *)shortDescription andImage:(UIImage *)image andQuantity:(int)quantity andAmount:(int)amount {
	self = [self init];

	if (self) {
		self.image = image;
		self.shortDescription = shortDescription;
		self.name = name;
		self.amount = amount;
		self.quantity = quantity;
	}

	return self;
}

- (instancetype)initWithName:(NSString *)name andShortDescription:(NSString *)shortDescription andImageUrl:(NSString *)imageUrl andQuantity:(int)quantity andAmount:(int)amount {
    self = [self init];
    
	if (self) {
		self.imageUrl = imageUrl;
		self.shortDescription = shortDescription;
		self.name = name;
		self.amount = amount;
		self.quantity = quantity;
	}

	return self;
}

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.imageUrl = @"";
        self.shortDescription = @"";
        self.name = @"";
        self.amount = 0;
        self.quantity = 1;
    }
    
    return self;
}

- (void)setImageUrl:(NSString *)imageUrl {
    if (imageUrl == nil) {
        imageUrl = @"";
    }
    
    _imageUrl = imageUrl;
}

- (NSString *) summaryDescription {
    return [NSString localizedStringWithFormat: UpaidMPMLocalizedString(@"Nazwa: %@\nCena: %.2f zł"), self.shortDescription , self.amount * self.quantity / 100.0f];
}

- (NSString *) summaryDescriptionForPayment {
    return [self summaryDescription];
}

@end
