//
//  UpaidMPMConfig.m
//  EuroFlorist
//
//  Created by Michał Majewski on 27.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidMPM.h"
#import "UpaidApi.h"
#import "UpaidApi+Dev.h"
#import "UpaidMPMLoginViewController.h"
#import "UpaidMPMPayViewController.h"
#import "UpaidApiCard+Extensions.h"

@interface UpaidMPM ()

@property (nonatomic) NSString *merchant;
@property (nonatomic) NSString *itemNo;
@property (nonatomic) NSString *additionalData;

@end

@implementation UpaidMPM

#pragma mark singleton init

+ (UpaidMPM *)sharedInstance {
  static UpaidMPM *sharedInstance = nil;
  static dispatch_once_t onceToken;
  
  dispatch_once(&onceToken, ^{
    sharedInstance = [[UpaidMPM alloc] init];
  });
  
  return sharedInstance;
}

- (id)init {
  self = [super init];
  
  if (self) {
    self.logsEnabled = NO;
    self.acquirer = UpaidMPMAcquirerECard;
    self.products = @[];
    self.showExitWarning = YES;
    self.uiConfig = [[UpaidMPMUIConfig alloc] init];
    [UpaidApi setPlatform:@"sdk"];
  }
  
  return self;
}

static UpaidMPM *paymentInstance = nil;

+ (void) setPaymentInstance: (UpaidMPM *) instance {
    if (instance != nil) {
        if (instance.delegate == nil) {
            UpaidMPMLog(@"Create UpaidMPMDelegate.");
            abort();
        }
        if (instance.products == nil || [instance.products count] == 0) {
            UpaidMPMLog(@"Create products.");
            abort();
        }
    }
    
    paymentInstance = instance;
}

+ (UpaidMPM *) paymentInstance {
  return paymentInstance;
}

static BOOL loggedInVar = NO;

+ (BOOL) loggedIn {
    return loggedInVar;
}

+ (void) setLoggedIn: (BOOL) loggedIn {
    loggedInVar = loggedIn;
}

- (void) setPartnerId: (int) partnerId andAppName: (NSString *) appName andMerchant: (NSString *) merchant {
  [UpaidApi setPartnerId: partnerId andAppName:appName];
  self.appName = appName;
  self.merchant = merchant;
}


- (void) setTestPartnerId: (int) testPartnerId {
  [UpaidApi setTestPartnerId:testPartnerId];
  self.logsEnabled = YES;
}

- (void) setAmount: (NSInteger) amount andItemNo: (NSString *) itemNo andAdditionalData: (NSString *) additionalData {
  self.amount = amount;
  self.itemNo = itemNo;
  self.additionalData = additionalData;
}

- (void) startPaymentForNavigationController: (UINavigationController *) navigationController {
    NSMutableArray *newViewControllers = [[NSMutableArray alloc] initWithArray: navigationController.viewControllers];
    [newViewControllers addObjectsFromArray: [self viewControllersForPayment]];
    [navigationController setViewControllers: newViewControllers animated:YES];
}

- (NSArray *) viewControllersForPayment {
  
  return [self viewControllersForPaymentWithConfig:[UpaidMPM sharedInstance]];
}

- (NSArray *) viewControllersForPaymentWithConfig: (UpaidMPM *) paymentConfigInstance {
  [UpaidMPM setPaymentInstance:paymentConfigInstance];
    NSMutableArray *viewControllers = [[NSMutableArray alloc] initWithObjects: [[UpaidMPMLoginViewController alloc] init], nil];
    
  //user try to buy again
  if ([UpaidMPM loggedIn]) {
      [viewControllers addObject: [[UpaidMPMPayViewController alloc] init]];
  }
    
    return (NSArray *) viewControllers;
}

+ (id<UpaidMPMDelegate>)delegate {
  id <UpaidMPMDelegate> delegate = [UpaidMPM paymentInstance].delegate;
  [UpaidMPMTools sharedInstance].activeViewController = nil;
  [UpaidMPM setPaymentInstance: nil];
  
  return delegate;
}

@end
