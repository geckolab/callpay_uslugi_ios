//
//  UpaidMPMCardHint.m
//  EuroFlorist
//
//  Created by Michał Majewski on 06.03.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidMPMCardHint.h"
#import "UpaidMPM.h"

@interface UpaidMPMCardHint ()

@property (weak, nonatomic) IBOutlet UIView *mpinContainer;
@property (weak, nonatomic) IBOutlet UILabel *mpinLabel;

@end

@implementation UpaidMPMCardHint

- (id)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];

  if (self) {
    UpaidMPMCardHint *hint = [[[NSBundle mainBundle] loadNibNamed: @"UpaidMPMCardHint" owner:self options:nil] objectAtIndex:0];
    hint.frame = frame;
    
    if (hint) {
      return hint;
    }
  }

  return self;
}

+ (void) showWithMpin: (BOOL) withMpin{
  UIWindow *window = [UIApplication sharedApplication].windows.firstObject;
  UpaidMPMCardHint *hint = [[UpaidMPMCardHint alloc] initWithFrame:CGRectMake(0, 0, window.frame.size.width, window.frame.size.height)];
  hint.mpinContainer.hidden = !withMpin;
  hint.mpinLabel.text = UpaidMPMLocalizedString(Texts_CardHintMpin);
  [window addSubview: hint];
}

- (IBAction)userTapAnywhere:(UITapGestureRecognizer *)sender {
  [self removeFromSuperview];
}

@end
