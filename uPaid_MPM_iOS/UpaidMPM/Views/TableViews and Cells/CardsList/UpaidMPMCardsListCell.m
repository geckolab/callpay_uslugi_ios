//
//  ProductDetailCell.m
//  EuroFlorist
//
//  Created by Michał Majewski on 04.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidMPMCardsListCell.h"
#import "UpaidMPM.h"

@interface UpaidMPMCardsListCell ()

@property (weak, nonatomic) IBOutlet UILabel *nicknameLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIButton *verifyButton;

@end

@implementation UpaidMPMCardsListCell

- (void) setCard:(UpaidApiCard *)card {
  _card = card;
  self.nicknameLabel.text = card.nickname;
  
  NSMutableArray *statuses = [[NSMutableArray alloc] init];
  
  if (card.authorized) {
    [statuses addObject: UpaidMPMLocalizedString(Texts_CardsListCellVerified)];
  } else {
    [statuses addObject: UpaidMPMLocalizedString(Texts_CardsListCellUnverified)];
  }
  
  self.verifyButton.hidden = card.authorized;
  [self.verifyButton setImage:[UIImage imageNamed: @"mpm_card_verified"] forState:UIControlStateSelected | UIControlStateHighlighted];
  
  if ([[UpaidMPM paymentInstance].card.iid isEqualToString: card.iid]) {
    [statuses addObject: UpaidMPMLocalizedString(Texts_CardsListCellChoosen)];
  }
  
  if (card.locked) {
    self.verifyButton.hidden = YES;
    [statuses addObject: UpaidMPMLocalizedString(Texts_CardsListCellBlocked)];
  }
    
  if (card.ddefault && card.authorized) {
    [statuses addObject: UpaidMPMLocalizedString(Texts_CardsListCellDefault)];
  }
  
  self.statusLabel.text = [statuses componentsJoinedByString: @", "];
  
  if (card.locked) {
    self.nicknameLabel.attributedText = [[NSAttributedString alloc] initWithString: self.nicknameLabel.text attributes: @{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    self.statusLabel.attributedText = [[NSAttributedString alloc] initWithString: self.statusLabel.text attributes: @{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
  }
}

#pragma mark buttons

- (IBAction)verifyButtonClicked {
  if (!self.card.authorized) {
      [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_VERIFY_CARD object:self userInfo: @{@"card" : self.card}];
  }
}

@end
