//
//  SelectMasterPassTableViewCell.m
//  MpmTestApp
//
//  Created by Paweł Grzmil on 06.08.2014.
//  Copyright (c) 2014 uPaid. All rights reserved.
//

#import "UpaidMPMSelectMasterPassTableViewCell.h"
#import "UpaidMPMTools.h"
#import "UpaidMPM.h"
#import "UpaidMPMButton.h"

@interface UpaidMPMSelectMasterPassTableViewCell()

@property (weak, nonatomic) IBOutlet UpaidMPMButton *selectAnotherCardButton;

@end

@implementation UpaidMPMSelectMasterPassTableViewCell

- (void) awakeFromNib {
    NSString *title = UpaidMPMLocalizedString(Texts_CardsListCellAnotherCard);
    [self.selectAnotherCardButton setTitle: title forState:UIControlStateNormal];
    [self.selectAnotherCardButton setTitle: title forState:UIControlStateHighlighted];
}

- (IBAction) selectMasterPassClicked {
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_MASTERPASS_SELECTOR object:nil];
  
}
@end
