//
//  ProductDetailsTableView.m
//  EuroFlorist
//
//  Created by Michał Majewski on 04.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidMPMCardsListTableView.h"
#import "UpaidMPM.h"
#import "UpaidMPMCardsListViewController.h"
#import "UpaidMPMSelectMasterPassTableViewCell.h"

@interface UpaidMPMCardsListTableView () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation UpaidMPMCardsListTableView

static NSString *STANDARD_CELL = @"STANDARD_CELL";
static NSString *MASTERPASS_LINK_CELL = @"MASTERPASS_LINK_CELL";

- (id)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];

	if (self) {
		self.cards = [[UpaidApiCardArray alloc] init];
		self.delegate = self;
		self.dataSource = self;
		[self registerNib:[UINib nibWithNibName:[UpaidMPM paymentInstance].uiConfig.cardsListCellNibName bundle:nil] forCellReuseIdentifier:STANDARD_CELL];
		[self registerNib:[UINib nibWithNibName:[UpaidMPM paymentInstance].uiConfig.masterpassSelectorCellNibName bundle:nil] forCellReuseIdentifier:MASTERPASS_LINK_CELL];
	}

	return self;
}

- (void)refreshList {
	self.cards = [UpaidMPMTools sharedInstance].cards;

	[self reloadData];
}

- (void)showInfoAlertWithMessage:(NSString *)message {
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:UpaidMPMLocalizedString(message) delegate:nil cancelButtonTitle:nil otherButtonTitles: UpaidMPMLocalizedString(Texts_Ok), nil];
	[alert show];
}

#pragma mark tableView delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < [self.cards count]) {        
        UpaidApiCard *card = [self.cards objectAtIndex:indexPath.row];
        
        if (card.locked) {
            [self showInfoAlertWithMessage: UpaidMPMLocalizedString(Texts_CardsListCellBlockedError)];
        } else if (!card.authorized) {
          [self showInfoAlertWithMessage: UpaidMPMLocalizedString(Texts_CardsListCellVerifiedError)];
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SELECT_CARD object:self userInfo:@{@"card" : card}];
        }
    }
}

#pragma mark tableViewDataSource delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [self.cards count] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell;
	if (indexPath.row == self.cards.count) {
		cell = [tableView dequeueReusableCellWithIdentifier:MASTERPASS_LINK_CELL];
		if (cell == nil) {
			cell = [[UpaidMPMSelectMasterPassTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MASTERPASS_LINK_CELL];
		}
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
	}
	else {
		cell = [tableView dequeueReusableCellWithIdentifier:STANDARD_CELL];
		if (cell == nil) {
			cell = [[UpaidMPMCardsListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:STANDARD_CELL];
		}

		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		((UpaidMPMCardsListCell *)cell).card = [self.cards objectAtIndex:indexPath.row];
	}
	return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
	return 0.01f;
}

@end
