//
//  ProductDetailCell.m
//  EuroFlorist
//
//  Created by Michał Majewski on 04.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidMPMProductsListCell.h"
#import "UpaidMPM.h"
#import "UpaidMPMBasicColorLabel.h"
#import "UpaidMPMAccentColorLabel.h"

@interface UpaidMPMProductsListCell ()

@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end

@implementation UpaidMPMProductsListCell

-(void)setProduct:(UpaidMPMProduct *)product {
    _product = product;
    
    if (self.productImageView != nil) {
        self.productImageView.image = product.image;
    }
    
    if (self.nameLabel != nil) {
        self.nameLabel.text = product.name;
    }
    
    if (self.descriptionLabel != nil) {
        self.descriptionLabel.text = product.shortDescription;
    }
}

@end
