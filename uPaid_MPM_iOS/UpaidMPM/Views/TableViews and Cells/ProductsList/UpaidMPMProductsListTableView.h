//
//  ProductDetailsTableView.h
//  EuroFlorist
//
//  Created by Michał Majewski on 04.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UpaidApiCardArray.h"
#import "UpaidMPMProductsListCell.h"

@interface UpaidMPMProductsListTableView : UITableView

@end
