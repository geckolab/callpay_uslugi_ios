//
//  ProductDetailsTableView.m
//  EuroFlorist
//
//  Created by Michał Majewski on 04.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidMPMProductsListTableView.h"
#import "UpaidMPM.h"
#import "UpaidMPMCardsListViewController.h"
#import "UpaidMPMProduct.h"

@interface UpaidMPMProductsListTableView () <UITableViewDataSource, UITableViewDelegate >

@end

@implementation UpaidMPMProductsListTableView


static NSString *STANDARD_CELL = @"STANDARD_CELL";

-(id)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
  
  if (self) {
    self.delegate = self;
    self.dataSource = self;
    [self registerNib:[UINib nibWithNibName: [UpaidMPM paymentInstance].uiConfig.productsListCellNibName bundle: nil] forCellReuseIdentifier: STANDARD_CELL];
  }
  
  return self;
}

#pragma mark tableViewDataSource delegate

- (UpaidMPMProduct *) productForIndexPath: (NSIndexPath *) indexPath {
    return [UpaidMPM paymentInstance].products[indexPath.row];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return [[UpaidMPM paymentInstance].products count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  UpaidMPMProductsListCell *cell = [tableView dequeueReusableCellWithIdentifier: STANDARD_CELL];
  
  if (cell == nil) {
    cell = [[UpaidMPMProductsListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier: STANDARD_CELL];
  }
  
  cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.product = [self productForIndexPath: indexPath];
  return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
  return 0.01f;
}

@end
