//
//  MPMTextField.m
//  EuroFlorist
//
//  Created by Michał Majewski on 27.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidMPMTextField.h"
#import "UpaidMPM.h"

@interface UpaidMPMTextField ()

@property (nonatomic) NSString *hintMessage;
@property (nonatomic) UIButton *hintButton;
@property (nonatomic) id hintTarget;
@property (nonatomic) SEL hintSelector;

@end

@implementation UpaidMPMTextField

- (id)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
  
  if (self) {
    self.placeholder = UpaidMPMLocalizedString(self.placeholder);
    
    self.textColor = [UpaidMPM paymentInstance].uiConfig.textFieldColor;
    [self setValue:[UpaidMPM paymentInstance].uiConfig.textFieldPlaceholderColor forKeyPath:@"_placeholderLabel.textColor"];
    self.backgroundColor = [UpaidMPM paymentInstance].uiConfig.textFieldBackgroundColor;
    self.background = [UpaidMPM paymentInstance].uiConfig.textFieldBackgroundImage;
    self.borderStyle = UITextBorderStyleNone;
    self.edgeInsets = [UpaidMPM paymentInstance].uiConfig.textFieldEdgeInsets;
    
    self.hintTarget = self;
    self.hintSelector = @selector(showHintAlert);
  }
  
  return self;
}

#pragma mark paddings

- (CGRect)textRectForBounds:(CGRect)bounds {
  return [super textRectForBounds:UIEdgeInsetsInsetRect(bounds, self.edgeInsets)];
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
  return [super editingRectForBounds:UIEdgeInsetsInsetRect(bounds, self.edgeInsets)];
}

#pragma mark hint button

- (void)setEnabled:(BOOL)enabled {
    [super setEnabled: enabled];
    
    if (self.hintButton != nil) {
        self.hintButton.hidden = !enabled;
    }
}

- (void) setHintMessage: (NSString *) message {
  _hintMessage = message;
  
  [self setupHintButton];
}

- (void) setHintTarget: (id) target andSelector: (SEL) selector {
  self.hintTarget = target;
  self.hintSelector = selector;
  
  [self setupHintButton];
}

- (void) setupHintButton {
  self.hintButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
  [self.hintButton setImage:[UIImage imageNamed:@"mpm_hint"] forState:UIControlStateNormal];
  [self.hintButton setImage:[UIImage imageNamed:@"mpm_hint_pressed"] forState:UIControlStateHighlighted];
  [self.hintButton addTarget:self.hintTarget action:self.hintSelector forControlEvents:UIControlEventTouchUpInside];
  UIView *container = [[UIView alloc] initWithFrame: CGRectMake(0, 0, self.hintButton.frame.size.width + self.edgeInsets.right, self.hintButton.frame.size.height)];
  [container addSubview: self.hintButton];
  self.rightView = container;
  self.rightViewMode = UITextFieldViewModeAlways;
}

- (void) showHintAlert {
  UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"" message: UpaidMPMLocalizedString(self.hintMessage) delegate:nil cancelButtonTitle:nil otherButtonTitles: UpaidMPMLocalizedString(Texts_Ok), nil];
  [alert show];
}

@end
