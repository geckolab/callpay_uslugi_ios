//
//  UpaidMPMCardHint.h
//  EuroFlorist
//
//  Created by Michał Majewski on 06.03.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpaidMPMCardHint : UIView

+ (void) showWithMpin: (BOOL) withMpin;

@end
