//
//  UpaidMPMCardVisualImageView.h
//  MpmTestApp
//
//  Created by Michał Majewski on 03.02.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpaidMPMCardVisualImageView : UIImageView

@end
