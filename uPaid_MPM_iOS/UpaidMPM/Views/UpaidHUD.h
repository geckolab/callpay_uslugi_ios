//
//  UpaidHUD.h
//  EuroFlorist
//
//  Created by Michał Majewski on 07.04.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpaidHUD : UIView

+ (void) show;
+ (void) dismiss;

@end
