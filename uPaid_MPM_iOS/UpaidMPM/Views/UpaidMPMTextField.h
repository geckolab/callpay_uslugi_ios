//
//  MPMTextField.h
//  EuroFlorist
//
//  Created by Michał Majewski on 27.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpaidMPMTextField : UITextField

@property (nonatomic) UIEdgeInsets edgeInsets;

- (void) setHintMessage: (NSString *) message;
- (void) setHintTarget: (id) target andSelector: (SEL) selector;

@end
