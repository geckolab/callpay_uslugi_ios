//
//  UpaidHUD.m
//  EuroFlorist
//
//  Created by Michał Majewski on 07.04.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidHUD.h"
#import "UpaidMPM.h"

@interface UpaidHUD ()

@property (nonatomic) UIActivityIndicatorView *activityIndicatorView;

@end

@implementation UpaidHUD

+ (UpaidHUD *)sharedInstance {
  static UpaidHUD *sharedInstance = nil;
  static dispatch_once_t onceToken;
  
  dispatch_once(&onceToken, ^{
    sharedInstance = [[UpaidHUD alloc] init];
  });
  
  return sharedInstance;
}

- (id)init {
  self = [super init];
  
  if (self) {
    self.activityIndicatorView = [UpaidMPM paymentInstance].uiConfig.indicatorView;
    [self.activityIndicatorView startAnimating];
    [self addSubview: self.activityIndicatorView];
  }
  
  return self;
}

- (void) showOnWindow: (UIWindow *) window {
  self.frame = window.frame;
  [self.activityIndicatorView setCenter: self.center];
  [window addSubview: self];
}

+ (void) show{
  [[UpaidHUD sharedInstance] showOnWindow: [UIApplication sharedApplication].windows.firstObject];
  [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

+ (void) dismiss {
  [[UpaidHUD sharedInstance] removeFromSuperview];
  [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

@end
