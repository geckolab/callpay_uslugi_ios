//
//  MPMLabel.m
//  EuroFlorist
//
//  Created by Michał Majewski on 27.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidMPMBaseLabel.h"
#import "UpaidMPMConst.h"

@implementation UpaidMPMBaseLabel

- (id)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
  
  if (self) {
    self.text = UpaidMPMLocalizedString(self.text);
  }
  
  return self;
}

@end
