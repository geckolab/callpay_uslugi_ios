//
//  MPMBasicColorLabel.m
//  EuroFlorist
//
//  Created by Michał Majewski on 27.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidMPMBasicColorLabel.h"
#import "UpaidMPM.h"

@implementation UpaidMPMBasicColorLabel

- (id)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
  
  if (self) {
    self.textColor = [UpaidMPM paymentInstance].uiConfig.basicColor;
  }
  
  return self;
}

@end
