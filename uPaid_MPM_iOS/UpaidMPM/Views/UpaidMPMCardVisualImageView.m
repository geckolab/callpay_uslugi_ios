//
//  UpaidMPMCardVisualImageView.m
//  MpmTestApp
//
//  Created by Michał Majewski on 03.02.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "UpaidMPMCardVisualImageView.h"
#import "LBorderView.h"

@interface UpaidMPMCardVisualImageView()

@property (nonatomic) LBorderView *borderView;

@end

@implementation UpaidMPMCardVisualImageView

- (void)setImage:(UIImage *)image {
    if (image != nil) {
        [self.borderView removeFromSuperview];
    } else {
        [self addSubview: self.borderView];
    }
    
    [super setImage: image];
}

- (LBorderView *)borderView {
    if (_borderView == nil) {
        _borderView= [[LBorderView alloc] initWithFrame: self.bounds];
        _borderView.cornerRadius = 4;
        _borderView.borderType = BorderTypeDashed;
        _borderView.borderWidth = 1;
        _borderView.borderColor = [UIColor lightGrayColor];
        _borderView.dashPattern = 4;
        _borderView.spacePattern = 4;
    }
    
    return _borderView;
}

@end
