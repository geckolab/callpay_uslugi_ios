//
//  UpaidMPMButton.m
//  EuroFlorist
//
//  Created by Michał Majewski on 03.03.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidMPMButton.h"
#import "UpaidMPM.h"
#import <QuartzCore/QuartzCore.h>

@implementation UpaidMPMButton

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        UIColor *titleColor = [UpaidMPM paymentInstance].uiConfig.buttonTitleColor;
        
        if (titleColor == nil) {
            titleColor = [UIColor whiteColor];
        }
        
        UIColor *backgroundColor = [UpaidMPM paymentInstance].uiConfig.buttonBackgroundColor;
        
        if (backgroundColor == nil) {
            backgroundColor = [UpaidMPM paymentInstance].uiConfig.accentColor;
        }
        
        [self setBackgroundColor: backgroundColor];
        [self setTitleColor: titleColor forState: UIControlStateNormal];
        [self setTitleColor: titleColor forState: UIControlStateHighlighted];
        [self setTitle: UpaidMPMLocalizedString([self titleForState:UIControlStateNormal]) forState:UIControlStateNormal];
        
        if ([UpaidMPM paymentInstance].uiConfig.buttonBorderColor != nil) {
            [[self layer] setBorderWidth: 1.0f];
            [[self layer] setBorderColor: [UpaidMPM paymentInstance].uiConfig.buttonBorderColor.CGColor];
        }
    }
    
    return self;
}

@end
