//
//  UpaidMPMCheckboxButton.m
//  EuroFlorist
//
//  Created by Michał Majewski on 28.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidMPMCheckboxButton.h"
#import "UpaidMPM.h"

@implementation UpaidMPMCheckboxButton

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self ) {
        [self addTarget:self action:@selector(onPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return self;
}

- (void) awakeFromNib {
    UIColor *titleColor = [UpaidMPM paymentInstance].uiConfig.buttonTitleColor;
    
    if (titleColor == nil) {
        titleColor = [UIColor blackColor];
    }
    
    [self setTitleColor: titleColor forState: UIControlStateNormal];
    [self setTitleColor: titleColor forState: UIControlStateHighlighted];
    
    [self setImage:[UIImage imageNamed:@"mpm_checkbox"] forState: UIControlStateNormal];
    [self setImage:[UIImage imageNamed:@"mpm_checkbox_pressed"] forState: UIControlStateHighlighted];
    [self setImage:[UIImage imageNamed:@"mpm_checkbox_checked"] forState: UIControlStateSelected];
    [self setImage:[UIImage imageNamed:@"mpm_checkbox_checked_pressed"] forState: UIControlStateSelected | UIControlStateHighlighted];
}

- (IBAction)onPressed:(id)sender {
    self.selected = !self.selected;
}

@end
