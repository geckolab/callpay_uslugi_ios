//
//  UpaidMPMFooter.m
//  EuroFlorist
//
//  Created by Michał Majewski on 14.03.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidMPMFooter.h"
#import "UpaidApi.h"
#import "UpaidMPM.h"
#import "UpaidMPMBasicColorLabel.h"

@interface UpaidMPMFooter ()

@property (weak, nonatomic) IBOutlet UILabel *betaLabel;
@property (weak, nonatomic) IBOutlet UpaidMPMBasicColorLabel *walletInfoLabel;
@property (weak, nonatomic) IBOutlet UpaidMPMBasicColorLabel *securityInfoLabel;

@end

@implementation UpaidMPMFooter

- (id)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];

	if (self) {
		if (self.subviews.count == 0) {
			UIView *subview = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
			subview.frame = self.bounds;
			[self addSubview:subview];
		}
	}

	return self;
}

- (void)awakeFromNib {
	[super awakeFromNib];

    self.walletInfoLabel.text = UpaidMPMLocalizedString(Texts_FooterWalletInfo);
    self.securityInfoLabel.text = UpaidMPMLocalizedString(Texts_FooterSecurityInfo);
	self.betaLabel.hidden = ![UpaidApi isTestEnv];
}

- (void)layoutSubviews {
    self.frame = self.superview.bounds;
    [super layoutSubviews];
}

#pragma mark buttons

- (IBAction)showHelpButtonClicked:(id)sender {
}

@end
