//
//  UpaidMPMBaseViewController.m
//  EuroFlorist
//
//  Created by Michał Majewski on 27.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidMPMBaseViewController.h"
#import "UpaidMPMBasicColorLabel.h"
#import "UpaidMPMPayViewController.h"
#import "UpaidMPM.h"
#import "UpaidMPMLoginViewController.h"
#import "UpaidHUD.h"
#import "UpaidApi.h"

@interface UpaidMPMBaseViewController () <UIGestureRecognizerDelegate>

@property (nonatomic) UITapGestureRecognizer *tapRecognizer;
@property BOOL registeredForKeyboardNotifications;
@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UpaidMPMBasicColorLabel *loggedAsLabel;
@property (nonatomic) UIBarButtonItem *previousRightBarButton;
@property (nonatomic) CGRect contentViewFrame;

@end

@implementation UpaidMPMBaseViewController

#pragma mark view callbacks



- (void)viewDidLoad {
    [super viewDidLoad];
    //frame changed magically when textfield is edited again
    self.contentViewFrame = self.contentView.frame;
    self.view.backgroundColor = [UpaidMPM paymentInstance].uiConfig.backgroundColor;
    
    if (self.footerView != nil) {
        UIView *footer = [[[NSBundle mainBundle] loadNibNamed:[UpaidMPM paymentInstance].uiConfig.footerNibName owner:nil options:nil] objectAtIndex:0];
        self.footerView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.footerView addSubview: footer];
    }
    
    if (self.loggedAsLabel != nil) {
        if (![UpaidMPM loggedIn]) {
            self.loggedAsLabel.hidden = YES;
        } else {
            NSString* phoneText = [NSString stringWithFormat:@"+%@ %@ %@ %@", [[UpaidApi lastUsedPhone] substringToIndex:2],
                                   [[UpaidApi lastUsedPhone] substringWithRange:NSMakeRange(2,3)],
                                   [[UpaidApi lastUsedPhone] substringWithRange:NSMakeRange(5,3)],
                                   [[UpaidApi lastUsedPhone] substringWithRange:NSMakeRange(8,3)]];
            self.loggedAsLabel.text = [NSString stringWithFormat: @"%@: %@", UpaidMPMLocalizedString(Texts_LoggedAs), phoneText];
        }
    }
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self registerForKeyboardNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(testActionNotification:) name:NOTIFICATION_TEST_ACTION_FIRED object:nil];
    [UpaidMPMTools sharedInstance].activeViewController = self;
    
    self.navigationItem.titleView = [UpaidMPM paymentInstance].uiConfig.titleView;
}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self unregisterFromKeyboardNotifications];
     [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_TEST_ACTION_FIRED object:nil];
    [UpaidMPMTools sharedInstance].activeViewController = nil;
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear: animated];
    self.navigationItem.titleView = nil;
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    if (self.scrollView != nil && self.contentView != nil && !CGSizeEqualToSize(self.scrollView.contentSize, self.contentViewFrame.size)) {
        self.scrollView.contentSize = self.contentViewFrame.size;
    }
}

#pragma mark view HUD

- (void) showHUD {
    [self.view endEditing:YES];
    [UpaidHUD show];
    
    self.previousRightBarButton = [self navigationItem].rightBarButtonItem;
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [activityIndicator setColor: [UINavigationBar appearance].tintColor];
    [self navigationItem].rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    [activityIndicator startAnimating];
}

- (void) dismissHUD {
    [UpaidHUD dismiss];
    
    if (self.previousRightBarButton != nil) {
        [self navigationItem].rightBarButtonItem = self.previousRightBarButton;
    } else {
        [self navigationItem].rightBarButtonItem = nil;
    }
}

#pragma mark view submethods


- (void) createRightBarButtonWithTitle: (NSString *) title {
    UIBarButtonItem *nextButton = [[UIBarButtonItem alloc] initWithTitle:UpaidMPMLocalizedString(title) style:UIBarButtonItemStylePlain target:self action:@selector(rightBarButtonClicked:)];
    self.navigationItem.rightBarButtonItem = nextButton;
}

- (BOOL) popToPayViewController {
    return [self popToViewController: [UpaidMPMPayViewController class]];
}

- (BOOL) popToViewController: (Class) class {
    NSArray *viewControllers = self.navigationController.viewControllers;
    
    for (int i = (int)[viewControllers count] - 1; i >= 0; i--) {
        if ([viewControllers[i] class] == class) {
            [self.navigationController popToViewController:viewControllers[i] animated:YES];
            
            return YES;
        }
    }
    
    return NO;
}

- (void) showAlertWithMessage: (NSString *) message andTag: (NSInteger) tag {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:self cancelButtonTitle:nil otherButtonTitles: UpaidMPMLocalizedString(Texts_Ok), nil];
    alert.tag = tag;
    [alert show];
}

- (void) showAlertWithMessage: (NSString *) message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:message delegate: nil cancelButtonTitle:nil otherButtonTitles: UpaidMPMLocalizedString(Texts_Ok), nil];
    [alert show];
}

#pragma mark buttons

- (IBAction) rightBarButtonClicked: (UIBarButtonItem *) sender {
    
}

#pragma mark default UpaidApi error handler

- (void) onUpaidApiFail: (UpaidApiResult *) result {
    if ([UpaidMPM paymentInstance].delegate != nil && [[UpaidMPM paymentInstance].delegate respondsToSelector:@selector(onUpaidMPMError:)]) {
        [[UpaidMPM paymentInstance].delegate onUpaidMPMError:result.statusDescription];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:result.statusDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
}

#pragma mark TEST action

- (void) testActionNotification: (NSNotification *) notification {
    if ([UpaidApi isTestEnv]) {
        [self testActionFired];
    }
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

-(void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if (motion == UIEventSubtypeMotionShake) {
        if ([UpaidApi isTestEnv]) {
            [self testActionFired];
        }
    }
}

- (void) testActionFired {
    
}

#pragma mark -
#pragma mark Keyboard Management

- (void) textFieldDidBeginEditing:(UITextField *)textField {
    if (self.scrollView != nil && textField.frame.origin.y > 135) {
        [self.scrollView setContentOffset: CGPointMake(0, textField.frame.origin.y - 135) animated:YES];
    }
}

- (void) textFieldDidEndEditing:(UITextField *)textField {
    if (self.scrollView != nil && self.navigationController.visibleViewController == self) {
        [self.scrollView setContentOffset: CGPointMake(0, 0) animated:YES];
    }
}

- (void) keyboardWillShow:(NSNotification *)notification {
    if (self.contentView != nil) {
        CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        CGRect frame = self.contentView.frame;
        frame.size.height += keyboardSize.height;
        self.contentViewFrame = frame;
        self.contentView.frame = self.contentViewFrame;
    }
    
    [self.view addGestureRecognizer: self.tapRecognizer];
}

- (void) keyboardWillHide:(NSNotification *)notification {
    if (self.scrollView != nil && self.navigationController.visibleViewController == self) {
        [self.scrollView setContentOffset: CGPointMake(0, 0) animated:NO];
    }
    
    [self.view removeGestureRecognizer: self.tapRecognizer];
}

- (void) registerForKeyboardNotifications {
    if (!self.registeredForKeyboardNotifications) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        self.registeredForKeyboardNotifications = YES;
        
        self.tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTapAnywhere)];
    }
}

- (void) unregisterFromKeyboardNotifications {
    if (self.registeredForKeyboardNotifications) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
        self.registeredForKeyboardNotifications = NO;
    }
}

-(void) userTapAnywhere {
    [self.view endEditing:YES];
}

@end
