//
//  UpaidMPMInitViewController.m
//  EuroFlorist
//
//  Created by Michał Majewski on 27.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidMPMLoginViewController.h"
#import "UpaidMPMPreRegisterViewController.h"
#import "UpaidMPMPayViewController.h"
#import "UpaidMPMCardAddViewController.h"
#import "UpaidMPMAboutMCMViewController.h"

#import "UpaidMPM.h"
#import "UpaidMPMTextField.h"
#import "UpaidMPMAccentColorLabel.h"
#import "UpaidMPMBasicColorLabel.h"

#import "UpaidApiCheckTask.h"
#import "UpaidApiLoginTask.h"
#import "UpaidApiCardsListTask.h"
#import "UpaidMPMTools.h"

@interface UpaidMPMLoginViewController () <UpaidApiCheckDelegate, UpaidApiLoginDelegate, UpaidMPMToolsDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UpaidMPMTextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;
@property (weak, nonatomic) IBOutlet UpaidMPMAccentColorLabel *titleLabel;
@property (weak, nonatomic) IBOutlet UpaidMPMBasicColorLabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

enum kAlertTags : NSUInteger{
  kAlertInsertCode,
  kAlertBadCode,
  kAlertMistake,
  kAlertExit,
  kAlertBadPhone,
  kAlertLogout
};

@end

@implementation UpaidMPMLoginViewController

- (instancetype)init {
    self = [super initWithNibName:[UpaidMPM sharedInstance].uiConfig.loginViewControllerNibName bundle:nil];
    
    return self;
}

#pragma mark view

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel.text = UpaidMPMLocalizedString(Texts_LoginTitleLabel);
    self.phoneLabel.text = UpaidMPMLocalizedString(Texts_LoginPhoneLabel);
    self.infoLabel.text = [NSString stringWithFormat:@"%@ %@", [UpaidMPM paymentInstance].appName, UpaidMPMLocalizedString(Texts_LoginInfoLabel)];
    self.phoneTextField.placeholder = UpaidMPMLocalizedString(Texts_LoginPhonePlaceholder);
    [self.logoutButton setTitle:UpaidMPMLocalizedString(Texts_LoginLogoutButtonText) forState:UIControlStateNormal];
    [self.logoutButton setTitle:UpaidMPMLocalizedString(Texts_LoginLogoutButtonText) forState:UIControlStateHighlighted];
    
    [self.logoutButton setTitleColor: [UpaidMPM paymentInstance].uiConfig.accentColor forState:UIControlStateNormal];
    [self createRightBarButtonWithTitle: UpaidMPMLocalizedString(Texts_Next)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"mpm_exit"] style:UIBarButtonItemStylePlain target:self action:@selector(exitButtonClicked)];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSString *lastUsedPhone = [UpaidApi lastUsedPhone];
    NSString *codeForLastUsedPhone = [UpaidApi smsCodeForPhone: lastUsedPhone];
    
    self.logoutButton.hidden = lastUsedPhone == nil || [codeForLastUsedPhone isEqualToString: @""];
    
    if (!lastUsedPhone) {
        if ([UpaidMPM paymentInstance].initialPhone != nil) {
            self.phoneTextField.text = [UpaidMPM paymentInstance].initialPhone;
        } else {
            self.phoneTextField.text = @"";
        }
    } else {
        self.phoneTextField.text = lastUsedPhone;
        UpaidMPMBaseViewController *activeController = [UpaidMPMTools sharedInstance].activeViewController;
        
        //if user was on logged in and pressed back button, so don't want to be automatically logged in again
        if (!([activeController isKindOfClass: [UpaidMPMPayViewController class]] || [activeController isKindOfClass: [UpaidMPMCardAddViewController class]])) {
            
            //automatically start login process
            [self showHUD];
            
            if (![codeForLastUsedPhone isEqualToString: @""]) {
                [self executeLoginTaskWithCode: codeForLastUsedPhone];
            } else {
                [self executeCheckTask];
            }
        }
    }
}

#pragma mark submethods
- (void) executeCheckTask {
    [self.view endEditing:YES];
    UpaidApiCheckTask *task = [[UpaidApiCheckTask alloc] initWithData:[[UpaidApiCheckData alloc] initWithPhone: self.phoneTextField.text] andDelegate:self];
    [task execute];
}

- (void) executeLoginTaskWithCode: (NSString *) code {
    [self.view endEditing:YES];
    UpaidApiLoginTask *task = [[UpaidApiLoginTask alloc] initWithData:[[UpaidApiLoginData alloc] initWithPhone: self.phoneTextField.text andCode: code] andDelegate:self];
    [task execute];
}

- (void) exit {
    [UpaidMPM.delegate afterUnsuccessfullPaymentWithNavigationController: self.navigationController];
}

#pragma mark buttons management

- (IBAction)logoutButtonClicked:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:UpaidMPMLocalizedString(Texts_LoginLogoutAlert) delegate:self cancelButtonTitle:UpaidMPMLocalizedString(Texts_No) otherButtonTitles:UpaidMPMLocalizedString(Texts_Yes), nil];
    alert.tag = kAlertLogout;
    [alert show];
}

- (void) logout {
    [UpaidMPMTools logout];
    self.logoutButton.hidden = YES;
    self.phoneTextField.text = @"";
    [self.phoneTextField becomeFirstResponder];
}

- (void) exitButtonClicked {
    if (![UpaidMPM paymentInstance].showExitWarning) {
        [self exit];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:UpaidMPMLocalizedString(Texts_LoginExitAlert) delegate:self cancelButtonTitle:UpaidMPMLocalizedString(Texts_No) otherButtonTitles:UpaidMPMLocalizedString(Texts_Yes), nil];
        alert.tag = kAlertExit;
        [alert show];
    }
}

- (IBAction)readMoreButtonClicked {
    [self.navigationController pushViewController:[[UpaidMPMAboutMCMViewController alloc] init] animated:YES];
}

- (IBAction) rightBarButtonClicked:(UIBarButtonItem *)sender {
    if ([self.phoneTextField.text length] < 9) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:UpaidMPMLocalizedString(UpaidMPMLocalizedString(Texts_LoginPhoneError)) delegate:self cancelButtonTitle: UpaidMPMLocalizedString(Texts_Ok) otherButtonTitles: nil];
        alert.tag = kAlertBadPhone;
        [alert show];
    } else {
        NSString *phone = self.phoneTextField.text;
        
        if ([phone length] == 9) {
            phone = [NSString stringWithFormat: @"48%@", phone];
        }
        
        self.phoneTextField.text = phone;
        NSString *codeForLastUsedPhone = [UpaidApi smsCodeForPhone: self.phoneTextField.text ];
        [self showHUD];
        
        //automatically start login process
        if (![codeForLastUsedPhone isEqualToString: @""]) {
            [self executeLoginTaskWithCode: codeForLastUsedPhone];
        } else {
            [self executeCheckTask];
        }
    }
}

- (void) showAlertForCode {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:UpaidMPMLocalizedString(Texts_LoginEnterCode) delegate:self cancelButtonTitle:UpaidMPMLocalizedString(Texts_Cancel) otherButtonTitles: UpaidMPMLocalizedString(Texts_Ok), nil];
    
    alert.tag = kAlertInsertCode;
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    UITextField *textField = [alert textFieldAtIndex:0];
    textField.keyboardType = UIKeyboardTypeNumberPad;
    textField.placeholder = UpaidMPMLocalizedString(Texts_LoginEnterCodePlaceholder);
    textField.text = [UpaidApi testSmsCodeForPhone:self.phoneTextField.text];
    
    [alert show];
}

- (void) onNoUserEvent {
    [self dismissHUD];
    
    NSString *phone = self.phoneTextField.text;
    
    if (phone.length > 11 && [[phone substringToIndex: 2] isEqualToString: @"48"]) {
        phone = [phone substringToIndex: 11];
    }
    
    UpaidMPMPreRegisterViewController *viewController = [[UpaidMPMPreRegisterViewController alloc] initWithPhone: phone];
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark UpaidApiCheckDelegate implementation

- (void) onUpaidApiCheckSuccess: (UpaidApiCheckResult *) result {
    [self showAlertForCode];
}

- (void) onUpaidApiCheckNoUser: (UpaidApiCheckResult *) result {
    [self onNoUserEvent];
}

- (void) onUpaidApiCheckFail: (UpaidApiCheckResult *) result {
    [self dismissHUD];
    [self onUpaidApiFail:result];
}

#pragma mark UpaidApiLoginDelegate implementation

- (void) onUpaidApiLoginSuccess: (UpaidApiLoginResult *) result {
    [UpaidMPMTools startCardsListWithHUD: NO];
}

- (void) onUpaidApiLoginFail: (UpaidApiLoginResult *) result {
    [self dismissHUD];
    [self onUpaidApiFail:result];
}

- (void) onUpaidApiLoginBadCode: (UpaidApiLoginResult *) result {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:UpaidMPMLocalizedString(Texts_LoginWrongCodeError) delegate:self cancelButtonTitle:UpaidMPMLocalizedString(Texts_Cancel) otherButtonTitles:UpaidMPMLocalizedString(Texts_LoginEnterCodeAgain), UpaidMPMLocalizedString(Texts_LoginAskForNewCode), nil];
    alert.tag = kAlertBadCode;
    [alert show];
}

- (void) onUpaidApiLoginNoUser: (UpaidApiLoginResult *) result {
    [self onNoUserEvent];
}

#pragma mark UpaidMPMToolsDelegate implementation

- (void) cardsListChanged {
    [self dismissHUD];
    
    [UpaidMPM paymentInstance].card = [[UpaidMPMTools sharedInstance].cards defaultCard];
    
    [UpaidMPM setLoggedIn: YES];
    
    UpaidMPMPayViewController *viewController = [[UpaidMPMPayViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void) cardsListError: (UpaidApiCardsListResult *) result {
    [self dismissHUD];
    [self onUpaidApiFail:result];
}

#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == kAlertBadPhone) {
        [self.phoneTextField becomeFirstResponder];
        return;
    }
    
    if (buttonIndex == 0) {
        [self dismissHUD];
        return;
    }
    
    switch (alertView.tag) {
        case kAlertLogout:
            [self logout];
            break;
        case kAlertExit:
            if (buttonIndex == 1) {
                [self exit];
                break;
            }
        case kAlertBadCode:
            if (buttonIndex == 1) {
                [self showAlertForCode];
            } else if (buttonIndex == 2) {
                [self executeCheckTask];
            }
            break;
        case kAlertInsertCode: {
            NSString *codeText = [alertView textFieldAtIndex: 0].text;
            
            if ([codeText length] > 0) {
                [self executeLoginTaskWithCode: codeText];
            } else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:UpaidMPMLocalizedString(Texts_LoginEmptyCodeError) delegate:self cancelButtonTitle:UpaidMPMLocalizedString(Texts_Cancel) otherButtonTitles: UpaidMPMLocalizedString(Texts_Ok), nil];
                alert.tag = kAlertMistake;
                [alert show];
            }
            break;
        }
        case kAlertMistake:
            [self showAlertForCode];
            break;
    }
}

#pragma mark UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if ([textField.text length] == 0) {
        textField.text = @"48";
    }
}

- (void) textFieldDidEndEditing:(UITextField *)textField {
    if ([textField.text length] == 2) {
        textField.text = @"";
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (range.length == 0 && [textField.text length] >= 12) {
        return NO;
    }
    
    return YES;
}

@end
