//
//  UpaidMPMRegisterViewController.h
//  EuroFlorist
//
//  Created by Michał Majewski on 27.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidMPMBaseViewController.h"

@interface UpaidMPMRegisterViewController : UpaidMPMBaseViewController

- (id)initWithPhone: (NSString *) phone;

@end
