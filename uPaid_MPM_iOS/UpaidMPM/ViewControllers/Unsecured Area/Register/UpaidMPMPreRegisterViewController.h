//
//  UpaidMPMPreRegisterViewController.h
//  MpmTestApp
//
//  Created by Michał Majewski on 23.10.2014.
//  Copyright (c) 2014 uPaid. All rights reserved.
//

#import "UpaidMPMBaseViewController.h"

@interface UpaidMPMPreRegisterViewController : UpaidMPMBaseViewController

- (id)initWithPhone: (NSString *) phone;

@end
