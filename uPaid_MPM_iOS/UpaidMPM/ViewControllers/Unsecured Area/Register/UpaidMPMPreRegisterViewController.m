//
//  UpaidMPMPreRegisterViewController.m
//  MpmTestApp
//
//  Created by Michał Majewski on 23.10.2014.
//  Copyright (c) 2014 uPaid. All rights reserved.
//

#import "UpaidMPMPreRegisterViewController.h"
#import "UpaidMPM.h"
#import "UpaidMPMRegisterViewController.h"
#import "UpaidMPMMasterPassSelectorViewController.h"
#import "UpaidMPMBasicColorLabel.h"
#import "UpaidMPMAccentColorLabel.h"
#import "UpaidMPMButton.h"
#import "UpaidMPMPayViewController.h"

@interface UpaidMPMPreRegisterViewController ()

@property (nonatomic) NSString *phone;
@property (weak, nonatomic) IBOutlet UpaidMPMBasicColorLabel *summaryLabel;
@property (weak, nonatomic) IBOutlet UpaidMPMAccentColorLabel *summaryTitleLabel;
@property (weak, nonatomic) IBOutlet UpaidMPMAccentColorLabel *informationTitleLabel;
@property (weak, nonatomic) IBOutlet UpaidMPMBasicColorLabel *informationLabel;
@property (weak, nonatomic) IBOutlet UpaidMPMButton *registerButton;

@end

@implementation UpaidMPMPreRegisterViewController

- (id)initWithPhone: (NSString *) phone{
    self = [super initWithNibName: [UpaidMPM paymentInstance].uiConfig.preRegisterViewControllerNibName bundle:nil];
    
    if (self) {
        self.phone = phone;
    }
    
    return self;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    self.summaryTitleLabel.text = UpaidMPMLocalizedString(Texts_Summary);
    self.informationTitleLabel.text = UpaidMPMLocalizedString(Texts_Information);
    self.informationLabel.text = UpaidMPMLocalizedString(Texts_PreRegisterInformationText);
    [self.registerButton setTitle: UpaidMPMLocalizedString(Texts_PreRegisterRegisterButton) forState:UIControlStateNormal];
    [self.registerButton setTitle: UpaidMPMLocalizedString(Texts_PreRegisterRegisterButton) forState:UIControlStateHighlighted];
    
    self.summaryLabel.text = [[[UpaidMPM paymentInstance].products firstObject] summaryDescription];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    
    if ([UpaidMPM paymentInstance].card != nil) {
        UpaidMPMPayViewController *viewController = [[UpaidMPMPayViewController alloc] init];
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

#pragma mark buttons

- (IBAction)registerButtonClicked:(id)sender {
    UpaidMPMRegisterViewController *viewController = [[UpaidMPMRegisterViewController alloc] initWithPhone: self.phone];
    [self.navigationController pushViewController:viewController animated:YES];
    self.summaryLabel.text = [[[UpaidMPM paymentInstance].products firstObject] summaryDescription];
}

- (IBAction)buyWithMasterpassButtonClicked:(id)sender {
     [self.navigationController pushViewController:[[UpaidMPMMasterPassSelectorViewController alloc] init] animated:YES];
}


@end
