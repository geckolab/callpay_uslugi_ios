//
//  UpaidMPMRegisterViewController.m
//  EuroFlorist
//
//  Created by Michał Majewski on 27.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidMPMRegisterViewController.h"
#import "UpaidMPMTextField.h"
#import "UpaidMPMCheckboxButton.h"
#import "NSString+Validation.h"
#import "UpaidMPM.h"
#import "UpaidMPMAccentColorLabel.h"
#import "UpaidMPMBasicColorLabel.h"
#import "UpaidApiRegisterTask.h"
#import "UpaidMPMLoginViewController.h"

@interface UpaidMPMRegisterViewController () <UITextFieldDelegate, UpaidApiRegisterDelegate>

@property (nonatomic) NSString *phone;
@property (weak, nonatomic) IBOutlet UpaidMPMTextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UpaidMPMTextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UpaidMPMTextField *emailTextField;
@property (weak, nonatomic) IBOutlet UpaidMPMTextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UpaidMPMTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UpaidMPMTextField *passwordConfirmTextField;
@property (weak, nonatomic) IBOutlet UpaidMPMBasicColorLabel *rulesLabel;
@property (weak, nonatomic) IBOutlet UpaidMPMAccentColorLabel *titleLabel;
@property (weak, nonatomic) IBOutlet UpaidMPMBasicColorLabel *mailingLabel;

@property (weak, nonatomic) IBOutlet UpaidMPMCheckboxButton *rulesCheckbox;
@property (weak, nonatomic) IBOutlet UpaidMPMCheckboxButton *mailingCheckbox;

enum kAlertTags : NSUInteger{
  kAlertBadFirstName,
  kAlertBadLastName,
  kAlertBadEmail,
  kAlertBadPhoneNumber,
  kAlertBadPassword,
  kAlertPasswordNotEqual,
  kAlertRules
};

@end

@implementation UpaidMPMRegisterViewController

- (id)initWithPhone: (NSString *) phone{
    self = [super initWithNibName: [UpaidMPM paymentInstance].uiConfig.registerViewControllerNibName bundle:nil];
    
    if (self) {
        self.phone = phone;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel.text = UpaidMPMLocalizedString(Texts_RegistrationTitleLabel);
    self.mailingLabel.text = UpaidMPMLocalizedString(Texts_RegistrationMarketingText);
    self.rulesLabel.text = UpaidMPMLocalizedString(Texts_RegistrationRulesText);
    
    self.firstNameTextField.placeholder = UpaidMPMLocalizedString(Texts_RegistrationFirstNamePlaceholder);
    self.lastNameTextField.placeholder = UpaidMPMLocalizedString(Texts_RegistrationLastNamePlaceholder);
    self.emailTextField.placeholder = UpaidMPMLocalizedString(Texts_RegistrationEmailPlaceholder);
    self.phoneNumberTextField.placeholder = UpaidMPMLocalizedString(Texts_RegistrationPhonePlaceholder);
    self.passwordTextField.placeholder = UpaidMPMLocalizedString(Texts_RegistrationPasswordPlaceholder);
    self.passwordConfirmTextField.placeholder = UpaidMPMLocalizedString(Texts_RegistrationConfirmPasswordPlaceholder)
    ;
    self.phoneNumberTextField.text = self.phone;
    [UpaidApi setLastUsedPhone: nil];
    [self createRightBarButtonWithTitle: UpaidMPMLocalizedString(Texts_Register)];
    [self.emailTextField setHintMessage: UpaidMPMLocalizedString(Texts_RegistrationEmailHint)];
    [self.passwordTextField setHintMessage: UpaidMPMLocalizedString(Texts_RegistrationPasswordHint)];
}

#pragma mark buttons
- (IBAction)openRules:(id)sender {
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: @"https://upaid.pl/files/regulamin_upaid_html.html"]];
}

- (void)rightBarButtonClicked:(UIBarButtonItem *)sender {
    [self.view endEditing: YES];
    
    if ([self.firstNameTextField.text length] == 0) {
        [self showAlertWithMessage: UpaidMPMLocalizedString(Texts_RegistrationFirstNameError) andTag:kAlertBadFirstName];
    } else if ([self.lastNameTextField.text length] == 0) {
        [self showAlertWithMessage:UpaidMPMLocalizedString(Texts_RegistrationLastNameError) andTag:kAlertBadLastName];
    } else if (![self.emailTextField.text isEmail]) {
        [self showAlertWithMessage: UpaidMPMLocalizedString(Texts_RegistrationEmailError) andTag:kAlertBadEmail];
    } else if ([self.phoneNumberTextField.text length] < 11) {
        [self showAlertWithMessage: UpaidMPMLocalizedString(Texts_RegistrationPhoneError) andTag:kAlertBadPhoneNumber];
    } else if (![self.passwordTextField.text isValidPassword]) {
        [self showAlertWithMessage: UpaidMPMLocalizedString(Texts_RegistrationPasswordError) andTag:kAlertBadPassword];
    } else if (![self.passwordConfirmTextField.text isEqualToString: self.passwordTextField.text]) {
        [self showAlertWithMessage: UpaidMPMLocalizedString(Texts_RegistrationConfirmPasswordError) andTag:kAlertPasswordNotEqual];
    } else if (!self.rulesCheckbox.selected) {
        [self showAlertWithMessage: UpaidMPMLocalizedString(Texts_RegistrationRulesError) andTag:kAlertRules];
    } else {
        UpaidApiRegisterData *registerData = [[UpaidApiRegisterData alloc] initWithPhone:self.phoneNumberTextField.text andEmail:self.emailTextField.text andFirstName:self.firstNameTextField.text andLastName:self.lastNameTextField.text andMailing:self.mailingCheckbox.selected];
        registerData.password = self.passwordTextField.text;

        [[[UpaidApiRegisterTask alloc] initWithData: registerData andDelegate:self] execute];
        [self showHUD];
    }
}

#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case kAlertBadFirstName:
            [self.firstNameTextField becomeFirstResponder];
            break;
        case kAlertBadLastName:
            [self.lastNameTextField becomeFirstResponder];
            break;
        case kAlertBadEmail:
            [self.emailTextField becomeFirstResponder];
            break;
        case kAlertRules:
            [self.rulesCheckbox becomeFirstResponder];
            break;
        case kAlertBadPhoneNumber:
            [self.phoneNumberTextField becomeFirstResponder];
            break;
        case kAlertBadPassword:
            [self.passwordTextField becomeFirstResponder];
            break;
        case kAlertPasswordNotEqual:
            [self.passwordConfirmTextField becomeFirstResponder];
            break;
    }
}

#pragma mark UITextFieldDelegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (range.length == 0) {
        if (textField == self.phoneNumberTextField && [textField.text length] == 11) {
            return NO;
        }
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.firstNameTextField) {
        [self.lastNameTextField becomeFirstResponder];
    } else if (textField == self.lastNameTextField) {
        [self.emailTextField becomeFirstResponder];
    } else if (textField == self.emailTextField) {
        [self.passwordTextField becomeFirstResponder];
    } else if (textField == self.passwordTextField) {
        [self.passwordConfirmTextField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    
    return NO;
}

#pragma mark UpaidApiRegisterDelegate implementation

- (void) onUpaidApiRegisterSuccess: (UpaidApiRegisterResult *) result {
    [UpaidApi setLastUsedPhone: self.phoneNumberTextField.text];
    [self dismissHUD];
    
    [self popToViewController: [UpaidMPMLoginViewController class]];
}

- (void) onUpaidApiRegisterFail: (UpaidApiRegisterResult *) result {
    [self dismissHUD];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"" message:result.statusDescription delegate:self cancelButtonTitle:nil otherButtonTitles: UpaidMPMLocalizedString(Texts_Ok), nil];
    [alert show];
}

@end
