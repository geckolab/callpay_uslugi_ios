//
//  MasterPassSelectorViewController.m
//  MpmTestApp
//
//  Created by Paweł Grzmil on 31.07.2014.
//  Copyright (c) 2014 uPaid. All rights reserved.
//

#import "UpaidMPMMasterPassSelectorViewController.h"
#import "UpaidApiGetMasterPassSelectorLinkTask.h"
#import "UpaidApiShoppingCartItemArray.h"
#import "UpaidApiShoppingCartItem.h"
#import "UpaidMPM.h"
#import "UpaidApiCard+Extensions.h"
#import "UpaidMPMPayViewController.h"

@interface UpaidMPMMasterPassSelectorViewController () <UIWebViewDelegate, UpaidApiGetMasterPassSelectorLinkDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation UpaidMPMMasterPassSelectorViewController

- (instancetype)init {
    self = [super initWithNibName:[UpaidMPM paymentInstance].uiConfig.masterpassSelectorViewControllerNibName bundle:nil];
    
    return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	[self showHUD];

	UpaidApiShoppingCartItemArray *cart = [[UpaidApiShoppingCartItemArray alloc] init];
    
	for (UpaidMPMProduct *product in[UpaidMPM sharedInstance].products) {
		[cart addObject:[[UpaidApiShoppingCartItem alloc] initWithDescription:product.name andQuantity:product.quantity andAmount:product.amount andImage_url:product.imageUrl]];
	}
    
    if (cart.count == 0) {
        cart = nil;
    }

	UpaidApiGetMasterPassSelectorLinkData *data = [[UpaidApiGetMasterPassSelectorLinkData alloc] initWithCallbackUrl:@"" andCurrency:@"PLN" andShoppingCart:cart];
	[[[UpaidApiGetMasterPassSelectorLinkTask alloc] initWithData:data andDelegate:self] execute];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
}

#pragma mark UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if ([[[request URL] absoluteString] hasPrefix:@"js-call://setCardId"]) {
        NSArray *components = [[[request URL] absoluteString] componentsSeparatedByString:@":"];
        
        UpaidApiCard *card = [[UpaidApiCard alloc] init];
        [card setupMasterpassCardWithId: components[2]];
        [UpaidMPM paymentInstance].card = card;
        
        if (![self popToPayViewController]) {
            [self.navigationController popViewControllerAnimated: YES];
        }
        
        return NO;
    }
    
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    [self showHUD];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self dismissHUD];
}

#pragma mark UpaidApiGetMasterPassSelectorLinkDelegate implementation

- (void)onUpaidApiGetMasterPassSelectorLinkSuccess:(UpaidApiGetMasterPassSelectorLinkResult *)result {
	[self dismissHUD];
	NSURL *url = [NSURL URLWithString:result.redirectUrl];
	NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
	[self.webView loadRequest:requestObj];
}

- (void)onUpaidApiGetMasterPassSelectorLinkFail:(UpaidApiGetMasterPassSelectorLinkResult *)result {
	[self dismissHUD];
    [self onUpaidApiFail: result];
    [self.navigationController popViewControllerAnimated: YES];
}

@end
