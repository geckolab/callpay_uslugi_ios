//
//  UpaidMPMBaseViewController.h
//  EuroFlorist
//
//  Created by Michał Majewski on 27.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidApiResult.h"
#import "UpaidApiCard.h"
#import "UpaidApi.h"

@interface UpaidMPMBaseViewController : UIViewController <UITextFieldDelegate>

- (void) createRightBarButtonWithTitle: (NSString *) title;
- (IBAction) rightBarButtonClicked: (UIBarButtonItem *) sender;

- (void) onUpaidApiFail: (UpaidApiResult *) result;

- (void) showHUD;
- (void) dismissHUD;
- (BOOL) popToPayViewController;
- (BOOL) popToViewController: (Class) class;

- (void) textFieldDidBeginEditing:(UITextField *)textField;
- (void) textFieldDidEndEditing:(UITextField *)textField;
- (void) showAlertWithMessage: (NSString *) message andTag: (NSInteger) tag;
- (void) showAlertWithMessage: (NSString *) message;

- (void) testActionFired;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@end
