//
//  UpaidMPMPayViewController.m
//  EuroFlorist
//
//  Created by Michał Majewski on 27.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidMPMPayViewController.h"
#import "UpaidMPMAccentColorLabel.h"
#import "UpaidMPMBasicColorLabel.h"
#import "UpaidMPMTextField.h"
#import "UpaidMPMCheckboxButton.h"
#import "UpaidMPM.h"
#import "UpaidApiBuyTask.h"
#import "UpaidMPMCardsListViewController.h"
#import "UpaidMPMCardHint.h"
#import "UpaidMPMCardAddViewController.h"
#import "UpaidMPMProductsListTableView.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiCard+Extensions.h"
#import "UIImageView+WebCache.h"
//#import <SDWebImage/UIImageView+WebCache.h>
#import "UpaidMPMButton.h"
#import <QuartzCore/QuartzCore.h>
#import "UpaidMPMCardVisualImageView.h"

@interface UpaidMPMPayViewController () <UpaidApiBuyDelegate, UpaidMPMToolsDelegate>

@property (weak, nonatomic) IBOutlet UILabel *cardLabel;
@property (weak, nonatomic) IBOutlet UpaidMPMTextField *cvcTextField;
@property (weak, nonatomic) IBOutlet UIView *cardContainer;
@property (weak, nonatomic) IBOutlet UIView *amountContainer;
@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UIView *buttonsContainer;
@property (weak, nonatomic) IBOutlet UpaidMPMBasicColorLabel *noCardsLabel;

@property (weak, nonatomic) IBOutlet UpaidMPMProductsListTableView *productsTableView;
@property (weak, nonatomic) IBOutlet UpaidMPMCardVisualImageView *cardVisualImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *cardVisualActivityIndicator;
@property (weak, nonatomic) IBOutlet UpaidMPMButton *addCardButton;
@property (weak, nonatomic) IBOutlet UIButton *changeCardButton;
@property (weak, nonatomic) IBOutlet UpaidMPMAccentColorLabel *acceptTransactionLabel;
@property (weak, nonatomic) IBOutlet UpaidMPMAccentColorLabel *summaryLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loggedAsHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cvcTextFieldHeightConstraint;


enum kAlertTags : NSUInteger{
  kAlertFailPayment,
  kAlertNoCards,
  kAlertSameTransaction,
  kAlertEnterCVC
};

@end

@implementation UpaidMPMPayViewController

- (instancetype)init {
    self = [super initWithNibName:[UpaidMPM paymentInstance].uiConfig.payViewControllerNibName bundle:nil];
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.amountLabel.text = [[[UpaidMPM paymentInstance].products firstObject] summaryDescriptionForPayment];
    self.noCardsLabel.text = UpaidMPMLocalizedString(Texts_PayNoCardsLabel);
    self.acceptTransactionLabel.text = UpaidMPMLocalizedString(Texts_PayAcceptTransaction);
    self.summaryLabel.text = UpaidMPMLocalizedString(Texts_Summary);
    
    if (![UpaidMPM loggedIn]) {
        self.addCardButton.hidden = YES;
        self.cvcTextFieldHeightConstraint.constant = 0;
        self.loggedAsHeightConstraint.constant = 0;
        self.changeCardButton.hidden = YES;
    } else {
        [self.cvcTextField setHintTarget:self andSelector:@selector(showCVCHint)];
        [self.addCardButton setTitle: UpaidMPMLocalizedString(Texts_AddCardButton) forState: UIControlStateNormal];
        [self.addCardButton setTitle: UpaidMPMLocalizedString(Texts_AddCardButton) forState: UIControlStateHighlighted];
    }
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
    [self updateCardLabel];
    self.cvcTextField.text = @"";
}


-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear: animated];
    
    if ([self isMovingFromParentViewController]) {
        [UpaidMPM paymentInstance].card = nil;
    }
}

- (void) updateCardLabel {
    self.cvcTextField.text = @"";
    
    if ([UpaidMPM paymentInstance].card != nil) {
        UpaidApiCard *selectedCard = [UpaidMPM paymentInstance].card;
        self.cvcTextField.hidden = [selectedCard isMasterPassCard];
        self.noCardsLabel.hidden = YES;
        [self.cardVisualActivityIndicator startAnimating];
        self.cardLabel.text = selectedCard.nickname;
        self.cvcTextField.placeholder = UpaidMPMLocalizedString([selectedCard hasMpin] ? Texts_PayMpinPlaceholder :Texts_PayCVCPlaceholder);
        
        [self.cardVisualImageView sd_setImageWithURL:(NSURL *) selectedCard.frontImageURL completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [self.cardVisualActivityIndicator stopAnimating];
        }];
    } else {
        self.cardLabel.text = UpaidMPMLocalizedString(Texts_PayNoCardsAlert);
        self.cardVisualImageView.image = nil;
        self.noCardsLabel.hidden = NO;
    }
}

- (void) showAlertWithMessage: (NSString *) message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:nil cancelButtonTitle:nil otherButtonTitles: UpaidMPMLocalizedString(Texts_Ok), nil];
    [alert show];
}

#pragma mark buttons

- (void) showCVCHint {
    [UpaidMPMCardHint showWithMpin: YES];
}

- (IBAction)selectedCardHintButtonClicked {
    [self showAlertWithMessage: UpaidMPMLocalizedString(Texts_PaySelectCardHint)];
}

- (IBAction)addCardButtonClicked:(id)sender {
    [self.navigationController pushViewController:[[UpaidMPMCardAddViewController alloc] init] animated:YES];
}

- (IBAction) buyButtonClicked:(UIButton *)sender {
    [self.view endEditing: YES];
    UpaidApiCard *selectedCard = [UpaidMPM paymentInstance].card;
    //when card was locked during payment, and cards list failed
    if (selectedCard == nil) {
        [self showAlertWithMessage: UpaidMPMLocalizedString(Texts_PayNoCardsAlert)];
    } else if (![selectedCard isMasterPassCard] && ([self.cvcTextField.text length] != ([selectedCard hasMpin] ? 4 : 3))) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:UpaidMPMLocalizedString([selectedCard hasMpin] ? Texts_PayMpinError : Texts_PayCVCError) delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        alert.tag = kAlertEnterCVC;
        [alert show];
    } else {
        if ([[UpaidMPM paymentInstance].delegate respondsToSelector:@selector(beforePaymentWithCard:)]) {
            if (![[UpaidMPM paymentInstance].delegate performSelector:@selector(beforePaymentWithCard:) withObject:selectedCard]){
                return;
            }
        }
        
        [self executeBuyTaskWithForce: NO];
    }
}

- (void) executeBuyTaskWithForce: (BOOL) force {
    UpaidApiBuyTask *task = [[UpaidApiBuyTask alloc] initWithData: [[UpaidApiBuyData alloc] initWithBuyData: [UpaidMPMTools transactionDataWithCVC2:self.cvcTextField.text] andForce: (int) force andPartnerId:[[UpaidApi partnerId] intValue]] andDelegate:self];
    
    [task execute];
    [self showHUD];
}

- (IBAction)changeCardButtonClicked:(id)sender {
    [self goToCardsList];
}

- (void) goToCardsList {
    [self.view endEditing: YES];
    UpaidMPMCardsListViewController *viewController = [[UpaidMPMCardsListViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark UpaidApiBuyDelegate implementation

- (void) onUpaidApiBuySuccess: (UpaidApiBuyResult *) result {
    [self dismissHUD];
    [UpaidMPM.delegate afterSuccessfullPaymentWithNavigationController:self.navigationController andId:result.upaidId andAdditionalData:result.more];
}

- (void) onUpaidApiBuyFail: (UpaidApiBuyResult *) result {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:result.statusDescription delegate:self cancelButtonTitle:UpaidMPMLocalizedString(Texts_PayCancelPaymentButton) otherButtonTitles: UpaidMPMLocalizedString(Texts_PayRetryPaymentButton), nil];
    alert.tag = kAlertFailPayment;
    [alert show];
}

- (void) onUpaidApiBuyCardBlocked: (UpaidApiBuyResult *) result {
    [self showAlertWithMessage:result.statusDescription];
    [UpaidMPM paymentInstance].card = nil;
    [self updateCardLabel];
    
    [UpaidMPMTools startCardsListWithHUD: NO];
}

- (void) onUpaidApiBuySameTransaction: (UpaidApiBuyResult *) result {
    [self dismissHUD];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"" message: UpaidMPMLocalizedString(Texts_PayRepeatedTransactionAlert) delegate:self cancelButtonTitle: UpaidMPMLocalizedString(Texts_Cancel) otherButtonTitles: UpaidMPMLocalizedString(Texts_Pay), nil];
    alert.tag = kAlertSameTransaction;
    [alert show];
}

#pragma mark UpaidMPMToolsDelegate

- (void) cardsListChanged {
    [self dismissHUD];
    [UpaidMPM paymentInstance].card = [[UpaidMPMTools sharedInstance].cards defaultCard];
    [self updateCardLabel];
}

- (void) cardsListError: (UpaidApiCardsListResult *) result {
    [self dismissHUD];
    [self onUpaidApiFail:result];
}

#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case kAlertEnterCVC:
            [self.cvcTextField becomeFirstResponder];
            break;
        case kAlertSameTransaction:
            if (buttonIndex == 1) {
                [self executeBuyTaskWithForce: YES];
            }
            break;
        case kAlertNoCards:
            if (buttonIndex == 0) {
                [UpaidMPMTools logout];
                [self.navigationController popViewControllerAnimated: YES];
            } else {
                [self.navigationController pushViewController:[[UpaidMPMCardsListViewController alloc] init] animated:YES];
            }
            break;
        case kAlertFailPayment:
            [self dismissHUD];
            
            if (buttonIndex == 0) {
                [UpaidMPM.delegate afterUnsuccessfullPaymentWithNavigationController: self.navigationController];
            }
            break;
    }
}

@end
