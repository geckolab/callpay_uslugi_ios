//
//  UpaidMPMCardVerifyViewController.h
//  EuroFlorist
//
//  Created by Michał Majewski on 03.03.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidMPMBaseViewController.h"
#import "UpaidApiCard.h"

@interface UpaidMPMCardVerifyViewController : UpaidMPMBaseViewController

- (id)initWithCard: (UpaidApiCard *) card andPaymentId: (int) paymentId andPares: (NSString *) pares andCvc: (NSString *) cvc;

@end
