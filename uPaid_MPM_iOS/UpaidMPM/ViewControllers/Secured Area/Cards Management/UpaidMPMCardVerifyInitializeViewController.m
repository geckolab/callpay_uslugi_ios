//
//  UpaidMPMCardInitializeViewController.m
//  MpmTestApp
//
//  Created by Michał Majewski on 15.10.2014.
//  Copyright (c) 2014 uPaid. All rights reserved.
//

#import "UpaidMPMCardVerifyInitializeViewController.h"
#import "UpaidMPMCardVerifyViewController.h"
#import "UpaidMPM.h"

@interface UpaidMPMCardVerifyInitializeViewController () <UIWebViewDelegate>

@property (nonatomic) UpaidApiCard *card;
@property (nonatomic) int paymentId;
@property (nonatomic) NSString *pageContent;
@property (nonatomic) NSString *cvc;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation UpaidMPMCardVerifyInitializeViewController

- (id)initWithCard: (UpaidApiCard *) card andPaymentId: (int) paymentId andPageContent: (NSString *) pageContent andCvc: (NSString *) cvc{
    self = [super initWithNibName: [UpaidMPM paymentInstance].uiConfig.cardVerifyInitializeViewControllerNibName bundle:nil];
    
    if (self) {
        self.card = card;
        self.paymentId = paymentId;
        self.pageContent = pageContent;
        self.cvc = cvc;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.webView loadHTMLString: self.pageContent baseURL:nil];
}

- (void) goToVerifyViewController: (NSString *) pares {
    [self dismissHUD];
    NSMutableArray *viewControllers = [NSMutableArray arrayWithArray: self.navigationController.viewControllers];
    
    [viewControllers removeLastObject];
    [viewControllers addObject: [[UpaidMPMCardVerifyViewController alloc] initWithCard: self.card andPaymentId:self.paymentId andPares:pares andCvc: self.cvc]];
    [self.navigationController setViewControllers:viewControllers animated:YES];
}

#pragma mark UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSLog(@"%@", [[request URL] absoluteString]);
    if ([[[request URL] absoluteString] hasPrefix:@"js-call://setData"]) {
        NSArray *components = [[[request URL] absoluteString] componentsSeparatedByString:@":"];
        [self goToVerifyViewController: [components lastObject]];
        
        return NO;
    }
    
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    [self showHUD];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self dismissHUD];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [[[UIAlertView alloc] initWithTitle: @"" message:UpaidMPMLocalizedString(Texts_TechnicalError) delegate:nil cancelButtonTitle:UpaidMPMLocalizedString(Texts_Ok) otherButtonTitles: nil] show];
    [self.navigationController popViewControllerAnimated: YES];
}

@end
