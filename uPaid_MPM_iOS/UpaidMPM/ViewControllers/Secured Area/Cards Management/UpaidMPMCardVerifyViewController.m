//
//  UpaidMPMCardVerifyViewController.m
//  EuroFlorist
//
//  Created by Michał Majewski on 03.03.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidMPMCardVerifyViewController.h"
#import "UpaidMPMBasicColorLabel.h"
#import "UpaidMPMTextField.h"
#import "UpaidApiCardVerify3DSTask.h"
#import "UpaidMPM.h"

@interface UpaidMPMCardVerifyViewController () <UpaidApiCardVerify3DSDelegate, UITextFieldDelegate, UpaidMPMToolsDelegate>

@property (nonatomic) UpaidApiCard *card;
@property (nonatomic) NSString *cvc;
@property (nonatomic) NSString *pares;
@property (nonatomic) BOOL cardsListCalled;
@property (nonatomic) int paymentId;
@property (weak, nonatomic) IBOutlet UpaidMPMTextField *mpinTextField;
@property (weak, nonatomic) IBOutlet UpaidMPMTextField *mpinConfirmTextField;
@property (weak, nonatomic) IBOutlet UpaidMPMBasicColorLabel *cardNameLabel;

enum kAlertTags : NSUInteger{
  kAlertFreezedAmount,
  kAlertBadMpin,
  kAlertBadConfirmMpin,
  kAlertVerifySuccess,
  kAlertCardBlocked
};

@end

@implementation UpaidMPMCardVerifyViewController

- (id)initWithCard: (UpaidApiCard *) card andPaymentId: (int) paymentId andPares: (NSString *) pares andCvc: (NSString *) cvc{
  self = [super initWithNibName: [UpaidMPM paymentInstance].uiConfig.cardVerifyViewControllerNibName bundle:nil];
  
  if (self) {
      self.card = card;
      self.cardsListCalled = NO;
      self.pares = pares;
      self.paymentId = paymentId;
      self.cvc = cvc;
  }
  
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
    self.cardNameLabel.text = self.card.nickname;
  [self createRightBarButtonWithTitle: UpaidMPMLocalizedString(Texts_Next)];
  [self.mpinTextField setHintMessage: UpaidMPMLocalizedString(Texts_CardVerifyMpinHint)];
}

#pragma mark buttons

- (void) rightBarButtonClicked:(UIBarButtonItem *)sender {
  if ([self.mpinTextField.text length] < 4) {
    [self showAlertWithMessage: UpaidMPMLocalizedString(Texts_CardVerifyMpinError) andTag:kAlertBadMpin];
  } else if (![self.mpinTextField.text isEqualToString: self.mpinConfirmTextField.text]) {
    [self showAlertWithMessage: UpaidMPMLocalizedString(Texts_CardVerifyConfirmMpinError) andTag:kAlertBadConfirmMpin];
  } else {    
    [self showHUD];
      UpaidApiCardVerify3DSTask *task = [[UpaidApiCardVerify3DSTask alloc] initWithData:[[UpaidApiCardVerify3DSData alloc] initWithCardId:self.card.iid andPaymentId: self.paymentId andCvc: self.cvc andMpin: self.mpinTextField.text andPares:self.pares] andDelegate:self];
    [task execute];
  }
}

#pragma mark UpaidApiCardVerify3DSDelegate implementation

- (void) onUpaidApiCardVerify3DSSuccess: (UpaidApiCardVerify3DSResult *) result {
    self.card.authorized = YES;
    
    [self showAlertWithMessage: UpaidMPMLocalizedString(Texts_CardVerifySuccessAlert) andTag:kAlertVerifySuccess];
}

- (void) onUpaidApiCardVerify3DSFail: (UpaidApiCardVerify3DSResult *) result {
    [self dismissHUD];
    [self onUpaidApiFail: result];
}

#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
  switch (alertView.tag) {
    case kAlertCardBlocked:
    case kAlertVerifySuccess:
      self.cardsListCalled = YES;
      [UpaidMPMTools startCardsListWithHUD: NO];
      break;
    case kAlertBadMpin:
      [self.scrollView setContentOffset: CGPointMake(0, self.mpinTextField.frame.origin.y)];
      [self.mpinTextField becomeFirstResponder];
      break;
    case kAlertBadConfirmMpin:
      [self.scrollView setContentOffset: CGPointMake(0, self.mpinConfirmTextField.frame.origin.y)];
      [self.mpinConfirmTextField becomeFirstResponder];
      break;
  }
}

#pragma mark UpaidMPMToolsDelegate

- (void) cardsListChanged {
    [self processCardsList];
}

- (void) cardsListError: (UpaidApiCardsListResult *) result {
    [self processCardsList];
}

- (void) processCardsList {
    if (self.cardsListCalled) {
        [self dismissHUD];
        
        NSMutableArray *viewControllers = [NSMutableArray arrayWithArray: self.navigationController.viewControllers];
        
        [viewControllers removeLastObject];
        [viewControllers removeLastObject];
        [self.navigationController setViewControllers:viewControllers animated:YES];
    }
}

#pragma mark UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (range.length == 0) {
      if ((textField == self.mpinTextField || textField == self.mpinConfirmTextField) && [textField.text length] == 4) {
        return NO;
      }
    }
    
    return YES;
}

@end
