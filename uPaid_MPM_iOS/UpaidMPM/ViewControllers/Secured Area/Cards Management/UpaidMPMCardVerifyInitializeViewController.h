//
//  UpaidMPMCardInitializeViewController.h
//  MpmTestApp
//
//  Created by Michał Majewski on 15.10.2014.
//  Copyright (c) 2014 uPaid. All rights reserved.
//

#import "UpaidMPMBaseViewController.h"

@interface UpaidMPMCardVerifyInitializeViewController : UpaidMPMBaseViewController

- (id)initWithCard: (UpaidApiCard *) card andPaymentId: (int) paymentId andPageContent: (NSString *) pageContent andCvc: (NSString *) cvc;

@end
