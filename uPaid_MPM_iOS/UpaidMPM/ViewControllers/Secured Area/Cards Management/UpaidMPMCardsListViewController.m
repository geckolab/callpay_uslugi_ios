//
//  UpaidMPMCardsListViewController.m
//  EuroFlorist
//
//  Created by Michał Majewski on 03.03.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidMPMCardsListViewController.h"
#import "UpaidMPMBasicColorLabel.h"
#import "UpaidMPM.h"
#import "UpaidMPMCardAddViewController.h"
#import "UpaidMPMCardsListTableView.h"
#import "UpaidMPMMasterPassSelectorViewController.h"

@interface UpaidMPMCardsListViewController () <UpaidMPMToolsDelegate>

@property (weak, nonatomic) IBOutlet UpaidMPMCardsListTableView *cardsListTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *noCardsContainerHeightConstraint;

@end

@implementation UpaidMPMCardsListViewController

- (instancetype)init {
    self = [super initWithNibName:[UpaidMPM paymentInstance].uiConfig.cardsListViewControllerNibName  bundle:nil];

    return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  [self createRightBarButtonWithTitle: UpaidMPMLocalizedString(Texts_AddCardButton)];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
    [UpaidMPMTools startCardsListWithHUD: NO];
    [self cardsListChanged];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(masterpassSelector:) name:NOTIFICATION_MASTERPASS_SELECTOR object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectCard:) name:NOTIFICATION_SELECT_CARD object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(verifyCard:) name:NOTIFICATION_VERIFY_CARD object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear: animated];
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}

#pragma mark buttons

- (void)rightBarButtonClicked:(UIBarButtonItem *)sender {
    [self addCard];
}

#pragma mark notifications handlers

- (void) verifyCard: (NSNotification *) notification {
    UpaidMPMCardAddViewController *viewController = [[UpaidMPMCardAddViewController alloc] init];
    viewController.card = notification.userInfo[@"card"];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void) masterpassSelector: (NSNotification *) notification {
    [self.navigationController pushViewController:[[UpaidMPMMasterPassSelectorViewController alloc]  init] animated:YES];
}

- (void) selectCard: (NSNotification *) notification{
    [UpaidMPM paymentInstance].card = notification.userInfo[@"card"];
    [self.navigationController popViewControllerAnimated: YES];
}

#pragma mark methods available from tableView 

- (void) addCard {
  [self.navigationController pushViewController:[[UpaidMPMCardAddViewController alloc] init] animated:YES];
}

#pragma mark UpaidMPMToolsDelegate

- (void) cardsListChanged {
  [self.cardsListTableView refreshList];
    
    if (self.cardsListTableView.cards.count > 0) {
        self.noCardsContainerHeightConstraint.constant = 0;
    }
}

- (void) cardsListError: (UpaidApiCardsListResult *) result {
  [self onUpaidApiFail:result];
}

#pragma mark UIAlertViewDelegate

@end
