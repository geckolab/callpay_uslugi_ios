//
//  UpaidMPMCardAddViewController.m
//  EuroFlorist
//
//  Created by Michał Majewski on 03.03.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidMPMCardAddViewController.h"
#import "UpaidMPMTextField.h"
#import "UpaidApiCardAddTask.h"
#import "UpaidMPM.h"
#import "UpaidMPMPayViewController.h"
#import "UpaidApiInitialize3DSTask.h"
#import "UpaidMPMCardVerifyViewController.h"
#import "UpaidMPMCardVerifyInitializeViewController.h"
#import "UpaidMPMAccentColorLabel.h"

@interface UpaidMPMCardAddViewController () <UITextFieldDelegate, UpaidApiCardAddDelegate, UpaidApiInitialize3DSDelegate>

@property (weak, nonatomic) IBOutlet UpaidMPMTextField *cardNumberTextField;
@property (weak, nonatomic) IBOutlet UpaidMPMTextField *cardDateTextField;
@property (weak, nonatomic) IBOutlet UpaidMPMTextField *cardNicknameTextField;
@property (weak, nonatomic) IBOutlet UpaidMPMTextField *cardCvcTextField;
@property (nonatomic) UpaidApiInitialize3DSResult *result;
@property (weak, nonatomic) IBOutlet UpaidMPMAccentColorLabel *titleLabel;

enum kAlertTags : NSUInteger{
    kAlertBadPan,
    kAlertBadCvc,
    kAlertBadExpirationDate,
    kAlertGoToVerify
};

@end

@implementation UpaidMPMCardAddViewController

- (id)init{
    self = [super initWithNibName: [UpaidMPM paymentInstance].uiConfig.cardAddViewControllerNibName bundle:nil];
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createRightBarButtonWithTitle: UpaidMPMLocalizedString(Texts_Next)];
    
    [self.cardNumberTextField setHintMessage: UpaidMPMLocalizedString(Texts_CardAddCardNumberHint)];
    [self.cardDateTextField setHintMessage: UpaidMPMLocalizedString(Texts_CardAddExpirationDateHint)];
    [self.cardNicknameTextField setHintMessage: UpaidMPMLocalizedString(Texts_CardAddNicknameHint)];
    [self.cardCvcTextField setHintMessage: UpaidMPMLocalizedString(Texts_CardCvcCvvHint)];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
    
    if (self.card != nil) {
        self.titleLabel.text = UpaidMPMLocalizedString(Texts_CardEditTitleLabel);
        self.cardNumberTextField.text = self.card.pan;
        [self.cardNumberTextField setEnabled: NO];
        self.cardDateTextField.text = self.card.expDate;
        [self.cardDateTextField setEnabled: NO];
        self.cardNicknameTextField.text = self.card.nickname;
        [self.cardNicknameTextField setEnabled: NO];
        self.cardCvcTextField.text = @"";
//        [self.cardCvcTextField becomeFirstResponder];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear: animated];
  [self.view endEditing: YES];
}

#pragma mark buttons

- (void) rightBarButtonClicked:(UIBarButtonItem *)sender {
    [self.view endEditing: YES];
    
    if ([self.cardCvcTextField.text length] < 3) {
        return [self showAlertWithMessage: UpaidMPMLocalizedString(Texts_CardAddCvcCvvError) andTag:kAlertBadCvc];
    }
    
    if (self.card != nil) {
        [self showHUD];
      
        return [self startInitialize3DS];
    }
    
    if ([self.cardNumberTextField.text length] < 15) {
        [self showAlertWithMessage: UpaidMPMLocalizedString(Texts_CardAddCardNumberError) andTag:kAlertBadPan];
    } else if ([self.cardDateTextField.text length] < 5) {
        [self showAlertWithMessage: UpaidMPMLocalizedString(Texts_CardAddExpirationDateError) andTag:kAlertBadExpirationDate];
    } else {
        NSArray *cardDateComponents = [self.cardDateTextField.text componentsSeparatedByString: @"/"];
        NSString *cardNumber = [self.cardNumberTextField.text stringByReplacingOccurrencesOfString:@"-" withString:@""];
        int expMonth = [cardDateComponents[0] intValue];
        int expYear = [cardDateComponents[1] intValue];
        
        [self showHUD];
        UpaidApiCardAddTask *task = [[UpaidApiCardAddTask alloc] initWithData:[[UpaidApiCardAddData alloc] initWithPan:cardNumber andExpMonth: expMonth andExpYear:expYear andNickname:self.cardNicknameTextField.text] andDelegate:self];
        [task execute];
    }
}

- (void) startInitialize3DS {
    [[[UpaidApiInitialize3DSTask alloc] initWithData: [[UpaidApiInitialize3DSData alloc ] initWithCardId: self.card.iid] andDelegate:self] execute];
}

- (void) goToVerify {
    if (self.result.pageContent != nil) {
        [self.navigationController pushViewController: [[UpaidMPMCardVerifyInitializeViewController alloc] initWithCard: self.card andPaymentId: self.result.paymentId andPageContent: self.result.pageContent andCvc: self.cardCvcTextField.text] animated: YES];
    } else {
        [self.navigationController pushViewController: [[UpaidMPMCardVerifyViewController alloc] initWithCard: self.card andPaymentId: self.result.paymentId andPares: nil andCvc: self.cardCvcTextField.text] animated: YES];
    }
}

#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case kAlertBadPan:
            [self.cardNumberTextField becomeFirstResponder];
            break;
        case kAlertBadExpirationDate:
            [self.cardDateTextField becomeFirstResponder];
            break;
        case kAlertGoToVerify:
            [self goToVerify];
            break;
    }
}

#pragma mark UpaidApiCardAddDelegate implementation

- (void) onUpaidApiCardAddSuccess: (UpaidApiCardAddResult *) result {
    self.card = result.card;
    [[UpaidMPMTools sharedInstance].cards addObject:result.card];
    [UpaidMPMTools startCardsListWithHUD: NO];
    [self startInitialize3DS];
}

- (void) onUpaidApiCardAddFail: (UpaidApiCardAddResult *) result {
    [self dismissHUD];
    [self onUpaidApiFail: result];
}

#pragma mark UpaidApiInitialize3DSDelegate implementation

- (void) onUpaidApiInitialize3DSSuccess: (UpaidApiInitialize3DSResult *) result {
    [self dismissHUD];
    
    self.result = result;
    [self goToVerify];
//    [self showAlertWithMessage: UpaidMPMLocalizedString(Texts_CardAddGoToVerification) andTag:kAlertGoToVerify];
}

- (void) onUpaidApiInitialize3DSFail: (UpaidApiInitialize3DSResult *) result {
    [self dismissHUD];
    [self onUpaidApiFail:result];
}

#pragma mark UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == self.cardCvcTextField) {
        if (range.length == 0 && [textField.text length] >= 3) {
            return NO;
        }
    }
    
    if (textField == self.cardNumberTextField) {
        if (range.length == 0) {
            if ([textField.text length] == 19) {
                return NO;
            }
            
            if ((range.location + 1) % 5 == 0) {
                textField.text = [textField.text stringByAppendingFormat:@"-%@", string];
                
                return NO;
            }
        } else if (range.length == 1) { //delete button
            if (range.location > 0 && range.location % 5 == 0) {
                range.location--;
                textField.text = [textField.text stringByReplacingCharactersInRange:range withString:@""];
            }
        }
    } else if (textField == self.cardDateTextField) {
        if (range.length == 1) { //delete button
            if (range.location == 3) {
                range.location--;
                textField.text = [textField.text stringByReplacingCharactersInRange:range withString:@""];
            }
        } else if (range.length == 0) {
            if ([textField.text length] == 5) {
                return NO;
            }
            
            if (range.location == 0 && [string intValue] > 1) {
                textField.text = [@"0" stringByAppendingString:string];
                
                return NO;
            }
            
            if (range.location == 1) {
                if ([textField.text intValue] == 1 && [string intValue] > 2) {
                    textField.text = @"12";
                    
                    return NO;
                } else if ([textField.text intValue] == 0 && [string intValue] == 0) {
                    textField.text = @"01";
                    
                    return NO;
                }
                
            }
            
            if (range.location == 2) {
                textField.text = [textField.text stringByAppendingFormat:@"/%@", string];
                
                return NO;
            }
        }
    }
    
    return YES;
}

#pragma mark test action

-(void) testActionFired {
    NSString *url = @"http://upaid.yum.pl/dajpankarte.php?type=mc_pl";
    self.cardNumberTextField.text = [NSString stringWithContentsOfURL:[NSURL URLWithString:url] encoding:NSUTF8StringEncoding error:nil];
    self.cardDateTextField.text = @"12/22";
    self.cardCvcTextField.text = @"123";
    [self.cardNicknameTextField becomeFirstResponder];
}

@end
