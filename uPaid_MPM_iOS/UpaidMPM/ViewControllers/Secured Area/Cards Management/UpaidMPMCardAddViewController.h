//
//  UpaidMPMCardAddViewController.h
//  EuroFlorist
//
//  Created by Michał Majewski on 03.03.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidMPMBaseViewController.h"
@class UpaidApiCard;

@interface UpaidMPMCardAddViewController : UpaidMPMBaseViewController

@property (nonatomic) UpaidApiCard *card;

@end
