//
//  UpaidMPMTools.h
//  EuroFlorist
//
//  Created by Michał Majewski on 07.03.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidMPMBaseViewController.h"
#import "UpaidApiCardArray.h"
#import "UpaidMPMToolsDelegate.h"
#import "UpaidApiBuyTransactionData.h"

@interface UpaidMPMTools : NSObject

@property (nonatomic) UpaidMPMBaseViewController *activeViewController;
@property (nonatomic) UpaidApiCardArray *cards;

+ (UpaidMPMTools *)sharedInstance;
+ (void) startCardsListWithHUD: (BOOL) showHUD;
+ (UpaidApiBuyTransactionData *) transactionDataWithCVC2: (NSString *) cvc2;
+ (void) logout;

@end
