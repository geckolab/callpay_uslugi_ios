//
//  UpaidMPMToolsDelegate.h
//  EuroFlorist
//
//  Created by Michał Majewski on 07.03.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiCardsListResult.h"

@protocol UpaidMPMToolsDelegate <NSObject>

@optional
- (void) cardsListChanged;
- (void) cardsListError: (UpaidApiCardsListResult *) result;

@end
