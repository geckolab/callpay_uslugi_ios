//
//  UpaidMPMTools.m
//  EuroFlorist
//
//  Created by Michał Majewski on 07.03.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidMPMTools.h"
#import "UpaidApiCardsListTask.h"
#import "UpaidMPM.h"

@interface UpaidMPM ()

- (NSString *) merchant;
- (NSInteger) amount;
- (NSString *) itemNo;
- (NSString *) additionalData;

@end

@interface UpaidMPMTools () <UpaidApiCardsListDelegate>

@property (nonatomic) BOOL showHUD;
@property (nonatomic) BOOL cardsListIsLoading;

@end

@implementation UpaidMPMTools

- (id)init {
  self = [super init];
  
  if (self) {
    self.cardsListIsLoading = NO;
  }
  
  return self;
}

+ (UpaidMPMTools *)sharedInstance {
  static UpaidMPMTools *sharedInstance = nil;
  static dispatch_once_t onceToken;
  
  dispatch_once(&onceToken, ^{
    sharedInstance = [[UpaidMPMTools alloc] init];
  });
  
  return sharedInstance;
}

+ (void) logout {
  [UpaidMPMTools sharedInstance].cards = nil;
  [UpaidMPM paymentInstance].card = nil;
  [UpaidApi logout];
  [UpaidMPM setLoggedIn: NO];
}

+ (UpaidApiBuyTransactionData *) transactionDataWithCVC2: (NSString *) cvc2 {
  return [[UpaidApiBuyTransactionData alloc] initWithAmount:(int)[UpaidMPM paymentInstance].amount andItemId: [UpaidMPM paymentInstance].itemNo andMerchant:[UpaidMPM paymentInstance].merchant andData: [UpaidMPM paymentInstance].additionalData andCard:[UpaidMPM paymentInstance].card.iid andCvc2: cvc2];
}

#pragma mark cards list

+ (void) startCardsListWithHUD: (BOOL) showHUD {
  UpaidMPMTools *instance = [UpaidMPMTools sharedInstance];
  instance.showHUD = showHUD;
  
  if (!instance.cardsListIsLoading) {
    instance.cardsListIsLoading = YES;
    UpaidApiCardsListTask *task = [[UpaidApiCardsListTask alloc] initWithData:[[UpaidApiCardsListData alloc] init] andDelegate:instance];
    [task execute];
  }
  
  if(showHUD) {
    [instance.activeViewController showHUD];
  }
}

- (BOOL) activeViewControllerConformsProtocol {
  return self.activeViewController != nil && [self.activeViewController conformsToProtocol: @protocol(UpaidMPMToolsDelegate)];
}

#pragma mark UpaidApiCardsListDelegate implementation

- (void) onUpaidApiCardsListSuccess: (UpaidApiCardsListResult *) result {
  self.cardsListIsLoading = NO;
  
  if (self.showHUD) {
    [self.activeViewController dismissHUD];
  }
  
  self.cards = result.cards;
  
  if ([self activeViewControllerConformsProtocol] && [self.activeViewController respondsToSelector:@selector(cardsListChanged)]) {
    [self.activeViewController performSelector:@selector(cardsListChanged)];
  }
}

- (void) onUpaidApiCardsListFail: (UpaidApiCardsListResult *) result {
  self.cardsListIsLoading = NO;
  
  if (self.showHUD) {
    [self.activeViewController dismissHUD];
  }
    
    self.cards = [UpaidApiCardArray new];
  
  if ([self activeViewControllerConformsProtocol] && [self.activeViewController respondsToSelector:@selector(cardsListError:)]) {
    [self.activeViewController performSelector:@selector(cardsListError:) withObject:result];
  }
}

- (void) onUpaidApiCardsListNoCards: (UpaidApiCardsListResult *) result {
  [self onUpaidApiCardsListSuccess:result];
}

@end
