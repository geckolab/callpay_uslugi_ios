//
//  UpaidMPMViewsConfig.h
//  mPotwor
//
//  Created by Michał Majewski on 26.04.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpaidMPMUIConfig : NSObject

@property (nonatomic) NSString *loginViewControllerNibName;
@property (nonatomic) NSString *preRegisterViewControllerNibName;
@property (nonatomic) NSString *registerViewControllerNibName;
@property (nonatomic) NSString *payViewControllerNibName;
@property (nonatomic) NSString *cardAddViewControllerNibName;
@property (nonatomic) NSString *cardVerifyInitializeViewControllerNibName;
@property (nonatomic) NSString *cardVerifyViewControllerNibName;
@property (nonatomic) NSString *cardsListViewControllerNibName;
@property (nonatomic) NSString *masterpassSelectorViewControllerNibName;
@property (nonatomic) NSString *masterpassSelectorCellNibName;
@property (nonatomic) NSString *cardsListCellNibName;
@property (nonatomic) NSString *productsListCellNibName;
@property (nonatomic) NSString *footerNibName;

@property (nonatomic) UIColor *backgroundColor;
@property (nonatomic) UIView *titleView;
@property (nonatomic) UIColor *basicColor;
@property (nonatomic) UIColor *accentColor;
@property (nonatomic) UIColor *buttonTitleColor;
@property (nonatomic) UIColor *buttonBackgroundColor;
@property (nonatomic) UIColor *buttonBorderColor;
@property (nonatomic) UIColor *textFieldPlaceholderColor;
@property (nonatomic) UIColor *textFieldColor;
@property (nonatomic) UIColor *textFieldBackgroundColor;
@property (nonatomic) UIImage *textFieldBackgroundImage;
@property (nonatomic) UIEdgeInsets textFieldEdgeInsets;
@property (nonatomic) UIActivityIndicatorView *indicatorView;
@end
