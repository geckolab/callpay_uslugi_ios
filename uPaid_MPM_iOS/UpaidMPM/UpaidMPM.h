//
//  UpaidMPMConfig.h
//  EuroFlorist
//
//  Created by Michał Majewski on 27.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidMPMConst.h"
#import "UpaidMPMDelegate.h"
#import "UpaidApiCard.h"
#import "UpaidMPMBaseViewController.h"
#import "UpaidMPMTools.h"
#import "UpaidMPMUIConfig.h"
#import "UpaidMPMProduct.h"
#import "UpaidMPMTexts.h"

@interface UpaidMPM : NSObject

typedef NS_OPTIONS(NSUInteger, UpaidMPMAcquirer) {
  UpaidMPMAcquirerPayU = 1,
  UpaidMPMAcquirerECard,
  UpaidMPMAcquirerFirstData,
  UpaidMPMAcquirerPeP
};

@property (nonatomic) UpaidApiCard *card;
@property (nonatomic) BOOL logsEnabled;
@property (nonatomic) id<UpaidMPMDelegate> delegate;
@property (nonatomic) UpaidMPMAcquirer acquirer;
@property (nonatomic) NSInteger amount;
@property (nonatomic) UpaidMPMUIConfig *uiConfig;
@property (nonatomic) NSArray *products;
@property (nonatomic) BOOL showExitWarning;
@property (nonatomic) NSString *initialPhone;
@property (nonatomic) NSString *appName;

+ (id<UpaidMPMDelegate>)delegate;
+ (BOOL) loggedIn;
+ (void) setLoggedIn: (BOOL) loggedIn;
+ (UpaidMPM *)sharedInstance;
- (NSArray *) viewControllersForPayment;
- (void) startPaymentForNavigationController: (UINavigationController *) navigationController;

/**
 *  Methods sets static config values for payment in UpaidMPM
 *
 *  @param partnerId partnerId, determined during the integration
 *  @param appName   custom app name, using to resolve problems with payments etc.
 *  @param merchant  merchant name, determined during the integration
 */
- (void) setPartnerId: (int) partnerId andAppName: (NSString *) appName andMerchant: (NSString *) merchant;

/**
 *  Methods sets dynamic config values for payment in UpaidMPM
 *
 *  @param amount         amount in pennies
 *  @param itemNo         order number etc
 *  @param additionalData custom additionalData - can be nil or empty string
 */
- (void) setAmount: (NSInteger) amount andItemNo: (NSString *) itemNo andAdditionalData: (NSString *) additionalData;

/**
 *  Methods sets test environment to payments
 *
 *  @param testPartnerId test partnerId
 */
- (void) setTestPartnerId: (int) testPartnerId;

//payment config instance
+ (void) setPaymentInstance: (UpaidMPM *) instance;
+ (UpaidMPM *) paymentInstance;

@end
