//
//  UpaidMPMViewsConfig.m
//  mPotwor
//
//  Created by Michał Majewski on 26.04.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidMPMUIConfig.h"
#import "UpaidMPMConst.h"

@implementation UpaidMPMUIConfig

- (id)init
{
    self = [super init];
    if (self) {
      self.loginViewControllerNibName = @"UpaidMPMLoginViewController";
      self.registerViewControllerNibName = @"UpaidMPMRegisterViewController";
        
      self.preRegisterViewControllerNibName = @"UpaidMPMPreRegisterViewController";
      self.payViewControllerNibName = @"UpaidMPMPayViewController";
      self.cardAddViewControllerNibName = @"UpaidMPMCardAddViewController";
      self.cardVerifyInitializeViewControllerNibName = @"UpaidMPMCardVerifyInitializeViewController";
      self.cardsListViewControllerNibName = @"UpaidMPMCardsListViewController";
      self.cardVerifyViewControllerNibName = @"UpaidMPMCardVerifyViewController";
      self.masterpassSelectorViewControllerNibName = @"UpaidMPMMasterPassSelectorViewController";
      self.masterpassSelectorCellNibName = @"UpaidMPMSelectMasterPassTableViewCell";
      self.cardsListCellNibName = @"UpaidMPMCardsListCell";
      self.productsListCellNibName = @"UpaidMPMProductsListCell";
      self.footerNibName = @"UpaidMPMFooter";
      self.backgroundColor = [UIColor whiteColor];
        UIImage *titleImage = [UIImage imageNamed: @"mpm_masterpass_logo"];
        UIButton *button = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, titleImage.size.width, titleImage.size.height)];
        [button setImage:titleImage forState:UIControlStateNormal];
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(titleButtonLongPress:)];
        [button addGestureRecognizer:longPress];
        self.titleView = button;
        
//        [[UIImageView alloc] initWithImage:[UIImage imageNamed: @"mpm_masterpass_logo"]];
      
      self.basicColor =[UIColor blackColor];
        self.accentColor = [UIColor colorWithRed:0.8 green:0.0 blue:0.0039 alpha:1]; /*#cc0001*/
//      self.accentColor = [UIColor colorWithRed:0.878 green:0.094 blue:0.161 alpha:1]; /*#e01829*/
      self.buttonTitleColor = nil;
      self.buttonBackgroundColor = nil;
      self.buttonBorderColor = nil;
      self.textFieldPlaceholderColor = [UIColor colorWithRed:0.737 green:0.737 blue:0.737 alpha:1] /*#bcbcbc*/;
      self.textFieldColor = [UIColor blackColor];
      self.textFieldBackgroundColor = nil;
      self.textFieldBackgroundImage = [UIImage imageNamed:@"mpm_textfield_background"];
      self.textFieldEdgeInsets = UIEdgeInsetsMake(0, 1, 5, 0);
      
      self.indicatorView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
      [self.indicatorView setActivityIndicatorViewStyle: UIActivityIndicatorViewStyleGray];
    }
    return self;
}

- (void) titleButtonLongPress: (id) sender {
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_TEST_ACTION_FIRED object:nil];
}

//- (UIView *)titleView {
//  NSData *archivedViewData = [NSKeyedArchiver archivedDataWithRootObject: _titleView];
//  
//  return (UIView *) [NSKeyedUnarchiver unarchiveObjectWithData:archivedViewData];
//}

@end
