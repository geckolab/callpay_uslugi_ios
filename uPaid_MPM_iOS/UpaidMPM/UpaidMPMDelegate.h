//
//  UpaidMPMDelegate.h
//  EuroFlorist
//
//  Created by Michał Majewski on 28.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiCard.h"

@protocol UpaidMPMDelegate <NSObject>

- (void) afterSuccessfullPaymentWithNavigationController: (UINavigationController *) navigationController andId: (int) paymentId andAdditionalData: (NSString *) resultAdditionalData;
- (void) afterUnsuccessfullPaymentWithNavigationController: (UINavigationController *) navigationController;

@optional
- (void) onUpaidMPMError: (NSString *) errorMessage;

- (BOOL) beforePaymentWithCard: (UpaidApiCard *) card;

@end
