//
//  UpaidMPMConst.h
//  EuroFlorist
//
//  Created by Michał Majewski on 27.02.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#define UPAID_MPM_LOCALIZABLE_TABLE @"UpaidMPMLocalizable"
#define UpaidMPMLocalizedString(key) NSLocalizedStringFromTable(key, UPAID_MPM_LOCALIZABLE_TABLE, nil)

#define UpaidMPMLog(...) [[UpaidMPM paymentInstance] logsEnabled] ? NSLog(__VA_ARGS__) : nil

#define NOTIFICATION_MASTERPASS_SELECTOR @"NOTIFICATION_MASTERPASS_SELECTOR"
#define NOTIFICATION_VERIFY_CARD @"NOTIFICATION_VERIFY_CARD"
#define NOTIFICATION_SELECT_CARD @"NOTIFICATION_SELECT_CARD"

#define NOTIFICATION_TEST_ACTION_FIRED @"NOTIFICATION_TEST_ACTION_FIRED"

#define UPAID_MPM_PHOTOS_TEST_URL  @"http://beta.upaid.pl/filesManager"
#define UPAID_MPM_PHOTOS_PROD_URL  @"https://upaid.pl/filesManager"