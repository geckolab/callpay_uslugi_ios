//
//  Card+Images.h
//  uPaidWallet
//
//  Created by Paweł Grzmil on 28.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o. All rights reserved.
//

#import "UpaidApiCard.h"

@interface UpaidApiCard (Extensions)
- (UIImage *)authorizedIcon;
- (NSURL *)frontImageURL;
- (NSURL *)backImageURL;
- (void) setupMasterpassCardWithId: (NSString *) cardId;
- (BOOL) isMasterPassCard;
- (BOOL) hasMpin;

@end
