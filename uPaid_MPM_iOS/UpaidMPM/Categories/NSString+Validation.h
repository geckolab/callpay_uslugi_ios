//
//  NSString+Validation.h
//  EuroFlorist
//
//  Created by Michał Majewski on 05.03.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Validation)

- (BOOL) isEmail;
- (BOOL) isValidPassword;

@end
