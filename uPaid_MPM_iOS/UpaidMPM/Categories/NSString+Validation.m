//
//  NSString+Validation.m
//  EuroFlorist
//
//  Created by Michał Majewski on 05.03.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "NSString+Validation.h"

@implementation NSString (Validation)

- (BOOL)isEmail {
  NSString *regex = @"^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-+]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z‌​]{2,4})$";
  NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
  
  return [regExPredicate evaluateWithObject:self];
}

- (BOOL) isValidPassword {
    NSString *regex = @"^((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,})$";
    NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    return [regExPredicate evaluateWithObject:self];
}

@end
