//
//  Card+Images.m
//  uPaidWallet
//
//  Created by Paweł Grzmil on 28.12.2013.
//  Copyright (c) 2013 uPaid Sp. z o.o. All rights reserved.
//

#import "UpaidApiCard+Extensions.h"
#import "UpaidApi+Dev.h"
#import "UpaidMPM.h"

@implementation UpaidApiCard (Extensions)


static NSString *MASTERPASS_CARD = @"MASTERPASS_CARD";

- (UIImage *)authorizedIcon {
	return self.authorized ? [UIImage imageNamed:@"karta_zweryfikowana"] : [UIImage imageNamed:@"karta_niezweryfikowana"];
}

- (NSString *) imageUrlPrefix {
    return [UpaidApi isTestEnv] ? UPAID_MPM_PHOTOS_TEST_URL : UPAID_MPM_PHOTOS_PROD_URL;
}

- (NSURL *) imageUrlWithSessionForName: (NSString *) name {
    return [NSURL URLWithString: [NSString stringWithFormat:@"%@/view/%@/%@", [self imageUrlPrefix], [UpaidApi sessionToken], name]];
}

- (NSURL *) imageUrlForType: (NSString *) type {
    return [NSURL URLWithString: [NSString stringWithFormat:@"%@/viewVisualByType/%@?width=300", [self imageUrlPrefix], type]];
}

- (NSURL *)frontImageURL {
    if ([self isMasterPassCard]) {
        return [self imageUrlForType: @"masterpass"];
    } else if (self.frontImg != nil && ![self.frontImg isEqualToString: @""]) {
        return [self imageUrlWithSessionForName: self.frontImg];
    } else {
        return [self imageUrlForType: self.cardType];
    }
}

- (NSURL *)backImageURL {
	return [self imageUrlWithSessionForName: self.backImg];
}

- (void) setupMasterpassCardWithId: (NSString *) cardId {
    self.iid = cardId;
    self.nickname = @"Karta MasterPass";
    self.smName = MASTERPASS_CARD;
}

- (BOOL) isMasterPassCard {
    return [self.smName isEqualToString: MASTERPASS_CARD];
}

- (BOOL) hasMpin {
    return [self.authType rangeOfString:@"mpin"].location != NSNotFound;
}

@end
