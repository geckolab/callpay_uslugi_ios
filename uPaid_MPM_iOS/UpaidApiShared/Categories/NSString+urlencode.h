//
//  NSString+urlencode.h
//  mPotwor
//
//  Created by Michał Majewski on 27.12.2013.
//  Copyright (c) 2013 Michał Majewski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (urlencode)

- (NSString *)urlencode;

@end
