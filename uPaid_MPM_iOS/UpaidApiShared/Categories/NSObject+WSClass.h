//
//  NSObject+WSClass.h
//  mPotwor
//
//  Created by Michał Majewski on 03.01.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (WSClass)

- (id)wsClass;

- (id)initWithWsClass:(id)wsClass;

@end
