//
//  NSString+urlencode.m
//  mPotwor
//
//  Created by Michał Majewski on 27.12.2013.
//  Copyright (c) 2013 Michał Majewski. All rights reserved.
//

#import "NSString+urlencode.h"

@implementation NSString (urlencode)

- (NSString *)urlencode {
	CFStringRef safeString = CFURLCreateStringByAddingPercentEscapes(
	        NULL,
	        (CFStringRef)self,
	        NULL,
	        CFSTR("/%&=?$#+-~@<>|\\*,.()[]{}^!"),
	        kCFStringEncodingUTF8
	        );

	NSString *stringToReturn = (__bridge NSString *)safeString;
	CFRelease(safeString);

	return stringToReturn;
}

@end
