//
//  UpaidApiResult.h
//  mPotwor
//
//  Created by Michał Majewski on 16.12.2013.
//  Copyright (c) 2013 Michał Majewski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpaidApiResult : NSObject

@property (nonatomic) NSException *exception;
@property (nonatomic) NSInteger status;
@property (nonatomic) NSString *statusDescription;

@end
