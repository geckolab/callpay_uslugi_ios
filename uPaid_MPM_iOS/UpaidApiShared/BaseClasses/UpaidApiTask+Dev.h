//
//  UpaidApiTask+Dev.h
//  mPotwor
//
//  Created by Michał Majewski on 27.12.2013.
//  Copyright (c) 2013 Michał Majewski. All rights reserved.
//

#import "UpaidApiTask.h"

@interface UpaidApiTask (Dev)

- (void)logUnusedCallback:(NSString *)callback withStatus:(NSString *)status;
- (void)executeFinished;
- (void)sessionRenewFailed;

@end
