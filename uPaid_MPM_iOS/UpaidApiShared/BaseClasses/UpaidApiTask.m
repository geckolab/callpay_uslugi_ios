//
//  UpaidApiTask.m
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 Michał Majewski. All rights reserved.
//

#import "UpaidApiTask.h"
#import "MyWsdlProxy.h"
#import "UpaidApiData.h"
#import "UpaidApiConstans.h"

@interface UpaidApiTask () <Wsdl2CodeProxyDelegate>

@property MyWsdlProxy *proxy;
@property NSString *taskId;

@end

@implementation UpaidApiTask

- (void)execute {
	self.taskId = [UpaidApi pointerIdToTask:self];
}

- (id)init {
	self = [super init];

	if (self) {
		self.proxy = [[MyWsdlProxy alloc] initWithUrl:[UpaidApi wsdlUrl] AndDelegate:self];
	}

	return self;
}

- (void)logUnusedCallback:(NSString *)callback withStatus:(NSString *)status {
	UpaidLog(@"Method: %@, status: %@", callback, status);
}

- (void)proxyRecievedError:(NSException *)ex InMethod:(NSString *)method {
	[self executeFinished];
	UpaidLog(@"exception:%@\nmethod:%@", ex, method);
}

- (void)proxydidFinishLoadingData:(id)data InMethod:(NSString *)method {
	[self executeFinished];
	UpaidLogStack(@"data:%@\nmethod:%@", data, method);
}

- (void)executeFinished {
	[UpaidApi releasePointerToTaskId:self.taskId];
}

@end
