//
//  UpaidApiTask.h
//  UpaidApi
//
//  Created by Michał Majewski on 10.12.2013.
//  Copyright (c) 2013 Michał Majewski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpaidApiTask : NSObject

- (void)execute;

@end
