//
//  UpaidApiSessionExpiredHandler.h
//  mPotwor
//
//  Created by Michał Majewski on 08.01.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpaidApiLoginTask.h"

@interface UpaidApiSessionRenewer : NSObject <UpaidApiLoginDelegate>

- (void)renewSessionForTask:(UpaidApiTask *)task;

@end
