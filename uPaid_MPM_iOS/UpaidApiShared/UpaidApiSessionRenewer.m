//
//  UpaidApiSessionExpiredHandler.m
//  mPotwor
//
//  Created by Michał Majewski on 08.01.2014.
//  Copyright (c) 2014 Michał Majewski. All rights reserved.
//

#import "UpaidApiSessionRenewer.h"
#import "UpaidApi.h"
#import "UpaidApi+Dev.h"
#import "UpaidApiTask+Dev.h"
#import "UpaidApiConstans.h"

@interface UpaidApiSessionRenewer ()

@property UpaidApiTask *task;
@property int attemptsCount;

@end

@implementation UpaidApiSessionRenewer

- (id)init {
	self = [super init];

	if (self) {
		self.attemptsCount = 0;
	}

	return self;
}

- (void)renewSessionForTask:(UpaidApiTask *)task {
	if (self.attemptsCount == 0) {
		UpaidLog(@"Session expired. Attempt to login again.");
		self.task = task;
		[[[UpaidApiLoginTask alloc] initWithData:[[UpaidApiLoginData alloc] initWithPhone:[UpaidApi lastUsedPhone] andCode:[UpaidApi smsCodeForPhone:[UpaidApi lastUsedPhone]]] andDelegate:self] execute];
	}
	else {
		[self renewFailed];
	}
}

#pragma mark UpaidApiLoginDelegate implementation

- (void)onUpaidApiLoginSuccess:(UpaidApiLoginResult *)result {
	[self.task execute];
	self.task = nil;
}

- (void)onUpaidApiLoginFail:(UpaidApiLoginResult *)result {
	[self renewFailed];
}

- (void)renewFailed {
	[self.task sessionRenewFailed];
	self.task = nil;
}

@end
