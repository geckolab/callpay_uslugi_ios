//
//  MPMBuyTicketDelegate.h
//  CallPay
//
//  Created by Jacek on 13.03.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "UpaidMPMDelegate.h"

@interface MPMBuyTicketDelegate : NSObject <UpaidMPMDelegate>

- (void) afterSuccessfullPaymentWithNavigationController: (UINavigationController *) navigationController andId: (int) paymentId andAdditionalData: (NSString *) resultAdditionalData;

- (void) afterUnsuccessfullPaymentWithNavigationController: (UINavigationController *) navigationController;

@end
