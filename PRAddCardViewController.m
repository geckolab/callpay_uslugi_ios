//
//  PRAddCardViewController.m
//  pr
//
//  Created by Alexander Pimenov on 21.06.13.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PRAddCardViewController.h"
#import "PRAsyncConnection.h"
#import "XLog.h"
#import "PRUtils.h"
#import "PRVerifyWarningViewController.h"
#import "PRAsyncConnection.h"

static NSString *const LogTag = @"PRAddCardViewController";


@interface PRAddCardViewController () <UITextFieldDelegate, PRVerifyWarningDelegate>

@property (weak, nonatomic) IBOutlet UITextField *firstCardNumberBlock;
@property (weak, nonatomic) IBOutlet UITextField *secondCardNumberBlock;
@property (weak, nonatomic) IBOutlet UITextField *thirdCardNumberBlock;
@property (weak, nonatomic) IBOutlet UITextField *fourthCardNumberBlock;

@property (weak, nonatomic) IBOutlet UITextField *firstDateBlock;
@property (weak, nonatomic) IBOutlet UITextField *secondDateBlock;

@property (weak, nonatomic) IBOutlet UIButton *addCardButton;

@property (nonatomic) BOOL userAcceptedTerms;

@property (strong, nonatomic) NSNumber *amount;

@end


@implementation PRAddCardViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    UIBarButtonItem * mcmLogo = [[UIBarButtonItem alloc] initWithCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mcm_icon"]]];
    self.navigationItem.rightBarButtonItem = mcmLogo;
    
    [self addDoneButtonToNumberPad];

    [self addBackgroundImageToAddCardButton];
}

- (void)addDoneButtonToNumberPad
{
    for (UITextField *textField in [self textFields])
    {
        [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }

    UIToolbar *numberToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:[[UIBarButtonItem alloc]
                                                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                           target:nil
                                                                                           action:nil],
                                                    [[UIBarButtonItem alloc]
                                                                      initWithTitle:@"Gotowe"
                                                                              style:UIBarButtonItemStyleDone
                                                                             target:self
                                                                             action:@selector(doneWithNumberPad)],
                                                    nil];
    [numberToolbar sizeToFit];

    for (UITextField *textField in [self textFields])
    {
        [textField setInputAccessoryView:numberToolbar];
    }
}

- (void)doneWithNumberPad
{
    for (UITextField *textField in [self textFields])
    {
        [textField resignFirstResponder];
    }
}

- (void)addBackgroundImageToAddCardButton
{
    [[self addCardButton] setBackgroundImage:[self resizableImageForHighlightedState:NO] forState:UIControlStateNormal];
    [[self addCardButton]
           setBackgroundImage:[self resizableImageForHighlightedState:YES] forState:UIControlStateHighlighted];
}

- (UIImage *)resizableImageForHighlightedState:(BOOL)isHighlighted
{
    NSString *imageName = isHighlighted ? @"button_blue_pressed.png" : @"button_blue_active.png";
    UIImage *image = [UIImage imageNamed:imageName];
    return [image resizableImageWithCapInsets:UIEdgeInsetsMake(0, 15, 0, 15)];
}

- (void)viewDidUnload
{
    [self setFirstCardNumberBlock:nil];
    [self setSecondCardNumberBlock:nil];
    [self setThirdCardNumberBlock:nil];
    [self setFourthCardNumberBlock:nil];
    [self setFirstDateBlock:nil];
    [self setSecondDateBlock:nil];
    [self setAddCardButton:nil];
    [super viewDidUnload];
}

- (void) buttonOkClickedInViewController:(UIViewController*) controller
{
    [[self delegate] addCardViewControllerAddedNewCard:self];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showVerifyWarning"])
    {
        PRVerifyWarningViewController *destController = segue.destinationViewController;
        [destController setAmount:_amount.intValue];
        [destController setDelegate:self];
    }
}

- (BOOL)            textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
            replacementString:(NSString *)string
{
    if ([string length] == 0)
    {
        return YES;
    }
    else if (([string length] == 1) &&
             ([[textField text] length] < [self maximumLengthForTextField:textField]) &&
             [self stringIsNumeric:string])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (BOOL)stringIsNumeric:(NSString *)string
{
    NSCharacterSet *notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([string rangeOfCharacterFromSet:notDigits].location == NSNotFound)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void) fillTextBlockWithZeroOnBeginning:(UITextField*) textField
{
    if(textField.text.length == 1)
    {
        textField.text = [NSString stringWithFormat:@"0%@", textField.text];
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if([textField isEqual:_secondDateBlock] == YES)
    {
        [self fillTextBlockWithZeroOnBeginning:_firstDateBlock];
    }
}

- (void)textFieldDidChange:(UITextField *)changedTextField
{
    UITextField *textFieldToFocusOn;
    NSUInteger changedTextFieldIndex = [[self textFields] indexOfObject:changedTextField];
    
    BOOL changedFocus = NO;
    
    if([changedTextField isEqual:_firstDateBlock])
    {
        int no = _firstDateBlock.text.intValue;
        if(no>1)
        {
            changedFocus = YES;
        }
    }

    if ([[changedTextField text] length] == [self maximumLengthForTextField:changedTextField] || changedFocus == YES)
    {
        changedTextFieldIndex++;

        if (changedTextFieldIndex < [[self textFields] count])
        {
            textFieldToFocusOn = [[self textFields] objectAtIndex:changedTextFieldIndex];
            [textFieldToFocusOn becomeFirstResponder];
        }
    }

    [self switchAddButtonState];
}

- (NSUInteger)maximumLengthForTextField:(UITextField *)textField
{
    return [[self cardNumberBlocks] containsObject:textField] ? 4 : 2;
}

- (BOOL)allFieldsFilled
{
    for (UITextField *textField in [self textFields])
    {
        NSUInteger textFieldCapacity = [self maximumLengthForTextField:textField];

        if ([[textField text] length] < textFieldCapacity)
        {
            return NO;
        }
    }

    return YES;
}

- (NSArray *)textFields
{
    return @[[self firstCardNumberBlock],
             [self secondCardNumberBlock],
             [self thirdCardNumberBlock],
             [self fourthCardNumberBlock],
             [self firstDateBlock],
             [self secondDateBlock]];
}

- (NSArray *)cardNumberBlocks
{
    return @[[self firstCardNumberBlock],
             [self secondCardNumberBlock],
             [self thirdCardNumberBlock],
             [self fourthCardNumberBlock]];
}

- (IBAction)termsSwitchValueChanged:(UISwitch *)termsSwitch
{
    [self setUserAcceptedTerms:[termsSwitch isOn]];

    [self switchAddButtonState];
}

- (void)switchAddButtonState
{
    if ([self userAcceptedTerms] && [self allFieldsFilled])
    {
        [[self addCardButton] setEnabled:YES];
    }
    else
    {
        [[self addCardButton] setEnabled:NO];
    }
}

- (IBAction)addCardButtonClicked:(id)sender
{
    NSString *cardNumber = [NSString stringWithFormat:@"%@-%@-%@-%@",
                                                      [[self firstCardNumberBlock] text],
                                                      [[self secondCardNumberBlock] text],
                                                      [[self thirdCardNumberBlock] text],
                                                      [[self fourthCardNumberBlock] text]];

    NSString *expirationDate = [NSString stringWithFormat:@"%@-%@",
                                                          [[self firstDateBlock] text],
                                                          [[self secondDateBlock] text]];

    PRAsyncConnection *connection = [[PRAsyncConnection alloc]
                                                        initWithParent:self
                                                       successSelector:@selector(addCardRequestSucceed:)
                                                       didFailSelector:@selector(addCardRequestFailed:)];
    [connection sendRegisterCardWithNumber:cardNumber expirationDate:expirationDate];
}

- (void)addCardRequestSucceed:(PRAsyncConnection *)connection
{
    XLogTag(LogTag, @"%@", [[NSString alloc] initWithData:connection.responseData encoding:NSUTF8StringEncoding]);

    NSError *error;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:connection.responseData
                                                         options:(NSJSONReadingOptions)kNilOptions
                                                           error:&error];

    if (error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        return;
    }

    NSDictionary *register_card_response = [json objectForKey:@"register_card_response"];

    NSNumber *status = [register_card_response objectForKey:@"status"];

    if (status == nil)
    {
        [PRUtils displayStatusErrorDialog];
        return;
    }

    switch (status.intValue)
    {
        case STATUS_OK:
        {
            NSString *identifier = [register_card_response objectForKey:@"id"];
            
            PRAsyncConnection *connection = [[PRAsyncConnection alloc] initWithParent:self successSelector:@selector(connectionTotalValuesSuccess:) didFailSelector:@selector(connectionTotalValuesFailed:)];
            [connection sendTotalTransactionsValueRequestWithCardId:identifier];
        }
            break;
        case STATUS_USER_BLOCKED:
            [PRUtils displayStandardAlertViewWithTitle:NSLocalizedString(@"alertAttention", nil)
                                               message:NSLocalizedString(@"alertUserBlocked", nil)
                                 andConfirmButtonTitle:NSLocalizedString(@"alertOk", nil)];
            break;
        case STATUS_INVALID_CARD_NUMBER:
            [PRUtils displayStandardAlertViewWithTitle:NSLocalizedString(@"alertAttention", nil)
                                               message:NSLocalizedString(@"alertInvalidCardNumber", nil)
                                 andConfirmButtonTitle:NSLocalizedString(@"alertOk", nil)];
            break;

        case STATUS_ALREADY_REGISTERED_CARD:
            [PRUtils displayStandardAlertViewWithTitle:NSLocalizedString(@"alertAttention", nil)
                                               message:NSLocalizedString(@"alertAlreadyRegisteredCard", nil)
                                 andConfirmButtonTitle:NSLocalizedString(@"alertOk", nil)];
            break;

        default:
            break;
    }
}

- (void)addCardRequestFailed:(PRAsyncConnection *)connection
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Brak połączenia z internetem" delegate:nil
                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
}

#pragma mark - connection getTotalTransactionsValues selectors
- (void) connectionTotalValuesSuccess:(PRAsyncConnection*) connection
{
    
    XLogTag(LogTag, @"%@", [[NSString alloc] initWithData:connection.responseData encoding:NSUTF8StringEncoding]);
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:connection.responseData
                          options:kNilOptions
                          error:&error];
    
    if(error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        [PRUtils displayStatusErrorDialog];
        return;
    }
    
    NSDictionary *response = [json objectForKey:@"total_transactions_value_response"];
    
    NSNumber *status = [response objectForKey:@"status"];
    
    if(status==nil)
    {
        [PRUtils displayStatusErrorDialog];
        return;
    }
    
    switch (status.intValue) {
        case STATUS_OK:
        {
            _amount = [NSNumber numberWithInt:(20000 - ((NSNumber*)[response objectForKey:@"value"]).intValue)];
            
            [self performSegueWithIdentifier:@"showVerifyWarning" sender:self];
            
        }
            return;
        case STATUS_WRONG_CARD_ID:
        {
            [PRUtils displayStandardAlertViewWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertWrongCardId", nil) andConfirmButtonTitle:NSLocalizedString(@"alertOk", nil)];
        }
            return;
    }
    
}

- (void) connectionTotalValuesFailed:(PRAsyncConnection*) connection
{
    [PRUtils displayStatusErrorDialog];
}


@end
