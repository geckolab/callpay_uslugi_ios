//
//  TransactionEntity+Additons.m
//  pr
//
//  Created by Marcin Szulc on 25.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "TransactionEntity+Additons.h"
#import "PRUtils.h"

@implementation TransactionEntity (Additons)

+ (TransactionEntity*) insertTransactionEntityWithId:(NSNumber*) tId to: (NSNumber*) tJT treturn: (NSNumber*) tJR value:(NSNumber*) value timestamp:(NSString*) timestamp andTickets:(NSSet*) tickets withManagedObjectContext:(NSManagedObjectContext*) managedObjectContext
{
    TransactionEntity *transaction = [NSEntityDescription insertNewObjectForEntityForName:@"TransactionEntity" inManagedObjectContext:managedObjectContext];
    
    transaction.transactionId = tId;
    transaction.transactionJourneyTo = tJT;
    transaction.transactionJourneyReturn = tJR;
    transaction.transactionValue = value;
    transaction.transactionTimestamp = [PRUtils getDateFromStringTimestamp:timestamp];
    
    [transaction addTickets:tickets];
    
    return transaction;
}

+ (NSArray*) fetchAllTransactionsWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"TransactionEntity"];
    [request setSortDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"transactionTimestamp" ascending:NO]]];
    return [managedObjectContext executeFetchRequest:request error:nil];
}

@end
