//
//  MainMenuButton.m
//  CallPay
//
//  Created by Jacek on 16.03.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "MainMenuButton.h"

@implementation MainMenuButton


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
//    [super drawRect:rect];
    
//    self.imageView.frame = CGRectMake(0, 0, 192, 191);
//    self.titleLabel.frame = CGRectMake(0, 191, 192, 20);
    if (self.enabled)
    {
        [self.titleLabel setTextColor:[UIColor whiteColor]];
        self.imageView.image = [self.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    else
    {
        [self.titleLabel setTextColor:[UIColor colorWithRed:88/255.0 green:89/255.0 blue:91/255.0 alpha:1.0]];
        self.imageView.image = [self.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.imageView setTintColor:[UIColor colorWithRed:88/255.0 green:89/255.0 blue:91/255.0 alpha:1.0]];
    }
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setup];
    }
    return self;
}
- (id)init
{
    self = [super init];
    if (self)
    {
        [self setFrame:CGRectMake(0, 0, 60, 60)];
        [self setup];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self setup];
    }
    return self;
}
-(void)setup
{
    self.autoresizingMask = UIViewAutoresizingNone;
    [self setTranslatesAutoresizingMaskIntoConstraints:YES];
    
    // Width constraint
//    [self addConstraint:[NSLayoutConstraint constraintWithItem:self
//                                                     attribute:NSLayoutAttributeWidth
//                                                     relatedBy:NSLayoutRelationEqual
//                                                        toItem:self
//                                                     attribute:NSLayoutAttributeWidth
//                                                    multiplier:0.5
//                                                      constant:132.0]];
//
//    // Height constraint
//    [self addConstraint:[NSLayoutConstraint constraintWithItem:self
//                                                     attribute:NSLayoutAttributeHeight
//                                                     relatedBy:NSLayoutRelationEqual
//                                                        toItem:self
//                                                     attribute:NSLayoutAttributeHeight
//                                                    multiplier:1.0
//                                                      constant:0]];
    
//    self.imageView.frame = CGRectMake((self.frame.size.width - 96) / 2, 0, 192, 191);
//    self.titleLabel.frame = CGRectMake((self.frame.size.width - 96) / 2, 191, 192, 20);
    self.titleLabel.numberOfLines = 2;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    UIFont *fnt = [UIFont fontWithName:@"Swis721LtEU" size:self.titleLabel.font.pointSize];
    [self.titleLabel setFont:fnt];//self.backgroundColor = [UIColor redColor];
}
-(void)setNeedsLayout
{
//    self.imageView.frame = CGRectMake((self.frame.size.width - 96) / 2, 0, 96, 95);
//    self.titleLabel.frame = CGRectMake((self.frame.size.width - 96) / 2, 91, 96, 50);
//    [self.imageView setFrame:CGRectMake((self.frame.size.width - 96) / 2, 0, 96, 95)];
    [super setNeedsLayout];
    self.imageView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    self.titleLabel.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
}
-(void)setNeedsDisplay
{
    [super setNeedsDisplay];
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
//    return CGRectMake((self.frame.size.width - 96) / 2, 91, 96, 50);
    return CGRectMake(0, 91, self.frame.size.width, 50);
}
- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    return CGRectMake((self.frame.size.width - 96) / 2, 0, 96, 95);
}

- (void)updateConstraints {
//    [self addConstraint:[NSLayoutConstraint constraintWithItem:self
//                                                     attribute:NSLayoutAttributeWidth
//                                                     relatedBy:NSLayoutRelationEqual
//                                                        toItem:self
//                                                     attribute:NSLayoutAttributeWidth
//                                                    multiplier:0.5
//                                                      constant:132.0]];
    [super updateConstraints];
    NSLog(@"update constraints");
    // Width constraint
//    [self addConstraint:[NSLayoutConstraint constraintWithItem:self
//                                                     attribute:NSLayoutAttributeWidth
//                                                     relatedBy:NSLayoutRelationEqual
//                                                        toItem:self
//                                                     attribute:NSLayoutAttributeWidth
//                                                    multiplier:1.0
//                                                      constant:132.0]];
}

//88, 89, 91 disabled color
// Swis721LtEU-Normal.ttf

@end
