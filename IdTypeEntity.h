//
//  IdTypeEntity.h
//  CallPay
//
//  Created by Jacek on 15.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface IdTypeEntity : NSManagedObject

@property (nonatomic, retain) NSNumber * idTypeId;
@property (nonatomic, retain) NSString * idTypeName;
@property (nonatomic, retain) NSString * idTypeNumber;

@end
