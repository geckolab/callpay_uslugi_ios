//
//  TransactionEntity.h
//  CallPay
//
//  Created by Jacek on 15.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TicketEntity;

@interface TransactionEntity : NSManagedObject

@property (nonatomic, retain) NSNumber * transactionId;
@property (nonatomic, retain) NSNumber * transactionJourneyReturn;
@property (nonatomic, retain) NSNumber * transactionJourneyTo;
@property (nonatomic, retain) NSDate * transactionTimestamp;
@property (nonatomic, retain) NSNumber * transactionValue;
@property (nonatomic, retain) NSSet *tickets;
@end

@interface TransactionEntity (CoreDataGeneratedAccessors)

- (void)addTicketsObject:(TicketEntity *)value;
- (void)removeTicketsObject:(TicketEntity *)value;
- (void)addTickets:(NSSet *)values;
- (void)removeTickets:(NSSet *)values;

@end
