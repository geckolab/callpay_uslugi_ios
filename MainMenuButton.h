//
//  MainMenuButton.h
//  CallPay
//
//  Created by Jacek on 16.03.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>
//IB_DESIGNABLE
@interface MainMenuButton : UIButton

-(void)setup;

@end
