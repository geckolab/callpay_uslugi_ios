//
//  StationEntity.h
//  CallPay
//
//  Created by Jacek on 15.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface StationEntity : NSManagedObject

@property (nonatomic, retain) NSNumber * stationId;
@property (nonatomic, retain) NSString * stationName;

@end
