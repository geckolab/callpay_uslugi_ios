//
//  PRCardObject.h
//  pr
//
//  Created by Alexander Pimenov on 24.06.13.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PRCardObject : NSObject

@property (nonatomic) NSString *identifier;
@property (nonatomic) NSString *number;
@property (nonatomic) NSString *expirationDate;
@property (nonatomic, getter = isVerified) BOOL verified;

@property (nonatomic, getter = isActive) BOOL active;

- (BOOL)isDefaultForPurchases;
- (void)setDefaultForPurchases;

@end