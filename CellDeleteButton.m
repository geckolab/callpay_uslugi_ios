//
//  CellDeleteButton.m
//  CallPay
//
//  Created by Jacek on 02.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "CellDeleteButton.h"

@implementation CellDeleteButton


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    if (!(self.highlighted || self.selected))
    {
        [self.titleLabel setTextColor:[UIColor whiteColor]];
        self.imageView.image = [self.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    else
    {
        [self.titleLabel setTextColor:[UIColor colorWithRed:88/255.0 green:89/255.0 blue:91/255.0 alpha:1.0]];
        self.imageView.image = [self.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.imageView setTintColor:[UIColor colorWithRed:88/255.0 green:89/255.0 blue:91/255.0 alpha:1.0]];
    }
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setup];
    }
    return self;
}
- (id)init
{
    self = [super init];
    if (self)
    {
        [self setFrame:CGRectMake(0, 0, 19, 19)];
        [self setup];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self setup];
    }
    return self;
}
-(void)setup
{
    [self setImage:[UIImage imageNamed:@"new_icon_no"] forState:UIControlStateNormal];
    [self setAdjustsImageWhenHighlighted:YES];
    [self setTitle:@"" forState:UIControlStateNormal];
//    self.imageView.clipsToBounds = NO;
//    self.imageView.frame = CGRectMake((self.frame.size.width - 19) / 2, 0, 19, 19);
//    self.imageView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    //[self.imageView setTranslatesAutoresizingMaskIntoConstraints:NO];
    //[self setTranslatesAutoresizingMaskIntoConstraints:NO];
}
- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    return CGRectMake((self.frame.size.width - self.currentImage.size.width) / 2, 0, self.currentImage.size.width, self.currentImage.size.height);
//    return CGRectMake((contentRect.size.width - 19) / 2, 0, 19, 19);
//    return CGRectMake(0, 0, 19, 19);
}
- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    //    return CGRectMake((self.frame.size.width - 96) / 2, 91, 96, 50);
    return CGRectMake(0, self.currentImage.size.height, contentRect.size.height - self.currentImage.size.height, contentRect.size.width);
}
-(void)setNeedsLayout
{
//    self.imageView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
//    self.titleLabel.frame = CGRectMake(0, 0, 0, 0);
}
-(void)setNeedsDisplay
{
    [super setNeedsDisplay];
}
-(void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    [self setNeedsDisplay];
}
-(void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    [self setNeedsDisplay];
}

@end
