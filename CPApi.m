//
//  CPApi.m
//  CallPay
//
//  Created by Jacek on 24.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "CPApi.h"

@implementation CPApi

static CPApi *sharedAPIManager = nil;

+ (id)sharedManager
{
    NSLog(@"CPAPI shared manager");
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedAPIManager = [[self alloc] init];
    });
    return sharedAPIManager;
}

-(void)checkExternalTransfer
{
    
    NSArray *keys = [NSArray arrayWithObjects:@"clientPhone", @"serviceId", @"productCode", @"partnerToken", nil];
    NSArray *objects = [NSArray arrayWithObjects:@"48123456789", @"256",@"27740", @"token",  nil];
    NSDictionary *questionDict = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    
    NSData *jsonData;
    NSString *jsonString;
    if([NSJSONSerialization isValidJSONObject:questionDict])
    {
        jsonData = [NSJSONSerialization dataWithJSONObject:questionDict options:0 error:nil];
        jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
     NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@check-external",CP_API_URL]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
//    [request setHTTPMethod:@"GET"];
//    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody: jsonData];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%d", [jsonData length]] forHTTPHeaderField:@"Content-Length"];
    
    [request setTimeoutInterval:25.0];
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             NSLog(@"ERROR fetching: %@", error);
             if ([_delegateCheckExternalTransfer respondsToSelector:@selector(checkExternalTransferDidFail)])
             {
                 [_delegateCheckExternalTransfer checkExternalTransferDidFail];
             }
             return;
         }
         else
         {
//             if ([DEBUG_URL isEqualToString:@""])
//             {
//                 data = [data decodeApiContent];
//             }
             NSDictionary *parsedObject = [self getDictionaryWithJsonData:data];NSLog(@"%@",parsedObject);
             if (parsedObject != nil)
             {
                 NSLog(@"parsed dict: %@",parsedObject);
                 if ([_delegateCheckExternalTransfer respondsToSelector:@selector(checkExternalTransferDidSuccessWithDictionary:)])
                 {
                     [_delegateCheckExternalTransfer checkExternalTransferDidSuccessWithDictionary:parsedObject];
                 }
             }
             else
             {
//                 [self performSelectorOnMainThread:@selector(liga1RefreshFinished) withObject:nil waitUntilDone:NO];
                 NSLog(@"no dict!!!");
                 if ([_delegateCheckExternalTransfer respondsToSelector:@selector(checkExternalTransferDidFail)])
                 {
                     [_delegateCheckExternalTransfer checkExternalTransferDidFail];
                 }
             }
         }
     }];
}

-(NSDictionary *)getDictionaryWithJsonData:(NSData*)data
{
    NSString *jsonString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    NSDictionary *parsedObject;NSLog(@"\n%@\n",jsonString);
    NSString* string = [[[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] stringByReplacingOccurrencesOfString:@"\t" withString:@""] stringByReplacingOccurrencesOfString:@"\0" withString:@""];
    data = [string dataUsingEncoding:NSUTF8StringEncoding];
    
    @try
    {
        NSError *localError = nil;//NSLog(@"\nJSON str %@\n",jsonString);
        //        if([NSJSONSerialization isValidJSONObject:data])
        {
            parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers|NSJSONReadingAllowFragments error:&localError];
        }
        //        else
        //        {
        //            return nil;
        //        }
        //        NSLog(@"\n%@\n",parsedObject);
        if (localError != nil)
        {
            NSLog(@"\n--- getDictionaryWithJsonData ---\n%@\n\n%@\n\n--- END getDictionaryWithJsonData ---\n",localError,jsonString);
            return nil;
        }
        else
        {
            return parsedObject;
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"\n--- getDictionaryWithJsonData ---\n%@\n\n%@\n\n--- END getDictionaryWithJsonData ---\n",exception,jsonString);
        return nil;
    }
    @finally
    {
        
    }
}

-(void)checkExternalTransferWithPhone:(NSString*)phoneNo andProductCode:(NSString*)productCode andToken:(NSString*)token
{
    NSArray *productCodeArray = [productCode componentsSeparatedByString:@"."];
    NSString *serviceId = [productCodeArray firstObject];
    NSString *pCode;
    if([productCodeArray count] > 1)
        pCode = [productCodeArray lastObject];
    else
        pCode = @"";
    
    NSArray *keys = [NSArray arrayWithObjects:@"clientPhone", @"serviceId", @"productCode", @"partnerToken", nil];
    NSArray *objects = [NSArray arrayWithObjects:phoneNo, serviceId, pCode, token,  nil];
    NSDictionary *questionDict = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    
    NSData *jsonData;
    NSString *jsonString;
    if([NSJSONSerialization isValidJSONObject:questionDict])
    {
        jsonData = [NSJSONSerialization dataWithJSONObject:questionDict options:0 error:nil];
        jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@check-external",CP_API_URL]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];NSLog(@"%@",url);

    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody: jsonData];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%d", [jsonData length]] forHTTPHeaderField:@"Content-Length"];
    
    [request setTimeoutInterval:25.0];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSLog(@"\n\n%@\n\n", [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
         
         if (error)
         {
             NSLog(@"ERROR fetching: %@", error);
             if ([_delegateCheckExternalTransfer respondsToSelector:@selector(checkExternalTransferDidFail)])
             {
//                 [_delegateCheckExternalTransfer checkExternalTransferDidFail];
                 [_delegateCheckExternalTransfer performSelectorOnMainThread:@selector(checkExternalTransferDidFail) withObject:nil waitUntilDone:NO];
             }
             return;
         }
         else
         {
             //             if ([DEBUG_URL isEqualToString:@""])
             //             {
             //                 data = [data decodeApiContent];
             //             }
             NSDictionary *parsedObject = [self getDictionaryWithJsonData:data];NSLog(@"%@",parsedObject);
             if (parsedObject != nil)
             {
                 NSLog(@"parsed dict: %@",parsedObject);
                 if ([[parsedObject valueForKey:@"status"] intValue] == 1)
                 {
                     
                     if ([_delegateCheckExternalTransfer respondsToSelector:@selector(checkExternalTransferDidSuccessWithDictionary:)])
                     {
//                         [_delegateCheckExternalTransfer checkExternalTransferDidSuccessWithDictionary:[parsedObject objectForKey:@"return"]];
                         [_delegateCheckExternalTransfer performSelectorOnMainThread:@selector(checkExternalTransferDidSuccessWithDictionary:) withObject:[parsedObject objectForKey:@"return"] waitUntilDone:NO];
                     }
                 }
                 else
                 {
                     if ([_delegateCheckExternalTransfer respondsToSelector:@selector(checkExternalTransferDidFail)])
                     {
//                         [_delegateCheckExternalTransfer checkExternalTransferDidFail];
                         [_delegateCheckExternalTransfer performSelectorOnMainThread:@selector(checkExternalTransferDidFail) withObject:nil waitUntilDone:NO];
                     }
                 }
             }
             else
             {
                 //                 [self performSelectorOnMainThread:@selector(liga1RefreshFinished) withObject:nil waitUntilDone:NO];
                 NSLog(@"no dict!!!");
                 if ([_delegateCheckExternalTransfer respondsToSelector:@selector(checkExternalTransferDidFail)])
                 {
//                     [_delegateCheckExternalTransfer checkExternalTransferDidFail];
                     [_delegateCheckExternalTransfer performSelectorOnMainThread:@selector(checkExternalTransferDidFail) withObject:nil waitUntilDone:NO];
                 }
             }
         }
     }];
}
-(void)startTransactionNormalWithSessionToken:(NSString*)sessionToken
{
    NSArray *keys = [NSArray arrayWithObjects:@"sessionToken", @"acquirerData", nil];
    NSArray *objects = [NSArray arrayWithObjects:sessionToken, @"",  nil];
    NSDictionary *questionDict = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    
    NSData *jsonData;
    NSString *jsonString;
    if([NSJSONSerialization isValidJSONObject:questionDict])
    {
        jsonData = [NSJSONSerialization dataWithJSONObject:questionDict options:0 error:nil];
        jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@transaction-start-normal",CP_API_URL]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody: jsonData];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%d", [jsonData length]] forHTTPHeaderField:@"Content-Length"];
    
    [request setTimeoutInterval:25.0];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             NSLog(@"ERROR fetching: %@", error);
             if ([_delegateTransactionStart respondsToSelector:@selector(transactionStartDidFail)])
             {
//                 [_delegateTransactionStart transactionStartDidFail];
                 [_delegateTransactionStart performSelectorOnMainThread:@selector(transactionStartDidFail) withObject:nil waitUntilDone:NO];
             }
             return;
         }
         else
         {
             //             if ([DEBUG_URL isEqualToString:@""])
             //             {
             //                 data = [data decodeApiContent];
             //             }
             NSDictionary *parsedObject = [self getDictionaryWithJsonData:data];NSLog(@"%@",parsedObject);
             if (parsedObject != nil)
             {
                 NSLog(@"parsed dict: %@",parsedObject);
                 if ([_delegateTransactionStart respondsToSelector:@selector(transactionStartDidSuccessWithDictionary:)])
                 {
//                     [_delegateTransactionStart transactionStartDidSuccessWithDictionary:parsedObject];
                     [_delegateTransactionStart performSelectorOnMainThread:@selector(transactionStartDidSuccessWithDictionary:) withObject:[parsedObject objectForKey:@"return"] waitUntilDone:NO];
                 }
             }
             else
             {
                 //                 [self performSelectorOnMainThread:@selector(liga1RefreshFinished) withObject:nil waitUntilDone:NO];
                 NSLog(@"no dict!!!");
                 if ([_delegateTransactionStart respondsToSelector:@selector(transactionStartDidFail)])
                 {
//                     [_delegateTransactionStart transactionStartDidFail];
                     [_delegateTransactionStart performSelectorOnMainThread:@selector(transactionStartDidFail) withObject:nil waitUntilDone:NO];
                 }
             }
         }
     }];
}

@end
