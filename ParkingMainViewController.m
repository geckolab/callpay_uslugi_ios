//
//  ParkingMainViewController.m
//  CallPay
//
//  Created by Jacek on 10.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "ParkingMainViewController.h"

#import "CarsManager.h"

@interface ParkingMainViewController ()

@end

@implementation ParkingMainViewController

@synthesize citySelectedView = _citySelectedView;
@synthesize carSelectedView = _carSelectedView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSString *cityToDisplay = [ProjectLocations object].currentParkingCity;
//    NSString *carToDisplay = [ProjectLocations object].currentCar;
    NSString *carToDisplay = [CarsManager object].currentCar;
    if ([cityToDisplay isEqualToString:@""])
    {
        cityToDisplay = @"BRAK";
    }
    if ([carToDisplay isEqualToString:@""])
    {
        carToDisplay = @"BRAK";
    }
    [_citySelectedView setCity:cityToDisplay];
    [_carSelectedView setCity:carToDisplay];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(IBAction)buyAction:(id)sender
{
    if ([[CarsManager object].currentCar isEqualToString:@""] || [[CarsManager object] countAll] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:@"Wybierz numer rejestracyjny pojazdu." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ParkingCarListVC"];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if ([[ProjectLocations object].currentParkingCity isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:@"Wybierz miasto." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ParkingCityListVC"];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ParkingBuyVC"];
//    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ParkingTicketListVC"];
        [self.navigationController pushViewController:vc animated:YES];
    }
}
-(IBAction)citySelectAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ParkingCityListVC"];
    [self.navigationController pushViewController:vc animated:YES];
}
-(IBAction)carSelectAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ParkingCarListVC"];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
