//
//  CPWsdl.m
//  CallPay
//
//  Created by Jacek on 23.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "CPWsdl.h"

#import "CPUCPCheckResult.h"

#define CP_URL @"https://testapi.callpay.pl/wsdl/IExternalAPI.wsdl"
//#define CP_URL @"https://24.callpay.pl/wsdl/IExternalAPI.wsdl"

#define ALERT(msg) {UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"SOAPEngine Sample" message:msg delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil];[alert show];}

@implementation CPWsdl

static CPWsdl *object;

+ (CPWsdl *) object {
    if (!object) {
        object = [[CPWsdl alloc] init];
    }
    
    return object;
}

-(id)init
{
    self = [super init];
    if (self)
    {
//        self.serviceUrl = CP_URL;
    }
    return self;
}

-(void)checkExternalTransfer
{
    SOAPEngine *soap = [[SOAPEngine alloc] init];
    soap.userAgent = @"SOAPEngine";
    /*
    soap.delegate = self; // use SOAPEngineDelegate
    
    // each single value
    [soap setValue:@"48123456789" forKey:@"client-phone"];
    [soap setIntegerValue:256 forKey:@"service-id"];
    // service url without ?WSDL, and you can search the soapAction in the WSDL
    [soap requestURL:CP_URL soapAction:@"CpCheck"];
     */
    [soap requestWSDL:CP_URL operation:@"CpCheck" completeWithDictionary:^(NSInteger statusCode, NSDictionary *dict) {
        
        NSLog(@"Result: %@", dict);
        
    } failWithError:^(NSError *error) {
        
        NSLog(@"%@", error);
    }];
    
    /*
    NSMutableArray* _params = [NSMutableArray array];
    
    [_params addObject: [[SoapParameter alloc] initWithValue: [NSNumber numberWithInt: @"48604400097"] forName: @"client-phone"]];
    [_params addObject: [[SoapParameter alloc] initWithValue: @"256" forName: @"service-id"]];
    [_params addObject: [[SoapParameter alloc] initWithValue: @"27740" forName: @"product-code"]];
    [_params addObject: [[SoapParameter alloc] initWithValue: @"token" forName: @"partner-token"]];
//    NSString* _envelope = [Soap createEnvelope: @"CpCheck-ExternalTransfer" forNamespace: self.namespace withParameters: _params withHeaders: self.headers];
    NSString* _envelope = [Soap createEnvelope: @"CpCheck" forNamespace: self.namespace withParameters: _params withHeaders: self.headers];
//    SoapRequest* _request = [SoapRequest create: self action: @selector(checkExternalTransferResult) service: self soapAction: @"upaid.pl#check" postData: _envelope deserializeTo: [UPaidstatusResult alloc]];
    
    SoapRequest* _request = [SoapRequest create:nil action: @selector(checkExternalTransferResult) service:self soapAction:@"CpCheck" postData:_envelope deserializeTo:[CPUCPCheckResult alloc]];
    //_request.requestDelegate = self;
    [_request send];
     */
}

-(void)checkExternalTransferResult
{
    NSLog(@"test");
    
}
- (void)soapEngine:(SOAPEngine *)soapEngine didFinishLoading:(NSString *)stringXML {
    
    NSDictionary *result = [soapEngine dictionaryValue];NSLog(@"\n\nresult\n%@\n",result);
    // read data from a dataset table
    NSArray *list = [result valueForKeyPath:@"NewDataSet.Table"];
}

- (void)soapEngine:(SOAPEngine *)soapEngine didFailWithError:(NSError *)error {
    
    NSString *msg = [NSString stringWithFormat:@"ERROR: %@", error.localizedDescription];
    ALERT(msg);
}

@end
