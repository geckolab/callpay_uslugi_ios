//
//  PRSummaryViewController.m
//  pr
//
//  Created by Marcin Szulc on 24.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PRSummaryViewController.h"
#import "PRSearchRelationsViewController.h"

@interface PRSummaryViewController ()

@end

@implementation PRSummaryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIBarButtonItem * mcmLogo = [[UIBarButtonItem alloc] initWithCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mcm_icon"]]];
    self.navigationItem.rightBarButtonItem = mcmLogo;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.hidesBackButton = YES;
}

- (IBAction)buttonSearchAgainOnClick:(id)sender {
    
    PRSearchRelationsViewController *destController = [self.navigationController.viewControllers objectAtIndex:0];
    
    [destController resetOrderObject];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)buttonYourTicketOnClick:(id)sender {
    [self.tabBarController setSelectedIndex:1];
    
    PRSearchRelationsViewController *destController = [self.navigationController.viewControllers objectAtIndex:0];
    
    [destController resetOrderObject];
    
    [self.navigationController popToRootViewControllerAnimated:NO];
}
@end
