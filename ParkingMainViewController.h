//
//  ParkingMainViewController.h
//  CallPay
//
//  Created by Jacek on 10.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "ProjectViewController.h"

#import "SelectedCityView.h"
#import "ProjectLocations.h"

@interface ParkingMainViewController : ProjectViewController

@property (nonatomic, strong) IBOutlet SelectedCityView *citySelectedView;
@property (nonatomic, strong) IBOutlet SelectedCityView *carSelectedView;

-(IBAction)buyAction:(id)sender;
-(IBAction)citySelectAction:(id)sender;
-(IBAction)carSelectAction:(id)sender;

@end
