//
//  DiscountEntity+Additions.m
//  pr
//
//  Created by Marcin Szulc on 18.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "DiscountEntity+Additions.h"

@implementation DiscountEntity (Additions)

+ (DiscountEntity*) insertDiscountWithId:(NSNumber*) index andName:(NSString*) name andClass:(NSNumber*) vehicleClass andSortIndex:(NSNumber*) sortIndex withManagedObjectContext:(NSManagedObjectContext*) managedObjectContext
{
    DiscountEntity *discount = [NSEntityDescription insertNewObjectForEntityForName:@"DiscountEntity" inManagedObjectContext:managedObjectContext];
    
    discount.discountId = index;
    discount.discountName = name;
    discount.discountVehicleClass = vehicleClass;
    discount.discountSortIndex = sortIndex;
    
    return discount;
}

+ (void) clearAllRecordsWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext
{
    NSArray *discounts = [self fetchAllDiscountsWithManagedObjectContext:managedObjectContext forClass:nil];
    
    for(DiscountEntity *discount in discounts)
    {
        [managedObjectContext deleteObject:discount];
    }
}

+ (NSArray*) fetchAllDiscountsWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext forClass:(NSNumber*) vehicleClass
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"DiscountEntity"];
    [request setSortDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"discountSortIndex" ascending:YES]]];
    if(vehicleClass != nil)
    {
        [request setPredicate:[NSPredicate predicateWithFormat:@"discountVehicleClass <= %@", vehicleClass]];
    }
    return [managedObjectContext executeFetchRequest:request error:nil];
}

@end
