//
//  PREditCardViewController.m
//  pr
//
//  Created by Alexander Pimenov on 24.06.13.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PREditCardViewController.h"
#import "PRCardObject.h"
#import "UIColor+HexColorCreation.h"
#import "PRAsyncConnection.h"
#import "XLog.h"
#import "PRUtils.h"
#import "PRVerifyCardPinViewController.h"
#import "PRVerifyWarningViewController.h"

static NSString *const LogTag = @"PREditCardViewController";


@interface PREditCardViewController () <PRVerifyWarningDelegate>

@property (weak, nonatomic) IBOutlet UILabel *cardNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *expirationDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *defaultStatusLabel;

@property (weak, nonatomic) IBOutlet UIButton *makeDefaultButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIButton *verifyButton;
@property (strong, nonatomic) NSNumber *amount;
@end


@implementation PREditCardViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    UIBarButtonItem * mcmLogo = [[UIBarButtonItem alloc] initWithCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mcm_icon"]]];
    self.navigationItem.rightBarButtonItem = mcmLogo;
    
    [self initializeWithCardData];

    [self addBackgroundImagesToButtons];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqual:@"verifyCard"])
    {
        PRVerifyCardPinViewController *dest = segue.destinationViewController;
        [dest setCard:_card];
    }
    else if ([segue.identifier isEqualToString:@"showVerifyWarning"])
    {
        PRVerifyWarningViewController *destController = segue.destinationViewController;
        [destController setAmount:_amount.intValue];
        [destController setDelegate:self];
    }
}

- (void) buttonOkClickedInViewController:(UIViewController*) controller
{
    [self defaultButtonClicked];
}

- (void)initializeWithCardData
{
    [[self cardNumberLabel] setText:[[self card] number]];
    [[self expirationDateLabel] setText:[[self card] expirationDate]];

    if ([[self card] isActive])
    {
        [[self statusLabel] setText:@"AKTYWNA"];
        [[self statusLabel] setTextColor:[UIColor colorWithHexString:@"0DB63A"]];
    }
    else
    {
        [[self statusLabel] setText:@"ZABLOKOWANA"];
        [[self statusLabel] setTextColor:[UIColor colorWithHexString:@"AD2333"]];
    }

    if ([[self card] isDefaultForPurchases])
    {
        [[self defaultStatusLabel] setText:NSLocalizedString(@"YES", nil)];
        [[self makeDefaultButton] setEnabled:NO];
    }
    else
    {
        [[self defaultStatusLabel] setText:NSLocalizedString(@"NO", nil)];
    }
    
    [_verifyButton setEnabled:[_card isVerified] == NO];
    
}

- (void)addBackgroundImagesToButtons
{
    [[self verifyButton] setBackgroundImage:[self resizableImageWithBlueColor:YES
                                                               ForHighlightedState:NO]
                                        forState:UIControlStateNormal];
    [[self verifyButton] setBackgroundImage:[self resizableImageWithBlueColor:YES
                                                               ForHighlightedState:YES]
                                        forState:UIControlStateHighlighted];
    
    [[self makeDefaultButton] setBackgroundImage:[self resizableImageWithBlueColor:YES
                                                               ForHighlightedState:NO]
            forState:UIControlStateNormal];
    [[self makeDefaultButton] setBackgroundImage:[self resizableImageWithBlueColor:YES
                                                               ForHighlightedState:YES]
            forState:UIControlStateHighlighted];

    [[self deleteButton] setBackgroundImage:[self resizableImageWithBlueColor:NO
                                                          ForHighlightedState:NO]
            forState:UIControlStateNormal];
    [[self deleteButton] setBackgroundImage:[self resizableImageWithBlueColor:NO
                                                          ForHighlightedState:YES]
            forState:UIControlStateHighlighted];
}

- (UIImage *)resizableImageWithBlueColor:(BOOL)isBlueColor
                     ForHighlightedState:(BOOL)isHighlighted
{
    NSString *imageName = [NSString stringWithFormat:@"button_%@_%@.png",
                                                     isBlueColor ? @"blue" : @"red",
                                                     isHighlighted ? @"pressed" : @"active"];
    UIImage *image = [UIImage imageNamed:imageName];
    return [image resizableImageWithCapInsets:UIEdgeInsetsMake(0, 15, 0, 15)];
}

- (void) defaultButtonClicked
{
    [[self card] setDefaultForPurchases];
    
    [[self defaultStatusLabel] setText:@"tak"];
    [[self makeDefaultButton] setEnabled:NO];
    
    [[self navigationController] popViewControllerAnimated:YES];
}

- (IBAction)makeDefaultButtonClicked:(id)sender
{
    if([_card isVerified] == YES)
    {
        [self defaultButtonClicked];
    }
    else
    {
        PRAsyncConnection *connection = [[PRAsyncConnection alloc] initWithParent:self successSelector:@selector(connectionTotalValuesSuccess:) didFailSelector:@selector(connectionTotalValuesFailed:)];
        [connection sendTotalTransactionsValueRequestWithCardId:_card.identifier];
    }
}

- (IBAction)deleteCardButtonClicked:(id)sender
{
    PRAsyncConnection *connection = [[PRAsyncConnection alloc]
                                                        initWithParent:self
                                                       successSelector:@selector(deleteCardRequestSucceed:)
                                                       didFailSelector:@selector(deleteCardRequestFailed:)];

    [connection sendDeleteCardWithNumber:[[self card] identifier]];
}

- (IBAction)verifyButtonClicked:(id)sender {
    [self performSegueWithIdentifier:@"verifyCard" sender:self];
}

- (void)deleteCardRequestSucceed:(PRAsyncConnection *)connection
{
    XLogTag(LogTag, @"%@", [[NSString alloc] initWithData:connection.responseData encoding:NSUTF8StringEncoding]);

    NSError *error;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:connection.responseData
                                                         options:(NSJSONReadingOptions)kNilOptions
                                                           error:&error];

    if (error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        return;
    }

    NSDictionary *remove_card_response = [json objectForKey:@"remove_card_response"];

    NSNumber *status = [remove_card_response objectForKey:@"status"];

    if (status == nil)
    {
        [PRUtils displayStatusErrorDialog];
        return;
    }

    switch (status.intValue)
    {
        case STATUS_OK:
            [[self delegate] editCardViewControllerDeletedCard:self];
            break;
        case STATUS_UNABLE_TO_REMOVE_CARD:
            [PRUtils displayStandardAlertViewWithTitle:NSLocalizedString(@"alertAttention", nil)
                                               message:NSLocalizedString(@"alertRemoveCardResponse", nil)
                                 andConfirmButtonTitle:NSLocalizedString(@"alertOk", nil)];
            break;
        default:
            break;
    }
}

- (void)deleteCardRequestFailed:(PRAsyncConnection *)connection
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Brak połączenia z internetem" delegate:nil
                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
}

- (void)viewDidUnload
{
    [self setCardNumberLabel:nil];
    [self setStatusLabel:nil];
    [self setExpirationDateLabel:nil];
    [self setDefaultStatusLabel:nil];
    [self setMakeDefaultButton:nil];
    [self setDeleteButton:nil];
    [super viewDidUnload];
}

#pragma mark - connection getTotalTransactionsValues selectors
- (void) connectionTotalValuesSuccess:(PRAsyncConnection*) connection
{
    
    XLogTag(LogTag, @"%@", [[NSString alloc] initWithData:connection.responseData encoding:NSUTF8StringEncoding]);
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:connection.responseData
                          options:kNilOptions
                          error:&error];
    
    if(error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        [PRUtils displayStatusErrorDialog];
        return;
    }
    
    NSDictionary *response = [json objectForKey:@"total_transactions_value_response"];
    
    NSNumber *status = [response objectForKey:@"status"];
    
    if(status==nil)
    {
        [PRUtils displayStatusErrorDialog];
        return;
    }
    
    switch (status.intValue) {
        case STATUS_OK:
        {
            _amount = [NSNumber numberWithInt:(20000 - ((NSNumber*)[response objectForKey:@"value"]).intValue)];
            
            [self performSegueWithIdentifier:@"showVerifyWarning" sender:self];

        }
            return;
        case STATUS_WRONG_CARD_ID:
        {
            [PRUtils displayStandardAlertViewWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertWrongCardId", nil) andConfirmButtonTitle:NSLocalizedString(@"alertOk", nil)];
        }
            return;
    }
    
}

- (void) connectionTotalValuesFailed:(PRAsyncConnection*) connection
{
    [PRUtils displayStatusErrorDialog];
}

@end
