//
//  SettingsEntity+Additions.h
//  pr
//
//  Created by Marcin Szulc on 17.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "SettingsEntity.h"

@interface SettingsEntity (Additions)

+ (SettingsEntity*) insertDefaultSettingsWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext;

+ (SettingsEntity*) fetchSettingsObjectWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext;

@end
