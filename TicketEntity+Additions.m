//
//  TicketEntity+Additions.m
//  pr
//
//  Created by Marcin Szulc on 24.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "TicketEntity+Additions.h"
#import "PROrderObject.h"
#import "PRRelationObject.h"
#import "TransactionEntity+Additons.h"
#import "PRUtils.h"

@implementation TicketEntity (Additions)

+ (TicketEntity*) insertTicketEntityWithValuesFromJsonResponseTicket:(NSDictionary*) _ticket withManagedObjectContext:(NSManagedObjectContext*) managedObjectContext
{
    TicketEntity *ticket = [NSEntityDescription insertNewObjectForEntityForName:@"TicketEntity" inManagedObjectContext:managedObjectContext];
    
    ticket.ticketCode = [_ticket objectForKey:@"code"];
    ticket.ticketSignature = [_ticket objectForKey:@"signature"];
    ticket.ticketValue = [_ticket objectForKey:@"value"];
    ticket.ticketPrice = [_ticket objectForKey:@"price"];
    ticket.ticketVatRate = [_ticket objectForKey:@"vat_rate"];
    ticket.ticketVatValue = [_ticket objectForKey:@"vat_value"];
    ticket.ticketCarrier = [_ticket objectForKey:@"carrier"];
    ticket.ticketOffer = [_ticket objectForKey:@"offer"];
    ticket.ticketDiscount = [_ticket objectForKey:@"discount"];
    ticket.ticketCategoryTo = [_ticket objectForKey:@"category_to"];
    ticket.ticketCategoryReturn = [_ticket objectForKey:@"category_return"];
    ticket.ticketFrom = [_ticket objectForKey:@"from"];
    ticket.ticketTo = [_ticket objectForKey:@"to"];
    ticket.ticketThrough = [_ticket objectForKey:@"through"];
    ticket.ticketValidTo = [_ticket objectForKey:@"valid_to"];
    ticket.ticketValidToHours = [_ticket objectForKey:@"valid_to_hours"];
    ticket.ticketValidReturn = [_ticket objectForKey:@"valid_return"];
    ticket.ticketValidReturnHours = [_ticket objectForKey:@"valid_return_hours"];
    
    NSDictionary *ownerData = [[_ticket objectForKey:@"owner_data"] objectForKey:@"owner"];
    
    ticket.ticketOwnerName = [ownerData objectForKey:@"name"];
    ticket.ticketOwnerSurname = [ownerData objectForKey:@"surname"];
    ticket.ticketOwnerDocument = [ownerData objectForKey:@"document"];
    ticket.ticketOwnerSerial = [ownerData objectForKey:@"serial"];
    
    ticket.ticketSerial = [_ticket objectForKey:@"serial"];
    ticket.ticketDistanceTo = [_ticket objectForKey:@"distance_to"];
    ticket.ticketDistanceReturn = [_ticket objectForKey:@"distance_return"];
    
    ticket.ticketClassTo = [_ticket objectForKey:@"vehicle_class_to"];
    ticket.ticketClassReturn = [_ticket objectForKey:@"vehicle_class_return"];
    
    ticket.ticketDescription = [_ticket objectForKey:@"description"];
    
    return ticket;
}

+ (NSArray*) fetchAllTicketsWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"TicketEntity"];
    return [managedObjectContext executeFetchRequest:request error:nil];
}

+ (BOOL) isTicketActive:(TicketEntity*) ticket
{
    return [TicketEntity getSecondsSinceTicketBought:ticket] > TICKET_ACTIVE_SECONDS;
}

+ (int) getSecondsSinceTicketBought:(TicketEntity*) ticket
{
    NSDate *ticketBoughtAt = ticket.transaction.transactionTimestamp;
    NSDate *now = [NSDate date];
    return (now.timeIntervalSince1970 - ticketBoughtAt.timeIntervalSince1970);
}

+ (BOOL) isTicketValid:(TicketEntity*) ticket
{
    NSDate *currentDate = [PRUtils resetDateToMidnight:[NSDate date]];
    NSDate *compareDate = [PRUtils getDateFromDateString:ticket.ticketValidTo withInputFormat:@"yyyyMMddHHmmss"];
    return [currentDate compare:compareDate] != NSOrderedDescending;
}

@end
