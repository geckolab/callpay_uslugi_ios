//
//  CityActiveStatusView.h
//  CallPay
//
//  Created by Jacek on 03.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CityActiveStatusView : UIView
{
    UIImage *accessoryImage;
    UIImageView *accessoryImageView;
    BOOL statusActive;
}
-(void)setup;

-(void)setActive:(BOOL)active;

@end
