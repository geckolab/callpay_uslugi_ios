//
//  PRRegisterCardViewController.h
//  pr
//
//  Created by Marcin Szulc on 25.03.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PRRegisterCardViewController;


@protocol PRRegisterCardViewControllerDelegate <NSObject>

- (void)registerCardViewControllerShouldBeDismissed:(PRRegisterCardViewController *)registerCardViewController;

@end


@interface PRRegisterCardViewController : UIViewController

@property (weak, nonatomic) id <PRRegisterCardViewControllerDelegate> delegate;

@property (strong, nonatomic) NSString *upaidToken;

@property (nonatomic, getter = isUsedForInitialCardSelection) BOOL usedForInitialCardSelection;

@end
