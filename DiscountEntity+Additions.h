//
//  DiscountEntity+Additions.h
//  pr
//
//  Created by Marcin Szulc on 18.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "DiscountEntity.h"

@interface DiscountEntity (Additions)

+ (DiscountEntity*) insertDiscountWithId:(NSNumber*) index andName:(NSString*) name andClass:(NSNumber*) vehicleClass andSortIndex:(NSNumber*) sortIndex withManagedObjectContext:(NSManagedObjectContext*) managedObjectContext;

+ (void) clearAllRecordsWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext;

+ (NSArray*) fetchAllDiscountsWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext forClass:(NSNumber*) vehicleClass;

@end
