//
//  TransactionEntity.m
//  CallPay
//
//  Created by Jacek on 15.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "TransactionEntity.h"
#import "TicketEntity.h"


@implementation TransactionEntity

@dynamic transactionId;
@dynamic transactionJourneyReturn;
@dynamic transactionJourneyTo;
@dynamic transactionTimestamp;
@dynamic transactionValue;
@dynamic tickets;

@end
