//
//  CPUCPCheckResult.h
//  CallPay
//
//  Created by Jacek on 23.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "Soap.h"


@interface CPUCPCheckResult : SoapObject
{
    int _amount;
    int _acquirer;
    NSString* _session_token;
    NSString* _partner_token;
    NSString* _authorization_type;
    NSString* _authorization_field_type;
    int _authorization_field_length;
    
}

@property int amount;
@property int acquirer;
@property (retain, nonatomic) NSString* session_token;
@property (retain, nonatomic) NSString* partner_token;
@property (retain, nonatomic) NSString* authorization_type;
@property (retain, nonatomic) NSString* authorization_field_type;
@property int authorization_field_length;

+ (CPUCPCheckResult*) createWithNode: (CXMLNode*) node;
- (id) initWithNode: (CXMLNode*) node;
- (NSMutableString*) serialize;
- (NSMutableString*) serialize: (NSString*) nodeName;
- (NSMutableString*) serializeAttributes;
- (NSMutableString*) serializeElements;

@end
