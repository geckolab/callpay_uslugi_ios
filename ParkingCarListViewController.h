//
//  ParkingCarListViewController.h
//  CallPay
//
//  Created by Jacek on 10.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "ProjectViewController.h"

@interface ParkingCarListViewController : ProjectViewController <UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *carsList;
    NSMutableArray *filteredList;
}

@property (nonatomic, strong) IBOutlet UITextField *addText;

@property (nonatomic, strong) IBOutlet UISearchBar *sBar;
@property (nonatomic, strong) IBOutlet UITableView *tableView;

-(void)searchListWith:(NSString*)searchText;

-(void)reloadData;

-(IBAction)addTextChangeAction:(id)sender;

@end
