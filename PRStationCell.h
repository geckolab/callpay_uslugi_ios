//
//  PRStationCell.h
//  pr
//
//  Created by Marcin Szulc on 21.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RectangleWhiteBarsView.h"

#define PR_STATION_TABLE_CELL_HEIGHT 39

@interface PRStationCell : UITableViewCell

@property (nonatomic, strong) RectangleWhiteBarsView *barsView;
@property (strong, nonatomic) IBOutlet UILabel *textStationName;

@end
