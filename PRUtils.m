//
//  PRUtils.m
//  pr
//
//  Created by Marcin Szulc on 16.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PRUtils.h"
#import <QuartzCore/QuartzCore.h>

#import <CommonCrypto/CommonDigest.h>
#import "PRRelationObject.h"

@implementation PRUtils

+ (id) insertStringOrNumberFrom:(id) from To:(id) to
{
    if([to isKindOfClass:[NSNumber class]])
    {
        if([from isKindOfClass:[NSNumber class]])
        {
            return from;
        }
        else if([from isKindOfClass:[NSString class]])
        {
            return [NSNumber numberWithInt:((NSString*)from).intValue];
        }
    }
    else if([to isKindOfClass:[NSString class]])
    {
        if([from isKindOfClass:[NSNumber class]])
        {
            return ((NSNumber*)from).stringValue;
        }
        else if([from isKindOfClass:[NSString class]])
        {
            return from;
        }
    }
    
    return nil;
}

#pragma mark - date

+ (NSString*) getDateForFormat:(NSString*) format fromDate:(NSDate*) date
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:format];
    
    return [df stringFromDate:date];
}

+ (NSString*) getTravelTimeForStartHour:(NSString*) startHour andBackHour:(NSString*) backHour
{
    NSArray *startTimeComponents = [startHour componentsSeparatedByString:@":"];
    NSArray *endTimeComponents  = [backHour componentsSeparatedByString:@":"];
    
    NSNumber *startTimeHour = [NSNumber numberWithInt:((NSString*)[startTimeComponents objectAtIndex:0]).intValue];
    NSNumber *startTimeMinutes = [NSNumber numberWithInt:((NSString*)[startTimeComponents objectAtIndex:1]).intValue];
    
    NSNumber *backTimeHour = [NSNumber numberWithInt:((NSString*)[endTimeComponents objectAtIndex:0]).intValue];
    NSNumber *backTimeMinutes = [NSNumber numberWithInt:((NSString*)[endTimeComponents objectAtIndex:1]).intValue];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *startComponents = [[NSDateComponents alloc]init];
    startComponents.hour = startTimeHour.intValue;
    startComponents.minute = startTimeMinutes.intValue;
    startComponents.year = 2013;
    startComponents.month = 1;
    startComponents.day = 1;
    
    NSDateComponents *backComponents = [[NSDateComponents alloc]init];
    backComponents.hour = backTimeHour.intValue;
    backComponents.minute = backTimeMinutes.intValue;
    backComponents.year = 2013;
    backComponents.month = 1;
    backComponents.day = backTimeHour.intValue < startTimeHour.intValue ? 2 : 1;
    
    NSDate *startDate = [calendar dateFromComponents:startComponents];
    NSDate *endDate = [calendar dateFromComponents:backComponents];
    
    NSTimeInterval distanceBetweenDates = [endDate timeIntervalSinceDate:startDate];
    
    NSInteger hoursBetweenDates = distanceBetweenDates / 3600;
    NSInteger minutesBetweenDates = (distanceBetweenDates - (hoursBetweenDates * 3600))/60;
    
    return [NSString stringWithFormat:@"%d godz. %d min", hoursBetweenDates, minutesBetweenDates];
}

+ (NSDate*) getDateFromStringTimestamp:(NSString*) date
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:@"yyyyMMddHHmmSS"];
    
    return [df dateFromString:date];
}

+ (NSDate*) getDateFromDateString:(NSString*) date withInputFormat:(NSString*) inputFormat
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:inputFormat];
    
    return [df dateFromString:date];
}

+ (NSDate*) getDateFromDateString:(NSString*) date
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:@"yyyyMMdd"];
    
    return [df dateFromString:date];
}

+ (NSDate*) resetDateToMidnight:(NSDate*) date
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    return [calendar dateFromComponents:components];
}

+ (NSString*) getFormattedDateFromStringTimestamp:(NSString*) date
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:@"yyyyMMddHHmmSS"];
    
    NSDate *dateResult = [df dateFromString:date];
    
    [df setDateFormat:@"yyyy.MM.dd HH:mm:SS"];
    
    return [df stringFromDate:dateResult];
}

+ (NSString*) getFormattedDateFromString:(NSString*) date withInputFormat:(NSString*) inputFormat andOutputFormat:(NSString*) outputFormat
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:inputFormat];
    
    NSDate *dateResult = [df dateFromString:date];
    
    [df setDateFormat:outputFormat];
    
    return [df stringFromDate:dateResult];
}

+ (NSString*) getFormattedDateFromString:(NSString*) date
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:@"yyyyMMdd"];
    
    NSDate *dateResult = [df dateFromString:date];
    
    [df setDateFormat:@"dd.MM.yyyy"];
    
    return [df stringFromDate:dateResult];
}

+ (NSString*) getFormattedTimestampFromDate:(NSDate*) date
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:@"dd.MM.yyyy HH:mm:SS"];
    
    return [df stringFromDate:date];
}

+ (NSString*) getFormattedDateFromDateForReturnConnection:(PROrderObject*) order
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:@"yyyyMMdd"];
    
    NSString *dateBack = [df stringFromDate:order.orderDateBack];
    
    if([order.orderDateFrom isEqual:order.orderDateBack])
    {
        NSString *hour = [order.relationFrom.relationArrivalTime stringByReplacingOccurrencesOfString:@":" withString:@""];
        
        return [NSString stringWithFormat:@"%@-%@", dateBack, hour];
    }
    else
    {
        return [NSString stringWithFormat:@"%@-0000", dateBack];
    }
}

+ (NSString*) getFormattedDateFromDateForConnection:(NSDate*) date
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:@"yyyyMMdd"];
    
    return [df stringFromDate:date];
}

+ (NSString*) getFormattedDateFromDate:(NSDate*) date
{
    if(date==nil)
    {
        return nil;
    }
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:@"dd.MM.yyyy"];
    
    return [df stringFromDate:date];
}

+ (NSDate *)addYears:(NSInteger)years toDate:(NSDate*)date {
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *components = [[NSDateComponents alloc]init];
    components.year = years;
    return [calendar dateByAddingComponents:components toDate:date options:0];
}

+ (NSDate*)addMonths:(NSInteger)months toDate:(NSDate*)date
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents* dateComponents = [[NSDateComponents alloc]init];
    [dateComponents setMonth:months];
    return [calendar dateByAddingComponents:dateComponents toDate:date options:0];
}

+ (NSDate *)addWeeks:(NSInteger)weeks toDate:(NSDate*)date {
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents* dateComponents = [[NSDateComponents alloc]init];
    dateComponents.week = weeks;
    return [calendar dateByAddingComponents:dateComponents toDate:date options:0];
}

+ (NSDate*)addDays:(NSInteger)days toDate:(NSDate*)date
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents* dateComponents = [[NSDateComponents alloc]init];
    [dateComponents setDay:days];
    return [calendar dateByAddingComponents:dateComponents toDate:date options:0];
}


#pragma mark - views
+ (void) setView:(UIView*) bottom atBottomOfView:(UIView*) top andAddAsSubview:(bool) add
{
    CGRect frame = bottom.frame;
    frame.origin.y = top.frame.size.height;
    bottom.frame = frame;
    
    if(add == YES)
    {
        [top addSubview:bottom];
    }
    
}

#pragma mark - animations
+ (void)slideView:(UIView*)view toY:(CGFloat)yEnd completion:(void(^)())completion {
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = view.frame;
        frame.origin.y = yEnd;
        view.frame = frame;
    } completion:^(BOOL finished) {
        if (!finished) {
            CGRect frame = view.frame;
            frame.origin.y = yEnd;
            view.frame = frame;
        }
        if (completion != nil) {
            completion();
        }
    }];
}

#pragma mark - alerts
+ (void) displayStatusErrorDialog
{
    [self displayStandardAlertViewWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertStatusError", nil) andConfirmButtonTitle:NSLocalizedString(@"alertOk", nil)];
}

+ (void) displayStandardAlertViewWithTitle:(NSString*) title message:(NSString*) message andConfirmButtonTitle:(NSString*) buttonTitle
{
    [[[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:buttonTitle otherButtonTitles: nil] show];
}

#pragma mark - strings

+(NSMutableString*) generateRandomString: (int) string_length
{
    NSString *chars = @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    NSMutableString *randomString = [NSMutableString stringWithCapacity: string_length];
    for (int i=0; i<string_length; i++)
    {
        [randomString appendFormat:@"%C", [chars characterAtIndex: arc4random()%[chars length]]];
    }
    return randomString;
}

+(NSString*) getRelationCategoryNameForKey:(NSString*) key
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"RelationCategoryDictionary" ofType:@"plist"];
    NSMutableArray *categories = [[NSMutableArray alloc] initWithContentsOfFile:path];
    
    for(NSDictionary *category in categories)
    {
        NSString *cat = [category objectForKey:@"category"];
        if([cat isEqualToString:key])
        {
            return [category objectForKey:@"name"];
        }
    }
    
    return nil;
}

+(NSString*) getFormattedPriceForCents:(int) cents andAddCurrency:(BOOL) addCurrency
{
    float price = (float)cents / 100.0;
    
    return addCurrency == YES ? [NSString stringWithFormat:@"%.2f pln", price] : [NSString stringWithFormat:@"%.2f", price];
}

+ (NSString*) base64StringFromString:(NSString*) stringToConvert
{
    char *lookup = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    
    NSMutableString *convertedString = [NSMutableString new];
    NSMutableString *binaryString = [NSMutableString new];
    const char* stringToConvertAscii = [stringToConvert UTF8String];
    for (int i=0; i<strlen(stringToConvertAscii); i++) {
        int AsciiChar = stringToConvertAscii[i];
        char eightBitRepresentation[8];
        for (int j=0; j<8; j++) {
            eightBitRepresentation[7-j] = AsciiChar % 2 ? 0x31 : 0x30;
            AsciiChar /= 2;
        }
        [binaryString appendFormat:@"%s",eightBitRepresentation];
    }
    while ([binaryString length] % 6 != 0) {
        [binaryString appendString:@"0"];
    }
    int count = 0;
    char sixBitRepresentation[6];
    for (NSUInteger i=0; i < [binaryString length]; i++) {
        sixBitRepresentation[count++] = [binaryString characterAtIndex:i] == '1' ? 1 : 0;
        if (count==6) {
            count=0;
            int myVal=0;
            for (int k=0; k<6; k++) {
                myVal += sixBitRepresentation[k]*pow(2,5-k);
                sixBitRepresentation[k]=0;
            }
            [convertedString appendFormat:@"%c",lookup[myVal]];
        }
    }
    
    return convertedString;
}

+ (NSData *)base64DataFromString: (NSString *)string
{
    unsigned long ixtext, lentext;
    unsigned char ch, inbuf[4], outbuf[3];
    short i, ixinbuf;
    Boolean flignore, flendtext = false;
    const unsigned char *tempcstring;
    NSMutableData *theData;
    
    if (string == nil)
    {
        return [NSData data];
    }
    
    ixtext = 0;
    
    tempcstring = (const unsigned char *)[string UTF8String];
    
    lentext = [string length];
    
    theData = [NSMutableData dataWithCapacity: lentext];
    
    ixinbuf = 0;
    
    while (true)
    {
        if (ixtext >= lentext)
        {
            break;
        }
        
        ch = tempcstring [ixtext++];
        
        flignore = false;
        
        if ((ch >= 'A') && (ch <= 'Z'))
        {
            ch = ch - 'A';
        }
        else if ((ch >= 'a') && (ch <= 'z'))
        {
            ch = ch - 'a' + 26;
        }
        else if ((ch >= '0') && (ch <= '9'))
        {
            ch = ch - '0' + 52;
        }
        else if (ch == '+')
        {
            ch = 62;
        }
        else if (ch == '=')
        {
            flendtext = true;
        }
        else if (ch == '/')
        {
            ch = 63;
        }
        else
        {
            flignore = true;
        }
        
        if (!flignore)
        {
            short ctcharsinbuf = 3;
            Boolean flbreak = false;
            
            if (flendtext)
            {
                if (ixinbuf == 0)
                {
                    break;
                }
                
                if ((ixinbuf == 1) || (ixinbuf == 2))
                {
                    ctcharsinbuf = 1;
                }
                else
                {
                    ctcharsinbuf = 2;
                }
                
                ixinbuf = 3;
                
                flbreak = true;
            }
            
            inbuf [ixinbuf++] = ch;
            
            if (ixinbuf == 4)
            {
                ixinbuf = 0;
                
                outbuf[0] = (inbuf[0] << 2) | ((inbuf[1] & 0x30) >> 4);
                outbuf[1] = ((inbuf[1] & 0x0F) << 4) | ((inbuf[2] & 0x3C) >> 2);
                outbuf[2] = ((inbuf[2] & 0x03) << 6) | (inbuf[3] & 0x3F);
                
                for (i = 0; i < ctcharsinbuf; i++)
                {
                    [theData appendBytes: &outbuf[i] length: 1];
                }
            }
            
            if (flbreak)
            {
                break;
            }
        }
    }
    
    return theData;
}

+ (NSString*)createMD5fromString:(NSString*) text
{
    // Create pointer to the string as UTF8
    const char *ptr = [text UTF8String];
    
    // Create byte array of unsigned chars
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    
    // Create 16 byte MD5 hash value, store in buffer
    CC_MD5(ptr, strlen(ptr), md5Buffer);
    
    // Convert MD5 value in the buffer to NSString of hex values
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x",md5Buffer[i]];
    
    return output;
}

+ (NSString *)md5StringFromData:(NSData *)data
{
    void *cData = malloc([data length]);
    unsigned char resultCString[16];
    [data getBytes:cData length:[data length]];
    
    CC_MD5(cData, [data length], resultCString);
    free(cData);
    
    NSString *result = [NSString stringWithFormat:
                        @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
                        resultCString[0], resultCString[1], resultCString[2], resultCString[3],
                        resultCString[4], resultCString[5], resultCString[6], resultCString[7],
                        resultCString[8], resultCString[9], resultCString[10], resultCString[11],
                        resultCString[12], resultCString[13], resultCString[14], resultCString[15]
                        ];
    return result;
}

+ (bool) verifiedIdNo:(NSString*) no withIdType:(NSString*) idType
{
    if(no == nil || no.length == 0)
    {
        return NO;
    }
    
    if([idType.lowercaseString isEqualToString:@"dowód osobisty"])
    {
        return [PRUtils verifyUserIdWithNumber:no];
    }
    else if([idType.lowercaseString isEqualToString:@"regiokarta"])
    {
        return [PRUtils verifyRegioCardWithNo:no];
    }
    
    return YES;
}

+ (bool) verifyUserIdWithNumber:(NSString*) no
{
    //preverify
    if(no.length!=9)
    {
        return NO;
    }
    
    for(int i=0; i<no.length; i++)
    {
        unichar c = [no characterAtIndex:i];
        if(i<3)
        {
            if([PRUtils characterIsLetter:c] == NO)
            {
                return NO;
            }
        }
        else
        {
            if([PRUtils characterIsDigit:c] == NO)
            {
                return NO;
            }
        }
    }
    
    //formal analisis
    int weights[] = {7,3,1,9,7,3,1,7,3};
    int sum = 0;
    
    no = no.uppercaseString;
    
    for (int i=0; i<no.length; i++) {
        unichar c = [no characterAtIndex:i];
        if([PRUtils characterIsDigit:c] == YES)
        {
            c-=0x30;
        }
        else if ([PRUtils characterIsLetter:c] == YES)
        {
            c-=0x41;
        }
        
        sum += (c*weights[i]);
    }
    
    if(sum%10 == 0)
    {
        NSLog(@"POPRAWNY nr dowodu tozsamosci");
        return YES;
    }
    
    return NO;
}

+ (bool) verifyRegioCardWithNo:(NSString*) no
{
//    numery 8 cyfrowe złożone wyłącznie z cyfr
//    numery 9 cyfrowe, w których pierwszy znak jest literą, a następnie wyłącznie cyfry (8).
    
    if(no.length == 8)
    {
        for (int i=0; i<no.length; i++) {
            unichar c = [no characterAtIndex:i];
            
            if([PRUtils characterIsDigit:c] == NO)
            {
                return NO;
            }
            
        }
        return YES;
    }
    else if (no.length == 9)
    {
        for (int i=0; i<no.length; i++)
        {
            unichar c = [no characterAtIndex:i];
            if(i==0)
            {
                if([PRUtils characterIsLetter:c] == NO)
                {
                    return NO;
                }
            }
            else
            {
                if([PRUtils characterIsDigit:c] == NO)
                {
                    return NO;
                }
            }
        }
        return YES;
    }
    
    return NO;
}
+ (bool) characterIsDigit:(unichar) c
{
    return (c>=0x30 && c<=0x39);
}

+ (bool) characterIsLetter:(unichar) c
{
    return ((c>=0x41 && c<=0x5a) || (c>=0x61 && c<=0x7a));
}

@end
