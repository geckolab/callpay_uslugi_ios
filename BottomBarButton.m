//
//  BottomBarButton.m
//  CallPay
//
//  Created by Jacek on 08.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "BottomBarButton.h"

@implementation BottomBarButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setup];
    }
    return self;
}
- (id)init
{
    self = [super init];
    if (self)
    {
        [self setFrame:CGRectMake(0, 0, 94, 50)];
        [self setup];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self setup];
    }
    return self;
}

-(void)setup
{
    [self setTitle:@"" forState:UIControlStateNormal];
    [self.titleLabel setFont:[UIFont fontWithName:@"Swis721LtEU" size:10]];
    [self.titleLabel setTextAlignment:NSTextAlignmentCenter];
//    [self.titleLabel setTextColor:[UIColor colorWithRed:183/255.0 green:212/255.0 blue:51/255.0 alpha:1.0]];
    [self setTitleColor:[UIColor colorWithRed:183/255.0 green:212/255.0 blue:51/255.0 alpha:1.0] forState:UIControlStateNormal];
//    self.backgroundColor = [UIColor redColor];
}
- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    if ([self.currentTitle isEqualToString:@""])
    {
        return CGRectMake((self.frame.size.width - self.currentImage.size.width) / 2, (self.frame.size.height - self.currentImage.size.height) / 2, self.currentImage.size.width, self.currentImage.size.height);
    }
    else
    {
        return CGRectMake((self.frame.size.width - self.currentImage.size.width) / 2, 5, self.currentImage.size.width, self.currentImage.size.height);
    }
}
- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    if ([self.currentTitle isEqualToString:@""])
    {
        return CGRectMake(0, 0, 0, 0);
    }
    else
    {
        return CGRectMake(0, self.frame.size.height - 15, self.frame.size.width, 12);
    }
}

@end
