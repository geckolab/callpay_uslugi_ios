//
//  PRAlertView.m
//  pr
//
//  Created by Marcin Szulc on 17.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PRAlertView.h"

@interface PRAlertView()
{
    SEL alertSelector;
    id alertParent;
    UIAlertView *alertView;
}

@end

@implementation PRAlertView

- (id) initWithTitle:(NSString *)title message:(NSString *)message withParent:(id)parent callbackSelector:(SEL) selector cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ...
{
    self = [super init];
    
    if(self)
    {
        alertParent = parent;
        alertSelector = selector;
        alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:cancelButtonTitle otherButtonTitles:otherButtonTitles, nil];
    }
    
    return self;
    
}

- (void) show
{
    [alertView show];
}

#pragma mark - alert view protocol
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if([alertParent respondsToSelector:alertSelector])
    {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [alertParent performSelector:alertSelector withObject:[NSNumber numberWithInt:buttonIndex]];
#pragma clang diagnostic pop
    }
}

@end
