//
//  PRCardsTableCell.h
//  pr
//
//  Created by Alexander Pimenov on 24.06.13.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRCardsTableCell : UITableViewCell

@property (nonatomic) NSString *cardNumber;
@property (nonatomic) NSString *expirationDate;
@property (nonatomic) BOOL verified;

@property (nonatomic, getter = isActive) BOOL active;

@end
