//
//  PRDatePickerViewController.h
//  pr
//
//  Created by Marcin Szulc on 16.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PRDatePickerProtocol <NSObject>

@optional
- (void) dateSet:(NSDate*) date;
- (void) dateSetCancelled;

@end

@interface PRDatePickerViewController : UIViewController

@property (strong, nonatomic) NSDate *maxDate;
@property (strong, nonatomic) NSDate *currentDate;
@property (strong, nonatomic) NSDate *minDate;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttonsCollection;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (strong, nonatomic) id delegate;

- (IBAction)buttonCancelOnClick:(id)sender;
- (IBAction)buttonOkOnClick:(id)sender;

@end
