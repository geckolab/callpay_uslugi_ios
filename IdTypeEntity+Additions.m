//
//  IdTypeEntity+Additions.m
//  pr
//
//  Created by Marcin Szulc on 21.02.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "IdTypeEntity+Additions.h"

@implementation IdTypeEntity (Additions)

+ (IdTypeEntity*) insertIdTypeWithId:(NSNumber*) index andName:(NSString*) name withManagedObjectContext:(NSManagedObjectContext*) managedObjectContext
{
    IdTypeEntity *idTypeEntity = [NSEntityDescription insertNewObjectForEntityForName:@"IdTypeEntity" inManagedObjectContext:managedObjectContext];
    
    idTypeEntity.idTypeId = index;
    idTypeEntity.idTypeName = name;
    
    return idTypeEntity;
}

+ (void) clearAllRecordsWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext
{
    NSArray *idTypes = [self fetchAllIdTypesWithManagedObjectContext:managedObjectContext];
    
    for(IdTypeEntity *discount in idTypes)
    {
        [managedObjectContext deleteObject:discount];
    }
}

+ (NSArray*) fetchAllIdTypesWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"IdTypeEntity"];
    [request setSortDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"idTypeId" ascending:YES]]];
    return [managedObjectContext executeFetchRequest:request error:nil];
}

+ (IdTypeEntity*) fetchIdTypeWithId:(NSNumber*) index withManagedObjectContext:(NSManagedObjectContext*) managedObjectContext
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"IdTypeEntity"];
    [request setPredicate:[NSPredicate predicateWithFormat:@"idTypeId = %@", index]];
    
    NSArray *result = [managedObjectContext executeFetchRequest:request error:nil];
    
    if(result == nil || result.count == 0)
    {
        return nil;
    }
    return [result objectAtIndex:0];
}

@end
