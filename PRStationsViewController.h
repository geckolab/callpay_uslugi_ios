//
//  PRStationsViewController.h
//  pr
//
//  Created by Marcin Szulc on 16.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PRStationCell.h"

@class StationEntity;
@class PROrderObject;

@interface PRStationsViewController : ProjectViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITextField *textStation;
@property (strong, nonatomic) IBOutlet UITableView *stationsTable;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *progressIndicator;

@property (strong, nonatomic) PROrderObject *orderObject;
@property (strong, nonatomic) StationEntity *stationToGet;

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;

- (IBAction)searchTextChanged:(id)sender;
- (IBAction)textDidEndOnExit:(id)sender;

@end
