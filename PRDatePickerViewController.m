//
//  PRDatePickerViewController.m
//  pr
//
//  Created by Marcin Szulc on 16.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PRDatePickerViewController.h"

@interface PRDatePickerViewController ()

@end

@implementation PRDatePickerViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.0]];
    
    if(_maxDate!=nil) [_datePicker setMaximumDate:_maxDate];
    if(_minDate!=nil) [_datePicker setMinimumDate:_minDate];
    if(_currentDate!=nil) [_datePicker setDate:_currentDate];
}

- (void)setMaxDate:(NSDate *)maxDate
{
    _maxDate = maxDate;
    [_datePicker setMaximumDate:_maxDate];
}

- (void)setMinDate:(NSDate *)minDate
{
    _minDate = minDate;
    [_datePicker setMinimumDate:_minDate];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonCancelOnClick:(id)sender {
    if([_delegate respondsToSelector:@selector(dateSetCancelled)])
    {
        [_delegate dateSetCancelled];
    }
}

- (IBAction)buttonOkOnClick:(id)sender {
    if ([_delegate respondsToSelector:@selector(dateSet:)]) {
        [_delegate dateSet:_datePicker.date];
    }
}
@end
