//
//  BottomBarView.h
//  CallPay
//
//  Created by Jacek on 08.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BottomBarButton.h"

@interface BottomBarView : UIView
{
    UIImageView *bgView;
    NSArray *buttons;
}

@property (nonatomic, strong) BottomBarButton *settingsButton;
@property (nonatomic, strong) BottomBarButton *historyButton;
@property (nonatomic, strong) BottomBarButton *activeButton;

-(void)setBackgroundImage:(UIImage*)image;
-(void)setup;

@end
