//
//  PRCardsContainer.h
//  pr
//
//  Created by Alexander Pimenov on 24.06.13.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PRCardObject;


@interface PRCardsContainer : NSObject

- (void)addCard:(PRCardObject *)card;

- (BOOL)containsDefaultCard;
- (BOOL)containsOtherCards;

- (PRCardObject *)defaultCard;
- (PRCardObject *)otherCardAtIndex:(NSUInteger)index;

- (void)removeAllCards;

- (NSUInteger)count;

@end