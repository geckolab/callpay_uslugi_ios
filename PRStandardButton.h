//
//  PRStandardButton.h
//  pr
//
//  Created by Marcin Szulc on 18.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRStandardButton : UIButton

@property (nonatomic, assign) BOOL lockedForResponse;

- (void) setTitle:(NSString *)title;

@end
