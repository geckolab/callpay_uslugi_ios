//
//  CityActiveStatusView.m
//  CallPay
//
//  Created by Jacek on 03.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "CityActiveStatusView.h"

@implementation CityActiveStatusView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [accessoryImageView setFrame:CGRectMake((self.frame.size.width - accessoryImageView.frame.size.width) / 2, (self.frame.size.height - accessoryImageView.frame.size.height) / 2, accessoryImageView.frame.size.width, accessoryImageView.frame.size.height)];
    [accessoryImageView setHidden:!statusActive];
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setup];
    }
    return self;
}
- (id)init
{
    self = [super init];
    if (self)
    {
        [self setFrame:CGRectMake(0, 0, 19, 19)];
        [self setup];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self setup];
    }
    return self;
}
-(void)setup
{
    self.backgroundColor = [UIColor clearColor];
    accessoryImage = [UIImage imageNamed:@"new_icon_yes"];
    accessoryImageView = [[UIImageView alloc] initWithImage:accessoryImage];
    [self addSubview:accessoryImageView];
    [self setActive:YES];
}
-(void)setActive:(BOOL)active
{
    statusActive = active;
    [self setNeedsDisplay];
}

@end
