//
//  PRRelationBackDateViewController.h
//  pr
//
//  Created by Marcin Szulc on 23.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PRStandardControl;
@class PRStandardButton;

@interface PRRelationBackDateViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *labelBackDate;

@property (strong, nonatomic) IBOutlet PRStandardControl *controlDate;
@property (strong, nonatomic) IBOutlet PRStandardButton *buttonSearch;


@end
