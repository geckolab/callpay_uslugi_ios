//
//  PRPassengerDetailsViewController.m
//  pr
//
//  Created by Marcin Szulc on 23.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PRPassengerDetailsViewController.h"
#import "PRStandardButton.h"
#import "PROrderObject.h"
#import "SettingsEntity+Additions.h"
//#import "PRAppDelegate.h"
#import "PRAsyncConnection.h"
#import "PRDiscountsViewController.h"
#import "DiscountEntity+Additions.h"
#import "PRAlertView.h"
#import "PRUtils.h"
#import "PRPinViewController.h"
#import "TicketEntity+Additions.h"
#import "PRSummaryViewController.h"
#import "TransactionEntity+Additons.h"
#import "IdTypeEntity+Additions.h"

#import "PRDBManager.h"

#import "XLog.h"

#import "PRCardObject.h"

static NSString * const LogTag = @"PassengerDetailsViewController";

@interface PRPassengerDetailsViewController ()
{
@private
    bool wasPinControllerShowed;
}

@property (strong, nonatomic) NSArray *idTypes;
@property (strong, nonatomic) PRAlertView *alert;
@property (strong, nonatomic) PRPinViewController *pinController;
@property (strong, nonatomic) IBOutlet UIView *viewScrollViewContent;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *viewOfferReturn;
@property (strong, nonatomic) IBOutlet UIView *viewOfferTo;
@property (strong, nonatomic) IBOutlet UILabel *textOfferTo;
@property (strong, nonatomic) IBOutlet UILabel *textOfferReturn;
@property (strong, nonatomic) IBOutlet UIView *viewBuy;

@end

@implementation PRPassengerDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UIBarButtonItem * mcmLogo = [[UIBarButtonItem alloc] initWithCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mcm_icon"]]];
    self.navigationItem.rightBarButtonItem = mcmLogo;
    
//    PRAppDelegate *appDelegate = (PRAppDelegate*)[UIApplication sharedApplication].delegate;
    
    _idTypes = [IdTypeEntity fetchAllIdTypesWithManagedObjectContext:[[PRDBManager object]managedObjectContext]];
    
    wasPinControllerShowed = NO;
    
//    [_scrollView setContentSize:_viewScrollViewContent.frame.size];
    [self.scrollView setContentSize:_viewScrollViewContent.frame.size];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTextName:nil];
    [self setButtonIdType:nil];
    [self setTextIdNo:nil];
    [self setLabelDiscount:nil];
    [self setLabelPrice:nil];
    [self setButtonBuyTicket:nil];
    [self setIndicatorPrice:nil];
    [self setTextSurname:nil];
    [self setSwitchRegioCard:nil];
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    PRAppDelegate *appDelegate = (PRAppDelegate*)[UIApplication sharedApplication].delegate;
    
    SettingsEntity *settings = [SettingsEntity fetchSettingsObjectWithManagedObjectContext:[[PRDBManager object]managedObjectContext]];
    
    _textName.text = settings.userName;
    _textSurname.text = settings.userSurname;
    _textIdNo.text = settings.userIdNo;
    
    [_switchRegioCard setOn:settings.userHasRegioCard.boolValue];
    
    if(settings.userIdType == nil)
    {
        settings.userIdType = ((IdTypeEntity*)[_idTypes objectAtIndex:0]).idTypeId;
        
        NSError *error;
        if([[[PRDBManager object]managedObjectContext] save:&error] == NO)
        {
            XLogTag(LogTag, @"%@", error.localizedDescription);
            return;
        }
    }

    _orderObject.passengerIdType = [IdTypeEntity fetchIdTypeWithId:settings.userIdType withManagedObjectContext:[[PRDBManager object]managedObjectContext]];
    
    [_buttonIdType setTitle:_orderObject.passengerIdType.idTypeName];
    
    DiscountEntity *discount = _orderObject.discount == nil ? [[DiscountEntity fetchAllDiscountsWithManagedObjectContext:[[PRDBManager object]managedObjectContext] forClass:_orderObject.vehicleClass] objectAtIndex:0] : _orderObject.discount;
    
    _labelDiscount.text = discount.discountName;
    
    _orderObject.passengerPhone = settings.userPhoneNo;
    _orderObject.passengerHasRegioCard = settings.userHasRegioCard.boolValue;
    _orderObject.passengerName = settings.userName;
    _orderObject.passengerSurname = settings.userSurname;
    _orderObject.discount = discount;
    //TODO: fix
    _orderObject.passengerIdNo = (settings.userIdNo == nil || [settings.userIdNo isEqualToString:@""]) ? @"aaa" : settings.userIdNo;
    
    [self enableOrDisableBuyTicketButton];
    
    [self askForPrice];
    
    if(settings.userAcceptedIdWarning.boolValue == NO)
    {
//        _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertIdWarning", nil) withParent:self callbackSelector:@selector(userAcceptedIdWarning:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
//        [_alert show];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertIdWarning", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil), nil];
        [alert show];
        
        settings.userAcceptedIdWarning = [NSNumber numberWithBool:YES];
        
        NSError *error;
        if([[[PRDBManager object]managedObjectContext] save:&error] == NO)
        {
            XLogTag(LogTag, @"%@", error.localizedDescription);
            return;
        }
    }
    
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"showDiscounts"])
    {
        PRDiscountsViewController *destController = segue.destinationViewController;
        [destController setOrderObject:_orderObject];
    }
    else if([segue.identifier isEqualToString:@"showPin"])
    {
        wasPinControllerShowed = YES;
        _pinController = segue.destinationViewController;
        [_pinController setOrderObject:_orderObject];
        [_pinController setDefaultCard:_defaultCard];
    }
    else if([segue.identifier isEqualToString:@"showSummary"])
    {
    }
}

#pragma mark - private

- (void) userAcceptedIdWarning:(NSInteger) buttonIndex
{
//    PRAppDelegate *appDelegate = (PRAppDelegate*)[UIApplication sharedApplication].delegate;
    SettingsEntity *settings = [SettingsEntity fetchSettingsObjectWithManagedObjectContext:[[PRDBManager object]managedObjectContext]];
    
    settings.userAcceptedIdWarning = [NSNumber numberWithBool:YES];
    
    NSError *error;
    
    if([[[PRDBManager object]managedObjectContext] save:&error] == NO)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        return;
    }
    
}

- (void) askForPrice
{
//    if(wasPinControllerShowed == YES)
//    {
//        wasPinControllerShowed = NO;
//        return;
//    }
    
    _viewOfferTo.hidden = YES;
    _viewOfferReturn.hidden = YES;
    
    _indicatorPrice.hidden = NO;
    _labelPrice.hidden = YES;
    
    [self enableOrDisableBuyTicketButton];

    if (_defaultCard && [_defaultCard isActive])
    {
        [_orderObject setCardID:[_defaultCard identifier]];
        
        PRAsyncConnection *connection = [[PRAsyncConnection alloc] initWithParent:self successSelector:@selector(connectionSuccess:) didFailSelector:@selector(connectionFailed:)];
        [connection sendStartJourneyRequestWithOrderObject:_orderObject];
    }
    else
    {
        [PRUtils displayStandardAlertViewWithTitle:NSLocalizedString(@"alertAttention", nil)
                                           message:@"Aby dokonać zakupu biletu musisz mieć zarejestrowaną i aktywną kartę płatniczą"
                             andConfirmButtonTitle:NSLocalizedString(@"alertOk", nil)];
    }
}

- (void) enableOrDisableBuyTicketButton
{
    if(_textIdNo.text.length == 0 || _textName.text.length == 0 || _textSurname.text.length == 0 || _indicatorPrice.hidden == NO)
    {
        
        
        _buttonBuyTicket.enabled = NO;
    }
    else
    {
        _buttonBuyTicket.enabled = YES;
    }
}

- (void) buyTicket
{
    [_buttonBuyTicket setLockedForResponse:YES];
    
    PRAsyncConnection *connection = [[PRAsyncConnection alloc] initWithParent:self successSelector:@selector(connectionBuyTicketSuccess:) didFailSelector:@selector(connectionBuyTicketFailed:)];
    [connection sendAuthPaymentRequestWithOrderObject:_orderObject];
}

- (void) parseBuyTicketResponse:(NSDictionary*) response
{
//    PRAppDelegate *appDelegate = (PRAppDelegate*)[UIApplication sharedApplication].delegate;
    
    NSNumber *transactionId = [response objectForKey:@"id"];
    NSNumber *transactionJourneyTo = [response objectForKey:@"journey_to"];
    NSNumber *transactionJourneyReturn = [response objectForKey:@"journey_return"];
    NSNumber *transactionValue = [response objectForKey:@"value"];
    NSString *transactionTimestamp = [response objectForKey:@"transaction_timestamp"];
    
    NSArray *tickets = [response objectForKey:@"tickets"];
    
    NSMutableArray *transactionTickets = [[NSMutableArray alloc] init];
    
    for(NSDictionary *ticket in tickets)
    {
        if([ticket isEqual:@""])
        {
            XLogTag(LogTag, @"empty list parameter");
            continue;
        }
        
        TicketEntity *transactionTicket = [TicketEntity insertTicketEntityWithValuesFromJsonResponseTicket:[ticket objectForKey:@"ticket"] withManagedObjectContext:[[PRDBManager object]managedObjectContext]];
        [transactionTickets addObject:transactionTicket];
        
    }
    
    [TransactionEntity insertTransactionEntityWithId:transactionId to:transactionJourneyTo treturn:transactionJourneyReturn value:transactionValue timestamp:transactionTimestamp andTickets:[NSSet setWithArray:transactionTickets] withManagedObjectContext:[[PRDBManager object]managedObjectContext]];
    
    NSError *error;
    
    if([[[PRDBManager object]managedObjectContext] save:&error] == NO)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        return;
    }
    
    [self performSegueWithIdentifier:@"showSummary" sender:self];
}

#pragma mark - buttons

- (IBAction)buttonIdTypeOnClick:(id)sender {
//    PRAppDelegate *appDelegate = (PRAppDelegate*)[UIApplication sharedApplication].delegate;
    IdTypeEntity *idType = _orderObject.passengerIdType;
    idType.idTypeNumber = _textIdNo.text;
    
    NSError *error;
    if([[[PRDBManager object]managedObjectContext] save:&error] == NO)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        return;
    }

    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"dokumentTozsamosci", nil) delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles: nil];
    for(IdTypeEntity *idType in _idTypes)
    {
        [sheet addButtonWithTitle:idType.idTypeName];
    }
    [sheet showFromTabBar:self.tabBarController.tabBar];
}

- (IBAction)controlDiscountOnClick:(id)sender {
    [self performSegueWithIdentifier:@"showDiscounts" sender:self];
}

- (IBAction)buttonBuyTicketOnClick:(id)sender {
    
    if([PRUtils verifiedIdNo:_textIdNo.text withIdType:_orderObject.passengerIdType.idTypeName] == NO)
    {
        if([_orderObject.passengerIdType.idTypeName.lowercaseString isEqualToString:@"regiokarta"] || [_orderObject.passengerIdType.idTypeName.lowercaseString isEqualToString:@"dowód osobisty"])
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:[NSString stringWithFormat:NSLocalizedString(@"alertWrongSerialNo", nil), _orderObject.passengerIdType.idTypeName] withParent:self callbackSelector:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
        else
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:[NSString stringWithFormat:NSLocalizedString(@"alertNoSerialNo", nil), _orderObject.passengerIdType.idTypeName] withParent:self callbackSelector:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
        return;
    }
    
    _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertTicketOwner", nil) withParent:self callbackSelector:@selector(buyTicketAlertSelector) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
    [_alert show];
    
    
}

- (void) buyTicketAlertSelector
{
    if(_orderObject.passengerAuthType == nil || _orderObject.passengerAuthType.length == 0)
    {
        [self buyTicket];
    }
    else
    {
        [self performSegueWithIdentifier:@"showPin" sender:self];
    }
}

- (IBAction)backgroundOnClick:(id)sender {
    [_textIdNo resignFirstResponder];
    [_textName resignFirstResponder];
    [_textSurname resignFirstResponder];
    
    _orderObject.passengerName = _textName.text;
    _orderObject.passengerSurname = _textSurname.text;
    _orderObject.passengerIdNo = _textIdNo.text;
    
//    PRAppDelegate *appDelegate = (PRAppDelegate*)[UIApplication sharedApplication].delegate;
    
    SettingsEntity *settings = [SettingsEntity fetchSettingsObjectWithManagedObjectContext:[[PRDBManager object]managedObjectContext]];
    
    settings.userName = _textName.text;
    settings.userSurname = _textSurname.text;
    settings.userIdNo = _textIdNo.text;
    
    NSError *error;
    if([[[PRDBManager object]managedObjectContext] save:&error] == NO)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        return;
    }
    
    [self askForPrice];
}

- (IBAction)switchRegioCardValueChanged:(id)sender {
//    PRAppDelegate *appDelegate = (PRAppDelegate*)[UIApplication sharedApplication].delegate;
    
    SettingsEntity *settings = [SettingsEntity fetchSettingsObjectWithManagedObjectContext:[[PRDBManager object]managedObjectContext]];
    
    settings.userHasRegioCard = [NSNumber numberWithBool:([_switchRegioCard isOn])];
    
    _orderObject.passengerHasRegioCard = settings.userHasRegioCard.boolValue;
    
    NSError *error;
    if([[[PRDBManager object]managedObjectContext] save:&error] == NO)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        return;
    }
    
    [self askForPrice];
}

#pragma mark - action sheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [_buttonIdType setTitle:((IdTypeEntity*)[_idTypes objectAtIndex:buttonIndex]).idTypeName];
    
//    PRAppDelegate *appDelegate = (PRAppDelegate*)[UIApplication sharedApplication].delegate;
    
    SettingsEntity *settings = [SettingsEntity fetchSettingsObjectWithManagedObjectContext:[[PRDBManager object]managedObjectContext]];
    
    settings.userIdType = ((IdTypeEntity*)[_idTypes objectAtIndex:buttonIndex]).idTypeId;
    
    _orderObject.passengerIdType = [_idTypes objectAtIndex:buttonIndex];
    _orderObject.passengerIdNo = _orderObject.passengerIdType.idTypeNumber;
    _textIdNo.text = _orderObject.passengerIdType.idTypeNumber;
    
    NSError *error;
    if([[[PRDBManager object]managedObjectContext] save:&error] == NO)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        return;
    }
    
    [_textIdNo becomeFirstResponder];
    
    //[self askForPrice];
}

#pragma mark - alert
- (void) connectionFailed
{
    _indicatorPrice.hidden = YES;
//    _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertStatusError", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
//    [_alert show];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertStatusError", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil), nil];
    [alert show];
    
}

- (void) connectionFailedAlertCallback:(NSNumber*) buttonIndex
{
    [_buttonBuyTicket setLockedForResponse:NO];
    _indicatorPrice.hidden = YES;
}

#pragma mark - connections
- (void) connectionBuyTicketSuccess:(PRAsyncConnection*) connection
{
    
    XLogTag(LogTag, @"%@", [[NSString alloc] initWithData:connection.responseData encoding:NSUTF8StringEncoding]);
    
    [_buttonBuyTicket setLockedForResponse:NO];
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:connection.responseData
                          options:kNilOptions
                          error:&error];
    
    if(error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        [self connectionFailed];
        return;
    }
    
    NSDictionary *response = [json objectForKey:@"start_journey_response"];
    
    NSNumber *status = [response objectForKey:@"status"];
    
    if(status==nil)
    {
        [self connectionFailed];
        return;
    }
    
    switch (status.intValue) {
        case STATUS_OK:
        {
            [self parseBuyTicketResponse:response];
        }
            return;
        case STATUS_USER_BLOCKED:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertUserBlocked", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
        case STATUS_NO_MONEY:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertNoMoney", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
        case STATUS_WRONG_PIN:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertWrongPin", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
        case STATUS_NO_RELATION_BETWEEN_STATIONS:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertNoRelation", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
        case STATUS_WRON_PHONE_NO:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertWrongPhoneNo", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;

        case STATUS_INVALID_PARAMETERS:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertInvalidParameters", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
            return;
        }
        case STATUS_WRONG_DATE:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertWrongDate", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
            return;
        }
        case STATUS_CALLPAY_GENERAL_ERROR:
        {
            [self connectionFailed];
        }
            return;
        case STATUS_CANNOT_BUY_TICKET:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertCannotBuyTicket", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
    }

}

- (void) connectionBuyTicketFailed:(PRAsyncConnection*) connection
{
    [_buttonBuyTicket setLockedForResponse:NO];
    [self connectionFailed];
}

- (void) connectionSuccess:(PRAsyncConnection*) connection
{
    XLogTag(LogTag, @"%@", [[NSString alloc] initWithData:connection.responseData encoding:NSUTF8StringEncoding]);
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:connection.responseData
                          options:kNilOptions
                          error:&error];
    
    if(error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        [self connectionFailed];
        return;
    }
    
    NSDictionary *response = [json objectForKey:@"auth_payment_challenge"];
    
    NSNumber *status = [response objectForKey:@"status"];
    
    if(status==nil)
    {
        [self connectionFailed];
        [self enableOrDisableBuyTicketButton];
        return;
    }
    
    switch (status.intValue) {
        case STATUS_OK:
        {
            _labelPrice.text = [PRUtils getFormattedPriceForCents:((NSNumber*)[response objectForKey:@"value"]).intValue andAddCurrency:YES];

            _orderObject.passengerAuthType = [response objectForKey:@"auth_type"];
            _orderObject.passengerTransactionToken = [response objectForKey:@"transaction_token"];

            _indicatorPrice.hidden = YES;
            _labelPrice.hidden = NO;
            
            _textOfferTo.text = [response objectForKey:@"offer_to"];
            _textOfferReturn.text = [response objectForKey:@"offer_return"];
            
            [_textOfferTo sizeToFit];
            [_textOfferReturn sizeToFit];
            
            CGRect frame = _textOfferTo.frame;
            int x = _viewOfferTo.frame.size.width - frame.size.width;
            frame.origin.x = x;
            _textOfferTo.frame = frame;
            
            frame = _viewOfferTo.frame;
            frame.size.height = _textOfferTo.frame.size.height;
            _viewOfferTo.frame = frame;
            
            frame = _textOfferReturn.frame;
            x = _viewOfferReturn.frame.size.width - frame.size.width;
            frame.origin.x = x;
            _textOfferReturn.frame = frame;
            
            if(_textOfferTo.text != nil)
            {
                _viewOfferTo.hidden = NO;
            }
            
            if(_textOfferReturn.text != nil)
            {
                _viewOfferReturn.hidden = NO;
                frame = _viewOfferReturn.frame;
                frame.size.height = _textOfferReturn.frame.size.height;
                frame.origin.y = _viewOfferTo.frame.origin.y + _viewOfferTo.frame.size.height;
                _viewOfferReturn.frame = frame;
            }
            
            [_viewOfferTo sizeToFit];
            [_viewOfferReturn sizeToFit];
            
            if(_viewOfferReturn.hidden == NO)
            {
                frame = _viewOfferReturn.frame;
                const int y = frame.origin.y + frame.size.height;
                frame = _viewBuy.frame;
                frame.origin.y = y;
                _viewBuy.frame = frame;
            }
            else
            {
                CGRect frame = _viewOfferTo.frame;
                const int y = frame.origin.y + frame.size.height;
                frame = _viewBuy.frame;
                frame.origin.y = y;
                _viewBuy.frame = frame;
            }
            
            frame = _viewScrollViewContent.frame;
            frame.size.height = _viewBuy.frame.origin.y + _viewBuy.frame.size.height + 20;
            _viewScrollViewContent.frame = frame;
//            [_scrollView setContentSize:_viewScrollViewContent.frame.size];
            [self.scrollView setContentSize:_viewScrollViewContent.frame.size];
            
        }
            break;
        case STATUS_USER_BLOCKED:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertUserBlocked", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
        case STATUS_INVALID_PARAMETERS:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertInvalidParameters", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
        case STATUS_WRONG_DATE:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertWrongDate", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
            return;
        }
        case STATUS_NO_RELATION_BETWEEN_STATIONS:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertNoRelationBetweenStations", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
            return;
        }
        case STATUS_NO_MONEY:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertNoMoney", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
        case STATUS_WRON_PHONE_NO:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertWrongPhoneNo", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
        case STATUS_CALLPAY_GENERAL_ERROR:
        {
            [self connectionFailed];
        }
            return;
        case STATUS_CANNOT_BUY_TICKET:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertCannotBuyTicket", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
            
    }
    
    [self enableOrDisableBuyTicketButton];

}

- (void) connectionFailed:(PRAsyncConnection*) connection
{
    [self connectionFailed];
    [self enableOrDisableBuyTicketButton];
}


@end
