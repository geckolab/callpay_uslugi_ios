//
//  CPWsdl.h
//  CallPay
//
//  Created by Jacek on 23.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "SoapService.h"
//#import <SOAPEngine64/SOAPEngine>
#import <SOAPEngine64/SOAPEngine.h>

@interface CPWsdl : NSObject//SoapService

+ (CPWsdl *) object;

-(void)checkExternalTransfer;
-(void)checkExternalTransferResult;

@end
