//
//  PRVerifyCard2ViewController.h
//  pr
//
//  Created by Marcin Szulc on 02.10.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PRCardObject.h"

@interface PRVerifyCard2ViewController : UIViewController

@property (strong, nonatomic) PRCardObject *card;

@end
