//
//  PRRelationsViewController.h
//  pr
//
//  Created by Marcin Szulc on 21.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PROrderObject;

@interface PRRelationsViewController : ProjectViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) PROrderObject *orderObject;

@property (strong, nonatomic) IBOutlet UITableView *tableRelations;

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;

@end
