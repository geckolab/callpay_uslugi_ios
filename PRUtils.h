//
//  PRUtils.h
//  pr
//
//  Created by Marcin Szulc on 16.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PROrderObject.h"

@interface PRUtils : NSObject

//+ (id) insertStringOrNumberFrom:(id) from To:(id) to;

//date
+ (NSDate*) getDateFromDateString:(NSString*) date withInputFormat:(NSString*) inputFormat;

+ (NSString*) getFormattedDateFromString:(NSString*) date withInputFormat:(NSString*) inputFormat andOutputFormat:(NSString*) outputFormat;

+ (NSString*) getDateForFormat:(NSString*) format fromDate:(NSDate*) date;

+ (NSString*) getTravelTimeForStartHour:(NSString*) startHour andBackHour:(NSString*) backHour;

+ (NSDate*) getDateFromStringTimestamp:(NSString*) date;
+ (NSDate*) getDateFromDateString:(NSString*) date;
+ (NSDate*) resetDateToMidnight:(NSDate*) date;

+ (NSString*) getFormattedDateFromStringTimestamp:(NSString*) date;
+ (NSString*) getFormattedDateFromString:(NSString*) date;
+ (NSString*) getFormattedTimestampFromDate:(NSDate*) date;
+ (NSString*) getFormattedDateFromDateForReturnConnection:(PROrderObject*) order;
+ (NSString*) getFormattedDateFromDateForConnection:(NSDate*) date;
+ (NSString*) getFormattedDateFromDate:(NSDate*) date;

+ (NSDate *)addYears:(NSInteger)years toDate:(NSDate*)date;
+ (NSDate *)addMonths:(NSInteger)months toDate:(NSDate*)date;
+ (NSDate *)addWeeks:(NSInteger)weeks toDate:(NSDate*)date;
+ (NSDate *)addDays:(NSInteger)days toDate:(NSDate*)date;

//views
+ (void) setView:(UIView*) bottom atBottomOfView:(UIView*) top andAddAsSubview:(bool) add;

//animations
+ (void)slideView:(UIView*)view toY:(CGFloat)yEnd completion:(void(^)())completion;

//alerts
+ (void) displayStatusErrorDialog;

+ (void) displayStandardAlertViewWithTitle:(NSString*) title message:(NSString*) message andConfirmButtonTitle:(NSString*) buttonTitle;

//strings
+ (NSMutableString*) generateRandomString: (int) string_length;

+ (NSString*) getRelationCategoryNameForKey:(NSString*) key;

+(NSString*) getFormattedPriceForCents:(int) cents andAddCurrency:(BOOL) addCurrency;

+ (NSString*) base64StringFromString:(NSString*) stringToConvert;

+ (NSData *)base64DataFromString: (NSString *)string;

+ (NSString*)createMD5fromString:(NSString*) text;

+ (NSString *)md5StringFromData:(NSData *)data;

+ (bool) verifiedIdNo:(NSString*) no withIdType:(NSString*) idType;

@end
