//
//  Car+Extensions.m
//  CallPay
//
//  Created by Jacek on 21.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "Car+Extensions.h"

#import "NSManagedObject+GeckoExtensions.h"

#import "CarsManager.h"

@implementation Car (Extensions)

-(void)setDefaultCar:(NSNumber *)defaultCar
{
    [self willChangeValueForKey:@"defaultCar"];
//    if ([defaultCar boolValue])
//    {
//        NSArray *list = [Car findAllOther:self];
//        for (Car __strong *c in list)
//        {
//           // [c setDefaultCar:[NSNumber numberWithBool:NO]];NSLog(@"\n %@ \n",c);
//        }
//    }
//    else
//    {
//        
//    }
    
    [self setPrimitiveValue:defaultCar forKey:@"defaultCar"];
    [self didChangeValueForKey:@"defaultCar"];
}

-(void)markDefault
{
    NSArray *list = [Car findAllOther:self];
    for (Car __strong *c in list)
    {
        [c setDefaultCar:[NSNumber numberWithBool:NO]];NSLog(@"\n %@ \n",c);
    }
    [self setDefaultCar:[NSNumber numberWithBool:YES]];NSLog(@"markDef %@",self);
    
    NSLog(@"\n\n%@\n\n",[Car findAllObjects]);
    [[CarsManager object] saveContext];
}

@end
