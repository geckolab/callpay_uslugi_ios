//
//  TicketsButton.h
//  CallPay
//
//  Created by Jacek on 25.03.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CustomButton.h"

@interface TicketsButton : CustomButton

-(void)setup;

@end
