//
//  ActiveTicketsHeaderView.m
//  CallPay
//
//  Created by Jacek on 30.03.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "ActiveTicketsHeaderView.h"

@implementation ActiveTicketsHeaderView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setup];
    }
    return self;
}
- (id)init
{
    self = [super init];
    if (self)
    {
        [self setFrame:CGRectMake(0, 0, 320, 100)];
        [self setup];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self setup];
    }
    return self;
}
-(void)setup
{//self.backgroundColor = [UIColor yellowColor];
    title = @"";
    iconImg = [UIImage new];
    iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 24)];//titleLabel.backgroundColor = [UIColor redColor];
    titleLabel.adjustsFontSizeToFitWidth = YES;
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setFont:[UIFont fontWithName:@"Swis721LtEU" size:20]];
    [titleLabel setMinimumFontSize:8.0];
    [titleLabel setMinimumScaleFactor:0.5];
    [titleLabel setText:title];
    
    [self addSubview:iconImageView];
    [self addSubview:titleLabel];
}

-(void)setTitle:(NSString *) newTitle
{
    title = newTitle;
    [titleLabel setText:title];
    [self setNeedsLayout];
}
-(void)setIcon:(UIImage *) newIcon
{
    iconImg = newIcon;
    [iconImageView setImage:iconImg];
    [self setNeedsLayout];
}
-(void)setNeedsLayout
{
    [super setNeedsLayout];
}
-(void)layoutSubviews
{
    [iconImageView setFrame:CGRectMake(0, (self.frame.size.height - iconImg.size.height) / 2, iconImg.size.width, iconImg.size.height)];
//    [titleLabel setFrame:CGRectMake(iconImageView.frame.size.width + 10, (self.frame.size.height - titleLabel.frame.size.height) / 2, self.frame.size.width - (iconImageView.frame.size.width + 10), titleLabel.frame.size.height)];
    [titleLabel setFrame:CGRectMake(iconImageView.frame.size.width + 10, (self.frame.size.height - titleLabel.frame.size.height) / 2, self.frame.size.width - (iconImageView.frame.size.width + 10), titleLabel.frame.size.height)];
    if(iconImageView.frame.size.height > 0)
    {
        [titleLabel setFrame:CGRectMake(titleLabel.frame.origin.x, (iconImageView.frame.origin.y + (iconImageView.frame.size.height)) - titleLabel.frame.size.height, titleLabel.frame.size.width, titleLabel.frame.size.height)];
    }
}

@end
