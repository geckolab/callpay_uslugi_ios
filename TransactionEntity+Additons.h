//
//  TransactionEntity+Additons.h
//  pr
//
//  Created by Marcin Szulc on 25.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "TransactionEntity.h"

@interface TransactionEntity (Additons)

+ (TransactionEntity*) insertTransactionEntityWithId:(NSNumber*) tId to: (NSNumber*) tJT treturn: (NSNumber*) tJR value:(NSNumber*) value timestamp:(NSString*) timestamp andTickets:(NSSet*) tickets withManagedObjectContext:(NSManagedObjectContext*) managedObjectContext;

+ (NSArray*) fetchAllTransactionsWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext;

@end
