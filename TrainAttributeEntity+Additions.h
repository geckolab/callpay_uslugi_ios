//
//  TrainAttributeEntity+Additions.h
//  pr
//
//  Created by Marcin Szulc on 28.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "TrainAttributeEntity.h"

@interface TrainAttributeEntity (Additions)

+ (TrainAttributeEntity*) insertTrainAttributeWithId:(NSNumber*) index andName:(NSString*) name andDescription:(NSString*) description withManagedObjectContext:(NSManagedObjectContext*) managedObjectContext;

+ (void) clearAllRecordsWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext;

+ (NSArray*) fetchAllTrainAttributesWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext;

+ (TrainAttributeEntity*) fetchTrainAttributeWithId:(NSNumber*) trainAttributeId withManagedObjectContext:(NSManagedObjectContext*) managedObjectContext;

@end
