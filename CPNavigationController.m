//
//  CPNavigationController.m
//  CallPay
//
//  Created by Jacek on 08.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "CPNavigationController.h"

#import "CPNavigationBar.h"

@interface CPNavigationController ()

@end

@implementation CPNavigationController

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.delegate = self;
        bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        [bgView setImage:[UIImage imageNamed:@"Bg4"]];
        [self.view addSubview:bgView];
        [self.view sendSubviewToBack:bgView];
        self.navigationBar.backgroundColor = [UIColor redColor];
        //        self.navigationBar.delegate = self;
        [self setValue:[[CPNavigationBar alloc] init] forKeyPath:@"navigationBar"];
    }
    return self;
}
- (instancetype)initWithRootViewController:(UIViewController *)rootViewController
{
    if(self = [super initWithRootViewController:rootViewController])
    {
        self.delegate = self;
        bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        [bgView setImage:[UIImage imageNamed:@"Bg4"]];
        [self.view addSubview:bgView];
        [self.view sendSubviewToBack:bgView];
        self.navigationBar.backgroundColor = [UIColor redColor];
//        self.navigationBar.delegate = self;
        [self setValue:[[CPNavigationBar alloc] init] forKeyPath:@"navigationBar"];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar {
    
//    return UIBarPositionTop;
//    return UIBarPositionBottom;
    return UIBarPositionTopAttached;
}

/*
-(UIViewController*)popViewControllerAnimated:(BOOL)animated
{
    [super popViewControllerAnimated:YES];
}

- (void) pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (NO)
    {
        [super pushViewController:viewController animated:YES];
    }
    else
    {
//        [super pushViewController:viewController animated:animated];
        UIViewController *oldViewController = nil;
        if (self.childViewControllers.count > 0) {
            oldViewController = [self.childViewControllers lastObject];
        }
        UIViewController *newViewController = viewController;
        
        newViewController.view.frame = CGRectMake(self.view.bounds.size.width,
                                                  self.navigationBar.bounds.size.height,
                                                  self.view.bounds.size.width,
                                                  self.view.bounds.size.height - self.navigationBar.bounds.size.height);
        
        
        [self addChildViewController:newViewController];
        
//        UINavigationItem *item = [self newNavigationItemForViewController:newViewController previousNavigationItem:navigationBar.topItem];
//        [navigationBar pushNavigationItem:item animated:animated];
        
        if (!oldViewController) {
            [self.view addSubview:newViewController.view];
            [newViewController didMoveToParentViewController:self];
            newViewController.view.center = self.view.center;
            return;
        }
        
        [self transitionFromViewController:oldViewController
                          toViewController:newViewController
                                  duration:(!animated || !oldViewController) ? 0 : 0.35f
                                   options:0
                                animations:^{
                                    newViewController.view.center = self.view.center;
                                    oldViewController.view.center = CGPointMake(oldViewController.view.center.x - self.view.bounds.size.width, oldViewController.view.center.y);
                                }
                                completion:^(BOOL finished){
                                    [newViewController didMoveToParentViewController:self];
                                    [super pushViewController:viewController animated:NO];
                                }];
    }
}
 */
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    UIViewController *top = [self topViewController];
    if ([top isEqual:viewController])
    {
        if ([self.viewControllers count] > 2)
        {
            top = [self.viewControllers objectAtIndex:[self.viewControllers count] - 2];
        }
        else
        {
            top = [self.viewControllers firstObject];
        }
    }
    [UIView animateWithDuration:0.2f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
    top.view.alpha = 0.0;
    viewController.view.alpha = 1.0;
        }completion:^(BOOL finished){
    top.view.alpha = 0.0;
    viewController.view.alpha = 1.0;
        }];
    NSLog(@"%@ to %@",[top class], [viewController class]);
    
}

@end
