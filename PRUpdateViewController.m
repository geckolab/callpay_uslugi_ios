//
//  PRUpdateViewController.m
//  pr
//
//  Created by Marcin Szulc on 18.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PRUpdateViewController.h"

#import "PRAsyncConnection.h"
#import "XLog.h"
//#import "PRAppDelegate.h"
#import "SettingsEntity+Additions.h"
#import "StationEntity+Additions.h"
#import "DiscountEntity+Additions.h"
#import "TrainAttributeEntity+Additions.h"
#import "IdTypeEntity+Additions.h"

#import "PRAlertView.h"

#import "PRDBManager.h"

static NSString * const LogTag = @"UpdateViewController";

@interface PRUpdateViewController ()

@property (strong, nonatomic) PRAlertView *alert;

@property (strong, nonatomic) NSNumber *tableStationsVersion;
@property (strong, nonatomic) NSNumber *tableDiscountsVersion;
@property (strong, nonatomic) NSNumber *tableTrainAttributesVersion;
@property (strong, nonatomic) NSNumber *tableIdTypesVersion;

@property (atomic, assign) bool needToUpdateStations;
@property (atomic, assign) bool needToUpdateDiscounts;
@property (atomic, assign) bool needToUpdateTrainAttributes;
@property (atomic, assign) bool needToUpdateIdTypes;

@end

@implementation PRUpdateViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated
{
    
    _needToUpdateStations = NO;
    _needToUpdateDiscounts = NO;
    _needToUpdateTrainAttributes = NO;
    _needToUpdateIdTypes = NO;
    
//    PRAppDelegate *appDelegate = (PRAppDelegate*)[UIApplication sharedApplication].delegate;
    SettingsEntity *settings = [SettingsEntity fetchSettingsObjectWithManagedObjectContext:[[PRDBManager object] managedObjectContext]];
    
    _tableDiscountsVersion = settings.tableDiscountsVersion;
    _tableStationsVersion = settings.tableStationsVersion;
    _tableTrainAttributesVersion = settings.tableTrainAttributesVersion;
    _tableIdTypesVersion = settings.tableIdTypesVersion;
    
    PRAsyncConnection *connection = [[PRAsyncConnection alloc] initWithParent:self successSelector:@selector(dbVersionCheckSuccess:) didFailSelector:@selector(dbVersionCheckFailed:)];
    [connection sendDBCheckVersionRequestWithPhoneNo:settings.userPhoneNo];
    [super viewDidAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) saveAndDismiss
{
//    PRAppDelegate *appDelegate = (PRAppDelegate*)[UIApplication sharedApplication].delegate;
    SettingsEntity *settings = [SettingsEntity fetchSettingsObjectWithManagedObjectContext:[[PRDBManager object]managedObjectContext]];
    
    settings.tableDiscountsVersion = _tableDiscountsVersion;
    settings.tableStationsVersion = _tableStationsVersion;
    settings.tableTrainAttributesVersion = _tableTrainAttributesVersion;
    settings.tableIdTypesVersion = _tableIdTypesVersion;
    
    settings.lastCheckDate = [NSDate date];
    settings.lastUpdateDate = [NSDate date];
    
    NSError *error;
    if([[[PRDBManager object]managedObjectContext] save:&error] == NO)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        return;
    }

    [[self delegate] updateViewControllerShouldBeDismissed:self];
}

- (void) connectionFailed
{
    _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertStatusError", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
    [_alert show];
}

- (void) connectionFailedAlertCallback:(NSNumber*) buttonIndex
{
//    PRAppDelegate *appDelegate = (PRAppDelegate*)[UIApplication sharedApplication].delegate;
    [[[PRDBManager object]managedObjectContext] rollback];

    [[self delegate] updateViewControllerShouldBeDismissed:self];
}

#pragma mark - connection
- (void) dbVersionCheckSuccess:(PRAsyncConnection*) connection
{
    XLogTag(LogTag, @"%@", [[NSString alloc] initWithData:connection.responseData encoding:NSUTF8StringEncoding]);
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:connection.responseData
                          options:kNilOptions
                          error:&error];
    
    if(error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        [self connectionFailed];
        return;
    }
    
    NSDictionary *response = [json objectForKey:@"db_sync_response"];
    
    NSNumber *status = [response objectForKey:@"status"];
    
    if(status==nil)
    {
        [self connectionFailed];
        return;
    }
    
    switch (status.intValue) {
        case STATUS_OK:
        {
            NSArray *tables = [response objectForKey:@"tables"];
            
            for(NSDictionary *table in tables)
            {
                if([table isEqual:@""])
                {
                    XLogTag(LogTag, @"empty list parameter");
                    continue;
                }
                
                NSString *tableName = [[table objectForKey:@"table"] objectForKey:@"name"];
                NSNumber *tableNumber = [[table objectForKey:@"table"] objectForKey:@"version"];
                
                if([tableName isEqualToString:@"stations"])
                {
                    if(tableNumber.intValue > _tableStationsVersion.intValue)
                    {
                        _needToUpdateStations = YES;
                        _tableStationsVersion = tableNumber;
                    }
                }
                else if([tableName isEqualToString:@"discounts"])
                {
                    if(tableNumber.intValue > _tableDiscountsVersion.intValue)
                    {
                        _needToUpdateDiscounts = YES;
                        _tableDiscountsVersion = tableNumber;
                    }
                }
                else if([tableName isEqualToString:@"train_attributes"])
                {
                    if(tableNumber.intValue > _tableTrainAttributesVersion.intValue)
                    {
                        _needToUpdateTrainAttributes = YES;
                        _tableTrainAttributesVersion = tableNumber;
                    }
                }
                else if([tableName isEqualToString:@"document_types"])
                {
                    if(tableNumber.intValue > _tableIdTypesVersion.intValue)
                    {
                        _needToUpdateIdTypes = YES;
                        _tableIdTypesVersion = tableNumber;
                    }
                }
                
            }
            
        }
            break;
        case STATUS_USER_BLOCKED:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertUserBlocked", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
    }
    
    if(_needToUpdateStations == YES)
    {
        PRAsyncConnection *connection = [[PRAsyncConnection alloc] initWithParent:self successSelector:@selector(stationsDBSuccess:) didFailSelector:@selector(stationsDBFailed:)];
        [connection sendUpdateStationsRequest];
    }
    else if(_needToUpdateDiscounts == YES)
    {
        PRAsyncConnection *connection = [[PRAsyncConnection alloc] initWithParent:self successSelector:@selector(discountsDBSuccess:) didFailSelector:@selector(discountsDBFailed:)];
        [connection sendUpdateDiscountsRequest];
    }
    else if (_needToUpdateTrainAttributes == YES)
    {
        PRAsyncConnection *connection = [[PRAsyncConnection alloc] initWithParent:self successSelector:@selector(trainAttributesDBSuccess:) didFailSelector:@selector(trainAttributesDBFailed:)];
        [connection sendUpdateTrainAttributesRequest];
    }
    else if (_needToUpdateIdTypes == YES)
    {
        PRAsyncConnection *connection = [[PRAsyncConnection alloc] initWithParent:self successSelector:@selector(idTypesDBSuccess:) didFailSelector:@selector(idTypesDBFailed:)];
        [connection sendUpdateIdTypesRequest];
    }
    else
    {
        [[self delegate] updateViewControllerShouldBeDismissed:self];
    }
}

- (void) dbVersionCheckFailed:(PRAsyncConnection*) connection
{
    [self connectionFailed];
}

- (void) idTypesDBSuccess:(PRAsyncConnection*) connection
{
    XLogTag(LogTag, @"%@", [[NSString alloc] initWithData:connection.responseData encoding:NSUTF8StringEncoding]);
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:connection.responseData
                          options:kNilOptions
                          error:&error];
    
    if(error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        [self connectionFailed];
        return;
    }
    
    NSDictionary *response = [json objectForKey:@"document_types_response"];
    
    NSNumber *status = [response objectForKey:@"status"];
    
    if(status==nil)
    {
        [self connectionFailed];
        return;
    }
    
    switch (status.intValue) {
        case STATUS_OK:
        {
//            PRAppDelegate *appDelegate = (PRAppDelegate*)[UIApplication sharedApplication].delegate;
            [IdTypeEntity clearAllRecordsWithManagedObjectContext:[[PRDBManager object]managedObjectContext]];
            
            NSArray *idTypes = [response objectForKey:@"document_types"];
            
            for (NSDictionary *idType in idTypes) {
                NSNumber *idTypeId = [[idType objectForKey:@"document_type"] objectForKey:@"id"];
                NSString *idTypeName = [[idType objectForKey:@"document_type"] objectForKey:@"name"];
                
                [IdTypeEntity insertIdTypeWithId:idTypeId andName:idTypeName withManagedObjectContext:[[PRDBManager object]managedObjectContext]];
            }
            
        }
            break;
        case STATUS_USER_BLOCKED:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertUserBlocked", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
            
    }
    

    [self saveAndDismiss];
    
}

- (void) idTypesDBFailed:(PRAsyncConnection*) connection
{
    [self connectionFailed];
}

- (void) stationsDBSuccess:(PRAsyncConnection*) connection
{
    
    XLogTag(LogTag, @"%@", [[NSString alloc] initWithData:connection.responseData encoding:NSUTF8StringEncoding]);
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:connection.responseData
                          options:kNilOptions
                          error:&error];
    
    if(error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        [self connectionFailed];
        return;
    }

    NSDictionary *response = [json objectForKey:@"stations_response"];
    
    NSNumber *status = [response objectForKey:@"status"];
    
    if(status==nil)
    {
        [self connectionFailed];
        return;
    }
    
    XLogTag(LogTag, @"%@", status);
    
    switch (status.intValue) {
        case STATUS_OK:
        {
//            PRAppDelegate *appDelegate = (PRAppDelegate*)[UIApplication sharedApplication].delegate;
            [StationEntity clearAllRecordsWithManagedObjectContext:[[PRDBManager object]managedObjectContext]];
            
            NSArray *stations = [response objectForKey:@"stations"];
            
            for(NSDictionary *station in stations)
            {
                if([station isEqual:@""])
                {
                    XLogTag(LogTag, @"empty list parameter");
                    continue;
                }
                NSNumber *stationId = [[station objectForKey:@"station"] objectForKey:@"id"];
                
                NSString *stationName = [[station objectForKey:@"station"] objectForKey:@"name"];
               
                [StationEntity insertStationWithId:stationId andName:stationName withManagedObjectContext:[[PRDBManager object]managedObjectContext]];
                
            }
            
        }
            break;
            
        case STATUS_USER_BLOCKED:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertUserBlocked", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
        case STATUS_NO_STATIONS:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertNoStation", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
        case STATUS_WRONG_DATE:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertWrongDate", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
    }
    
    if(_needToUpdateDiscounts == YES)
    {
    //perform discounts sync after success
        PRAsyncConnection *nextConnection = [[PRAsyncConnection alloc] initWithParent:self successSelector:@selector(discountsDBSuccess:) didFailSelector:@selector(discountsDBFailed:)];
        [nextConnection sendUpdateDiscountsRequest];
    }
    else if (_needToUpdateTrainAttributes == YES)
    {
        PRAsyncConnection *connection = [[PRAsyncConnection alloc] initWithParent:self successSelector:@selector(trainAttributesDBSuccess:) didFailSelector:@selector(trainAttributesDBFailed:)];
        [connection sendUpdateTrainAttributesRequest];
    }
    else if (_needToUpdateIdTypes == YES)
    {
        PRAsyncConnection *connection = [[PRAsyncConnection alloc] initWithParent:self successSelector:@selector(idTypesDBSuccess:) didFailSelector:@selector(idTypesDBFailed:)];
        [connection sendUpdateIdTypesRequest];
    }
    else
    {
        [self saveAndDismiss];
    }
}

- (void) stationsDBFailed:(PRAsyncConnection*) connection
{
    [self connectionFailed];
}

- (void) discountsDBSuccess:(PRAsyncConnection*) connection
{
    XLogTag(LogTag, @"%@", [[NSString alloc] initWithData:connection.responseData encoding:NSUTF8StringEncoding]);
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:connection.responseData
                          options:kNilOptions
                          error:&error];
    
    if(error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        [self connectionFailed];
        return;
    }
    
    NSDictionary *response = [json objectForKey:@"discounts_response"];
    
    NSNumber *status = [response objectForKey:@"status"];
    
    if(status==nil)
    {
        [self connectionFailed];
        return;
    }
    
    switch (status.intValue) {
        case STATUS_OK:
        {
//            PRAppDelegate *appDelegate = (PRAppDelegate*)[UIApplication sharedApplication].delegate;
            [DiscountEntity clearAllRecordsWithManagedObjectContext:[[PRDBManager object]managedObjectContext]];
            
            NSArray *discounts = [response objectForKey:@"discounts"];
            
            for (int i=0; i<discounts.count; i++) {
                
                NSDictionary *discount = [discounts objectAtIndex:i];
                
                NSNumber *discountId = [[discount objectForKey:@"discount"] objectForKey:@"id"];
                NSString *discountName = [[discount objectForKey:@"discount"] objectForKey:@"name"];
                NSNumber *discountVehicleClass = [[discount objectForKey:@"discount"] objectForKey:@"vehicle_class"];
                
                NSLog(@"%@", discountName);
                
                [DiscountEntity insertDiscountWithId:discountId andName:discountName andClass:discountVehicleClass andSortIndex:[NSNumber numberWithInt:i] withManagedObjectContext:[[PRDBManager object]managedObjectContext]];
            }
            
        }
            break;
        case STATUS_USER_BLOCKED:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertUserBlocked", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
        
    }
    
    if (_needToUpdateTrainAttributes == YES)
    {
        PRAsyncConnection *connection = [[PRAsyncConnection alloc] initWithParent:self successSelector:@selector(trainAttributesDBSuccess:) didFailSelector:@selector(trainAttributesDBFailed:)];
        [connection sendUpdateTrainAttributesRequest];
    }
    else if (_needToUpdateIdTypes == YES)
    {
        PRAsyncConnection *connection = [[PRAsyncConnection alloc] initWithParent:self successSelector:@selector(idTypesDBSuccess:) didFailSelector:@selector(idTypesDBFailed:)];
        [connection sendUpdateIdTypesRequest];
    }
    else
    {
        [self saveAndDismiss];
    }
}

- (void) discountsDBFailed:(PRAsyncConnection*) connection
{
    [self connectionFailed];
}

- (void) trainAttributesDBSuccess:(PRAsyncConnection*) connection
{
    XLogTag(LogTag, @"%@", [[NSString alloc] initWithData:connection.responseData encoding:NSUTF8StringEncoding]);
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:connection.responseData
                          options:kNilOptions
                          error:&error];
    
    if(error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        [self connectionFailed];
        return;
    }
    
    NSDictionary *response = [json objectForKey:@"train_attributes_response"];
    
    NSNumber *status = [response objectForKey:@"status"];
    
    if(status==nil)
    {
        [self connectionFailed];
        return;
    }
    
    switch (status.intValue) {
        case STATUS_OK:
        {
//            PRAppDelegate *appDelegate = (PRAppDelegate*)[UIApplication sharedApplication].delegate;
            [TrainAttributeEntity clearAllRecordsWithManagedObjectContext:[[PRDBManager object]managedObjectContext]];
            
            NSArray *trainAttributes = [response objectForKey:@"train_attributes"];
            
            for (NSDictionary *trainAttribute in trainAttributes) {
                NSNumber *trainAttributeId = [[trainAttribute objectForKey:@"train_attribute"] objectForKey:@"id"];
                NSString *trainAttributeName = [[trainAttribute objectForKey:@"train_attribute"] objectForKey:@"name"];
                NSString *trainAttributeDescription = [[trainAttribute objectForKey:@"train_attribute"] objectForKey:@"description"];
                
                [TrainAttributeEntity insertTrainAttributeWithId:trainAttributeId andName:trainAttributeName andDescription:trainAttributeDescription withManagedObjectContext:[[PRDBManager object]managedObjectContext]];
            }
            
        }
            break;
        case STATUS_USER_BLOCKED:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertUserBlocked", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
            
    }
    
    if (_needToUpdateIdTypes == YES)
    {
        PRAsyncConnection *connection = [[PRAsyncConnection alloc] initWithParent:self successSelector:@selector(idTypesDBSuccess:) didFailSelector:@selector(idTypesDBFailed:)];
        [connection sendUpdateIdTypesRequest];
    }
    else
    {
        [self saveAndDismiss];
    }
}

- (void) trainAttributesDBFailed:(PRAsyncConnection*) connection
{
    [self connectionFailed];
}

@end
