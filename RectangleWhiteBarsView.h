//
//  RectangleWhiteBarsView.h
//  CallPay
//
//  Created by Jacek on 31.03.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RectangleWhiteBarsView : UIView
{
    BOOL isHighlighted;
    BOOL isSelected;
}

-(void)setup;
-(void)setSelected:(BOOL)selected;
-(void)setHighlited:(BOOL)highlited;

@end
