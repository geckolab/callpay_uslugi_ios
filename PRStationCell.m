//
//  PRStationCell.m
//  pr
//
//  Created by Marcin Szulc on 21.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PRStationCell.h"

@implementation PRStationCell

@synthesize barsView = _barsView;

- (void)awakeFromNib
{
    _barsView = [[RectangleWhiteBarsView alloc] initWithFrame:CGRectMake(0, -1, self.bounds.size.width, self.bounds.size.height+1)];
    [self setBackgroundColor:[UIColor clearColor]];
    [self.contentView insertSubview:_barsView atIndex:0];
    [_textStationName setTextColor:[UIColor whiteColor]];
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    [_barsView setSelected:selected];
}

@end
