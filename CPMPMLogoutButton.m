//
//  CPMPMLogoutButton.m
//  CallPay
//
//  Created by Jacek on 09.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "CPMPMLogoutButton.h"

@implementation CPMPMLogoutButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    return CGRectMake(0, 0, self.frame.size.width- self.currentImage.size.width - 20, self.frame.size.height);
}
- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    return CGRectMake(self.frame.size.width - self.currentImage.size.width - 10, (self.frame.size.height - self.currentImage.size.height) / 2, self.currentImage.size.width, self.currentImage.size.height);
}

@end
