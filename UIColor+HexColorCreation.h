//
//  UIColor+HexColorCreation.h
//  DailyHoroscope
//
//  Created by Alexander Pimenov on 24.05.13.
//  Copyright (c) 2013 Orlyapps Janzen & Strauß GbR. All rights reserved.
//

@interface UIColor (HexColorCreation)

+ (UIColor *)colorWithHexString:(NSString *)hex;

@end