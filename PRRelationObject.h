//
//  PRRelationObject.h
//  pr
//
//  Created by Marcin Szulc on 21.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ATTRIBUTE_ID_CLASS_1_AND_2 1
#define ATTRIBUTE_ID_CLASS_1_ONLY 2
#define ATTRIBUTE_ID_CLASS_2_ONLY 3

@interface PRRelationAttributeObject : NSObject

@property (strong, nonatomic) NSNumber *attributeId;
@property (strong, nonatomic) NSString *attributeName;
@property (strong, nonatomic) NSString *attributeDescription;

@end

@interface PRRelationObject : NSObject

@property (strong, nonatomic) NSNumber *relationId;
@property (strong, nonatomic) NSString *relationDepartureTime;
@property (strong, nonatomic) NSString *relationArrivalTime;
@property (strong, nonatomic) NSString *relationCategory;
@property (strong, nonatomic) NSMutableArray *relationAttributes;

@property (strong, nonatomic) NSNumber *relationSelectedClass;

@end
