//
//  PRPinViewController.m
//  pr
//
//  Created by Marcin Szulc on 24.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PRPinViewController.h"
#import "PRStandardButton.h"
#import "PROrderObject.h"
#import "PRPassengerDetailsViewController.h"
#import "PRAsyncConnection.h"
#import "XLog.h"
//#import "PRAppDelegate.h"
#import "PRAlertView.h"
#import "TicketEntity+Additions.h"
#import "TransactionEntity+Additons.h"
#import "PRSummaryViewController.h"
#import "PRCardObject.h"

#import "PRDBManager.h"

static NSString * const LogTag = @"PinViewController";

@interface PRPinViewController ()
@property (strong, nonatomic) PRAlertView *alert;
@end

@implementation PRPinViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIBarButtonItem * mcmLogo = [[UIBarButtonItem alloc] initWithCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mcm_icon"]]];
    self.navigationItem.rightBarButtonItem = mcmLogo;
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"anuluj", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"gotowe", nil) style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    [numberToolbar sizeToFit];
    _textPin.inputAccessoryView = numberToolbar;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setLabelPin:nil];
    [self setTextPin:nil];
    [self setButtonBuyTicket:nil];
    [self setLabelDescription:nil];
    [self setImageCvC:nil];
    [super viewDidUnload];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _labelPin.text = [NSString stringWithFormat:NSLocalizedString(@"podajKod", nil), _orderObject.passengerAuthType];
    
    if([_orderObject.passengerAuthType.lowercaseString isEqualToString:@"pin"] == YES)
    {
        _labelDescription.text = NSLocalizedString(@"pinOpis", nil);
        _imageCvC.hidden = YES;
    }
    else
    {
        _labelDescription.text = [NSString stringWithFormat:NSLocalizedString(@"cvcOpis", nil), _orderObject.passengerAuthType];
        _imageCvC.hidden = NO;
    }
    
    _textPin.text = @"";
    
    _labelYouPayByCard.text = [NSString stringWithFormat:NSLocalizedString(@"youPayByCard", nil), _defaultCard.number];
}

-(void)cancelNumberPad{
    [_textPin resignFirstResponder];
    _textPin.text = @"";
}

-(void)doneWithNumberPad{
    [_textPin resignFirstResponder];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"showSummary"])
    {
    }
}

- (void) enableOrDisableButtonOnClick
{
    if(_textPin.text.length>0)
    {
        _buttonBuyTicket.enabled = YES;
    }
    else
    {
        _buttonBuyTicket.enabled = NO;
    }
}

- (IBAction)backgroundOnClick:(id)sender {
    [_textPin resignFirstResponder];
}

- (IBAction)buttonBuyTicketOnClick:(id)sender {
    _orderObject.passengerAuthCode = _textPin.text;
    [self buyTicket];
    
}

- (void) buyTicket
{
    [_buttonBuyTicket setLockedForResponse:YES];
    PRAsyncConnection *connection = [[PRAsyncConnection alloc] initWithParent:self successSelector:@selector(connectionBuyTicketSuccess:) didFailSelector:@selector(connectionBuyTicketFailed:)];
    [connection sendAuthPaymentRequestWithOrderObject:_orderObject];
}

- (void) parseBuyTicketResponse:(NSDictionary*) response
{
//    PRAppDelegate *appDelegate = (PRAppDelegate*)[UIApplication sharedApplication].delegate;
    
    NSNumber *transactionId = [response objectForKey:@"id"];
    NSNumber *transactionJourneyTo = [response objectForKey:@"journey_to"];
    NSNumber *transactionJourneyReturn = [response objectForKey:@"journey_return"];
    NSNumber *transactionValue = [response objectForKey:@"value"];
    NSString *transactionTimestamp = [response objectForKey:@"transaction_timestamp"];
    
    NSArray *tickets = [response objectForKey:@"tickets"];
    
    NSMutableArray *transactionTickets = [[NSMutableArray alloc] init];
    
    for(NSDictionary *ticket in tickets)
    {
        if([ticket isEqual:@""])
        {
            XLogTag(LogTag, @"empty list parameter");
            continue;
        }
        TicketEntity *transactionTicket = [TicketEntity insertTicketEntityWithValuesFromJsonResponseTicket:[ticket objectForKey:@"ticket"] withManagedObjectContext:[[PRDBManager object]managedObjectContext]];
        [transactionTickets addObject:transactionTicket];
    }
    
    [TransactionEntity insertTransactionEntityWithId:transactionId to:transactionJourneyTo treturn:transactionJourneyReturn value:transactionValue timestamp:transactionTimestamp andTickets:[NSSet setWithArray:transactionTickets] withManagedObjectContext:[[PRDBManager object]managedObjectContext]];
    
    NSError *error;
    
    if([[[PRDBManager object]managedObjectContext] save:&error] == NO)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        return;
    }
    
    [self performSegueWithIdentifier:@"showSummary" sender:self];
}

#pragma mark - alert
- (void) connectionFailed
{
    _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertStatusError", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
    [_alert show];
}

- (void) connectionFailedAlertCallback:(NSNumber*) buttonIndex
{
    
}

#pragma mark - connections
- (void) connectionBuyTicketSuccess:(PRAsyncConnection*) connection
{
    
    XLogTag(LogTag, @"%@", [[NSString alloc] initWithData:connection.responseData encoding:NSUTF8StringEncoding]);
    
    [_buttonBuyTicket setLockedForResponse:NO];
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:connection.responseData
                          options:kNilOptions
                          error:&error];
    
    if(error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        [self connectionFailed];
        return;
    }
    
    NSDictionary *response = [json objectForKey:@"start_journey_response"];
    
    NSNumber *status = [response objectForKey:@"status"];
    
    if(status==nil)
    {
        [self connectionFailed];
        return;
    }
    
    switch (status.intValue) {
        case STATUS_OK:
        {
            [self parseBuyTicketResponse:response];
        }
            return;
        case STATUS_USER_BLOCKED:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertUserBlocked", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
        case STATUS_NO_MONEY:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertNoMoney", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
        case STATUS_WRONG_PIN:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:[NSString stringWithFormat:NSLocalizedString(@"alertWrongPin", nil), _orderObject.passengerAuthType] withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
        case STATUS_NO_RELATION_BETWEEN_STATIONS:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertNoRelation", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
        case STATUS_WRON_PHONE_NO:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertWrongPhoneNo", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
        case STATUS_WRONG_DATE:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertWrongDate", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
        case STATUS_INVALID_PARAMETERS:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertInvalidParameters", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
        case STATUS_CALLPAY_GENERAL_ERROR:
        {
            [self connectionFailed];
        }
            return;
        case STATUS_CANNOT_BUY_TICKET:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertCannotBuyTicket", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
    }
    
}

- (void) connectionBuyTicketFailed:(PRAsyncConnection*) connection
{
    [_buttonBuyTicket setLockedForResponse:NO];
    [self connectionFailed];
}


@end
