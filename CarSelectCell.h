//
//  CarSelectCell.h
//  CallPay
//
//  Created by Jacek on 13.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RectangleWhiteBarsView.h"
#import "CellDeleteButton.h"
#import "CityActiveStatusView.h"

@interface CarSelectCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *carLabel;

@property (nonatomic, strong) RectangleWhiteBarsView *barsView;
@property (nonatomic, weak) IBOutlet CellDeleteButton *deleteButton;
@property (nonatomic, weak) IBOutlet CityActiveStatusView *activeView;

-(void)setCarLabelText:(NSString*)text;
-(void)setActive:(BOOL)active;

@end
