//
//  PRCardsProvider.h
//  pr
//
//  Created by Alexander Pimenov on 03.07.13.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PRCardsProvider;
@class PRCardsContainer;


@protocol PRCardsProviderDelegate <NSObject>

- (void)cardsProvider:(PRCardsProvider *)cardsProvider
         fetchedCards:(PRCardsContainer *)cardsContainer;

- (void)cardsProviderUnableConnectToServer:(PRCardsProvider *)cardsProvider;

- (void)cardsProviderFailedToFetchCards:(PRCardsProvider *)cardsProvider;

- (void)cardsProviderFailedToFetchCards:(PRCardsProvider *)cardsProvider
                        withErrorStatus:(NSInteger)status;

@end


@interface PRCardsProvider : NSObject

@property (weak, nonatomic) id <PRCardsProviderDelegate> delegate;

- (void)fetchCards;

@end