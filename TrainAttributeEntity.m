//
//  TrainAttributeEntity.m
//  CallPay
//
//  Created by Jacek on 15.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "TrainAttributeEntity.h"


@implementation TrainAttributeEntity

@dynamic trainAttributeDescription;
@dynamic trainAttributeId;
@dynamic trainAttributeName;

@end
