//
//  PRTrainAttributeCell.h
//  pr
//
//  Created by Marcin Szulc on 22.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#define PR_TRAIN_ATTRIBUTE_CELL_HEIGHT 48

#import <UIKit/UIKit.h>

@interface PRTrainAttributeCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *labelAttribute;
@property (strong, nonatomic) IBOutlet UILabel *labelAttributeDescription;

@end
