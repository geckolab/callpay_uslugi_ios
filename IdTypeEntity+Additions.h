//
//  IdTypeEntity+Additions.h
//  pr
//
//  Created by Marcin Szulc on 21.02.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "IdTypeEntity.h"

@interface IdTypeEntity (Additions)

+ (IdTypeEntity*) insertIdTypeWithId:(NSNumber*) index andName:(NSString*) name withManagedObjectContext:(NSManagedObjectContext*) managedObjectContext;

+ (void) clearAllRecordsWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext;

+ (NSArray*) fetchAllIdTypesWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext;

+ (IdTypeEntity*) fetchIdTypeWithId:(NSNumber*) index withManagedObjectContext:(NSManagedObjectContext*) managedObjectContext;

@end
