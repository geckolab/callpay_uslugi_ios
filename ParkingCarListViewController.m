//
//  ParkingCarListViewController.m
//  CallPay
//
//  Created by Jacek on 10.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "ParkingCarListViewController.h"

#import "CarSelectCell.h"

#import "CarsManager.h"
#import "UIImage-Extensions.h"

@interface ParkingCarListViewController ()

@end

@implementation ParkingCarListViewController

@synthesize sBar = _sBar;
@synthesize tableView = _tableView;

@synthesize addText = _addText;

- (void)viewDidLoad {
    [super viewDidLoad];
    [[CarsManager object] setCarListVCDelegate:self];
    [_sBar setBackgroundImage:[UIImage new]];
    [_sBar setTranslucent:YES];
    [_sBar setDelegate:self];
    [_tableView setDataSource:self];
    [_tableView setDelegate:self];
    _tableView.backgroundColor = [UIColor clearColor];
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [_tableView registerNib:[UINib nibWithNibName:@"CarSelectCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"CarSelectCell"];
    
    carsList = [[NSMutableArray alloc] initWithObjects:@{@"number": @"el12345",@"idx":@"0"},@{@"number":@"wp12345",@"idx":@"1"},@{@"number":@"pba1234",@"idx":@"2"}, nil];
//    filteredList = [[NSMutableArray alloc] initWithArray:carsList copyItems:YES];
    filteredList = [[NSMutableArray alloc] init];
//    [self searchListWith:@""];
    
//    UIImage *add = [[UIImage imageNamed:@"icon_no"] imageRotatedByDegrees:45.0];
    UIImage *add = [UIImage imageNamed:@"icon_no"];
    UIButton *addButton = [UIButton buttonWithType:0];
    [addButton setFrame:CGRectMake(0, 0, add.size.width, add.size.height)];
    [addButton setImage:[add imageRotatedByDegrees:45.0] forState:UIControlStateNormal];
    [addButton addTarget:self action:@selector(addAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc] initWithCustomView:addButton];
    self.navigationItem.rightBarButtonItem = rightBarItem;
    
//    _addText = [[UITextField alloc] initWithFrame:_sBar.frame];
//    _addText = [[UITextField alloc] initWithFrame:CGRectMake(0, _sBar.frame.origin.y, self.view.frame.size.width, _sBar.frame.size.height)];
//    _addText.backgroundColor = [UIColor redColor];
//    [_addText removeFromSuperview];
    [_addText setHidden:YES];
    _addText.returnKeyType = UIReturnKeyDone;
    [self.view bringSubviewToFront:_sBar];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[CarsManager object] setCarListVCDelegate:self];
//    [self searchListWith:@""];
    [[CarsManager object]reloadData];
    //[self performSelector:@selector(searchListWith:) withObject:@"EL" afterDelay:2.0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (CGFloat)tableView:(UITableView *)tabView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return 2;
    else
        return 0;
}
- (UIView *)tableView:(UITableView *)tabView viewForHeaderInSection:(NSInteger)section
{
    UIView *hv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tabView.frame.size.width, 2)];
    [hv setBackgroundColor:[UIColor clearColor]];
    return hv;
}
- (CGFloat)tableView:(UITableView *)tabView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([_sBar.text isEqualToString:@""] && [_addText.text isEqualToString:@""])
    {
        return 1;
    }
    else
    {
        if ([filteredList count] != 1)
        {
            return 2;
        }
        else
        {
            return 1;
        }
    }
    return 1;
}
-(NSInteger) getRowsCount {
    //return 10;//[self.locationsKeys count];
    return [filteredList count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0)
    {
        return [self getRowsCount];
    }
    else
    {
        return 1;
    }
}
- (UITableViewCell *)tableView:(UITableView *)table cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"CarSelectCell";
    //    NSString *cellIdentifier = [NSString stringWithFormat:@"TicketCell", indexPath.row];
    CarSelectCell *cell = [table dequeueReusableCellWithIdentifier:cellIdentifier];
    
    //    if (cell == nil || IS_IOS7_OR_LATER)
    if (cell == nil)
    {
        cell = [[CarSelectCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    //    else {
    //        if ([cell subviews]){
    //            for (UIView *subview in [cell subviews]) {
    //                [subview removeFromSuperview];
    //            }
    //        }
    //    }
    
    //  static NSString *CellIdentifier = @"Cell";
    //
    //  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //
    //  if (cell == nil) {
    //    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    //  }
    
    
    cell.selectedBackgroundView.backgroundColor=[UIColor clearColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
//    [cell.carLabel setText:@"test123"];
//    [cell setCarLabelText:[filteredList objectAtIndex:indexPath.row]];
    if (indexPath.section == 0)
    {
        NSDictionary *dict = (NSDictionary*)[filteredList objectAtIndex:indexPath.row];
//    NSLog(@"%@",dict);
//        [cell setCarLabelText:(NSString*)[dict objectForKey:@"number"]];
        Car *c = (Car *)[dict objectForKey:@"obj"];
        [cell setCarLabelText:(NSString*)[dict objectForKey:@"number"]];
//        [cell setCarLabelText:[NSString stringWithFormat:@"%@ %hhd",[dict objectForKey:@"number"],[c.defaultCar boolValue]]];
        if ([[CarsManager object].currentCar isEqualToString:[dict objectForKey:@"number"]])
        {
            [cell setActive:YES];
        }
        else
        {
            [cell setActive:NO];
        }
    }
    else
    {
        if (![_sBar.text isEqualToString:@""])
        {
            [cell setCarLabelText:[NSString stringWithFormat:@"Dodaj samochód:\n%@",_sBar.text]];
        }
        else if (![_addText.text isEqualToString:@""])
        {
            [cell setCarLabelText:[NSString stringWithFormat:@"Dodaj samochód:\n%@",_addText.text]];
        }
        else
        {
            [cell setCarLabelText:@"Dodaj samochód:\n"];
        }
//        [cell setCarLabelText:[NSString stringWithFormat:@"Dodaj samochód:\n%@",_sBar.text]];
//        [cell setCarLabelText:[NSString stringWithFormat:@"Dodaj samochód:\n%@",_addText.text]];
    }
    
    return cell;
}

-(void)searchListWith:(NSString*)searchText
{
//    NSArray *filteredArray = [carsList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF like  %@", [searchText stringByAppendingString:@"*"]]];
    NSArray *filteredArray;
    if ([searchText isEqualToString:@""])
    {
        [filteredList removeAllObjects];
//        [filteredList addObjectsFromArray:carsList];
//        for (int idx= 0; idx < [carsList count]; idx++)
        for (int idx= 0; idx < [[CarsManager object].carsList count]; idx++)
        {
//            [filteredList addObject:@{@"number" : [carsList objectAtIndex:idx], @"idx" : [NSString stringWithFormat:@"%d",idx]}];
            [filteredList addObject:[[CarsManager object].carsList objectAtIndex:idx]];
        }
    }
    else
    {
//        filteredArray = [carsList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF contains[cd] %@", [searchText stringByAppendingString:@""]]];
//        filteredArray = [carsList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(number contains[cd] %@)", [searchText stringByAppendingString:@""]]];
//        filteredArray = [carsList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(number == %@)", [searchText stringByAppendingString:@""]]];
        NSPredicate *filter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
//            return [[evaluatedObject valueForKeyPath:@"object.number"]
//                    isEqualToString:searchText];
            NSDictionary *dictionary = (NSDictionary *)evaluatedObject;
            NSString *value = [dictionary objectForKey:@"number"];
            return [value rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound;
        }];
        filteredArray = [[CarsManager object].carsList filteredArrayUsingPredicate:filter];
        [filteredList removeAllObjects];
        [filteredList addObjectsFromArray:filteredArray];
    }
//    NSArray *filteredArray = [carsList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF contains[cd] %@", [searchText stringByAppendingString:@""]]];
    if (![_sBar.text isEqualToString:@""] && [filteredList count] == 0)
    {
        [_sBar resignFirstResponder];
        _sBar.returnKeyType = UIReturnKeyDone;
        [_sBar becomeFirstResponder];
    }
    else
    {
        if (![_sBar.text isEqualToString:@""])
        {
            [_sBar resignFirstResponder];
            _sBar.returnKeyType = UIReturnKeySearch;
            [_sBar becomeFirstResponder];
        }
    }
    
    [_tableView reloadData];
}
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    return YES;
}
-(void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self searchListWith:searchText];
}
- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSMutableCharacterSet *allowedCharacters = [NSMutableCharacterSet alphanumericCharacterSet];
    if([text rangeOfCharacterFromSet:allowedCharacters.invertedSet].location == NSNotFound)
    {
        
        return YES;
        
    }
    
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSMutableCharacterSet *allowedCharacters = [NSMutableCharacterSet alphanumericCharacterSet];
    if([string rangeOfCharacterFromSet:allowedCharacters.invertedSet].location == NSNotFound)
    {
        
        return YES;
        
    }
    
    return NO;
}

-(void)popVC
{
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count] - 2] animated:YES];
}

- (NSIndexPath *)tableView:(UITableView *)tabView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        NSDictionary *dict = [filteredList objectAtIndex:indexPath.row];
        [[CarsManager object] setCurrentCar:[dict valueForKey:@"number"]];
    }
    else
    {
//        CarSelectCell *c = (CarSelectCell*)[_tableView cellForRowAtIndexPath:indexPath];
//        [[CarsManager object] addCar:c.carLabel.text];
        if (![_sBar.text isEqualToString:@""])
        {
            [[CarsManager object] addCar:[_sBar.text uppercaseString]];
        }
        else if (![_addText.text isEqualToString:@""])
        {
            [[CarsManager object] addCar:[_addText.text uppercaseString]];
        }
        
    }
    [self performSelector:@selector(popVC) withObject:nil afterDelay:1.0];
    return indexPath;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[CarsManager object]setCarListVCDelegate:nil];
}

-(void)reloadData
{
    _sBar.text = @"";
    [self searchListWith:@""];
//    [_tableView reloadData];
}

-(void)deleteCarWithPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        Car *c = (Car*)[[filteredList objectAtIndex:indexPath.row]objectForKey:@"obj"];
        [[CarsManager object]removeCarWithEntity:c];
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        return UITableViewCellEditingStyleDelete;
    }
    else
    {
        return UITableViewCellEditingStyleNone;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self deleteCarWithPath:indexPath];
}

- (void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    //[_tableView reloadData];
    
}
-(void)addAction:(id)sender
{
    NSLog(@"add action");
    [filteredList removeAllObjects];
    [_tableView reloadData];
    [_sBar setHidden:YES];
    [_addText setHidden:NO];
    [self.view bringSubviewToFront:_addText];
    [_addText becomeFirstResponder];
    _sBar.text = @"";
    _addText.text = @"";
}

-(IBAction)addTextChangeAction:(id)sender
{
//    [self searchListWith:_addText.text];
    [_tableView reloadData];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [_addText setHidden:YES];
    [_sBar setHidden:NO];
    [self.view bringSubviewToFront:_sBar];
    [self searchListWith:@""];
    if (![_addText.text isEqualToString:@""])
    {
        [[CarsManager object] addCar:[_addText.text uppercaseString]];
    }
    _sBar.text = @"";
    _addText.text = @"";
    [self performSelector:@selector(popVC) withObject:nil afterDelay:1.0];
    return YES;
}

@end

/*
 - (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar;                      // return NO to not become first responder
 - (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar;                     // called when text starts editing
 - (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar;                        // return NO to not resign first responder
 - (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar;                       // called when text ends editing
 - (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText;   // called when text changes (including clear)
 - (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text NS_AVAILABLE_IOS(3_0); // called before text changes
 
 - (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar;                     // called when keyboard search button pressed
 - (void)searchBarBookmarkButtonClicked:(UISearchBar *)searchBar;                   // called when bookmark button pressed
 - (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar;                     // called when cancel button pressed
 - (void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar NS_AVAILABLE_IOS(3_2); // called when search results button pressed
 
 - (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope NS_AVAILABLE_IOS(3_0);
 */
