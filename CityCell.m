//
//  CityCell.m
//  CallPay
//
//  Created by Jacek on 02.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "CityCell.h"

@implementation CityCell

@synthesize cityNameLabel = _cityNameLabel;
@synthesize activeLabel = _activeLabel;
@synthesize cityStatus = _cityStatus;
@synthesize downloadButton = _downloadButton;
@synthesize barsView = _barsView;

- (void)awakeFromNib {
    _barsView = [[RectangleWhiteBarsView alloc] initWithFrame:CGRectMake(0, -1, self.bounds.size.width, self.bounds.size.height+1)];
    [self.contentView insertSubview:_barsView atIndex:0];
    [_cityStatus setActive:NO];
//    [_downloadButton setSelected:NO];
    [_downloadButton setActive:NO];
    _cityNameLabel.text = @"";
    _activeLabel.text = @"";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [_barsView setSelected:selected];
    // Configure the view for the selected state
}
-(void)prepareForReuse
{
    [super prepareForReuse];
    _cityNameLabel.text = @"";
    _activeLabel.text = @"";
    [_cityStatus setActive:NO];
//    [_downloadButton setSelected:NO];
    [_downloadButton setActive:NO];
    [_downloadButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
}
-(void)setTag:(NSInteger)tag
{
    [_barsView setTag:tag];
    [_downloadButton setTag:tag];
}

@end
