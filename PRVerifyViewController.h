//
//  PRVerifyViewController.h
//  pr
//
//  Created by Marcin Szulc on 17.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PRStandardButton;

@interface PRVerifyViewController : ProjectViewController

@property (strong, nonatomic) IBOutlet UITextField *textName;
@property (strong, nonatomic) IBOutlet UITextField *textPhoneNo;
@property (strong, nonatomic) IBOutlet UITextField *textSurname;


@property (strong, nonatomic) IBOutlet PRStandardButton *buttonVerify;
@property (strong, nonatomic) IBOutlet UISwitch *regulationsSwitch;

- (IBAction)textDidEndOnExit:(id)sender;

- (IBAction)buttonVerifyOnClick:(id)sender;

- (IBAction)backgroundTouched:(id)sender;
- (IBAction)regulationSwitchValueChanged:(id)sender;

@end
