//
//  SettingsEntity.h
//  CallPay
//
//  Created by Jacek on 15.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SettingsEntity : NSManagedObject

@property (nonatomic, retain) NSDate * lastCheckDate;
@property (nonatomic, retain) NSDate * lastUpdateDate;
@property (nonatomic, retain) NSNumber * tableDiscountsVersion;
@property (nonatomic, retain) NSNumber * tableIdTypesVersion;
@property (nonatomic, retain) NSNumber * tableStationsVersion;
@property (nonatomic, retain) NSNumber * tableTrainAttributesVersion;
@property (nonatomic, retain) NSNumber * userAcceptedCardsWarning;
@property (nonatomic, retain) NSNumber * userAcceptedIdWarning;
@property (nonatomic, retain) NSNumber * userAcceptedTermsOfVerification;
@property (nonatomic, retain) NSString * userCertFileName;
@property (nonatomic, retain) NSNumber * userHasRegioCard;
@property (nonatomic, retain) NSString * userIdNo;
@property (nonatomic, retain) NSNumber * userIdType;
@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSString * userPassword;
@property (nonatomic, retain) NSString * userPhoneNo;
@property (nonatomic, retain) NSString * userSurname;
@property (nonatomic, retain) NSNumber * userVerified;

@end
