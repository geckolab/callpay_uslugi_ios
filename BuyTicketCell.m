//
//  BuyTicketCell.m
//  CallPay
//
//  Created by Jacek on 30.03.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "BuyTicketCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation BuyTicketCell

@synthesize nameLabel = _nameLabel;
@synthesize priceLabel = _priceLabel;

- (void)awakeFromNib {
    barsView = [[RectangleWhiteBarsView alloc] initWithFrame:CGRectMake(0, -1, self.bounds.size.width, self.bounds.size.height+1)];
    [self.contentView addSubview:barsView];
    
    [_nameLabel setFont:[UIFont fontWithName:@"Swis721MdEU" size:15]];//_nameLabel.backgroundColor = [UIColor greenColor];
    [_priceLabel setFont:[UIFont fontWithName:@"Swis721LtEU" size:39]];//_priceLabel.backgroundColor = [UIColor redColor];
    [_nameLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_priceLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_nameLabel setTextColor:[UIColor whiteColor]];
    [_priceLabel setTextColor:[UIColor whiteColor]];
    self.backgroundColor = [UIColor clearColor];
//    [self setAccessoryType:UITableViewCellAccessoryDetailButton];
    UIImageView *accView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 13, 23)];
    [accView setImage:[UIImage imageNamed:@"new_icon_more"]];
    self.accessoryView = accView;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)prepareForReuse
{
    [super prepareForReuse];
    _nameLabel.text = @"";
    _priceLabel.text = @"";
    [self setNeedsDisplay];
}
-(void)setPrice:(NSString*)price
{
    NSNumberFormatter * formatter = [[NSNumberFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"pl_PL"];
    formatter.numberStyle = NSNumberFormatterCurrencyStyle;
    [formatter setLocale:locale];
    
    CGFloat pr = [price floatValue] / 100;
    
    [_priceLabel setText:[formatter stringFromNumber:[[NSNumber alloc] initWithFloat:pr]]];
}
- (void) drawLayer:(CALayer*) layer inContext:(CGContextRef) ctx
{
    NSLog(@"draw layer");
    if ([self isHighlighted] || [self isSelected])
    {
        [layer setBackgroundColor:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.48].CGColor];
    }
    else
    {
        [layer setBackgroundColor:[UIColor clearColor].CGColor];
    }
    
    CGContextSetStrokeColorWithColor(ctx, [UIColor whiteColor].CGColor);
    CGContextSetLineWidth(ctx, 2);
    
    CGContextMoveToPoint(ctx, 0, 0);
    CGContextAddLineToPoint(ctx, layer.frame.size.width, 0);
    
    CGContextStrokePath(ctx);
    
    CGContextSetLineWidth(ctx, 2);
    
    CGContextMoveToPoint(ctx, 0, layer.frame.size.height);
    CGContextAddLineToPoint(ctx, layer.frame.size.width, layer.frame.size.height);
    
    CGContextStrokePath(ctx);
    
    
}
- (NSIndexPath *)tableView:(UITableView *)tabView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath;
}
-(void)setNeedsDisplay
{
    [super setNeedsDisplay];
    [self.backgroundView.layer setNeedsDisplay];
}
@end
