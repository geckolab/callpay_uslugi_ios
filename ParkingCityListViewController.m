//
//  ParkingCityListViewController.m
//  CallPay
//
//  Created by Jacek on 10.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "ParkingCityListViewController.h"

#import "CityCell.h"

#import "ProjectLocations.h"

@interface ParkingCityListViewController ()

@end

@implementation ParkingCityListViewController

typedef enum _ChooseCity {
    CatalogInfoAlert = 0,
    CatalogDelete = 1,
    CatalogDownload
} ChooseCity;

@synthesize locationsKeys;
@synthesize tableV = _tableV;

- (id) init
{
    self = [super init];
    
    if (self) {
        self.locationsKeys = [[[[ProjectLocations object] parkingLocationList] allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        checkSelectedButtonClicked = NO;
        self.catalogsManager = [[ParkingCatalogsManager alloc] initWithDelegate: self];
    }
    
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.locationsKeys = [[[[ProjectLocations object] parkingLocationList] allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        checkSelectedButtonClicked = NO;
        self.catalogsManager = [[ParkingCatalogsManager alloc] initWithDelegate: self];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [_tableV setDataSource:self];
    [_tableV setDelegate:self];
    _tableV.backgroundColor = [UIColor clearColor];
    [_tableV setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [_tableV registerNib:[UINib nibWithNibName:@"CityCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"CityCell"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)popVC
{
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count] - 2] animated:YES];
}

- (CGFloat)tableView:(UITableView *)tabView heightForHeaderInSection:(NSInteger)section
{
    return 2;
}
- (UIView *)tableView:(UITableView *)tabView viewForHeaderInSection:(NSInteger)section
{
    UIView *hv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tabView.frame.size.width, 2)];
    [hv setBackgroundColor:[UIColor clearColor]];
    return hv;
}
- (CGFloat)tableView:(UITableView *)tabView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger) getRowsCount {
    return [self.locationsKeys count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self getRowsCount];
}
- (UITableViewCell *)tableView:(UITableView *)table cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"CityCell";
    //    NSString *cellIdentifier = [NSString stringWithFormat:@"TicketCell", indexPath.row];
    CityCell *cell = [table dequeueReusableCellWithIdentifier:cellIdentifier];
    
    //    if (cell == nil || IS_IOS7_OR_LATER)
    if (cell == nil)
    {
        cell = [[CityCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    //    else {
    //        if ([cell subviews]){
    //            for (UIView *subview in [cell subviews]) {
    //                [subview removeFromSuperview];
    //            }
    //        }
    //    }
    
    //  static NSString *CellIdentifier = @"Cell";
    //
    //  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //
    //  if (cell == nil) {
    //    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    //  }
    
    
    cell.selectedBackgroundView.backgroundColor=[UIColor clearColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    NSDictionary *location = [[[ProjectLocations object] parkingLocationList] objectForKey: self.locationsKeys[indexPath.row]];
    [cell setTag:indexPath.row];
    [cell.downloadButton addTarget:self action:@selector(newButtonDownloadAction:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.cityNameLabel.text = [location valueForKey:@"name"];
    if ([[ProjectLocations object].currentParkingCity isEqualToString: locationsKeys[indexPath.row]]) {
        cell.activeLabel.text = @"Aktualnie wybrane";
    }
    if ([ProjectViewHelper isSavedLocationForCityParking:self.locationsKeys[indexPath.row]]) {
        [cell.cityStatus setActive:YES];
        //        [cell.downloadButton setSelected:YES];
        [cell.downloadButton setActive:YES];
    }
    else
    {
        [cell.cityStatus setActive:NO];
        //        [cell.downloadButton setSelected:NO];
        [cell.downloadButton setActive:NO];
    }
    
    return cell;
}
- (NSIndexPath *)tableView:(UITableView *)tabView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    NSLog(@"selected cell %d",indexPath.row);
    CityCell *c = (CityCell*)[_tableV cellForRowAtIndexPath:indexPath];
    checkSelectedButtonClicked = YES;
    if (c != nil) {
        activeButton = c.barsView;
    }
    else
        activeButton = nil;
    
    NSString *cityIndex = self.locationsKeys[indexPath.row];
    
    if (![ProjectViewHelper isSavedLocationForCityParking: cityIndex]) {
        [self startDownload: cityIndex];
    } else {
        [self selectButtonClickedComplete];
    }
    return indexPath;
}
- (void) startDownload: (NSString *) cityIndex {
    _cityIndex = cityIndex;
    [self.catalogsManager startDownload: cityIndex];
}

- (NSDictionary *) dictionaryForActiveButton {
    return [self.catalogsManager dictionaryForCityIndex:self.locationsKeys[[activeButton tag]]];
}

- (void) selectButtonClickedComplete {
    if (checkSelectedButtonClicked) {
        checkSelectedButtonClicked = NO;
        [[ProjectLocations object] setCurrentParkingCity:locationsKeys[[activeButton tag]]];
        [_tableV reloadData];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
        //    if ([prefs objectForKey:LOCATIONS_SAVED_CITY] != nil) {
        [prefs setObject: [ProjectLocations object].currentParkingCity forKey:LOCATIONS_SAVED_CITY_PARKING];
        [prefs synchronize];
        //    }
        [self performSelector:@selector(popVC) withObject:nil afterDelay:1.0];
    }
}
- (void) catalogDownloadSuccess: (NSString *) cityIndex {
    [self selectButtonClickedComplete];
}

- (void) catalogDownloadFail: (NSString *) cityIndex {
    [self activeButtonChangeSelected];
}

- (void) activeButtonChangeSelected {
    if (activeButton != nil) {
        if ([activeButton isMemberOfClass:NSClassFromString(@"CellConfirmButton")]) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[activeButton tag] inSection:0];
            
            CityCell *c = (CityCell*)[_tableV cellForRowAtIndexPath:indexPath];
            [activeButton setActive:![activeButton isActive]];
            [c.cityStatus setActive:[activeButton isActive]];
        }
        else
            [activeButton setSelected:![activeButton isSelected]];
    }
}

-(IBAction)newButtonDownloadAction:(id)sender
{
    activeButton = sender;
    
    if ([sender isActive]) {
        if ([[ProjectLocations object].currentParkingCity isEqualToString: self.locationsKeys[[activeButton tag]]]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message: @"Nie można usunąć katalogu dla miasta wybranego jako aktualne" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert setTag: CatalogInfoAlert];
            [alert show];
            //[alert release];
        } else {
            NSString *city = [[self dictionaryForActiveButton] valueForKey:@"name"];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message: [NSString stringWithFormat:@"Czy na pewno chcesz usunąć zapisany katalog dla miasta %@?", city] delegate:self cancelButtonTitle:@"Anuluj" otherButtonTitles:@"Usuń", nil];
            [alert setTag: CatalogDelete];
            [alert show];
            //[alert release];
        }
    } else {
        [self activeButtonChangeSelected];
        [self startDownload: self.locationsKeys[[sender tag]]];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case CatalogDelete: {
            if (buttonIndex == 1) {
                [self activeButtonChangeSelected];
                NSString *cityIndex = self.locationsKeys[[activeButton tag]];
                
                [self.catalogsManager deleteCatalogForCity:cityIndex];
            }
            break;
        }
        case CatalogDownload: {
            
        }
    }
}

@end
