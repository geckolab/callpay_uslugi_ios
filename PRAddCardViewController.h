//
//  PRAddCardViewController.h
//  pr
//
//  Created by Alexander Pimenov on 21.06.13.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PRAddCardViewController;
@class PRCardObject;


@protocol PRAddCardViewControllerDelegate <NSObject>

- (void)addCardViewControllerAddedNewCard:(PRAddCardViewController *)addCardViewController;

@end


@interface PRAddCardViewController : UIViewController

@property (weak, nonatomic) id <PRAddCardViewControllerDelegate> delegate;

@end
