//
//  SelectedCityView.h
//  CallPay
//
//  Created by Jacek on 26.03.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectedCityView : UIView
{
    UIImageView *iconView;
    UILabel *titleLabel;
    UILabel *cityLabel;
}

-(void)setup;
-(void)setCity:(NSString*)name;

-(void)setCar:(BOOL)car;
-(void)setCenterLabels:(BOOL)center;

@end
