//
//  ActiveTicketCell.h
//  CallPay
//
//  Created by Jacek on 07.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RectangleWhiteBarsView.h"

@interface ActiveTicketCell : UITableViewCell
{
    RectangleWhiteBarsView *barsView;
}
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *buyDateLabel;

@end
