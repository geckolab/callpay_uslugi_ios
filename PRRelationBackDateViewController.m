//
//  PRRelationBackDateViewController.m
//  pr
//
//  Created by Marcin Szulc on 23.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PRRelationBackDateViewController.h"

@interface PRRelationBackDateViewController ()

@end

@implementation PRRelationBackDateViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {}
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setLabelBackDate:nil];
    [self setControlDate:nil];
    [self setButtonSearch:nil];
    [super viewDidUnload];
}
@end
