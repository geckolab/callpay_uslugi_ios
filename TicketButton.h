//
//  TicketButton.h
//  CallPay
//
//  Created by Jacek on 24.03.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TicketButton : UIButton
{
    BOOL normalnyEnabled;
    NSString *ticketName;
}

@property (nonatomic, strong) IBOutlet UIView *contentSubview;
@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet UILabel *typeLabel;
@property (nonatomic, strong) IBOutlet UILabel *timeLabel;
@property (nonatomic, strong) IBOutlet UIImageView *typeIconView;

-(void)setup;
-(void)setNormalny:(BOOL)type;

@end
