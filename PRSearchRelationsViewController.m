//
//  PRSearchRelationsViewController.m
//  pr
//
//  Created by Marcin Szulc on 16.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PRSearchRelationsViewController.h"
#import "PRUtils.h"
#import "PRStationsViewController.h"
#import "PRDatePickerViewController.h"
#import "Config.h"
#import "PRStandardControl.h"
#import "PROrderObject.h"
#import "StationEntity+Additions.h"
#import "PRStandardButton.h"
#import "PRRelationsViewController.h"

@interface PRSearchRelationsViewController ()

@property (strong, nonatomic) PROrderObject *orderObject;

@end

@implementation PRSearchRelationsViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
    
    }
    
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate setDelegate:self];
    
    [self refreshUI];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self hideDateView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    _date = [NSDate date];
    
    [self resetOrderObject];
    
    _controlStationTo.enabled = NO;
    
    _labelDate.text = [PRUtils getFormattedDateFromDate:_date];
    
    _datePickerViewController = [[PRDatePickerViewController alloc] initWithNibName:@"PRDatePickerViewController" bundle:nil];
    _datePickerViewController.delegate = self;
    _datePickerViewController.minDate = _date;
    _datePickerViewController.maxDate = [PRUtils addDays:CONFIG_MAX_DAYS_FORWARD toDate:_date];
    
    [PRUtils setView:_datePickerViewController.view atBottomOfView:self.view andAddAsSubview:YES];
    
    CGRect rect = _datePickerViewController.view.frame;
    rect.size.width = self.view.bounds.size.width;
    _datePickerViewController.view.frame = rect;
    
    UIBarButtonItem * mcmLogo = [[UIBarButtonItem alloc] initWithCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mcm_icon"]]];
    self.navigationItem.rightBarButtonItem = mcmLogo;
}

- (void)viewDidUnload {
    [self setControlStationTo:nil];
    [self setLabelStationFrom:nil];
    [self setLabelStationTo:nil];
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"searchStationTo"])
    {
        PRStationsViewController *destController = segue.destinationViewController;
        [destController.navigationItem setTitle:NSLocalizedString(@"stacjaDo", nil)];
        _orderObject.stationFromToFill = NO;
        [destController setOrderObject:_orderObject];
        
    }
    else if ([segue.identifier isEqualToString:@"searchStationFrom"])
    {
        PRStationsViewController *destController = segue.destinationViewController;
        [destController.navigationItem setTitle:NSLocalizedString(@"stacjaOd", nil)];
        _orderObject.stationFromToFill = YES;
        [destController setOrderObject:_orderObject];
    }
    else if ([segue.identifier isEqualToString:@"relations"])
    {
        PRRelationsViewController *destController = segue.destinationViewController;
        _orderObject.relationBackToFill = NO;
        _orderObject.relationBack = nil;
        [destController setOrderObject:_orderObject];
    }
}

#pragma mark - private
- (void) refreshUI
{
    NSDate *date = [NSDate date];
    date = [PRUtils resetDateToMidnight:date];
    
    if([_date compare:date] == NSOrderedAscending)
    {
        _date = date;
    }
    
    _datePickerViewController.minDate = _date;
    _datePickerViewController.maxDate = [PRUtils addDays:CONFIG_MAX_DAYS_FORWARD toDate:_date];
    
    [self handleViewsAfterDateOrStationCHanges];
}

- (void) handleViewsAfterDateOrStationCHanges
{
    _labelDate.text = [PRUtils getFormattedDateFromDate:_date];
    _labelStationFrom.text = _orderObject.stationFrom.stationName;
    _labelStationTo.text = _orderObject.stationTo.stationName;
    
    if(_orderObject.stationFrom == nil)
    {
        _controlStationTo.enabled = NO;
    }
    else
    {
        _controlStationTo.enabled = YES;
    }
    
    if(_orderObject.stationTo==nil)
    {
        _buttonSearch.enabled = NO;
    }
    else
    {
        _buttonSearch.enabled = YES;
    }
}

- (void) resetOrderObject
{
    _orderObject = nil;
    _orderObject = [[PROrderObject alloc] init];
    _orderObject.orderDateFrom = _date;
}

- (void) showDateView
{
    [PRUtils slideView:_datePickerViewController.view toY:(self.view.frame.size.height - _datePickerViewController.view.frame.size.height) completion:nil];
}

- (void) hideDateView
{
    [PRUtils slideView:_datePickerViewController.view toY:self.view.frame.size.height completion:nil];
}

#pragma mark - controls actions

- (IBAction)controlDateOnClick:(id)sender {
    _datePickerViewController.datePicker.date = _date;
    [self showDateView];
}

- (IBAction)buttonSearchOnClick:(id)sender {
    [self performSegueWithIdentifier:@"relations" sender:self];
}

- (IBAction)controlStationFromOnClick:(id)sender {
    [self performSegueWithIdentifier:@"searchStationFrom" sender:sender];
}

- (IBAction)controlStationToOnClick:(id)sender {
    [self performSegueWithIdentifier:@"searchStationTo" sender:sender];
}

#pragma mark - date picker view controller delegate
- (void) dateSet:(NSDate*) date
{
    _date = date;
    _labelDate.text = [PRUtils getFormattedDateFromDate:_date];
    
    _orderObject.orderDateFrom = _date;
    
    _orderObject.stationTo = nil;
    [self handleViewsAfterDateOrStationCHanges];

    [self hideDateView];
}

- (void) dateSetCancelled
{
    [self hideDateView];
}

#pragma mark - prappdelegate

- (void) applicationBecameActive
{
    [self refreshUI];
}

@end
