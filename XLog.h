
#define XLog(msg, ...)               NSLog(msg, ##__VA_ARGS__)
#define XLogTag(tag, format, ...)    NSLog(@"[%@] " format, tag, ##__VA_ARGS__)

#ifdef DEBUG
#   define XLogDebug(msg, ...)               NSLog(msg, ##__VA_ARGS__)
#   define XLogTagDebug(tag, format, ...)    NSLog(@"[%@] " format, tag, ##__VA_ARGS__)
#else
#   define XLogDebug(msg, ...)
#   define XLogTagDebug(tag, format, ...)
#endif

NS_INLINE NSString* XErrorMsg(NSError *error) {
    return [NSString stringWithFormat: @"\nError domain: %@\nError code: %d\nError message: %@\nError userInfo: %@\n", error.domain, error.code, [error localizedDescription], [error userInfo]];
}