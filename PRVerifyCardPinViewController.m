//
//  PRVerifyCardPinViewController.m
//  pr
//
//  Created by Marcin Szulc on 24.09.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PRVerifyCardPinViewController.h"
#import "PRStandardButton.h"
#import "PRAsyncConnection.h"
#import "XLog.h"
#import "PRAlertView.h"
#import "PRVerifyCard2ViewController.h"

static NSString * const LogTag = @"VerifyCardPinViewController";

@interface PRVerifyCardPinViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *editPin;
@property (weak, nonatomic) IBOutlet PRStandardButton *okButton;
@property (strong, nonatomic) PRAlertView *alert;
@end

@implementation PRVerifyCardPinViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIBarButtonItem * mcmLogo = [[UIBarButtonItem alloc] initWithCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mcm_icon"]]];
    self.navigationItem.rightBarButtonItem = mcmLogo;
    
    [_okButton setEnabled:NO];
    [_editPin becomeFirstResponder];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqual:@"verifyCard2"] == YES)
    {
        PRVerifyCard2ViewController *dest = segue.destinationViewController;
        [dest setCard:_card];
    }
}

- (IBAction)buttonOkOnClick:(id)sender {
    
    [_okButton setLockedForResponse:YES];
    
    PRAsyncConnection *connection = [[PRAsyncConnection alloc] initWithParent:self successSelector:@selector(connectionSuccess:) didFailSelector:@selector(connectionFailed:)];
    [connection sendFreezeMoneyRequestWithCardId:_card.identifier andAuthCode:_editPin.text];
}

- (void) connectionSuccess:(PRAsyncConnection*) connection
{
    [_okButton setLockedForResponse:NO];
    
    XLogTag(LogTag, @"Response: %@", [[NSString alloc] initWithData:connection.responseData encoding:NSUTF8StringEncoding]);
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:connection.responseData
                          options:kNilOptions
                          error:&error];
    
    if(error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        [self connectionFailed:connection];
        return;
    }
    
    NSDictionary *response = [json objectForKey:@"freeze_money_response"];
    
    NSNumber *status = [response objectForKey:@"status"];
    
    if(status==nil)
    {
        [self connectionFailed:connection];
        return;
    }
    
    switch (status.intValue) {
        case STATUS_OK:
        {
            [self performSegueWithIdentifier:@"verifyCard2" sender:self];
        }
            return;
            
        case STATUS_USER_BLOCKED:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertUserBlocked", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
        case STATUS_NO_MONEY:
        {
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertNoMoney", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
        }
            return;
        case STATUS_WRONG_CARD_ID:
            _alert = [[PRAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertWrongCardId", nil) withParent:self callbackSelector:@selector(connectionFailedAlertCallback:) cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil)];
            [_alert show];
            return;
            
    }
    
}

- (void) connectionFailed:(PRAsyncConnection*) connection
{
    [_okButton setLockedForResponse:NO];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alertAttention", nil) message:NSLocalizedString(@"alertStatusError", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"alertOk", nil), nil];
    [alert show];
}

- (void) connectionFailedAlertCallback:(NSNumber*) buttonIndex
{
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return range.location<4;
}

- (IBAction)textFieldDidChange:(id)sender {
    if(_editPin.text.length>=3)
    {
        [_okButton setEnabled:YES];
    }
    else
    {
        [_okButton setEnabled:NO];
    }
}

@end
