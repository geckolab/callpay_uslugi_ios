//
//  PRTabBarViewController.h
//  CallPay
//
//  Created by Jacek on 15.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRTabBarViewController : UITabBarController

- (void) verify;
- (void) updateDatabase:(NSNumber*) buttonIndex;

@end
