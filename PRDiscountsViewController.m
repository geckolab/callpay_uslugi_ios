//
//  PRDiscountsViewController.m
//  pr
//
//  Created by Marcin Szulc on 23.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PRDiscountsViewController.h"
#import "PROrderObject.h"
//#import "PRAppDelegate.h"
#import "DiscountEntity+Additions.h"
#import "XLog.h"

#import <QuartzCore/QuartzCore.h>

#import "PRDBManager.h"

static NSString * const LogTag = @"DiscountsViewController";

@interface PRDiscountsViewController ()

@property (strong, nonatomic) NSArray *cells;

@end

@implementation PRDiscountsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    _tableDiscounts.layer.cornerRadius = 8.0;
    _tableDiscounts.layer.borderWidth = 1.0;
    _tableDiscounts.layer.borderColor = [UIColor colorWithWhite:0.66 alpha:0.6].CGColor;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTableDiscounts:nil];
    [super viewDidUnload];
}

- (void) viewWillAppear:(BOOL)animated
{
    [self fillTable];
    [super viewWillAppear:animated];
}

- (void) fillTable
{
//    PRAppDelegate *appDelegate = (PRAppDelegate*)[UIApplication sharedApplication].delegate;
    
    _cells = [DiscountEntity fetchAllDiscountsWithManagedObjectContext:[[PRDBManager object]managedObjectContext] forClass:_orderObject.vehicleClass];
    
    [_tableDiscounts reloadData];
}

#pragma mark - table view delegate and datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _cells.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[NSString stringWithFormat:@"discount%d", indexPath.row]];
    
    cell.textLabel.font = [UIFont boldSystemFontOfSize:12];
    cell.textLabel.text = ((DiscountEntity*)[_cells objectAtIndex:indexPath.row]).discountName;
    [cell.textLabel setAdjustsLetterSpacingToFitWidth:YES];
    [cell.textLabel setMinimumScaleFactor:0.5f];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _orderObject.discount = [_cells objectAtIndex:indexPath.row];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
