//
//  Car+Extensions.h
//  CallPay
//
//  Created by Jacek on 21.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "Car.h"

@interface Car (Extensions)

-(void)markDefault;

@end
