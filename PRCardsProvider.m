//
//  PRCardsProvider.m
//  pr
//
//  Created by Alexander Pimenov on 03.07.13.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved. 
//

#import "PRCardsProvider.h"
#import "PRAsyncConnection.h"
#import "XLog.h"
#import "PRUtils.h"
#import "PRCardsContainer.h"
#import "PRCardObject.h"

static NSString *const LogTag = @"PRCardsProvider";

@implementation PRCardsProvider

- (void)fetchCards
{
    PRAsyncConnection *connection = [[PRAsyncConnection alloc]
                                                        initWithParent:self
                                                       successSelector:@selector(getCardsSuccess:)
                                                       didFailSelector:@selector(getCardsFailed:)];
    [connection sendGetListOfCards];
}

- (void)getCardsSuccess:(PRAsyncConnection *)connection
{
    NSData *responseData = [connection responseData];

    XLogTag(LogTag, @"%@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);

    NSError *error;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData
                                                         options:(NSJSONReadingOptions)kNilOptions
                                                           error:&error];

    if (error)
    {
        XLogTag(LogTag, @"%@", error.localizedDescription);
        return;
    }

    NSDictionary *cards_list_response = [json objectForKey:@"cards_list_response"];

    NSNumber *status = [cards_list_response objectForKey:@"status"];

    if (status == nil)
    {
        [[self delegate] cardsProviderFailedToFetchCards:self];
        return;
    }

    PRCardsContainer *cardsContainer;

    switch (status.intValue)
    {
        case STATUS_OK:
            cardsContainer = [self cardsFromCardsData:[cards_list_response objectForKey:@"cards"]];
            [[self delegate]
                    cardsProvider:self
                     fetchedCards:cardsContainer];
            break;
        default:
            [[self delegate]
                    cardsProviderFailedToFetchCards:self
                                    withErrorStatus:status.intValue];
            break;
    }
}

- (PRCardsContainer *)cardsFromCardsData:(NSArray *)cardsData
{
    PRCardsContainer *cardsContainer = [[PRCardsContainer alloc] init];

    for (NSDictionary *cardData in cardsData)
    {
        NSDictionary *cardFields = [cardData objectForKey:@"card"];

        PRCardObject *card = [[PRCardObject alloc] init];
        [card setIdentifier:[cardFields objectForKey:@"id"]];
        [card setNumber:[cardFields objectForKey:@"pan"]];
        [card setExpirationDate:[cardFields objectForKey:@"exp_date"]];

        [card setActive:[[cardFields objectForKey:@"locked"] boolValue] == NO];
        [card setVerified:[[cardFields objectForKey:@"verified"] boolValue]];
        
        [cardsContainer addCard:card];
    }

    return cardsContainer;
}

- (void)getCardsFailed:(PRAsyncConnection *)connection
{
    [[self delegate] cardsProviderUnableConnectToServer:self];
}


@end
