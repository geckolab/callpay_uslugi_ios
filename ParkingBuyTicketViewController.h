//
//  ParkingBuyTicketViewController.h
//  CallPay
//
//  Created by Jacek on 20.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "ProjectViewController.h"

#import "ActiveTicketsHeaderView.h"

#import "UpaidMPMDelegate.h"

#import "ParkingTicket.h"

#import "CPApi.h"

@interface ParkingBuyTicketViewController : ProjectViewController <UpaidMPMDelegate, CheckExternalTransferDelegate>

@property (nonatomic, strong) IBOutlet ActiveTicketsHeaderView *headerView;

@property (nonatomic, strong) NSString *ticketName;
@property (assign) CGFloat price;
@property (nonatomic, strong) NSString *productCode;
@property (nonatomic, strong) NSString *verificationPhone;
@property (nonatomic, strong) NSString *verificationTime;

@property (nonatomic, strong) IBOutlet UILabel *ticketNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *ticketPriceLabel;
@property (nonatomic, strong) IBOutlet UILabel *numberLabel;

@property (retain, nonatomic) ParkingTicket *ticket;

-(void)setPriceLabel:(CGFloat)priceValue;

-(IBAction)buyAction:(id)sender;

- (id) initWithTicket: (ParkingTicket *) __ticket;
-(void)setupWithTicket: (ParkingTicket *) __ticket;

@end
