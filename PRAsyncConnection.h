//
//  PRAsyncConnection.h
//  pr
//
//  Created by Marcin Szulc on 17.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

//#define USE_TEST_URL

#ifdef USE_TEST_URL

    #define CONNECTION_RAPID_URL @"https://testapi.callpay.pl:8443"
    #define CONNECTION_CALLPAY_URL @"https://91.199.95.248:8443"

#else

    #define CONNECTION_CALLPAY_URL @"https://api.callpay.pl:8443"
    #define CONNECTION_RAPID_URL @"https://91.199.95.113:8443"

#endif

#define STATUS_OK 1
#define STATUS_USER_BLOCKED 2
#define STATUS_WRONG_DATE 3
#define STATUS_NO_STATIONS 4
#define STATUS_NO_RELATION_BETWEEN_STATIONS 5

#define STATUS_INVALID_PARAMETERS 6

#define STATUS_NO_MONEY 7
#define STATUS_WRONG_PIN 8

#define STATUS_INVALID_PASSWORD 9

#define STATUS_WRONG_CERT_TOKEN 10
#define STATUS_WRONG_CERT_ID 11

#define STATUS_WRON_PHONE_NO 12
#define STATUS_CALLPAY_GENERAL_ERROR 13
#define STATUS_NO_UPAID_USER 14

#define STATUS_INVALID_CARD_NUMBER 15
#define STATUS_ALREADY_REGISTERED_CARD 16

#define STATUS_UNABLE_TO_REMOVE_CARD 17

#define STATUS_NOT_AUTHORIZED_TO_REFUND 18
#define STATUS_REFUND_ABILITY_EXPIRED 19
#define STATUS_WRONG_CARD_ID 20

#define STATUS_WRONG_AMOUNT 21
#define STATUS_CARD_ALREADY_VERIFIED 22
#define STATUS_NO_MONEY_TO_VERIFY_CARD 23
#define STATUS_CARD_BLOCKED 24
#define STATUS_CANNOT_BUY_TICKET 25

#import <Foundation/Foundation.h>

@class PROrderObject;

@interface PRAsyncConnection : NSObject <NSURLConnectionDelegate>

@property (strong, nonatomic, readonly) NSMutableData *responseData;

- (id) initWithParent:(id) parent successSelector:(SEL) success didFailSelector:(SEL) didFail;

- (void) sendIsTicketRefundableRequestWithTicketSignature:(NSString*) signature andSerial: (NSString*) serial;

- (void) sendRegisterRequestWithPhoneNo:(NSString*) phoneNo andPassword:(NSString*) password andName:(NSString*) name andSurname:(NSString*) surname;

- (void) sendGetCertificateRequestWithCertToken:(NSString*) cert_token andSMScode:(NSString*) smsCode;

- (void) sendDBCheckVersionRequestWithPhoneNo:(NSString*) phoneNo;

- (void) sendUpdateStationsRequest;
- (void) sendUpdateDiscountsRequest;
- (void) sendUpdateTrainAttributesRequest;
- (void) sendUpdateIdTypesRequest;

- (void) sendGetStationsToRequestWithDate:(NSString*) date andStationFromIndex:(int) index;

- (void) sendGetRelationsRequestWithDate:(NSString*) date stationFromIndex:(int) stationFrom andStationToIndex:(int) stationTo;

- (void) sendStartJourneyRequestWithOrderObject:(PROrderObject*) orderObject;

- (void) sendAuthPaymentRequestWithOrderObject:(PROrderObject*) orderObject;

- (void) sendGetUpaidTokenWithPhoneNo:(NSString*) phoneNo;

- (void) sendGetListOfCards;

- (void) sendRegisterCardWithNumber:(NSString *)cardNumber
                     expirationDate:(NSString *)expirationDate;

- (void) sendDeleteCardWithNumber:(NSString *)cardNumber;

- (void)sendRefundTicketWithSerialNumber:(NSString *)serial
                               signature:(NSString *)signature;

- (void)sendFreezeMoneyRequestWithCardId:(NSString*) cardId andAuthCode:(NSString*) authCode;
- (void)sendVerifyCardRequestWithCardId:(NSString*) cardId andAmount:(int) amount;
- (void)sendTotalTransactionsValueRequestWithCardId:(NSString*) cardId;

@end
