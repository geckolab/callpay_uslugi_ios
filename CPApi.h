//
//  CPApi.h
//  CallPay
//
//  Created by Jacek on 24.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <Foundation/Foundation.h>

//#define CP_API_URL @"http://callpaytest.pl/callpay/"
//#define CP_API_URL @"http://callpaytest.pl.192.168.0.201.xip.io/callpay/"

#define CP_API_URL @"http://91.199.95.209:9090/callpay/"

@protocol CheckExternalTransferDelegate <NSObject>

- (void) checkExternalTransferDidSuccessWithDictionary:(NSDictionary*)dictionary;
- (void) checkExternalTransferDidFail;

@end

@protocol TransactionStartDelegate <NSObject>

- (void) transactionStartDidSuccessWithDictionary:(NSDictionary*)dictionary;
- (void) transactionStartDidFail;

@end

@interface CPApi : NSObject
{
//    id __delegateCheckExternalTransfer;
}

@property (nonatomic, strong) id delegateCheckExternalTransfer;
@property (nonatomic, strong) id delegateTransactionStart;

+ (id)sharedManager;

-(void)checkExternalTransfer;

-(void)checkExternalTransferWithPhone:(NSString*)phoneNo andProductCode:(NSString*)productCode andToken:(NSString*)token;

-(void)startTransactionNormalWithSessionToken:(NSString*)sessionToken;

@end
