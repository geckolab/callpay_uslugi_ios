//
//  ParkingTicket.h
//  CallPay
//
//  Created by Jacek on 10.06.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Car;

@interface ParkingTicket : NSManagedObject

@property (nonatomic, retain) NSString * cityIndex;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic, retain) NSString * productCode;
@property (nonatomic, retain) NSString * productName;
@property (nonatomic, retain) NSDate * purchaseDate;
@property (nonatomic, retain) NSString * securityCodeType;
@property (nonatomic, retain) NSString * securityCodeValueLength;
@property (nonatomic, retain) NSString * securityCodeValueType;
@property (nonatomic, retain) NSString * seller;
@property (nonatomic, retain) NSString * sessionToken;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSString * transactionId;
@property (nonatomic, retain) NSDate * verificationDate;
@property (nonatomic, retain) NSString * verificationPhone;
@property (nonatomic, retain) NSNumber * verificationTime;
@property (nonatomic, retain) NSNumber * duration;
@property (nonatomic, retain) Car *car;

@end
