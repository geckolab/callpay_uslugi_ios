//
//  UILabel+CustomFont.m
//  CallPay
//
//  Created by Jacek on 01.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "UILabel+CustomFont.h"

@implementation UILabel (CustomFont)

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (NSString *)fontName {
    return self.font.fontName;
}

- (void)setFontName:(NSString *)fontName {
    self.font = [UIFont fontWithName:fontName size:self.font.pointSize];
}


@end
