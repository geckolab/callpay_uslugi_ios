//
//  TicketEntity.h
//  CallPay
//
//  Created by Jacek on 15.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TransactionEntity;

@interface TicketEntity : NSManagedObject

@property (nonatomic, retain) NSString * ticketCarrier;
@property (nonatomic, retain) NSString * ticketCategoryReturn;
@property (nonatomic, retain) NSString * ticketCategoryTo;
@property (nonatomic, retain) NSNumber * ticketClassReturn;
@property (nonatomic, retain) NSNumber * ticketClassTo;
@property (nonatomic, retain) NSString * ticketCode;
@property (nonatomic, retain) NSString * ticketDescription;
@property (nonatomic, retain) NSString * ticketDiscount;
@property (nonatomic, retain) NSNumber * ticketDistanceReturn;
@property (nonatomic, retain) NSNumber * ticketDistanceTo;
@property (nonatomic, retain) NSString * ticketFrom;
@property (nonatomic, retain) NSNumber * ticketIsRefundable;
@property (nonatomic, retain) NSString * ticketOffer;
@property (nonatomic, retain) NSNumber * ticketOwnerDocument;
@property (nonatomic, retain) NSString * ticketOwnerName;
@property (nonatomic, retain) NSString * ticketOwnerSerial;
@property (nonatomic, retain) NSString * ticketOwnerSurname;
@property (nonatomic, retain) NSNumber * ticketPrice;
@property (nonatomic, retain) NSString * ticketSerial;
@property (nonatomic, retain) NSString * ticketSignature;
@property (nonatomic, retain) NSString * ticketThrough;
@property (nonatomic, retain) NSString * ticketTo;
@property (nonatomic, retain) NSString * ticketValidReturn;
@property (nonatomic, retain) NSNumber * ticketValidReturnHours;
@property (nonatomic, retain) NSString * ticketValidTo;
@property (nonatomic, retain) NSNumber * ticketValidToHours;
@property (nonatomic, retain) NSNumber * ticketValue;
@property (nonatomic, retain) NSNumber * ticketVatRate;
@property (nonatomic, retain) NSNumber * ticketVatValue;
@property (nonatomic, retain) TransactionEntity *transaction;

@end
