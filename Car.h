//
//  Car.h
//  CallPay
//
//  Created by Jacek on 21.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ParkingTicket;

@interface Car : NSManagedObject

@property (nonatomic, retain) NSString * plateNumber;
@property (nonatomic, retain) NSNumber * defaultCar;
@property (nonatomic, retain) ParkingTicket *tickets;

@end
