//
//  PRTrainAttributeCell.m
//  pr
//
//  Created by Marcin Szulc on 22.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PRTrainAttributeCell.h"

@implementation PRTrainAttributeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
