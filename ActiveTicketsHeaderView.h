//
//  ActiveTicketsHeaderView.h
//  CallPay
//
//  Created by Jacek on 30.03.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActiveTicketsHeaderView : UIView
{
    UIImage *iconImg;
    UIImageView *iconImageView;
    NSString *title;
    UILabel *titleLabel;
}

-(void)setTitle:(NSString *) newTitle;
-(void)setIcon:(UIImage *) newIcon;

-(void)setup;

@end
