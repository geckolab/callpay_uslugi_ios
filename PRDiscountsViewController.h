//
//  PRDiscountsViewController.h
//  pr
//
//  Created by Marcin Szulc on 23.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PROrderObject;

@interface PRDiscountsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) PROrderObject *orderObject;

@property (strong, nonatomic) IBOutlet UITableView *tableDiscounts;

@end
