//
//  TicketEntity+Additions.h
//  pr
//
//  Created by Marcin Szulc on 24.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "TicketEntity.h"

#define TICKET_ACTIVE_SECONDS (5*60)

@class PROrderObject;

@interface TicketEntity (Additions)

+ (TicketEntity*) insertTicketEntityWithValuesFromJsonResponseTicket:(NSDictionary*) _ticket withManagedObjectContext:(NSManagedObjectContext*) managedObjectContext;

+ (NSArray*) fetchAllTicketsWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext;

+ (BOOL) isTicketActive:(TicketEntity*) ticket;
+ (int) getSecondsSinceTicketBought:(TicketEntity*) ticket;
+ (BOOL) isTicketValid:(TicketEntity*) ticket;

@end
