//
//  CPMPMButtons.m
//  CallPay
//
//  Created by Jacek on 09.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "CPMPMButtons.h"

@implementation CPMPMButtons

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)setup
{
    [super setup];
    [self.titleLabel setTextColor:[UIColor whiteColor]];
    [self.titleLabel setFont:[UIFont fontWithName:@"Swiss721LtEU" size:self.titleLabel.font.pointSize]];
}

-(void)setTitle:(NSString *)title forState:(UIControlState)state
{
    [super setTitle:title forState:state];
    if (self.currentImage.size.width > 0)
    {
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
    }
    else
    {
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
    }
}
-(void)setImage:(UIImage *)image forState:(UIControlState)state
{
    [super setImage:image forState:state];
    if (image.size.width > 0)
    {
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
    }
    else
    {
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
    }
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    if(self.currentImage == nil || self.currentImage.size.width == 0)
    {
        return CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    }
    else
    {
        if(self.currentImage.size.height <= self.frame.size.height)
        {
            return CGRectMake((self.currentImage.size.width / 2) + 20, 0, self.frame.size.width - 20 - (self.currentImage.size.width / 2), self.frame.size.height);
        }
        else
            return CGRectMake(self.currentImage.size.width + 20, 0, self.frame.size.width - 20 - self.currentImage.size.width, self.frame.size.height);
    }
}

- (CGRect)imageRectForContentRect:(CGRect)contentRect
{//85
    if(self.currentImage.size.height <= self.frame.size.height)
    {
        return CGRectMake( 10 , (self.frame.size.height - (self.currentImage.size.height / 2)) / 2, self.currentImage.size.width / 2, self.currentImage.size.height / 2);
    }
    else
    {
        return CGRectMake( 10 , (self.frame.size.height - self.currentImage.size.height) / 2, self.currentImage.size.width, self.currentImage.size.height);
    }
}
- (void) drawLayer:(CALayer*) layer inContext:(CGContextRef) ctx
{
    NSLog(@"draw layer");
    layer.contentsScale = [UIScreen mainScreen].scale;
    if ([self isHighlighted] || [self isSelected])
    {
        [layer setBackgroundColor:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.48].CGColor];
    }
    else
    {
        [layer setBackgroundColor:[UIColor clearColor].CGColor];
    }
    
    CGContextSetStrokeColorWithColor(ctx, [UIColor whiteColor].CGColor);
    CGContextSetLineWidth(ctx, 2);
    
    CGContextMoveToPoint(ctx, 0, 0);
    CGContextAddLineToPoint(ctx, layer.frame.size.width, 0);
    
    CGContextStrokePath(ctx);
    
    CGContextSetLineWidth(ctx, 2);
    
    CGContextMoveToPoint(ctx, 0, layer.frame.size.height-2);
    CGContextAddLineToPoint(ctx, layer.frame.size.width, layer.frame.size.height-2);
    
    CGContextStrokePath(ctx);
    
    
}

@end
