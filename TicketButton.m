//
//  TicketButton.m
//  CallPay
//
//  Created by Jacek on 24.03.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "TicketButton.h"

@implementation TicketButton

@synthesize contentSubview,nameLabel,typeLabel,timeLabel,typeIconView;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setup];
    }
    return self;
}
- (id)init
{
    self = [super init];
    if (self)
    {
        [self setFrame:CGRectMake(0, 0, 115, 168)];
        [self setup];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self setup];
    }
    return self;
}

-(void)setup
{
    [[NSBundle mainBundle] loadNibNamed:@"TicketButton" owner:self options:nil];
    //self.backgroundColor = [UIColor redColor];
    contentSubview.backgroundColor = [UIColor clearColor];
    [self setBackgroundImage:[UIImage imageNamed:@"bil_ramka_0.png"] forState:UIControlStateNormal];
    [self setBackgroundImage:[UIImage imageNamed:@"bil_ramka_1.png"] forState:UIControlStateSelected];
    [self setBackgroundImage:[UIImage imageNamed:@"bil_ramka_1.png"] forState:UIControlStateHighlighted];
    [self setBackgroundImage:[UIImage imageNamed:@"bil_ramka_1.png"] forState:UIControlStateDisabled];
    [self setTitle:@"" forState:UIControlStateNormal];
//    [self addSubview:contentSubview];
    for (UIView *sv in contentSubview.subviews)
    {
        [self insertSubview:sv belowSubview:self.imageView];
    }
    ticketName = nameLabel.text;
    [typeLabel setFont:[UIFont fontWithName:@"Swis721LtEU" size:12]];
    [timeLabel setFont:[UIFont fontWithName:@"Swis721LtEU" size:12]];
//    [self insertSubview:contentSubview belowSubview:self.imageView];
    typeLabel.textAlignment = NSTextAlignmentLeft;//typeLabel.backgroundColor = [UIColor greenColor];
    [self setNormalny:NO];
}

-(void)setNormalny:(BOOL)type
{
    normalnyEnabled = type;
    NSRange range1 = NSMakeRange(0, ticketName.length);
    NSString *ticketNameType;
    if(type)
    {
        [typeLabel setText:@"Bilet\nnormalny"];
        ticketNameType = @" N";
        [typeIconView setImage:[UIImage imageNamed:@"bil_normalny.png"]];
    }
    else
    {
        [typeLabel setText:@"Bilet\nulgowy"];
        ticketNameType = @" U";
        [typeIconView setImage:[UIImage imageNamed:@"bil_ulgowy.png"]];
    }
    NSRange range2 = NSMakeRange(ticketName.length, ticketNameType.length);
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:[ticketName stringByAppendingString:ticketNameType]];
    [attrStr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:range1];
    [attrStr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:range2];
    [attrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Swis721LtEU" size:74] range:range1];
//    [attrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Thin" size:20] range:range2];
    
    [attrStr setAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"Swis721LtEU" size:20]
                                      , NSBaselineOffsetAttributeName : @36} range:range2];
    
    [nameLabel setAttributedText:attrStr];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
}
@end
