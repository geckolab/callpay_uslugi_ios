//
//  StationEntity+Additions.h
//  pr
//
//  Created by Marcin Szulc on 18.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "StationEntity.h"

@interface StationEntity (Additions)

+ (StationEntity*) insertStationWithId:(NSNumber*) index andName:(NSString*) name withManagedObjectContext:(NSManagedObjectContext*) managedObjectContext;

+ (void) clearAllRecordsWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext;

+ (NSArray*) fetchAllStationsWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext;
+ (NSArray*) fetchAllStationsWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext whereStationIdWitihinIndexes:(NSMutableArray*) indexes;

+ (NSArray*) fetchAllStationsWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext whereNameBeginsWithString:(NSString*) name;
+ (NSArray*) fetchAllStationsWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext whereNameBeginsWithString:(NSString*) name andStationIdWitihinIndexes:(NSMutableArray*) indexes;

+ (StationEntity*) fetchStationAtIndex:(int) index withManagedObjectContext:(NSManagedObjectContext*) managedObjectContext;

@end
