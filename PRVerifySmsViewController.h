//
//  PRVerifySmsViewController.h
//  pr
//
//  Created by Marcin Szulc on 17.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *const PRVerifySMSViewControllerVerifiedUserNotification;

@class PRStandardButton;


@interface PRVerifySmsViewController : ProjectViewController

@property (strong, nonatomic) NSString *cert_token;
@property (strong, nonatomic) NSString *phoneNo;

@property (strong, nonatomic) NSString *stringSmsCode;

@property (strong, nonatomic) IBOutlet PRStandardButton *buttonVerify;

@property (strong, nonatomic) IBOutlet UITextField *textSmsCode;

- (IBAction)buttonVerifyOnClick:(id)sender;
- (IBAction)backgroundTouched:(id)sender;

@end
