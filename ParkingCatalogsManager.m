//
//  ParkingCatalogsManager.m
//  CallPay
//
//  Created by Jacek on 13.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "ParkingCatalogsManager.h"
#import "ProjectLocations.h"

@implementation ParkingCatalogsManager

- (id) initWithDelegate: (id) delegate {
    self = [super init];
    
    if (self) {
        _delegate = delegate;
        self.catalogsToUpdate = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (NSDictionary *) parseJsonForCurrentCity {
    NSString *cacheFile = [self cacheFileForCity: [ProjectLocations object].currentParkingCity];
    
    
    if([[NSFileManager defaultManager] fileExistsAtPath:cacheFile]) {
        NSError *jsonParsingError = nil;
        
        return [NSJSONSerialization JSONObjectWithData: [NSData dataWithContentsOfFile:cacheFile] options:0 error:&jsonParsingError];
    }
    
    return @{};
}

- (void) startDownload: (NSString *) cityIndex {
    _cityIndex = cityIndex;
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
//    NSString *url = [NSString stringWithFormat: @"%@/callpay/catalog/%@/%@", UPAID_URL_PREFIX, [prefs stringForKey:PHONE_NUM_KEY], cityIndex];
    NSString *url = [NSString stringWithFormat: @"%@/callpay/parkingcatalog/0/%@%@", UPAID_URL_PREFIX, cityIndex, UPAID_URL_SUFIX];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString: url]];
    [request setUseKeychainPersistence:YES];NSLog(@"%@",url);
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestFinished:)];
    [request setDidFailSelector:@selector(requestFailed:)];
    [request startAsynchronous];
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient networkIndicator:YES];
}

- (void)requestFinished:(ASIHTTPRequest *)request{
    NSData *data = [request.responseString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *cacheDirectory = [self cacheDirectory];
    NSString *cacheFile = [self cacheFileForCity: _cityIndex];
    
    NSError * error = nil;
    [[NSFileManager defaultManager] createDirectoryAtPath:cacheDirectory
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:&error];
    if ([data writeToFile:cacheFile atomically:YES]) {
        [[NSUserDefaults standardUserDefaults] setValue:[[self dictionaryForCityIndex: _cityIndex] valueForKey:@"updated"] forKey:[NSString stringWithFormat: @"parking_location_%@", _cityIndex]];
        [[NSUserDefaults standardUserDefaults] synchronize];
        UpaidLog(@"zapisano defaults dla %@", [NSString stringWithFormat: @"parking_location_%@", _cityIndex]);
        
        if ([self.catalogsToUpdate count] > 0) {
            [self updateCatalogsCache];
        } else {
            [SVProgressHUD dismissWithSuccess:@"Katalog pobrany pomyślnie" afterDelay:2.0f];
            [_delegate catalogDownloadSuccess:_cityIndex];
        }
    } else {
        [self requestFailed:request];
    }
}

- (NSDictionary *) dictionaryForCityIndex: (NSString *) cityIndex {
    return [[[ProjectLocations object] parkingLocationList] objectForKey: cityIndex];
}

- (NSString *) cacheDirectory {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    
    return [NSString stringWithFormat:@"%@/JSON", [paths objectAtIndex:0]];
}

- (NSString *) cacheFileForCity: (NSString *) cityIndex {
    return [NSString stringWithFormat:@"%@/parking_%@.json", [self cacheDirectory], cityIndex];
}

- (void)requestFailed:(ASIHTTPRequest *)request{
    [SVProgressHUD dismissWithError:@"Nie udało się pobrać katalogu, spróbuj ponownie później" afterDelay: 2.0f];
    [_delegate catalogDownloadFail:_cityIndex];
}

- (void) deleteCatalogForCity: (NSString *) cityIndex {
    NSError *error;
    NSString *filePath = [self cacheFileForCity:cityIndex];
    
    if (![[NSFileManager defaultManager] removeItemAtPath:filePath error:&error]) {
        UpaidLog(@"Unable to delete file: %@", [error localizedDescription]);
    } else {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat: @"parking_location_%@", cityIndex]];
        UpaidLog(@"usunieto defaults dla %@", [NSString stringWithFormat: @"parking_location_%@", cityIndex]);
    }
}

- (BOOL) checkCatalogsCache {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    for (NSString* cityIndex in [[ProjectLocations object] parkingLocationList]) {
        NSDictionary *dictionary = [[[ProjectLocations object] parkingLocationList] objectForKey:cityIndex];
        NSNumber *updated = [prefs valueForKey: [NSString stringWithFormat: @"parking_location_%@", cityIndex]];
        UpaidLog(@"odczytano defaults dla %@ : %@", [NSString stringWithFormat: @"parking_location_%@", cityIndex], updated);
        
        if (updated != nil) {
            if ([updated intValue] != [[dictionary valueForKey:@"updated"] intValue]) {
                UpaidLog(@"%d %d", [updated intValue], [[dictionary valueForKey:@"updated"] intValue]);
                [self.catalogsToUpdate addObject:cityIndex];
            }
        }
    }
    
    return [self.catalogsToUpdate count] == 0;
}

- (NSString *) sizeToUpdate {
    int sum = 0;
    
    for (NSString *cityIndex in self.catalogsToUpdate) {
        sum += [[[self dictionaryForCityIndex:cityIndex] valueForKey:@"size"] integerValue];
    }
    
    return [NSString stringWithFormat:@"%.2f", sum / 1024.0];
}

- (NSString *) nameForCurrentCity {
    return [[self dictionaryForCityIndex:[ProjectLocations object].currentParkingCity] valueForKey: @"name"];
}

- (void) deleteCatalogsCache {
    [SVProgressHUD show];
    [self _deleteCatalogsCache: YES];
    [SVProgressHUD dismissWithSuccess:@"Usunięto zapisane pliki." afterDelay:2.0f];
}

- (void) _deleteCatalogsCache: (BOOL) clearArray {
    for (NSString *cityIndex in self.catalogsToUpdate) {
        [self deleteCatalogForCity:cityIndex];
    }
    
    if (clearArray) {
        [self.catalogsToUpdate removeAllObjects];
    }
}

- (void) updateCatalogsCache {
    [self _deleteCatalogsCache:NO];
    NSString *currentCityIndex = [self.catalogsToUpdate lastObject];
    [self.catalogsToUpdate removeLastObject];
    [self startDownload:currentCityIndex];
}

- (void) updateCurrentCityCache {
    [self _deleteCatalogsCache: YES];
    [self startDownload:[ProjectLocations object].currentParkingCity];
}



@end
