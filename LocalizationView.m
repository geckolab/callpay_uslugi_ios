//
//  LocalizationView.m
//  CallPay
//
//  Created by Jacek on 26.03.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "LocalizationView.h"

@implementation LocalizationView

@synthesize btnSwith;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setup];
    }
    return self;
}
- (id)init
{
    self = [super init];
    if (self)
    {
        [self setFrame:CGRectMake(0, 0, 320, 100)];
        [self setup];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self setup];
    }
    return self;
}
-(void)setup
{
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 24)];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setFont:[UIFont fontWithName:@"Swis721LtEU" size:20]];
    [titleLabel setText:@"Lokalizacja"];
    
    [self addSubview:titleLabel];
    
    self.btnSwith = [[CustomSwitch alloc] init];
    self.btnSwith.frame = CGRectMake(0, titleLabel.frame.size.height+10, btnSwith.frame.size.width, btnSwith.frame.size.height);
    [self addSubview:self.btnSwith];
    
    descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(65, titleLabel.frame.size.height+10, self.frame.size.width - 85, 30)];
    [descriptionLabel setNumberOfLines:2];
    [descriptionLabel setTextColor:[UIColor whiteColor]];
    [descriptionLabel setFont:[UIFont fontWithName:@"Swis721LtEU" size:10]];
    [self addSubview:descriptionLabel];
    [descriptionLabel setText:@"Zaznacz tę opcję, jeśli chcesz aby aplikacja\nmogła zmieniać dane lokalizacyjne."];
}

@end
