//
//  PRRelationObject.m
//  pr
//
//  Created by Marcin Szulc on 21.01.2013.
//  Copyright (c) 2013 Marcin Szulc. All rights reserved.
//

#import "PRRelationObject.h"

@implementation PRRelationAttributeObject

- (id) init
{
    self = [super init];
    if(self)
    {}
    return self;
}

@end

@implementation PRRelationObject

- (id) init
{
    self = [super init];
    if(self)
    {
        _relationSelectedClass = [NSNumber numberWithInt:2];
    }
    return self;
}

@end
