//
//  IdTypeEntity.m
//  CallPay
//
//  Created by Jacek on 15.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "IdTypeEntity.h"


@implementation IdTypeEntity

@dynamic idTypeId;
@dynamic idTypeName;
@dynamic idTypeNumber;

@end
