//
//  CarSelectCell.m
//  CallPay
//
//  Created by Jacek on 13.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "CarSelectCell.h"

@implementation CarSelectCell

@synthesize carLabel = _carLabel;
@synthesize barsView = _barsView;
@synthesize activeView = _activeView;

- (void)awakeFromNib {
    _barsView = [[RectangleWhiteBarsView alloc] initWithFrame:CGRectMake(0, -1, self.bounds.size.width, self.bounds.size.height+1)];
    [self.contentView insertSubview:_barsView atIndex:0];
    _carLabel.text = @"";
    [self setActive:NO];
    [self setCarLabelText:@""];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    [_barsView setSelected:selected];
}
-(void)prepareForReuse
{
    [super prepareForReuse];
    _carLabel.text = @"";
    [self setActive:NO];
}
-(void)setCarLabelText:(NSString*)text
{
    _carLabel.text = [text uppercaseString];
    [_carLabel sizeToFit];
}
-(void)setActive:(BOOL)active
{
    [_activeView setActive:active];
}

@end
