//
//  ActiveTicketCell.m
//  CallPay
//
//  Created by Jacek on 07.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "ActiveTicketCell.h"

@implementation ActiveTicketCell

@synthesize nameLabel = _nameLabel;
@synthesize buyDateLabel = _buyDateLabel;

- (void)awakeFromNib {
    barsView = [[RectangleWhiteBarsView alloc] initWithFrame:CGRectMake(0, -1, self.bounds.size.width, self.bounds.size.height+1)];
    //    [self.backgroundView addSubview:barsView];
    //    [self.contentView addSubview:barsView];
    [self.contentView insertSubview:barsView atIndex:0];
    [_nameLabel setText:@""];
    [_buyDateLabel setText:@""];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)prepareForReuse
{
    [super prepareForReuse];
    [_nameLabel setText:@""];
    [_buyDateLabel setText:@""];
}

@end
