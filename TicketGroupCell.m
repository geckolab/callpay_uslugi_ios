//
//  TicketGroupCell.m
//  CallPay
//
//  Created by Jacek on 03.04.2015.
//  Copyright (c) 2015 uPaid. All rights reserved.
//

#import "TicketGroupCell.h"

@implementation TicketGroupCell

@synthesize groupNameLabel = _groupNameLabel;

- (void)awakeFromNib {
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    _groupNameLabel.text = @"";
    barsView = [[RectangleWhiteBarsView alloc] initWithFrame:CGRectMake(0, -1, self.bounds.size.width, self.bounds.size.height+1)];
    [self.contentView addSubview:barsView];
    self.backgroundColor = [UIColor clearColor];
    UIImageView *accView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 13, 23)];
    [accView setImage:[UIImage imageNamed:@"new_icon_more"]];
    self.accessoryView = accView;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    [barsView setSelected:selected];
}
-(void)prepareForReuse
{
    [super prepareForReuse];
    _groupNameLabel.text = @"";
    [self setNeedsDisplay];
}

@end
